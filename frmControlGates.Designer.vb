<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmControlGates
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtEtiqueta As System.Windows.Forms.TextBox
	Public WithEvents txtFaixa As System.Windows.Forms.TextBox
	Public WithEvents txtAlbara As System.Windows.Forms.TextBox
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl38 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmControlGates))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtEtiqueta = New System.Windows.Forms.TextBox
		Me.txtFaixa = New System.Windows.Forms.TextBox
		Me.txtAlbara = New System.Windows.Forms.TextBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl38 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.BackColor = System.Drawing.Color.FromARGB(192, 192, 192)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "CONTROL FAIXES GATES"
		Me.ClientSize = New System.Drawing.Size(377, 277)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmControlGates"
		Me.txtEtiqueta.AutoSize = False
		Me.txtEtiqueta.Size = New System.Drawing.Size(193, 37)
		Me.txtEtiqueta.Location = New System.Drawing.Point(126, 172)
		Me.txtEtiqueta.Maxlength = 13
		Me.txtEtiqueta.TabIndex = 2
		Me.txtEtiqueta.AcceptsReturn = True
		Me.txtEtiqueta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEtiqueta.BackColor = System.Drawing.SystemColors.Window
		Me.txtEtiqueta.CausesValidation = True
		Me.txtEtiqueta.Enabled = True
		Me.txtEtiqueta.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEtiqueta.HideSelection = True
		Me.txtEtiqueta.ReadOnly = False
		Me.txtEtiqueta.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEtiqueta.MultiLine = False
		Me.txtEtiqueta.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEtiqueta.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEtiqueta.TabStop = True
		Me.txtEtiqueta.Visible = True
		Me.txtEtiqueta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEtiqueta.Name = "txtEtiqueta"
		Me.txtFaixa.AutoSize = False
		Me.txtFaixa.Size = New System.Drawing.Size(193, 37)
		Me.txtFaixa.Location = New System.Drawing.Point(126, 96)
		Me.txtFaixa.Maxlength = 13
		Me.txtFaixa.TabIndex = 1
		Me.txtFaixa.AcceptsReturn = True
		Me.txtFaixa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFaixa.BackColor = System.Drawing.SystemColors.Window
		Me.txtFaixa.CausesValidation = True
		Me.txtFaixa.Enabled = True
		Me.txtFaixa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFaixa.HideSelection = True
		Me.txtFaixa.ReadOnly = False
		Me.txtFaixa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFaixa.MultiLine = False
		Me.txtFaixa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFaixa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFaixa.TabStop = True
		Me.txtFaixa.Visible = True
		Me.txtFaixa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFaixa.Name = "txtFaixa"
		Me.txtAlbara.AutoSize = False
		Me.txtAlbara.Size = New System.Drawing.Size(195, 37)
		Me.txtAlbara.Location = New System.Drawing.Point(126, 18)
		Me.txtAlbara.Maxlength = 14
		Me.txtAlbara.TabIndex = 0
		Me.txtAlbara.AcceptsReturn = True
		Me.txtAlbara.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtAlbara.BackColor = System.Drawing.SystemColors.Window
		Me.txtAlbara.CausesValidation = True
		Me.txtAlbara.Enabled = True
		Me.txtAlbara.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtAlbara.HideSelection = True
		Me.txtAlbara.ReadOnly = False
		Me.txtAlbara.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtAlbara.MultiLine = False
		Me.txtAlbara.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtAlbara.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtAlbara.TabStop = True
		Me.txtAlbara.Visible = True
		Me.txtAlbara.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtAlbara.Name = "txtAlbara"
		Me.Label2.BackColor = System.Drawing.Color.FromARGB(192, 192, 192)
		Me.Label2.Text = "Etiqueta"
		Me.Label2.Size = New System.Drawing.Size(111, 33)
		Me.Label2.Location = New System.Drawing.Point(12, 176)
		Me.Label2.TabIndex = 5
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.BackColor = System.Drawing.Color.FromARGB(192, 192, 192)
		Me.Label1.Text = "Faixa"
		Me.Label1.Size = New System.Drawing.Size(111, 33)
		Me.Label1.Location = New System.Drawing.Point(12, 100)
		Me.Label1.TabIndex = 4
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl38.BackColor = System.Drawing.Color.FromARGB(192, 192, 192)
		Me.lbl38.Text = "Albar�"
		Me.lbl38.Size = New System.Drawing.Size(111, 33)
		Me.lbl38.Location = New System.Drawing.Point(12, 22)
		Me.lbl38.TabIndex = 3
		Me.lbl38.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl38.Enabled = True
		Me.lbl38.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl38.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl38.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl38.UseMnemonic = True
		Me.lbl38.Visible = True
		Me.lbl38.AutoSize = False
		Me.lbl38.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl38.Name = "lbl38"
		Me.Controls.Add(txtEtiqueta)
		Me.Controls.Add(txtFaixa)
		Me.Controls.Add(txtAlbara)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl38)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class