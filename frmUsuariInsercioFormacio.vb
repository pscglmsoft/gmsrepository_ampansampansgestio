Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmUsuariInsercioFormacio
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic FlagExtern As Boolean
	''OKPublic RegistreExtern As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub frmUsuariInsercioFormacio_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmUsuariInsercioFormacio_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmUsuariInsercioFormacio_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmUsuariInsercioFormacio_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	
	Private Sub txtCentro_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentro.GotFocus
		XGotFocus(Me, txtCentro)
	End Sub
	
	Private Sub txtCentro_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentro.LostFocus
		XLostFocus(Me, txtCentro)
	End Sub
	
	Private Sub txtFechaFin_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaFin.GotFocus
		XGotFocus(Me, txtFechaFin)
	End Sub
	
	Private Sub txtFechaFin_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaFin.LostFocus
		XLostFocus(Me, txtFechaFin)
	End Sub
	
	Private Sub txtUsuario_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.GotFocus
		XGotFocus(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.DoubleClick
		ConsultaTaula(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.LostFocus
		If txtUsuario.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUsuario.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFecha_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFecha.GotFocus
		XGotFocus(Me, txtFecha)
	End Sub
	
	Private Sub txtFecha_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFecha.LostFocus
		If txtFecha.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtFecha)
	End Sub
	
	Private Sub txtOrigen_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOrigen.GotFocus
		XGotFocus(Me, txtOrigen)
	End Sub
	
	Private Sub txtOrigen_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOrigen.DoubleClick
		ConsultaTaula(Me, txtOrigen)
	End Sub
	
	Private Sub txtOrigen_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOrigen.LostFocus
		If txtOrigen.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtOrigen)
	End Sub
	
	Private Sub txtOrigen_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtOrigen.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFormacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFormacion.GotFocus
		XGotFocus(Me, txtFormacion)
	End Sub
	
	Private Sub txtFormacion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFormacion.DoubleClick
		ConsultaTaula(Me, txtFormacion)
	End Sub
	
	Private Sub txtFormacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFormacion.LostFocus
		XLostFocus(Me, txtFormacion)
	End Sub
	
	Private Sub txtFormacion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFormacion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObervaciones_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObervaciones.GotFocus
		XGotFocus(Me, txtObervaciones)
	End Sub
	
	Private Sub txtObervaciones_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObervaciones.LostFocus
		XLostFocus(Me, txtObervaciones)
	End Sub
	
	Private Sub txtObervaciones_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObervaciones.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
