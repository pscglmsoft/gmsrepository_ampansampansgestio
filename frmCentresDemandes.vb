Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmCentresDemandes
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	
	Private Sub chkTothomLesPotGestionar_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTothomLesPotGestionar.GotFocus
		XGotFocus(Me, chkTothomLesPotGestionar)
	End Sub
	
	Private Sub chkTothomLesPotGestionar_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTothomLesPotGestionar.LostFocus
		XLostFocus(Me, chkTothomLesPotGestionar)
	End Sub
	
	Private Sub frmCentresDemandes_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmCentresDemandes_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmCentresDemandes_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmCentresDemandes_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCodi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.GotFocus
		XGotFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.DoubleClick
		ConsultaTaula(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.LostFocus
		If txtCodi.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtCodi)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcioCentre_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcioCentre.GotFocus
		XGotFocus(Me, txtDescripcioCentre)
	End Sub
	
	Private Sub txtDescripcioCentre_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcioCentre.LostFocus
		XLostFocus(Me, txtDescripcioCentre)
	End Sub
	
	Private Sub txtDescripcioCentre_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcioCentre.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtDescripcioCentre)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	Private Sub txtEmail_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmail.GotFocus
		XGotFocus(Me, txtEmail)
	End Sub
	
	Private Sub txtEmail_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmail.LostFocus
		XLostFocus(Me, txtEmail)
	End Sub
	
	Private Sub txtResponsableCentre_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsableCentre.GotFocus
		XGotFocus(Me, txtResponsableCentre)
	End Sub
	
	Private Sub txtResponsableCentre_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsableCentre.DoubleClick
		ConsultaTaula(Me, txtResponsableCentre)
	End Sub
	
	Private Sub txtResponsableCentre_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsableCentre.LostFocus
		XLostFocus(Me, txtResponsableCentre)
	End Sub
	
	Private Sub txtResponsableCentre_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtResponsableCentre.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtResponsableCentre)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtResponsableCentre)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtUsuariEntigest_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuariEntigest.GotFocus
		XGotFocus(Me, txtUsuariEntigest)
	End Sub
	
	Private Sub txtUsuariEntigest_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuariEntigest.DoubleClick
		ConsultaTaula(Me, txtUsuariEntigest)
	End Sub
	
	Private Sub txtUsuariEntigest_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuariEntigest.LostFocus
		XLostFocus(Me, txtUsuariEntigest)
	End Sub
	
	Private Sub txtUsuariEntigest_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUsuariEntigest.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtUsuariEntigest)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtUsuariEntigest)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDataBaixa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataBaixa.GotFocus
		XGotFocus(Me, txtDataBaixa)
	End Sub
	
	Private Sub txtDataBaixa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataBaixa.LostFocus
		XLostFocus(Me, txtDataBaixa)
	End Sub
End Class
