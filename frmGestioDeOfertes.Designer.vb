<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmGestioDeOfertes
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents rtfNotes As System.Windows.Forms.RichTextBox
	Public WithEvents TabControlPage5 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdInsercions As AxFlexCell.AxGrid
	Public WithEvents TabControlPage4 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdEntrevistat As AxFlexCell.AxGrid
	Public WithEvents TabControlPage3 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdPresentats As AxFlexCell.AxGrid
	Public WithEvents TabControlPage2 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents txtObservacions As System.Windows.Forms.TextBox
	Public WithEvents txtGrauDiscapacitatSuperi As System.Windows.Forms.TextBox
	Public WithEvents Text11 As System.Windows.Forms.TextBox
	Public WithEvents txtLlocTreball As System.Windows.Forms.TextBox
	Public WithEvents txtNumLlocs As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents txtMotiuBaixa As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtTecnic As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtServeiGestor As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtSucursal As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtEntitat As System.Windows.Forms.TextBox
	Public WithEvents txtDataAlta As AxDataControl.AxGmsData
	Public WithEvents txtDataBaixa As AxDataControl.AxGmsData
	Public WithEvents cmbCertificatDiscapacitat As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents lbl14 As System.Windows.Forms.Label
	Public WithEvents lbl13 As System.Windows.Forms.Label
	Public WithEvents lbl12 As System.Windows.Forms.Label
	Public WithEvents lbl11 As System.Windows.Forms.Label
	Public WithEvents lbl10 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents TabControlPage1 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents TabControl1 As AxXtremeSuiteControls.AxTabControl
	Public WithEvents txtCodi As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcio As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmGestioDeOfertes))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.TabControl1 = New AxXtremeSuiteControls.AxTabControl
		Me.TabControlPage5 = New AxXtremeSuiteControls.AxTabControlPage
		Me.rtfNotes = New System.Windows.Forms.RichTextBox
		Me.TabControlPage4 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdInsercions = New AxFlexCell.AxGrid
		Me.TabControlPage3 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdEntrevistat = New AxFlexCell.AxGrid
		Me.TabControlPage2 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdPresentats = New AxFlexCell.AxGrid
		Me.TabControlPage1 = New AxXtremeSuiteControls.AxTabControlPage
		Me.txtObservacions = New System.Windows.Forms.TextBox
		Me.txtGrauDiscapacitatSuperi = New System.Windows.Forms.TextBox
		Me.Text11 = New System.Windows.Forms.TextBox
		Me.txtLlocTreball = New System.Windows.Forms.TextBox
		Me.txtNumLlocs = New System.Windows.Forms.TextBox
		Me.Text9 = New System.Windows.Forms.TextBox
		Me.txtMotiuBaixa = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtTecnic = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtServeiGestor = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtSucursal = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtEntitat = New System.Windows.Forms.TextBox
		Me.txtDataAlta = New AxDataControl.AxGmsData
		Me.txtDataBaixa = New AxDataControl.AxGmsData
		Me.cmbCertificatDiscapacitat = New AxSSDataWidgets_B.AxSSDBCombo
		Me.lbl14 = New System.Windows.Forms.Label
		Me.lbl13 = New System.Windows.Forms.Label
		Me.lbl12 = New System.Windows.Forms.Label
		Me.lbl11 = New System.Windows.Forms.Label
		Me.lbl10 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.txtCodi = New System.Windows.Forms.TextBox
		Me.txtDescripcio = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.TabControl1.SuspendLayout()
		Me.TabControlPage5.SuspendLayout()
		Me.TabControlPage4.SuspendLayout()
		Me.TabControlPage3.SuspendLayout()
		Me.TabControlPage2.SuspendLayout()
		Me.TabControlPage1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.TabControlPage5, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdInsercions, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage4, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdEntrevistat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdPresentats, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataAlta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataBaixa, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbCertificatDiscapacitat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Gestio de ofertes"
		Me.ClientSize = New System.Drawing.Size(558, 521)
		Me.Location = New System.Drawing.Point(188, 160)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-OFERTES"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmGestioDeOfertes"
		TabControl1.OcxState = CType(resources.GetObject("TabControl1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControl1.Size = New System.Drawing.Size(537, 401)
		Me.TabControl1.Location = New System.Drawing.Point(10, 66)
		Me.TabControl1.TabIndex = 19
		Me.TabControl1.Name = "TabControl1"
		TabControlPage5.OcxState = CType(resources.GetObject("TabControlPage5.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage5.Size = New System.Drawing.Size(533, 377)
		Me.TabControlPage5.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage5.TabIndex = 42
		Me.TabControlPage5.Visible = False
		Me.TabControlPage5.Name = "TabControlPage5"
		Me.rtfNotes.Size = New System.Drawing.Size(521, 363)
		Me.rtfNotes.Location = New System.Drawing.Point(6, 8)
		Me.rtfNotes.TabIndex = 46
		Me.rtfNotes.Tag = "15"
		Me.rtfNotes.Enabled = True
		Me.rtfNotes.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
		Me.rtfNotes.RTF = resources.GetString("rtfNotes.TextRTF")
		Me.rtfNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.rtfNotes.Name = "rtfNotes"
		TabControlPage4.OcxState = CType(resources.GetObject("TabControlPage4.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage4.Size = New System.Drawing.Size(533, 377)
		Me.TabControlPage4.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage4.TabIndex = 23
		Me.TabControlPage4.Visible = False
		Me.TabControlPage4.Name = "TabControlPage4"
		GrdInsercions.OcxState = CType(resources.GetObject("GrdInsercions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdInsercions.Size = New System.Drawing.Size(520, 365)
		Me.GrdInsercions.Location = New System.Drawing.Point(6, 8)
		Me.GrdInsercions.TabIndex = 45
		Me.GrdInsercions.Name = "GrdInsercions"
		TabControlPage3.OcxState = CType(resources.GetObject("TabControlPage3.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage3.Size = New System.Drawing.Size(533, 377)
		Me.TabControlPage3.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage3.TabIndex = 22
		Me.TabControlPage3.Visible = False
		Me.TabControlPage3.Name = "TabControlPage3"
		GrdEntrevistat.OcxState = CType(resources.GetObject("GrdEntrevistat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdEntrevistat.Size = New System.Drawing.Size(520, 365)
		Me.GrdEntrevistat.Location = New System.Drawing.Point(6, 8)
		Me.GrdEntrevistat.TabIndex = 44
		Me.GrdEntrevistat.Name = "GrdEntrevistat"
		TabControlPage2.OcxState = CType(resources.GetObject("TabControlPage2.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage2.Size = New System.Drawing.Size(533, 377)
		Me.TabControlPage2.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage2.TabIndex = 21
		Me.TabControlPage2.Visible = False
		Me.TabControlPage2.Name = "TabControlPage2"
		GrdPresentats.OcxState = CType(resources.GetObject("GrdPresentats.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdPresentats.Size = New System.Drawing.Size(520, 365)
		Me.GrdPresentats.Location = New System.Drawing.Point(6, 8)
		Me.GrdPresentats.TabIndex = 43
		Me.GrdPresentats.Name = "GrdPresentats"
		TabControlPage1.OcxState = CType(resources.GetObject("TabControlPage1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage1.Size = New System.Drawing.Size(533, 377)
		Me.TabControlPage1.Location = New System.Drawing.Point(2, 22)
		Me.TabControlPage1.TabIndex = 20
		Me.TabControlPage1.Name = "TabControlPage1"
		Me.txtObservacions.AutoSize = False
		Me.txtObservacions.Size = New System.Drawing.Size(365, 77)
		Me.txtObservacions.Location = New System.Drawing.Point(134, 280)
		Me.txtObservacions.Maxlength = 200
		Me.txtObservacions.MultiLine = True
		Me.txtObservacions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtObservacions.TabIndex = 14
		Me.txtObservacions.Tag = "14"
		Me.txtObservacions.AcceptsReturn = True
		Me.txtObservacions.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacions.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacions.CausesValidation = True
		Me.txtObservacions.Enabled = True
		Me.txtObservacions.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacions.HideSelection = True
		Me.txtObservacions.ReadOnly = False
		Me.txtObservacions.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacions.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacions.TabStop = True
		Me.txtObservacions.Visible = True
		Me.txtObservacions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacions.Name = "txtObservacions"
		Me.txtGrauDiscapacitatSuperi.AutoSize = False
		Me.txtGrauDiscapacitatSuperi.Size = New System.Drawing.Size(31, 19)
		Me.txtGrauDiscapacitatSuperi.Location = New System.Drawing.Point(164, 256)
		Me.txtGrauDiscapacitatSuperi.Maxlength = 3
		Me.txtGrauDiscapacitatSuperi.TabIndex = 13
		Me.txtGrauDiscapacitatSuperi.Tag = "13"
		Me.txtGrauDiscapacitatSuperi.AcceptsReturn = True
		Me.txtGrauDiscapacitatSuperi.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGrauDiscapacitatSuperi.BackColor = System.Drawing.SystemColors.Window
		Me.txtGrauDiscapacitatSuperi.CausesValidation = True
		Me.txtGrauDiscapacitatSuperi.Enabled = True
		Me.txtGrauDiscapacitatSuperi.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGrauDiscapacitatSuperi.HideSelection = True
		Me.txtGrauDiscapacitatSuperi.ReadOnly = False
		Me.txtGrauDiscapacitatSuperi.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGrauDiscapacitatSuperi.MultiLine = False
		Me.txtGrauDiscapacitatSuperi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGrauDiscapacitatSuperi.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGrauDiscapacitatSuperi.TabStop = True
		Me.txtGrauDiscapacitatSuperi.Visible = True
		Me.txtGrauDiscapacitatSuperi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtGrauDiscapacitatSuperi.Name = "txtGrauDiscapacitatSuperi"
		Me.Text11.AutoSize = False
		Me.Text11.BackColor = System.Drawing.Color.White
		Me.Text11.Enabled = False
		Me.Text11.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text11.Size = New System.Drawing.Size(340, 19)
		Me.Text11.Location = New System.Drawing.Point(158, 208)
		Me.Text11.TabIndex = 38
		Me.Text11.Tag = "^11"
		Me.Text11.AcceptsReturn = True
		Me.Text11.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text11.CausesValidation = True
		Me.Text11.HideSelection = True
		Me.Text11.ReadOnly = False
		Me.Text11.Maxlength = 0
		Me.Text11.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text11.MultiLine = False
		Me.Text11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text11.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text11.TabStop = True
		Me.Text11.Visible = True
		Me.Text11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text11.Name = "Text11"
		Me.txtLlocTreball.AutoSize = False
		Me.txtLlocTreball.Size = New System.Drawing.Size(21, 19)
		Me.txtLlocTreball.Location = New System.Drawing.Point(134, 208)
		Me.txtLlocTreball.Maxlength = 2
		Me.txtLlocTreball.TabIndex = 11
		Me.txtLlocTreball.Tag = "11"
		Me.txtLlocTreball.AcceptsReturn = True
		Me.txtLlocTreball.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLlocTreball.BackColor = System.Drawing.SystemColors.Window
		Me.txtLlocTreball.CausesValidation = True
		Me.txtLlocTreball.Enabled = True
		Me.txtLlocTreball.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLlocTreball.HideSelection = True
		Me.txtLlocTreball.ReadOnly = False
		Me.txtLlocTreball.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLlocTreball.MultiLine = False
		Me.txtLlocTreball.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLlocTreball.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLlocTreball.TabStop = True
		Me.txtLlocTreball.Visible = True
		Me.txtLlocTreball.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtLlocTreball.Name = "txtLlocTreball"
		Me.txtNumLlocs.AutoSize = False
		Me.txtNumLlocs.Size = New System.Drawing.Size(21, 19)
		Me.txtNumLlocs.Location = New System.Drawing.Point(134, 184)
		Me.txtNumLlocs.Maxlength = 2
		Me.txtNumLlocs.TabIndex = 10
		Me.txtNumLlocs.Tag = "10"
		Me.txtNumLlocs.AcceptsReturn = True
		Me.txtNumLlocs.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNumLlocs.BackColor = System.Drawing.SystemColors.Window
		Me.txtNumLlocs.CausesValidation = True
		Me.txtNumLlocs.Enabled = True
		Me.txtNumLlocs.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNumLlocs.HideSelection = True
		Me.txtNumLlocs.ReadOnly = False
		Me.txtNumLlocs.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNumLlocs.MultiLine = False
		Me.txtNumLlocs.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNumLlocs.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNumLlocs.TabStop = True
		Me.txtNumLlocs.Visible = True
		Me.txtNumLlocs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNumLlocs.Name = "txtNumLlocs"
		Me.Text9.AutoSize = False
		Me.Text9.BackColor = System.Drawing.Color.White
		Me.Text9.Enabled = False
		Me.Text9.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text9.Size = New System.Drawing.Size(340, 19)
		Me.Text9.Location = New System.Drawing.Point(158, 160)
		Me.Text9.TabIndex = 35
		Me.Text9.Tag = "^9"
		Me.Text9.AcceptsReturn = True
		Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text9.CausesValidation = True
		Me.Text9.HideSelection = True
		Me.Text9.ReadOnly = False
		Me.Text9.Maxlength = 0
		Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text9.MultiLine = False
		Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text9.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text9.TabStop = True
		Me.Text9.Visible = True
		Me.Text9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text9.Name = "Text9"
		Me.txtMotiuBaixa.AutoSize = False
		Me.txtMotiuBaixa.Size = New System.Drawing.Size(21, 19)
		Me.txtMotiuBaixa.Location = New System.Drawing.Point(134, 160)
		Me.txtMotiuBaixa.Maxlength = 2
		Me.txtMotiuBaixa.TabIndex = 9
		Me.txtMotiuBaixa.Tag = "9"
		Me.txtMotiuBaixa.AcceptsReturn = True
		Me.txtMotiuBaixa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMotiuBaixa.BackColor = System.Drawing.SystemColors.Window
		Me.txtMotiuBaixa.CausesValidation = True
		Me.txtMotiuBaixa.Enabled = True
		Me.txtMotiuBaixa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMotiuBaixa.HideSelection = True
		Me.txtMotiuBaixa.ReadOnly = False
		Me.txtMotiuBaixa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMotiuBaixa.MultiLine = False
		Me.txtMotiuBaixa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMotiuBaixa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMotiuBaixa.TabStop = True
		Me.txtMotiuBaixa.Visible = True
		Me.txtMotiuBaixa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMotiuBaixa.Name = "txtMotiuBaixa"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(256, 19)
		Me.Text6.Location = New System.Drawing.Point(242, 88)
		Me.Text6.TabIndex = 31
		Me.Text6.Tag = "^6"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtTecnic.AutoSize = False
		Me.txtTecnic.Size = New System.Drawing.Size(105, 19)
		Me.txtTecnic.Location = New System.Drawing.Point(134, 88)
		Me.txtTecnic.Maxlength = 15
		Me.txtTecnic.TabIndex = 6
		Me.txtTecnic.Tag = "6"
		Me.txtTecnic.AcceptsReturn = True
		Me.txtTecnic.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTecnic.BackColor = System.Drawing.SystemColors.Window
		Me.txtTecnic.CausesValidation = True
		Me.txtTecnic.Enabled = True
		Me.txtTecnic.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTecnic.HideSelection = True
		Me.txtTecnic.ReadOnly = False
		Me.txtTecnic.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTecnic.MultiLine = False
		Me.txtTecnic.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTecnic.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTecnic.TabStop = True
		Me.txtTecnic.Visible = True
		Me.txtTecnic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTecnic.Name = "txtTecnic"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(340, 19)
		Me.Text5.Location = New System.Drawing.Point(158, 64)
		Me.Text5.TabIndex = 29
		Me.Text5.Tag = "^5"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtServeiGestor.AutoSize = False
		Me.txtServeiGestor.Size = New System.Drawing.Size(21, 19)
		Me.txtServeiGestor.Location = New System.Drawing.Point(134, 64)
		Me.txtServeiGestor.Maxlength = 2
		Me.txtServeiGestor.TabIndex = 5
		Me.txtServeiGestor.Tag = "5"
		Me.txtServeiGestor.AcceptsReturn = True
		Me.txtServeiGestor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtServeiGestor.BackColor = System.Drawing.SystemColors.Window
		Me.txtServeiGestor.CausesValidation = True
		Me.txtServeiGestor.Enabled = True
		Me.txtServeiGestor.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtServeiGestor.HideSelection = True
		Me.txtServeiGestor.ReadOnly = False
		Me.txtServeiGestor.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtServeiGestor.MultiLine = False
		Me.txtServeiGestor.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtServeiGestor.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtServeiGestor.TabStop = True
		Me.txtServeiGestor.Visible = True
		Me.txtServeiGestor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtServeiGestor.Name = "txtServeiGestor"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(320, 19)
		Me.Text4.Location = New System.Drawing.Point(179, 40)
		Me.Text4.TabIndex = 27
		Me.Text4.Tag = "^4"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtSucursal.AutoSize = False
		Me.txtSucursal.Size = New System.Drawing.Size(42, 19)
		Me.txtSucursal.Location = New System.Drawing.Point(134, 40)
		Me.txtSucursal.Maxlength = 4
		Me.txtSucursal.TabIndex = 4
		Me.txtSucursal.Tag = "4"
		Me.txtSucursal.AcceptsReturn = True
		Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSucursal.BackColor = System.Drawing.SystemColors.Window
		Me.txtSucursal.CausesValidation = True
		Me.txtSucursal.Enabled = True
		Me.txtSucursal.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSucursal.HideSelection = True
		Me.txtSucursal.ReadOnly = False
		Me.txtSucursal.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSucursal.MultiLine = False
		Me.txtSucursal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSucursal.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSucursal.TabStop = True
		Me.txtSucursal.Visible = True
		Me.txtSucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSucursal.Name = "txtSucursal"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(280, 19)
		Me.Text3.Location = New System.Drawing.Point(220, 16)
		Me.Text3.TabIndex = 25
		Me.Text3.Tag = "^3"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtEntitat.AutoSize = False
		Me.txtEntitat.Size = New System.Drawing.Size(83, 19)
		Me.txtEntitat.Location = New System.Drawing.Point(134, 16)
		Me.txtEntitat.Maxlength = 8
		Me.txtEntitat.TabIndex = 3
		Me.txtEntitat.Tag = "3"
		Me.txtEntitat.AcceptsReturn = True
		Me.txtEntitat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEntitat.BackColor = System.Drawing.SystemColors.Window
		Me.txtEntitat.CausesValidation = True
		Me.txtEntitat.Enabled = True
		Me.txtEntitat.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEntitat.HideSelection = True
		Me.txtEntitat.ReadOnly = False
		Me.txtEntitat.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEntitat.MultiLine = False
		Me.txtEntitat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEntitat.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEntitat.TabStop = True
		Me.txtEntitat.Visible = True
		Me.txtEntitat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEntitat.Name = "txtEntitat"
		txtDataAlta.OcxState = CType(resources.GetObject("txtDataAlta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataAlta.Size = New System.Drawing.Size(87, 19)
		Me.txtDataAlta.Location = New System.Drawing.Point(134, 112)
		Me.txtDataAlta.TabIndex = 7
		Me.txtDataAlta.Name = "txtDataAlta"
		txtDataBaixa.OcxState = CType(resources.GetObject("txtDataBaixa.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataBaixa.Size = New System.Drawing.Size(87, 19)
		Me.txtDataBaixa.Location = New System.Drawing.Point(134, 136)
		Me.txtDataBaixa.TabIndex = 8
		Me.txtDataBaixa.Name = "txtDataBaixa"
		cmbCertificatDiscapacitat.OcxState = CType(resources.GetObject("cmbCertificatDiscapacitat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbCertificatDiscapacitat.Size = New System.Drawing.Size(145, 19)
		Me.cmbCertificatDiscapacitat.Location = New System.Drawing.Point(134, 232)
		Me.cmbCertificatDiscapacitat.TabIndex = 12
		Me.cmbCertificatDiscapacitat.Name = "cmbCertificatDiscapacitat"
		Me.lbl14.Text = "Observacions"
		Me.lbl14.Size = New System.Drawing.Size(111, 15)
		Me.lbl14.Location = New System.Drawing.Point(20, 284)
		Me.lbl14.TabIndex = 41
		Me.lbl14.Tag = "14"
		Me.lbl14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl14.BackColor = System.Drawing.SystemColors.Control
		Me.lbl14.Enabled = True
		Me.lbl14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl14.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl14.UseMnemonic = True
		Me.lbl14.Visible = True
		Me.lbl14.AutoSize = False
		Me.lbl14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl14.Name = "lbl14"
		Me.lbl13.Text = "Grau discapacitat superior a"
		Me.lbl13.Size = New System.Drawing.Size(143, 15)
		Me.lbl13.Location = New System.Drawing.Point(20, 260)
		Me.lbl13.TabIndex = 40
		Me.lbl13.Tag = "13"
		Me.lbl13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl13.BackColor = System.Drawing.SystemColors.Control
		Me.lbl13.Enabled = True
		Me.lbl13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl13.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl13.UseMnemonic = True
		Me.lbl13.Visible = True
		Me.lbl13.AutoSize = False
		Me.lbl13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl13.Name = "lbl13"
		Me.lbl12.Text = "Certificat discapacitat"
		Me.lbl12.Size = New System.Drawing.Size(111, 15)
		Me.lbl12.Location = New System.Drawing.Point(20, 236)
		Me.lbl12.TabIndex = 39
		Me.lbl12.Tag = "12"
		Me.lbl12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl12.BackColor = System.Drawing.SystemColors.Control
		Me.lbl12.Enabled = True
		Me.lbl12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl12.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl12.UseMnemonic = True
		Me.lbl12.Visible = True
		Me.lbl12.AutoSize = False
		Me.lbl12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl12.Name = "lbl12"
		Me.lbl11.Text = "Lloc treball"
		Me.lbl11.Size = New System.Drawing.Size(111, 15)
		Me.lbl11.Location = New System.Drawing.Point(20, 212)
		Me.lbl11.TabIndex = 37
		Me.lbl11.Tag = "11"
		Me.lbl11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl11.BackColor = System.Drawing.SystemColors.Control
		Me.lbl11.Enabled = True
		Me.lbl11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl11.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl11.UseMnemonic = True
		Me.lbl11.Visible = True
		Me.lbl11.AutoSize = False
		Me.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl11.Name = "lbl11"
		Me.lbl10.Text = "Num. llocs"
		Me.lbl10.Size = New System.Drawing.Size(111, 15)
		Me.lbl10.Location = New System.Drawing.Point(20, 188)
		Me.lbl10.TabIndex = 36
		Me.lbl10.Tag = "10"
		Me.lbl10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl10.BackColor = System.Drawing.SystemColors.Control
		Me.lbl10.Enabled = True
		Me.lbl10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl10.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl10.UseMnemonic = True
		Me.lbl10.Visible = True
		Me.lbl10.AutoSize = False
		Me.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl10.Name = "lbl10"
		Me.lbl9.Text = "Motiu baixa"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(20, 164)
		Me.lbl9.TabIndex = 34
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl8.Text = "Data baixa"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(20, 140)
		Me.lbl8.TabIndex = 33
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl7.Text = "Data alta"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(20, 116)
		Me.lbl7.TabIndex = 32
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl6.Text = "Tecnic"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(20, 92)
		Me.lbl6.TabIndex = 30
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl5.Text = "Servei gestor"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(20, 68)
		Me.lbl5.TabIndex = 28
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl4.Text = "Sucursal"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(20, 44)
		Me.lbl4.TabIndex = 26
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl3.Text = "Entitat"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(20, 20)
		Me.lbl3.TabIndex = 24
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.txtCodi.AutoSize = False
		Me.txtCodi.Size = New System.Drawing.Size(31, 19)
		Me.txtCodi.Location = New System.Drawing.Point(124, 10)
		Me.txtCodi.Maxlength = 3
		Me.txtCodi.TabIndex = 1
		Me.txtCodi.Tag = "*1"
		Me.txtCodi.AcceptsReturn = True
		Me.txtCodi.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodi.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodi.CausesValidation = True
		Me.txtCodi.Enabled = True
		Me.txtCodi.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodi.HideSelection = True
		Me.txtCodi.ReadOnly = False
		Me.txtCodi.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodi.MultiLine = False
		Me.txtCodi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodi.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodi.TabStop = True
		Me.txtCodi.Visible = True
		Me.txtCodi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodi.Name = "txtCodi"
		Me.txtDescripcio.AutoSize = False
		Me.txtDescripcio.Size = New System.Drawing.Size(415, 19)
		Me.txtDescripcio.Location = New System.Drawing.Point(124, 34)
		Me.txtDescripcio.Maxlength = 100
		Me.txtDescripcio.TabIndex = 2
		Me.txtDescripcio.Tag = "2"
		Me.txtDescripcio.AcceptsReturn = True
		Me.txtDescripcio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcio.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcio.CausesValidation = True
		Me.txtDescripcio.Enabled = True
		Me.txtDescripcio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcio.HideSelection = True
		Me.txtDescripcio.ReadOnly = False
		Me.txtDescripcio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcio.MultiLine = False
		Me.txtDescripcio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcio.TabStop = True
		Me.txtDescripcio.Visible = True
		Me.txtDescripcio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcio.Name = "txtDescripcio"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(375, 490)
		Me.cmdAceptar.TabIndex = 15
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(465, 490)
		Me.cmdGuardar.TabIndex = 17
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl1.Text = "Codi"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Descripci�"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 16
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 489)
		Me.lblLock.TabIndex = 18
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 483
		Me.Line1.Y2 = 483
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 484
		Me.Line2.Y2 = 484
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbCertificatDiscapacitat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataBaixa, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataAlta, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdPresentats, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdEntrevistat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage4, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdInsercions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage5, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(TabControl1)
		Me.Controls.Add(txtCodi)
		Me.Controls.Add(txtDescripcio)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.TabControl1.Controls.Add(TabControlPage5)
		Me.TabControl1.Controls.Add(TabControlPage4)
		Me.TabControl1.Controls.Add(TabControlPage3)
		Me.TabControl1.Controls.Add(TabControlPage2)
		Me.TabControl1.Controls.Add(TabControlPage1)
		Me.TabControlPage5.Controls.Add(rtfNotes)
		Me.TabControlPage4.Controls.Add(GrdInsercions)
		Me.TabControlPage3.Controls.Add(GrdEntrevistat)
		Me.TabControlPage2.Controls.Add(GrdPresentats)
		Me.TabControlPage1.Controls.Add(txtObservacions)
		Me.TabControlPage1.Controls.Add(txtGrauDiscapacitatSuperi)
		Me.TabControlPage1.Controls.Add(Text11)
		Me.TabControlPage1.Controls.Add(txtLlocTreball)
		Me.TabControlPage1.Controls.Add(txtNumLlocs)
		Me.TabControlPage1.Controls.Add(Text9)
		Me.TabControlPage1.Controls.Add(txtMotiuBaixa)
		Me.TabControlPage1.Controls.Add(Text6)
		Me.TabControlPage1.Controls.Add(txtTecnic)
		Me.TabControlPage1.Controls.Add(Text5)
		Me.TabControlPage1.Controls.Add(txtServeiGestor)
		Me.TabControlPage1.Controls.Add(Text4)
		Me.TabControlPage1.Controls.Add(txtSucursal)
		Me.TabControlPage1.Controls.Add(Text3)
		Me.TabControlPage1.Controls.Add(txtEntitat)
		Me.TabControlPage1.Controls.Add(txtDataAlta)
		Me.TabControlPage1.Controls.Add(txtDataBaixa)
		Me.TabControlPage1.Controls.Add(cmbCertificatDiscapacitat)
		Me.TabControlPage1.Controls.Add(lbl14)
		Me.TabControlPage1.Controls.Add(lbl13)
		Me.TabControlPage1.Controls.Add(lbl12)
		Me.TabControlPage1.Controls.Add(lbl11)
		Me.TabControlPage1.Controls.Add(lbl10)
		Me.TabControlPage1.Controls.Add(lbl9)
		Me.TabControlPage1.Controls.Add(lbl8)
		Me.TabControlPage1.Controls.Add(lbl7)
		Me.TabControlPage1.Controls.Add(lbl6)
		Me.TabControlPage1.Controls.Add(lbl5)
		Me.TabControlPage1.Controls.Add(lbl4)
		Me.TabControlPage1.Controls.Add(lbl3)
		Me.TabControl1.ResumeLayout(False)
		Me.TabControlPage5.ResumeLayout(False)
		Me.TabControlPage4.ResumeLayout(False)
		Me.TabControlPage3.ResumeLayout(False)
		Me.TabControlPage2.ResumeLayout(False)
		Me.TabControlPage1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class