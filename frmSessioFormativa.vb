Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmSessioFormativa
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	
	
	Private Sub cmdParticipants_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdParticipants.ClickEvent
		Dim I As Short
		Dim Nod As String
		
		If txtCodi.Text = "" Then Exit Sub
		
		'###################### GUARDEM ELS T�CNICS #####################
		Nod = ""
		For I = 1 To GrdTecnics.Rows - 1
			If GrdTecnics.Cell(I, 3).Text = "1" Then
				If Nod <> "" Then
					Nod = Nod & Chr(13)
				End If
				Nod = Nod & GrdTecnics.Cell(I, 1).Text & S & GrdTecnics.Cell(I, 3).Text
			End If
		Next I
		
		CacheNetejaParametres()
		MCache.P1 = txtCodi.Text
		MCache.PDELIM = Chr(13)
		MCache.PLIST = Nod
		CacheXecute("D GTECNICS^INSERCIO")
		CacheNetejaParametres()
		
		With frmParticipants
			.Sessio = txtCodi.Text
			.DataS = txtData.Text
			.TAcc = CShort(txtTipoAccion.Text)
			.TSAcc = CShort(txtTipusSubaccio.Text)
			.Tec = txtTecnic.Text
			.Serv = CShort(txtServei.Text)
			.Temps = txtTemps.Value
			.Obs = txtObservacions.Text
			.Show()
		End With
	End Sub
	
	'UPGRADE_WARNING: Form evento frmSessioFormativa.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmSessioFormativa_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		
		CarregaTecnics()
		CarregaParticipants()
		
	End Sub
	
	Private Sub frmSessioFormativa_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmSessioFormativa_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		cmdParticipants.Picture = SetIcon("Users", 16)
	End Sub
	
	Private Sub frmSessioFormativa_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmSessioFormativa_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	
	
	
	Private Sub TabControl1_Selected(sender As Object, ByVal eventArgs As TabControlEventArgs) Handles TabControl1.Selected
		If ABM = "AL" Then cmdGuardar_ClickEvent(cmdGuardar, New System.EventArgs())
		CarregaTecnics()
		CarregaParticipants()
	End Sub
	
	Private Sub txtCodi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.GotFocus
		XGotFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.DoubleClick
		ConsultaTaula(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.LostFocus
		If txtCodi.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtCodi)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.GotFocus
		XGotFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.LostFocus
		XLostFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtDescripcio)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtData_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtData.GotFocus
		XGotFocus(Me, txtData)
	End Sub
	
	Private Sub txtData_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtData.LostFocus
		XLostFocus(Me, txtData)
	End Sub
	
	Private Sub txtData_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtData.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtData)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtServei_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.GotFocus
		XGotFocus(Me, txtServei)
	End Sub
	
	Private Sub txtServei_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.DoubleClick
		ConsultaTaula(Me, txtServei)
	End Sub
	
	Private Sub txtServei_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.LostFocus
		XLostFocus(Me, txtServei)
	End Sub
	
	Private Sub txtServei_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtServei.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtServei)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtServei)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTecnic_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnic.GotFocus
		XGotFocus(Me, txtTecnic)
	End Sub
	
	Private Sub txtTecnic_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnic.DoubleClick
		ConsultaTaula(Me, txtTecnic)
	End Sub
	
	Private Sub txtTecnic_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnic.LostFocus
		XLostFocus(Me, txtTecnic)
	End Sub
	
	Private Sub txtTecnic_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTecnic.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTecnic)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtTecnic)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipoAccion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoAccion.GotFocus
		XGotFocus(Me, txtTipoAccion)
	End Sub
	
	Private Sub txtTipoAccion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoAccion.DoubleClick
		ConsultaTaula(Me, txtTipoAccion)
	End Sub
	
	Private Sub txtTipoAccion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoAccion.LostFocus
		XLostFocus(Me, txtTipoAccion)
	End Sub
	
	Private Sub txtTipoAccion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipoAccion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTipoAccion)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtTipoAccion)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipusSubaccio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusSubaccio.GotFocus
		XGotFocus(Me, txtTipusSubaccio)
	End Sub
	
	Private Sub txtTipusSubaccio_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusSubaccio.DoubleClick
		ConsultaTaula(Me, txtTipusSubaccio,  , txtTipoAccion.Text)
	End Sub
	
	Private Sub txtTipusSubaccio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusSubaccio.LostFocus
		XLostFocus(Me, txtTipusSubaccio)
	End Sub
	
	Private Sub txtTipusSubaccio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipusSubaccio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTipusSubaccio, txtTipoAccion.Text)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtTipusSubaccio)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTemps_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTemps.GotFocus
		XGotFocus(Me, txtTemps)
	End Sub
	
	Private Sub txtTemps_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTemps.LostFocus
		XLostFocus(Me, txtTemps)
	End Sub
	
	Private Sub txtTemps_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTemps.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtTemps)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservacions_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.GotFocus
		XGotFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.LostFocus
		XLostFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservacions.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtObservacions)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub CarregaTecnics()
		CarregaFGrid(GrdTecnics, "CTECNICS^INSERCIO", txtCodi.Text)
	End Sub
	
	Public Sub CarregaParticipants()
		CarregaFGrid(GrdParticipants, "CPARTICIPANTS^INSERCIO", txtCodi.Text)
	End Sub
End Class
