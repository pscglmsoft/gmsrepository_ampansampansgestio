<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmUsuariInsercioLaboral
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtNombreCompleto As System.Windows.Forms.TextBox
	Public WithEvents txtPerfilLaboralN As System.Windows.Forms.TextBox
	Public WithEvents txtDocMotivacionLaboral As System.Windows.Forms.TextBox
	Public WithEvents txtGradoOcupabilidad As System.Windows.Forms.TextBox
	Public WithEvents txtValoracionTecnica As System.Windows.Forms.TextBox
	Public WithEvents txtGradoOcupabilidadDoc As System.Windows.Forms.TextBox
	Public WithEvents txtGencat As System.Windows.Forms.TextBox
	Public WithEvents txtICAP As System.Windows.Forms.TextBox
	Public WithEvents txtPerfilLaboral As System.Windows.Forms.TextBox
	Public WithEvents GrdSeguiment As AxFlexCell.AxGrid
	Public WithEvents TabControlPage10 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdRelacions As AxFlexCell.AxGrid
	Public WithEvents TabControlPage9 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdServeis As AxFlexCell.AxGrid
	Public WithEvents TabControlPage8 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdPrepLab As AxFlexCell.AxGrid
	Public WithEvents TabControlPage7 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents txtPai As System.Windows.Forms.TextBox
	Public WithEvents TxtFunciona As System.Windows.Forms.TextBox
	Public WithEvents cmdPai As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdFunciona As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents TabControlPage6 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents txtObservacionesPreferFor As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtTerceraPreferenciaFor As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtSegonaPreferenciaFor As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents txtPrimeraPreferenciaFor As System.Windows.Forms.TextBox
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents Label16 As System.Windows.Forms.Label
	Public WithEvents TabControlPage5 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents txtDisponibilidadHorariaP As System.Windows.Forms.TextBox
	Public WithEvents txtObservacionesOtros As System.Windows.Forms.TextBox
	Public WithEvents txtDisponibilidadGeograf As System.Windows.Forms.TextBox
	Public WithEvents txtIncorporacion As System.Windows.Forms.TextBox
	Public WithEvents txtSueldoAproximado As System.Windows.Forms.TextBox
	Public WithEvents txtCaracteristicasTrabajo As System.Windows.Forms.TextBox
	Public WithEvents txtObservacionesPref As System.Windows.Forms.TextBox
	Public WithEvents Text53 As System.Windows.Forms.TextBox
	Public WithEvents txtTerceraPreferenciaTrab As System.Windows.Forms.TextBox
	Public WithEvents Text52 As System.Windows.Forms.TextBox
	Public WithEvents txtSegundaPreferenciaTrab As System.Windows.Forms.TextBox
	Public WithEvents Text51 As System.Windows.Forms.TextBox
	Public WithEvents txtPrimeraPreferenciaTrab As System.Windows.Forms.TextBox
	Public WithEvents cmbPreferenciaEmpresa As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents cmbPreferenciaJornada As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents cmbNivelHabilidades As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents chkDispuestoHacerPractica As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents lbl62 As System.Windows.Forms.Label
	Public WithEvents lbl61 As System.Windows.Forms.Label
	Public WithEvents lbl60 As System.Windows.Forms.Label
	Public WithEvents lbl59 As System.Windows.Forms.Label
	Public WithEvents lbl58 As System.Windows.Forms.Label
	Public WithEvents lbl57 As System.Windows.Forms.Label
	Public WithEvents lbl56 As System.Windows.Forms.Label
	Public WithEvents lbl55 As System.Windows.Forms.Label
	Public WithEvents lbl54 As System.Windows.Forms.Label
	Public WithEvents lbl53 As System.Windows.Forms.Label
	Public WithEvents lbl52 As System.Windows.Forms.Label
	Public WithEvents lbl51 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents TabControlPage4 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents txtCurriculumVitae As System.Windows.Forms.TextBox
	Public WithEvents txtOtrosProgramas As System.Windows.Forms.TextBox
	Public WithEvents txtOtros As System.Windows.Forms.TextBox
	Public WithEvents txtFormacionCom As System.Windows.Forms.TextBox
	Public WithEvents txtEspecialidad As System.Windows.Forms.TextBox
	Public WithEvents Text35 As System.Windows.Forms.TextBox
	Public WithEvents txtFormacionReglada As System.Windows.Forms.TextBox
	Public WithEvents chkCatalan As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkCastellano As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkIngles As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkFrances As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkWord As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkAcces As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkExcel As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkInternet As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkPowerpoint As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkTieneExperienciaLab As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents grdHistLaboral As AxFlexCell.AxGrid
	Public WithEvents cmbCurri As AxXtremeSuiteControls.AxPushButton
	Public WithEvents GrdFormacio As AxFlexCell.AxGrid
	Public WithEvents Label17 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents lbl50 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents lbl48 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents lbl42 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl37 As System.Windows.Forms.Label
	Public WithEvents lbl36 As System.Windows.Forms.Label
	Public WithEvents lbl35 As System.Windows.Forms.Label
	Public WithEvents TabControlPage3 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents txtCondicionantesEntorno As System.Windows.Forms.TextBox
	Public WithEvents txtLimitaciones As System.Windows.Forms.TextBox
	Public WithEvents txtTipoPrestacion As System.Windows.Forms.TextBox
	Public WithEvents chkPrestacionEconomica As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkInscripcionOtg As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents txtFechaOtg As AxDataControl.AxGmsData
	Public WithEvents cmbTransporte As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents cmbValoracionSoc As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents lbl32 As System.Windows.Forms.Label
	Public WithEvents lbl31 As System.Windows.Forms.Label
	Public WithEvents lbl30 As System.Windows.Forms.Label
	Public WithEvents lbl29 As System.Windows.Forms.Label
	Public WithEvents lbl28 As System.Windows.Forms.Label
	Public WithEvents lbl26 As System.Windows.Forms.Label
	Public WithEvents TabControlPage2 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents Text8 As System.Windows.Forms.TextBox
	Public WithEvents txtIdoneidadProgramaN As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents txtPuntAtencio As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtTecnico As System.Windows.Forms.TextBox
	Public WithEvents txtValoracioPossibilitat As System.Windows.Forms.TextBox
	Public WithEvents txtServicioInicio As System.Windows.Forms.TextBox
	Public WithEvents Text16 As System.Windows.Forms.TextBox
	Public WithEvents txtDemandaInicial As System.Windows.Forms.TextBox
	Public WithEvents txtObservaciones As System.Windows.Forms.TextBox
	Public WithEvents txtIdoneidadPrograma As System.Windows.Forms.TextBox
	Public WithEvents txtFechaInicio As AxDataControl.AxGmsData
	Public WithEvents Label21 As System.Windows.Forms.Label
	Public WithEvents Label19 As System.Windows.Forms.Label
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents lbl15 As System.Windows.Forms.Label
	Public WithEvents lbl16 As System.Windows.Forms.Label
	Public WithEvents lbl19 As System.Windows.Forms.Label
	Public WithEvents lbl20 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents XPFrame302 As AxciaXPFrame30.AxXPFrame30
	Public WithEvents OptRecollida As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents cmdImprimir As AxXtremeSuiteControls.AxPushButton
	Public WithEvents OptProfessional As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents XPFrame301 As AxciaXPFrame30.AxXPFrame30
	Public WithEvents txtPoblacionNacimiento As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtDiagnostico As System.Windows.Forms.TextBox
	Public WithEvents txtGradoDiscapacidad As System.Windows.Forms.TextBox
	Public WithEvents Text21 As System.Windows.Forms.TextBox
	Public WithEvents txtColectivo As System.Windows.Forms.TextBox
	Public WithEvents Text18 As System.Windows.Forms.TextBox
	Public WithEvents txtMotivoBaja As System.Windows.Forms.TextBox
	Public WithEvents txtEmail As System.Windows.Forms.TextBox
	Public WithEvents txtCodigoPostal As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtPoblacion As System.Windows.Forms.TextBox
	Public WithEvents txtTelefonoMovil As System.Windows.Forms.TextBox
	Public WithEvents txtTelefono As System.Windows.Forms.TextBox
	Public WithEvents txtNIF As System.Windows.Forms.TextBox
	Public WithEvents txtDireccion As System.Windows.Forms.TextBox
	Public WithEvents txtSegundoApellido As System.Windows.Forms.TextBox
	Public WithEvents txtPrimerApellido As System.Windows.Forms.TextBox
	Public WithEvents txtNombre As System.Windows.Forms.TextBox
	Public WithEvents txtFechaNacimiento As AxDataControl.AxGmsData
	Public WithEvents cmbSexo As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents txtFechaFin As AxDataControl.AxGmsData
	Public WithEvents chkTieneCertificadoDiscap As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents cmdDocumentacio As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label18 As System.Windows.Forms.Label
	Public WithEvents lbl24 As System.Windows.Forms.Label
	Public WithEvents lbl23 As System.Windows.Forms.Label
	Public WithEvents lbl21 As System.Windows.Forms.Label
	Public WithEvents lbl18 As System.Windows.Forms.Label
	Public WithEvents lbl17 As System.Windows.Forms.Label
	Public WithEvents lbl14 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl13 As System.Windows.Forms.Label
	Public WithEvents lbl12 As System.Windows.Forms.Label
	Public WithEvents lbl10 As System.Windows.Forms.Label
	Public WithEvents lbl11 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents TabControlPage1 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents cmdGrauOcu As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGencat As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdIcap As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdPerfilLab As AxXtremeSuiteControls.AxPushButton
	Public WithEvents chkGradoOcupabilidad As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkGencat As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkIcap As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkPerfilLaboral As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkMotivacionLaboral As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents cmdMotLaboral As AxXtremeSuiteControls.AxPushButton
	Public WithEvents GrdProgrames As AxFlexCell.AxGrid
	Public WithEvents GrdInsercions As AxFlexCell.AxGrid
	Public WithEvents Label20 As System.Windows.Forms.Label
	Public WithEvents lbl33 As System.Windows.Forms.Label
	Public WithEvents lbl34 As System.Windows.Forms.Label
	Public WithEvents TabControl1 As AxXtremeSuiteControls.AxTabControl
	Public WithEvents txtCodigoEntidad As System.Windows.Forms.TextBox
	Public WithEvents txtCodigo As System.Windows.Forms.TextBox
	Public WithEvents cmbEstado As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents chkDisponibleParaOferta As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents cmdEntidad As AxXtremeSuiteControls.AxPushButton
	Public WithEvents chkActualmenteTrabajando As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents GrdServeisAct As AxFlexCell.AxGrid
	Public WithEvents chkSusceptibleServeiAssis As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkGarantiaJuvenil As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents txtGarantiaJ As AxDataControl.AxGmsData
	Public WithEvents lblTreballant As System.Windows.Forms.Label
	Public WithEvents Label33 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmUsuariInsercioLaboral))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtNombreCompleto = New System.Windows.Forms.TextBox
		Me.TabControl1 = New AxXtremeSuiteControls.AxTabControl
		Me.txtPerfilLaboralN = New System.Windows.Forms.TextBox
		Me.txtDocMotivacionLaboral = New System.Windows.Forms.TextBox
		Me.txtGradoOcupabilidad = New System.Windows.Forms.TextBox
		Me.txtValoracionTecnica = New System.Windows.Forms.TextBox
		Me.txtGradoOcupabilidadDoc = New System.Windows.Forms.TextBox
		Me.txtGencat = New System.Windows.Forms.TextBox
		Me.txtICAP = New System.Windows.Forms.TextBox
		Me.txtPerfilLaboral = New System.Windows.Forms.TextBox
		Me.TabControlPage10 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdSeguiment = New AxFlexCell.AxGrid
		Me.TabControlPage9 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdRelacions = New AxFlexCell.AxGrid
		Me.TabControlPage8 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdServeis = New AxFlexCell.AxGrid
		Me.TabControlPage7 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdPrepLab = New AxFlexCell.AxGrid
		Me.TabControlPage6 = New AxXtremeSuiteControls.AxTabControlPage
		Me.txtPai = New System.Windows.Forms.TextBox
		Me.TxtFunciona = New System.Windows.Forms.TextBox
		Me.cmdPai = New AxXtremeSuiteControls.AxPushButton
		Me.cmdFunciona = New AxXtremeSuiteControls.AxPushButton
		Me.Label7 = New System.Windows.Forms.Label
		Me.Label12 = New System.Windows.Forms.Label
		Me.TabControlPage5 = New AxXtremeSuiteControls.AxTabControlPage
		Me.txtObservacionesPreferFor = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtTerceraPreferenciaFor = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtSegonaPreferenciaFor = New System.Windows.Forms.TextBox
		Me.Text7 = New System.Windows.Forms.TextBox
		Me.txtPrimeraPreferenciaFor = New System.Windows.Forms.TextBox
		Me.Label13 = New System.Windows.Forms.Label
		Me.Label14 = New System.Windows.Forms.Label
		Me.Label15 = New System.Windows.Forms.Label
		Me.Label16 = New System.Windows.Forms.Label
		Me.TabControlPage4 = New AxXtremeSuiteControls.AxTabControlPage
		Me.txtDisponibilidadHorariaP = New System.Windows.Forms.TextBox
		Me.txtObservacionesOtros = New System.Windows.Forms.TextBox
		Me.txtDisponibilidadGeograf = New System.Windows.Forms.TextBox
		Me.txtIncorporacion = New System.Windows.Forms.TextBox
		Me.txtSueldoAproximado = New System.Windows.Forms.TextBox
		Me.txtCaracteristicasTrabajo = New System.Windows.Forms.TextBox
		Me.txtObservacionesPref = New System.Windows.Forms.TextBox
		Me.Text53 = New System.Windows.Forms.TextBox
		Me.txtTerceraPreferenciaTrab = New System.Windows.Forms.TextBox
		Me.Text52 = New System.Windows.Forms.TextBox
		Me.txtSegundaPreferenciaTrab = New System.Windows.Forms.TextBox
		Me.Text51 = New System.Windows.Forms.TextBox
		Me.txtPrimeraPreferenciaTrab = New System.Windows.Forms.TextBox
		Me.cmbPreferenciaEmpresa = New AxSSDataWidgets_B.AxSSDBCombo
		Me.cmbPreferenciaJornada = New AxSSDataWidgets_B.AxSSDBCombo
		Me.cmbNivelHabilidades = New AxSSDataWidgets_B.AxSSDBCombo
		Me.chkDispuestoHacerPractica = New AxXtremeSuiteControls.AxCheckBox
		Me.Label8 = New System.Windows.Forms.Label
		Me.lbl62 = New System.Windows.Forms.Label
		Me.lbl61 = New System.Windows.Forms.Label
		Me.lbl60 = New System.Windows.Forms.Label
		Me.lbl59 = New System.Windows.Forms.Label
		Me.lbl58 = New System.Windows.Forms.Label
		Me.lbl57 = New System.Windows.Forms.Label
		Me.lbl56 = New System.Windows.Forms.Label
		Me.lbl55 = New System.Windows.Forms.Label
		Me.lbl54 = New System.Windows.Forms.Label
		Me.lbl53 = New System.Windows.Forms.Label
		Me.lbl52 = New System.Windows.Forms.Label
		Me.lbl51 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.TabControlPage3 = New AxXtremeSuiteControls.AxTabControlPage
		Me.txtCurriculumVitae = New System.Windows.Forms.TextBox
		Me.txtOtrosProgramas = New System.Windows.Forms.TextBox
		Me.txtOtros = New System.Windows.Forms.TextBox
		Me.txtFormacionCom = New System.Windows.Forms.TextBox
		Me.txtEspecialidad = New System.Windows.Forms.TextBox
		Me.Text35 = New System.Windows.Forms.TextBox
		Me.txtFormacionReglada = New System.Windows.Forms.TextBox
		Me.chkCatalan = New AxXtremeSuiteControls.AxCheckBox
		Me.chkCastellano = New AxXtremeSuiteControls.AxCheckBox
		Me.chkIngles = New AxXtremeSuiteControls.AxCheckBox
		Me.chkFrances = New AxXtremeSuiteControls.AxCheckBox
		Me.chkWord = New AxXtremeSuiteControls.AxCheckBox
		Me.chkAcces = New AxXtremeSuiteControls.AxCheckBox
		Me.chkExcel = New AxXtremeSuiteControls.AxCheckBox
		Me.chkInternet = New AxXtremeSuiteControls.AxCheckBox
		Me.chkPowerpoint = New AxXtremeSuiteControls.AxCheckBox
		Me.chkTieneExperienciaLab = New AxXtremeSuiteControls.AxCheckBox
		Me.grdHistLaboral = New AxFlexCell.AxGrid
		Me.cmbCurri = New AxXtremeSuiteControls.AxPushButton
		Me.GrdFormacio = New AxFlexCell.AxGrid
		Me.Label17 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.lbl50 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.lbl48 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.lbl42 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl37 = New System.Windows.Forms.Label
		Me.lbl36 = New System.Windows.Forms.Label
		Me.lbl35 = New System.Windows.Forms.Label
		Me.TabControlPage2 = New AxXtremeSuiteControls.AxTabControlPage
		Me.txtCondicionantesEntorno = New System.Windows.Forms.TextBox
		Me.txtLimitaciones = New System.Windows.Forms.TextBox
		Me.txtTipoPrestacion = New System.Windows.Forms.TextBox
		Me.chkPrestacionEconomica = New AxXtremeSuiteControls.AxCheckBox
		Me.chkInscripcionOtg = New AxXtremeSuiteControls.AxCheckBox
		Me.txtFechaOtg = New AxDataControl.AxGmsData
		Me.cmbTransporte = New AxSSDataWidgets_B.AxSSDBCombo
		Me.cmbValoracionSoc = New AxSSDataWidgets_B.AxSSDBCombo
		Me.lbl32 = New System.Windows.Forms.Label
		Me.lbl31 = New System.Windows.Forms.Label
		Me.lbl30 = New System.Windows.Forms.Label
		Me.lbl29 = New System.Windows.Forms.Label
		Me.lbl28 = New System.Windows.Forms.Label
		Me.lbl26 = New System.Windows.Forms.Label
		Me.TabControlPage1 = New AxXtremeSuiteControls.AxTabControlPage
		Me.XPFrame302 = New AxciaXPFrame30.AxXPFrame30
		Me.Text8 = New System.Windows.Forms.TextBox
		Me.txtIdoneidadProgramaN = New System.Windows.Forms.TextBox
		Me.Text9 = New System.Windows.Forms.TextBox
		Me.txtPuntAtencio = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtTecnico = New System.Windows.Forms.TextBox
		Me.txtValoracioPossibilitat = New System.Windows.Forms.TextBox
		Me.txtServicioInicio = New System.Windows.Forms.TextBox
		Me.Text16 = New System.Windows.Forms.TextBox
		Me.txtDemandaInicial = New System.Windows.Forms.TextBox
		Me.txtObservaciones = New System.Windows.Forms.TextBox
		Me.txtIdoneidadPrograma = New System.Windows.Forms.TextBox
		Me.txtFechaInicio = New AxDataControl.AxGmsData
		Me.Label21 = New System.Windows.Forms.Label
		Me.Label19 = New System.Windows.Forms.Label
		Me.Label11 = New System.Windows.Forms.Label
		Me.Label10 = New System.Windows.Forms.Label
		Me.lbl15 = New System.Windows.Forms.Label
		Me.lbl16 = New System.Windows.Forms.Label
		Me.lbl19 = New System.Windows.Forms.Label
		Me.lbl20 = New System.Windows.Forms.Label
		Me.Label9 = New System.Windows.Forms.Label
		Me.XPFrame301 = New AxciaXPFrame30.AxXPFrame30
		Me.OptRecollida = New AxXtremeSuiteControls.AxRadioButton
		Me.cmdImprimir = New AxXtremeSuiteControls.AxPushButton
		Me.OptProfessional = New AxXtremeSuiteControls.AxRadioButton
		Me.txtPoblacionNacimiento = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtDiagnostico = New System.Windows.Forms.TextBox
		Me.txtGradoDiscapacidad = New System.Windows.Forms.TextBox
		Me.Text21 = New System.Windows.Forms.TextBox
		Me.txtColectivo = New System.Windows.Forms.TextBox
		Me.Text18 = New System.Windows.Forms.TextBox
		Me.txtMotivoBaja = New System.Windows.Forms.TextBox
		Me.txtEmail = New System.Windows.Forms.TextBox
		Me.txtCodigoPostal = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtPoblacion = New System.Windows.Forms.TextBox
		Me.txtTelefonoMovil = New System.Windows.Forms.TextBox
		Me.txtTelefono = New System.Windows.Forms.TextBox
		Me.txtNIF = New System.Windows.Forms.TextBox
		Me.txtDireccion = New System.Windows.Forms.TextBox
		Me.txtSegundoApellido = New System.Windows.Forms.TextBox
		Me.txtPrimerApellido = New System.Windows.Forms.TextBox
		Me.txtNombre = New System.Windows.Forms.TextBox
		Me.txtFechaNacimiento = New AxDataControl.AxGmsData
		Me.cmbSexo = New AxSSDataWidgets_B.AxSSDBCombo
		Me.txtFechaFin = New AxDataControl.AxGmsData
		Me.chkTieneCertificadoDiscap = New AxXtremeSuiteControls.AxCheckBox
		Me.cmdDocumentacio = New AxXtremeSuiteControls.AxPushButton
		Me.Label18 = New System.Windows.Forms.Label
		Me.lbl24 = New System.Windows.Forms.Label
		Me.lbl23 = New System.Windows.Forms.Label
		Me.lbl21 = New System.Windows.Forms.Label
		Me.lbl18 = New System.Windows.Forms.Label
		Me.lbl17 = New System.Windows.Forms.Label
		Me.lbl14 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl13 = New System.Windows.Forms.Label
		Me.lbl12 = New System.Windows.Forms.Label
		Me.lbl10 = New System.Windows.Forms.Label
		Me.lbl11 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.cmdGrauOcu = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGencat = New AxXtremeSuiteControls.AxPushButton
		Me.cmdIcap = New AxXtremeSuiteControls.AxPushButton
		Me.cmdPerfilLab = New AxXtremeSuiteControls.AxPushButton
		Me.chkGradoOcupabilidad = New AxXtremeSuiteControls.AxCheckBox
		Me.chkGencat = New AxXtremeSuiteControls.AxCheckBox
		Me.chkIcap = New AxXtremeSuiteControls.AxCheckBox
		Me.chkPerfilLaboral = New AxXtremeSuiteControls.AxCheckBox
		Me.chkMotivacionLaboral = New AxXtremeSuiteControls.AxCheckBox
		Me.cmdMotLaboral = New AxXtremeSuiteControls.AxPushButton
		Me.GrdProgrames = New AxFlexCell.AxGrid
		Me.GrdInsercions = New AxFlexCell.AxGrid
		Me.Label20 = New System.Windows.Forms.Label
		Me.lbl33 = New System.Windows.Forms.Label
		Me.lbl34 = New System.Windows.Forms.Label
		Me.txtCodigoEntidad = New System.Windows.Forms.TextBox
		Me.txtCodigo = New System.Windows.Forms.TextBox
		Me.cmbEstado = New AxSSDataWidgets_B.AxSSDBCombo
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.chkDisponibleParaOferta = New AxXtremeSuiteControls.AxCheckBox
		Me.cmdEntidad = New AxXtremeSuiteControls.AxPushButton
		Me.chkActualmenteTrabajando = New AxXtremeSuiteControls.AxCheckBox
		Me.GrdServeisAct = New AxFlexCell.AxGrid
		Me.chkSusceptibleServeiAssis = New AxXtremeSuiteControls.AxCheckBox
		Me.chkGarantiaJuvenil = New AxXtremeSuiteControls.AxCheckBox
		Me.txtGarantiaJ = New AxDataControl.AxGmsData
		Me.lblTreballant = New System.Windows.Forms.Label
		Me.Label33 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.TabControl1.SuspendLayout()
		Me.TabControlPage10.SuspendLayout()
		Me.TabControlPage9.SuspendLayout()
		Me.TabControlPage8.SuspendLayout()
		Me.TabControlPage7.SuspendLayout()
		Me.TabControlPage6.SuspendLayout()
		Me.TabControlPage5.SuspendLayout()
		Me.TabControlPage4.SuspendLayout()
		Me.TabControlPage3.SuspendLayout()
		Me.TabControlPage2.SuspendLayout()
		Me.TabControlPage1.SuspendLayout()
		Me.XPFrame302.SuspendLayout()
		Me.XPFrame301.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.GrdSeguiment, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage10, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdRelacions, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage9, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdServeis, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage8, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdPrepLab, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage7, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdPai, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdFunciona, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage6, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage5, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbPreferenciaEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbPreferenciaJornada, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbNivelHabilidades, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkDispuestoHacerPractica, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage4, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkCatalan, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkCastellano, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkIngles, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkFrances, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkWord, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkAcces, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkExcel, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkInternet, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkPowerpoint, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkTieneExperienciaLab, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.grdHistLaboral, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbCurri, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdFormacio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkPrestacionEconomica, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkInscripcionOtg, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaOtg, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbTransporte, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbValoracionSoc, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaInicio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.XPFrame302, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.OptRecollida, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdImprimir, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.OptProfessional, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.XPFrame301, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaNacimiento, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbSexo, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaFin, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkTieneCertificadoDiscap, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdDocumentacio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGrauOcu, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGencat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdIcap, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdPerfilLab, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkGradoOcupabilidad, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkGencat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkIcap, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkPerfilLaboral, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkMotivacionLaboral, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdMotLaboral, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdProgrames, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdInsercions, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbEstado, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkDisponibleParaOferta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdEntidad, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkActualmenteTrabajando, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdServeisAct, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkSusceptibleServeiAssis, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkGarantiaJuvenil, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtGarantiaJ, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Usuario inserci�n laboral"
		Me.ClientSize = New System.Drawing.Size(896, 589)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-USUARI_INSERCIO_LABORAL"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmUsuariInsercioLaboral"
		Me.txtNombreCompleto.AutoSize = False
		Me.txtNombreCompleto.Size = New System.Drawing.Size(319, 19)
		Me.txtNombreCompleto.Location = New System.Drawing.Point(104, 10)
		Me.txtNombreCompleto.Maxlength = 50
		Me.txtNombreCompleto.TabIndex = 175
		Me.txtNombreCompleto.Tag = "64"
		Me.txtNombreCompleto.AcceptsReturn = True
		Me.txtNombreCompleto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNombreCompleto.BackColor = System.Drawing.SystemColors.Window
		Me.txtNombreCompleto.CausesValidation = True
		Me.txtNombreCompleto.Enabled = True
		Me.txtNombreCompleto.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNombreCompleto.HideSelection = True
		Me.txtNombreCompleto.ReadOnly = False
		Me.txtNombreCompleto.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNombreCompleto.MultiLine = False
		Me.txtNombreCompleto.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNombreCompleto.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNombreCompleto.TabStop = True
		Me.txtNombreCompleto.Visible = True
		Me.txtNombreCompleto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNombreCompleto.Name = "txtNombreCompleto"
		TabControl1.OcxState = CType(resources.GetObject("TabControl1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControl1.Size = New System.Drawing.Size(887, 483)
		Me.TabControl1.Location = New System.Drawing.Point(4, 76)
		Me.TabControl1.TabIndex = 110
		Me.TabControl1.Name = "TabControl1"
		Me.txtPerfilLaboralN.AutoSize = False
		Me.txtPerfilLaboralN.Size = New System.Drawing.Size(31, 19)
		Me.txtPerfilLaboralN.Location = New System.Drawing.Point(-4530, 278)
		Me.txtPerfilLaboralN.Maxlength = 3
		Me.txtPerfilLaboralN.TabIndex = 103
		Me.txtPerfilLaboralN.Tag = "101###########7"
		Me.txtPerfilLaboralN.Visible = False
		Me.txtPerfilLaboralN.AcceptsReturn = True
		Me.txtPerfilLaboralN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPerfilLaboralN.BackColor = System.Drawing.SystemColors.Window
		Me.txtPerfilLaboralN.CausesValidation = True
		Me.txtPerfilLaboralN.Enabled = True
		Me.txtPerfilLaboralN.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPerfilLaboralN.HideSelection = True
		Me.txtPerfilLaboralN.ReadOnly = False
		Me.txtPerfilLaboralN.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPerfilLaboralN.MultiLine = False
		Me.txtPerfilLaboralN.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPerfilLaboralN.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPerfilLaboralN.TabStop = True
		Me.txtPerfilLaboralN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPerfilLaboralN.Name = "txtPerfilLaboralN"
		Me.txtDocMotivacionLaboral.AutoSize = False
		Me.txtDocMotivacionLaboral.Size = New System.Drawing.Size(660, 19)
		Me.txtDocMotivacionLaboral.Location = New System.Drawing.Point(-4530, 230)
		Me.txtDocMotivacionLaboral.Maxlength = 150
		Me.txtDocMotivacionLaboral.TabIndex = 99
		Me.txtDocMotivacionLaboral.Tag = "82###########7"
		Me.txtDocMotivacionLaboral.Visible = False
		Me.txtDocMotivacionLaboral.AcceptsReturn = True
		Me.txtDocMotivacionLaboral.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDocMotivacionLaboral.BackColor = System.Drawing.SystemColors.Window
		Me.txtDocMotivacionLaboral.CausesValidation = True
		Me.txtDocMotivacionLaboral.Enabled = True
		Me.txtDocMotivacionLaboral.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDocMotivacionLaboral.HideSelection = True
		Me.txtDocMotivacionLaboral.ReadOnly = False
		Me.txtDocMotivacionLaboral.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDocMotivacionLaboral.MultiLine = False
		Me.txtDocMotivacionLaboral.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDocMotivacionLaboral.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDocMotivacionLaboral.TabStop = True
		Me.txtDocMotivacionLaboral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDocMotivacionLaboral.Name = "txtDocMotivacionLaboral"
		Me.txtGradoOcupabilidad.AutoSize = False
		Me.txtGradoOcupabilidad.Size = New System.Drawing.Size(31, 19)
		Me.txtGradoOcupabilidad.Location = New System.Drawing.Point(-4530, 254)
		Me.txtGradoOcupabilidad.Maxlength = 3
		Me.txtGradoOcupabilidad.TabIndex = 102
		Me.txtGradoOcupabilidad.Tag = "33###########7"
		Me.txtGradoOcupabilidad.Visible = False
		Me.txtGradoOcupabilidad.AcceptsReturn = True
		Me.txtGradoOcupabilidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGradoOcupabilidad.BackColor = System.Drawing.SystemColors.Window
		Me.txtGradoOcupabilidad.CausesValidation = True
		Me.txtGradoOcupabilidad.Enabled = True
		Me.txtGradoOcupabilidad.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGradoOcupabilidad.HideSelection = True
		Me.txtGradoOcupabilidad.ReadOnly = False
		Me.txtGradoOcupabilidad.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGradoOcupabilidad.MultiLine = False
		Me.txtGradoOcupabilidad.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGradoOcupabilidad.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGradoOcupabilidad.TabStop = True
		Me.txtGradoOcupabilidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtGradoOcupabilidad.Name = "txtGradoOcupabilidad"
		Me.txtValoracionTecnica.AutoSize = False
		Me.txtValoracionTecnica.Size = New System.Drawing.Size(807, 67)
		Me.txtValoracionTecnica.Location = New System.Drawing.Point(-4652, 58)
		Me.txtValoracionTecnica.Maxlength = 250
		Me.txtValoracionTecnica.MultiLine = True
		Me.txtValoracionTecnica.TabIndex = 85
		Me.txtValoracionTecnica.Tag = "34###########7"
		Me.txtValoracionTecnica.Visible = False
		Me.txtValoracionTecnica.AcceptsReturn = True
		Me.txtValoracionTecnica.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtValoracionTecnica.BackColor = System.Drawing.SystemColors.Window
		Me.txtValoracionTecnica.CausesValidation = True
		Me.txtValoracionTecnica.Enabled = True
		Me.txtValoracionTecnica.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtValoracionTecnica.HideSelection = True
		Me.txtValoracionTecnica.ReadOnly = False
		Me.txtValoracionTecnica.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtValoracionTecnica.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtValoracionTecnica.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtValoracionTecnica.TabStop = True
		Me.txtValoracionTecnica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtValoracionTecnica.Name = "txtValoracionTecnica"
		Me.txtGradoOcupabilidadDoc.AutoSize = False
		Me.txtGradoOcupabilidadDoc.Size = New System.Drawing.Size(660, 19)
		Me.txtGradoOcupabilidadDoc.Location = New System.Drawing.Point(-4530, 134)
		Me.txtGradoOcupabilidadDoc.Maxlength = 150
		Me.txtGradoOcupabilidadDoc.TabIndex = 87
		Me.txtGradoOcupabilidadDoc.Tag = "66###########7"
		Me.txtGradoOcupabilidadDoc.Visible = False
		Me.txtGradoOcupabilidadDoc.AcceptsReturn = True
		Me.txtGradoOcupabilidadDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGradoOcupabilidadDoc.BackColor = System.Drawing.SystemColors.Window
		Me.txtGradoOcupabilidadDoc.CausesValidation = True
		Me.txtGradoOcupabilidadDoc.Enabled = True
		Me.txtGradoOcupabilidadDoc.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGradoOcupabilidadDoc.HideSelection = True
		Me.txtGradoOcupabilidadDoc.ReadOnly = False
		Me.txtGradoOcupabilidadDoc.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGradoOcupabilidadDoc.MultiLine = False
		Me.txtGradoOcupabilidadDoc.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGradoOcupabilidadDoc.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGradoOcupabilidadDoc.TabStop = True
		Me.txtGradoOcupabilidadDoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtGradoOcupabilidadDoc.Name = "txtGradoOcupabilidadDoc"
		Me.txtGencat.AutoSize = False
		Me.txtGencat.Size = New System.Drawing.Size(660, 19)
		Me.txtGencat.Location = New System.Drawing.Point(-4530, 158)
		Me.txtGencat.Maxlength = 150
		Me.txtGencat.TabIndex = 90
		Me.txtGencat.Tag = "67###########7"
		Me.txtGencat.Visible = False
		Me.txtGencat.AcceptsReturn = True
		Me.txtGencat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGencat.BackColor = System.Drawing.SystemColors.Window
		Me.txtGencat.CausesValidation = True
		Me.txtGencat.Enabled = True
		Me.txtGencat.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGencat.HideSelection = True
		Me.txtGencat.ReadOnly = False
		Me.txtGencat.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGencat.MultiLine = False
		Me.txtGencat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGencat.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGencat.TabStop = True
		Me.txtGencat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtGencat.Name = "txtGencat"
		Me.txtICAP.AutoSize = False
		Me.txtICAP.Size = New System.Drawing.Size(660, 19)
		Me.txtICAP.Location = New System.Drawing.Point(-4530, 182)
		Me.txtICAP.Maxlength = 150
		Me.txtICAP.TabIndex = 93
		Me.txtICAP.Tag = "68###########7"
		Me.txtICAP.Visible = False
		Me.txtICAP.AcceptsReturn = True
		Me.txtICAP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtICAP.BackColor = System.Drawing.SystemColors.Window
		Me.txtICAP.CausesValidation = True
		Me.txtICAP.Enabled = True
		Me.txtICAP.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtICAP.HideSelection = True
		Me.txtICAP.ReadOnly = False
		Me.txtICAP.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtICAP.MultiLine = False
		Me.txtICAP.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtICAP.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtICAP.TabStop = True
		Me.txtICAP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtICAP.Name = "txtICAP"
		Me.txtPerfilLaboral.AutoSize = False
		Me.txtPerfilLaboral.Size = New System.Drawing.Size(660, 19)
		Me.txtPerfilLaboral.Location = New System.Drawing.Point(-4530, 206)
		Me.txtPerfilLaboral.Maxlength = 150
		Me.txtPerfilLaboral.TabIndex = 96
		Me.txtPerfilLaboral.Tag = "69###########7"
		Me.txtPerfilLaboral.Visible = False
		Me.txtPerfilLaboral.AcceptsReturn = True
		Me.txtPerfilLaboral.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPerfilLaboral.BackColor = System.Drawing.SystemColors.Window
		Me.txtPerfilLaboral.CausesValidation = True
		Me.txtPerfilLaboral.Enabled = True
		Me.txtPerfilLaboral.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPerfilLaboral.HideSelection = True
		Me.txtPerfilLaboral.ReadOnly = False
		Me.txtPerfilLaboral.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPerfilLaboral.MultiLine = False
		Me.txtPerfilLaboral.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPerfilLaboral.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPerfilLaboral.TabStop = True
		Me.txtPerfilLaboral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPerfilLaboral.Name = "txtPerfilLaboral"
		TabControlPage10.OcxState = CType(resources.GetObject("TabControlPage10.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage10.Size = New System.Drawing.Size(883, 459)
		Me.TabControlPage10.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage10.TabIndex = 179
		Me.TabControlPage10.Visible = False
		Me.TabControlPage10.Name = "TabControlPage10"
		GrdSeguiment.OcxState = CType(resources.GetObject("GrdSeguiment.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdSeguiment.Size = New System.Drawing.Size(870, 437)
		Me.GrdSeguiment.Location = New System.Drawing.Point(6, 6)
		Me.GrdSeguiment.TabIndex = 191
		Me.GrdSeguiment.Name = "GrdSeguiment"
		TabControlPage9.OcxState = CType(resources.GetObject("TabControlPage9.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage9.Size = New System.Drawing.Size(883, 459)
		Me.TabControlPage9.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage9.TabIndex = 177
		Me.TabControlPage9.Visible = False
		Me.TabControlPage9.Name = "TabControlPage9"
		GrdRelacions.OcxState = CType(resources.GetObject("GrdRelacions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdRelacions.Size = New System.Drawing.Size(870, 437)
		Me.GrdRelacions.Location = New System.Drawing.Point(6, 6)
		Me.GrdRelacions.TabIndex = 178
		Me.GrdRelacions.Name = "GrdRelacions"
		TabControlPage8.OcxState = CType(resources.GetObject("TabControlPage8.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage8.Size = New System.Drawing.Size(883, 459)
		Me.TabControlPage8.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage8.TabIndex = 118
		Me.TabControlPage8.Visible = False
		Me.TabControlPage8.Name = "TabControlPage8"
		GrdServeis.OcxState = CType(resources.GetObject("GrdServeis.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdServeis.Size = New System.Drawing.Size(870, 437)
		Me.GrdServeis.Location = New System.Drawing.Point(6, 6)
		Me.GrdServeis.TabIndex = 190
		Me.GrdServeis.Name = "GrdServeis"
		TabControlPage7.OcxState = CType(resources.GetObject("TabControlPage7.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage7.Size = New System.Drawing.Size(883, 459)
		Me.TabControlPage7.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage7.TabIndex = 117
		Me.TabControlPage7.Visible = False
		Me.TabControlPage7.Name = "TabControlPage7"
		GrdPrepLab.OcxState = CType(resources.GetObject("GrdPrepLab.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdPrepLab.Size = New System.Drawing.Size(870, 437)
		Me.GrdPrepLab.Location = New System.Drawing.Point(6, 6)
		Me.GrdPrepLab.TabIndex = 189
		Me.GrdPrepLab.Name = "GrdPrepLab"
		TabControlPage6.OcxState = CType(resources.GetObject("TabControlPage6.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage6.Size = New System.Drawing.Size(883, 459)
		Me.TabControlPage6.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage6.TabIndex = 116
		Me.TabControlPage6.Visible = False
		Me.TabControlPage6.Name = "TabControlPage6"
		Me.txtPai.AutoSize = False
		Me.txtPai.Size = New System.Drawing.Size(620, 19)
		Me.txtPai.Location = New System.Drawing.Point(116, 18)
		Me.txtPai.Maxlength = 150
		Me.txtPai.TabIndex = 81
		Me.txtPai.Tag = "65###########8"
		Me.txtPai.AcceptsReturn = True
		Me.txtPai.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPai.BackColor = System.Drawing.SystemColors.Window
		Me.txtPai.CausesValidation = True
		Me.txtPai.Enabled = True
		Me.txtPai.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPai.HideSelection = True
		Me.txtPai.ReadOnly = False
		Me.txtPai.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPai.MultiLine = False
		Me.txtPai.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPai.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPai.TabStop = True
		Me.txtPai.Visible = True
		Me.txtPai.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPai.Name = "txtPai"
		Me.TxtFunciona.AutoSize = False
		Me.TxtFunciona.Size = New System.Drawing.Size(620, 19)
		Me.TxtFunciona.Location = New System.Drawing.Point(116, 42)
		Me.TxtFunciona.Maxlength = 150
		Me.TxtFunciona.TabIndex = 83
		Me.TxtFunciona.Tag = "70###########8"
		Me.TxtFunciona.AcceptsReturn = True
		Me.TxtFunciona.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.TxtFunciona.BackColor = System.Drawing.SystemColors.Window
		Me.TxtFunciona.CausesValidation = True
		Me.TxtFunciona.Enabled = True
		Me.TxtFunciona.ForeColor = System.Drawing.SystemColors.WindowText
		Me.TxtFunciona.HideSelection = True
		Me.TxtFunciona.ReadOnly = False
		Me.TxtFunciona.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.TxtFunciona.MultiLine = False
		Me.TxtFunciona.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.TxtFunciona.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.TxtFunciona.TabStop = True
		Me.TxtFunciona.Visible = True
		Me.TxtFunciona.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.TxtFunciona.Name = "TxtFunciona"
		cmdPai.OcxState = CType(resources.GetObject("cmdPai.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdPai.Size = New System.Drawing.Size(21, 21)
		Me.cmdPai.Location = New System.Drawing.Point(742, 16)
		Me.cmdPai.TabIndex = 82
		Me.cmdPai.Name = "cmdPai"
		cmdFunciona.OcxState = CType(resources.GetObject("cmdFunciona.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdFunciona.Size = New System.Drawing.Size(21, 21)
		Me.cmdFunciona.Location = New System.Drawing.Point(742, 40)
		Me.cmdFunciona.TabIndex = 84
		Me.cmdFunciona.Name = "cmdFunciona"
		Me.Label7.Text = "PAI"
		Me.Label7.Size = New System.Drawing.Size(101, 15)
		Me.Label7.Location = New System.Drawing.Point(12, 22)
		Me.Label7.TabIndex = 188
		Me.Label7.Tag = "65"
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label7.BackColor = System.Drawing.SystemColors.Control
		Me.Label7.Enabled = True
		Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.UseMnemonic = True
		Me.Label7.Visible = True
		Me.Label7.AutoSize = False
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.Name = "Label7"
		Me.Label12.Text = "Programa Funciona"
		Me.Label12.Size = New System.Drawing.Size(103, 15)
		Me.Label12.Location = New System.Drawing.Point(12, 46)
		Me.Label12.TabIndex = 187
		Me.Label12.Tag = "70"
		Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label12.BackColor = System.Drawing.SystemColors.Control
		Me.Label12.Enabled = True
		Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label12.UseMnemonic = True
		Me.Label12.Visible = True
		Me.Label12.AutoSize = False
		Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label12.Name = "Label12"
		TabControlPage5.OcxState = CType(resources.GetObject("TabControlPage5.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage5.Size = New System.Drawing.Size(883, 459)
		Me.TabControlPage5.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage5.TabIndex = 115
		Me.TabControlPage5.Visible = False
		Me.TabControlPage5.Name = "TabControlPage5"
		Me.txtObservacionesPreferFor.AutoSize = False
		Me.txtObservacionesPreferFor.Size = New System.Drawing.Size(641, 43)
		Me.txtObservacionesPreferFor.Location = New System.Drawing.Point(128, 86)
		Me.txtObservacionesPreferFor.Maxlength = 250
		Me.txtObservacionesPreferFor.MultiLine = True
		Me.txtObservacionesPreferFor.TabIndex = 80
		Me.txtObservacionesPreferFor.Tag = "74###########5"
		Me.txtObservacionesPreferFor.AcceptsReturn = True
		Me.txtObservacionesPreferFor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacionesPreferFor.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacionesPreferFor.CausesValidation = True
		Me.txtObservacionesPreferFor.Enabled = True
		Me.txtObservacionesPreferFor.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacionesPreferFor.HideSelection = True
		Me.txtObservacionesPreferFor.ReadOnly = False
		Me.txtObservacionesPreferFor.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacionesPreferFor.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacionesPreferFor.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservacionesPreferFor.TabStop = True
		Me.txtObservacionesPreferFor.Visible = True
		Me.txtObservacionesPreferFor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacionesPreferFor.Name = "txtObservacionesPreferFor"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(330, 19)
		Me.Text2.Location = New System.Drawing.Point(152, 62)
		Me.Text2.TabIndex = 182
		Me.Text2.Tag = "^73"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtTerceraPreferenciaFor.AutoSize = False
		Me.txtTerceraPreferenciaFor.Size = New System.Drawing.Size(21, 19)
		Me.txtTerceraPreferenciaFor.Location = New System.Drawing.Point(128, 62)
		Me.txtTerceraPreferenciaFor.Maxlength = 2
		Me.txtTerceraPreferenciaFor.TabIndex = 79
		Me.txtTerceraPreferenciaFor.Tag = "73###########5"
		Me.txtTerceraPreferenciaFor.AcceptsReturn = True
		Me.txtTerceraPreferenciaFor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTerceraPreferenciaFor.BackColor = System.Drawing.SystemColors.Window
		Me.txtTerceraPreferenciaFor.CausesValidation = True
		Me.txtTerceraPreferenciaFor.Enabled = True
		Me.txtTerceraPreferenciaFor.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTerceraPreferenciaFor.HideSelection = True
		Me.txtTerceraPreferenciaFor.ReadOnly = False
		Me.txtTerceraPreferenciaFor.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTerceraPreferenciaFor.MultiLine = False
		Me.txtTerceraPreferenciaFor.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTerceraPreferenciaFor.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTerceraPreferenciaFor.TabStop = True
		Me.txtTerceraPreferenciaFor.Visible = True
		Me.txtTerceraPreferenciaFor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTerceraPreferenciaFor.Name = "txtTerceraPreferenciaFor"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(330, 19)
		Me.Text4.Location = New System.Drawing.Point(152, 38)
		Me.Text4.TabIndex = 181
		Me.Text4.Tag = "^72"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtSegonaPreferenciaFor.AutoSize = False
		Me.txtSegonaPreferenciaFor.Size = New System.Drawing.Size(21, 19)
		Me.txtSegonaPreferenciaFor.Location = New System.Drawing.Point(128, 38)
		Me.txtSegonaPreferenciaFor.Maxlength = 2
		Me.txtSegonaPreferenciaFor.TabIndex = 78
		Me.txtSegonaPreferenciaFor.Tag = "72###########5"
		Me.txtSegonaPreferenciaFor.AcceptsReturn = True
		Me.txtSegonaPreferenciaFor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSegonaPreferenciaFor.BackColor = System.Drawing.SystemColors.Window
		Me.txtSegonaPreferenciaFor.CausesValidation = True
		Me.txtSegonaPreferenciaFor.Enabled = True
		Me.txtSegonaPreferenciaFor.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSegonaPreferenciaFor.HideSelection = True
		Me.txtSegonaPreferenciaFor.ReadOnly = False
		Me.txtSegonaPreferenciaFor.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSegonaPreferenciaFor.MultiLine = False
		Me.txtSegonaPreferenciaFor.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSegonaPreferenciaFor.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSegonaPreferenciaFor.TabStop = True
		Me.txtSegonaPreferenciaFor.Visible = True
		Me.txtSegonaPreferenciaFor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSegonaPreferenciaFor.Name = "txtSegonaPreferenciaFor"
		Me.Text7.AutoSize = False
		Me.Text7.BackColor = System.Drawing.Color.White
		Me.Text7.Enabled = False
		Me.Text7.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text7.Size = New System.Drawing.Size(330, 19)
		Me.Text7.Location = New System.Drawing.Point(152, 14)
		Me.Text7.TabIndex = 180
		Me.Text7.Tag = "^71"
		Me.Text7.AcceptsReturn = True
		Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text7.CausesValidation = True
		Me.Text7.HideSelection = True
		Me.Text7.ReadOnly = False
		Me.Text7.Maxlength = 0
		Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text7.MultiLine = False
		Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text7.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text7.TabStop = True
		Me.Text7.Visible = True
		Me.Text7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text7.Name = "Text7"
		Me.txtPrimeraPreferenciaFor.AutoSize = False
		Me.txtPrimeraPreferenciaFor.Size = New System.Drawing.Size(21, 19)
		Me.txtPrimeraPreferenciaFor.Location = New System.Drawing.Point(128, 14)
		Me.txtPrimeraPreferenciaFor.Maxlength = 2
		Me.txtPrimeraPreferenciaFor.TabIndex = 77
		Me.txtPrimeraPreferenciaFor.Tag = "71###########5"
		Me.txtPrimeraPreferenciaFor.AcceptsReturn = True
		Me.txtPrimeraPreferenciaFor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPrimeraPreferenciaFor.BackColor = System.Drawing.SystemColors.Window
		Me.txtPrimeraPreferenciaFor.CausesValidation = True
		Me.txtPrimeraPreferenciaFor.Enabled = True
		Me.txtPrimeraPreferenciaFor.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPrimeraPreferenciaFor.HideSelection = True
		Me.txtPrimeraPreferenciaFor.ReadOnly = False
		Me.txtPrimeraPreferenciaFor.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPrimeraPreferenciaFor.MultiLine = False
		Me.txtPrimeraPreferenciaFor.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPrimeraPreferenciaFor.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPrimeraPreferenciaFor.TabStop = True
		Me.txtPrimeraPreferenciaFor.Visible = True
		Me.txtPrimeraPreferenciaFor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPrimeraPreferenciaFor.Name = "txtPrimeraPreferenciaFor"
		Me.Label13.Text = "Observaciones preferencias"
		Me.Label13.Size = New System.Drawing.Size(111, 35)
		Me.Label13.Location = New System.Drawing.Point(12, 90)
		Me.Label13.TabIndex = 186
		Me.Label13.Tag = "54"
		Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label13.BackColor = System.Drawing.SystemColors.Control
		Me.Label13.Enabled = True
		Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label13.UseMnemonic = True
		Me.Label13.Visible = True
		Me.Label13.AutoSize = False
		Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label13.Name = "Label13"
		Me.Label14.Text = "3� preferencia trabajo"
		Me.Label14.Size = New System.Drawing.Size(111, 15)
		Me.Label14.Location = New System.Drawing.Point(12, 66)
		Me.Label14.TabIndex = 185
		Me.Label14.Tag = "73"
		Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label14.BackColor = System.Drawing.SystemColors.Control
		Me.Label14.Enabled = True
		Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label14.UseMnemonic = True
		Me.Label14.Visible = True
		Me.Label14.AutoSize = False
		Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label14.Name = "Label14"
		Me.Label15.Text = "2� preferencia trabajo"
		Me.Label15.Size = New System.Drawing.Size(111, 15)
		Me.Label15.Location = New System.Drawing.Point(12, 42)
		Me.Label15.TabIndex = 184
		Me.Label15.Tag = "72"
		Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label15.BackColor = System.Drawing.SystemColors.Control
		Me.Label15.Enabled = True
		Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label15.UseMnemonic = True
		Me.Label15.Visible = True
		Me.Label15.AutoSize = False
		Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label15.Name = "Label15"
		Me.Label16.Text = "1� preferencia trabajo"
		Me.Label16.Size = New System.Drawing.Size(111, 15)
		Me.Label16.Location = New System.Drawing.Point(12, 18)
		Me.Label16.TabIndex = 183
		Me.Label16.Tag = "71"
		Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label16.BackColor = System.Drawing.SystemColors.Control
		Me.Label16.Enabled = True
		Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label16.UseMnemonic = True
		Me.Label16.Visible = True
		Me.Label16.AutoSize = False
		Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label16.Name = "Label16"
		TabControlPage4.OcxState = CType(resources.GetObject("TabControlPage4.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage4.Size = New System.Drawing.Size(883, 459)
		Me.TabControlPage4.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage4.TabIndex = 114
		Me.TabControlPage4.Visible = False
		Me.TabControlPage4.Name = "TabControlPage4"
		Me.txtDisponibilidadHorariaP.AutoSize = False
		Me.txtDisponibilidadHorariaP.Size = New System.Drawing.Size(383, 19)
		Me.txtDisponibilidadHorariaP.Location = New System.Drawing.Point(386, 414)
		Me.txtDisponibilidadHorariaP.Maxlength = 100
		Me.txtDisponibilidadHorariaP.TabIndex = 76
		Me.txtDisponibilidadHorariaP.Tag = "84###########4"
		Me.txtDisponibilidadHorariaP.AcceptsReturn = True
		Me.txtDisponibilidadHorariaP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDisponibilidadHorariaP.BackColor = System.Drawing.SystemColors.Window
		Me.txtDisponibilidadHorariaP.CausesValidation = True
		Me.txtDisponibilidadHorariaP.Enabled = True
		Me.txtDisponibilidadHorariaP.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDisponibilidadHorariaP.HideSelection = True
		Me.txtDisponibilidadHorariaP.ReadOnly = False
		Me.txtDisponibilidadHorariaP.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDisponibilidadHorariaP.MultiLine = False
		Me.txtDisponibilidadHorariaP.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDisponibilidadHorariaP.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDisponibilidadHorariaP.TabStop = True
		Me.txtDisponibilidadHorariaP.Visible = True
		Me.txtDisponibilidadHorariaP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDisponibilidadHorariaP.Name = "txtDisponibilidadHorariaP"
		Me.txtObservacionesOtros.AutoSize = False
		Me.txtObservacionesOtros.Size = New System.Drawing.Size(642, 41)
		Me.txtObservacionesOtros.Location = New System.Drawing.Point(126, 344)
		Me.txtObservacionesOtros.Maxlength = 150
		Me.txtObservacionesOtros.MultiLine = True
		Me.txtObservacionesOtros.TabIndex = 73
		Me.txtObservacionesOtros.Tag = "61###########4"
		Me.txtObservacionesOtros.AcceptsReturn = True
		Me.txtObservacionesOtros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacionesOtros.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacionesOtros.CausesValidation = True
		Me.txtObservacionesOtros.Enabled = True
		Me.txtObservacionesOtros.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacionesOtros.HideSelection = True
		Me.txtObservacionesOtros.ReadOnly = False
		Me.txtObservacionesOtros.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacionesOtros.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacionesOtros.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservacionesOtros.TabStop = True
		Me.txtObservacionesOtros.Visible = True
		Me.txtObservacionesOtros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacionesOtros.Name = "txtObservacionesOtros"
		Me.txtDisponibilidadGeograf.AutoSize = False
		Me.txtDisponibilidadGeograf.Size = New System.Drawing.Size(642, 41)
		Me.txtDisponibilidadGeograf.Location = New System.Drawing.Point(126, 298)
		Me.txtDisponibilidadGeograf.Maxlength = 150
		Me.txtDisponibilidadGeograf.MultiLine = True
		Me.txtDisponibilidadGeograf.TabIndex = 72
		Me.txtDisponibilidadGeograf.Tag = "60###########4"
		Me.txtDisponibilidadGeograf.AcceptsReturn = True
		Me.txtDisponibilidadGeograf.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDisponibilidadGeograf.BackColor = System.Drawing.SystemColors.Window
		Me.txtDisponibilidadGeograf.CausesValidation = True
		Me.txtDisponibilidadGeograf.Enabled = True
		Me.txtDisponibilidadGeograf.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDisponibilidadGeograf.HideSelection = True
		Me.txtDisponibilidadGeograf.ReadOnly = False
		Me.txtDisponibilidadGeograf.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDisponibilidadGeograf.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDisponibilidadGeograf.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDisponibilidadGeograf.TabStop = True
		Me.txtDisponibilidadGeograf.Visible = True
		Me.txtDisponibilidadGeograf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDisponibilidadGeograf.Name = "txtDisponibilidadGeograf"
		Me.txtIncorporacion.AutoSize = False
		Me.txtIncorporacion.Size = New System.Drawing.Size(641, 19)
		Me.txtIncorporacion.Location = New System.Drawing.Point(126, 274)
		Me.txtIncorporacion.Maxlength = 100
		Me.txtIncorporacion.TabIndex = 71
		Me.txtIncorporacion.Tag = "59###########4"
		Me.txtIncorporacion.AcceptsReturn = True
		Me.txtIncorporacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtIncorporacion.BackColor = System.Drawing.SystemColors.Window
		Me.txtIncorporacion.CausesValidation = True
		Me.txtIncorporacion.Enabled = True
		Me.txtIncorporacion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtIncorporacion.HideSelection = True
		Me.txtIncorporacion.ReadOnly = False
		Me.txtIncorporacion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtIncorporacion.MultiLine = False
		Me.txtIncorporacion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtIncorporacion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtIncorporacion.TabStop = True
		Me.txtIncorporacion.Visible = True
		Me.txtIncorporacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtIncorporacion.Name = "txtIncorporacion"
		Me.txtSueldoAproximado.AutoSize = False
		Me.txtSueldoAproximado.Size = New System.Drawing.Size(641, 19)
		Me.txtSueldoAproximado.Location = New System.Drawing.Point(126, 250)
		Me.txtSueldoAproximado.Maxlength = 50
		Me.txtSueldoAproximado.TabIndex = 70
		Me.txtSueldoAproximado.Tag = "58###########4"
		Me.txtSueldoAproximado.AcceptsReturn = True
		Me.txtSueldoAproximado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSueldoAproximado.BackColor = System.Drawing.SystemColors.Window
		Me.txtSueldoAproximado.CausesValidation = True
		Me.txtSueldoAproximado.Enabled = True
		Me.txtSueldoAproximado.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSueldoAproximado.HideSelection = True
		Me.txtSueldoAproximado.ReadOnly = False
		Me.txtSueldoAproximado.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSueldoAproximado.MultiLine = False
		Me.txtSueldoAproximado.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSueldoAproximado.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSueldoAproximado.TabStop = True
		Me.txtSueldoAproximado.Visible = True
		Me.txtSueldoAproximado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSueldoAproximado.Name = "txtSueldoAproximado"
		Me.txtCaracteristicasTrabajo.AutoSize = False
		Me.txtCaracteristicasTrabajo.Size = New System.Drawing.Size(641, 43)
		Me.txtCaracteristicasTrabajo.Location = New System.Drawing.Point(126, 154)
		Me.txtCaracteristicasTrabajo.Maxlength = 250
		Me.txtCaracteristicasTrabajo.MultiLine = True
		Me.txtCaracteristicasTrabajo.TabIndex = 67
		Me.txtCaracteristicasTrabajo.Tag = "55###########4"
		Me.txtCaracteristicasTrabajo.AcceptsReturn = True
		Me.txtCaracteristicasTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCaracteristicasTrabajo.BackColor = System.Drawing.SystemColors.Window
		Me.txtCaracteristicasTrabajo.CausesValidation = True
		Me.txtCaracteristicasTrabajo.Enabled = True
		Me.txtCaracteristicasTrabajo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCaracteristicasTrabajo.HideSelection = True
		Me.txtCaracteristicasTrabajo.ReadOnly = False
		Me.txtCaracteristicasTrabajo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCaracteristicasTrabajo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCaracteristicasTrabajo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCaracteristicasTrabajo.TabStop = True
		Me.txtCaracteristicasTrabajo.Visible = True
		Me.txtCaracteristicasTrabajo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCaracteristicasTrabajo.Name = "txtCaracteristicasTrabajo"
		Me.txtObservacionesPref.AutoSize = False
		Me.txtObservacionesPref.Size = New System.Drawing.Size(641, 43)
		Me.txtObservacionesPref.Location = New System.Drawing.Point(126, 106)
		Me.txtObservacionesPref.Maxlength = 250
		Me.txtObservacionesPref.MultiLine = True
		Me.txtObservacionesPref.TabIndex = 66
		Me.txtObservacionesPref.Tag = "54###########4"
		Me.txtObservacionesPref.AcceptsReturn = True
		Me.txtObservacionesPref.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacionesPref.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacionesPref.CausesValidation = True
		Me.txtObservacionesPref.Enabled = True
		Me.txtObservacionesPref.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacionesPref.HideSelection = True
		Me.txtObservacionesPref.ReadOnly = False
		Me.txtObservacionesPref.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacionesPref.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacionesPref.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservacionesPref.TabStop = True
		Me.txtObservacionesPref.Visible = True
		Me.txtObservacionesPref.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacionesPref.Name = "txtObservacionesPref"
		Me.Text53.AutoSize = False
		Me.Text53.BackColor = System.Drawing.Color.White
		Me.Text53.Enabled = False
		Me.Text53.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text53.Size = New System.Drawing.Size(330, 19)
		Me.Text53.Location = New System.Drawing.Point(150, 82)
		Me.Text53.TabIndex = 159
		Me.Text53.Tag = "^53"
		Me.Text53.AcceptsReturn = True
		Me.Text53.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text53.CausesValidation = True
		Me.Text53.HideSelection = True
		Me.Text53.ReadOnly = False
		Me.Text53.Maxlength = 0
		Me.Text53.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text53.MultiLine = False
		Me.Text53.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text53.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text53.TabStop = True
		Me.Text53.Visible = True
		Me.Text53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text53.Name = "Text53"
		Me.txtTerceraPreferenciaTrab.AutoSize = False
		Me.txtTerceraPreferenciaTrab.Size = New System.Drawing.Size(21, 19)
		Me.txtTerceraPreferenciaTrab.Location = New System.Drawing.Point(126, 82)
		Me.txtTerceraPreferenciaTrab.Maxlength = 2
		Me.txtTerceraPreferenciaTrab.TabIndex = 65
		Me.txtTerceraPreferenciaTrab.Tag = "53###########4"
		Me.txtTerceraPreferenciaTrab.AcceptsReturn = True
		Me.txtTerceraPreferenciaTrab.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTerceraPreferenciaTrab.BackColor = System.Drawing.SystemColors.Window
		Me.txtTerceraPreferenciaTrab.CausesValidation = True
		Me.txtTerceraPreferenciaTrab.Enabled = True
		Me.txtTerceraPreferenciaTrab.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTerceraPreferenciaTrab.HideSelection = True
		Me.txtTerceraPreferenciaTrab.ReadOnly = False
		Me.txtTerceraPreferenciaTrab.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTerceraPreferenciaTrab.MultiLine = False
		Me.txtTerceraPreferenciaTrab.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTerceraPreferenciaTrab.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTerceraPreferenciaTrab.TabStop = True
		Me.txtTerceraPreferenciaTrab.Visible = True
		Me.txtTerceraPreferenciaTrab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTerceraPreferenciaTrab.Name = "txtTerceraPreferenciaTrab"
		Me.Text52.AutoSize = False
		Me.Text52.BackColor = System.Drawing.Color.White
		Me.Text52.Enabled = False
		Me.Text52.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text52.Size = New System.Drawing.Size(330, 19)
		Me.Text52.Location = New System.Drawing.Point(150, 58)
		Me.Text52.TabIndex = 158
		Me.Text52.Tag = "^52"
		Me.Text52.AcceptsReturn = True
		Me.Text52.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text52.CausesValidation = True
		Me.Text52.HideSelection = True
		Me.Text52.ReadOnly = False
		Me.Text52.Maxlength = 0
		Me.Text52.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text52.MultiLine = False
		Me.Text52.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text52.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text52.TabStop = True
		Me.Text52.Visible = True
		Me.Text52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text52.Name = "Text52"
		Me.txtSegundaPreferenciaTrab.AutoSize = False
		Me.txtSegundaPreferenciaTrab.Size = New System.Drawing.Size(21, 19)
		Me.txtSegundaPreferenciaTrab.Location = New System.Drawing.Point(126, 58)
		Me.txtSegundaPreferenciaTrab.Maxlength = 2
		Me.txtSegundaPreferenciaTrab.TabIndex = 64
		Me.txtSegundaPreferenciaTrab.Tag = "52###########4"
		Me.txtSegundaPreferenciaTrab.AcceptsReturn = True
		Me.txtSegundaPreferenciaTrab.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSegundaPreferenciaTrab.BackColor = System.Drawing.SystemColors.Window
		Me.txtSegundaPreferenciaTrab.CausesValidation = True
		Me.txtSegundaPreferenciaTrab.Enabled = True
		Me.txtSegundaPreferenciaTrab.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSegundaPreferenciaTrab.HideSelection = True
		Me.txtSegundaPreferenciaTrab.ReadOnly = False
		Me.txtSegundaPreferenciaTrab.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSegundaPreferenciaTrab.MultiLine = False
		Me.txtSegundaPreferenciaTrab.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSegundaPreferenciaTrab.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSegundaPreferenciaTrab.TabStop = True
		Me.txtSegundaPreferenciaTrab.Visible = True
		Me.txtSegundaPreferenciaTrab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSegundaPreferenciaTrab.Name = "txtSegundaPreferenciaTrab"
		Me.Text51.AutoSize = False
		Me.Text51.BackColor = System.Drawing.Color.White
		Me.Text51.Enabled = False
		Me.Text51.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text51.Size = New System.Drawing.Size(330, 19)
		Me.Text51.Location = New System.Drawing.Point(150, 34)
		Me.Text51.TabIndex = 157
		Me.Text51.Tag = "^51"
		Me.Text51.AcceptsReturn = True
		Me.Text51.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text51.CausesValidation = True
		Me.Text51.HideSelection = True
		Me.Text51.ReadOnly = False
		Me.Text51.Maxlength = 0
		Me.Text51.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text51.MultiLine = False
		Me.Text51.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text51.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text51.TabStop = True
		Me.Text51.Visible = True
		Me.Text51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text51.Name = "Text51"
		Me.txtPrimeraPreferenciaTrab.AutoSize = False
		Me.txtPrimeraPreferenciaTrab.Size = New System.Drawing.Size(21, 19)
		Me.txtPrimeraPreferenciaTrab.Location = New System.Drawing.Point(126, 34)
		Me.txtPrimeraPreferenciaTrab.Maxlength = 2
		Me.txtPrimeraPreferenciaTrab.TabIndex = 63
		Me.txtPrimeraPreferenciaTrab.Tag = "51###########4"
		Me.txtPrimeraPreferenciaTrab.AcceptsReturn = True
		Me.txtPrimeraPreferenciaTrab.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPrimeraPreferenciaTrab.BackColor = System.Drawing.SystemColors.Window
		Me.txtPrimeraPreferenciaTrab.CausesValidation = True
		Me.txtPrimeraPreferenciaTrab.Enabled = True
		Me.txtPrimeraPreferenciaTrab.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPrimeraPreferenciaTrab.HideSelection = True
		Me.txtPrimeraPreferenciaTrab.ReadOnly = False
		Me.txtPrimeraPreferenciaTrab.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPrimeraPreferenciaTrab.MultiLine = False
		Me.txtPrimeraPreferenciaTrab.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPrimeraPreferenciaTrab.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPrimeraPreferenciaTrab.TabStop = True
		Me.txtPrimeraPreferenciaTrab.Visible = True
		Me.txtPrimeraPreferenciaTrab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPrimeraPreferenciaTrab.Name = "txtPrimeraPreferenciaTrab"
		cmbPreferenciaEmpresa.OcxState = CType(resources.GetObject("cmbPreferenciaEmpresa.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbPreferenciaEmpresa.Size = New System.Drawing.Size(145, 19)
		Me.cmbPreferenciaEmpresa.Location = New System.Drawing.Point(126, 202)
		Me.cmbPreferenciaEmpresa.TabIndex = 68
		Me.cmbPreferenciaEmpresa.Name = "cmbPreferenciaEmpresa"
		cmbPreferenciaJornada.OcxState = CType(resources.GetObject("cmbPreferenciaJornada.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbPreferenciaJornada.Size = New System.Drawing.Size(145, 19)
		Me.cmbPreferenciaJornada.Location = New System.Drawing.Point(126, 226)
		Me.cmbPreferenciaJornada.TabIndex = 69
		Me.cmbPreferenciaJornada.Name = "cmbPreferenciaJornada"
		cmbNivelHabilidades.OcxState = CType(resources.GetObject("cmbNivelHabilidades.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbNivelHabilidades.Size = New System.Drawing.Size(145, 19)
		Me.cmbNivelHabilidades.Location = New System.Drawing.Point(310, 390)
		Me.cmbNivelHabilidades.TabIndex = 74
		Me.cmbNivelHabilidades.Name = "cmbNivelHabilidades"
		chkDispuestoHacerPractica.OcxState = CType(resources.GetObject("chkDispuestoHacerPractica.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkDispuestoHacerPractica.Size = New System.Drawing.Size(155, 19)
		Me.chkDispuestoHacerPractica.Location = New System.Drawing.Point(10, 414)
		Me.chkDispuestoHacerPractica.TabIndex = 75
		Me.chkDispuestoHacerPractica.Name = "chkDispuestoHacerPractica"
		Me.Label8.Text = "Disponibilidad horaria para hecer pr�cticas"
		Me.Label8.Size = New System.Drawing.Size(215, 15)
		Me.Label8.Location = New System.Drawing.Point(176, 418)
		Me.Label8.TabIndex = 203
		Me.Label8.Tag = "84"
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label8.BackColor = System.Drawing.SystemColors.Control
		Me.Label8.Enabled = True
		Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.UseMnemonic = True
		Me.Label8.Visible = True
		Me.Label8.AutoSize = False
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.Name = "Label8"
		Me.lbl62.Text = "Nivel de conocimientos y habilidades de b�squeda de trabajo"
		Me.lbl62.Size = New System.Drawing.Size(299, 15)
		Me.lbl62.Location = New System.Drawing.Point(10, 392)
		Me.lbl62.TabIndex = 171
		Me.lbl62.Tag = "62"
		Me.lbl62.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl62.BackColor = System.Drawing.SystemColors.Control
		Me.lbl62.Enabled = True
		Me.lbl62.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl62.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl62.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl62.UseMnemonic = True
		Me.lbl62.Visible = True
		Me.lbl62.AutoSize = False
		Me.lbl62.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl62.Name = "lbl62"
		Me.lbl61.Text = "Observaciones / Otros"
		Me.lbl61.Size = New System.Drawing.Size(111, 15)
		Me.lbl61.Location = New System.Drawing.Point(10, 348)
		Me.lbl61.TabIndex = 170
		Me.lbl61.Tag = "61"
		Me.lbl61.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl61.BackColor = System.Drawing.SystemColors.Control
		Me.lbl61.Enabled = True
		Me.lbl61.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl61.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl61.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl61.UseMnemonic = True
		Me.lbl61.Visible = True
		Me.lbl61.AutoSize = False
		Me.lbl61.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl61.Name = "lbl61"
		Me.lbl60.Text = "Disponibilidad geogr�fica"
		Me.lbl60.Size = New System.Drawing.Size(111, 15)
		Me.lbl60.Location = New System.Drawing.Point(10, 302)
		Me.lbl60.TabIndex = 169
		Me.lbl60.Tag = "60"
		Me.lbl60.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl60.BackColor = System.Drawing.SystemColors.Control
		Me.lbl60.Enabled = True
		Me.lbl60.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl60.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl60.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl60.UseMnemonic = True
		Me.lbl60.Visible = True
		Me.lbl60.AutoSize = False
		Me.lbl60.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl60.Name = "lbl60"
		Me.lbl59.Text = "Incorporaci�n"
		Me.lbl59.Size = New System.Drawing.Size(111, 15)
		Me.lbl59.Location = New System.Drawing.Point(10, 278)
		Me.lbl59.TabIndex = 168
		Me.lbl59.Tag = "59"
		Me.lbl59.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl59.BackColor = System.Drawing.SystemColors.Control
		Me.lbl59.Enabled = True
		Me.lbl59.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl59.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl59.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl59.UseMnemonic = True
		Me.lbl59.Visible = True
		Me.lbl59.AutoSize = False
		Me.lbl59.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl59.Name = "lbl59"
		Me.lbl58.Text = "Sueldo aproximado"
		Me.lbl58.Size = New System.Drawing.Size(111, 15)
		Me.lbl58.Location = New System.Drawing.Point(10, 254)
		Me.lbl58.TabIndex = 167
		Me.lbl58.Tag = "58"
		Me.lbl58.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl58.BackColor = System.Drawing.SystemColors.Control
		Me.lbl58.Enabled = True
		Me.lbl58.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl58.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl58.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl58.UseMnemonic = True
		Me.lbl58.Visible = True
		Me.lbl58.AutoSize = False
		Me.lbl58.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl58.Name = "lbl58"
		Me.lbl57.Text = "Preferencia jornada"
		Me.lbl57.Size = New System.Drawing.Size(111, 15)
		Me.lbl57.Location = New System.Drawing.Point(10, 230)
		Me.lbl57.TabIndex = 166
		Me.lbl57.Tag = "57"
		Me.lbl57.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl57.BackColor = System.Drawing.SystemColors.Control
		Me.lbl57.Enabled = True
		Me.lbl57.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl57.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl57.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl57.UseMnemonic = True
		Me.lbl57.Visible = True
		Me.lbl57.AutoSize = False
		Me.lbl57.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl57.Name = "lbl57"
		Me.lbl56.Text = "Preferencia empresa"
		Me.lbl56.Size = New System.Drawing.Size(111, 15)
		Me.lbl56.Location = New System.Drawing.Point(10, 206)
		Me.lbl56.TabIndex = 165
		Me.lbl56.Tag = "56"
		Me.lbl56.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl56.BackColor = System.Drawing.SystemColors.Control
		Me.lbl56.Enabled = True
		Me.lbl56.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl56.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl56.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl56.UseMnemonic = True
		Me.lbl56.Visible = True
		Me.lbl56.AutoSize = False
		Me.lbl56.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl56.Name = "lbl56"
		Me.lbl55.Text = "Caracteristicas del trabajo"
		Me.lbl55.Size = New System.Drawing.Size(117, 43)
		Me.lbl55.Location = New System.Drawing.Point(10, 158)
		Me.lbl55.TabIndex = 164
		Me.lbl55.Tag = "55"
		Me.lbl55.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl55.BackColor = System.Drawing.SystemColors.Control
		Me.lbl55.Enabled = True
		Me.lbl55.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl55.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl55.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl55.UseMnemonic = True
		Me.lbl55.Visible = True
		Me.lbl55.AutoSize = False
		Me.lbl55.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl55.Name = "lbl55"
		Me.lbl54.Text = "Observaciones preferencias"
		Me.lbl54.Size = New System.Drawing.Size(111, 35)
		Me.lbl54.Location = New System.Drawing.Point(10, 110)
		Me.lbl54.TabIndex = 163
		Me.lbl54.Tag = "54"
		Me.lbl54.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl54.BackColor = System.Drawing.SystemColors.Control
		Me.lbl54.Enabled = True
		Me.lbl54.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl54.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl54.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl54.UseMnemonic = True
		Me.lbl54.Visible = True
		Me.lbl54.AutoSize = False
		Me.lbl54.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl54.Name = "lbl54"
		Me.lbl53.Text = "3� preferencia trabajo"
		Me.lbl53.Size = New System.Drawing.Size(111, 15)
		Me.lbl53.Location = New System.Drawing.Point(10, 86)
		Me.lbl53.TabIndex = 162
		Me.lbl53.Tag = "53"
		Me.lbl53.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl53.BackColor = System.Drawing.SystemColors.Control
		Me.lbl53.Enabled = True
		Me.lbl53.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl53.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl53.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl53.UseMnemonic = True
		Me.lbl53.Visible = True
		Me.lbl53.AutoSize = False
		Me.lbl53.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl53.Name = "lbl53"
		Me.lbl52.Text = "2� preferencia trabajo"
		Me.lbl52.Size = New System.Drawing.Size(111, 15)
		Me.lbl52.Location = New System.Drawing.Point(10, 62)
		Me.lbl52.TabIndex = 161
		Me.lbl52.Tag = "52"
		Me.lbl52.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl52.BackColor = System.Drawing.SystemColors.Control
		Me.lbl52.Enabled = True
		Me.lbl52.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl52.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl52.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl52.UseMnemonic = True
		Me.lbl52.Visible = True
		Me.lbl52.AutoSize = False
		Me.lbl52.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl52.Name = "lbl52"
		Me.lbl51.Text = "1� preferencia trabajo"
		Me.lbl51.Size = New System.Drawing.Size(111, 15)
		Me.lbl51.Location = New System.Drawing.Point(10, 38)
		Me.lbl51.TabIndex = 160
		Me.lbl51.Tag = "51"
		Me.lbl51.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl51.BackColor = System.Drawing.SystemColors.Control
		Me.lbl51.Enabled = True
		Me.lbl51.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl51.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl51.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl51.UseMnemonic = True
		Me.lbl51.Visible = True
		Me.lbl51.AutoSize = False
		Me.lbl51.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl51.Name = "lbl51"
		Me.Label5.Text = "PREFERENCIAS EN LA B�SQUEDA DE TRABAJO"
		Me.Label5.Size = New System.Drawing.Size(363, 19)
		Me.Label5.Location = New System.Drawing.Point(10, 14)
		Me.Label5.TabIndex = 156
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		TabControlPage3.OcxState = CType(resources.GetObject("TabControlPage3.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage3.Size = New System.Drawing.Size(883, 459)
		Me.TabControlPage3.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage3.TabIndex = 113
		Me.TabControlPage3.Visible = False
		Me.TabControlPage3.Name = "TabControlPage3"
		Me.txtCurriculumVitae.AutoSize = False
		Me.txtCurriculumVitae.Size = New System.Drawing.Size(628, 19)
		Me.txtCurriculumVitae.Location = New System.Drawing.Point(114, 432)
		Me.txtCurriculumVitae.Maxlength = 150
		Me.txtCurriculumVitae.TabIndex = 61
		Me.txtCurriculumVitae.Tag = "50###########3"
		Me.txtCurriculumVitae.AcceptsReturn = True
		Me.txtCurriculumVitae.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCurriculumVitae.BackColor = System.Drawing.SystemColors.Window
		Me.txtCurriculumVitae.CausesValidation = True
		Me.txtCurriculumVitae.Enabled = True
		Me.txtCurriculumVitae.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCurriculumVitae.HideSelection = True
		Me.txtCurriculumVitae.ReadOnly = False
		Me.txtCurriculumVitae.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCurriculumVitae.MultiLine = False
		Me.txtCurriculumVitae.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCurriculumVitae.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCurriculumVitae.TabStop = True
		Me.txtCurriculumVitae.Visible = True
		Me.txtCurriculumVitae.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCurriculumVitae.Name = "txtCurriculumVitae"
		Me.txtOtrosProgramas.AutoSize = False
		Me.txtOtrosProgramas.Size = New System.Drawing.Size(655, 19)
		Me.txtOtrosProgramas.Location = New System.Drawing.Point(112, 164)
		Me.txtOtrosProgramas.Maxlength = 80
		Me.txtOtrosProgramas.TabIndex = 59
		Me.txtOtrosProgramas.Tag = "48###########3"
		Me.txtOtrosProgramas.AcceptsReturn = True
		Me.txtOtrosProgramas.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtOtrosProgramas.BackColor = System.Drawing.SystemColors.Window
		Me.txtOtrosProgramas.CausesValidation = True
		Me.txtOtrosProgramas.Enabled = True
		Me.txtOtrosProgramas.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtOtrosProgramas.HideSelection = True
		Me.txtOtrosProgramas.ReadOnly = False
		Me.txtOtrosProgramas.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtOtrosProgramas.MultiLine = False
		Me.txtOtrosProgramas.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtOtrosProgramas.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtOtrosProgramas.TabStop = True
		Me.txtOtrosProgramas.Visible = True
		Me.txtOtrosProgramas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtOtrosProgramas.Name = "txtOtrosProgramas"
		Me.txtOtros.AutoSize = False
		Me.txtOtros.Size = New System.Drawing.Size(297, 19)
		Me.txtOtros.Location = New System.Drawing.Point(470, 116)
		Me.txtOtros.Maxlength = 80
		Me.txtOtros.TabIndex = 53
		Me.txtOtros.Tag = "42###########3"
		Me.txtOtros.AcceptsReturn = True
		Me.txtOtros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtOtros.BackColor = System.Drawing.SystemColors.Window
		Me.txtOtros.CausesValidation = True
		Me.txtOtros.Enabled = True
		Me.txtOtros.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtOtros.HideSelection = True
		Me.txtOtros.ReadOnly = False
		Me.txtOtros.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtOtros.MultiLine = False
		Me.txtOtros.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtOtros.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtOtros.TabStop = True
		Me.txtOtros.Visible = True
		Me.txtOtros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtOtros.Name = "txtOtros"
		Me.txtFormacionCom.AutoSize = False
		Me.txtFormacionCom.Size = New System.Drawing.Size(655, 29)
		Me.txtFormacionCom.Location = New System.Drawing.Point(112, 82)
		Me.txtFormacionCom.Maxlength = 200
		Me.txtFormacionCom.MultiLine = True
		Me.txtFormacionCom.TabIndex = 48
		Me.txtFormacionCom.Tag = "37###########3"
		Me.txtFormacionCom.AcceptsReturn = True
		Me.txtFormacionCom.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFormacionCom.BackColor = System.Drawing.SystemColors.Window
		Me.txtFormacionCom.CausesValidation = True
		Me.txtFormacionCom.Enabled = True
		Me.txtFormacionCom.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFormacionCom.HideSelection = True
		Me.txtFormacionCom.ReadOnly = False
		Me.txtFormacionCom.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFormacionCom.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFormacionCom.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFormacionCom.TabStop = True
		Me.txtFormacionCom.Visible = True
		Me.txtFormacionCom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFormacionCom.Name = "txtFormacionCom"
		Me.txtEspecialidad.AutoSize = False
		Me.txtEspecialidad.Size = New System.Drawing.Size(655, 29)
		Me.txtEspecialidad.Location = New System.Drawing.Point(112, 48)
		Me.txtEspecialidad.Maxlength = 200
		Me.txtEspecialidad.MultiLine = True
		Me.txtEspecialidad.TabIndex = 47
		Me.txtEspecialidad.Tag = "36###########3"
		Me.txtEspecialidad.AcceptsReturn = True
		Me.txtEspecialidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEspecialidad.BackColor = System.Drawing.SystemColors.Window
		Me.txtEspecialidad.CausesValidation = True
		Me.txtEspecialidad.Enabled = True
		Me.txtEspecialidad.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEspecialidad.HideSelection = True
		Me.txtEspecialidad.ReadOnly = False
		Me.txtEspecialidad.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEspecialidad.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEspecialidad.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEspecialidad.TabStop = True
		Me.txtEspecialidad.Visible = True
		Me.txtEspecialidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEspecialidad.Name = "txtEspecialidad"
		Me.Text35.AutoSize = False
		Me.Text35.BackColor = System.Drawing.Color.White
		Me.Text35.Enabled = False
		Me.Text35.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text35.Size = New System.Drawing.Size(494, 19)
		Me.Text35.Location = New System.Drawing.Point(146, 24)
		Me.Text35.TabIndex = 146
		Me.Text35.Tag = "^35"
		Me.Text35.AcceptsReturn = True
		Me.Text35.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text35.CausesValidation = True
		Me.Text35.HideSelection = True
		Me.Text35.ReadOnly = False
		Me.Text35.Maxlength = 0
		Me.Text35.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text35.MultiLine = False
		Me.Text35.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text35.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text35.TabStop = True
		Me.Text35.Visible = True
		Me.Text35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text35.Name = "Text35"
		Me.txtFormacionReglada.AutoSize = False
		Me.txtFormacionReglada.Size = New System.Drawing.Size(31, 19)
		Me.txtFormacionReglada.Location = New System.Drawing.Point(112, 24)
		Me.txtFormacionReglada.Maxlength = 3
		Me.txtFormacionReglada.TabIndex = 46
		Me.txtFormacionReglada.Tag = "35###########3"
		Me.txtFormacionReglada.AcceptsReturn = True
		Me.txtFormacionReglada.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFormacionReglada.BackColor = System.Drawing.SystemColors.Window
		Me.txtFormacionReglada.CausesValidation = True
		Me.txtFormacionReglada.Enabled = True
		Me.txtFormacionReglada.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFormacionReglada.HideSelection = True
		Me.txtFormacionReglada.ReadOnly = False
		Me.txtFormacionReglada.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFormacionReglada.MultiLine = False
		Me.txtFormacionReglada.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFormacionReglada.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFormacionReglada.TabStop = True
		Me.txtFormacionReglada.Visible = True
		Me.txtFormacionReglada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFormacionReglada.Name = "txtFormacionReglada"
		chkCatalan.OcxState = CType(resources.GetObject("chkCatalan.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkCatalan.Size = New System.Drawing.Size(53, 19)
		Me.chkCatalan.Location = New System.Drawing.Point(112, 118)
		Me.chkCatalan.TabIndex = 49
		Me.chkCatalan.Name = "chkCatalan"
		chkCastellano.OcxState = CType(resources.GetObject("chkCastellano.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkCastellano.Size = New System.Drawing.Size(67, 19)
		Me.chkCastellano.Location = New System.Drawing.Point(184, 118)
		Me.chkCastellano.TabIndex = 50
		Me.chkCastellano.Name = "chkCastellano"
		chkIngles.OcxState = CType(resources.GetObject("chkIngles.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkIngles.Size = New System.Drawing.Size(55, 19)
		Me.chkIngles.Location = New System.Drawing.Point(268, 118)
		Me.chkIngles.TabIndex = 51
		Me.chkIngles.Name = "chkIngles"
		chkFrances.OcxState = CType(resources.GetObject("chkFrances.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkFrances.Size = New System.Drawing.Size(55, 19)
		Me.chkFrances.Location = New System.Drawing.Point(336, 118)
		Me.chkFrances.TabIndex = 52
		Me.chkFrances.Name = "chkFrances"
		chkWord.OcxState = CType(resources.GetObject("chkWord.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkWord.Size = New System.Drawing.Size(53, 19)
		Me.chkWord.Location = New System.Drawing.Point(112, 144)
		Me.chkWord.TabIndex = 54
		Me.chkWord.Name = "chkWord"
		chkAcces.OcxState = CType(resources.GetObject("chkAcces.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkAcces.Size = New System.Drawing.Size(67, 19)
		Me.chkAcces.Location = New System.Drawing.Point(184, 144)
		Me.chkAcces.TabIndex = 55
		Me.chkAcces.Name = "chkAcces"
		chkExcel.OcxState = CType(resources.GetObject("chkExcel.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkExcel.Size = New System.Drawing.Size(47, 19)
		Me.chkExcel.Location = New System.Drawing.Point(268, 144)
		Me.chkExcel.TabIndex = 56
		Me.chkExcel.Name = "chkExcel"
		chkInternet.OcxState = CType(resources.GetObject("chkInternet.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkInternet.Size = New System.Drawing.Size(55, 19)
		Me.chkInternet.Location = New System.Drawing.Point(336, 144)
		Me.chkInternet.TabIndex = 57
		Me.chkInternet.Name = "chkInternet"
		chkPowerpoint.OcxState = CType(resources.GetObject("chkPowerpoint.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkPowerpoint.Size = New System.Drawing.Size(71, 19)
		Me.chkPowerpoint.Location = New System.Drawing.Point(402, 144)
		Me.chkPowerpoint.TabIndex = 58
		Me.chkPowerpoint.Name = "chkPowerpoint"
		chkTieneExperienciaLab.OcxState = CType(resources.GetObject("chkTieneExperienciaLab.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkTieneExperienciaLab.Size = New System.Drawing.Size(153, 19)
		Me.chkTieneExperienciaLab.Location = New System.Drawing.Point(10, 300)
		Me.chkTieneExperienciaLab.TabIndex = 60
		Me.chkTieneExperienciaLab.Name = "chkTieneExperienciaLab"
		grdHistLaboral.OcxState = CType(resources.GetObject("grdHistLaboral.OcxState"), System.Windows.Forms.AxHost.State)
		Me.grdHistLaboral.Size = New System.Drawing.Size(758, 93)
		Me.grdHistLaboral.Location = New System.Drawing.Point(10, 334)
		Me.grdHistLaboral.TabIndex = 173
		Me.grdHistLaboral.Name = "grdHistLaboral"
		cmbCurri.OcxState = CType(resources.GetObject("cmbCurri.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbCurri.Size = New System.Drawing.Size(21, 21)
		Me.cmbCurri.Location = New System.Drawing.Point(746, 430)
		Me.cmbCurri.TabIndex = 62
		Me.cmbCurri.Name = "cmbCurri"
		GrdFormacio.OcxState = CType(resources.GetObject("GrdFormacio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdFormacio.Size = New System.Drawing.Size(758, 75)
		Me.GrdFormacio.Location = New System.Drawing.Point(10, 200)
		Me.GrdFormacio.TabIndex = 192
		Me.GrdFormacio.Name = "GrdFormacio"
		Me.Label17.Text = "Historial formaci�n"
		Me.Label17.Size = New System.Drawing.Size(157, 15)
		Me.Label17.Location = New System.Drawing.Point(10, 186)
		Me.Label17.TabIndex = 193
		Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label17.BackColor = System.Drawing.SystemColors.Control
		Me.Label17.Enabled = True
		Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label17.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label17.UseMnemonic = True
		Me.Label17.Visible = True
		Me.Label17.AutoSize = False
		Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label17.Name = "Label17"
		Me.Label6.Text = "Historial laboral"
		Me.Label6.Size = New System.Drawing.Size(157, 15)
		Me.Label6.Location = New System.Drawing.Point(10, 320)
		Me.Label6.TabIndex = 174
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.lbl50.Text = "Curr�culum Vitae"
		Me.lbl50.Size = New System.Drawing.Size(111, 15)
		Me.lbl50.Location = New System.Drawing.Point(10, 436)
		Me.lbl50.TabIndex = 155
		Me.lbl50.Tag = "50"
		Me.lbl50.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl50.BackColor = System.Drawing.SystemColors.Control
		Me.lbl50.Enabled = True
		Me.lbl50.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl50.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl50.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl50.UseMnemonic = True
		Me.lbl50.Visible = True
		Me.lbl50.AutoSize = False
		Me.lbl50.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl50.Name = "lbl50"
		Me.Label4.Text = "EXPERIENCIA PROFESIONAL"
		Me.Label4.Size = New System.Drawing.Size(275, 19)
		Me.Label4.Location = New System.Drawing.Point(10, 282)
		Me.Label4.TabIndex = 154
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label3.Text = "DATOS FORMACI�N"
		Me.Label3.Size = New System.Drawing.Size(275, 19)
		Me.Label3.Location = New System.Drawing.Point(10, 6)
		Me.Label3.TabIndex = 153
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.lbl48.Text = "Otros programas"
		Me.lbl48.Size = New System.Drawing.Size(111, 15)
		Me.lbl48.Location = New System.Drawing.Point(12, 168)
		Me.lbl48.TabIndex = 152
		Me.lbl48.Tag = "48"
		Me.lbl48.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl48.BackColor = System.Drawing.SystemColors.Control
		Me.lbl48.Enabled = True
		Me.lbl48.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl48.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl48.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl48.UseMnemonic = True
		Me.lbl48.Visible = True
		Me.lbl48.AutoSize = False
		Me.lbl48.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl48.Name = "lbl48"
		Me.Label2.Text = "Inform�tica:"
		Me.Label2.Size = New System.Drawing.Size(59, 17)
		Me.Label2.Location = New System.Drawing.Point(12, 146)
		Me.Label2.TabIndex = 151
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.lbl42.Text = "Otros idiomas"
		Me.lbl42.Size = New System.Drawing.Size(99, 15)
		Me.lbl42.Location = New System.Drawing.Point(402, 122)
		Me.lbl42.TabIndex = 150
		Me.lbl42.Tag = "42"
		Me.lbl42.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl42.BackColor = System.Drawing.SystemColors.Control
		Me.lbl42.Enabled = True
		Me.lbl42.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl42.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl42.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl42.UseMnemonic = True
		Me.lbl42.Visible = True
		Me.lbl42.AutoSize = False
		Me.lbl42.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl42.Name = "lbl42"
		Me.Label1.Text = "Idiomas:"
		Me.Label1.Size = New System.Drawing.Size(59, 17)
		Me.Label1.Location = New System.Drawing.Point(12, 122)
		Me.Label1.TabIndex = 149
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl37.Text = "Formaci�n complementaria"
		Me.lbl37.Size = New System.Drawing.Size(111, 29)
		Me.lbl37.Location = New System.Drawing.Point(10, 86)
		Me.lbl37.TabIndex = 148
		Me.lbl37.Tag = "37"
		Me.lbl37.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl37.BackColor = System.Drawing.SystemColors.Control
		Me.lbl37.Enabled = True
		Me.lbl37.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl37.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl37.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl37.UseMnemonic = True
		Me.lbl37.Visible = True
		Me.lbl37.AutoSize = False
		Me.lbl37.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl37.Name = "lbl37"
		Me.lbl36.Text = "Especialidad"
		Me.lbl36.Size = New System.Drawing.Size(111, 15)
		Me.lbl36.Location = New System.Drawing.Point(10, 52)
		Me.lbl36.TabIndex = 147
		Me.lbl36.Tag = "36"
		Me.lbl36.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl36.BackColor = System.Drawing.SystemColors.Control
		Me.lbl36.Enabled = True
		Me.lbl36.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl36.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl36.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl36.UseMnemonic = True
		Me.lbl36.Visible = True
		Me.lbl36.AutoSize = False
		Me.lbl36.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl36.Name = "lbl36"
		Me.lbl35.Text = "Formaci�n reglada"
		Me.lbl35.Size = New System.Drawing.Size(111, 15)
		Me.lbl35.Location = New System.Drawing.Point(10, 28)
		Me.lbl35.TabIndex = 145
		Me.lbl35.Tag = "35"
		Me.lbl35.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl35.BackColor = System.Drawing.SystemColors.Control
		Me.lbl35.Enabled = True
		Me.lbl35.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl35.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl35.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl35.UseMnemonic = True
		Me.lbl35.Visible = True
		Me.lbl35.AutoSize = False
		Me.lbl35.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl35.Name = "lbl35"
		TabControlPage2.OcxState = CType(resources.GetObject("TabControlPage2.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage2.Size = New System.Drawing.Size(883, 459)
		Me.TabControlPage2.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage2.TabIndex = 112
		Me.TabControlPage2.Visible = False
		Me.TabControlPage2.Name = "TabControlPage2"
		Me.txtCondicionantesEntorno.AutoSize = False
		Me.txtCondicionantesEntorno.Size = New System.Drawing.Size(543, 19)
		Me.txtCondicionantesEntorno.Location = New System.Drawing.Point(224, 134)
		Me.txtCondicionantesEntorno.Maxlength = 100
		Me.txtCondicionantesEntorno.TabIndex = 43
		Me.txtCondicionantesEntorno.Tag = "30###########6"
		Me.txtCondicionantesEntorno.AcceptsReturn = True
		Me.txtCondicionantesEntorno.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCondicionantesEntorno.BackColor = System.Drawing.SystemColors.Window
		Me.txtCondicionantesEntorno.CausesValidation = True
		Me.txtCondicionantesEntorno.Enabled = True
		Me.txtCondicionantesEntorno.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCondicionantesEntorno.HideSelection = True
		Me.txtCondicionantesEntorno.ReadOnly = False
		Me.txtCondicionantesEntorno.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCondicionantesEntorno.MultiLine = False
		Me.txtCondicionantesEntorno.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCondicionantesEntorno.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCondicionantesEntorno.TabStop = True
		Me.txtCondicionantesEntorno.Visible = True
		Me.txtCondicionantesEntorno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCondicionantesEntorno.Name = "txtCondicionantesEntorno"
		Me.txtLimitaciones.AutoSize = False
		Me.txtLimitaciones.Size = New System.Drawing.Size(759, 55)
		Me.txtLimitaciones.Location = New System.Drawing.Point(8, 74)
		Me.txtLimitaciones.Maxlength = 250
		Me.txtLimitaciones.MultiLine = True
		Me.txtLimitaciones.TabIndex = 42
		Me.txtLimitaciones.Tag = "29###########6"
		Me.txtLimitaciones.AcceptsReturn = True
		Me.txtLimitaciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLimitaciones.BackColor = System.Drawing.SystemColors.Window
		Me.txtLimitaciones.CausesValidation = True
		Me.txtLimitaciones.Enabled = True
		Me.txtLimitaciones.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLimitaciones.HideSelection = True
		Me.txtLimitaciones.ReadOnly = False
		Me.txtLimitaciones.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLimitaciones.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLimitaciones.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLimitaciones.TabStop = True
		Me.txtLimitaciones.Visible = True
		Me.txtLimitaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtLimitaciones.Name = "txtLimitaciones"
		Me.txtTipoPrestacion.AutoSize = False
		Me.txtTipoPrestacion.BackColor = System.Drawing.Color.White
		Me.txtTipoPrestacion.Size = New System.Drawing.Size(494, 19)
		Me.txtTipoPrestacion.Location = New System.Drawing.Point(274, 6)
		Me.txtTipoPrestacion.TabIndex = 39
		Me.txtTipoPrestacion.Tag = "26###########6"
		Me.txtTipoPrestacion.AcceptsReturn = True
		Me.txtTipoPrestacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoPrestacion.CausesValidation = True
		Me.txtTipoPrestacion.Enabled = True
		Me.txtTipoPrestacion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoPrestacion.HideSelection = True
		Me.txtTipoPrestacion.ReadOnly = False
		Me.txtTipoPrestacion.Maxlength = 0
		Me.txtTipoPrestacion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoPrestacion.MultiLine = False
		Me.txtTipoPrestacion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoPrestacion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoPrestacion.TabStop = True
		Me.txtTipoPrestacion.Visible = True
		Me.txtTipoPrestacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoPrestacion.Name = "txtTipoPrestacion"
		chkPrestacionEconomica.OcxState = CType(resources.GetObject("chkPrestacionEconomica.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkPrestacionEconomica.Size = New System.Drawing.Size(127, 19)
		Me.chkPrestacionEconomica.Location = New System.Drawing.Point(8, 6)
		Me.chkPrestacionEconomica.TabIndex = 38
		Me.chkPrestacionEconomica.Name = "chkPrestacionEconomica"
		chkInscripcionOtg.OcxState = CType(resources.GetObject("chkInscripcionOtg.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkInscripcionOtg.Size = New System.Drawing.Size(127, 19)
		Me.chkInscripcionOtg.Location = New System.Drawing.Point(8, 30)
		Me.chkInscripcionOtg.TabIndex = 40
		Me.chkInscripcionOtg.Name = "chkInscripcionOtg"
		txtFechaOtg.OcxState = CType(resources.GetObject("txtFechaOtg.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaOtg.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaOtg.Location = New System.Drawing.Point(274, 30)
		Me.txtFechaOtg.TabIndex = 41
		Me.txtFechaOtg.Name = "txtFechaOtg"
		cmbTransporte.OcxState = CType(resources.GetObject("cmbTransporte.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbTransporte.Size = New System.Drawing.Size(145, 19)
		Me.cmbTransporte.Location = New System.Drawing.Point(146, 158)
		Me.cmbTransporte.TabIndex = 44
		Me.cmbTransporte.Name = "cmbTransporte"
		cmbValoracionSoc.OcxState = CType(resources.GetObject("cmbValoracionSoc.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbValoracionSoc.Size = New System.Drawing.Size(145, 19)
		Me.cmbValoracionSoc.Location = New System.Drawing.Point(146, 182)
		Me.cmbValoracionSoc.TabIndex = 45
		Me.cmbValoracionSoc.Name = "cmbValoracionSoc"
		Me.lbl32.Text = "Valoraci�n SOC"
		Me.lbl32.Size = New System.Drawing.Size(111, 15)
		Me.lbl32.Location = New System.Drawing.Point(8, 186)
		Me.lbl32.TabIndex = 144
		Me.lbl32.Tag = "32"
		Me.lbl32.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl32.BackColor = System.Drawing.SystemColors.Control
		Me.lbl32.Enabled = True
		Me.lbl32.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl32.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl32.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl32.UseMnemonic = True
		Me.lbl32.Visible = True
		Me.lbl32.AutoSize = False
		Me.lbl32.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl32.Name = "lbl32"
		Me.lbl31.Text = "Transporte, desplazamiento"
		Me.lbl31.Size = New System.Drawing.Size(137, 15)
		Me.lbl31.Location = New System.Drawing.Point(8, 162)
		Me.lbl31.TabIndex = 143
		Me.lbl31.Tag = "31"
		Me.lbl31.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl31.BackColor = System.Drawing.SystemColors.Control
		Me.lbl31.Enabled = True
		Me.lbl31.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl31.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl31.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl31.UseMnemonic = True
		Me.lbl31.Visible = True
		Me.lbl31.AutoSize = False
		Me.lbl31.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl31.Name = "lbl31"
		Me.lbl30.Text = "Condicionantes del entorno, con quien vive?"
		Me.lbl30.Size = New System.Drawing.Size(211, 15)
		Me.lbl30.Location = New System.Drawing.Point(8, 138)
		Me.lbl30.TabIndex = 142
		Me.lbl30.Tag = "30"
		Me.lbl30.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl30.BackColor = System.Drawing.SystemColors.Control
		Me.lbl30.Enabled = True
		Me.lbl30.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl30.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl30.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl30.UseMnemonic = True
		Me.lbl30.Visible = True
		Me.lbl30.AutoSize = False
		Me.lbl30.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl30.Name = "lbl30"
		Me.lbl29.Text = "Limitaciones para desarrollar determinadas tareas"
		Me.lbl29.Size = New System.Drawing.Size(279, 15)
		Me.lbl29.Location = New System.Drawing.Point(8, 58)
		Me.lbl29.TabIndex = 141
		Me.lbl29.Tag = "29"
		Me.lbl29.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl29.BackColor = System.Drawing.SystemColors.Control
		Me.lbl29.Enabled = True
		Me.lbl29.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl29.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl29.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl29.UseMnemonic = True
		Me.lbl29.Visible = True
		Me.lbl29.AutoSize = False
		Me.lbl29.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl29.Name = "lbl29"
		Me.lbl28.Text = "Fecha"
		Me.lbl28.Size = New System.Drawing.Size(111, 15)
		Me.lbl28.Location = New System.Drawing.Point(160, 34)
		Me.lbl28.TabIndex = 140
		Me.lbl28.Tag = "28"
		Me.lbl28.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl28.BackColor = System.Drawing.SystemColors.Control
		Me.lbl28.Enabled = True
		Me.lbl28.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl28.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl28.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl28.UseMnemonic = True
		Me.lbl28.Visible = True
		Me.lbl28.AutoSize = False
		Me.lbl28.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl28.Name = "lbl28"
		Me.lbl26.Text = "Tipo prestaci�n"
		Me.lbl26.Size = New System.Drawing.Size(111, 15)
		Me.lbl26.Location = New System.Drawing.Point(160, 10)
		Me.lbl26.TabIndex = 139
		Me.lbl26.Tag = "26"
		Me.lbl26.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl26.BackColor = System.Drawing.SystemColors.Control
		Me.lbl26.Enabled = True
		Me.lbl26.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl26.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl26.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl26.UseMnemonic = True
		Me.lbl26.Visible = True
		Me.lbl26.AutoSize = False
		Me.lbl26.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl26.Name = "lbl26"
		TabControlPage1.OcxState = CType(resources.GetObject("TabControlPage1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage1.Size = New System.Drawing.Size(883, 459)
		Me.TabControlPage1.Location = New System.Drawing.Point(2, 22)
		Me.TabControlPage1.TabIndex = 111
		Me.TabControlPage1.Name = "TabControlPage1"
		XPFrame302.OcxState = CType(resources.GetObject("XPFrame302.OcxState"), System.Windows.Forms.AxHost.State)
		Me.XPFrame302.Size = New System.Drawing.Size(867, 201)
		Me.XPFrame302.Location = New System.Drawing.Point(10, 226)
		Me.XPFrame302.Name = "XPFrame302"
		Me.Text8.AutoSize = False
		Me.Text8.BackColor = System.Drawing.Color.White
		Me.Text8.Enabled = False
		Me.Text8.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text8.Size = New System.Drawing.Size(300, 21)
		Me.Text8.Location = New System.Drawing.Point(148, 98)
		Me.Text8.TabIndex = 27
		Me.Text8.Tag = "^102"
		Me.Text8.AcceptsReturn = True
		Me.Text8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text8.CausesValidation = True
		Me.Text8.HideSelection = True
		Me.Text8.ReadOnly = False
		Me.Text8.Maxlength = 0
		Me.Text8.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text8.MultiLine = False
		Me.Text8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text8.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text8.TabStop = True
		Me.Text8.Visible = True
		Me.Text8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text8.Name = "Text8"
		Me.txtIdoneidadProgramaN.AutoSize = False
		Me.txtIdoneidadProgramaN.Size = New System.Drawing.Size(21, 21)
		Me.txtIdoneidadProgramaN.Location = New System.Drawing.Point(124, 98)
		Me.txtIdoneidadProgramaN.Maxlength = 2
		Me.txtIdoneidadProgramaN.TabIndex = 26
		Me.txtIdoneidadProgramaN.Tag = "102"
		Me.txtIdoneidadProgramaN.AcceptsReturn = True
		Me.txtIdoneidadProgramaN.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtIdoneidadProgramaN.BackColor = System.Drawing.SystemColors.Window
		Me.txtIdoneidadProgramaN.CausesValidation = True
		Me.txtIdoneidadProgramaN.Enabled = True
		Me.txtIdoneidadProgramaN.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtIdoneidadProgramaN.HideSelection = True
		Me.txtIdoneidadProgramaN.ReadOnly = False
		Me.txtIdoneidadProgramaN.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtIdoneidadProgramaN.MultiLine = False
		Me.txtIdoneidadProgramaN.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtIdoneidadProgramaN.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtIdoneidadProgramaN.TabStop = True
		Me.txtIdoneidadProgramaN.Visible = True
		Me.txtIdoneidadProgramaN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtIdoneidadProgramaN.Name = "txtIdoneidadProgramaN"
		Me.Text9.AutoSize = False
		Me.Text9.BackColor = System.Drawing.Color.White
		Me.Text9.Enabled = False
		Me.Text9.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text9.Size = New System.Drawing.Size(312, 21)
		Me.Text9.Location = New System.Drawing.Point(148, 172)
		Me.Text9.TabIndex = 33
		Me.Text9.Tag = "^98"
		Me.Text9.AcceptsReturn = True
		Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text9.CausesValidation = True
		Me.Text9.HideSelection = True
		Me.Text9.ReadOnly = False
		Me.Text9.Maxlength = 0
		Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text9.MultiLine = False
		Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text9.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text9.TabStop = True
		Me.Text9.Visible = True
		Me.Text9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text9.Name = "Text9"
		Me.txtPuntAtencio.AutoSize = False
		Me.txtPuntAtencio.Size = New System.Drawing.Size(21, 21)
		Me.txtPuntAtencio.Location = New System.Drawing.Point(124, 172)
		Me.txtPuntAtencio.Maxlength = 2
		Me.txtPuntAtencio.TabIndex = 32
		Me.txtPuntAtencio.Tag = "98"
		Me.txtPuntAtencio.AcceptsReturn = True
		Me.txtPuntAtencio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPuntAtencio.BackColor = System.Drawing.SystemColors.Window
		Me.txtPuntAtencio.CausesValidation = True
		Me.txtPuntAtencio.Enabled = True
		Me.txtPuntAtencio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPuntAtencio.HideSelection = True
		Me.txtPuntAtencio.ReadOnly = False
		Me.txtPuntAtencio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPuntAtencio.MultiLine = False
		Me.txtPuntAtencio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPuntAtencio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPuntAtencio.TabStop = True
		Me.txtPuntAtencio.Visible = True
		Me.txtPuntAtencio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPuntAtencio.Name = "txtPuntAtencio"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(254, 19)
		Me.Text5.Location = New System.Drawing.Point(604, 172)
		Me.Text5.TabIndex = 35
		Me.Text5.Tag = "^99"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtTecnico.AutoSize = False
		Me.txtTecnico.Size = New System.Drawing.Size(87, 19)
		Me.txtTecnico.Location = New System.Drawing.Point(514, 172)
		Me.txtTecnico.Maxlength = 8
		Me.txtTecnico.TabIndex = 34
		Me.txtTecnico.Tag = "99"
		Me.txtTecnico.AcceptsReturn = True
		Me.txtTecnico.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTecnico.BackColor = System.Drawing.SystemColors.Window
		Me.txtTecnico.CausesValidation = True
		Me.txtTecnico.Enabled = True
		Me.txtTecnico.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTecnico.HideSelection = True
		Me.txtTecnico.ReadOnly = False
		Me.txtTecnico.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTecnico.MultiLine = False
		Me.txtTecnico.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTecnico.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTecnico.TabStop = True
		Me.txtTecnico.Visible = True
		Me.txtTecnico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTecnico.Name = "txtTecnico"
		Me.txtValoracioPossibilitat.AutoSize = False
		Me.txtValoracioPossibilitat.Size = New System.Drawing.Size(735, 19)
		Me.txtValoracioPossibilitat.Location = New System.Drawing.Point(124, 122)
		Me.txtValoracioPossibilitat.Maxlength = 100
		Me.txtValoracioPossibilitat.TabIndex = 29
		Me.txtValoracioPossibilitat.Tag = "97"
		Me.txtValoracioPossibilitat.AcceptsReturn = True
		Me.txtValoracioPossibilitat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtValoracioPossibilitat.BackColor = System.Drawing.SystemColors.Window
		Me.txtValoracioPossibilitat.CausesValidation = True
		Me.txtValoracioPossibilitat.Enabled = True
		Me.txtValoracioPossibilitat.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtValoracioPossibilitat.HideSelection = True
		Me.txtValoracioPossibilitat.ReadOnly = False
		Me.txtValoracioPossibilitat.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtValoracioPossibilitat.MultiLine = False
		Me.txtValoracioPossibilitat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtValoracioPossibilitat.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtValoracioPossibilitat.TabStop = True
		Me.txtValoracioPossibilitat.Visible = True
		Me.txtValoracioPossibilitat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtValoracioPossibilitat.Name = "txtValoracioPossibilitat"
		Me.txtServicioInicio.AutoSize = False
		Me.txtServicioInicio.Size = New System.Drawing.Size(21, 21)
		Me.txtServicioInicio.Location = New System.Drawing.Point(290, 148)
		Me.txtServicioInicio.Maxlength = 2
		Me.txtServicioInicio.TabIndex = 31
		Me.txtServicioInicio.Tag = "16"
		Me.txtServicioInicio.AcceptsReturn = True
		Me.txtServicioInicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtServicioInicio.BackColor = System.Drawing.SystemColors.Window
		Me.txtServicioInicio.CausesValidation = True
		Me.txtServicioInicio.Enabled = True
		Me.txtServicioInicio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtServicioInicio.HideSelection = True
		Me.txtServicioInicio.ReadOnly = False
		Me.txtServicioInicio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtServicioInicio.MultiLine = False
		Me.txtServicioInicio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtServicioInicio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtServicioInicio.TabStop = True
		Me.txtServicioInicio.Visible = True
		Me.txtServicioInicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtServicioInicio.Name = "txtServicioInicio"
		Me.Text16.AutoSize = False
		Me.Text16.BackColor = System.Drawing.Color.White
		Me.Text16.Enabled = False
		Me.Text16.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text16.Size = New System.Drawing.Size(342, 21)
		Me.Text16.Location = New System.Drawing.Point(314, 148)
		Me.Text16.TabIndex = 206
		Me.Text16.Tag = "^16"
		Me.Text16.AcceptsReturn = True
		Me.Text16.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text16.CausesValidation = True
		Me.Text16.HideSelection = True
		Me.Text16.ReadOnly = False
		Me.Text16.Maxlength = 0
		Me.Text16.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text16.MultiLine = False
		Me.Text16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text16.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text16.TabStop = True
		Me.Text16.Visible = True
		Me.Text16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text16.Name = "Text16"
		Me.txtDemandaInicial.AutoSize = False
		Me.txtDemandaInicial.Size = New System.Drawing.Size(735, 39)
		Me.txtDemandaInicial.Location = New System.Drawing.Point(124, 12)
		Me.txtDemandaInicial.Maxlength = 250
		Me.txtDemandaInicial.MultiLine = True
		Me.txtDemandaInicial.TabIndex = 24
		Me.txtDemandaInicial.Tag = "19"
		Me.txtDemandaInicial.AcceptsReturn = True
		Me.txtDemandaInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDemandaInicial.BackColor = System.Drawing.SystemColors.Window
		Me.txtDemandaInicial.CausesValidation = True
		Me.txtDemandaInicial.Enabled = True
		Me.txtDemandaInicial.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDemandaInicial.HideSelection = True
		Me.txtDemandaInicial.ReadOnly = False
		Me.txtDemandaInicial.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDemandaInicial.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDemandaInicial.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDemandaInicial.TabStop = True
		Me.txtDemandaInicial.Visible = True
		Me.txtDemandaInicial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDemandaInicial.Name = "txtDemandaInicial"
		Me.txtObservaciones.AutoSize = False
		Me.txtObservaciones.Size = New System.Drawing.Size(735, 37)
		Me.txtObservaciones.Location = New System.Drawing.Point(124, 56)
		Me.txtObservaciones.Maxlength = 250
		Me.txtObservaciones.MultiLine = True
		Me.txtObservaciones.TabIndex = 25
		Me.txtObservaciones.Tag = "20"
		Me.txtObservaciones.AcceptsReturn = True
		Me.txtObservaciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservaciones.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservaciones.CausesValidation = True
		Me.txtObservaciones.Enabled = True
		Me.txtObservaciones.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservaciones.HideSelection = True
		Me.txtObservaciones.ReadOnly = False
		Me.txtObservaciones.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservaciones.TabStop = True
		Me.txtObservaciones.Visible = True
		Me.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservaciones.Name = "txtObservaciones"
		Me.txtIdoneidadPrograma.AutoSize = False
		Me.txtIdoneidadPrograma.Size = New System.Drawing.Size(337, 19)
		Me.txtIdoneidadPrograma.Location = New System.Drawing.Point(522, 98)
		Me.txtIdoneidadPrograma.Maxlength = 100
		Me.txtIdoneidadPrograma.TabIndex = 28
		Me.txtIdoneidadPrograma.Tag = "85"
		Me.txtIdoneidadPrograma.AcceptsReturn = True
		Me.txtIdoneidadPrograma.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtIdoneidadPrograma.BackColor = System.Drawing.SystemColors.Window
		Me.txtIdoneidadPrograma.CausesValidation = True
		Me.txtIdoneidadPrograma.Enabled = True
		Me.txtIdoneidadPrograma.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtIdoneidadPrograma.HideSelection = True
		Me.txtIdoneidadPrograma.ReadOnly = False
		Me.txtIdoneidadPrograma.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtIdoneidadPrograma.MultiLine = False
		Me.txtIdoneidadPrograma.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtIdoneidadPrograma.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtIdoneidadPrograma.TabStop = True
		Me.txtIdoneidadPrograma.Visible = True
		Me.txtIdoneidadPrograma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtIdoneidadPrograma.Name = "txtIdoneidadPrograma"
		txtFechaInicio.OcxState = CType(resources.GetObject("txtFechaInicio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaInicio.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaInicio.Location = New System.Drawing.Point(124, 148)
		Me.txtFechaInicio.TabIndex = 30
		Me.txtFechaInicio.Name = "txtFechaInicio"
		Me.Label21.Text = "Idoneitat a programa"
		Me.Label21.Size = New System.Drawing.Size(111, 15)
		Me.Label21.Location = New System.Drawing.Point(10, 102)
		Me.Label21.TabIndex = 216
		Me.Label21.Tag = "102"
		Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label21.BackColor = System.Drawing.SystemColors.Control
		Me.Label21.Enabled = True
		Me.Label21.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label21.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label21.UseMnemonic = True
		Me.Label21.Visible = True
		Me.Label21.AutoSize = False
		Me.Label21.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label21.Name = "Label21"
		Me.Label19.Text = "Punt Atenci�"
		Me.Label19.Size = New System.Drawing.Size(111, 15)
		Me.Label19.Location = New System.Drawing.Point(10, 176)
		Me.Label19.TabIndex = 214
		Me.Label19.Tag = "98"
		Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label19.BackColor = System.Drawing.SystemColors.Control
		Me.Label19.Enabled = True
		Me.Label19.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label19.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label19.UseMnemonic = True
		Me.Label19.Visible = True
		Me.Label19.AutoSize = False
		Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label19.Name = "Label19"
		Me.Label11.Text = "T�cnico"
		Me.Label11.Size = New System.Drawing.Size(111, 15)
		Me.Label11.Location = New System.Drawing.Point(464, 176)
		Me.Label11.TabIndex = 213
		Me.Label11.Tag = "99"
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label11.BackColor = System.Drawing.SystemColors.Control
		Me.Label11.Enabled = True
		Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label11.UseMnemonic = True
		Me.Label11.Visible = True
		Me.Label11.AutoSize = False
		Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label11.Name = "Label11"
		Me.Label10.Text = "Valoraci� possibilitat inserci�"
		Me.Label10.Size = New System.Drawing.Size(111, 27)
		Me.Label10.Location = New System.Drawing.Point(10, 122)
		Me.Label10.TabIndex = 212
		Me.Label10.Tag = "97"
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label10.BackColor = System.Drawing.SystemColors.Control
		Me.Label10.Enabled = True
		Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label10.UseMnemonic = True
		Me.Label10.Visible = True
		Me.Label10.AutoSize = False
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label10.Name = "Label10"
		Me.lbl15.Text = "Fecha inicio"
		Me.lbl15.Size = New System.Drawing.Size(111, 15)
		Me.lbl15.Location = New System.Drawing.Point(10, 152)
		Me.lbl15.TabIndex = 211
		Me.lbl15.Tag = "15"
		Me.lbl15.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl15.BackColor = System.Drawing.SystemColors.Control
		Me.lbl15.Enabled = True
		Me.lbl15.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl15.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl15.UseMnemonic = True
		Me.lbl15.Visible = True
		Me.lbl15.AutoSize = False
		Me.lbl15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl15.Name = "lbl15"
		Me.lbl16.Text = "Servicio inicio"
		Me.lbl16.Size = New System.Drawing.Size(111, 15)
		Me.lbl16.Location = New System.Drawing.Point(218, 152)
		Me.lbl16.TabIndex = 210
		Me.lbl16.Tag = "16"
		Me.lbl16.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl16.BackColor = System.Drawing.SystemColors.Control
		Me.lbl16.Enabled = True
		Me.lbl16.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl16.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl16.UseMnemonic = True
		Me.lbl16.Visible = True
		Me.lbl16.AutoSize = False
		Me.lbl16.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl16.Name = "lbl16"
		Me.lbl19.Text = "Demanda inicial"
		Me.lbl19.Size = New System.Drawing.Size(111, 15)
		Me.lbl19.Location = New System.Drawing.Point(10, 16)
		Me.lbl19.TabIndex = 209
		Me.lbl19.Tag = "19"
		Me.lbl19.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl19.BackColor = System.Drawing.SystemColors.Control
		Me.lbl19.Enabled = True
		Me.lbl19.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl19.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl19.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl19.UseMnemonic = True
		Me.lbl19.Visible = True
		Me.lbl19.AutoSize = False
		Me.lbl19.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl19.Name = "lbl19"
		Me.lbl20.Text = "Observaciones"
		Me.lbl20.Size = New System.Drawing.Size(111, 15)
		Me.lbl20.Location = New System.Drawing.Point(10, 58)
		Me.lbl20.TabIndex = 208
		Me.lbl20.Tag = "20"
		Me.lbl20.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl20.BackColor = System.Drawing.SystemColors.Control
		Me.lbl20.Enabled = True
		Me.lbl20.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl20.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl20.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl20.UseMnemonic = True
		Me.lbl20.Visible = True
		Me.lbl20.AutoSize = False
		Me.lbl20.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl20.Name = "lbl20"
		Me.Label9.Text = "Idoneidad a programa"
		Me.Label9.Size = New System.Drawing.Size(83, 15)
		Me.Label9.Location = New System.Drawing.Point(458, 100)
		Me.Label9.TabIndex = 207
		Me.Label9.Tag = "85"
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label9.BackColor = System.Drawing.SystemColors.Control
		Me.Label9.Enabled = True
		Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.UseMnemonic = True
		Me.Label9.Visible = True
		Me.Label9.AutoSize = False
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.Name = "Label9"
		XPFrame301.OcxState = CType(resources.GetObject("XPFrame301.OcxState"), System.Windows.Forms.AxHost.State)
		Me.XPFrame301.Size = New System.Drawing.Size(111, 93)
		Me.XPFrame301.Location = New System.Drawing.Point(772, 6)
		Me.XPFrame301.Name = "XPFrame301"
		OptRecollida.OcxState = CType(resources.GetObject("OptRecollida.OcxState"), System.Windows.Forms.AxHost.State)
		Me.OptRecollida.Size = New System.Drawing.Size(81, 15)
		Me.OptRecollida.Location = New System.Drawing.Point(6, 8)
		Me.OptRecollida.TabIndex = 201
		Me.OptRecollida.Name = "OptRecollida"
		cmdImprimir.OcxState = CType(resources.GetObject("cmdImprimir.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdImprimir.Size = New System.Drawing.Size(79, 35)
		Me.cmdImprimir.Location = New System.Drawing.Point(16, 46)
		Me.cmdImprimir.TabIndex = 200
		Me.cmdImprimir.Name = "cmdImprimir"
		OptProfessional.OcxState = CType(resources.GetObject("OptProfessional.OcxState"), System.Windows.Forms.AxHost.State)
		Me.OptProfessional.Size = New System.Drawing.Size(99, 15)
		Me.OptProfessional.Location = New System.Drawing.Point(6, 26)
		Me.OptProfessional.TabIndex = 202
		Me.OptProfessional.Name = "OptProfessional"
		Me.txtPoblacionNacimiento.AutoSize = False
		Me.txtPoblacionNacimiento.Size = New System.Drawing.Size(62, 19)
		Me.txtPoblacionNacimiento.Location = New System.Drawing.Point(122, 152)
		Me.txtPoblacionNacimiento.Maxlength = 6
		Me.txtPoblacionNacimiento.TabIndex = 17
		Me.txtPoblacionNacimiento.Tag = "75"
		Me.txtPoblacionNacimiento.AcceptsReturn = True
		Me.txtPoblacionNacimiento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPoblacionNacimiento.BackColor = System.Drawing.SystemColors.Window
		Me.txtPoblacionNacimiento.CausesValidation = True
		Me.txtPoblacionNacimiento.Enabled = True
		Me.txtPoblacionNacimiento.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPoblacionNacimiento.HideSelection = True
		Me.txtPoblacionNacimiento.ReadOnly = False
		Me.txtPoblacionNacimiento.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPoblacionNacimiento.MultiLine = False
		Me.txtPoblacionNacimiento.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPoblacionNacimiento.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPoblacionNacimiento.TabStop = True
		Me.txtPoblacionNacimiento.Visible = True
		Me.txtPoblacionNacimiento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPoblacionNacimiento.Name = "txtPoblacionNacimiento"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(259, 19)
		Me.Text1.Location = New System.Drawing.Point(187, 152)
		Me.Text1.TabIndex = 194
		Me.Text1.Tag = "^75"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtDiagnostico.AutoSize = False
		Me.txtDiagnostico.Size = New System.Drawing.Size(465, 19)
		Me.txtDiagnostico.Location = New System.Drawing.Point(408, 200)
		Me.txtDiagnostico.Maxlength = 100
		Me.txtDiagnostico.TabIndex = 23
		Me.txtDiagnostico.Tag = "24"
		Me.txtDiagnostico.AcceptsReturn = True
		Me.txtDiagnostico.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDiagnostico.BackColor = System.Drawing.SystemColors.Window
		Me.txtDiagnostico.CausesValidation = True
		Me.txtDiagnostico.Enabled = True
		Me.txtDiagnostico.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDiagnostico.HideSelection = True
		Me.txtDiagnostico.ReadOnly = False
		Me.txtDiagnostico.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDiagnostico.MultiLine = False
		Me.txtDiagnostico.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDiagnostico.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDiagnostico.TabStop = True
		Me.txtDiagnostico.Visible = True
		Me.txtDiagnostico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDiagnostico.Name = "txtDiagnostico"
		Me.txtGradoDiscapacidad.AutoSize = False
		Me.txtGradoDiscapacidad.Size = New System.Drawing.Size(31, 19)
		Me.txtGradoDiscapacidad.Location = New System.Drawing.Point(304, 200)
		Me.txtGradoDiscapacidad.Maxlength = 3
		Me.txtGradoDiscapacidad.TabIndex = 22
		Me.txtGradoDiscapacidad.Tag = "23"
		Me.txtGradoDiscapacidad.AcceptsReturn = True
		Me.txtGradoDiscapacidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGradoDiscapacidad.BackColor = System.Drawing.SystemColors.Window
		Me.txtGradoDiscapacidad.CausesValidation = True
		Me.txtGradoDiscapacidad.Enabled = True
		Me.txtGradoDiscapacidad.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGradoDiscapacidad.HideSelection = True
		Me.txtGradoDiscapacidad.ReadOnly = False
		Me.txtGradoDiscapacidad.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGradoDiscapacidad.MultiLine = False
		Me.txtGradoDiscapacidad.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGradoDiscapacidad.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGradoDiscapacidad.TabStop = True
		Me.txtGradoDiscapacidad.Visible = True
		Me.txtGradoDiscapacidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtGradoDiscapacidad.Name = "txtGradoDiscapacidad"
		Me.Text21.AutoSize = False
		Me.Text21.BackColor = System.Drawing.Color.White
		Me.Text21.Enabled = False
		Me.Text21.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text21.Size = New System.Drawing.Size(258, 19)
		Me.Text21.Location = New System.Drawing.Point(146, 176)
		Me.Text21.TabIndex = 136
		Me.Text21.Tag = "^21"
		Me.Text21.AcceptsReturn = True
		Me.Text21.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text21.CausesValidation = True
		Me.Text21.HideSelection = True
		Me.Text21.ReadOnly = False
		Me.Text21.Maxlength = 0
		Me.Text21.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text21.MultiLine = False
		Me.Text21.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text21.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text21.TabStop = True
		Me.Text21.Visible = True
		Me.Text21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text21.Name = "Text21"
		Me.txtColectivo.AutoSize = False
		Me.txtColectivo.Size = New System.Drawing.Size(21, 19)
		Me.txtColectivo.Location = New System.Drawing.Point(122, 176)
		Me.txtColectivo.Maxlength = 2
		Me.txtColectivo.TabIndex = 20
		Me.txtColectivo.Tag = "21"
		Me.txtColectivo.AcceptsReturn = True
		Me.txtColectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtColectivo.BackColor = System.Drawing.SystemColors.Window
		Me.txtColectivo.CausesValidation = True
		Me.txtColectivo.Enabled = True
		Me.txtColectivo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtColectivo.HideSelection = True
		Me.txtColectivo.ReadOnly = False
		Me.txtColectivo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtColectivo.MultiLine = False
		Me.txtColectivo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtColectivo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtColectivo.TabStop = True
		Me.txtColectivo.Visible = True
		Me.txtColectivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtColectivo.Name = "txtColectivo"
		Me.Text18.AutoSize = False
		Me.Text18.BackColor = System.Drawing.Color.White
		Me.Text18.Enabled = False
		Me.Text18.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text18.Size = New System.Drawing.Size(342, 21)
		Me.Text18.Location = New System.Drawing.Point(312, 432)
		Me.Text18.TabIndex = 134
		Me.Text18.Tag = "^18"
		Me.Text18.AcceptsReturn = True
		Me.Text18.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text18.CausesValidation = True
		Me.Text18.HideSelection = True
		Me.Text18.ReadOnly = False
		Me.Text18.Maxlength = 0
		Me.Text18.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text18.MultiLine = False
		Me.Text18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text18.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text18.TabStop = True
		Me.Text18.Visible = True
		Me.Text18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text18.Name = "Text18"
		Me.txtMotivoBaja.AutoSize = False
		Me.txtMotivoBaja.Size = New System.Drawing.Size(21, 21)
		Me.txtMotivoBaja.Location = New System.Drawing.Point(288, 432)
		Me.txtMotivoBaja.Maxlength = 2
		Me.txtMotivoBaja.TabIndex = 37
		Me.txtMotivoBaja.Tag = "18"
		Me.txtMotivoBaja.AcceptsReturn = True
		Me.txtMotivoBaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMotivoBaja.BackColor = System.Drawing.SystemColors.Window
		Me.txtMotivoBaja.CausesValidation = True
		Me.txtMotivoBaja.Enabled = True
		Me.txtMotivoBaja.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMotivoBaja.HideSelection = True
		Me.txtMotivoBaja.ReadOnly = False
		Me.txtMotivoBaja.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMotivoBaja.MultiLine = False
		Me.txtMotivoBaja.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMotivoBaja.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMotivoBaja.TabStop = True
		Me.txtMotivoBaja.Visible = True
		Me.txtMotivoBaja.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMotivoBaja.Name = "txtMotivoBaja"
		Me.txtEmail.AutoSize = False
		Me.txtEmail.Size = New System.Drawing.Size(227, 19)
		Me.txtEmail.Location = New System.Drawing.Point(646, 152)
		Me.txtEmail.Maxlength = 50
		Me.txtEmail.TabIndex = 19
		Me.txtEmail.Tag = "14"
		Me.txtEmail.AcceptsReturn = True
		Me.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmail.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmail.CausesValidation = True
		Me.txtEmail.Enabled = True
		Me.txtEmail.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmail.HideSelection = True
		Me.txtEmail.ReadOnly = False
		Me.txtEmail.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmail.MultiLine = False
		Me.txtEmail.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmail.TabStop = True
		Me.txtEmail.Visible = True
		Me.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmail.Name = "txtEmail"
		Me.txtCodigoPostal.AutoSize = False
		Me.txtCodigoPostal.Size = New System.Drawing.Size(77, 19)
		Me.txtCodigoPostal.Location = New System.Drawing.Point(646, 128)
		Me.txtCodigoPostal.Maxlength = 10
		Me.txtCodigoPostal.TabIndex = 18
		Me.txtCodigoPostal.Tag = "7"
		Me.txtCodigoPostal.AcceptsReturn = True
		Me.txtCodigoPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigoPostal.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigoPostal.CausesValidation = True
		Me.txtCodigoPostal.Enabled = True
		Me.txtCodigoPostal.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigoPostal.HideSelection = True
		Me.txtCodigoPostal.ReadOnly = False
		Me.txtCodigoPostal.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigoPostal.MultiLine = False
		Me.txtCodigoPostal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigoPostal.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigoPostal.TabStop = True
		Me.txtCodigoPostal.Visible = True
		Me.txtCodigoPostal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigoPostal.Name = "txtCodigoPostal"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(259, 19)
		Me.Text6.Location = New System.Drawing.Point(187, 128)
		Me.Text6.TabIndex = 129
		Me.Text6.Tag = "^6"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtPoblacion.AutoSize = False
		Me.txtPoblacion.Size = New System.Drawing.Size(62, 19)
		Me.txtPoblacion.Location = New System.Drawing.Point(122, 128)
		Me.txtPoblacion.Maxlength = 6
		Me.txtPoblacion.TabIndex = 16
		Me.txtPoblacion.Tag = "6"
		Me.txtPoblacion.AcceptsReturn = True
		Me.txtPoblacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPoblacion.BackColor = System.Drawing.SystemColors.Window
		Me.txtPoblacion.CausesValidation = True
		Me.txtPoblacion.Enabled = True
		Me.txtPoblacion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPoblacion.HideSelection = True
		Me.txtPoblacion.ReadOnly = False
		Me.txtPoblacion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPoblacion.MultiLine = False
		Me.txtPoblacion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPoblacion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPoblacion.TabStop = True
		Me.txtPoblacion.Visible = True
		Me.txtPoblacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPoblacion.Name = "txtPoblacion"
		Me.txtTelefonoMovil.AutoSize = False
		Me.txtTelefonoMovil.Size = New System.Drawing.Size(121, 19)
		Me.txtTelefonoMovil.Location = New System.Drawing.Point(646, 80)
		Me.txtTelefonoMovil.Maxlength = 20
		Me.txtTelefonoMovil.TabIndex = 14
		Me.txtTelefonoMovil.Tag = "13"
		Me.txtTelefonoMovil.AcceptsReturn = True
		Me.txtTelefonoMovil.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTelefonoMovil.BackColor = System.Drawing.SystemColors.Window
		Me.txtTelefonoMovil.CausesValidation = True
		Me.txtTelefonoMovil.Enabled = True
		Me.txtTelefonoMovil.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTelefonoMovil.HideSelection = True
		Me.txtTelefonoMovil.ReadOnly = False
		Me.txtTelefonoMovil.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTelefonoMovil.MultiLine = False
		Me.txtTelefonoMovil.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTelefonoMovil.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTelefonoMovil.TabStop = True
		Me.txtTelefonoMovil.Visible = True
		Me.txtTelefonoMovil.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTelefonoMovil.Name = "txtTelefonoMovil"
		Me.txtTelefono.AutoSize = False
		Me.txtTelefono.Size = New System.Drawing.Size(121, 19)
		Me.txtTelefono.Location = New System.Drawing.Point(646, 56)
		Me.txtTelefono.Maxlength = 20
		Me.txtTelefono.TabIndex = 12
		Me.txtTelefono.Tag = "12"
		Me.txtTelefono.AcceptsReturn = True
		Me.txtTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTelefono.BackColor = System.Drawing.SystemColors.Window
		Me.txtTelefono.CausesValidation = True
		Me.txtTelefono.Enabled = True
		Me.txtTelefono.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTelefono.HideSelection = True
		Me.txtTelefono.ReadOnly = False
		Me.txtTelefono.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTelefono.MultiLine = False
		Me.txtTelefono.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTelefono.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTelefono.TabStop = True
		Me.txtTelefono.Visible = True
		Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTelefono.Name = "txtTelefono"
		Me.txtNIF.AutoSize = False
		Me.txtNIF.Size = New System.Drawing.Size(119, 19)
		Me.txtNIF.Location = New System.Drawing.Point(646, 8)
		Me.txtNIF.Maxlength = 15
		Me.txtNIF.TabIndex = 8
		Me.txtNIF.Tag = "8"
		Me.txtNIF.AcceptsReturn = True
		Me.txtNIF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNIF.BackColor = System.Drawing.SystemColors.Window
		Me.txtNIF.CausesValidation = True
		Me.txtNIF.Enabled = True
		Me.txtNIF.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNIF.HideSelection = True
		Me.txtNIF.ReadOnly = False
		Me.txtNIF.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNIF.MultiLine = False
		Me.txtNIF.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNIF.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNIF.TabStop = True
		Me.txtNIF.Visible = True
		Me.txtNIF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNIF.Name = "txtNIF"
		Me.txtDireccion.AutoSize = False
		Me.txtDireccion.Size = New System.Drawing.Size(645, 19)
		Me.txtDireccion.Location = New System.Drawing.Point(122, 104)
		Me.txtDireccion.Maxlength = 50
		Me.txtDireccion.TabIndex = 15
		Me.txtDireccion.Tag = "5"
		Me.txtDireccion.AcceptsReturn = True
		Me.txtDireccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDireccion.BackColor = System.Drawing.SystemColors.Window
		Me.txtDireccion.CausesValidation = True
		Me.txtDireccion.Enabled = True
		Me.txtDireccion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDireccion.HideSelection = True
		Me.txtDireccion.ReadOnly = False
		Me.txtDireccion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDireccion.MultiLine = False
		Me.txtDireccion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDireccion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDireccion.TabStop = True
		Me.txtDireccion.Visible = True
		Me.txtDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDireccion.Name = "txtDireccion"
		Me.txtSegundoApellido.AutoSize = False
		Me.txtSegundoApellido.Size = New System.Drawing.Size(227, 19)
		Me.txtSegundoApellido.Location = New System.Drawing.Point(122, 56)
		Me.txtSegundoApellido.Maxlength = 20
		Me.txtSegundoApellido.TabIndex = 11
		Me.txtSegundoApellido.Tag = "4"
		Me.txtSegundoApellido.AcceptsReturn = True
		Me.txtSegundoApellido.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSegundoApellido.BackColor = System.Drawing.SystemColors.Window
		Me.txtSegundoApellido.CausesValidation = True
		Me.txtSegundoApellido.Enabled = True
		Me.txtSegundoApellido.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSegundoApellido.HideSelection = True
		Me.txtSegundoApellido.ReadOnly = False
		Me.txtSegundoApellido.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSegundoApellido.MultiLine = False
		Me.txtSegundoApellido.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSegundoApellido.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSegundoApellido.TabStop = True
		Me.txtSegundoApellido.Visible = True
		Me.txtSegundoApellido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSegundoApellido.Name = "txtSegundoApellido"
		Me.txtPrimerApellido.AutoSize = False
		Me.txtPrimerApellido.Size = New System.Drawing.Size(227, 19)
		Me.txtPrimerApellido.Location = New System.Drawing.Point(122, 32)
		Me.txtPrimerApellido.Maxlength = 20
		Me.txtPrimerApellido.TabIndex = 9
		Me.txtPrimerApellido.Tag = "3"
		Me.txtPrimerApellido.AcceptsReturn = True
		Me.txtPrimerApellido.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPrimerApellido.BackColor = System.Drawing.SystemColors.Window
		Me.txtPrimerApellido.CausesValidation = True
		Me.txtPrimerApellido.Enabled = True
		Me.txtPrimerApellido.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPrimerApellido.HideSelection = True
		Me.txtPrimerApellido.ReadOnly = False
		Me.txtPrimerApellido.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPrimerApellido.MultiLine = False
		Me.txtPrimerApellido.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPrimerApellido.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPrimerApellido.TabStop = True
		Me.txtPrimerApellido.Visible = True
		Me.txtPrimerApellido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPrimerApellido.Name = "txtPrimerApellido"
		Me.txtNombre.AutoSize = False
		Me.txtNombre.Size = New System.Drawing.Size(227, 19)
		Me.txtNombre.Location = New System.Drawing.Point(122, 8)
		Me.txtNombre.Maxlength = 20
		Me.txtNombre.TabIndex = 7
		Me.txtNombre.Tag = "2"
		Me.txtNombre.AcceptsReturn = True
		Me.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNombre.BackColor = System.Drawing.SystemColors.Window
		Me.txtNombre.CausesValidation = True
		Me.txtNombre.Enabled = True
		Me.txtNombre.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNombre.HideSelection = True
		Me.txtNombre.ReadOnly = False
		Me.txtNombre.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNombre.MultiLine = False
		Me.txtNombre.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNombre.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNombre.TabStop = True
		Me.txtNombre.Visible = True
		Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNombre.Name = "txtNombre"
		txtFechaNacimiento.OcxState = CType(resources.GetObject("txtFechaNacimiento.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaNacimiento.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaNacimiento.Location = New System.Drawing.Point(646, 32)
		Me.txtFechaNacimiento.TabIndex = 10
		Me.txtFechaNacimiento.Name = "txtFechaNacimiento"
		cmbSexo.OcxState = CType(resources.GetObject("cmbSexo.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbSexo.Size = New System.Drawing.Size(101, 19)
		Me.cmbSexo.Location = New System.Drawing.Point(122, 80)
		Me.cmbSexo.TabIndex = 13
		Me.cmbSexo.Name = "cmbSexo"
		txtFechaFin.OcxState = CType(resources.GetObject("txtFechaFin.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaFin.Size = New System.Drawing.Size(89, 20)
		Me.txtFechaFin.Location = New System.Drawing.Point(122, 432)
		Me.txtFechaFin.TabIndex = 36
		Me.txtFechaFin.Name = "txtFechaFin"
		chkTieneCertificadoDiscap.OcxState = CType(resources.GetObject("chkTieneCertificadoDiscap.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkTieneCertificadoDiscap.Size = New System.Drawing.Size(167, 19)
		Me.chkTieneCertificadoDiscap.Location = New System.Drawing.Point(8, 200)
		Me.chkTieneCertificadoDiscap.TabIndex = 21
		Me.chkTieneCertificadoDiscap.Name = "chkTieneCertificadoDiscap"
		cmdDocumentacio.OcxState = CType(resources.GetObject("cmdDocumentacio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdDocumentacio.Size = New System.Drawing.Size(105, 35)
		Me.cmdDocumentacio.Location = New System.Drawing.Point(774, 102)
		Me.cmdDocumentacio.TabIndex = 204
		Me.cmdDocumentacio.Name = "cmdDocumentacio"
		Me.Label18.Text = "Poblaci�n"
		Me.Label18.Size = New System.Drawing.Size(111, 15)
		Me.Label18.Location = New System.Drawing.Point(8, 156)
		Me.Label18.TabIndex = 195
		Me.Label18.Tag = "75"
		Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label18.BackColor = System.Drawing.SystemColors.Control
		Me.Label18.Enabled = True
		Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label18.UseMnemonic = True
		Me.Label18.Visible = True
		Me.Label18.AutoSize = False
		Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label18.Name = "Label18"
		Me.lbl24.Text = "Diagn�stico"
		Me.lbl24.Size = New System.Drawing.Size(111, 15)
		Me.lbl24.Location = New System.Drawing.Point(344, 202)
		Me.lbl24.TabIndex = 138
		Me.lbl24.Tag = "24"
		Me.lbl24.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl24.BackColor = System.Drawing.SystemColors.Control
		Me.lbl24.Enabled = True
		Me.lbl24.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl24.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl24.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl24.UseMnemonic = True
		Me.lbl24.Visible = True
		Me.lbl24.AutoSize = False
		Me.lbl24.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl24.Name = "lbl24"
		Me.lbl23.Text = "Grado discapacidad"
		Me.lbl23.Size = New System.Drawing.Size(111, 15)
		Me.lbl23.Location = New System.Drawing.Point(190, 204)
		Me.lbl23.TabIndex = 137
		Me.lbl23.Tag = "23"
		Me.lbl23.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl23.BackColor = System.Drawing.SystemColors.Control
		Me.lbl23.Enabled = True
		Me.lbl23.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl23.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl23.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl23.UseMnemonic = True
		Me.lbl23.Visible = True
		Me.lbl23.AutoSize = False
		Me.lbl23.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl23.Name = "lbl23"
		Me.lbl21.Text = "Colectivo"
		Me.lbl21.Size = New System.Drawing.Size(111, 15)
		Me.lbl21.Location = New System.Drawing.Point(8, 180)
		Me.lbl21.TabIndex = 135
		Me.lbl21.Tag = "21"
		Me.lbl21.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl21.BackColor = System.Drawing.SystemColors.Control
		Me.lbl21.Enabled = True
		Me.lbl21.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl21.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl21.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl21.UseMnemonic = True
		Me.lbl21.Visible = True
		Me.lbl21.AutoSize = False
		Me.lbl21.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl21.Name = "lbl21"
		Me.lbl18.Text = "Motivo baja"
		Me.lbl18.Size = New System.Drawing.Size(111, 15)
		Me.lbl18.Location = New System.Drawing.Point(216, 436)
		Me.lbl18.TabIndex = 133
		Me.lbl18.Tag = "18"
		Me.lbl18.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl18.BackColor = System.Drawing.SystemColors.Control
		Me.lbl18.Enabled = True
		Me.lbl18.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl18.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl18.UseMnemonic = True
		Me.lbl18.Visible = True
		Me.lbl18.AutoSize = False
		Me.lbl18.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl18.Name = "lbl18"
		Me.lbl17.Text = "Fecha fin"
		Me.lbl17.Size = New System.Drawing.Size(111, 15)
		Me.lbl17.Location = New System.Drawing.Point(8, 436)
		Me.lbl17.TabIndex = 132
		Me.lbl17.Tag = "17"
		Me.lbl17.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl17.BackColor = System.Drawing.SystemColors.Control
		Me.lbl17.Enabled = True
		Me.lbl17.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl17.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl17.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl17.UseMnemonic = True
		Me.lbl17.Visible = True
		Me.lbl17.AutoSize = False
		Me.lbl17.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl17.Name = "lbl17"
		Me.lbl14.Text = "E-mail"
		Me.lbl14.Size = New System.Drawing.Size(53, 15)
		Me.lbl14.Location = New System.Drawing.Point(534, 156)
		Me.lbl14.TabIndex = 131
		Me.lbl14.Tag = "14"
		Me.lbl14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl14.BackColor = System.Drawing.SystemColors.Control
		Me.lbl14.Enabled = True
		Me.lbl14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl14.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl14.UseMnemonic = True
		Me.lbl14.Visible = True
		Me.lbl14.AutoSize = False
		Me.lbl14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl14.Name = "lbl14"
		Me.lbl7.Text = "Codigo postal"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(534, 132)
		Me.lbl7.TabIndex = 130
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl6.Text = "Poblaci�n"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(8, 132)
		Me.lbl6.TabIndex = 128
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl13.Text = "Tel�fono movil"
		Me.lbl13.Size = New System.Drawing.Size(111, 15)
		Me.lbl13.Location = New System.Drawing.Point(532, 84)
		Me.lbl13.TabIndex = 127
		Me.lbl13.Tag = "13"
		Me.lbl13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl13.BackColor = System.Drawing.SystemColors.Control
		Me.lbl13.Enabled = True
		Me.lbl13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl13.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl13.UseMnemonic = True
		Me.lbl13.Visible = True
		Me.lbl13.AutoSize = False
		Me.lbl13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl13.Name = "lbl13"
		Me.lbl12.Text = "Tel�fono"
		Me.lbl12.Size = New System.Drawing.Size(111, 15)
		Me.lbl12.Location = New System.Drawing.Point(532, 60)
		Me.lbl12.TabIndex = 126
		Me.lbl12.Tag = "12"
		Me.lbl12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl12.BackColor = System.Drawing.SystemColors.Control
		Me.lbl12.Enabled = True
		Me.lbl12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl12.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl12.UseMnemonic = True
		Me.lbl12.Visible = True
		Me.lbl12.AutoSize = False
		Me.lbl12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl12.Name = "lbl12"
		Me.lbl10.Text = "Sexo"
		Me.lbl10.Size = New System.Drawing.Size(111, 15)
		Me.lbl10.Location = New System.Drawing.Point(8, 84)
		Me.lbl10.TabIndex = 125
		Me.lbl10.Tag = "10"
		Me.lbl10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl10.BackColor = System.Drawing.SystemColors.Control
		Me.lbl10.Enabled = True
		Me.lbl10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl10.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl10.UseMnemonic = True
		Me.lbl10.Visible = True
		Me.lbl10.AutoSize = False
		Me.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl10.Name = "lbl10"
		Me.lbl11.Text = "Fecha nacimiento"
		Me.lbl11.Size = New System.Drawing.Size(111, 15)
		Me.lbl11.Location = New System.Drawing.Point(532, 36)
		Me.lbl11.TabIndex = 124
		Me.lbl11.Tag = "11"
		Me.lbl11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl11.BackColor = System.Drawing.SystemColors.Control
		Me.lbl11.Enabled = True
		Me.lbl11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl11.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl11.UseMnemonic = True
		Me.lbl11.Visible = True
		Me.lbl11.AutoSize = False
		Me.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl11.Name = "lbl11"
		Me.lbl8.Text = "CIF/NIF/NIE"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(532, 12)
		Me.lbl8.TabIndex = 123
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl5.Text = "Direcci�n"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(8, 108)
		Me.lbl5.TabIndex = 122
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl4.Text = "Segundo apellido"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(8, 60)
		Me.lbl4.TabIndex = 121
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl3.Text = "Primer apellido"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(8, 36)
		Me.lbl3.TabIndex = 120
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl2.Text = "Nombre"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(8, 12)
		Me.lbl2.TabIndex = 119
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		cmdGrauOcu.OcxState = CType(resources.GetObject("cmdGrauOcu.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGrauOcu.Size = New System.Drawing.Size(21, 21)
		Me.cmdGrauOcu.Location = New System.Drawing.Point(-3866, 132)
		Me.cmdGrauOcu.TabIndex = 88
		Me.cmdGrauOcu.Visible = False
		Me.cmdGrauOcu.Name = "cmdGrauOcu"
		cmdGencat.OcxState = CType(resources.GetObject("cmdGencat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGencat.Size = New System.Drawing.Size(21, 21)
		Me.cmdGencat.Location = New System.Drawing.Point(-3866, 156)
		Me.cmdGencat.TabIndex = 91
		Me.cmdGencat.Visible = False
		Me.cmdGencat.Name = "cmdGencat"
		cmdIcap.OcxState = CType(resources.GetObject("cmdIcap.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdIcap.Size = New System.Drawing.Size(21, 21)
		Me.cmdIcap.Location = New System.Drawing.Point(-3866, 180)
		Me.cmdIcap.TabIndex = 94
		Me.cmdIcap.Visible = False
		Me.cmdIcap.Name = "cmdIcap"
		cmdPerfilLab.OcxState = CType(resources.GetObject("cmdPerfilLab.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdPerfilLab.Size = New System.Drawing.Size(21, 21)
		Me.cmdPerfilLab.Location = New System.Drawing.Point(-3866, 204)
		Me.cmdPerfilLab.TabIndex = 97
		Me.cmdPerfilLab.Visible = False
		Me.cmdPerfilLab.Name = "cmdPerfilLab"
		chkGradoOcupabilidad.OcxState = CType(resources.GetObject("chkGradoOcupabilidad.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkGradoOcupabilidad.Size = New System.Drawing.Size(111, 19)
		Me.chkGradoOcupabilidad.Location = New System.Drawing.Point(-4656, 136)
		Me.chkGradoOcupabilidad.TabIndex = 86
		Me.chkGradoOcupabilidad.Visible = False
		Me.chkGradoOcupabilidad.Name = "chkGradoOcupabilidad"
		chkGencat.OcxState = CType(resources.GetObject("chkGencat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkGencat.Size = New System.Drawing.Size(111, 19)
		Me.chkGencat.Location = New System.Drawing.Point(-4656, 160)
		Me.chkGencat.TabIndex = 89
		Me.chkGencat.Visible = False
		Me.chkGencat.Name = "chkGencat"
		chkIcap.OcxState = CType(resources.GetObject("chkIcap.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkIcap.Size = New System.Drawing.Size(111, 19)
		Me.chkIcap.Location = New System.Drawing.Point(-4656, 182)
		Me.chkIcap.TabIndex = 92
		Me.chkIcap.Visible = False
		Me.chkIcap.Name = "chkIcap"
		chkPerfilLaboral.OcxState = CType(resources.GetObject("chkPerfilLaboral.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkPerfilLaboral.Size = New System.Drawing.Size(111, 19)
		Me.chkPerfilLaboral.Location = New System.Drawing.Point(-4656, 206)
		Me.chkPerfilLaboral.TabIndex = 95
		Me.chkPerfilLaboral.Visible = False
		Me.chkPerfilLaboral.Name = "chkPerfilLaboral"
		chkMotivacionLaboral.OcxState = CType(resources.GetObject("chkMotivacionLaboral.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkMotivacionLaboral.Size = New System.Drawing.Size(111, 19)
		Me.chkMotivacionLaboral.Location = New System.Drawing.Point(-4656, 228)
		Me.chkMotivacionLaboral.TabIndex = 98
		Me.chkMotivacionLaboral.Visible = False
		Me.chkMotivacionLaboral.Name = "chkMotivacionLaboral"
		cmdMotLaboral.OcxState = CType(resources.GetObject("cmdMotLaboral.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdMotLaboral.Size = New System.Drawing.Size(21, 21)
		Me.cmdMotLaboral.Location = New System.Drawing.Point(-3866, 228)
		Me.cmdMotLaboral.TabIndex = 100
		Me.cmdMotLaboral.Visible = False
		Me.cmdMotLaboral.Name = "cmdMotLaboral"
		GrdProgrames.OcxState = CType(resources.GetObject("GrdProgrames.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdProgrames.Size = New System.Drawing.Size(870, 437)
		Me.GrdProgrames.Location = New System.Drawing.Point(-4658, 28)
		Me.GrdProgrames.TabIndex = 199
		Me.GrdProgrames.Visible = False
		Me.GrdProgrames.Name = "GrdProgrames"
		GrdInsercions.OcxState = CType(resources.GetObject("GrdInsercions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdInsercions.Size = New System.Drawing.Size(870, 437)
		Me.GrdInsercions.Location = New System.Drawing.Point(-4658, 28)
		Me.GrdInsercions.TabIndex = 205
		Me.GrdInsercions.Visible = False
		Me.GrdInsercions.Name = "GrdInsercions"
		Me.Label20.Text = "Pefil Laboral"
		Me.Label20.Size = New System.Drawing.Size(111, 15)
		Me.Label20.Location = New System.Drawing.Point(-4656, 282)
		Me.Label20.TabIndex = 215
		Me.Label20.Tag = "101"
		Me.Label20.Visible = False
		Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label20.BackColor = System.Drawing.SystemColors.Control
		Me.Label20.Enabled = True
		Me.Label20.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label20.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label20.UseMnemonic = True
		Me.Label20.AutoSize = False
		Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label20.Name = "Label20"
		Me.lbl33.Text = "Grau Ocupalbilitat"
		Me.lbl33.Size = New System.Drawing.Size(111, 15)
		Me.lbl33.Location = New System.Drawing.Point(-4656, 258)
		Me.lbl33.TabIndex = 197
		Me.lbl33.Tag = "33"
		Me.lbl33.Visible = False
		Me.lbl33.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl33.BackColor = System.Drawing.SystemColors.Control
		Me.lbl33.Enabled = True
		Me.lbl33.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl33.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl33.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl33.UseMnemonic = True
		Me.lbl33.AutoSize = False
		Me.lbl33.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl33.Name = "lbl33"
		Me.lbl34.Text = "Valoraci�n t�cnica / observaciones"
		Me.lbl34.Size = New System.Drawing.Size(179, 15)
		Me.lbl34.Location = New System.Drawing.Point(-4652, 42)
		Me.lbl34.TabIndex = 196
		Me.lbl34.Tag = "34"
		Me.lbl34.Visible = False
		Me.lbl34.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl34.BackColor = System.Drawing.SystemColors.Control
		Me.lbl34.Enabled = True
		Me.lbl34.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl34.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl34.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl34.UseMnemonic = True
		Me.lbl34.AutoSize = False
		Me.lbl34.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl34.Name = "lbl34"
		Me.txtCodigoEntidad.AutoSize = False
		Me.txtCodigoEntidad.Size = New System.Drawing.Size(53, 19)
		Me.txtCodigoEntidad.Location = New System.Drawing.Point(474, 10)
		Me.txtCodigoEntidad.Maxlength = 40
		Me.txtCodigoEntidad.TabIndex = 108
		Me.txtCodigoEntidad.Tag = "126"
		Me.txtCodigoEntidad.AcceptsReturn = True
		Me.txtCodigoEntidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigoEntidad.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigoEntidad.CausesValidation = True
		Me.txtCodigoEntidad.Enabled = True
		Me.txtCodigoEntidad.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigoEntidad.HideSelection = True
		Me.txtCodigoEntidad.ReadOnly = False
		Me.txtCodigoEntidad.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigoEntidad.MultiLine = False
		Me.txtCodigoEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigoEntidad.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigoEntidad.TabStop = True
		Me.txtCodigoEntidad.Visible = True
		Me.txtCodigoEntidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigoEntidad.Name = "txtCodigoEntidad"
		Me.txtCodigo.AutoSize = False
		Me.txtCodigo.Size = New System.Drawing.Size(42, 19)
		Me.txtCodigo.Location = New System.Drawing.Point(58, 10)
		Me.txtCodigo.Maxlength = 4
		Me.txtCodigo.TabIndex = 0
		Me.txtCodigo.Tag = "*1"
		Me.txtCodigo.AcceptsReturn = True
		Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigo.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigo.CausesValidation = True
		Me.txtCodigo.Enabled = True
		Me.txtCodigo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigo.HideSelection = True
		Me.txtCodigo.ReadOnly = False
		Me.txtCodigo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigo.MultiLine = False
		Me.txtCodigo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigo.TabStop = True
		Me.txtCodigo.Visible = True
		Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigo.Name = "txtCodigo"
		cmbEstado.OcxState = CType(resources.GetObject("cmbEstado.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbEstado.Size = New System.Drawing.Size(121, 19)
		Me.cmbEstado.Location = New System.Drawing.Point(58, 34)
		Me.cmbEstado.TabIndex = 1
		Me.cmbEstado.Name = "cmbEstado"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(719, 561)
		Me.cmdAceptar.TabIndex = 101
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(809, 561)
		Me.cmdGuardar.TabIndex = 106
		Me.cmdGuardar.Name = "cmdGuardar"
		chkDisponibleParaOferta.OcxState = CType(resources.GetObject("chkDisponibleParaOferta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkDisponibleParaOferta.Size = New System.Drawing.Size(125, 19)
		Me.chkDisponibleParaOferta.Location = New System.Drawing.Point(180, 36)
		Me.chkDisponibleParaOferta.TabIndex = 2
		Me.chkDisponibleParaOferta.Name = "chkDisponibleParaOferta"
		cmdEntidad.OcxState = CType(resources.GetObject("cmdEntidad.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdEntidad.Size = New System.Drawing.Size(27, 27)
		Me.cmdEntidad.Location = New System.Drawing.Point(532, 6)
		Me.cmdEntidad.TabIndex = 172
		Me.cmdEntidad.Name = "cmdEntidad"
		chkActualmenteTrabajando.OcxState = CType(resources.GetObject("chkActualmenteTrabajando.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkActualmenteTrabajando.Size = New System.Drawing.Size(131, 19)
		Me.chkActualmenteTrabajando.Location = New System.Drawing.Point(422, 36)
		Me.chkActualmenteTrabajando.TabIndex = 3
		Me.chkActualmenteTrabajando.Name = "chkActualmenteTrabajando"
		GrdServeisAct.OcxState = CType(resources.GetObject("GrdServeisAct.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdServeisAct.Size = New System.Drawing.Size(280, 71)
		Me.GrdServeisAct.Location = New System.Drawing.Point(608, 4)
		Me.GrdServeisAct.TabIndex = 198
		Me.GrdServeisAct.Name = "GrdServeisAct"
		chkSusceptibleServeiAssis.OcxState = CType(resources.GetObject("chkSusceptibleServeiAssis.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkSusceptibleServeiAssis.Size = New System.Drawing.Size(199, 19)
		Me.chkSusceptibleServeiAssis.Location = New System.Drawing.Point(180, 58)
		Me.chkSusceptibleServeiAssis.TabIndex = 4
		Me.chkSusceptibleServeiAssis.Name = "chkSusceptibleServeiAssis"
		chkGarantiaJuvenil.OcxState = CType(resources.GetObject("chkGarantiaJuvenil.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkGarantiaJuvenil.Size = New System.Drawing.Size(99, 19)
		Me.chkGarantiaJuvenil.Location = New System.Drawing.Point(422, 58)
		Me.chkGarantiaJuvenil.TabIndex = 5
		Me.chkGarantiaJuvenil.Name = "chkGarantiaJuvenil"
		txtGarantiaJ.OcxState = CType(resources.GetObject("txtGarantiaJ.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtGarantiaJ.Size = New System.Drawing.Size(87, 19)
		Me.txtGarantiaJ.Location = New System.Drawing.Point(522, 56)
		Me.txtGarantiaJ.TabIndex = 6
		Me.txtGarantiaJ.Name = "txtGarantiaJ"
		Me.lblTreballant.ForeColor = System.Drawing.Color.FromARGB(0, 0, 192)
		Me.lblTreballant.Size = New System.Drawing.Size(109, 19)
		Me.lblTreballant.Location = New System.Drawing.Point(310, 34)
		Me.lblTreballant.TabIndex = 176
		Me.lblTreballant.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblTreballant.BackColor = System.Drawing.SystemColors.Control
		Me.lblTreballant.Enabled = True
		Me.lblTreballant.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblTreballant.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblTreballant.UseMnemonic = True
		Me.lblTreballant.Visible = True
		Me.lblTreballant.AutoSize = False
		Me.lblTreballant.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblTreballant.Name = "lblTreballant"
		Me.Label33.Text = "Entidad"
		Me.Label33.Size = New System.Drawing.Size(57, 15)
		Me.Label33.Location = New System.Drawing.Point(430, 14)
		Me.Label33.TabIndex = 109
		Me.Label33.Tag = "126"
		Me.Label33.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label33.BackColor = System.Drawing.SystemColors.Control
		Me.Label33.Enabled = True
		Me.Label33.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label33.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label33.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label33.UseMnemonic = True
		Me.Label33.Visible = True
		Me.Label33.AutoSize = False
		Me.Label33.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label33.Name = "Label33"
		Me.lbl1.Text = "Codigo"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(6, 14)
		Me.lbl1.TabIndex = 104
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl9.Text = "Estado"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(6, 38)
		Me.lbl9.TabIndex = 105
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 561)
		Me.lblLock.TabIndex = 107
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		CType(Me.txtGarantiaJ, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkGarantiaJuvenil, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkSusceptibleServeiAssis, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdServeisAct, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkActualmenteTrabajando, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdEntidad, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkDisponibleParaOferta, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbEstado, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdInsercions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdProgrames, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdMotLaboral, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkMotivacionLaboral, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkPerfilLaboral, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkIcap, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkGencat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkGradoOcupabilidad, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdPerfilLab, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdIcap, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGencat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGrauOcu, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdDocumentacio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkTieneCertificadoDiscap, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaFin, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbSexo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaNacimiento, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.XPFrame301, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.OptProfessional, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdImprimir, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.OptRecollida, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.XPFrame302, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaInicio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbValoracionSoc, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbTransporte, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaOtg, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkInscripcionOtg, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkPrestacionEconomica, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdFormacio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbCurri, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.grdHistLaboral, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkTieneExperienciaLab, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkPowerpoint, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkInternet, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkExcel, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkAcces, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkWord, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkFrances, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkIngles, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkCastellano, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkCatalan, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage4, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkDispuestoHacerPractica, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbNivelHabilidades, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbPreferenciaJornada, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbPreferenciaEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage5, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage6, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdFunciona, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdPai, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage7, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdPrepLab, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage8, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdServeis, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage9, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdRelacions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage10, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdSeguiment, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtNombreCompleto)
		Me.Controls.Add(TabControl1)
		Me.Controls.Add(txtCodigoEntidad)
		Me.Controls.Add(txtCodigo)
		Me.Controls.Add(cmbEstado)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(chkDisponibleParaOferta)
		Me.Controls.Add(cmdEntidad)
		Me.Controls.Add(chkActualmenteTrabajando)
		Me.Controls.Add(GrdServeisAct)
		Me.Controls.Add(chkSusceptibleServeiAssis)
		Me.Controls.Add(chkGarantiaJuvenil)
		Me.Controls.Add(txtGarantiaJ)
		Me.Controls.Add(lblTreballant)
		Me.Controls.Add(Label33)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(lblLock)
		Me.TabControl1.Controls.Add(txtPerfilLaboralN)
		Me.TabControl1.Controls.Add(txtDocMotivacionLaboral)
		Me.TabControl1.Controls.Add(txtGradoOcupabilidad)
		Me.TabControl1.Controls.Add(txtValoracionTecnica)
		Me.TabControl1.Controls.Add(txtGradoOcupabilidadDoc)
		Me.TabControl1.Controls.Add(txtGencat)
		Me.TabControl1.Controls.Add(txtICAP)
		Me.TabControl1.Controls.Add(txtPerfilLaboral)
		Me.TabControl1.Controls.Add(TabControlPage10)
		Me.TabControl1.Controls.Add(TabControlPage9)
		Me.TabControl1.Controls.Add(TabControlPage8)
		Me.TabControl1.Controls.Add(TabControlPage7)
		Me.TabControl1.Controls.Add(TabControlPage6)
		Me.TabControl1.Controls.Add(TabControlPage5)
		Me.TabControl1.Controls.Add(TabControlPage4)
		Me.TabControl1.Controls.Add(TabControlPage3)
		Me.TabControl1.Controls.Add(TabControlPage2)
		Me.TabControl1.Controls.Add(TabControlPage1)
		Me.TabControl1.Controls.Add(cmdGrauOcu)
		Me.TabControl1.Controls.Add(cmdGencat)
		Me.TabControl1.Controls.Add(cmdIcap)
		Me.TabControl1.Controls.Add(cmdPerfilLab)
		Me.TabControl1.Controls.Add(chkGradoOcupabilidad)
		Me.TabControl1.Controls.Add(chkGencat)
		Me.TabControl1.Controls.Add(chkIcap)
		Me.TabControl1.Controls.Add(chkPerfilLaboral)
		Me.TabControl1.Controls.Add(chkMotivacionLaboral)
		Me.TabControl1.Controls.Add(cmdMotLaboral)
		Me.TabControl1.Controls.Add(GrdProgrames)
		Me.TabControl1.Controls.Add(GrdInsercions)
		Me.TabControl1.Controls.Add(Label20)
		Me.TabControl1.Controls.Add(lbl33)
		Me.TabControl1.Controls.Add(lbl34)
		Me.TabControlPage10.Controls.Add(GrdSeguiment)
		Me.TabControlPage9.Controls.Add(GrdRelacions)
		Me.TabControlPage8.Controls.Add(GrdServeis)
		Me.TabControlPage7.Controls.Add(GrdPrepLab)
		Me.TabControlPage6.Controls.Add(txtPai)
		Me.TabControlPage6.Controls.Add(TxtFunciona)
		Me.TabControlPage6.Controls.Add(cmdPai)
		Me.TabControlPage6.Controls.Add(cmdFunciona)
		Me.TabControlPage6.Controls.Add(Label7)
		Me.TabControlPage6.Controls.Add(Label12)
		Me.TabControlPage5.Controls.Add(txtObservacionesPreferFor)
		Me.TabControlPage5.Controls.Add(Text2)
		Me.TabControlPage5.Controls.Add(txtTerceraPreferenciaFor)
		Me.TabControlPage5.Controls.Add(Text4)
		Me.TabControlPage5.Controls.Add(txtSegonaPreferenciaFor)
		Me.TabControlPage5.Controls.Add(Text7)
		Me.TabControlPage5.Controls.Add(txtPrimeraPreferenciaFor)
		Me.TabControlPage5.Controls.Add(Label13)
		Me.TabControlPage5.Controls.Add(Label14)
		Me.TabControlPage5.Controls.Add(Label15)
		Me.TabControlPage5.Controls.Add(Label16)
		Me.TabControlPage4.Controls.Add(txtDisponibilidadHorariaP)
		Me.TabControlPage4.Controls.Add(txtObservacionesOtros)
		Me.TabControlPage4.Controls.Add(txtDisponibilidadGeograf)
		Me.TabControlPage4.Controls.Add(txtIncorporacion)
		Me.TabControlPage4.Controls.Add(txtSueldoAproximado)
		Me.TabControlPage4.Controls.Add(txtCaracteristicasTrabajo)
		Me.TabControlPage4.Controls.Add(txtObservacionesPref)
		Me.TabControlPage4.Controls.Add(Text53)
		Me.TabControlPage4.Controls.Add(txtTerceraPreferenciaTrab)
		Me.TabControlPage4.Controls.Add(Text52)
		Me.TabControlPage4.Controls.Add(txtSegundaPreferenciaTrab)
		Me.TabControlPage4.Controls.Add(Text51)
		Me.TabControlPage4.Controls.Add(txtPrimeraPreferenciaTrab)
		Me.TabControlPage4.Controls.Add(cmbPreferenciaEmpresa)
		Me.TabControlPage4.Controls.Add(cmbPreferenciaJornada)
		Me.TabControlPage4.Controls.Add(cmbNivelHabilidades)
		Me.TabControlPage4.Controls.Add(chkDispuestoHacerPractica)
		Me.TabControlPage4.Controls.Add(Label8)
		Me.TabControlPage4.Controls.Add(lbl62)
		Me.TabControlPage4.Controls.Add(lbl61)
		Me.TabControlPage4.Controls.Add(lbl60)
		Me.TabControlPage4.Controls.Add(lbl59)
		Me.TabControlPage4.Controls.Add(lbl58)
		Me.TabControlPage4.Controls.Add(lbl57)
		Me.TabControlPage4.Controls.Add(lbl56)
		Me.TabControlPage4.Controls.Add(lbl55)
		Me.TabControlPage4.Controls.Add(lbl54)
		Me.TabControlPage4.Controls.Add(lbl53)
		Me.TabControlPage4.Controls.Add(lbl52)
		Me.TabControlPage4.Controls.Add(lbl51)
		Me.TabControlPage4.Controls.Add(Label5)
		Me.TabControlPage3.Controls.Add(txtCurriculumVitae)
		Me.TabControlPage3.Controls.Add(txtOtrosProgramas)
		Me.TabControlPage3.Controls.Add(txtOtros)
		Me.TabControlPage3.Controls.Add(txtFormacionCom)
		Me.TabControlPage3.Controls.Add(txtEspecialidad)
		Me.TabControlPage3.Controls.Add(Text35)
		Me.TabControlPage3.Controls.Add(txtFormacionReglada)
		Me.TabControlPage3.Controls.Add(chkCatalan)
		Me.TabControlPage3.Controls.Add(chkCastellano)
		Me.TabControlPage3.Controls.Add(chkIngles)
		Me.TabControlPage3.Controls.Add(chkFrances)
		Me.TabControlPage3.Controls.Add(chkWord)
		Me.TabControlPage3.Controls.Add(chkAcces)
		Me.TabControlPage3.Controls.Add(chkExcel)
		Me.TabControlPage3.Controls.Add(chkInternet)
		Me.TabControlPage3.Controls.Add(chkPowerpoint)
		Me.TabControlPage3.Controls.Add(chkTieneExperienciaLab)
		Me.TabControlPage3.Controls.Add(grdHistLaboral)
		Me.TabControlPage3.Controls.Add(cmbCurri)
		Me.TabControlPage3.Controls.Add(GrdFormacio)
		Me.TabControlPage3.Controls.Add(Label17)
		Me.TabControlPage3.Controls.Add(Label6)
		Me.TabControlPage3.Controls.Add(lbl50)
		Me.TabControlPage3.Controls.Add(Label4)
		Me.TabControlPage3.Controls.Add(Label3)
		Me.TabControlPage3.Controls.Add(lbl48)
		Me.TabControlPage3.Controls.Add(Label2)
		Me.TabControlPage3.Controls.Add(lbl42)
		Me.TabControlPage3.Controls.Add(Label1)
		Me.TabControlPage3.Controls.Add(lbl37)
		Me.TabControlPage3.Controls.Add(lbl36)
		Me.TabControlPage3.Controls.Add(lbl35)
		Me.TabControlPage2.Controls.Add(txtCondicionantesEntorno)
		Me.TabControlPage2.Controls.Add(txtLimitaciones)
		Me.TabControlPage2.Controls.Add(txtTipoPrestacion)
		Me.TabControlPage2.Controls.Add(chkPrestacionEconomica)
		Me.TabControlPage2.Controls.Add(chkInscripcionOtg)
		Me.TabControlPage2.Controls.Add(txtFechaOtg)
		Me.TabControlPage2.Controls.Add(cmbTransporte)
		Me.TabControlPage2.Controls.Add(cmbValoracionSoc)
		Me.TabControlPage2.Controls.Add(lbl32)
		Me.TabControlPage2.Controls.Add(lbl31)
		Me.TabControlPage2.Controls.Add(lbl30)
		Me.TabControlPage2.Controls.Add(lbl29)
		Me.TabControlPage2.Controls.Add(lbl28)
		Me.TabControlPage2.Controls.Add(lbl26)
		Me.TabControlPage1.Controls.Add(XPFrame302)
		Me.TabControlPage1.Controls.Add(XPFrame301)
		Me.TabControlPage1.Controls.Add(txtPoblacionNacimiento)
		Me.TabControlPage1.Controls.Add(Text1)
		Me.TabControlPage1.Controls.Add(txtDiagnostico)
		Me.TabControlPage1.Controls.Add(txtGradoDiscapacidad)
		Me.TabControlPage1.Controls.Add(Text21)
		Me.TabControlPage1.Controls.Add(txtColectivo)
		Me.TabControlPage1.Controls.Add(Text18)
		Me.TabControlPage1.Controls.Add(txtMotivoBaja)
		Me.TabControlPage1.Controls.Add(txtEmail)
		Me.TabControlPage1.Controls.Add(txtCodigoPostal)
		Me.TabControlPage1.Controls.Add(Text6)
		Me.TabControlPage1.Controls.Add(txtPoblacion)
		Me.TabControlPage1.Controls.Add(txtTelefonoMovil)
		Me.TabControlPage1.Controls.Add(txtTelefono)
		Me.TabControlPage1.Controls.Add(txtNIF)
		Me.TabControlPage1.Controls.Add(txtDireccion)
		Me.TabControlPage1.Controls.Add(txtSegundoApellido)
		Me.TabControlPage1.Controls.Add(txtPrimerApellido)
		Me.TabControlPage1.Controls.Add(txtNombre)
		Me.TabControlPage1.Controls.Add(txtFechaNacimiento)
		Me.TabControlPage1.Controls.Add(cmbSexo)
		Me.TabControlPage1.Controls.Add(txtFechaFin)
		Me.TabControlPage1.Controls.Add(chkTieneCertificadoDiscap)
		Me.TabControlPage1.Controls.Add(cmdDocumentacio)
		Me.TabControlPage1.Controls.Add(Label18)
		Me.TabControlPage1.Controls.Add(lbl24)
		Me.TabControlPage1.Controls.Add(lbl23)
		Me.TabControlPage1.Controls.Add(lbl21)
		Me.TabControlPage1.Controls.Add(lbl18)
		Me.TabControlPage1.Controls.Add(lbl17)
		Me.TabControlPage1.Controls.Add(lbl14)
		Me.TabControlPage1.Controls.Add(lbl7)
		Me.TabControlPage1.Controls.Add(lbl6)
		Me.TabControlPage1.Controls.Add(lbl13)
		Me.TabControlPage1.Controls.Add(lbl12)
		Me.TabControlPage1.Controls.Add(lbl10)
		Me.TabControlPage1.Controls.Add(lbl11)
		Me.TabControlPage1.Controls.Add(lbl8)
		Me.TabControlPage1.Controls.Add(lbl5)
		Me.TabControlPage1.Controls.Add(lbl4)
		Me.TabControlPage1.Controls.Add(lbl3)
		Me.TabControlPage1.Controls.Add(lbl2)
		Me.XPFrame302.Controls.Add(Text8)
		Me.XPFrame302.Controls.Add(txtIdoneidadProgramaN)
		Me.XPFrame302.Controls.Add(Text9)
		Me.XPFrame302.Controls.Add(txtPuntAtencio)
		Me.XPFrame302.Controls.Add(Text5)
		Me.XPFrame302.Controls.Add(txtTecnico)
		Me.XPFrame302.Controls.Add(txtValoracioPossibilitat)
		Me.XPFrame302.Controls.Add(txtServicioInicio)
		Me.XPFrame302.Controls.Add(Text16)
		Me.XPFrame302.Controls.Add(txtDemandaInicial)
		Me.XPFrame302.Controls.Add(txtObservaciones)
		Me.XPFrame302.Controls.Add(txtIdoneidadPrograma)
		Me.XPFrame302.Controls.Add(txtFechaInicio)
		Me.XPFrame302.Controls.Add(Label21)
		Me.XPFrame302.Controls.Add(Label19)
		Me.XPFrame302.Controls.Add(Label11)
		Me.XPFrame302.Controls.Add(Label10)
		Me.XPFrame302.Controls.Add(lbl15)
		Me.XPFrame302.Controls.Add(lbl16)
		Me.XPFrame302.Controls.Add(lbl19)
		Me.XPFrame302.Controls.Add(lbl20)
		Me.XPFrame302.Controls.Add(Label9)
		Me.XPFrame301.Controls.Add(OptRecollida)
		Me.XPFrame301.Controls.Add(cmdImprimir)
		Me.XPFrame301.Controls.Add(OptProfessional)
		Me.TabControl1.ResumeLayout(False)
		Me.TabControlPage10.ResumeLayout(False)
		Me.TabControlPage9.ResumeLayout(False)
		Me.TabControlPage8.ResumeLayout(False)
		Me.TabControlPage7.ResumeLayout(False)
		Me.TabControlPage6.ResumeLayout(False)
		Me.TabControlPage5.ResumeLayout(False)
		Me.TabControlPage4.ResumeLayout(False)
		Me.TabControlPage3.ResumeLayout(False)
		Me.TabControlPage2.ResumeLayout(False)
		Me.TabControlPage1.ResumeLayout(False)
		Me.XPFrame302.ResumeLayout(False)
		Me.XPFrame301.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class