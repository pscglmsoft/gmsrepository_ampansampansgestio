Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmRelacions
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	Private Sub frmRelacions_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
		
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmRelacions_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmRelacions_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmRelacions_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtUsuario_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.GotFocus
		XGotFocus(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.DoubleClick
		ConsultaTaula(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.LostFocus
		If txtUsuario.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUsuario.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaAlta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaAlta.GotFocus
		XGotFocus(Me, txtFechaAlta)
	End Sub
	
	Private Sub txtFechaAlta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaAlta.LostFocus
		If txtFechaAlta.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtFechaAlta)
	End Sub
	
	Private Sub txtNl_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNl.GotFocus
		XGotFocus(Me, txtNl)
	End Sub
	
	Private Sub txtNl_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNl.LostFocus
		If txtNl.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtNl)
	End Sub
	
	Private Sub txtNl_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNl.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipoRelacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoRelacion.GotFocus
		XGotFocus(Me, txtTipoRelacion)
	End Sub
	
	Private Sub txtTipoRelacion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoRelacion.DoubleClick
		ConsultaTaula(Me, txtTipoRelacion)
	End Sub
	
	Private Sub txtTipoRelacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoRelacion.LostFocus
		XLostFocus(Me, txtTipoRelacion)
	End Sub
	
	Private Sub txtTipoRelacion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipoRelacion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSubtipoRelacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubtipoRelacion.GotFocus
		XGotFocus(Me, txtSubtipoRelacion)
	End Sub
	
	Private Sub txtSubtipoRelacion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubtipoRelacion.DoubleClick
		ConsultaTaula(Me, txtSubtipoRelacion,  , txtTipoRelacion.Text)
	End Sub
	
	Private Sub txtSubtipoRelacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubtipoRelacion.LostFocus
		XLostFocus(Me, txtSubtipoRelacion)
	End Sub
	
	Private Sub txtSubtipoRelacion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSubtipoRelacion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtTipoRelacion.Text)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPersona_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPersona.GotFocus
		XGotFocus(Me, txtPersona)
	End Sub
	
	Private Sub txtPersona_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPersona.DoubleClick
		ConsultaTaula(Me, txtPersona)
	End Sub
	
	Private Sub txtPersona_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPersona.LostFocus
		XLostFocus(Me, txtPersona)
	End Sub
	
	Private Sub txtPersona_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPersona.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.GotFocus
		XGotFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtFechaBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.LostFocus
		XLostFocus(Me, txtFechaBaja)
	End Sub
End Class
