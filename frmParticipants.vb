Option Strict Off
Option Explicit On
Friend Class frmParticipants
	Inherits FormParent
	Public Sessio As String
	Public DataS As String
	Public TAcc As Short
	Public TSAcc As Short
	Public Tec As String
	Public Serv As Short
	Public Temps As String
	Public Obs As String
	Private FlagLoad As Boolean
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		Dim Nod As String
		Dim I As Short
		
		For I = 1 To GrdParticipants.Rows - 1
			If GrdParticipants.Cell(I, 3).Text = "1" Then
				If Nod <> "" Then
					Nod = Nod & Chr(13)
				End If
				Nod = Nod & GrdParticipants.Cell(I, 1).Text
			End If
		Next I
		CacheNetejaParametres()
		MCache.P1 = Sessio & S & DataS & S & TAcc & S & TSAcc & S & Tec & S & Serv & S & Temps & S & Obs
		
		MCache.PDELIM = Chr(13)
		MCache.PLIST = Nod
		CacheXecute("D GPART^INSERCIO")
		CacheNetejaParametres()
		xMsgBox("Sessions formatives entrades correctament", MsgBoxStyle.Information, Me.Text)
		Me.Unload()
		frmSessioFormativa.CarregaParticipants()
	End Sub
	
	Private Sub cmdCancelar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancelar.ClickEvent
		Me.Unload()
	End Sub
	
	'UPGRADE_WARNING: Form evento frmParticipants.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmParticipants_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		If FlagLoad = True Then
			CarregaParticipants()
			FlagLoad = False
		End If
	End Sub
	
	Private Sub frmParticipants_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		IniciForm(Me)
		FlagLoad = True
	End Sub
	
	
	
	Private Sub CarregaParticipants()
		CarregaFGrid(GrdParticipants, "CPART^INSERCIO", Sessio)
	End Sub
	
	
	Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Dim I As Short
		Select Case NomMenu
			Case "GrdParticipants"
				Select Case KeyMenu
					Case "M"
						FGCheckGrid(GrdParticipants, True)
					Case "D"
						FGCheckGrid(GrdParticipants, False)
				End Select
		End Select
	End Sub
	
	
	Private Sub GrdParticipants_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdParticipants.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdParticipants", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Marcar tots",  ,  , XPIcon("Checked"),  ,  ,  ,  , "M")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Desmarcar tots",  ,  , XPIcon("UnChecked"),  ,  ,  ,  , "D")
			End With
		End With
		XpExecutaMenu(Me, "GrdParticipants", eventArgs.X, eventArgs.Y)
	End Sub
End Class
