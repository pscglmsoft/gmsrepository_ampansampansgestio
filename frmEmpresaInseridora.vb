Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmEmpresaInseridora
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		BloquejaCamps()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	
	Public Sub FGetReg()
		BloquejaCamps()
	End Sub
	Private Sub chkInsercioDirecta_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkInsercioDirecta.ClickEvent
		If chkInsercioDirecta.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			txtOferta.Enabled = True
		Else
			txtOferta.Text = ""
			txtOferta.Enabled = False
		End If
	End Sub
	
	Private Sub cmbTipusRelacio_DropDownClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipusRelacio.DropDownClosed
		BloquejaCamps()
	End Sub
	
	Private Sub frmEmpresaInseridora_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If Verifica = False Then Exit Sub
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		If cmbTipusRelacio.Columns(1).Value <> 2 Then ActualitzaFase()
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If Verifica = False Then Exit Sub
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		If cmbTipusRelacio.Columns(1).Value <> 2 Then ActualitzaFase()
	End Sub
	
	Private Sub frmEmpresaInseridora_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmEmpresaInseridora_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmEmpresaInseridora_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtEmpresaInseridora_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresaInseridora.GotFocus
		XGotFocus(Me, txtEmpresaInseridora)
	End Sub
	
	Private Sub txtEmpresaInseridora_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresaInseridora.DoubleClick
		ConsultaTaula(Me, txtEmpresaInseridora)
	End Sub
	
	Private Sub txtEmpresaInseridora_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresaInseridora.LostFocus
		If txtEmpresaInseridora.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtEmpresaInseridora)
	End Sub
	
	Private Sub txtEmpresaInseridora_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresaInseridora.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtEmpresaInseridora)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtEmpresaInseridora)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtLlocTreball_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLlocTreball.DoubleClick
		ConsultaTaula(Me, txtLlocTreball)
	End Sub
	
	Private Sub txtLlocTreball_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLlocTreball.GotFocus
		XGotFocus(Me, txtLlocTreball)
	End Sub
	
	Private Sub txtLlocTreball_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLlocTreball.LostFocus
		XLostFocus(Me, txtLlocTreball)
	End Sub
	
	Private Sub txtLlocTreball_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtLlocTreball.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtLlocTreball)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtLlocTreball)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtOferta_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOferta.DoubleClick
		ConsultaTaula(Me, txtOferta)
	End Sub
	
	Private Sub txtOferta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOferta.GotFocus
		XGotFocus(Me, txtOferta)
	End Sub
	
	Private Sub txtOferta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOferta.LostFocus
		XLostFocus(Me, txtOferta)
		PosaServei()
	End Sub
	
	Private Sub txtOferta_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtOferta.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtOferta)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtOferta)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtServei_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.DoubleClick
		ConsultaTaula(Me, txtServei)
	End Sub
	
	Private Sub txtServei_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.GotFocus
		XGotFocus(Me, txtServei)
	End Sub
	
	Private Sub txtServei_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.LostFocus
		XLostFocus(Me, txtServei)
	End Sub
	
	Private Sub txtServei_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtServei.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtServei)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtServei)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSucursal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.GotFocus
		XGotFocus(Me, txtSucursal)
	End Sub
	
	Private Sub txtSucursal_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.DoubleClick
		ConsultaTaula(Me, txtSucursal,  , txtEmpresaInseridora.Text & S & "S")
	End Sub
	
	Private Sub txtSucursal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.LostFocus
		If txtSucursal.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtSucursal)
	End Sub
	
	Private Sub txtSucursal_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSucursal.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtSucursal, txtEmpresaInseridora.Text & S & "S")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtSucursal)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtUsuari_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.GotFocus
		XGotFocus(Me, txtUsuari)
	End Sub
	
	Private Sub txtUsuari_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.DoubleClick
		ConsultaTaula(Me, txtUsuari)
	End Sub
	
	Private Sub txtUsuari_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.LostFocus
		If txtUsuari.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtUsuari)
	End Sub
	
	Private Sub txtUsuari_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUsuari.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtUsuari)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtUsuari)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDataInici_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataInici.GotFocus
		XGotFocus(Me, txtDataInici)
	End Sub
	
	Private Sub txtDataInici_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataInici.LostFocus
		If txtDataInici.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtDataInici)
	End Sub
	
	Private Sub cmbTipusRelacio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipusRelacio.GotFocus
		XGotFocus(Me, cmbTipusRelacio)
	End Sub
	
	Private Sub cmbTipusRelacio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipusRelacio.LostFocus
		XLostFocus(Me, cmbTipusRelacio)
	End Sub
	
	Private Sub cmbTipusContracte_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipusContracte.GotFocus
		XGotFocus(Me, cmbTipusContracte)
	End Sub
	
	Private Sub cmbTipusContracte_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipusContracte.LostFocus
		XLostFocus(Me, cmbTipusContracte)
	End Sub
	
	Private Sub txtDataFi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataFi.GotFocus
		XGotFocus(Me, txtDataFi)
	End Sub
	
	Private Sub txtDataFi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataFi.LostFocus
		XLostFocus(Me, txtDataFi)
	End Sub
	
	Private Sub txtObservacions_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.GotFocus
		XGotFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.LostFocus
		XLostFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservacions.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtObservacions)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	Function Verifica() As Boolean
		Verifica = True
		If cmbTipusRelacio.Columns(1).Value = 1 And cmbTipusContracte.Columns(1).Value = "" Then
			xMsgBox("Falta informar el tipus de contracte", MsgBoxStyle.Information, Me.Text)
			cmbTipusContracte.Focus()
			Verifica = False
		End If
	End Function
	
	
	Private Sub BloquejaCamps()
		If ABM = "AL" Then
			cmbTipusRelacio.Enabled = True
		Else
			cmbTipusRelacio.Enabled = False
		End If
		
		If cmbTipusRelacio.Columns(1).Value <> 1 Then
			cmbTipusContracte.Value = ""
			cmbTipusContracte.Enabled = False
		Else
			cmbTipusContracte.Enabled = True
		End If
		
		If chkInsercioDirecta.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			txtOferta.Enabled = True
		Else
			txtOferta.Text = ""
			txtOferta.Enabled = False
		End If
	End Sub
	
	Private Sub ActualitzaFase()
		Dim Resposta As String
		Dim valor As String
		CacheNetejaParametres()
		MCache.P1 = txtUsuari.Text
		MCache.P2 = txtDataInici.Text
		valor = CacheXecute("S VALUE=$$MiraSiFase^INSERCIO(P1)")
		If valor = "SI" Then
			'ER = xMsgBox(GetString("Consulta", "SYS") + " " + Piece(ParConsulta, PC, 1) + " " + GetString("inexistente", "SYS") + ".", 4112, GetString("Error consultas", "SYS"), False)
			Resposta = CStr(xMsgBox("Vols afegir la data a la FASE 4 ?", MsgBoxStyle.YesNo, Me.Text))
			If CDbl(Resposta) = 6 Then
				CacheXecute("D ACTFASE^INSERCIO")
			End If
		End If
	End Sub
	
	
	Private Sub PosaServei()
		Dim Servei As String
		
		If txtOferta.Text = "" Then Exit Sub
		CacheNetejaParametres()
		MCache.P1 = txtOferta.Text
		Servei = CacheXecute("S VALUE=$P(^INOFERT(EMP,P1),S,4)")
		txtServei.Text = Servei
	End Sub
End Class
