<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDemandesPendents
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Timer_Renamed As System.Windows.Forms.Timer
	Public WithEvents chkTotesMeuCentreP As System.Windows.Forms.CheckBox
	Public WithEvents chkTotesMeuCentreM As System.Windows.Forms.CheckBox
	Public WithEvents chkTotesMeuCentreF As System.Windows.Forms.CheckBox
	Public WithEvents chkPendent As System.Windows.Forms.CheckBox
	Public WithEvents chkEnCursM As System.Windows.Forms.CheckBox
	Public WithEvents ChkRealitzadesM As System.Windows.Forms.CheckBox
	Public WithEvents ChkDesetmadesM As System.Windows.Forms.CheckBox
	Public WithEvents Frame2 As System.Windows.Forms.GroupBox
	Public WithEvents chkDesestimadaF As System.Windows.Forms.CheckBox
	Public WithEvents chkRealitzadesF As System.Windows.Forms.CheckBox
	Public WithEvents chkEnCursF As System.Windows.Forms.CheckBox
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents GrdDemandesPendents As AxFlexCell.AxGrid
	Public WithEvents GrdConsultaDemandes As AxFlexCell.AxGrid
	Public WithEvents GrdDemandesQueHeFet As AxFlexCell.AxGrid
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDemandesPendents))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.Timer_Renamed = New System.Windows.Forms.Timer(components)
		Me.chkTotesMeuCentreP = New System.Windows.Forms.CheckBox
		Me.chkTotesMeuCentreM = New System.Windows.Forms.CheckBox
		Me.chkTotesMeuCentreF = New System.Windows.Forms.CheckBox
		Me.Frame2 = New System.Windows.Forms.GroupBox
		Me.chkPendent = New System.Windows.Forms.CheckBox
		Me.chkEnCursM = New System.Windows.Forms.CheckBox
		Me.ChkRealitzadesM = New System.Windows.Forms.CheckBox
		Me.ChkDesetmadesM = New System.Windows.Forms.CheckBox
		Me.Frame1 = New System.Windows.Forms.GroupBox
		Me.chkDesestimadaF = New System.Windows.Forms.CheckBox
		Me.chkRealitzadesF = New System.Windows.Forms.CheckBox
		Me.chkEnCursF = New System.Windows.Forms.CheckBox
		Me.GrdDemandesPendents = New AxFlexCell.AxGrid
		Me.GrdConsultaDemandes = New AxFlexCell.AxGrid
		Me.GrdDemandesQueHeFet = New AxFlexCell.AxGrid
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.Frame2.SuspendLayout()
		Me.Frame1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.GrdDemandesPendents, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdConsultaDemandes, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdDemandesQueHeFet, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Gesti� de Peticions"
		Me.ClientSize = New System.Drawing.Size(1112, 594)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmDemandesPendents"
		Me.Timer_Renamed.Interval = 10000
		Me.Timer_Renamed.Enabled = True
		Me.chkTotesMeuCentreP.Text = "Totes les del meu centre"
		Me.chkTotesMeuCentreP.Size = New System.Drawing.Size(159, 13)
		Me.chkTotesMeuCentreP.Location = New System.Drawing.Point(946, 6)
		Me.chkTotesMeuCentreP.TabIndex = 17
		Me.chkTotesMeuCentreP.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkTotesMeuCentreP.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkTotesMeuCentreP.BackColor = System.Drawing.SystemColors.Control
		Me.chkTotesMeuCentreP.CausesValidation = True
		Me.chkTotesMeuCentreP.Enabled = True
		Me.chkTotesMeuCentreP.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkTotesMeuCentreP.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkTotesMeuCentreP.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkTotesMeuCentreP.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkTotesMeuCentreP.TabStop = True
		Me.chkTotesMeuCentreP.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkTotesMeuCentreP.Visible = True
		Me.chkTotesMeuCentreP.Name = "chkTotesMeuCentreP"
		Me.chkTotesMeuCentreM.Text = "Totes les del meu centre"
		Me.chkTotesMeuCentreM.Size = New System.Drawing.Size(159, 13)
		Me.chkTotesMeuCentreM.Location = New System.Drawing.Point(946, 400)
		Me.chkTotesMeuCentreM.TabIndex = 15
		Me.chkTotesMeuCentreM.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkTotesMeuCentreM.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkTotesMeuCentreM.BackColor = System.Drawing.SystemColors.Control
		Me.chkTotesMeuCentreM.CausesValidation = True
		Me.chkTotesMeuCentreM.Enabled = True
		Me.chkTotesMeuCentreM.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkTotesMeuCentreM.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkTotesMeuCentreM.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkTotesMeuCentreM.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkTotesMeuCentreM.TabStop = True
		Me.chkTotesMeuCentreM.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkTotesMeuCentreM.Visible = True
		Me.chkTotesMeuCentreM.Name = "chkTotesMeuCentreM"
		Me.chkTotesMeuCentreF.Text = "Totes les del meu centre"
		Me.chkTotesMeuCentreF.Size = New System.Drawing.Size(159, 13)
		Me.chkTotesMeuCentreF.Location = New System.Drawing.Point(946, 202)
		Me.chkTotesMeuCentreF.TabIndex = 14
		Me.chkTotesMeuCentreF.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkTotesMeuCentreF.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkTotesMeuCentreF.BackColor = System.Drawing.SystemColors.Control
		Me.chkTotesMeuCentreF.CausesValidation = True
		Me.chkTotesMeuCentreF.Enabled = True
		Me.chkTotesMeuCentreF.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkTotesMeuCentreF.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkTotesMeuCentreF.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkTotesMeuCentreF.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkTotesMeuCentreF.TabStop = True
		Me.chkTotesMeuCentreF.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkTotesMeuCentreF.Visible = True
		Me.chkTotesMeuCentreF.Name = "chkTotesMeuCentreF"
		Me.Frame2.Text = "Filtrar"
		Me.Frame2.Size = New System.Drawing.Size(465, 31)
		Me.Frame2.Location = New System.Drawing.Point(232, 388)
		Me.Frame2.TabIndex = 10
		Me.Frame2.BackColor = System.Drawing.SystemColors.Control
		Me.Frame2.Enabled = True
		Me.Frame2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame2.Visible = True
		Me.Frame2.Padding = New System.Windows.Forms.Padding(0)
		Me.Frame2.Name = "Frame2"
		Me.chkPendent.Text = "Pendent"
		Me.chkPendent.Size = New System.Drawing.Size(65, 13)
		Me.chkPendent.Location = New System.Drawing.Point(38, 12)
		Me.chkPendent.TabIndex = 16
		Me.chkPendent.CheckState = System.Windows.Forms.CheckState.Checked
		Me.chkPendent.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkPendent.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkPendent.BackColor = System.Drawing.SystemColors.Control
		Me.chkPendent.CausesValidation = True
		Me.chkPendent.Enabled = True
		Me.chkPendent.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkPendent.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkPendent.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkPendent.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkPendent.TabStop = True
		Me.chkPendent.Visible = True
		Me.chkPendent.Name = "chkPendent"
		Me.chkEnCursM.Text = "En Curs"
		Me.chkEnCursM.Size = New System.Drawing.Size(65, 13)
		Me.chkEnCursM.Location = New System.Drawing.Point(144, 12)
		Me.chkEnCursM.TabIndex = 13
		Me.chkEnCursM.CheckState = System.Windows.Forms.CheckState.Checked
		Me.chkEnCursM.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkEnCursM.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkEnCursM.BackColor = System.Drawing.SystemColors.Control
		Me.chkEnCursM.CausesValidation = True
		Me.chkEnCursM.Enabled = True
		Me.chkEnCursM.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkEnCursM.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkEnCursM.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkEnCursM.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkEnCursM.TabStop = True
		Me.chkEnCursM.Visible = True
		Me.chkEnCursM.Name = "chkEnCursM"
		Me.ChkRealitzadesM.Text = "Realitzades"
		Me.ChkRealitzadesM.Size = New System.Drawing.Size(77, 13)
		Me.ChkRealitzadesM.Location = New System.Drawing.Point(238, 12)
		Me.ChkRealitzadesM.TabIndex = 12
		Me.ChkRealitzadesM.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.ChkRealitzadesM.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.ChkRealitzadesM.BackColor = System.Drawing.SystemColors.Control
		Me.ChkRealitzadesM.CausesValidation = True
		Me.ChkRealitzadesM.Enabled = True
		Me.ChkRealitzadesM.ForeColor = System.Drawing.SystemColors.ControlText
		Me.ChkRealitzadesM.Cursor = System.Windows.Forms.Cursors.Default
		Me.ChkRealitzadesM.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ChkRealitzadesM.Appearance = System.Windows.Forms.Appearance.Normal
		Me.ChkRealitzadesM.TabStop = True
		Me.ChkRealitzadesM.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.ChkRealitzadesM.Visible = True
		Me.ChkRealitzadesM.Name = "ChkRealitzadesM"
		Me.ChkDesetmadesM.Text = "Desestimades"
		Me.ChkDesetmadesM.Size = New System.Drawing.Size(89, 13)
		Me.ChkDesetmadesM.Location = New System.Drawing.Point(352, 12)
		Me.ChkDesetmadesM.TabIndex = 11
		Me.ChkDesetmadesM.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.ChkDesetmadesM.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.ChkDesetmadesM.BackColor = System.Drawing.SystemColors.Control
		Me.ChkDesetmadesM.CausesValidation = True
		Me.ChkDesetmadesM.Enabled = True
		Me.ChkDesetmadesM.ForeColor = System.Drawing.SystemColors.ControlText
		Me.ChkDesetmadesM.Cursor = System.Windows.Forms.Cursors.Default
		Me.ChkDesetmadesM.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ChkDesetmadesM.Appearance = System.Windows.Forms.Appearance.Normal
		Me.ChkDesetmadesM.TabStop = True
		Me.ChkDesetmadesM.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.ChkDesetmadesM.Visible = True
		Me.ChkDesetmadesM.Name = "ChkDesetmadesM"
		Me.Frame1.Text = "Filtrar"
		Me.Frame1.Size = New System.Drawing.Size(377, 31)
		Me.Frame1.Location = New System.Drawing.Point(232, 190)
		Me.Frame1.TabIndex = 6
		Me.Frame1.BackColor = System.Drawing.SystemColors.Control
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Visible = True
		Me.Frame1.Padding = New System.Windows.Forms.Padding(0)
		Me.Frame1.Name = "Frame1"
		Me.chkDesestimadaF.Text = "Desestimades"
		Me.chkDesestimadaF.Size = New System.Drawing.Size(89, 13)
		Me.chkDesestimadaF.Location = New System.Drawing.Point(260, 12)
		Me.chkDesestimadaF.TabIndex = 9
		Me.chkDesestimadaF.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkDesestimadaF.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkDesestimadaF.BackColor = System.Drawing.SystemColors.Control
		Me.chkDesestimadaF.CausesValidation = True
		Me.chkDesestimadaF.Enabled = True
		Me.chkDesestimadaF.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkDesestimadaF.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkDesestimadaF.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkDesestimadaF.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkDesestimadaF.TabStop = True
		Me.chkDesestimadaF.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkDesestimadaF.Visible = True
		Me.chkDesestimadaF.Name = "chkDesestimadaF"
		Me.chkRealitzadesF.Text = "Realitzades"
		Me.chkRealitzadesF.Size = New System.Drawing.Size(77, 13)
		Me.chkRealitzadesF.Location = New System.Drawing.Point(146, 12)
		Me.chkRealitzadesF.TabIndex = 8
		Me.chkRealitzadesF.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkRealitzadesF.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkRealitzadesF.BackColor = System.Drawing.SystemColors.Control
		Me.chkRealitzadesF.CausesValidation = True
		Me.chkRealitzadesF.Enabled = True
		Me.chkRealitzadesF.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkRealitzadesF.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkRealitzadesF.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkRealitzadesF.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkRealitzadesF.TabStop = True
		Me.chkRealitzadesF.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkRealitzadesF.Visible = True
		Me.chkRealitzadesF.Name = "chkRealitzadesF"
		Me.chkEnCursF.Text = "En Curs"
		Me.chkEnCursF.Size = New System.Drawing.Size(65, 13)
		Me.chkEnCursF.Location = New System.Drawing.Point(46, 12)
		Me.chkEnCursF.TabIndex = 7
		Me.chkEnCursF.CheckState = System.Windows.Forms.CheckState.Checked
		Me.chkEnCursF.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkEnCursF.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkEnCursF.BackColor = System.Drawing.SystemColors.Control
		Me.chkEnCursF.CausesValidation = True
		Me.chkEnCursF.Enabled = True
		Me.chkEnCursF.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkEnCursF.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkEnCursF.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkEnCursF.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkEnCursF.TabStop = True
		Me.chkEnCursF.Visible = True
		Me.chkEnCursF.Name = "chkEnCursF"
		GrdDemandesPendents.OcxState = CType(resources.GetObject("GrdDemandesPendents.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdDemandesPendents.Size = New System.Drawing.Size(1102, 165)
		Me.GrdDemandesPendents.Location = New System.Drawing.Point(4, 24)
		Me.GrdDemandesPendents.TabIndex = 0
		Me.GrdDemandesPendents.Name = "GrdDemandesPendents"
		GrdConsultaDemandes.OcxState = CType(resources.GetObject("GrdConsultaDemandes.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdConsultaDemandes.Size = New System.Drawing.Size(1104, 165)
		Me.GrdConsultaDemandes.Location = New System.Drawing.Point(4, 222)
		Me.GrdConsultaDemandes.TabIndex = 1
		Me.GrdConsultaDemandes.Name = "GrdConsultaDemandes"
		GrdDemandesQueHeFet.OcxState = CType(resources.GetObject("GrdDemandesQueHeFet.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdDemandesQueHeFet.Size = New System.Drawing.Size(1104, 165)
		Me.GrdDemandesQueHeFet.Location = New System.Drawing.Point(4, 420)
		Me.GrdDemandesQueHeFet.TabIndex = 4
		Me.GrdDemandesQueHeFet.Name = "GrdDemandesQueHeFet"
		Me.Label3.Text = "Consulta Peticions que he fet"
		Me.Label3.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.ForeColor = System.Drawing.Color.FromARGB(0, 0, 128)
		Me.Label3.Size = New System.Drawing.Size(199, 17)
		Me.Label3.Location = New System.Drawing.Point(10, 400)
		Me.Label3.TabIndex = 5
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Consulta Peticions que m'han fet"
		Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.ForeColor = System.Drawing.Color.FromARGB(0, 0, 128)
		Me.Label2.Size = New System.Drawing.Size(199, 17)
		Me.Label2.Location = New System.Drawing.Point(10, 202)
		Me.Label2.TabIndex = 3
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Peticions Pendent de Gestionar"
		Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.ForeColor = System.Drawing.Color.FromARGB(0, 0, 128)
		Me.Label1.Size = New System.Drawing.Size(199, 17)
		Me.Label1.Location = New System.Drawing.Point(6, 4)
		Me.Label1.TabIndex = 2
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		CType(Me.GrdDemandesQueHeFet, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdConsultaDemandes, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdDemandesPendents, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(chkTotesMeuCentreP)
		Me.Controls.Add(chkTotesMeuCentreM)
		Me.Controls.Add(chkTotesMeuCentreF)
		Me.Controls.Add(Frame2)
		Me.Controls.Add(Frame1)
		Me.Controls.Add(GrdDemandesPendents)
		Me.Controls.Add(GrdConsultaDemandes)
		Me.Controls.Add(GrdDemandesQueHeFet)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.Frame2.Controls.Add(chkPendent)
		Me.Frame2.Controls.Add(chkEnCursM)
		Me.Frame2.Controls.Add(ChkRealitzadesM)
		Me.Frame2.Controls.Add(ChkDesetmadesM)
		Me.Frame1.Controls.Add(chkDesestimadaF)
		Me.Frame1.Controls.Add(chkRealitzadesF)
		Me.Frame1.Controls.Add(chkEnCursF)
		Me.Frame2.ResumeLayout(False)
		Me.Frame1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class