<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class FrmVerificacioFaixes
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtFaixaCompara As System.Windows.Forms.TextBox
	Public WithEvents txtFaixa As System.Windows.Forms.TextBox
	Public WithEvents cmdNovaVerificacio As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmVerificacioFaixes))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtFaixaCompara = New System.Windows.Forms.TextBox
		Me.txtFaixa = New System.Windows.Forms.TextBox
		Me.cmdNovaVerificacio = New AxXtremeSuiteControls.AxPushButton
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdNovaVerificacio, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.BackColor = System.Drawing.Color.FromARGB(192, 192, 192)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Verificaci� de Faixes"
		Me.ClientSize = New System.Drawing.Size(402, 272)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "FrmVerificacioFaixes"
		Me.txtFaixaCompara.AutoSize = False
		Me.txtFaixaCompara.Size = New System.Drawing.Size(193, 37)
		Me.txtFaixaCompara.Location = New System.Drawing.Point(174, 96)
		Me.txtFaixaCompara.Maxlength = 13
		Me.txtFaixaCompara.TabIndex = 2
		Me.txtFaixaCompara.AcceptsReturn = True
		Me.txtFaixaCompara.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFaixaCompara.BackColor = System.Drawing.SystemColors.Window
		Me.txtFaixaCompara.CausesValidation = True
		Me.txtFaixaCompara.Enabled = True
		Me.txtFaixaCompara.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFaixaCompara.HideSelection = True
		Me.txtFaixaCompara.ReadOnly = False
		Me.txtFaixaCompara.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFaixaCompara.MultiLine = False
		Me.txtFaixaCompara.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFaixaCompara.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFaixaCompara.TabStop = True
		Me.txtFaixaCompara.Visible = True
		Me.txtFaixaCompara.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFaixaCompara.Name = "txtFaixaCompara"
		Me.txtFaixa.AutoSize = False
		Me.txtFaixa.BackColor = System.Drawing.Color.FromARGB(192, 192, 192)
		Me.txtFaixa.Size = New System.Drawing.Size(193, 37)
		Me.txtFaixa.Location = New System.Drawing.Point(174, 14)
		Me.txtFaixa.Maxlength = 13
		Me.txtFaixa.TabIndex = 0
		Me.txtFaixa.AcceptsReturn = True
		Me.txtFaixa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFaixa.CausesValidation = True
		Me.txtFaixa.Enabled = True
		Me.txtFaixa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFaixa.HideSelection = True
		Me.txtFaixa.ReadOnly = False
		Me.txtFaixa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFaixa.MultiLine = False
		Me.txtFaixa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFaixa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFaixa.TabStop = True
		Me.txtFaixa.Visible = True
		Me.txtFaixa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFaixa.Name = "txtFaixa"
		cmdNovaVerificacio.OcxState = CType(resources.GetObject("cmdNovaVerificacio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdNovaVerificacio.Size = New System.Drawing.Size(223, 67)
		Me.cmdNovaVerificacio.Location = New System.Drawing.Point(102, 172)
		Me.cmdNovaVerificacio.TabIndex = 4
		Me.cmdNovaVerificacio.Name = "cmdNovaVerificacio"
		Me.Label2.BackColor = System.Drawing.Color.Transparent
		Me.Label2.Text = "Faixa a comparar"
		Me.Label2.Size = New System.Drawing.Size(187, 33)
		Me.Label2.Location = New System.Drawing.Point(20, 98)
		Me.Label2.TabIndex = 3
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.BackColor = System.Drawing.Color.Transparent
		Me.Label1.Text = "Faixa"
		Me.Label1.Size = New System.Drawing.Size(111, 33)
		Me.Label1.Location = New System.Drawing.Point(20, 18)
		Me.Label1.TabIndex = 1
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		CType(Me.cmdNovaVerificacio, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtFaixaCompara)
		Me.Controls.Add(txtFaixa)
		Me.Controls.Add(cmdNovaVerificacio)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class