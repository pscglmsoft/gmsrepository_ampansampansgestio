Option Strict Off
Option Explicit On
Friend Class frmUsuariInsercioLaboral
	Inherits FormParent
	
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	Private Path As String
	Private PATHGEN As String
	Private PathU As String
	Private PATHGENU As String
	Private NDoc As Short
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		If chkGarantiaJuvenil.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtGarantiaJ.Enabled = True
		Else
			txtGarantiaJ.Text = ""
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtGarantiaJ.Enabled = False
		End If
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
		lblTreballant.Text = ""
	End Sub
	
	Public Overrides Sub FGetReg()
		CarregaGrids((TabControl1.SelectedItem))
		MostraEstat()
		
		' Demanda Ester 01/06/16
		
		''''    If ABM <> "AL" Then
		''''        txtFechaInicio.Enabled = False
		''''        txtServicioInicio.Enabled = False
		''''        txtPuntAtencio.Enabled = False
		''''        txtTecnico.Enabled = False
		''''    End If
		CarregaServeisActius()
	End Sub
	
	Private Sub chkActualmenteTrabajando_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkActualmenteTrabajando.GotFocus
		XGotFocus(Me, chkActualmenteTrabajando)
	End Sub
	
	Private Sub chkActualmenteTrabajando_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkActualmenteTrabajando.LostFocus
		XLostFocus(Me, chkActualmenteTrabajando)
	End Sub
	
	Private Sub chkDisponibleParaOferta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDisponibleParaOferta.GotFocus
		XGotFocus(Me, chkDisponibleParaOferta)
	End Sub
	
	Private Sub chkDisponibleParaOferta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDisponibleParaOferta.LostFocus
		XLostFocus(Me, chkDisponibleParaOferta)
	End Sub
	
	
	
	
	
	Private Sub chkDispuestoHacerPractica_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDispuestoHacerPractica.ClickEvent
		If chkDispuestoHacerPractica.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			txtDisponibilidadHorariaP.Enabled = True
		Else
			txtDisponibilidadHorariaP.Text = ""
			txtDisponibilidadHorariaP.Enabled = False
		End If
	End Sub
	
	Private Sub chkDispuestoHacerPractica_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDispuestoHacerPractica.GotFocus
		XGotFocus(Me, chkDispuestoHacerPractica)
	End Sub
	
	Private Sub chkDispuestoHacerPractica_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDispuestoHacerPractica.LostFocus
		XLostFocus(Me, chkDispuestoHacerPractica)
	End Sub
	
	Private Sub chkGarantiaJuvenil_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkGarantiaJuvenil.ClickEvent
		If chkGarantiaJuvenil.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtGarantiaJ.Enabled = True
		Else
			txtGarantiaJ.Text = ""
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtGarantiaJ.Enabled = False
		End If
	End Sub
	
	Private Sub chkGarantiaJuvenil_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkGarantiaJuvenil.GotFocus
		XGotFocus(Me, chkGarantiaJuvenil)
	End Sub
	
	Private Sub chkGarantiaJuvenil_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkGarantiaJuvenil.LostFocus
		XLostFocus(Me, chkGarantiaJuvenil)
	End Sub
	
	Private Sub chkGencat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkGencat.GotFocus
		XGotFocus(Me, chkGencat)
	End Sub
	
	Private Sub chkGencat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkGencat.LostFocus
		XLostFocus(Me, chkGencat)
	End Sub
	
	Private Sub chkGradoOcupabilidad_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkGradoOcupabilidad.GotFocus
		XGotFocus(Me, chkGradoOcupabilidad)
	End Sub
	
	Private Sub chkGradoOcupabilidad_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkGradoOcupabilidad.LostFocus
		XLostFocus(Me, chkGradoOcupabilidad)
	End Sub
	
	Private Sub chkIcap_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkIcap.GotFocus
		XGotFocus(Me, chkIcap)
	End Sub
	
	Private Sub chkIcap_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkIcap.LostFocus
		XLostFocus(Me, chkIcap)
	End Sub
	
	
	Private Sub chkMotivacionLaboral_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkMotivacionLaboral.GotFocus
		XGotFocus(Me, chkMotivacionLaboral)
	End Sub
	
	Private Sub chkMotivacionLaboral_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkMotivacionLaboral.LostFocus
		XLostFocus(Me, chkMotivacionLaboral)
	End Sub
	
	Private Sub chkPerfilLaboral_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPerfilLaboral.GotFocus
		XGotFocus(Me, chkPerfilLaboral)
	End Sub
	
	Private Sub chkPerfilLaboral_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPerfilLaboral.LostFocus
		XLostFocus(Me, chkPerfilLaboral)
	End Sub
	
	Private Sub chkSusceptibleServeiAssis_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkSusceptibleServeiAssis.GotFocus
		XGotFocus(Me, chkSusceptibleServeiAssis)
	End Sub
	
	Private Sub chkSusceptibleServeiAssis_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkSusceptibleServeiAssis.LostFocus
		XLostFocus(Me, chkSusceptibleServeiAssis)
	End Sub
	
	Private Sub chkTieneCertificadoDiscap_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTieneCertificadoDiscap.ClickEvent
		If chkTieneCertificadoDiscap.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			txtGradoDiscapacidad.Enabled = True
		Else
			txtGradoDiscapacidad.Text = ""
			txtGradoDiscapacidad.Enabled = False
		End If
	End Sub
	
	Private Sub cmbCurri_MouseUpEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxXtremeSuiteControls._DPushButtonEvents_MouseUpEvent) Handles cmbCurri.MouseUpEvent
		If eventArgs.Button = MouseButtons.Left Then Exit Sub
		NDoc = 1
		txtCurriculumVitae.Focus()
		MenuDesplegable(eventArgs.X + 4200, eventArgs.Y)
	End Sub
	
	Private Sub cmdDocumentacio_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDocumentacio.ClickEvent
		If txtCodigo.Text = "" Then Exit Sub
		frmGDDocumentosPersona.Persona = txtCodigo.Text
		frmGDDocumentosPersona.NomPersona = txtNombreCompleto.Text
		frmGDDocumentosPersona.ReferenciaGestorDocumental = "AMPANSGESTIO" 'txtCodigo
		frmGDDocumentosPersona.Show()
	End Sub
	
	Private Sub cmdEntidad_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdEntidad.ClickEvent
		If txtCodigoEntidad.Text = "" Then Exit Sub
		CarregaEntitat(txtCodigoEntidad.Text)
		If txtCodigoEntidad.Text = "" Then Me.Unload()
	End Sub
	
	
	
	
	Private Sub cmdFunciona_MouseUpEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxXtremeSuiteControls._DPushButtonEvents_MouseUpEvent) Handles cmdFunciona.MouseUpEvent
		If eventArgs.Button = MouseButtons.Left Then Exit Sub
		NDoc = 7
		TxtFunciona.Focus()
		MenuDesplegable(eventArgs.X + 4200, eventArgs.Y)
	End Sub
	
	Private Sub cmdGencat_MouseUpEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxXtremeSuiteControls._DPushButtonEvents_MouseUpEvent) Handles cmdGencat.MouseUpEvent
		If eventArgs.Button = MouseButtons.Left Then Exit Sub
		NDoc = 4
		txtGencat.Focus()
		MenuDesplegable(eventArgs.X + 4200, eventArgs.Y)
	End Sub
	
	Private Sub cmdGrauOcu_MouseUpEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxXtremeSuiteControls._DPushButtonEvents_MouseUpEvent) Handles cmdGrauOcu.MouseUpEvent
		If eventArgs.Button = MouseButtons.Left Then Exit Sub
		NDoc = 3
		txtGradoOcupabilidad.Focus()
		MenuDesplegable(eventArgs.X + 4200, eventArgs.Y)
	End Sub
	
	
	Private Sub cmdIcap_MouseUpEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxXtremeSuiteControls._DPushButtonEvents_MouseUpEvent) Handles cmdIcap.MouseUpEvent
		If eventArgs.Button = MouseButtons.Left Then Exit Sub
		NDoc = 5
		txtICAP.Focus()
		MenuDesplegable(eventArgs.X + 4200, eventArgs.Y)
	End Sub
	
	
	Private Sub cmdImprimir_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdImprimir.ClickEvent
		If txtCodigo.Text = "" Then Exit Sub
		
		If OptRecollida.Value = True Then
			ImpresioPDF("RECOLLIDA INSERCIO HISTOR", txtCodigo.Text)
			
			Retard()
			
			ImpresioPDF("RECOLLIDA DADES INSERCIO", txtCodigo.Text)
			
		Else
			ImpresioPDF("I-FITXA_INSERCIO_HISTORIAL", txtCodigo.Text)
			
			Retard()
			
			ImpresioPDF("I-FITXA_INSERCIO", txtCodigo.Text)
			
		End If
		
	End Sub
	
	Private Sub cmdMotLaboral_MouseUpEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxXtremeSuiteControls._DPushButtonEvents_MouseUpEvent) Handles cmdMotLaboral.MouseUpEvent
		If eventArgs.Button = MouseButtons.Left Then Exit Sub
		NDoc = 8
		txtDocMotivacionLaboral.Focus()
		MenuDesplegable(eventArgs.X + 4200, eventArgs.Y)
	End Sub
	
	Private Sub cmdPai_MouseUpEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxXtremeSuiteControls._DPushButtonEvents_MouseUpEvent) Handles cmdPai.MouseUpEvent
		If eventArgs.Button = MouseButtons.Left Then Exit Sub
		NDoc = 2
		txtPai.Focus()
		MenuDesplegable(eventArgs.X + 4200, eventArgs.Y)
	End Sub
	
	Private Sub cmdPerfilLab_MouseUpEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxXtremeSuiteControls._DPushButtonEvents_MouseUpEvent) Handles cmdPerfilLab.MouseUpEvent
		If eventArgs.Button = MouseButtons.Left Then Exit Sub
		NDoc = 6
		txtPerfilLaboral.Focus()
		MenuDesplegable(eventArgs.X + 4200, eventArgs.Y)
	End Sub
	
	
	
	'UPGRADE_WARNING: Form evento frmUsuariInsercioLaboral.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmUsuariInsercioLaboral_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaGrids((TabControl1.SelectedItem))
		CarregaServeisActius()
		MostraEstat()
		XActivate(Me)
	End Sub
	
	'UPGRADE_WARNING: Form evento frmUsuariInsercioLaboral.Deactivate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmUsuariInsercioLaboral_Deactivate(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Deactivate
		XActivate(Me)
	End Sub
	
	Private Sub frmUsuariInsercioLaboral_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If txtCodigoEntidad.Text = "" Then
			xMsgBox("Alta no permitida !!!", MsgBoxStyle.Critical, "Error")
			Exit Sub
		End If
		
		If Valida = False Then Exit Sub
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If txtCodigoEntidad.Text = "" Then
			xMsgBox("Alta no permitida !!!", MsgBoxStyle.Critical, "Error")
			Exit Sub
		End If
		
		If Valida = False Then Exit Sub
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		
		CarregaServeisActius()
		MostraEstat()
		
		'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		txtFechaFin.Enabled = False
		txtMotivoBaja.Enabled = False
	End Sub
	
	Private Sub frmUsuariInsercioLaboral_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		cmdEntidad.Picture = SetIcon("Organisation", 16)
		
		If ControlEntitats = True Then
			cmdEntidad.Visible = True
		Else
			cmdEntidad.Visible = False
		End If
		
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		PATHGEN = CacheXecute("S VALUE=$P(^PARA(""RH"",1,EMP),S,2)")
		PATHGENU = CacheXecute("S VALUE=$P(^PARA(""USU"",1,EMP),S,2)")
		Path = PATHGEN
		PathU = PATHGENU
		
		'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		txtFechaFin.Enabled = False
		txtMotivoBaja.Enabled = False
		
		cmdImprimir.Picture = SetIcon("Print", 16)
		cmdDocumentacio.Picture = SetIcon("Modif", 16)
	End Sub
	
	Private Sub frmUsuariInsercioLaboral_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmUsuariInsercioLaboral_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub GrdFormacio_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdFormacio.DoubleClick
		
		If GrdFormacio.Selection.FirstRow >= 1 Then
			ObreHistForm()
		End If
	End Sub
	
	Private Sub GrdFormacio_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdFormacio.MouseUp
		If eventArgs.Button = MouseButtons.Left And GrdFormacio.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdFormacio", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar historial formaci�",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar historial formaci�",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou historial formaci�",  , True,  ,  ,  ,  ,  , "mnuNew")
				If GrdFormacio.ActiveCell.Row < 1 Or GrdFormacio.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "GrdFormacio")
	End Sub
	
	Private Sub grdHistLaboral_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdHistLaboral.DoubleClick
		
		If grdHistLaboral.Selection.FirstRow >= 1 Then
			ObreHistLab()
		End If
	End Sub
	
	Private Sub grdHistLaboral_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdHistLaboral.MouseUp
		If eventArgs.Button = MouseButtons.Left And grdHistLaboral.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdHistLaboral", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar historial laboral",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar historial laboral",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou historial laboral",  , True,  ,  ,  ,  ,  , "mnuNew")
				If grdHistLaboral.ActiveCell.Row < 1 Or grdHistLaboral.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "grdHistLaboral")
		
	End Sub
	
	
	Private Sub GrdInsercions_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdInsercions.DoubleClick
		If GrdInsercions.Selection.FirstRow >= 1 Then
			ObreInsercions()
		End If
	End Sub
	
	Private Sub GrdInsercions_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdInsercions.MouseUp
		If eventArgs.Button = MouseButtons.Left And GrdInsercions.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdInsercions", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar inserci�",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar inserci�",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nova alta inserci�",  , True,  ,  ,  ,  ,  , "mnuNew")
				If GrdInsercions.ActiveCell.Row < 1 Or GrdInsercions.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "GrdInsercions")
	End Sub
	
	Private Sub GrdPrepLab_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdPrepLab.DoubleClick
		
		If GrdPrepLab.Selection.FirstRow >= 1 Then
			ObrePrepLab()
		End If
	End Sub
	
	Private Sub GrdPrepLab_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdPrepLab.MouseUp
		If eventArgs.Button = MouseButtons.Left And GrdPrepLab.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdPrepLab", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar programa preparaci� laboral",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar programa preparaci� laboral",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou programa preparaci� laboral",  , True,  ,  ,  ,  ,  , "mnuNew")
				If GrdPrepLab.ActiveCell.Row < 1 Or GrdPrepLab.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "GrdPrepLab")
	End Sub
	
	
	
	
	Private Sub GrdProgrames_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdProgrames.DoubleClick
		If grdProgrames.Selection.FirstRow >= 1 Then
			ObreProgrames()
		End If
	End Sub
	
	Private Sub GrdProgrames_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdProgrames.MouseUp
		If eventArgs.Button = MouseButtons.Left And grdProgrames.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdProgrames", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar programa usuari",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar programa usuari",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou programa usuari",  , True,  ,  ,  ,  ,  , "mnuNew")
				If grdProgrames.ActiveCell.Row < 1 Or grdProgrames.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "GrdProgrames")
	End Sub
	
	Private Sub GrdRelacions_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdRelacions.DoubleClick
		If GrdRelacions.Selection.FirstRow >= 1 Then
			ObreRelacions()
		End If
	End Sub
	
	Private Sub GrdRelacions_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdRelacions.MouseUp
		If eventArgs.Button = MouseButtons.Left And GrdRelacions.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdRelacions", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar relaci�",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar relaci�",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou relaci�",  , True,  ,  ,  ,  ,  , "mnuNew")
				If GrdRelacions.ActiveCell.Row < 1 Or GrdRelacions.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "GrdRelacions")
	End Sub
	
	Private Sub GrdSeguiment_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdSeguiment.DoubleClick
		If GrdSeguiment.MouseRow < 1 Then Exit Sub
		ObreSegLab()
	End Sub
	
	Private Sub GrdSeguiment_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdSeguiment.MouseUp
		If eventArgs.Button = MouseButtons.Left And GrdSeguiment.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdSeguiment", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar seguimient laboral",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar seguimient laboral",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou seguimient laboral",  , True,  ,  ,  ,  ,  , "mnuNew")
				If GrdSeguiment.ActiveCell.Row < 1 Or GrdSeguiment.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "GrdSeguiment")
	End Sub
	
	
	
	Private Sub grdServeis_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdServeis.DoubleClick
		
		If grdServeis.Selection.FirstRow >= 1 Then
			ObreServei()
		End If
	End Sub
	
	Private Sub grdServeis_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdServeis.MouseUp
		If eventArgs.Button = MouseButtons.Left And GrdSeguiment.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdServeis", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar servei usuari",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar servei usuari",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou servei usuari",  , True,  ,  ,  ,  ,  , "mnuNew")
				If grdServeis.ActiveCell.Row < 1 Or grdServeis.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "GrdServeis")
	End Sub
	
	
	
	
	
	Private Sub GrdServeisAct_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdServeisAct.MouseUp
		If eventArgs.Button = MouseButtons.Left And GrdServeisAct.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdServeisAct", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Fase Recollida Dades",  ,  , XPIcon("FlagGrey"),  ,  ,  ,  , "REC")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Fase Valoraci�",  ,  , XPIcon("BlueFlag"),  ,  ,  ,  , "VAL")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Fase Preparaci� Laboral",  ,  , XPIcon("FlagVV"),  ,  ,  ,  , "PRE")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Fase Intermediaci�",  ,  , XPIcon("GreenFlag"),  ,  ,  ,  , "INT")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Modificar Fases",  ,  , XPIcon("Escriu"),  ,  ,  ,  , "MOD")
			End With
		End With
		XpExecutaMenu(Me, "GrdServeisAct")
	End Sub
	
	
	
	Private Sub TabControl1_Selected(sender As Object, ByVal eventArgs As TabControlEventArgs) Handles TabControl1.Selected
		CarregaGrids((eventArgs.TabPageIndex))
	End Sub
	
	Private Sub txtCodigo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.GotFocus
		XGotFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.DoubleClick
		ConsultaTaula(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.LostFocus
		If txtCodigo.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodigo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	
	Private Sub txtDisponibilidadHorariaP_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDisponibilidadHorariaP.GotFocus
		XGotFocus(Me, txtDisponibilidadHorariaP)
	End Sub
	
	Private Sub txtDisponibilidadHorariaP_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDisponibilidadHorariaP.LostFocus
		XLostFocus(Me, txtDisponibilidadHorariaP)
	End Sub
	
	Private Sub txtDocMotivacionLaboral_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDocMotivacionLaboral.GotFocus
		XGotFocus(Me, txtDocMotivacionLaboral)
	End Sub
	
	Private Sub txtDocMotivacionLaboral_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDocMotivacionLaboral.LostFocus
		XLostFocus(Me, txtDocMotivacionLaboral)
	End Sub
	
	
	
	Private Sub txtGarantiaJ_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGarantiaJ.GotFocus
		XGotFocus(Me, txtGarantiaJ)
	End Sub
	
	Private Sub txtGarantiaJ_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGarantiaJ.LostFocus
		XLostFocus(Me, txtGarantiaJ)
	End Sub
	
	Private Sub txtIdoneidadPrograma_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIdoneidadPrograma.GotFocus
		XGotFocus(Me, txtIdoneidadPrograma)
	End Sub
	
	Private Sub txtIdoneidadPrograma_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIdoneidadPrograma.LostFocus
		XLostFocus(Me, txtIdoneidadPrograma)
	End Sub
	
	
	
	Private Sub txtIdoneidadProgramaN_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIdoneidadProgramaN.DoubleClick
		ConsultaTaula(Me, txtIdoneidadProgramaN)
	End Sub
	
	Private Sub txtIdoneidadProgramaN_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIdoneidadProgramaN.GotFocus
		XGotFocus(Me, txtIdoneidadProgramaN)
	End Sub
	
	Private Sub txtIdoneidadProgramaN_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIdoneidadProgramaN.LostFocus
		XLostFocus(Me, txtIdoneidadProgramaN)
	End Sub
	
	Private Sub txtIdoneidadProgramaN_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtIdoneidadProgramaN.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtNombre_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombre.GotFocus
		XGotFocus(Me, txtNombre)
	End Sub
	
	Private Sub txtNombre_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombre.LostFocus
		XLostFocus(Me, txtNombre)
	End Sub
	
	Private Sub txtNombre_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNombre.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtNombreCompleto_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreCompleto.GotFocus
		XGotFocus(Me, txtNombreCompleto)
	End Sub
	
	Private Sub txtNombreCompleto_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreCompleto.LostFocus
		XLostFocus(Me, txtNombreCompleto)
	End Sub
	
	Private Sub txtObservacionesPreferFor_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesPreferFor.GotFocus
		XGotFocus(Me, txtObservacionesPreferFor)
	End Sub
	
	Private Sub txtObservacionesPreferFor_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesPreferFor.LostFocus
		XLostFocus(Me, txtObservacionesPreferFor)
	End Sub
	
	
	
	Private Sub txtPerfilLaboralN_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPerfilLaboralN.GotFocus
		XGotFocus(Me, txtPerfilLaboralN)
	End Sub
	
	Private Sub txtPerfilLaboralN_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPerfilLaboralN.LostFocus
		XLostFocus(Me, txtPerfilLaboralN)
	End Sub
	
	Private Sub txtPoblacionNacimiento_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacionNacimiento.DoubleClick
		ConsultaTaula(Me, txtPoblacionNacimiento)
	End Sub
	
	Private Sub txtPoblacionNacimiento_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacionNacimiento.GotFocus
		XGotFocus(Me, txtPoblacionNacimiento)
	End Sub
	
	Private Sub txtPoblacionNacimiento_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacionNacimiento.LostFocus
		XLostFocus(Me, txtPoblacionNacimiento)
	End Sub
	
	Private Sub txtPoblacionNacimiento_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPoblacionNacimiento.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPrimerApellido_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrimerApellido.GotFocus
		XGotFocus(Me, txtPrimerApellido)
	End Sub
	
	Private Sub txtPrimerApellido_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrimerApellido.LostFocus
		XLostFocus(Me, txtPrimerApellido)
	End Sub
	
	Private Sub txtPrimerApellido_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPrimerApellido.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPrimeraPreferenciaFor_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrimeraPreferenciaFor.DoubleClick
		ConsultaTaula(Me, txtPrimeraPreferenciaFor)
	End Sub
	
	Private Sub txtPrimeraPreferenciaFor_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrimeraPreferenciaFor.GotFocus
		XGotFocus(Me, txtPrimeraPreferenciaFor)
	End Sub
	
	Private Sub txtPrimeraPreferenciaFor_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrimeraPreferenciaFor.LostFocus
		XLostFocus(Me, txtPrimeraPreferenciaFor)
	End Sub
	
	Private Sub txtPrimeraPreferenciaFor_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPrimeraPreferenciaFor.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	Private Sub txtPuntAtencio_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntAtencio.DoubleClick
		ConsultaTaula(Me, txtPuntAtencio)
	End Sub
	
	Private Sub txtPuntAtencio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntAtencio.GotFocus
		XGotFocus(Me, txtPuntAtencio)
	End Sub
	
	Private Sub txtPuntAtencio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntAtencio.LostFocus
		XLostFocus(Me, txtPuntAtencio)
	End Sub
	
	Private Sub txtPuntAtencio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPuntAtencio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSegonaPreferenciaFor_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSegonaPreferenciaFor.DoubleClick
		ConsultaTaula(Me, txtSegonaPreferenciaFor)
	End Sub
	
	Private Sub txtSegonaPreferenciaFor_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSegonaPreferenciaFor.GotFocus
		XGotFocus(Me, txtSegonaPreferenciaFor)
	End Sub
	
	Private Sub txtSegonaPreferenciaFor_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSegonaPreferenciaFor.LostFocus
		XLostFocus(Me, txtSegonaPreferenciaFor)
	End Sub
	
	Private Sub txtSegonaPreferenciaFor_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSegonaPreferenciaFor.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSegundoApellido_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSegundoApellido.GotFocus
		XGotFocus(Me, txtSegundoApellido)
	End Sub
	
	Private Sub txtSegundoApellido_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSegundoApellido.LostFocus
		XLostFocus(Me, txtSegundoApellido)
	End Sub
	
	Private Sub txtSegundoApellido_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSegundoApellido.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDireccion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDireccion.GotFocus
		XGotFocus(Me, txtDireccion)
	End Sub
	
	Private Sub txtDireccion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDireccion.LostFocus
		XLostFocus(Me, txtDireccion)
	End Sub
	
	Private Sub txtDireccion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDireccion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPoblacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacion.GotFocus
		XGotFocus(Me, txtPoblacion)
	End Sub
	
	Private Sub txtPoblacion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacion.DoubleClick
		ConsultaTaula(Me, txtPoblacion)
	End Sub
	
	Private Sub txtPoblacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacion.LostFocus
		XLostFocus(Me, txtPoblacion)
	End Sub
	
	Private Sub txtPoblacion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPoblacion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCodigoPostal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoPostal.GotFocus
		XGotFocus(Me, txtCodigoPostal)
	End Sub
	
	Private Sub txtCodigoPostal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigoPostal.LostFocus
		XLostFocus(Me, txtCodigoPostal)
	End Sub
	
	Private Sub txtCodigoPostal_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodigoPostal.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtNif_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNif.GotFocus
		XGotFocus(Me, txtNIF)
	End Sub
	
	Private Sub txtNif_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNif.LostFocus
		XLostFocus(Me, txtNIF)
	End Sub
	
	Private Sub txtNIF_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNIF.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbEstado_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstado.GotFocus
		XGotFocus(Me, cmbEstado)
	End Sub
	
	Private Sub cmbEstado_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstado.LostFocus
		XLostFocus(Me, cmbEstado)
	End Sub
	
	Private Sub cmbSexo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbSexo.GotFocus
		XGotFocus(Me, cmbSexo)
	End Sub
	
	Private Sub cmbSexo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbSexo.LostFocus
		XLostFocus(Me, cmbSexo)
	End Sub
	
	Private Sub txtFechaNacimiento_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaNacimiento.GotFocus
		XGotFocus(Me, txtFechaNacimiento)
	End Sub
	
	Private Sub txtFechaNacimiento_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaNacimiento.LostFocus
		XLostFocus(Me, txtFechaNacimiento)
	End Sub
	
	
	Private Sub txtTecnico_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnico.DoubleClick
		ConsultaTaula(Me, txtTecnico)
	End Sub
	
	Private Sub txtTecnico_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnico.GotFocus
		XGotFocus(Me, txtTecnico)
	End Sub
	
	Private Sub txtTecnico_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnico.LostFocus
		XLostFocus(Me, txtTecnico)
	End Sub
	
	Private Sub txtTecnico_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTecnico.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTelefono_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefono.GotFocus
		XGotFocus(Me, txtTelefono)
	End Sub
	
	Private Sub txtTelefono_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefono.LostFocus
		XLostFocus(Me, txtTelefono)
	End Sub
	
	Private Sub txtTelefono_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTelefono.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTelefonoMovil_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefonoMovil.GotFocus
		XGotFocus(Me, txtTelefonoMovil)
	End Sub
	
	Private Sub txtTelefonoMovil_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefonoMovil.LostFocus
		XLostFocus(Me, txtTelefonoMovil)
	End Sub
	
	Private Sub txtTelefonoMovil_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTelefonoMovil.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEmail_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmail.GotFocus
		XGotFocus(Me, txtEmail)
	End Sub
	
	Private Sub txtEmail_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmail.LostFocus
		XLostFocus(Me, txtEmail)
	End Sub
	
	Private Sub txtEmail_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmail.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaInicio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaInicio.GotFocus
		XGotFocus(Me, txtFechaInicio)
	End Sub
	
	Private Sub txtFechaInicio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaInicio.LostFocus
		XLostFocus(Me, txtFechaInicio)
	End Sub
	
	Private Sub txtServicioInicio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServicioInicio.GotFocus
		XGotFocus(Me, txtServicioInicio)
	End Sub
	
	Private Sub txtServicioInicio_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServicioInicio.DoubleClick
		ConsultaTaula(Me, txtServicioInicio)
	End Sub
	
	Private Sub txtServicioInicio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServicioInicio.LostFocus
		XLostFocus(Me, txtServicioInicio)
	End Sub
	
	Private Sub txtServicioInicio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtServicioInicio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaFin_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaFin.GotFocus
		XGotFocus(Me, txtFechaFin)
	End Sub
	
	Private Sub txtFechaFin_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaFin.LostFocus
		XLostFocus(Me, txtFechaFin)
	End Sub
	
	Private Sub txtMotivoBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoBaja.GotFocus
		XGotFocus(Me, txtMotivoBaja)
	End Sub
	
	Private Sub txtMotivoBaja_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoBaja.DoubleClick
		ConsultaTaula(Me, txtMotivoBaja)
	End Sub
	
	Private Sub txtMotivoBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoBaja.LostFocus
		XLostFocus(Me, txtMotivoBaja)
	End Sub
	
	Private Sub txtMotivoBaja_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMotivoBaja.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDemandaInicial_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDemandaInicial.GotFocus
		XGotFocus(Me, txtDemandaInicial)
	End Sub
	
	Private Sub txtDemandaInicial_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDemandaInicial.LostFocus
		XLostFocus(Me, txtDemandaInicial)
	End Sub
	
	Private Sub txtDemandaInicial_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDemandaInicial.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservaciones_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservaciones.GotFocus
		XGotFocus(Me, txtObservaciones)
	End Sub
	
	Private Sub txtObservaciones_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservaciones.LostFocus
		XLostFocus(Me, txtObservaciones)
	End Sub
	
	Private Sub txtObservaciones_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservaciones.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtColectivo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtColectivo.GotFocus
		XGotFocus(Me, txtColectivo)
	End Sub
	
	Private Sub txtColectivo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtColectivo.DoubleClick
		ConsultaTaula(Me, txtColectivo)
	End Sub
	
	Private Sub txtColectivo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtColectivo.LostFocus
		XLostFocus(Me, txtColectivo)
	End Sub
	
	Private Sub txtColectivo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtColectivo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub chkTieneCertificadoDiscap_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTieneCertificadoDiscap.GotFocus
		XGotFocus(Me, chkTieneCertificadoDiscap)
	End Sub
	
	Private Sub chkTieneCertificadoDiscap_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTieneCertificadoDiscap.LostFocus
		XLostFocus(Me, chkTieneCertificadoDiscap)
	End Sub
	
	Private Sub txtGradoDiscapacidad_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGradoDiscapacidad.GotFocus
		XGotFocus(Me, txtGradoDiscapacidad)
	End Sub
	
	Private Sub txtGradoDiscapacidad_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGradoDiscapacidad.LostFocus
		XLostFocus(Me, txtGradoDiscapacidad)
	End Sub
	
	Private Sub txtGradoDiscapacidad_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtGradoDiscapacidad.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDiagnostico_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDiagnostico.GotFocus
		XGotFocus(Me, txtDiagnostico)
	End Sub
	
	Private Sub txtDiagnostico_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDiagnostico.LostFocus
		XLostFocus(Me, txtDiagnostico)
	End Sub
	
	Private Sub txtDiagnostico_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDiagnostico.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub chkPrestacionEconomica_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPrestacionEconomica.GotFocus
		XGotFocus(Me, chkPrestacionEconomica)
	End Sub
	
	Private Sub chkPrestacionEconomica_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPrestacionEconomica.LostFocus
		XLostFocus(Me, chkPrestacionEconomica)
	End Sub
	
	
	Private Sub txtTerceraPreferenciaFor_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTerceraPreferenciaFor.DoubleClick
		ConsultaTaula(Me, txtTerceraPreferenciaFor)
	End Sub
	
	Private Sub txtTerceraPreferenciaFor_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTerceraPreferenciaFor.GotFocus
		XGotFocus(Me, txtTerceraPreferenciaFor)
	End Sub
	
	Private Sub txtTerceraPreferenciaFor_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTerceraPreferenciaFor.LostFocus
		XLostFocus(Me, txtTerceraPreferenciaFor)
	End Sub
	
	Private Sub txtTerceraPreferenciaFor_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTerceraPreferenciaFor.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipoPrestacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoPrestacion.GotFocus
		XGotFocus(Me, txtTipoPrestacion)
	End Sub
	
	Private Sub txtTipoPrestacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoPrestacion.LostFocus
		XLostFocus(Me, txtTipoPrestacion)
	End Sub
	
	Private Sub chkInscripcionOtg_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkInscripcionOtg.GotFocus
		XGotFocus(Me, chkInscripcionOtg)
	End Sub
	
	Private Sub chkInscripcionOtg_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkInscripcionOtg.LostFocus
		XLostFocus(Me, chkInscripcionOtg)
	End Sub
	
	Private Sub txtFechaOtg_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaOtg.GotFocus
		XGotFocus(Me, txtFechaOtg)
	End Sub
	
	Private Sub txtFechaOtg_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaOtg.LostFocus
		XLostFocus(Me, txtFechaOtg)
	End Sub
	
	Private Sub txtLimitaciones_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLimitaciones.GotFocus
		XGotFocus(Me, txtLimitaciones)
	End Sub
	
	Private Sub txtLimitaciones_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLimitaciones.LostFocus
		XLostFocus(Me, txtLimitaciones)
	End Sub
	
	Private Sub txtLimitaciones_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtLimitaciones.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCondicionantesEntorno_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCondicionantesEntorno.GotFocus
		XGotFocus(Me, txtCondicionantesEntorno)
	End Sub
	
	Private Sub txtCondicionantesEntorno_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCondicionantesEntorno.LostFocus
		XLostFocus(Me, txtCondicionantesEntorno)
	End Sub
	
	Private Sub txtCondicionantesEntorno_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCondicionantesEntorno.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbTransporte_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTransporte.GotFocus
		XGotFocus(Me, cmbTransporte)
	End Sub
	
	Private Sub cmbTransporte_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTransporte.LostFocus
		XLostFocus(Me, cmbTransporte)
	End Sub
	
	Private Sub cmbValoracionSoc_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbValoracionSoc.GotFocus
		XGotFocus(Me, cmbValoracionSoc)
	End Sub
	
	Private Sub cmbValoracionSoc_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbValoracionSoc.LostFocus
		XLostFocus(Me, cmbValoracionSoc)
	End Sub
	
	Private Sub txtGradoOcupabilidad_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGradoOcupabilidad.GotFocus
		XGotFocus(Me, txtGradoOcupabilidad)
	End Sub
	
	Private Sub txtGradoOcupabilidad_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGradoOcupabilidad.LostFocus
		XLostFocus(Me, txtGradoOcupabilidad)
	End Sub
	
	Private Sub txtGradoOcupabilidad_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtGradoOcupabilidad.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtValoracionTecnica_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValoracionTecnica.GotFocus
		XGotFocus(Me, txtValoracionTecnica)
	End Sub
	
	Private Sub txtValoracionTecnica_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValoracionTecnica.LostFocus
		XLostFocus(Me, txtValoracionTecnica)
	End Sub
	
	Private Sub txtValoracionTecnica_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtValoracionTecnica.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFormacionReglada_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFormacionReglada.GotFocus
		XGotFocus(Me, txtFormacionReglada)
	End Sub
	
	Private Sub txtFormacionReglada_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFormacionReglada.DoubleClick
		ConsultaTaula(Me, txtFormacionReglada)
	End Sub
	
	Private Sub txtFormacionReglada_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFormacionReglada.LostFocus
		XLostFocus(Me, txtFormacionReglada)
	End Sub
	
	Private Sub txtFormacionReglada_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFormacionReglada.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEspecialidad_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEspecialidad.GotFocus
		XGotFocus(Me, txtEspecialidad)
	End Sub
	
	Private Sub txtEspecialidad_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEspecialidad.LostFocus
		XLostFocus(Me, txtEspecialidad)
	End Sub
	
	Private Sub txtEspecialidad_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEspecialidad.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFormacionCom_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFormacionCom.GotFocus
		XGotFocus(Me, txtFormacionCom)
	End Sub
	
	Private Sub txtFormacionCom_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFormacionCom.LostFocus
		XLostFocus(Me, txtFormacionCom)
	End Sub
	
	Private Sub txtFormacionCom_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFormacionCom.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub chkCatalan_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkCatalan.GotFocus
		XGotFocus(Me, chkCatalan)
	End Sub
	
	Private Sub chkCatalan_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkCatalan.LostFocus
		XLostFocus(Me, chkCatalan)
	End Sub
	
	Private Sub chkCastellano_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkCastellano.GotFocus
		XGotFocus(Me, chkCastellano)
	End Sub
	
	Private Sub chkCastellano_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkCastellano.LostFocus
		XLostFocus(Me, chkCastellano)
	End Sub
	
	Private Sub chkIngles_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkIngles.GotFocus
		XGotFocus(Me, chkIngles)
	End Sub
	
	Private Sub chkIngles_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkIngles.LostFocus
		XLostFocus(Me, chkIngles)
	End Sub
	
	Private Sub chkFrances_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkFrances.GotFocus
		XGotFocus(Me, chkFrances)
	End Sub
	
	Private Sub chkFrances_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkFrances.LostFocus
		XLostFocus(Me, chkFrances)
	End Sub
	
	Private Sub txtOtros_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOtros.GotFocus
		XGotFocus(Me, txtOtros)
	End Sub
	
	Private Sub txtOtros_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOtros.LostFocus
		XLostFocus(Me, txtOtros)
	End Sub
	
	Private Sub txtOtros_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtOtros.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub chkWord_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkWord.GotFocus
		XGotFocus(Me, chkWord)
	End Sub
	
	Private Sub chkWord_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkWord.LostFocus
		XLostFocus(Me, chkWord)
	End Sub
	
	Private Sub chkAcces_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkAcces.GotFocus
		XGotFocus(Me, chkAcces)
	End Sub
	
	Private Sub chkAcces_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkAcces.LostFocus
		XLostFocus(Me, chkAcces)
	End Sub
	
	Private Sub chkExcel_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkExcel.GotFocus
		XGotFocus(Me, chkExcel)
	End Sub
	
	Private Sub chkExcel_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkExcel.LostFocus
		XLostFocus(Me, chkExcel)
	End Sub
	
	Private Sub chkInternet_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkInternet.GotFocus
		XGotFocus(Me, chkInternet)
	End Sub
	
	Private Sub chkInternet_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkInternet.LostFocus
		XLostFocus(Me, chkInternet)
	End Sub
	
	Private Sub chkPowerpoint_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPowerpoint.GotFocus
		XGotFocus(Me, chkPowerpoint)
	End Sub
	
	Private Sub chkPowerpoint_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPowerpoint.LostFocus
		XLostFocus(Me, chkPowerpoint)
	End Sub
	
	Private Sub txtOtrosProgramas_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOtrosProgramas.GotFocus
		XGotFocus(Me, txtOtrosProgramas)
	End Sub
	
	Private Sub txtOtrosProgramas_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOtrosProgramas.LostFocus
		XLostFocus(Me, txtOtrosProgramas)
	End Sub
	
	Private Sub txtOtrosProgramas_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtOtrosProgramas.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub chkTieneExperienciaLab_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTieneExperienciaLab.GotFocus
		XGotFocus(Me, chkTieneExperienciaLab)
	End Sub
	
	Private Sub chkTieneExperienciaLab_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkTieneExperienciaLab.LostFocus
		XLostFocus(Me, chkTieneExperienciaLab)
	End Sub
	
	Private Sub txtCurriculumVitae_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCurriculumVitae.GotFocus
		XGotFocus(Me, txtCurriculumVitae)
	End Sub
	
	Private Sub txtCurriculumVitae_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCurriculumVitae.LostFocus
		XLostFocus(Me, txtCurriculumVitae)
	End Sub
	
	Private Sub txtCurriculumVitae_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCurriculumVitae.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPrimeraPreferenciaTrab_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrimeraPreferenciaTrab.GotFocus
		XGotFocus(Me, txtPrimeraPreferenciaTrab)
	End Sub
	
	Private Sub txtPrimeraPreferenciaTrab_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrimeraPreferenciaTrab.DoubleClick
		ConsultaTaula(Me, txtPrimeraPreferenciaTrab)
	End Sub
	
	Private Sub txtPrimeraPreferenciaTrab_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrimeraPreferenciaTrab.LostFocus
		XLostFocus(Me, txtPrimeraPreferenciaTrab)
	End Sub
	
	Private Sub txtPrimeraPreferenciaTrab_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPrimeraPreferenciaTrab.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSegundaPreferenciaTrab_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSegundaPreferenciaTrab.GotFocus
		XGotFocus(Me, txtSegundaPreferenciaTrab)
	End Sub
	
	Private Sub txtSegundaPreferenciaTrab_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSegundaPreferenciaTrab.DoubleClick
		ConsultaTaula(Me, txtSegundaPreferenciaTrab)
	End Sub
	
	Private Sub txtSegundaPreferenciaTrab_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSegundaPreferenciaTrab.LostFocus
		XLostFocus(Me, txtSegundaPreferenciaTrab)
	End Sub
	
	Private Sub txtSegundaPreferenciaTrab_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSegundaPreferenciaTrab.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTerceraPreferenciaTrab_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTerceraPreferenciaTrab.GotFocus
		XGotFocus(Me, txtTerceraPreferenciaTrab)
	End Sub
	
	Private Sub txtTerceraPreferenciaTrab_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTerceraPreferenciaTrab.DoubleClick
		ConsultaTaula(Me, txtTerceraPreferenciaTrab)
	End Sub
	
	Private Sub txtTerceraPreferenciaTrab_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTerceraPreferenciaTrab.LostFocus
		XLostFocus(Me, txtTerceraPreferenciaTrab)
	End Sub
	
	Private Sub txtTerceraPreferenciaTrab_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTerceraPreferenciaTrab.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservacionesPref_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesPref.GotFocus
		XGotFocus(Me, txtObservacionesPref)
	End Sub
	
	Private Sub txtObservacionesPref_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesPref.LostFocus
		XLostFocus(Me, txtObservacionesPref)
	End Sub
	
	Private Sub txtObservacionesPref_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservacionesPref.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCaracteristicasTrabajo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCaracteristicasTrabajo.GotFocus
		XGotFocus(Me, txtCaracteristicasTrabajo)
	End Sub
	
	Private Sub txtCaracteristicasTrabajo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCaracteristicasTrabajo.LostFocus
		XLostFocus(Me, txtCaracteristicasTrabajo)
	End Sub
	
	Private Sub txtCaracteristicasTrabajo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCaracteristicasTrabajo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbPreferenciaEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPreferenciaEmpresa.GotFocus
		XGotFocus(Me, cmbPreferenciaEmpresa)
	End Sub
	
	Private Sub cmbPreferenciaEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPreferenciaEmpresa.LostFocus
		XLostFocus(Me, cmbPreferenciaEmpresa)
	End Sub
	
	Private Sub cmbPreferenciaJornada_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPreferenciaJornada.GotFocus
		XGotFocus(Me, cmbPreferenciaJornada)
	End Sub
	
	Private Sub cmbPreferenciaJornada_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPreferenciaJornada.LostFocus
		XLostFocus(Me, cmbPreferenciaJornada)
	End Sub
	
	Private Sub txtSueldoAproximado_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSueldoAproximado.GotFocus
		XGotFocus(Me, txtSueldoAproximado)
	End Sub
	
	Private Sub txtSueldoAproximado_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSueldoAproximado.LostFocus
		XLostFocus(Me, txtSueldoAproximado)
	End Sub
	
	Private Sub txtSueldoAproximado_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSueldoAproximado.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtIncorporacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIncorporacion.GotFocus
		XGotFocus(Me, txtIncorporacion)
	End Sub
	
	Private Sub txtIncorporacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtIncorporacion.LostFocus
		XLostFocus(Me, txtIncorporacion)
	End Sub
	
	Private Sub txtIncorporacion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtIncorporacion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDisponibilidadGeograf_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDisponibilidadGeograf.GotFocus
		XGotFocus(Me, txtDisponibilidadGeograf)
	End Sub
	
	Private Sub txtDisponibilidadGeograf_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDisponibilidadGeograf.LostFocus
		XLostFocus(Me, txtDisponibilidadGeograf)
	End Sub
	
	Private Sub txtDisponibilidadGeograf_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDisponibilidadGeograf.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservacionesOtros_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesOtros.GotFocus
		XGotFocus(Me, txtObservacionesOtros)
	End Sub
	
	Private Sub txtObservacionesOtros_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesOtros.LostFocus
		XLostFocus(Me, txtObservacionesOtros)
	End Sub
	
	Private Sub txtObservacionesOtros_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservacionesOtros.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbNivelHabilidades_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNivelHabilidades.GotFocus
		XGotFocus(Me, cmbNivelHabilidades)
	End Sub
	
	Private Sub cmbNivelHabilidades_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbNivelHabilidades.LostFocus
		XLostFocus(Me, cmbNivelHabilidades)
	End Sub
	
	Private Sub CarregaHistoricLaboral()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(grdHistLaboral, "CGHISLAB^INSERCIO", txtCodigo.Text)
	End Sub
	
	Private Sub CarregaHistoricFormacio()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(GrdFormacio, "CGHISFORM^INSERCIO", txtCodigo.Text)
	End Sub
	
	Private Sub CarregaPrepLaboral()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(GrdPrepLab, "CGPLAB^INSERCIO", txtCodigo.Text)
	End Sub
	
	Private Sub CarregaSeguiment()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(GrdSeguiment, "CGSEG^INSERCIO", txtCodigo.Text)
	End Sub
	
	Private Sub CarregaServeis()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(grdServeis, "CGSERV^INSERCIO", txtCodigo.Text)
	End Sub
	
	Private Sub CarregaProgrames()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(grdProgrames, "CGPROGU^INSERCIO", txtCodigo.Text)
	End Sub
	
	Private Sub CarregaServeisActius()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(GrdServeisAct, "CGSERVACT^INSERCIO", txtCodigo.Text)
	End Sub
	
	Private Sub CarregaRelacions()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(GrdRelacions, "CGRELACIONS^INSERCIO", txtCodigo.Text)
	End Sub
	
	Private Sub CarregaInsercions()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(GrdInsercions, "CGINSERCIONS^INSERCIO", txtCodigo.Text)
	End Sub
	
	
	Private Sub CarregaGrids(ByRef curtab As Short)
		Select Case curtab
			Case 0
				CarregaServeisActius()
			Case 2
				CarregaHistoricLaboral()
				CarregaHistoricFormacio()
			Case 10
				CarregaPrepLaboral()
			Case 11
				CarregaServeis()
			Case 9
				CarregaProgrames()
			Case 1
				CarregaRelacions()
			Case 8
				CarregaSeguiment()
			Case 12
				CarregaInsercions()
				
		End Select
	End Sub
	
	Private Sub ObreHistLab()
		Dim cod As String
		Dim Data As String
		Dim Num As Short
		
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		cod = Piece(grdHistLaboral.Cell(grdHistLaboral.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(grdHistLaboral.Cell(grdHistLaboral.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Num = Piece(grdHistLaboral.Cell(grdHistLaboral.ActiveCell.Row, 1).Text, "|", 3)
		ObreFormulari(frmHistorialLaboral, "frmHistorialLaboral", cod & S & Data & S & Num)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	Private Sub ObreHistForm()
		Dim cod As String
		Dim Data As String
		Dim Num As Short
		
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		cod = Piece(GrdFormacio.Cell(GrdFormacio.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(GrdFormacio.Cell(GrdFormacio.ActiveCell.Row, 1).Text, "|", 2)
		ObreFormulari(frmUsuariInsercioFormacio, "frmUsuariInsercioFormacio", cod & S & Data)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	Private Sub ObrePrepLab()
		Dim Us As Short
		Dim Data As String
		Dim Prog As Short
		
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Us = Piece(GrdPrepLab.Cell(GrdPrepLab.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Prog = Piece(GrdPrepLab.Cell(GrdPrepLab.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(GrdPrepLab.Cell(GrdPrepLab.ActiveCell.Row, 1).Text, "|", 3)
		ObreFormulari(frmUsuariPreparacioLaboral, "frmUsuariPreparacioLaboral", Us & S & Prog & S & Data)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	Private Sub ObreSegLab()
		Dim Us As Short
		Dim Data As String
		Dim Num As Short
		
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Us = Piece(GrdSeguiment.Cell(GrdSeguiment.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(GrdSeguiment.Cell(GrdSeguiment.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Num = Piece(GrdSeguiment.Cell(GrdSeguiment.ActiveCell.Row, 1).Text, "|", 3)
		'frmSeguimentUsuari.usuariExt = Us
		ObreFormulari(frmSeguimentUsuari, "frmSeguimentUsuari", Us & S & Data & S & Num)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	Private Sub ObreServei()
		Dim Us As Short
		Dim SER As Short
		Dim Data As String
		
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Us = Piece(grdServeis.Cell(grdServeis.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SER = Piece(grdServeis.Cell(grdServeis.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(grdServeis.Cell(grdServeis.ActiveCell.Row, 1).Text, "|", 3)
		'frmServeisUsuaris.usuariExt = Us
		'frmServeisUsuaris.serveiExt = SER
		ObreFormulari(frmServeisUsuaris, "frmServeisUsuaris", Us & S & SER & S & Data)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	Private Sub ObreRelacions()
		Dim Us As Short
		Dim Data As String
		Dim Num As Short
		
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Us = Piece(GrdRelacions.Cell(GrdRelacions.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(GrdRelacions.Cell(GrdRelacions.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Num = Piece(GrdRelacions.Cell(GrdRelacions.ActiveCell.Row, 1).Text, "|", 3)
		ObreFormulari(frmRelacions, "frmRelacions", Us & S & Data & S & Num)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	Private Sub ObreProgrames()
		Dim Us As Short
		Dim Data As String
		Dim Num As Short
		Dim Programa As Short
		Dim Servei As Short
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Us = Piece(grdProgrames.Cell(grdProgrames.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Servei = Piece(grdProgrames.Cell(grdProgrames.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Programa = Piece(grdProgrames.Cell(grdProgrames.ActiveCell.Row, 1).Text, "|", 3)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(grdProgrames.Cell(grdProgrames.ActiveCell.Row, 1).Text, "|", 4)
		ObreFormulari(frmUsuariPrograma, "frmUsuariPrograma", Us & S & Servei & S & Programa & S & Data)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
	End Sub
	
	Private Sub ObreInsercions()
		Dim Us As Short
		Dim Ent As String
		Dim Suc As Short
		Dim Data As String
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Ent = Piece(GrdInsercions.Cell(GrdInsercions.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Suc = Piece(GrdInsercions.Cell(GrdInsercions.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Us = Piece(GrdInsercions.Cell(GrdInsercions.ActiveCell.Row, 1).Text, "|", 3)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(GrdInsercions.Cell(GrdInsercions.ActiveCell.Row, 1).Text, "|", 4)
		ObreFormulari(frmEmpresaInseridora, "frmEmpresaInseridora", Ent & S & Suc & S & Us & S & Data)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	
	Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		

		
		Dim SER As String
		Dim Data As String
		Select Case NomMenu
			Case "grdHistLaboral"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObreHistLab()
						LlibAplicacio.DeleteReg(frmHistorialLaboral)
						CarregaHistoricLaboral()
					Case "mnuEdit"
						ObreHistLab()
					Case "mnuNew"
						NouHistLab()
				End Select
			Case "GrdFormacio"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObreHistForm()
						LlibAplicacio.DeleteReg(frmUsuariInsercioFormacio)
						CarregaHistoricFormacio()
					Case "mnuEdit"
						ObreHistForm()
					Case "mnuNew"
						NouHistForm()
				End Select
			Case "GrdPrepLab"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObrePrepLab()
						LlibAplicacio.DeleteReg(frmUsuariPreparacioLaboral)
						CarregaPrepLaboral()
					Case "mnuEdit"
						ObrePrepLab()
					Case "mnuNew"
						NouPrepLab()
				End Select
			Case "GrdSeguiment"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObreSegLab()
						LlibAplicacio.DeleteReg(frmSeguimentUsuari)
						CarregaSeguiment()
					Case "mnuEdit"
						ObreSegLab()
					Case "mnuNew"
						NouSegLab()
				End Select
			Case "GrdServeis"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObreServei()
						LlibAplicacio.DeleteReg(frmServeisUsuaris)
						CarregaServeis()
					Case "mnuEdit"
						ObreServei()
					Case "mnuNew"
						NouServei()
				End Select
			Case "GrdRelacions"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObreRelacions()
						LlibAplicacio.DeleteReg(frmRelacions)
						CarregaRelacions()
					Case "mnuEdit"
						ObreRelacions()
					Case "mnuNew"
						NovaRelacions()
				End Select
			Case "GrdProgrames"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObreProgrames()
						LlibAplicacio.DeleteReg(frmUsuariPrograma)
						CarregaProgrames()
					Case "mnuEdit"
						ObreProgrames()
					Case "mnuNew"
						NouPrograma()
				End Select
			Case "GrdInsercions"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObreInsercions()
						LlibAplicacio.DeleteReg(frmEmpresaInseridora)
						CarregaInsercions()
					Case "mnuEdit"
						ObreInsercions()
					Case "mnuNew"
						NovaInsercio()
				End Select
			Case "cmdBuscaIndex"
				If NDoc = 1 Then
					Select Case KeyMenu
						Case "Pla"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							CrearPla(PATHGEN, Path, (MDI.CommonDialog1), txtCurriculumVitae, Me.Text)
						Case "Wor"
							CrearWord(PATHGEN, Path, txtCurriculumVitae, Me.Text)
						Case "Nou"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							ImportarDoc(Path, (MDI.CommonDialog1), txtCurriculumVitae)
						Case "Obr"
							If txtCurriculumVitae.Text = "" Then Exit Sub

							If System.IO.File.Exists(PATHGEN & txtCurriculumVitae.Text) = False Then
								ObreDocument(txtCurriculumVitae.Text, Path)
							Else
								ObreDocument(txtCurriculumVitae.Text, PATHGEN)
							End If
						Case "COP"
							CopiarC(PATHGEN, Path, txtCurriculumVitae, Me.Text)
						Case "Del"
							BorraDoc(PATHGEN, Path, txtCurriculumVitae, Me.Text)
					End Select
				End If
				If NDoc = 2 Then
					Select Case KeyMenu
						Case "Pla"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							CrearPla(PATHGENU, PathU, (MDI.CommonDialog1), txtPai, Me.Text)
						Case "Wor"
							CrearWord(PATHGENU, PathU, txtPai, Me.Text)
						Case "Nou"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							ImportarDoc(PathU, (MDI.CommonDialog1), txtPai)
						Case "Obr"
							If txtPai.Text = "" Then Exit Sub

							If System.IO.File.Exists(PATHGENU & txtPai.Text) = False Then
								ObreDocument(txtPai.Text, Path)
							Else
								ObreDocument(txtPai.Text, PATHGENU)
							End If
						Case "COP"
							CopiarC(PATHGENU, PathU, txtPai, Me.Text)
						Case "Del"
							BorraDoc(PATHGENU, PathU, txtPai, Me.Text)
					End Select
					
				End If
				
				If NDoc = 3 Then
					Select Case KeyMenu
						Case "Pla"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							CrearPla(PATHGENU, PathU, (MDI.CommonDialog1), txtGradoOcupabilidadDoc, Me.Text)
						Case "Wor"
							CrearWord(PATHGENU, PathU, txtGradoOcupabilidadDoc, Me.Text)
						Case "Nou"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							ImportarDoc(PathU, (MDI.CommonDialog1), txtGradoOcupabilidadDoc)
						Case "Obr"
							If txtGradoOcupabilidadDoc.Text = "" Then Exit Sub

							If System.IO.File.Exists(PATHGENU & txtGradoOcupabilidadDoc.Text) = False Then
								ObreDocument(txtGradoOcupabilidadDoc.Text, Path)
							Else
								ObreDocument(txtGradoOcupabilidadDoc.Text, PATHGENU)
							End If
						Case "COP"
							CopiarC(PATHGENU, PathU, txtGradoOcupabilidadDoc, Me.Text)
						Case "Del"
							BorraDoc(PATHGENU, PathU, txtGradoOcupabilidadDoc, Me.Text)
					End Select
					
				End If
				
				If NDoc = 4 Then
					Select Case KeyMenu
						Case "Pla"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							CrearPla(PATHGENU, PathU, (MDI.CommonDialog1), txtGencat, Me.Text)
						Case "Wor"
							CrearWord(PATHGENU, PathU, txtGencat, Me.Text)
						Case "Nou"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							ImportarDoc(PathU, (MDI.CommonDialog1), txtGencat)
						Case "Obr"
							If txtGencat.Text = "" Then Exit Sub

							If System.IO.File.Exists(PATHGENU & txtGencat.Text) = False Then
								ObreDocument(txtGencat.Text, PathU)
							Else
								ObreDocument(txtGencat.Text, PATHGENU)
							End If
						Case "COP"
							CopiarC(PATHGENU, PathU, txtGencat, Me.Text)
						Case "Del"
							BorraDoc(PATHGENU, PathU, txtGencat, Me.Text)
					End Select
					
				End If
				
				If NDoc = 5 Then
					Select Case KeyMenu
						Case "Pla"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							CrearPla(PATHGENU, PathU, (MDI.CommonDialog1), txtICAP, Me.Text)
						Case "Wor"
							CrearWord(PATHGENU, PathU, txtICAP, Me.Text)
						Case "Nou"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							ImportarDoc(PathU, (MDI.CommonDialog1), txtICAP)
						Case "Obr"
							If txtICAP.Text = "" Then Exit Sub

							If System.IO.File.Exists(PATHGENU & txtICAP.Text) = False Then
								ObreDocument(txtICAP.Text, PathU)
							Else
								ObreDocument(txtICAP.Text, PATHGENU)
							End If
						Case "COP"
							CopiarC(PATHGENU, PathU, txtICAP, Me.Text)
						Case "Del"
							BorraDoc(PATHGENU, PathU, txtICAP, Me.Text)
					End Select
					
				End If
				
				If NDoc = 6 Then
					Select Case KeyMenu
						Case "Pla"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							CrearPla(PATHGENU, PathU, (MDI.CommonDialog1), txtPerfilLaboral, Me.Text)
						Case "Wor"
							CrearWord(PATHGENU, PathU, txtPerfilLaboral, Me.Text)
						Case "Nou"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							ImportarDoc(PathU, (MDI.CommonDialog1), txtPerfilLaboral)
						Case "Obr"
							If txtPerfilLaboral.Text = "" Then Exit Sub

							If System.IO.File.Exists(PATHGENU & txtPerfilLaboral.Text) = False Then
								ObreDocument(txtPerfilLaboral.Text, PathU)
							Else
								ObreDocument(txtPerfilLaboral.Text, PATHGENU)
							End If
						Case "COP"
							CopiarC(PATHGENU, PathU, txtPerfilLaboral, Me.Text)
						Case "Del"
							BorraDoc(PATHGENU, PathU, txtPerfilLaboral, Me.Text)
					End Select
					
				End If
				
				If NDoc = 7 Then
					Select Case KeyMenu
						Case "Pla"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							CrearPla(PATHGENU, PathU, (MDI.CommonDialog1), TxtFunciona, Me.Text)
						Case "Wor"
							CrearWord(PATHGENU, PathU, TxtFunciona, Me.Text)
						Case "Nou"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							ImportarDoc(PathU, (MDI.CommonDialog1), TxtFunciona)
						Case "Obr"
							If TxtFunciona.Text = "" Then Exit Sub

							If System.IO.File.Exists(PATHGENU & TxtFunciona.Text) = False Then
								ObreDocument(TxtFunciona.Text, PathU)
							Else
								ObreDocument(TxtFunciona.Text, PATHGENU)
							End If
						Case "COP"
							CopiarC(PATHGENU, PathU, TxtFunciona, Me.Text)
						Case "Del"
							BorraDoc(PATHGENU, PathU, TxtFunciona, Me.Text)
					End Select
					
				End If
				
				If NDoc = 8 Then
					Select Case KeyMenu
						Case "Pla"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							CrearPla(PATHGENU, PathU, (MDI.CommonDialog1), txtDocMotivacionLaboral, Me.Text)
						Case "Wor"
							CrearWord(PATHGENU, PathU, txtDocMotivacionLaboral, Me.Text)
						Case "Nou"
							'UPGRADE_ISSUE: El control MSComDlg.CommonDialog CommonDialog1 no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E047632-2D91-44D6-B2A3-0801707AF686"'
							ImportarDoc(PathU, (MDI.CommonDialog1), txtDocMotivacionLaboral)
						Case "Obr"
							If txtDocMotivacionLaboral.Text = "" Then Exit Sub

							If System.IO.File.Exists(PATHGENU & txtDocMotivacionLaboral.Text) = False Then
								ObreDocument(txtDocMotivacionLaboral.Text, PathU)
							Else
								ObreDocument(txtDocMotivacionLaboral.Text, PATHGENU)
							End If
						Case "COP"
							CopiarC(PATHGENU, PathU, txtDocMotivacionLaboral, Me.Text)
						Case "Del"
							BorraDoc(PATHGENU, PathU, txtDocMotivacionLaboral, Me.Text)
					End Select
					
				End If
			Case "GrdServeisAct"
				frmServeisUsuaris.Unload()
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				SER = Piece(GrdServeisAct.Cell(GrdServeisAct.ActiveCell.Row, 1).Text, "|", 2)
				CacheNetejaParametres()
				MCache.P1 = txtCodigo.Text
				MCache.P2 = SER
				If SER = "" Then
					xMsgBox("Has de sel�leccionar el servei", MsgBoxStyle.Information, Me.Text)
					Exit Sub
				End If
				Select Case KeyMenu
					Case "REC"
						CacheXecute("D FASES^INSERCIO(""REC"")")
					Case "VAL"
						CacheXecute("D FASES^INSERCIO(""VAL"")")
					Case "PRE"
						CacheXecute("D FASES^INSERCIO(""PRE"")")
					Case "INT"
						CacheXecute("D FASES^INSERCIO(""INT"")")
					Case "MOD"
						frmServeisUsuaris.Fases = True
						Data = CacheXecute("S VALUE=$O(^USUSERVEI(EMP,P1,P2,""""),-1)")
						ObreFormulari(frmServeisUsuaris, "frmServeisUsuaris", txtCodigo.Text & S & SER & S & Data)
				End Select
				CarregaServeisActius()
				
		End Select
	End Sub
	
	Private Sub NouHistLab()
		ObreFormulari(frmHistorialLaboral, "frmHistorialLaboral", txtCodigo.Text)
	End Sub
	
	Private Sub NouHistForm()
		ObreFormulari(frmUsuariInsercioFormacio, "frmUsuariInsercioFormacio", txtCodigo.Text)
	End Sub
	
	Private Sub NouPrepLab()
		ObreFormulari(frmUsuariPreparacioLaboral, "frmUsuariPreparacioLaboral", txtCodigo.Text)
	End Sub
	
	Private Sub NouSegLab()
		ObreFormulari(frmSeguimentUsuari, "frmSeguimentUsuari", txtCodigo.Text)
	End Sub
	
	Private Sub NouServei()
		ObreFormulari(frmServeisUsuaris, "frmServeisUsuaris", txtCodigo.Text)
	End Sub
	
	Private Sub NovaRelacions()
		ObreFormulari(frmRelacions, "frmRelacions", txtCodigo.Text)
	End Sub
	
	Private Sub NouPrograma()
		ObreFormulari(frmUsuariPrograma, "frmUsuariPrograma", txtCodigo.Text)
	End Sub
	
	Private Sub NovaInsercio()
		ObreFormulari(frmEmpresaInseridora, "frmEmpresaInseridora", "" & S & "" & S & txtCodigo.Text)
	End Sub
	
	Private Sub MenuDesplegable(ByRef X As Single, ByRef Y As Single)
		Dim Opcio As gmsPopUpCode.cPopMenuItem
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("cmdBuscaIndex", PopMenuStyle.tsPrimaryMenu, True)
				Opcio = .MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou document de plantilla",  ,  , XPIcon("Clone"),  ,  ,  ,  , "Pla")
				If Permis(Me, e_Permisos.Altes, CStr(False)) = False Then Opcio.Enabled = False
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				Opcio = .MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou document de Word per vincular",  ,  , XPIcon("Word"),  ,  ,  ,  , "Wor")
				If Permis(Me, e_Permisos.Altes, CStr(False)) = False Then Opcio.Enabled = False
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				Opcio = .MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Vincular nou document",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "Nou")
				If Permis(Me, e_Permisos.Altes, CStr(False)) = False Then Opcio.Enabled = False
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				Opcio = .MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Obrir document",  ,  , XPIcon("Edit"),  ,  ,  ,  , "Obr")
				If Permis(Me, e_Permisos.Consultes, CStr(False)) = False Then Opcio.Enabled = False
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				Opcio = .MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Borrar",  ,  , XPIcon("Delete"),  ,  ,  ,  , "Del")
				If Permis(Me, e_Permisos.Baixes, CStr(False)) = False Then Opcio.Enabled = False
			End With
		End With
		XpExecutaMenu(Me, "cmdBuscaIndex", X, Y)
	End Sub
	
	Private Sub MostraEstat()
		Dim strResult As String
		
		If txtCodigo.Text = "" Then Exit Sub
		CacheNetejaParametres()
		MCache.P1 = txtCodigo.Text
		strResult = CacheXecute("D TREBALLANT^INSERCIO(P1)")
		If CDbl(strResult) = 1 Then
			lblTreballant.Visible = True
			lblTreballant.Text = "TREBALLANT"
		Else
			lblTreballant.Text = ""
			lblTreballant.Visible = False
		End If
	End Sub
	
	
	Function Valida() As Boolean
		Valida = True
		
		If chkTieneCertificadoDiscap.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked And txtGradoDiscapacidad.Text = "" Then
			xMsgBox("Falta informar el Grau de discapacitat ", MsgBoxStyle.Information)
			txtGradoDiscapacidad.Focus()
			Valida = False
			Exit Function
		End If
		
	End Function
	
	
	Private Sub txtValoracioPossibilitat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValoracioPossibilitat.GotFocus
		XGotFocus(Me, txtValoracioPossibilitat)
	End Sub
	
	Private Sub txtValoracioPossibilitat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValoracioPossibilitat.LostFocus
		XLostFocus(Me, txtValoracioPossibilitat)
	End Sub
End Class
