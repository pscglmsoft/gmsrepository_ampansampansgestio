<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmFasesInsercio
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtServicio As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtUsuario As System.Windows.Forms.TextBox
	Public WithEvents txtFechaAlta As AxDataControl.AxGmsData
	Public WithEvents txtFechaRecogidaDatos As AxDataControl.AxGmsData
	Public WithEvents txtFechaValoracion As AxDataControl.AxGmsData
	Public WithEvents txtFechaPrepLaboral As AxDataControl.AxGmsData
	Public WithEvents txtFechaIntermediacion As AxDataControl.AxGmsData
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmFasesInsercio))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtServicio = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtUsuario = New System.Windows.Forms.TextBox
		Me.txtFechaAlta = New AxDataControl.AxGmsData
		Me.txtFechaRecogidaDatos = New AxDataControl.AxGmsData
		Me.txtFechaValoracion = New AxDataControl.AxGmsData
		Me.txtFechaPrepLaboral = New AxDataControl.AxGmsData
		Me.txtFechaIntermediacion = New AxDataControl.AxGmsData
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtFechaAlta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaRecogidaDatos, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaValoracion, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaPrepLaboral, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaIntermediacion, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Form1"
		Me.ClientSize = New System.Drawing.Size(539, 202)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmFasesInsercio"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(372, 19)
		Me.Text2.Location = New System.Drawing.Point(150, 32)
		Me.Text2.TabIndex = 3
		Me.Text2.Tag = "^2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtServicio.AutoSize = False
		Me.txtServicio.Size = New System.Drawing.Size(21, 19)
		Me.txtServicio.Location = New System.Drawing.Point(126, 32)
		Me.txtServicio.Maxlength = 2
		Me.txtServicio.TabIndex = 2
		Me.txtServicio.Tag = "*2"
		Me.txtServicio.AcceptsReturn = True
		Me.txtServicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtServicio.BackColor = System.Drawing.SystemColors.Window
		Me.txtServicio.CausesValidation = True
		Me.txtServicio.Enabled = True
		Me.txtServicio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtServicio.HideSelection = True
		Me.txtServicio.ReadOnly = False
		Me.txtServicio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtServicio.MultiLine = False
		Me.txtServicio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtServicio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtServicio.TabStop = True
		Me.txtServicio.Visible = True
		Me.txtServicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtServicio.Name = "txtServicio"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(352, 19)
		Me.Text1.Location = New System.Drawing.Point(171, 8)
		Me.Text1.TabIndex = 1
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtUsuario.AutoSize = False
		Me.txtUsuario.Size = New System.Drawing.Size(42, 19)
		Me.txtUsuario.Location = New System.Drawing.Point(126, 8)
		Me.txtUsuario.Maxlength = 4
		Me.txtUsuario.TabIndex = 0
		Me.txtUsuario.Tag = "*1"
		Me.txtUsuario.AcceptsReturn = True
		Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsuario.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsuario.CausesValidation = True
		Me.txtUsuario.Enabled = True
		Me.txtUsuario.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsuario.HideSelection = True
		Me.txtUsuario.ReadOnly = False
		Me.txtUsuario.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsuario.MultiLine = False
		Me.txtUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsuario.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsuario.TabStop = True
		Me.txtUsuario.Visible = True
		Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUsuario.Name = "txtUsuario"
		txtFechaAlta.OcxState = CType(resources.GetObject("txtFechaAlta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaAlta.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaAlta.Location = New System.Drawing.Point(126, 56)
		Me.txtFechaAlta.TabIndex = 6
		Me.txtFechaAlta.Name = "txtFechaAlta"
		txtFechaRecogidaDatos.OcxState = CType(resources.GetObject("txtFechaRecogidaDatos.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaRecogidaDatos.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaRecogidaDatos.Location = New System.Drawing.Point(136, 82)
		Me.txtFechaRecogidaDatos.TabIndex = 8
		Me.txtFechaRecogidaDatos.Name = "txtFechaRecogidaDatos"
		txtFechaValoracion.OcxState = CType(resources.GetObject("txtFechaValoracion.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaValoracion.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaValoracion.Location = New System.Drawing.Point(424, 82)
		Me.txtFechaValoracion.TabIndex = 9
		Me.txtFechaValoracion.Name = "txtFechaValoracion"
		txtFechaPrepLaboral.OcxState = CType(resources.GetObject("txtFechaPrepLaboral.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaPrepLaboral.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaPrepLaboral.Location = New System.Drawing.Point(136, 106)
		Me.txtFechaPrepLaboral.TabIndex = 10
		Me.txtFechaPrepLaboral.Name = "txtFechaPrepLaboral"
		txtFechaIntermediacion.OcxState = CType(resources.GetObject("txtFechaIntermediacion.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaIntermediacion.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaIntermediacion.Location = New System.Drawing.Point(424, 106)
		Me.txtFechaIntermediacion.TabIndex = 11
		Me.txtFechaIntermediacion.Name = "txtFechaIntermediacion"
		Me.Label3.Text = "Fecha recogida de datos"
		Me.Label3.Size = New System.Drawing.Size(131, 15)
		Me.Label3.Location = New System.Drawing.Point(0, 86)
		Me.Label3.TabIndex = 15
		Me.Label3.Tag = "8"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label4.Text = "Fecha valoraci�n"
		Me.Label4.Size = New System.Drawing.Size(111, 15)
		Me.Label4.Location = New System.Drawing.Point(310, 86)
		Me.Label4.TabIndex = 14
		Me.Label4.Tag = "9"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label5.Text = "Fecha preparaci�n laboral"
		Me.Label5.Size = New System.Drawing.Size(131, 15)
		Me.Label5.Location = New System.Drawing.Point(0, 110)
		Me.Label5.TabIndex = 13
		Me.Label5.Tag = "10"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label6.Text = "Fecha intermediaci�n"
		Me.Label6.Size = New System.Drawing.Size(111, 15)
		Me.Label6.Location = New System.Drawing.Point(310, 110)
		Me.Label6.TabIndex = 12
		Me.Label6.Tag = "11"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.lbl3.Text = "Fecha alta"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(12, 60)
		Me.lbl3.TabIndex = 7
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl2.Text = "Servicio"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(12, 36)
		Me.lbl2.TabIndex = 5
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl1.Text = "Usuario"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(12, 12)
		Me.lbl1.TabIndex = 4
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtServicio)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtUsuario)
		Me.Controls.Add(txtFechaAlta)
		Me.Controls.Add(txtFechaRecogidaDatos)
		Me.Controls.Add(txtFechaValoracion)
		Me.Controls.Add(txtFechaPrepLaboral)
		Me.Controls.Add(txtFechaIntermediacion)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label4)
		Me.Controls.Add(Label5)
		Me.Controls.Add(Label6)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl1)
		CType(Me.txtFechaIntermediacion, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaPrepLaboral, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaValoracion, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaRecogidaDatos, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaAlta, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class