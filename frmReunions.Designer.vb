<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmReunions
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtProblema As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtSubtipusDemanda As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtTipusDemanda As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtDemandant As System.Windows.Forms.TextBox
	Public WithEvents txtData As AxDataControl.AxGmsData
	Public WithEvents GrdComissio As AxFlexCell.AxGrid
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtDatalimit As AxDataControl.AxGmsData
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmReunions))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtProblema = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtSubtipusDemanda = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtTipusDemanda = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtDemandant = New System.Windows.Forms.TextBox
		Me.txtData = New AxDataControl.AxGmsData
		Me.GrdComissio = New AxFlexCell.AxGrid
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.txtDatalimit = New AxDataControl.AxGmsData
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdComissio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDatalimit, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Reunions de la Comissi� de Direcci�"
		Me.ClientSize = New System.Drawing.Size(835, 254)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmReunions"
		Me.txtProblema.AutoSize = False
		Me.txtProblema.Size = New System.Drawing.Size(459, 107)
		Me.txtProblema.Location = New System.Drawing.Point(114, 104)
		Me.txtProblema.Maxlength = 250
		Me.txtProblema.MultiLine = True
		Me.txtProblema.TabIndex = 8
		Me.txtProblema.Tag = "6######1"
		Me.txtProblema.AcceptsReturn = True
		Me.txtProblema.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtProblema.BackColor = System.Drawing.SystemColors.Window
		Me.txtProblema.CausesValidation = True
		Me.txtProblema.Enabled = True
		Me.txtProblema.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtProblema.HideSelection = True
		Me.txtProblema.ReadOnly = False
		Me.txtProblema.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtProblema.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtProblema.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtProblema.TabStop = True
		Me.txtProblema.Visible = True
		Me.txtProblema.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtProblema.Name = "txtProblema"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(414, 19)
		Me.Text5.Location = New System.Drawing.Point(159, 80)
		Me.Text5.TabIndex = 7
		Me.Text5.Tag = "^5"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtSubtipusDemanda.AutoSize = False
		Me.txtSubtipusDemanda.Size = New System.Drawing.Size(42, 19)
		Me.txtSubtipusDemanda.Location = New System.Drawing.Point(114, 80)
		Me.txtSubtipusDemanda.Maxlength = 4
		Me.txtSubtipusDemanda.TabIndex = 6
		Me.txtSubtipusDemanda.Tag = "5####DEM-TAULA_SUBTIUPUS_DEMANDA#1#1"
		Me.txtSubtipusDemanda.AcceptsReturn = True
		Me.txtSubtipusDemanda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSubtipusDemanda.BackColor = System.Drawing.SystemColors.Window
		Me.txtSubtipusDemanda.CausesValidation = True
		Me.txtSubtipusDemanda.Enabled = True
		Me.txtSubtipusDemanda.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSubtipusDemanda.HideSelection = True
		Me.txtSubtipusDemanda.ReadOnly = False
		Me.txtSubtipusDemanda.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSubtipusDemanda.MultiLine = False
		Me.txtSubtipusDemanda.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSubtipusDemanda.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSubtipusDemanda.TabStop = True
		Me.txtSubtipusDemanda.Visible = True
		Me.txtSubtipusDemanda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSubtipusDemanda.Name = "txtSubtipusDemanda"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(414, 19)
		Me.Text4.Location = New System.Drawing.Point(159, 56)
		Me.Text4.TabIndex = 5
		Me.Text4.Tag = "^4"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtTipusDemanda.AutoSize = False
		Me.txtTipusDemanda.Size = New System.Drawing.Size(42, 19)
		Me.txtTipusDemanda.Location = New System.Drawing.Point(114, 56)
		Me.txtTipusDemanda.ReadOnly = True
		Me.txtTipusDemanda.Maxlength = 4
		Me.txtTipusDemanda.TabIndex = 4
		Me.txtTipusDemanda.Tag = "4####DEM-TAULA_DEMANDES#1#1"
		Me.txtTipusDemanda.AcceptsReturn = True
		Me.txtTipusDemanda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipusDemanda.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipusDemanda.CausesValidation = True
		Me.txtTipusDemanda.Enabled = True
		Me.txtTipusDemanda.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipusDemanda.HideSelection = True
		Me.txtTipusDemanda.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipusDemanda.MultiLine = False
		Me.txtTipusDemanda.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipusDemanda.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipusDemanda.TabStop = True
		Me.txtTipusDemanda.Visible = True
		Me.txtTipusDemanda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipusDemanda.Name = "txtTipusDemanda"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(362, 19)
		Me.Text2.Location = New System.Drawing.Point(211, 8)
		Me.Text2.TabIndex = 1
		Me.Text2.Tag = "^1"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtDemandant.AutoSize = False
		Me.txtDemandant.Size = New System.Drawing.Size(93, 19)
		Me.txtDemandant.Location = New System.Drawing.Point(114, 8)
		Me.txtDemandant.Maxlength = 9
		Me.txtDemandant.TabIndex = 0
		Me.txtDemandant.Tag = "1####I-PERSONAL#1#1"
		Me.txtDemandant.AcceptsReturn = True
		Me.txtDemandant.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDemandant.BackColor = System.Drawing.SystemColors.Window
		Me.txtDemandant.CausesValidation = True
		Me.txtDemandant.Enabled = True
		Me.txtDemandant.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDemandant.HideSelection = True
		Me.txtDemandant.ReadOnly = False
		Me.txtDemandant.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDemandant.MultiLine = False
		Me.txtDemandant.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDemandant.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDemandant.TabStop = True
		Me.txtDemandant.Visible = True
		Me.txtDemandant.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDemandant.Name = "txtDemandant"
		txtData.OcxState = CType(resources.GetObject("txtData.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtData.Size = New System.Drawing.Size(87, 19)
		Me.txtData.Location = New System.Drawing.Point(114, 32)
		Me.txtData.TabIndex = 2
		Me.txtData.Name = "txtData"
		GrdComissio.OcxState = CType(resources.GetObject("GrdComissio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdComissio.Size = New System.Drawing.Size(240, 183)
		Me.GrdComissio.Location = New System.Drawing.Point(584, 28)
		Me.GrdComissio.TabIndex = 16
		Me.GrdComissio.Name = "GrdComissio"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(740, 220)
		Me.cmdAceptar.TabIndex = 9
		Me.cmdAceptar.Name = "cmdAceptar"
		txtDatalimit.OcxState = CType(resources.GetObject("txtDatalimit.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDatalimit.Size = New System.Drawing.Size(87, 19)
		Me.txtDatalimit.Location = New System.Drawing.Point(488, 32)
		Me.txtDatalimit.TabIndex = 3
		Me.txtDatalimit.Name = "txtDatalimit"
		Me.Label1.Text = "Comissi� de Direcci�"
		Me.Label1.Font = New System.Drawing.Font("Perpetua Titling MT", 8.25!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.ForeColor = System.Drawing.Color.FromARGB(0, 0, 192)
		Me.Label1.Size = New System.Drawing.Size(143, 13)
		Me.Label1.Location = New System.Drawing.Point(586, 10)
		Me.Label1.TabIndex = 17
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl8.Text = "Problema"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(8, 108)
		Me.lbl8.TabIndex = 15
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl5.Text = "Subtipus demanda"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(8, 84)
		Me.lbl5.TabIndex = 14
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl4.Text = "Tipus demanda"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(8, 60)
		Me.lbl4.TabIndex = 13
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl3.Text = "Data"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(8, 36)
		Me.lbl3.TabIndex = 12
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl2.Text = "Demandant"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(8, 12)
		Me.lbl2.TabIndex = 11
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.Label4.Text = "Data"
		Me.Label4.Size = New System.Drawing.Size(111, 15)
		Me.Label4.Location = New System.Drawing.Point(374, 36)
		Me.Label4.TabIndex = 10
		Me.Label4.Tag = "13"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		CType(Me.txtDatalimit, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdComissio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtProblema)
		Me.Controls.Add(Text5)
		Me.Controls.Add(txtSubtipusDemanda)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtTipusDemanda)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtDemandant)
		Me.Controls.Add(txtData)
		Me.Controls.Add(GrdComissio)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(txtDatalimit)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(Label4)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class