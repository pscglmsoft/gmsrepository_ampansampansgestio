<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmRelacions
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtUsuario As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtFechaAlta As AxDataControl.AxGmsData
	Public WithEvents txtNl As System.Windows.Forms.TextBox
	Public WithEvents txtTipoRelacion As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtSubtipoRelacion As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtPersona As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtFechaBaja As AxDataControl.AxGmsData
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRelacions))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtUsuario = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtFechaAlta = New AxDataControl.AxGmsData
		Me.txtNl = New System.Windows.Forms.TextBox
		Me.txtTipoRelacion = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtSubtipoRelacion = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtPersona = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtFechaBaja = New AxDataControl.AxGmsData
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtFechaAlta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Relacions"
		Me.ClientSize = New System.Drawing.Size(471, 228)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-RELACIONS"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmRelacions"
		Me.txtUsuario.AutoSize = False
		Me.txtUsuario.Size = New System.Drawing.Size(42, 19)
		Me.txtUsuario.Location = New System.Drawing.Point(124, 10)
		Me.txtUsuario.Maxlength = 4
		Me.txtUsuario.TabIndex = 1
		Me.txtUsuario.Tag = "*1"
		Me.txtUsuario.AcceptsReturn = True
		Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsuario.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsuario.CausesValidation = True
		Me.txtUsuario.Enabled = True
		Me.txtUsuario.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsuario.HideSelection = True
		Me.txtUsuario.ReadOnly = False
		Me.txtUsuario.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsuario.MultiLine = False
		Me.txtUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsuario.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsuario.TabStop = True
		Me.txtUsuario.Visible = True
		Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUsuario.Name = "txtUsuario"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(290, 19)
		Me.Text1.Location = New System.Drawing.Point(169, 10)
		Me.Text1.TabIndex = 2
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		txtFechaAlta.OcxState = CType(resources.GetObject("txtFechaAlta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaAlta.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaAlta.Location = New System.Drawing.Point(124, 34)
		Me.txtFechaAlta.TabIndex = 4
		Me.txtFechaAlta.Name = "txtFechaAlta"
		Me.txtNl.AutoSize = False
		Me.txtNl.Size = New System.Drawing.Size(21, 19)
		Me.txtNl.Location = New System.Drawing.Point(124, 58)
		Me.txtNl.Maxlength = 2
		Me.txtNl.TabIndex = 6
		Me.txtNl.Tag = "*3"
		Me.txtNl.AcceptsReturn = True
		Me.txtNl.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNl.BackColor = System.Drawing.SystemColors.Window
		Me.txtNl.CausesValidation = True
		Me.txtNl.Enabled = True
		Me.txtNl.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNl.HideSelection = True
		Me.txtNl.ReadOnly = False
		Me.txtNl.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNl.MultiLine = False
		Me.txtNl.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNl.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNl.TabStop = True
		Me.txtNl.Visible = True
		Me.txtNl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNl.Name = "txtNl"
		Me.txtTipoRelacion.AutoSize = False
		Me.txtTipoRelacion.Size = New System.Drawing.Size(21, 19)
		Me.txtTipoRelacion.Location = New System.Drawing.Point(124, 82)
		Me.txtTipoRelacion.Maxlength = 2
		Me.txtTipoRelacion.TabIndex = 8
		Me.txtTipoRelacion.Tag = "4"
		Me.txtTipoRelacion.AcceptsReturn = True
		Me.txtTipoRelacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoRelacion.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoRelacion.CausesValidation = True
		Me.txtTipoRelacion.Enabled = True
		Me.txtTipoRelacion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoRelacion.HideSelection = True
		Me.txtTipoRelacion.ReadOnly = False
		Me.txtTipoRelacion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoRelacion.MultiLine = False
		Me.txtTipoRelacion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoRelacion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoRelacion.TabStop = True
		Me.txtTipoRelacion.Visible = True
		Me.txtTipoRelacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoRelacion.Name = "txtTipoRelacion"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(312, 19)
		Me.Text4.Location = New System.Drawing.Point(148, 82)
		Me.Text4.TabIndex = 9
		Me.Text4.Tag = "^4"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtSubtipoRelacion.AutoSize = False
		Me.txtSubtipoRelacion.Size = New System.Drawing.Size(21, 19)
		Me.txtSubtipoRelacion.Location = New System.Drawing.Point(124, 106)
		Me.txtSubtipoRelacion.Maxlength = 2
		Me.txtSubtipoRelacion.TabIndex = 11
		Me.txtSubtipoRelacion.Tag = "5"
		Me.txtSubtipoRelacion.AcceptsReturn = True
		Me.txtSubtipoRelacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSubtipoRelacion.BackColor = System.Drawing.SystemColors.Window
		Me.txtSubtipoRelacion.CausesValidation = True
		Me.txtSubtipoRelacion.Enabled = True
		Me.txtSubtipoRelacion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSubtipoRelacion.HideSelection = True
		Me.txtSubtipoRelacion.ReadOnly = False
		Me.txtSubtipoRelacion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSubtipoRelacion.MultiLine = False
		Me.txtSubtipoRelacion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSubtipoRelacion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSubtipoRelacion.TabStop = True
		Me.txtSubtipoRelacion.Visible = True
		Me.txtSubtipoRelacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSubtipoRelacion.Name = "txtSubtipoRelacion"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(312, 19)
		Me.Text5.Location = New System.Drawing.Point(148, 106)
		Me.Text5.TabIndex = 12
		Me.Text5.Tag = "^5"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtPersona.AutoSize = False
		Me.txtPersona.Size = New System.Drawing.Size(59, 19)
		Me.txtPersona.Location = New System.Drawing.Point(124, 130)
		Me.txtPersona.Maxlength = 8
		Me.txtPersona.TabIndex = 14
		Me.txtPersona.Tag = "6"
		Me.txtPersona.AcceptsReturn = True
		Me.txtPersona.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPersona.BackColor = System.Drawing.SystemColors.Window
		Me.txtPersona.CausesValidation = True
		Me.txtPersona.Enabled = True
		Me.txtPersona.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPersona.HideSelection = True
		Me.txtPersona.ReadOnly = False
		Me.txtPersona.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPersona.MultiLine = False
		Me.txtPersona.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPersona.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPersona.TabStop = True
		Me.txtPersona.Visible = True
		Me.txtPersona.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPersona.Name = "txtPersona"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(276, 19)
		Me.Text6.Location = New System.Drawing.Point(184, 130)
		Me.Text6.TabIndex = 15
		Me.Text6.Tag = "^6"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		txtFechaBaja.OcxState = CType(resources.GetObject("txtFechaBaja.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaBaja.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaBaja.Location = New System.Drawing.Point(124, 154)
		Me.txtFechaBaja.TabIndex = 17
		Me.txtFechaBaja.Name = "txtFechaBaja"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(289, 194)
		Me.cmdAceptar.TabIndex = 18
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(379, 194)
		Me.cmdGuardar.TabIndex = 19
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl1.Text = "Usuario"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Fecha alta"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 3
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Nl"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 5
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Tipo relaci�n"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 86)
		Me.lbl4.TabIndex = 7
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Subtipo relaci�n"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 110)
		Me.lbl5.TabIndex = 10
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Persona"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 134)
		Me.lbl6.TabIndex = 13
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "Fecha baja"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 158)
		Me.lbl7.TabIndex = 16
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 194)
		Me.lblLock.TabIndex = 20
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 187
		Me.Line1.Y2 = 187
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 188
		Me.Line2.Y2 = 188
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaAlta, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtUsuario)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtFechaAlta)
		Me.Controls.Add(txtNl)
		Me.Controls.Add(txtTipoRelacion)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtSubtipoRelacion)
		Me.Controls.Add(Text5)
		Me.Controls.Add(txtPersona)
		Me.Controls.Add(Text6)
		Me.Controls.Add(txtFechaBaja)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class