Option Strict Off
Option Explicit On
Friend Class frmCorrecte
	Inherits FormParent
	
	Private Sub cmdCorrecte_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCorrecte.Click
		CacheNetejaParametres()
		MCache.P1 = frmControlGates.txtAlbara
		MCache.P2 = frmControlGates.txtFaixa
		MCache.P3 = frmControlGates.txtEtiqueta
		MCache.P4 = "OK"
		CacheXecute("D GRAVA^GATES")
		ResetForm(frmControlGates)
		Me.Unload()
		frmControlGates.txtAlbara.Focus()
	End Sub
	
	
	Private Sub frmCorrecte_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		CentrarForm(Me)
	End Sub
End Class
