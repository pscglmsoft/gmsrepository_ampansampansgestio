Option Strict Off
Option Explicit On
Friend Class frmGDDocumentosPersona
	Inherits FormParent
	Public Persona As String
	Public NomPersona As String
	Public ReferenciaGestorDocumental As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		Me.Unload()
	End Sub
	
	Private Sub frmGDDocumentosPersona_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		Me.Text = Me.Text & " - " & NomPersona
		tvClasificacions.ImageList = frmImatgesXP.ImageList16
		CarregaArbre()
		'UPGRADE_WARNING: El l�mite inferior de la colecci�n tvClasificacions.Nodes cambi� de 1 a 0. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		tvClasificacions.SelectedNode = GetAllNodes(tvClasificacions).Item(1)
tvClasificacions_NodeMouseClick(tvClasificacions, New System.Windows.Forms.TreeNodeMouseClickEventArgs(tvClasificacions.SelectedNode, System.Windows.Forms.MouseButtons.None, 0, 0, 0))
	End Sub
	
	Private Sub frmGDDocumentosPersona_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Persona = ""
		NomPersona = ""
	End Sub
	
	Private Sub frmGDDocumentosPersona_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmGDDocumentosPersona_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub CarregaArbre(Optional ByRef Item As String = "")
		Dim Resposta As String
		CacheNetejaParametres()
		MCache.P1 = ReferenciaGestorDocumental
		MCache.P2 = Persona
		Resposta = CacheXecute("S VALUE=$$PreparaDocuments^GCOM.GD.DOCUMENTS(P1,P2)")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		CarregaTreeView(tvClasificacions, "CTreeClas^GCOM.GD.CONFIG", Modul & PC & Item & PC & Piece(Resposta, S, 1), True)
	End Sub
	
	Private Sub grdDocuments_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdDocuments.DoubleClick
		If grdDocuments.MouseRow < 1 Then Exit Sub
		ResMenu("grdDocuments", "mnuOpen")
	End Sub
	
	Private Sub grdDocuments_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdDocuments.MouseUp
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		Dim Fila As Integer
		Fila = grdDocuments.ActiveCell.Row
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdDocuments", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Abrir documento",  , Fila > 0, XPIcon("Open"),  ,  ,  ,  , "mnuOpen")
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Abrir documentos",  , grdDocuments.Rows > 1, XPIcon("WindowCascada"),  ,  ,  ,  , "mnuOpenTots")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Exportar en carpeta ...",  , grdDocuments.Rows > 1, XPIcon("SaveAll"),  ,  ,  ,  , "mnuExportarTots")
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar documento",  , (Fila > 0) And (UserAdmin = True),  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Exportar excel",  ,  ,  ,  ,  ,  ,  , "mnuExcel")
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Propiedades",  ,  , XPIcon("Propietats"),  ,  ,  ,  , "mnuPropietats")
			End With
		End With
		XpExecutaMenu(Me, "grdDocuments")
	End Sub
	
	Private Sub tvClasificacions_NodeMouseClick(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvClasificacions.NodeMouseClick
		Dim Node As System.Windows.Forms.TreeNode = eventArgs.Node
		Node.Expand()
		CarregaGrid()
	End Sub
	
	Private Sub CarregaGrid()
		Dim Parametres As String
		If (tvClasificacions.SelectedNode Is Nothing) = False Then
			Parametres = ReferenciaGestorDocumental
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Parametres = IPiece(Parametres, vbNullChar, 2, Piece(tvClasificacions.SelectedNode.Tag, "|", 1))
			Parametres = IPiece(Parametres, vbNullChar, 3, Persona)
			CarregaFGrid(grdDocuments, "CGridDocuments^GCOM.GD.DOCUMENTS", Parametres,  , True, True)
		Else
			FGridRemoveAll(grdDocuments)
		End If
	End Sub
	
	'UPGRADE_NOTE: Menu se actualiz� a Menu_Renamed. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub ResMenu(ByRef Menu As String, ByRef KeyMenu As String)
		Dim Pregunta As String
		Dim Index As String
		Dim Resposta As String
		Index = grdDocuments.Cell(grdDocuments.ActiveCell.Row, 1).Text
		Dim Tamany As String
		Select Case Menu
			Case "grdDocuments"
				Select Case KeyMenu
					Case "mnuOpen"
						If RemoteServer = True Then
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							Tamany = Piece(grdDocuments.Cell(grdDocuments.ActiveCell.Row, 1).Text, "|", 2)
							If (Tamany = "") Or (RetornaValorL(Tamany) > 500) Then
								Pregunta = "El descargar el fichero en remoto puede conllevar tiempo." & vbCr & "Esta seguro de querer abrir el documento ?"
								If xMsgBox(Pregunta, MsgBoxStyle.Information + MsgBoxStyle.YesNo, Me.Text) <> MsgBoxResult.Yes Then Exit Sub
							End If
						End If
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreDocumentGD(Piece(Piece(Index, "|", 1), "/", 1))
					Case "mnuOpenTots"
						ObreTotsDocuments()
					Case "mnuExportarTots"
						ExportarDocuments()
					Case "mnuDelete"
						Pregunta = "Esta seguro de querer eliminar el documento seleccionado ?"
						If xMsgBox(Pregunta, MsgBoxStyle.Information + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, Me.Text) <> MsgBoxResult.Yes Then Exit Sub
						CacheNetejaParametres()
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						MCache.P1 = Piece(Piece(Index, "|", 1), "/", 1)
						Resposta = CacheXecute("S VALUE=$$Delete^%ZWDOCAP(P1)")
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(Resposta, S, 1). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						If Piece(Resposta, S, 1) = "0" Then
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							xMsgBox(Piece(Resposta, S, 2), MsgBoxStyle.Critical, Me.Text)
							Exit Sub
						Else
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							xMsgBox(Piece(Resposta, S, 2), MsgBoxStyle.Information, Me.Text)
							CarregaArbre()
							CarregaGrid()
						End If
					Case "mnuPropietats"
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(Index, |, 5). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(Index, |, 4). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(Index, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						frmGDDocumentosPropiedades.Entrada(Piece(Index, "|", 1), Piece(Index, "|", 3), Piece(Index, "|", 4), Piece(Index, "|", 5))
						CarregaArbre()
						CarregaGrid()
						'COMENTAT PER JORDI 05/08/2015
					Case "mnuExcel"
						ExportFG(grdDocuments, ETipExport.eExcel)
				End Select
		End Select
	End Sub
	
	Private Sub ObreTotsDocuments()
		Dim Fila As Integer
		If VerificaTemps() = False Then Exit Sub
		MdiProgressBar.Visible = True
		MdiProgressBar.Max = grdDocuments.Rows
		For Fila = 1 To grdDocuments.Rows - 1
			MdiProgressBar.Value = Fila
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			ObreDocumentGD(Piece(Piece(grdDocuments.Cell(Fila, 1).Text, "|", 1), "/", 1))
			Application.DoEvents()
		Next Fila
		MdiProgressBar.Visible = False
	End Sub
	
	Private Function VerificaTemps() As Boolean
		Dim Tamany As Integer
		Dim FlagPregunta As Boolean
		VerificaTemps = False
		Dim Fila As Integer
		If grdDocuments.Rows > 10 Then
			FlagPregunta = True
		Else
			For Fila = 1 To grdDocuments.Rows - 1
				'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Tamany = Tamany + RetornaValorL(Piece(grdDocuments.Cell(Fila, 1).Text, "|", 2))
			Next Fila
			If Tamany > 5000 Then FlagPregunta = True
		End If
		If FlagPregunta = True Then
			If xMsgBox("El resultado de la operaci�n puede demorarse varios minutos." & vbCr & "Desea continuar de todos modos ?", MsgBoxStyle.Information + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, Me.Text) <> MsgBoxResult.Yes Then
				Exit Function
			End If
		End If
		VerificaTemps = True
	End Function
	
	Private Sub ExportarDocuments()
		Dim Fila As Integer
		Dim Carpeta As String
		Dim Fitxer As String
		Carpeta = ArbreDirectoris("", GetString("Exportar documentos"), "Seleccione directorio")
		If Carpeta = "" Then Exit Sub
		If VerificaTemps() = False Then Exit Sub
		MdiProgressBar.Visible = True
		MdiProgressBar.Max = grdDocuments.Rows
		For Fila = 1 To grdDocuments.Rows - 1
			MdiProgressBar.Value = Fila
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			ExportaDocumentGD(Piece(Piece(grdDocuments.Cell(Fila, 1).Text, "|", 1), "/", 1), Carpeta)
			Application.DoEvents()
		Next Fila
		MdiProgressBar.Visible = False
		xMsgBox("Documentos exportados correctamente.", MsgBoxStyle.Information, Me.Text)
	End Sub
End Class
