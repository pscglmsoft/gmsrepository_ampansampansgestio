Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmUsuariPreparacioLaboral
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	Private Sub frmUsuariPreparacioLaboral_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmUsuariPreparacioLaboral_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmUsuariPreparacioLaboral_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmUsuariPreparacioLaboral_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	Private Sub txtUsuario_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.GotFocus
		XGotFocus(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.DoubleClick
		ConsultaTaula(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.LostFocus
		If txtUsuario.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUsuario.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPrograma_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrograma.GotFocus
		XGotFocus(Me, txtPrograma)
	End Sub
	
	Private Sub txtPrograma_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrograma.DoubleClick
		ConsultaTaula(Me, txtPrograma)
	End Sub
	
	Private Sub txtPrograma_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrograma.LostFocus
		If txtPrograma.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtPrograma)
	End Sub
	
	Private Sub txtPrograma_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPrograma.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaAlta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaAlta.GotFocus
		XGotFocus(Me, txtFechaAlta)
	End Sub
	
	Private Sub txtFechaAlta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaAlta.LostFocus
		If txtFechaAlta.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtFechaAlta)
	End Sub
	
	Private Sub txtFechaBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.GotFocus
		XGotFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtFechaBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.LostFocus
		XLostFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtMotiuBaixa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotiuBaixa.GotFocus
		XGotFocus(Me, txtMotiuBaixa)
	End Sub
	
	Private Sub txtMotiuBaixa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotiuBaixa.DoubleClick
		ConsultaTaula(Me, txtMotiuBaixa)
	End Sub
	
	Private Sub txtMotiuBaixa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotiuBaixa.LostFocus
		XLostFocus(Me, txtMotiuBaixa)
	End Sub
	
	Private Sub txtMotiuBaixa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMotiuBaixa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservaciones_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservaciones.GotFocus
		XGotFocus(Me, txtObservaciones)
	End Sub
	
	Private Sub txtObservaciones_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservaciones.LostFocus
		XLostFocus(Me, txtObservaciones)
	End Sub
	
	Private Sub txtObservaciones_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservaciones.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
