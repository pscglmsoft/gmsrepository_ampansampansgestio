Option Strict Off
Option Explicit On
Friend Class FrmImprimirTransport
	Inherits FormParent
	''OKPublic Appl As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic FlagExtern As Boolean
	''OKPublic RegistreExtern As String
	''OKPublic Nkeys As Short
	''OKPublic ABM As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If txtDataIni.Text = "" Then
			xMsgBox("Falta informar la fecha inicio", MsgBoxStyle.Information, Me.Text)
			txtDataIni.Focus()
			Exit Sub
		End If
		If txtDataFi.Text = "" Then
			xMsgBox("Falta informar la fecha fin", MsgBoxStyle.Information, Me.Text)
			txtDataFi.Focus()
			Exit Sub
		End If
		If txtTransportista.Text = "" Then
			xMsgBox("Falta informar el transportista", MsgBoxStyle.Information, Me.Text)
			txtTransportista.Focus()
			Exit Sub
		End If
		ImpresioPDF("TRANSPORTD", txtDataIni.Text & S & txtDataFi.Text & S & txtTransportista.Text)
	End Sub
	
	Private Sub FrmImprimirTransport_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub FrmImprimirTransport_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub cmdCancelar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancelar.ClickEvent
		Me.Unload()
	End Sub
	
	
	Private Sub FrmImprimirTransport_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub txtTransportista_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTransportista.DoubleClick
		ConsultaTaula(Me, txtTransportista)
	End Sub
	
	Private Sub txtTransportista_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTransportista.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTransportista)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
