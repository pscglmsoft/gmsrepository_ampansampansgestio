<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class FrmSolPlanificades
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtTransportistaF As System.Windows.Forms.TextBox
	Public WithEvents txtSolicitud As System.Windows.Forms.TextBox
	Public WithEvents txtTipo As System.Windows.Forms.TextBox
	Public WithEvents _optTipo_1 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents _optTipo_2 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents Image2 As System.Windows.Forms.PictureBox
	Public WithEvents Image1 As System.Windows.Forms.PictureBox
	Public WithEvents frTipo As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents Text39 As System.Windows.Forms.TextBox
	Public WithEvents txtMotivoNoRealizado As System.Windows.Forms.TextBox
	Public WithEvents txtComentarioDes As System.Windows.Forms.TextBox
	Public WithEvents lbl39 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents FrameDes As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents txtMotivoNoRealizadoT As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtKilometros As System.Windows.Forms.TextBox
	Public WithEvents txtTransportista As System.Windows.Forms.TextBox
	Public WithEvents Text38 As System.Windows.Forms.TextBox
	Public WithEvents txtTipoTarifa As System.Windows.Forms.TextBox
	Public WithEvents Text34 As System.Windows.Forms.TextBox
	Public WithEvents txtComentario As System.Windows.Forms.TextBox
	Public WithEvents txtFechaRecogida As AxDataControl.AxGmsData
	Public WithEvents txtHoraRecogida As AxgmsTime.AxgmsTemps
	Public WithEvents txtFechaEntrega As AxDataControl.AxGmsData
	Public WithEvents txtHoraEntrega As AxgmsTime.AxgmsTemps
	Public WithEvents txtPrecio As AxDataControl.AxGmsImports
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtHorasTransporte As AxgmsTime.AxgmsTemps
	Public WithEvents txtVdaInt As AxDataControl.AxGmsImports
	Public WithEvents lblVdaInt As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents lbl38 As System.Windows.Forms.Label
	Public WithEvents lbl42 As System.Windows.Forms.Label
	Public WithEvents lbl43 As System.Windows.Forms.Label
	Public WithEvents lbl40 As System.Windows.Forms.Label
	Public WithEvents lbl41 As System.Windows.Forms.Label
	Public WithEvents lbl34 As System.Windows.Forms.Label
	Public WithEvents lbl35 As System.Windows.Forms.Label
	Public WithEvents lbl44 As System.Windows.Forms.Label
	Public WithEvents FrameTransp As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents grdSolPlanificades As AxFlexCell.AxGrid
	Public WithEvents txtDataIni As AxDataControl.AxGmsData
	Public WithEvents txtDataFi As AxDataControl.AxGmsData
	Public WithEvents cmdFiltraData As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdImprimir As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents optTipo As AxRadioButtonArray
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmSolPlanificades))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtTransportistaF = New System.Windows.Forms.TextBox
		Me.txtSolicitud = New System.Windows.Forms.TextBox
		Me.frTipo = New AxXtremeSuiteControls.AxGroupBox
		Me.txtTipo = New System.Windows.Forms.TextBox
		Me._optTipo_1 = New AxXtremeSuiteControls.AxRadioButton
		Me._optTipo_2 = New AxXtremeSuiteControls.AxRadioButton
		Me.Image2 = New System.Windows.Forms.PictureBox
		Me.Image1 = New System.Windows.Forms.PictureBox
		Me.FrameDes = New AxXtremeSuiteControls.AxGroupBox
		Me.Text39 = New System.Windows.Forms.TextBox
		Me.txtMotivoNoRealizado = New System.Windows.Forms.TextBox
		Me.txtComentarioDes = New System.Windows.Forms.TextBox
		Me.lbl39 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.FrameTransp = New AxXtremeSuiteControls.AxGroupBox
		Me.txtMotivoNoRealizadoT = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtKilometros = New System.Windows.Forms.TextBox
		Me.txtTransportista = New System.Windows.Forms.TextBox
		Me.Text38 = New System.Windows.Forms.TextBox
		Me.txtTipoTarifa = New System.Windows.Forms.TextBox
		Me.Text34 = New System.Windows.Forms.TextBox
		Me.txtComentario = New System.Windows.Forms.TextBox
		Me.txtFechaRecogida = New AxDataControl.AxGmsData
		Me.txtHoraRecogida = New AxgmsTime.AxgmsTemps
		Me.txtFechaEntrega = New AxDataControl.AxGmsData
		Me.txtHoraEntrega = New AxgmsTime.AxgmsTemps
		Me.txtPrecio = New AxDataControl.AxGmsImports
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.txtHorasTransporte = New AxgmsTime.AxgmsTemps
		Me.txtVdaInt = New AxDataControl.AxGmsImports
		Me.lblVdaInt = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.lbl38 = New System.Windows.Forms.Label
		Me.lbl42 = New System.Windows.Forms.Label
		Me.lbl43 = New System.Windows.Forms.Label
		Me.lbl40 = New System.Windows.Forms.Label
		Me.lbl41 = New System.Windows.Forms.Label
		Me.lbl34 = New System.Windows.Forms.Label
		Me.lbl35 = New System.Windows.Forms.Label
		Me.lbl44 = New System.Windows.Forms.Label
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.grdSolPlanificades = New AxFlexCell.AxGrid
		Me.txtDataIni = New AxDataControl.AxGmsData
		Me.txtDataFi = New AxDataControl.AxGmsData
		Me.cmdFiltraData = New AxXtremeSuiteControls.AxPushButton
		Me.cmdImprimir = New AxXtremeSuiteControls.AxPushButton
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.optTipo = New AxRadioButtonArray(components)
		Me.frTipo.SuspendLayout()
		Me.FrameDes.SuspendLayout()
		Me.FrameTransp.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me._optTipo_1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optTipo_2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.frTipo, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.FrameDes, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaRecogida, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtHoraRecogida, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaEntrega, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtHoraEntrega, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtHorasTransporte, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtVdaInt, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.FrameTransp, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.grdSolPlanificades, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataIni, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataFi, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdFiltraData, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdImprimir, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.optTipo, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Programar/Realizar solicitudes planificadas"
		Me.ClientSize = New System.Drawing.Size(1121, 549)
		Me.Location = New System.Drawing.Point(21, 92)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "FrmSolPlanificades"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(266, 19)
		Me.Text2.Location = New System.Drawing.Point(387, 18)
		Me.Text2.TabIndex = 42
		Me.Text2.Tag = "^4"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtTransportistaF.AutoSize = False
		Me.txtTransportistaF.Size = New System.Drawing.Size(42, 19)
		Me.txtTransportistaF.Location = New System.Drawing.Point(344, 18)
		Me.txtTransportistaF.Maxlength = 4
		Me.txtTransportistaF.TabIndex = 3
		Me.txtTransportistaF.Tag = "4####T-TRANSPORTISTA#1"
		Me.txtTransportistaF.AcceptsReturn = True
		Me.txtTransportistaF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTransportistaF.BackColor = System.Drawing.SystemColors.Window
		Me.txtTransportistaF.CausesValidation = True
		Me.txtTransportistaF.Enabled = True
		Me.txtTransportistaF.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTransportistaF.HideSelection = True
		Me.txtTransportistaF.ReadOnly = False
		Me.txtTransportistaF.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTransportistaF.MultiLine = False
		Me.txtTransportistaF.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTransportistaF.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTransportistaF.TabStop = True
		Me.txtTransportistaF.Visible = True
		Me.txtTransportistaF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTransportistaF.Name = "txtTransportistaF"
		Me.txtSolicitud.AutoSize = False
		Me.txtSolicitud.BackColor = System.Drawing.Color.FromARGB(192, 0, 0)
		Me.txtSolicitud.Size = New System.Drawing.Size(42, 19)
		Me.txtSolicitud.Location = New System.Drawing.Point(656, 482)
		Me.txtSolicitud.Maxlength = 4
		Me.txtSolicitud.TabIndex = 35
		Me.txtSolicitud.Visible = False
		Me.txtSolicitud.AcceptsReturn = True
		Me.txtSolicitud.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSolicitud.CausesValidation = True
		Me.txtSolicitud.Enabled = True
		Me.txtSolicitud.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSolicitud.HideSelection = True
		Me.txtSolicitud.ReadOnly = False
		Me.txtSolicitud.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSolicitud.MultiLine = False
		Me.txtSolicitud.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSolicitud.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSolicitud.TabStop = True
		Me.txtSolicitud.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSolicitud.Name = "txtSolicitud"
		frTipo.OcxState = CType(resources.GetObject("frTipo.OcxState"), System.Windows.Forms.AxHost.State)
		Me.frTipo.Size = New System.Drawing.Size(331, 39)
		Me.frTipo.Location = New System.Drawing.Point(698, 2)
		Me.frTipo.TabIndex = 0
		Me.frTipo.Name = "frTipo"
		Me.txtTipo.AutoSize = False
		Me.txtTipo.BackColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.txtTipo.Size = New System.Drawing.Size(11, 19)
		Me.txtTipo.Location = New System.Drawing.Point(0, 0)
		Me.txtTipo.Maxlength = 1
		Me.txtTipo.TabIndex = 5
		Me.txtTipo.Tag = "11"
		Me.txtTipo.Visible = False
		Me.txtTipo.AcceptsReturn = True
		Me.txtTipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipo.CausesValidation = True
		Me.txtTipo.Enabled = True
		Me.txtTipo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipo.HideSelection = True
		Me.txtTipo.ReadOnly = False
		Me.txtTipo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipo.MultiLine = False
		Me.txtTipo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipo.TabStop = True
		Me.txtTipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipo.Name = "txtTipo"
		_optTipo_1.OcxState = CType(resources.GetObject("_optTipo_1.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optTipo_1.Size = New System.Drawing.Size(123, 21)
		Me._optTipo_1.Location = New System.Drawing.Point(46, 12)
		Me._optTipo_1.TabIndex = 6
		Me._optTipo_1.Name = "_optTipo_1"
		_optTipo_2.OcxState = CType(resources.GetObject("_optTipo_2.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optTipo_2.Size = New System.Drawing.Size(83, 21)
		Me._optTipo_2.Location = New System.Drawing.Point(234, 12)
		Me._optTipo_2.TabIndex = 7
		Me._optTipo_2.Name = "_optTipo_2"
		Me.Image2.Size = New System.Drawing.Size(16, 16)
		Me.Image2.Location = New System.Drawing.Point(206, 16)
		Me.Image2.Image = CType(resources.GetObject("Image2.Image"), System.Drawing.Image)
		Me.Image2.Enabled = True
		Me.Image2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Image2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.Image2.Visible = True
		Me.Image2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Image2.Name = "Image2"
		Me.Image1.Size = New System.Drawing.Size(16, 16)
		Me.Image1.Location = New System.Drawing.Point(20, 14)
		Me.Image1.Image = CType(resources.GetObject("Image1.Image"), System.Drawing.Image)
		Me.Image1.Enabled = True
		Me.Image1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.Image1.Visible = True
		Me.Image1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Image1.Name = "Image1"
		FrameDes.OcxState = CType(resources.GetObject("FrameDes.OcxState"), System.Windows.Forms.AxHost.State)
		Me.FrameDes.Size = New System.Drawing.Size(497, 97)
		Me.FrameDes.Location = New System.Drawing.Point(748, 380)
		Me.FrameDes.TabIndex = 8
		Me.FrameDes.Name = "FrameDes"
		Me.Text39.AutoSize = False
		Me.Text39.BackColor = System.Drawing.Color.White
		Me.Text39.Enabled = False
		Me.Text39.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text39.Size = New System.Drawing.Size(324, 19)
		Me.Text39.Location = New System.Drawing.Point(150, 20)
		Me.Text39.TabIndex = 10
		Me.Text39.Tag = "^3"
		Me.Text39.AcceptsReturn = True
		Me.Text39.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text39.CausesValidation = True
		Me.Text39.HideSelection = True
		Me.Text39.ReadOnly = False
		Me.Text39.Maxlength = 0
		Me.Text39.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text39.MultiLine = False
		Me.Text39.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text39.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text39.TabStop = True
		Me.Text39.Visible = True
		Me.Text39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text39.Name = "Text39"
		Me.txtMotivoNoRealizado.AutoSize = False
		Me.txtMotivoNoRealizado.Size = New System.Drawing.Size(21, 19)
		Me.txtMotivoNoRealizado.Location = New System.Drawing.Point(126, 20)
		Me.txtMotivoNoRealizado.Maxlength = 2
		Me.txtMotivoNoRealizado.TabIndex = 9
		Me.txtMotivoNoRealizado.Tag = "3####T-MOTIUS_NO_REALITZAT#1#1"
		Me.txtMotivoNoRealizado.AcceptsReturn = True
		Me.txtMotivoNoRealizado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMotivoNoRealizado.BackColor = System.Drawing.SystemColors.Window
		Me.txtMotivoNoRealizado.CausesValidation = True
		Me.txtMotivoNoRealizado.Enabled = True
		Me.txtMotivoNoRealizado.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMotivoNoRealizado.HideSelection = True
		Me.txtMotivoNoRealizado.ReadOnly = False
		Me.txtMotivoNoRealizado.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMotivoNoRealizado.MultiLine = False
		Me.txtMotivoNoRealizado.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMotivoNoRealizado.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMotivoNoRealizado.TabStop = True
		Me.txtMotivoNoRealizado.Visible = True
		Me.txtMotivoNoRealizado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMotivoNoRealizado.Name = "txtMotivoNoRealizado"
		Me.txtComentarioDes.AutoSize = False
		Me.txtComentarioDes.Size = New System.Drawing.Size(347, 45)
		Me.txtComentarioDes.Location = New System.Drawing.Point(126, 42)
		Me.txtComentarioDes.Maxlength = 200
		Me.txtComentarioDes.MultiLine = True
		Me.txtComentarioDes.TabIndex = 11
		Me.txtComentarioDes.AcceptsReturn = True
		Me.txtComentarioDes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtComentarioDes.BackColor = System.Drawing.SystemColors.Window
		Me.txtComentarioDes.CausesValidation = True
		Me.txtComentarioDes.Enabled = True
		Me.txtComentarioDes.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtComentarioDes.HideSelection = True
		Me.txtComentarioDes.ReadOnly = False
		Me.txtComentarioDes.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtComentarioDes.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtComentarioDes.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtComentarioDes.TabStop = True
		Me.txtComentarioDes.Visible = True
		Me.txtComentarioDes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtComentarioDes.Name = "txtComentarioDes"
		Me.lbl39.Text = "Motivo no realizado"
		Me.lbl39.Size = New System.Drawing.Size(111, 15)
		Me.lbl39.Location = New System.Drawing.Point(12, 24)
		Me.lbl39.TabIndex = 13
		Me.lbl39.Tag = "39"
		Me.lbl39.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl39.BackColor = System.Drawing.SystemColors.Control
		Me.lbl39.Enabled = True
		Me.lbl39.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl39.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl39.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl39.UseMnemonic = True
		Me.lbl39.Visible = True
		Me.lbl39.AutoSize = False
		Me.lbl39.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl39.Name = "lbl39"
		Me.Label1.Text = "Comentario"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(12, 46)
		Me.Label1.TabIndex = 12
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		FrameTransp.OcxState = CType(resources.GetObject("FrameTransp.OcxState"), System.Windows.Forms.AxHost.State)
		Me.FrameTransp.Size = New System.Drawing.Size(725, 165)
		Me.FrameTransp.Location = New System.Drawing.Point(6, 380)
		Me.FrameTransp.TabIndex = 14
		Me.FrameTransp.Name = "FrameTransp"
		Me.txtMotivoNoRealizadoT.AutoSize = False
		Me.txtMotivoNoRealizadoT.Size = New System.Drawing.Size(21, 19)
		Me.txtMotivoNoRealizadoT.Location = New System.Drawing.Point(488, 92)
		Me.txtMotivoNoRealizadoT.Maxlength = 2
		Me.txtMotivoNoRealizadoT.TabIndex = 47
		Me.txtMotivoNoRealizadoT.Tag = "5####T-MOTIUS_NO_REALITZAT#1"
		Me.txtMotivoNoRealizadoT.Visible = False
		Me.txtMotivoNoRealizadoT.AcceptsReturn = True
		Me.txtMotivoNoRealizadoT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMotivoNoRealizadoT.BackColor = System.Drawing.SystemColors.Window
		Me.txtMotivoNoRealizadoT.CausesValidation = True
		Me.txtMotivoNoRealizadoT.Enabled = True
		Me.txtMotivoNoRealizadoT.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMotivoNoRealizadoT.HideSelection = True
		Me.txtMotivoNoRealizadoT.ReadOnly = False
		Me.txtMotivoNoRealizadoT.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMotivoNoRealizadoT.MultiLine = False
		Me.txtMotivoNoRealizadoT.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMotivoNoRealizadoT.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMotivoNoRealizadoT.TabStop = True
		Me.txtMotivoNoRealizadoT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMotivoNoRealizadoT.Name = "txtMotivoNoRealizadoT"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(202, 19)
		Me.Text1.Location = New System.Drawing.Point(512, 92)
		Me.Text1.TabIndex = 46
		Me.Text1.Tag = "^5"
		Me.Text1.Visible = False
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtKilometros.AutoSize = False
		Me.txtKilometros.Size = New System.Drawing.Size(55, 19)
		Me.txtKilometros.Location = New System.Drawing.Point(328, 68)
		Me.txtKilometros.Maxlength = 3
		Me.txtKilometros.TabIndex = 23
		Me.txtKilometros.AcceptsReturn = True
		Me.txtKilometros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtKilometros.BackColor = System.Drawing.SystemColors.Window
		Me.txtKilometros.CausesValidation = True
		Me.txtKilometros.Enabled = True
		Me.txtKilometros.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtKilometros.HideSelection = True
		Me.txtKilometros.ReadOnly = False
		Me.txtKilometros.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtKilometros.MultiLine = False
		Me.txtKilometros.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtKilometros.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtKilometros.TabStop = True
		Me.txtKilometros.Visible = True
		Me.txtKilometros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtKilometros.Name = "txtKilometros"
		Me.txtTransportista.AutoSize = False
		Me.txtTransportista.Size = New System.Drawing.Size(42, 19)
		Me.txtTransportista.Location = New System.Drawing.Point(98, 92)
		Me.txtTransportista.Maxlength = 4
		Me.txtTransportista.TabIndex = 24
		Me.txtTransportista.Tag = "2####T-TRANSPORTISTA#1#1"
		Me.txtTransportista.AcceptsReturn = True
		Me.txtTransportista.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTransportista.BackColor = System.Drawing.SystemColors.Window
		Me.txtTransportista.CausesValidation = True
		Me.txtTransportista.Enabled = True
		Me.txtTransportista.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTransportista.HideSelection = True
		Me.txtTransportista.ReadOnly = False
		Me.txtTransportista.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTransportista.MultiLine = False
		Me.txtTransportista.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTransportista.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTransportista.TabStop = True
		Me.txtTransportista.Visible = True
		Me.txtTransportista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTransportista.Name = "txtTransportista"
		Me.Text38.AutoSize = False
		Me.Text38.BackColor = System.Drawing.Color.White
		Me.Text38.Enabled = False
		Me.Text38.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text38.Size = New System.Drawing.Size(240, 19)
		Me.Text38.Location = New System.Drawing.Point(143, 92)
		Me.Text38.TabIndex = 37
		Me.Text38.Tag = "^2"
		Me.Text38.AcceptsReturn = True
		Me.Text38.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text38.CausesValidation = True
		Me.Text38.HideSelection = True
		Me.Text38.ReadOnly = False
		Me.Text38.Maxlength = 0
		Me.Text38.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text38.MultiLine = False
		Me.Text38.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text38.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text38.TabStop = True
		Me.Text38.Visible = True
		Me.Text38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text38.Name = "Text38"
		Me.txtTipoTarifa.AutoSize = False
		Me.txtTipoTarifa.Size = New System.Drawing.Size(21, 19)
		Me.txtTipoTarifa.Location = New System.Drawing.Point(464, 44)
		Me.txtTipoTarifa.Maxlength = 2
		Me.txtTipoTarifa.TabIndex = 20
		Me.txtTipoTarifa.Tag = "1####T-TIPUS_TARIFES#1"
		Me.txtTipoTarifa.Visible = False
		Me.txtTipoTarifa.AcceptsReturn = True
		Me.txtTipoTarifa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoTarifa.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoTarifa.CausesValidation = True
		Me.txtTipoTarifa.Enabled = True
		Me.txtTipoTarifa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoTarifa.HideSelection = True
		Me.txtTipoTarifa.ReadOnly = False
		Me.txtTipoTarifa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoTarifa.MultiLine = False
		Me.txtTipoTarifa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoTarifa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoTarifa.TabStop = True
		Me.txtTipoTarifa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoTarifa.Name = "txtTipoTarifa"
		Me.Text34.AutoSize = False
		Me.Text34.BackColor = System.Drawing.Color.White
		Me.Text34.Enabled = False
		Me.Text34.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text34.Size = New System.Drawing.Size(264, 19)
		Me.Text34.Location = New System.Drawing.Point(488, 44)
		Me.Text34.TabIndex = 15
		Me.Text34.Tag = "^1"
		Me.Text34.Visible = False
		Me.Text34.AcceptsReturn = True
		Me.Text34.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text34.CausesValidation = True
		Me.Text34.HideSelection = True
		Me.Text34.ReadOnly = False
		Me.Text34.Maxlength = 0
		Me.Text34.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text34.MultiLine = False
		Me.Text34.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text34.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text34.TabStop = True
		Me.Text34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text34.Name = "Text34"
		Me.txtComentario.AutoSize = False
		Me.txtComentario.Size = New System.Drawing.Size(573, 41)
		Me.txtComentario.Location = New System.Drawing.Point(98, 116)
		Me.txtComentario.Maxlength = 200
		Me.txtComentario.MultiLine = True
		Me.txtComentario.TabIndex = 25
		Me.txtComentario.AcceptsReturn = True
		Me.txtComentario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtComentario.BackColor = System.Drawing.SystemColors.Window
		Me.txtComentario.CausesValidation = True
		Me.txtComentario.Enabled = True
		Me.txtComentario.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtComentario.HideSelection = True
		Me.txtComentario.ReadOnly = False
		Me.txtComentario.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtComentario.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtComentario.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtComentario.TabStop = True
		Me.txtComentario.Visible = True
		Me.txtComentario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtComentario.Name = "txtComentario"
		txtFechaRecogida.OcxState = CType(resources.GetObject("txtFechaRecogida.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaRecogida.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaRecogida.Location = New System.Drawing.Point(98, 20)
		Me.txtFechaRecogida.TabIndex = 16
		Me.txtFechaRecogida.Name = "txtFechaRecogida"
		txtHoraRecogida.OcxState = CType(resources.GetObject("txtHoraRecogida.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtHoraRecogida.Size = New System.Drawing.Size(52, 19)
		Me.txtHoraRecogida.Location = New System.Drawing.Point(328, 20)
		Me.txtHoraRecogida.TabIndex = 17
		Me.txtHoraRecogida.Name = "txtHoraRecogida"
		txtFechaEntrega.OcxState = CType(resources.GetObject("txtFechaEntrega.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaEntrega.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaEntrega.Location = New System.Drawing.Point(98, 44)
		Me.txtFechaEntrega.TabIndex = 18
		Me.txtFechaEntrega.Name = "txtFechaEntrega"
		txtHoraEntrega.OcxState = CType(resources.GetObject("txtHoraEntrega.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtHoraEntrega.Size = New System.Drawing.Size(52, 19)
		Me.txtHoraEntrega.Location = New System.Drawing.Point(328, 44)
		Me.txtHoraEntrega.TabIndex = 19
		Me.txtHoraEntrega.Name = "txtHoraEntrega"
		txtPrecio.OcxState = CType(resources.GetObject("txtPrecio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtPrecio.Size = New System.Drawing.Size(61, 19)
		Me.txtPrecio.Location = New System.Drawing.Point(468, 68)
		Me.txtPrecio.TabIndex = 21
		Me.txtPrecio.Name = "txtPrecio"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(43, 41)
		Me.cmdGuardar.Location = New System.Drawing.Point(672, 116)
		Me.cmdGuardar.TabIndex = 26
		Me.cmdGuardar.Name = "cmdGuardar"
		txtHorasTransporte.OcxState = CType(resources.GetObject("txtHorasTransporte.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtHorasTransporte.Size = New System.Drawing.Size(52, 19)
		Me.txtHorasTransporte.Location = New System.Drawing.Point(98, 68)
		Me.txtHorasTransporte.TabIndex = 22
		Me.txtHorasTransporte.Name = "txtHorasTransporte"
		txtVdaInt.OcxState = CType(resources.GetObject("txtVdaInt.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtVdaInt.Size = New System.Drawing.Size(61, 19)
		Me.txtVdaInt.Location = New System.Drawing.Point(652, 68)
		Me.txtVdaInt.TabIndex = 49
		Me.txtVdaInt.Name = "txtVdaInt"
		Me.lblVdaInt.Text = "Importe vda. interna"
		Me.lblVdaInt.Size = New System.Drawing.Size(111, 15)
		Me.lblVdaInt.Location = New System.Drawing.Point(552, 72)
		Me.lblVdaInt.TabIndex = 50
		Me.lblVdaInt.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblVdaInt.BackColor = System.Drawing.SystemColors.Control
		Me.lblVdaInt.Enabled = True
		Me.lblVdaInt.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblVdaInt.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblVdaInt.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblVdaInt.UseMnemonic = True
		Me.lblVdaInt.Visible = True
		Me.lblVdaInt.AutoSize = False
		Me.lblVdaInt.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblVdaInt.Name = "lblVdaInt"
		Me.Label6.Text = "Motivo no realizado"
		Me.Label6.Size = New System.Drawing.Size(99, 15)
		Me.Label6.Location = New System.Drawing.Point(388, 96)
		Me.Label6.TabIndex = 48
		Me.Label6.Tag = "39"
		Me.Label6.Visible = False
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.Label5.Text = "Kil�metros recorridos"
		Me.Label5.Size = New System.Drawing.Size(111, 15)
		Me.Label5.Location = New System.Drawing.Point(222, 72)
		Me.Label5.TabIndex = 45
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label4.Text = "Horas transporte"
		Me.Label4.Size = New System.Drawing.Size(111, 15)
		Me.Label4.Location = New System.Drawing.Point(8, 72)
		Me.Label4.TabIndex = 44
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.lbl38.Text = "Transportista"
		Me.lbl38.Size = New System.Drawing.Size(111, 15)
		Me.lbl38.Location = New System.Drawing.Point(8, 96)
		Me.lbl38.TabIndex = 38
		Me.lbl38.Tag = "38"
		Me.lbl38.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl38.BackColor = System.Drawing.SystemColors.Control
		Me.lbl38.Enabled = True
		Me.lbl38.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl38.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl38.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl38.UseMnemonic = True
		Me.lbl38.Visible = True
		Me.lbl38.AutoSize = False
		Me.lbl38.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl38.Name = "lbl38"
		Me.lbl42.Text = "Fecha recogida"
		Me.lbl42.Size = New System.Drawing.Size(111, 15)
		Me.lbl42.Location = New System.Drawing.Point(8, 24)
		Me.lbl42.TabIndex = 36
		Me.lbl42.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl42.BackColor = System.Drawing.SystemColors.Control
		Me.lbl42.Enabled = True
		Me.lbl42.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl42.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl42.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl42.UseMnemonic = True
		Me.lbl42.Visible = True
		Me.lbl42.AutoSize = False
		Me.lbl42.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl42.Name = "lbl42"
		Me.lbl43.Text = "Hora recogida"
		Me.lbl43.Size = New System.Drawing.Size(111, 15)
		Me.lbl43.Location = New System.Drawing.Point(222, 24)
		Me.lbl43.TabIndex = 33
		Me.lbl43.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl43.BackColor = System.Drawing.SystemColors.Control
		Me.lbl43.Enabled = True
		Me.lbl43.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl43.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl43.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl43.UseMnemonic = True
		Me.lbl43.Visible = True
		Me.lbl43.AutoSize = False
		Me.lbl43.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl43.Name = "lbl43"
		Me.lbl40.Text = "Fecha entrega"
		Me.lbl40.Size = New System.Drawing.Size(111, 15)
		Me.lbl40.Location = New System.Drawing.Point(8, 48)
		Me.lbl40.TabIndex = 32
		Me.lbl40.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl40.BackColor = System.Drawing.SystemColors.Control
		Me.lbl40.Enabled = True
		Me.lbl40.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl40.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl40.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl40.UseMnemonic = True
		Me.lbl40.Visible = True
		Me.lbl40.AutoSize = False
		Me.lbl40.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl40.Name = "lbl40"
		Me.lbl41.Text = "Hora entrega"
		Me.lbl41.Size = New System.Drawing.Size(111, 15)
		Me.lbl41.Location = New System.Drawing.Point(222, 48)
		Me.lbl41.TabIndex = 31
		Me.lbl41.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl41.BackColor = System.Drawing.SystemColors.Control
		Me.lbl41.Enabled = True
		Me.lbl41.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl41.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl41.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl41.UseMnemonic = True
		Me.lbl41.Visible = True
		Me.lbl41.AutoSize = False
		Me.lbl41.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl41.Name = "lbl41"
		Me.lbl34.Text = "Tipo tarifa"
		Me.lbl34.Size = New System.Drawing.Size(111, 15)
		Me.lbl34.Location = New System.Drawing.Point(410, 48)
		Me.lbl34.TabIndex = 30
		Me.lbl34.Tag = "34"
		Me.lbl34.Visible = False
		Me.lbl34.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl34.BackColor = System.Drawing.SystemColors.Control
		Me.lbl34.Enabled = True
		Me.lbl34.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl34.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl34.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl34.UseMnemonic = True
		Me.lbl34.AutoSize = False
		Me.lbl34.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl34.Name = "lbl34"
		Me.lbl35.Text = "Importe coste"
		Me.lbl35.Size = New System.Drawing.Size(111, 15)
		Me.lbl35.Location = New System.Drawing.Point(400, 72)
		Me.lbl35.TabIndex = 29
		Me.lbl35.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl35.BackColor = System.Drawing.SystemColors.Control
		Me.lbl35.Enabled = True
		Me.lbl35.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl35.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl35.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl35.UseMnemonic = True
		Me.lbl35.Visible = True
		Me.lbl35.AutoSize = False
		Me.lbl35.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl35.Name = "lbl35"
		Me.lbl44.Text = "Comentario"
		Me.lbl44.Size = New System.Drawing.Size(111, 15)
		Me.lbl44.Location = New System.Drawing.Point(8, 120)
		Me.lbl44.TabIndex = 27
		Me.lbl44.Tag = "44"
		Me.lbl44.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl44.BackColor = System.Drawing.SystemColors.Control
		Me.lbl44.Enabled = True
		Me.lbl44.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl44.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl44.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl44.UseMnemonic = True
		Me.lbl44.Visible = True
		Me.lbl44.AutoSize = False
		Me.lbl44.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl44.Name = "lbl44"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(1036, 520)
		Me.cmdAceptar.TabIndex = 28
		Me.cmdAceptar.Name = "cmdAceptar"
		grdSolPlanificades.OcxState = CType(resources.GetObject("grdSolPlanificades.OcxState"), System.Windows.Forms.AxHost.State)
		Me.grdSolPlanificades.Size = New System.Drawing.Size(1112, 329)
		Me.grdSolPlanificades.Location = New System.Drawing.Point(4, 46)
		Me.grdSolPlanificades.TabIndex = 34
		Me.grdSolPlanificades.Name = "grdSolPlanificades"
		txtDataIni.OcxState = CType(resources.GetObject("txtDataIni.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataIni.Size = New System.Drawing.Size(87, 19)
		Me.txtDataIni.Location = New System.Drawing.Point(42, 18)
		Me.txtDataIni.TabIndex = 1
		Me.txtDataIni.Name = "txtDataIni"
		txtDataFi.OcxState = CType(resources.GetObject("txtDataFi.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataFi.Size = New System.Drawing.Size(87, 19)
		Me.txtDataFi.Location = New System.Drawing.Point(174, 18)
		Me.txtDataFi.TabIndex = 2
		Me.txtDataFi.Name = "txtDataFi"
		cmdFiltraData.OcxState = CType(resources.GetObject("cmdFiltraData.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdFiltraData.Size = New System.Drawing.Size(33, 27)
		Me.cmdFiltraData.Location = New System.Drawing.Point(656, 14)
		Me.cmdFiltraData.TabIndex = 4
		Me.cmdFiltraData.Name = "cmdFiltraData"
		cmdImprimir.OcxState = CType(resources.GetObject("cmdImprimir.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdImprimir.Size = New System.Drawing.Size(75, 36)
		Me.cmdImprimir.Location = New System.Drawing.Point(1040, 6)
		Me.cmdImprimir.TabIndex = 41
		Me.cmdImprimir.Name = "cmdImprimir"
		Me.Label3.Text = "Transportista"
		Me.Label3.Size = New System.Drawing.Size(111, 15)
		Me.Label3.Location = New System.Drawing.Point(268, 22)
		Me.Label3.TabIndex = 43
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Hasta"
		Me.Label2.Size = New System.Drawing.Size(31, 15)
		Me.Label2.Location = New System.Drawing.Point(134, 22)
		Me.Label2.TabIndex = 40
		Me.Label2.Tag = "4"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.lbl4.Text = "De"
		Me.lbl4.Size = New System.Drawing.Size(31, 15)
		Me.lbl4.Location = New System.Drawing.Point(6, 22)
		Me.lbl4.TabIndex = 39
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.optTipo.SetIndex(_optTipo_1, CType(1, Short))
		Me.optTipo.SetIndex(_optTipo_2, CType(2, Short))
		CType(Me.optTipo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdImprimir, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdFiltraData, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataFi, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataIni, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.grdSolPlanificades, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.FrameTransp, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtVdaInt, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtHorasTransporte, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtPrecio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtHoraEntrega, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaEntrega, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtHoraRecogida, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaRecogida, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.FrameDes, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.frTipo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optTipo_2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optTipo_1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtTransportistaF)
		Me.Controls.Add(txtSolicitud)
		Me.Controls.Add(frTipo)
		Me.Controls.Add(FrameDes)
		Me.Controls.Add(FrameTransp)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(grdSolPlanificades)
		Me.Controls.Add(txtDataIni)
		Me.Controls.Add(txtDataFi)
		Me.Controls.Add(cmdFiltraData)
		Me.Controls.Add(cmdImprimir)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(lbl4)
		Me.frTipo.Controls.Add(txtTipo)
		Me.frTipo.Controls.Add(_optTipo_1)
		Me.frTipo.Controls.Add(_optTipo_2)
		Me.frTipo.Controls.Add(Image2)
		Me.frTipo.Controls.Add(Image1)
		Me.FrameDes.Controls.Add(Text39)
		Me.FrameDes.Controls.Add(txtMotivoNoRealizado)
		Me.FrameDes.Controls.Add(txtComentarioDes)
		Me.FrameDes.Controls.Add(lbl39)
		Me.FrameDes.Controls.Add(Label1)
		Me.FrameTransp.Controls.Add(txtMotivoNoRealizadoT)
		Me.FrameTransp.Controls.Add(Text1)
		Me.FrameTransp.Controls.Add(txtKilometros)
		Me.FrameTransp.Controls.Add(txtTransportista)
		Me.FrameTransp.Controls.Add(Text38)
		Me.FrameTransp.Controls.Add(txtTipoTarifa)
		Me.FrameTransp.Controls.Add(Text34)
		Me.FrameTransp.Controls.Add(txtComentario)
		Me.FrameTransp.Controls.Add(txtFechaRecogida)
		Me.FrameTransp.Controls.Add(txtHoraRecogida)
		Me.FrameTransp.Controls.Add(txtFechaEntrega)
		Me.FrameTransp.Controls.Add(txtHoraEntrega)
		Me.FrameTransp.Controls.Add(txtPrecio)
		Me.FrameTransp.Controls.Add(cmdGuardar)
		Me.FrameTransp.Controls.Add(txtHorasTransporte)
		Me.FrameTransp.Controls.Add(txtVdaInt)
		Me.FrameTransp.Controls.Add(lblVdaInt)
		Me.FrameTransp.Controls.Add(Label6)
		Me.FrameTransp.Controls.Add(Label5)
		Me.FrameTransp.Controls.Add(Label4)
		Me.FrameTransp.Controls.Add(lbl38)
		Me.FrameTransp.Controls.Add(lbl42)
		Me.FrameTransp.Controls.Add(lbl43)
		Me.FrameTransp.Controls.Add(lbl40)
		Me.FrameTransp.Controls.Add(lbl41)
		Me.FrameTransp.Controls.Add(lbl34)
		Me.FrameTransp.Controls.Add(lbl35)
		Me.FrameTransp.Controls.Add(lbl44)
		Me.frTipo.ResumeLayout(False)
		Me.FrameDes.ResumeLayout(False)
		Me.FrameTransp.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class