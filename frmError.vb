Option Strict Off
Option Explicit On
Friend Class frmError
	Inherits FormParent
	
	Private Sub cmdCorrecte_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCorrecte.Click
		CacheNetejaParametres()
		MCache.P1 = frmControlGates.txtAlbara
		MCache.P2 = frmControlGates.txtFaixa
		MCache.P3 = frmControlGates.txtEtiqueta
		MCache.P4 = "NO"
		CacheXecute("D GRAVA^GATES")
		frmControlGates.txtEtiqueta.Text = ""
		frmControlGates.txtFaixa.Text = ""
		Me.Unload()
		frmControlGates.txtFaixa.Focus()
	End Sub
	
	
	Private Sub frmError_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		CentrarForm(Me)
	End Sub
End Class
