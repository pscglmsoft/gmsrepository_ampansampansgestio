Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmServeisUsuaris
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	Public Fases As Boolean
	'Public usuariExt As String
	'Public serveiExt As String
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		CarregaProgrames()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	'UPGRADE_WARNING: Form evento frmServeisUsuaris.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmServeisUsuaris_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaProgrames()
	End Sub
	
	Private Sub frmServeisUsuaris_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
		Fases = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		Dim Obert As Boolean
		Obert = False
		
		If txtFechaBaja.Text <> "" And txtMotivoBaja.Text = "" Then
			xMsgBox("Falta informar el motivo de baja", MsgBoxStyle.Information, Me.Text)
			txtMotivoBaja.Focus()
			Exit Sub
		End If
		
		
		
		If frmUsuariInsercioLaboral.Visible = True Then
			frmUsuariInsercioLaboral.Unload()
			Obert = True
		End If
		
		If txtFechaBaja.Text = "" And txtMotivoBaja.Text <> "" Then
			txtMotivoBaja.Text = ""
		End If
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		If Obert = True Then
			ObreFormulari(frmUsuariInsercioLaboral, "frmUsuariInsercioLaboral", txtUsuario.Text)
			Obert = False
		End If
		
		If CDbl(txtServicio.Text) = 4 Then txtF1.Text = txtFechaAlta.Text
		
		Inici()
		
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		frmUsuariInsercioLaboral.Unload()
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		
		If CDbl(txtServicio.Text) = 4 Then txtF1.Text = txtFechaAlta.Text
	End Sub
	
	Private Sub frmServeisUsuaris_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		If Fases = True Then
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtFechaIntermediacion.Enabled = True
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtFechaPrepLaboral.Enabled = True
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtFechaRecogidaDatos.Enabled = True
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtFechaValoracion.Enabled = True
		Else
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtFechaIntermediacion.Enabled = False
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtFechaPrepLaboral.Enabled = False
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtFechaRecogidaDatos.Enabled = False
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtFechaValoracion.Enabled = False
		End If
		'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		FrameNovesOp.Enabled = False
	End Sub
	
	Private Sub frmServeisUsuaris_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmServeisUsuaris_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	Private Sub GrdProgrames_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdProgrames.DoubleClick
		ObrePrograma()
	End Sub
	
	Private Sub GrdProgrames_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdProgrames.MouseUp
		If eventArgs.Button = MouseButtons.Left And grdProgrames.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdProgrames", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar programa",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar programa",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nuevo programa",  , True,  ,  ,  ,  ,  , "mnuNew")
				If grdProgrames.ActiveCell.Row < 1 Or grdProgrames.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "grdProgrames")
	End Sub
	
	
	
	
	
	
	Private Sub txtF1_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtF1.GotFocus
		XGotFocus(Me, txtF1)
	End Sub
	
	Private Sub txtF1_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtF1.LostFocus
		XLostFocus(Me, txtF1)
	End Sub
	
	
	
	Private Sub txtF2_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtF2.GotFocus
		XGotFocus(Me, txtF2)
	End Sub
	
	Private Sub txtF2_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtF2.LostFocus
		XLostFocus(Me, txtF2)
	End Sub
	
	
	
	Private Sub txtF3_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtF3.GotFocus
		XGotFocus(Me, txtF3)
	End Sub
	
	Private Sub txtF3_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtF3.LostFocus
		XLostFocus(Me, txtF3)
	End Sub
	
	Private Sub txtF4_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtF4.GotFocus
		XGotFocus(Me, txtF4)
	End Sub
	
	Private Sub txtF4_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtF4.LostFocus
		XLostFocus(Me, txtF4)
	End Sub
	
	Private Sub txtFechaIntermediacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaIntermediacion.GotFocus
		XGotFocus(Me, txtFechaIntermediacion)
	End Sub
	
	Private Sub txtFechaIntermediacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaIntermediacion.LostFocus
		XLostFocus(Me, txtFechaIntermediacion)
	End Sub
	
	Private Sub txtFechaPrepLaboral_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaPrepLaboral.GotFocus
		XGotFocus(Me, txtFechaPrepLaboral)
	End Sub
	
	Private Sub txtFechaPrepLaboral_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaPrepLaboral.LostFocus
		XLostFocus(Me, txtFechaPrepLaboral)
	End Sub
	
	Private Sub txtFechaRecogidaDatos_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaRecogidaDatos.GotFocus
		XGotFocus(Me, txtFechaRecogidaDatos)
	End Sub
	
	Private Sub txtFechaRecogidaDatos_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaRecogidaDatos.LostFocus
		XLostFocus(Me, txtFechaRecogidaDatos)
	End Sub
	
	
	Private Sub txtFechaValoracion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaValoracion.GotFocus
		XGotFocus(Me, txtFechaValoracion)
	End Sub
	
	Private Sub txtFechaValoracion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaValoracion.LostFocus
		XLostFocus(Me, txtFechaValoracion)
	End Sub
	
	Private Sub txtMotivoAlta_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoAlta.DoubleClick
		ConsultaTaula(Me, txtMotivoAlta)
	End Sub
	
	Private Sub txtMotivoAlta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoAlta.GotFocus
		XGotFocus(Me, txtMotivoAlta)
	End Sub
	
	Private Sub txtMotivoAlta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoAlta.LostFocus
		XLostFocus(Me, txtMotivoAlta)
	End Sub
	
	Private Sub txtMotivoAlta_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMotivoAlta.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtPuntControl_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntControl.DoubleClick
		ConsultaTaula(Me, txtPuntControl)
	End Sub
	
	Private Sub txtPuntControl_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntControl.GotFocus
		XGotFocus(Me, txtPuntControl)
	End Sub
	
	Private Sub txtPuntControl_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntControl.LostFocus
		XLostFocus(Me, txtPuntControl)
	End Sub
	
	Private Sub txtPuntControl_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPuntControl.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtTecnico_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnico.DoubleClick
		ConsultaTaula(Me, txtTecnico)
	End Sub
	
	Private Sub txtTecnico_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnico.GotFocus
		XGotFocus(Me, txtTecnico)
	End Sub
	
	Private Sub txtTecnico_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnico.LostFocus
		XLostFocus(Me, txtTecnico)
	End Sub
	
	Private Sub txtTecnico_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTecnico.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtUsuario_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.GotFocus
		XGotFocus(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.DoubleClick
		ConsultaTaula(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.LostFocus
		If txtUsuario.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUsuario.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtServicio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServicio.GotFocus
		XGotFocus(Me, txtServicio)
	End Sub
	
	Private Sub txtServicio_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServicio.DoubleClick
		ConsultaTaula(Me, txtServicio)
	End Sub
	
	Private Sub txtServicio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServicio.LostFocus
		If txtServicio.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtServicio)
	End Sub
	
	Private Sub txtServicio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtServicio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaAlta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaAlta.GotFocus
		XGotFocus(Me, txtFechaAlta)
	End Sub
	
	Private Sub txtFechaAlta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaAlta.LostFocus
		If txtFechaAlta.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtFechaAlta)
	End Sub
	
	Private Sub txtFechaBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.GotFocus
		XGotFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtFechaBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.LostFocus
		XLostFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtMotivoBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoBaja.GotFocus
		XGotFocus(Me, txtMotivoBaja)
	End Sub
	
	Private Sub txtMotivoBaja_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoBaja.DoubleClick
		ConsultaTaula(Me, txtMotivoBaja)
	End Sub
	
	Private Sub txtMotivoBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoBaja.LostFocus
		XLostFocus(Me, txtMotivoBaja)
	End Sub
	
	Private Sub txtMotivoBaja_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMotivoBaja.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservaciones_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservaciones.GotFocus
		XGotFocus(Me, txtObservaciones)
	End Sub
	
	Private Sub txtObservaciones_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservaciones.LostFocus
		XLostFocus(Me, txtObservaciones)
	End Sub
	
	Private Sub txtObservaciones_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservaciones.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub CarregaProgrames()
		MCache.P1 = txtUsuario.Text
		'txtUsuario = usuariExt
		MCache.P2 = txtServicio.Text
		'txtServicio = serveiExt
		CarregaFGrid(grdProgrames, "CGPROG^INSERCIO", txtUsuario.Text & S & txtServicio.Text)
	End Sub
	
	Private Sub ObrePrograma()
		Dim Us As Short
		Dim SER As Short
		Dim Data As String
		Dim Prog As Short
		
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Us = Piece(grdProgrames.Cell(grdProgrames.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SER = Piece(grdProgrames.Cell(grdProgrames.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Prog = Piece(grdProgrames.Cell(grdProgrames.ActiveCell.Row, 1).Text, "|", 3)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(grdProgrames.Cell(grdProgrames.ActiveCell.Row, 1).Text, "|", 4)
		ObreFormulari(frmUsuariPrograma, "frmUsuariPrograma", Us & S & SER & S & Prog & S & Data)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
	End Sub
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Select Case NomMenu
			Case "grdProgrames"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuNew"
						ObreFormulari(frmUsuariPrograma, "frmUsuariPrograma")
						frmUsuariPrograma.txtUsuari.Text = txtUsuario.Text
						frmUsuariPrograma.txtUsuari.Enabled = False
						DisplayDescripcio(frmUsuariPrograma, (frmUsuariPrograma.txtUsuari))
						frmUsuariPrograma.txtServei.Text = txtServicio.Text
						frmUsuariPrograma.txtServei.Enabled = False
						DisplayDescripcio(frmUsuariPrograma, (frmUsuariPrograma.txtServei))
						frmUsuariPrograma.txtDataInici.Text = txtFechaAlta.Text
					Case "mnuEdit"
						If grdProgrames.ActiveCell.Row = 0 Then Exit Sub
						ObrePrograma()
					Case "mnuDelete"
						If grdProgrames.ActiveCell.Row = 0 Then Exit Sub
						
						A = grdProgrames.Cell(grdProgrames.ActiveCell.Row, 2).Text
						If A = "" Then Exit Sub
						ObrePrograma()
						LlibAplicacio.DeleteReg(frmUsuariPrograma   )
						frmUsuariPrograma.Unload()
				End Select
		End Select
	End Sub
End Class
