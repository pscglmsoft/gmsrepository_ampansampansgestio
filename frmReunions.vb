Option Strict Off
Option Explicit On
Friend Class frmReunions
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		txtTipusDemanda.Text = CStr(5)
		DisplayDescripcio(Me, txtTipusDemanda)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If VerificaCamps = False Then Exit Sub
		'
		CacheNetejaParametres()
		MCache.P1 = txtDemandant.Text
		MCache.P2 = txtData.Text
		MCache.P3 = txtDatalimit.Text
		MCache.P4 = txtTipusDemanda.Text
		MCache.P5 = txtSubtipusDemanda.Text
		MCache.P6 = txtProblema.Text
		MCache.P7 = FGMontaKeysSeleccio(GrdComissio, 3)
		
		If MCache.P7 = "" Then
			xMsgBox("Falta sel�leccionar algun membre de la comissi� de Direcci�", MsgBoxStyle.Information, Me.Text)
			Exit Sub
		End If
		
		CacheXecute("D GRAVPETI^DEMANDES")
		xMsgBox("Peticions gravades correctament !!! ", MsgBoxStyle.Information, Me.Text)
		Inici()
	End Sub
	
	
	Private Sub frmReunions_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmReunions_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		txtTipusDemanda.Text = CStr(5)
		DisplayDescripcio(Me, txtTipusDemanda)
		CarregaGridComissio()
		
	End Sub
	
	
	
	Private Function VerificaCamps() As Boolean
		VerificaCamps = True
		
		If txtDemandant.Text = "" Then
			xMsgBox("Falta informar el Demandant", MsgBoxStyle.Information, Me.Text)
			txtDemandant.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If txtData.Text = "" Then
			xMsgBox("Falta informar la Data", MsgBoxStyle.Information, Me.Text)
			txtData.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		
		If txtSubtipusDemanda.Text = "" Then
			xMsgBox("Falta informar el Subtipus demanda", MsgBoxStyle.Information, Me.Text)
			txtSubtipusDemanda.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If txtProblema.Text = "" Then
			xMsgBox("Falta informar el problema", MsgBoxStyle.Information, Me.Text)
			txtProblema.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		
		
		
		
	End Function
	
	
	Private Sub txtData_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtData.GotFocus
		XGotFocus(Me, txtData)
	End Sub
	
	Private Sub txtData_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtData.LostFocus
		XLostFocus(Me, txtData)
	End Sub
	
	Private Sub txtDatalimit_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDatalimit.GotFocus
		XGotFocus(Me, txtDatalimit)
	End Sub
	
	Private Sub txtDatalimit_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDatalimit.LostFocus
		XLostFocus(Me, txtDatalimit)
	End Sub
	
	Private Sub txtDemandant_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDemandant.DoubleClick
		ConsultaTaula(Me, txtDemandant)
	End Sub
	
	Private Sub txtDemandant_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDemandant.GotFocus
		XGotFocus(Me, txtDemandant)
	End Sub
	
	Private Sub txtDemandant_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDemandant.LostFocus
		XLostFocus(Me, txtDemandant)
	End Sub
	
	Private Sub txtDemandant_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDemandant.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtSubtipusDemanda_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubtipusDemanda.DoubleClick
		ConsultaTaula(Me, txtSubtipusDemanda,  , txtTipusDemanda.Text)
	End Sub
	
	Private Sub txtSubtipusDemanda_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSubtipusDemanda.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtTipusDemanda.Text)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipusDemanda_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusDemanda.DoubleClick
		'    ConsultaTaula Me, txtTipusDemanda
	End Sub
	
	Private Sub txtTipusDemanda_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipusDemanda.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	Private Sub CarregaGridComissio()
		CarregaFGrid(GrdComissio, "CGCOMI^DEMANDES")
	End Sub
End Class
