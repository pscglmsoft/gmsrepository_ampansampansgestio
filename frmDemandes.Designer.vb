<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDemandes
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtUsuariEntigestGestio As System.Windows.Forms.TextBox
	Public WithEvents txtCodiDemandaOrigen As System.Windows.Forms.TextBox
	Public WithEvents txtCodi As System.Windows.Forms.TextBox
	Public WithEvents txtDemandant As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtData As AxDataControl.AxGmsData
	Public WithEvents txtTipusDemanda As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtSubtipusDemanda As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtCentreQueGestiona As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtPersonaQueGestiona As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents txtProblema As System.Windows.Forms.TextBox
	Public WithEvents cmbEstatDemanda As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtDataTancament As AxDataControl.AxGmsData
	Public WithEvents txtDatalimit As AxDataControl.AxGmsData
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDemandes))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtUsuariEntigestGestio = New System.Windows.Forms.TextBox
		Me.txtCodiDemandaOrigen = New System.Windows.Forms.TextBox
		Me.txtCodi = New System.Windows.Forms.TextBox
		Me.txtDemandant = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtData = New AxDataControl.AxGmsData
		Me.txtTipusDemanda = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtSubtipusDemanda = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtCentreQueGestiona = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtPersonaQueGestiona = New System.Windows.Forms.TextBox
		Me.Text7 = New System.Windows.Forms.TextBox
		Me.txtProblema = New System.Windows.Forms.TextBox
		Me.cmbEstatDemanda = New AxSSDataWidgets_B.AxSSDBCombo
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.txtDataTancament = New AxDataControl.AxGmsData
		Me.txtDatalimit = New AxDataControl.AxGmsData
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbEstatDemanda, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataTancament, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDatalimit, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Demandes"
		Me.ClientSize = New System.Drawing.Size(597, 437)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "DEM-DEMANDES"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmDemandes"
		Me.txtUsuariEntigestGestio.AutoSize = False
		Me.txtUsuariEntigestGestio.Size = New System.Drawing.Size(93, 19)
		Me.txtUsuariEntigestGestio.Location = New System.Drawing.Point(306, 362)
		Me.txtUsuariEntigestGestio.ReadOnly = True
		Me.txtUsuariEntigestGestio.Maxlength = 9
		Me.txtUsuariEntigestGestio.TabIndex = 31
		Me.txtUsuariEntigestGestio.Tag = "12"
		Me.txtUsuariEntigestGestio.AcceptsReturn = True
		Me.txtUsuariEntigestGestio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsuariEntigestGestio.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsuariEntigestGestio.CausesValidation = True
		Me.txtUsuariEntigestGestio.Enabled = True
		Me.txtUsuariEntigestGestio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsuariEntigestGestio.HideSelection = True
		Me.txtUsuariEntigestGestio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsuariEntigestGestio.MultiLine = False
		Me.txtUsuariEntigestGestio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsuariEntigestGestio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsuariEntigestGestio.TabStop = True
		Me.txtUsuariEntigestGestio.Visible = True
		Me.txtUsuariEntigestGestio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUsuariEntigestGestio.Name = "txtUsuariEntigestGestio"
		Me.txtCodiDemandaOrigen.AutoSize = False
		Me.txtCodiDemandaOrigen.Size = New System.Drawing.Size(62, 19)
		Me.txtCodiDemandaOrigen.Location = New System.Drawing.Point(522, 362)
		Me.txtCodiDemandaOrigen.Maxlength = 6
		Me.txtCodiDemandaOrigen.TabIndex = 29
		Me.txtCodiDemandaOrigen.Tag = "11"
		Me.txtCodiDemandaOrigen.AcceptsReturn = True
		Me.txtCodiDemandaOrigen.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodiDemandaOrigen.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodiDemandaOrigen.CausesValidation = True
		Me.txtCodiDemandaOrigen.Enabled = True
		Me.txtCodiDemandaOrigen.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodiDemandaOrigen.HideSelection = True
		Me.txtCodiDemandaOrigen.ReadOnly = False
		Me.txtCodiDemandaOrigen.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodiDemandaOrigen.MultiLine = False
		Me.txtCodiDemandaOrigen.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodiDemandaOrigen.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodiDemandaOrigen.TabStop = True
		Me.txtCodiDemandaOrigen.Visible = True
		Me.txtCodiDemandaOrigen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodiDemandaOrigen.Name = "txtCodiDemandaOrigen"
		Me.txtCodi.AutoSize = False
		Me.txtCodi.Size = New System.Drawing.Size(62, 19)
		Me.txtCodi.Location = New System.Drawing.Point(124, 10)
		Me.txtCodi.Maxlength = 6
		Me.txtCodi.TabIndex = 26
		Me.txtCodi.Tag = "*1"
		Me.txtCodi.AcceptsReturn = True
		Me.txtCodi.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodi.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodi.CausesValidation = True
		Me.txtCodi.Enabled = True
		Me.txtCodi.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodi.HideSelection = True
		Me.txtCodi.ReadOnly = False
		Me.txtCodi.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodi.MultiLine = False
		Me.txtCodi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodi.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodi.TabStop = True
		Me.txtCodi.Visible = True
		Me.txtCodi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodi.Name = "txtCodi"
		Me.txtDemandant.AutoSize = False
		Me.txtDemandant.Size = New System.Drawing.Size(93, 19)
		Me.txtDemandant.Location = New System.Drawing.Point(124, 34)
		Me.txtDemandant.Maxlength = 9
		Me.txtDemandant.TabIndex = 1
		Me.txtDemandant.Tag = "2"
		Me.txtDemandant.AcceptsReturn = True
		Me.txtDemandant.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDemandant.BackColor = System.Drawing.SystemColors.Window
		Me.txtDemandant.CausesValidation = True
		Me.txtDemandant.Enabled = True
		Me.txtDemandant.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDemandant.HideSelection = True
		Me.txtDemandant.ReadOnly = False
		Me.txtDemandant.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDemandant.MultiLine = False
		Me.txtDemandant.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDemandant.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDemandant.TabStop = True
		Me.txtDemandant.Visible = True
		Me.txtDemandant.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDemandant.Name = "txtDemandant"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(362, 19)
		Me.Text2.Location = New System.Drawing.Point(221, 34)
		Me.Text2.TabIndex = 3
		Me.Text2.Tag = "^2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		txtData.OcxState = CType(resources.GetObject("txtData.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtData.Size = New System.Drawing.Size(87, 19)
		Me.txtData.Location = New System.Drawing.Point(124, 58)
		Me.txtData.TabIndex = 5
		Me.txtData.Name = "txtData"
		Me.txtTipusDemanda.AutoSize = False
		Me.txtTipusDemanda.Size = New System.Drawing.Size(42, 19)
		Me.txtTipusDemanda.Location = New System.Drawing.Point(124, 82)
		Me.txtTipusDemanda.Maxlength = 4
		Me.txtTipusDemanda.TabIndex = 8
		Me.txtTipusDemanda.Tag = "4"
		Me.txtTipusDemanda.AcceptsReturn = True
		Me.txtTipusDemanda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipusDemanda.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipusDemanda.CausesValidation = True
		Me.txtTipusDemanda.Enabled = True
		Me.txtTipusDemanda.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipusDemanda.HideSelection = True
		Me.txtTipusDemanda.ReadOnly = False
		Me.txtTipusDemanda.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipusDemanda.MultiLine = False
		Me.txtTipusDemanda.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipusDemanda.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipusDemanda.TabStop = True
		Me.txtTipusDemanda.Visible = True
		Me.txtTipusDemanda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipusDemanda.Name = "txtTipusDemanda"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(414, 19)
		Me.Text4.Location = New System.Drawing.Point(169, 82)
		Me.Text4.TabIndex = 9
		Me.Text4.Tag = "^4"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtSubtipusDemanda.AutoSize = False
		Me.txtSubtipusDemanda.Size = New System.Drawing.Size(42, 19)
		Me.txtSubtipusDemanda.Location = New System.Drawing.Point(124, 106)
		Me.txtSubtipusDemanda.Maxlength = 4
		Me.txtSubtipusDemanda.TabIndex = 11
		Me.txtSubtipusDemanda.Tag = "5"
		Me.txtSubtipusDemanda.AcceptsReturn = True
		Me.txtSubtipusDemanda.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSubtipusDemanda.BackColor = System.Drawing.SystemColors.Window
		Me.txtSubtipusDemanda.CausesValidation = True
		Me.txtSubtipusDemanda.Enabled = True
		Me.txtSubtipusDemanda.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSubtipusDemanda.HideSelection = True
		Me.txtSubtipusDemanda.ReadOnly = False
		Me.txtSubtipusDemanda.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSubtipusDemanda.MultiLine = False
		Me.txtSubtipusDemanda.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSubtipusDemanda.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSubtipusDemanda.TabStop = True
		Me.txtSubtipusDemanda.Visible = True
		Me.txtSubtipusDemanda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSubtipusDemanda.Name = "txtSubtipusDemanda"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(414, 19)
		Me.Text5.Location = New System.Drawing.Point(169, 106)
		Me.Text5.TabIndex = 12
		Me.Text5.Tag = "^5"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtCentreQueGestiona.AutoSize = False
		Me.txtCentreQueGestiona.Size = New System.Drawing.Size(42, 19)
		Me.txtCentreQueGestiona.Location = New System.Drawing.Point(124, 130)
		Me.txtCentreQueGestiona.Maxlength = 4
		Me.txtCentreQueGestiona.TabIndex = 14
		Me.txtCentreQueGestiona.Tag = "6"
		Me.txtCentreQueGestiona.AcceptsReturn = True
		Me.txtCentreQueGestiona.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCentreQueGestiona.BackColor = System.Drawing.SystemColors.Window
		Me.txtCentreQueGestiona.CausesValidation = True
		Me.txtCentreQueGestiona.Enabled = True
		Me.txtCentreQueGestiona.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCentreQueGestiona.HideSelection = True
		Me.txtCentreQueGestiona.ReadOnly = False
		Me.txtCentreQueGestiona.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCentreQueGestiona.MultiLine = False
		Me.txtCentreQueGestiona.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCentreQueGestiona.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCentreQueGestiona.TabStop = True
		Me.txtCentreQueGestiona.Visible = True
		Me.txtCentreQueGestiona.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCentreQueGestiona.Name = "txtCentreQueGestiona"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(414, 19)
		Me.Text6.Location = New System.Drawing.Point(169, 130)
		Me.Text6.TabIndex = 15
		Me.Text6.Tag = "^6"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtPersonaQueGestiona.AutoSize = False
		Me.txtPersonaQueGestiona.Size = New System.Drawing.Size(93, 19)
		Me.txtPersonaQueGestiona.Location = New System.Drawing.Point(124, 154)
		Me.txtPersonaQueGestiona.Maxlength = 9
		Me.txtPersonaQueGestiona.TabIndex = 17
		Me.txtPersonaQueGestiona.Tag = "7"
		Me.txtPersonaQueGestiona.AcceptsReturn = True
		Me.txtPersonaQueGestiona.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPersonaQueGestiona.BackColor = System.Drawing.SystemColors.Window
		Me.txtPersonaQueGestiona.CausesValidation = True
		Me.txtPersonaQueGestiona.Enabled = True
		Me.txtPersonaQueGestiona.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPersonaQueGestiona.HideSelection = True
		Me.txtPersonaQueGestiona.ReadOnly = False
		Me.txtPersonaQueGestiona.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPersonaQueGestiona.MultiLine = False
		Me.txtPersonaQueGestiona.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPersonaQueGestiona.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPersonaQueGestiona.TabStop = True
		Me.txtPersonaQueGestiona.Visible = True
		Me.txtPersonaQueGestiona.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPersonaQueGestiona.Name = "txtPersonaQueGestiona"
		Me.Text7.AutoSize = False
		Me.Text7.BackColor = System.Drawing.Color.White
		Me.Text7.Enabled = False
		Me.Text7.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text7.Size = New System.Drawing.Size(362, 19)
		Me.Text7.Location = New System.Drawing.Point(221, 154)
		Me.Text7.TabIndex = 18
		Me.Text7.Tag = "^7"
		Me.Text7.AcceptsReturn = True
		Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text7.CausesValidation = True
		Me.Text7.HideSelection = True
		Me.Text7.ReadOnly = False
		Me.Text7.Maxlength = 0
		Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text7.MultiLine = False
		Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text7.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text7.TabStop = True
		Me.Text7.Visible = True
		Me.Text7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text7.Name = "Text7"
		Me.txtProblema.AutoSize = False
		Me.txtProblema.Size = New System.Drawing.Size(459, 179)
		Me.txtProblema.Location = New System.Drawing.Point(124, 178)
		Me.txtProblema.Maxlength = 250
		Me.txtProblema.MultiLine = True
		Me.txtProblema.TabIndex = 20
		Me.txtProblema.Tag = "8"
		Me.txtProblema.AcceptsReturn = True
		Me.txtProblema.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtProblema.BackColor = System.Drawing.SystemColors.Window
		Me.txtProblema.CausesValidation = True
		Me.txtProblema.Enabled = True
		Me.txtProblema.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtProblema.HideSelection = True
		Me.txtProblema.ReadOnly = False
		Me.txtProblema.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtProblema.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtProblema.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtProblema.TabStop = True
		Me.txtProblema.Visible = True
		Me.txtProblema.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtProblema.Name = "txtProblema"
		cmbEstatDemanda.OcxState = CType(resources.GetObject("cmbEstatDemanda.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbEstatDemanda.Size = New System.Drawing.Size(145, 19)
		Me.cmbEstatDemanda.Location = New System.Drawing.Point(438, 10)
		Me.cmbEstatDemanda.TabIndex = 22
		Me.cmbEstatDemanda.Name = "cmbEstatDemanda"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(411, 408)
		Me.cmdAceptar.TabIndex = 24
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(501, 408)
		Me.cmdGuardar.TabIndex = 25
		Me.cmdGuardar.Name = "cmdGuardar"
		txtDataTancament.OcxState = CType(resources.GetObject("txtDataTancament.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataTancament.Size = New System.Drawing.Size(87, 19)
		Me.txtDataTancament.Location = New System.Drawing.Point(124, 362)
		Me.txtDataTancament.TabIndex = 23
		Me.txtDataTancament.Name = "txtDataTancament"
		txtDatalimit.OcxState = CType(resources.GetObject("txtDatalimit.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDatalimit.Size = New System.Drawing.Size(87, 19)
		Me.txtDatalimit.Location = New System.Drawing.Point(498, 58)
		Me.txtDatalimit.TabIndex = 6
		Me.txtDatalimit.Name = "txtDatalimit"
		Me.Label4.Text = "Data"
		Me.Label4.Size = New System.Drawing.Size(111, 15)
		Me.Label4.Location = New System.Drawing.Point(384, 62)
		Me.Label4.TabIndex = 33
		Me.Label4.Tag = "13"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label3.Text = "Usuari EntiGest Gestio que gestiona"
		Me.Label3.Size = New System.Drawing.Size(111, 15)
		Me.Label3.Location = New System.Drawing.Point(192, 366)
		Me.Label3.TabIndex = 32
		Me.Label3.Tag = "12"
		Me.Label3.Visible = False
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Codi demanda origen"
		Me.Label2.Size = New System.Drawing.Size(111, 15)
		Me.Label2.Location = New System.Drawing.Point(408, 366)
		Me.Label2.TabIndex = 30
		Me.Label2.Tag = "11"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Data tancament"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 366)
		Me.Label1.TabIndex = 28
		Me.Label1.Tag = "10"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Codi"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Demandant"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Data"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 4
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Tipus demanda"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 86)
		Me.lbl4.TabIndex = 7
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Subtipus demanda"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 110)
		Me.lbl5.TabIndex = 10
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Centre que gestiona"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 134)
		Me.lbl6.TabIndex = 13
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "Persona que gestiona"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 158)
		Me.lbl7.TabIndex = 16
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl8.Text = "Problema"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(10, 182)
		Me.lbl8.TabIndex = 19
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl9.Text = "Estat demanda"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(324, 14)
		Me.lbl9.TabIndex = 21
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 407)
		Me.lblLock.TabIndex = 27
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 401
		Me.Line1.Y2 = 401
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 402
		Me.Line2.Y2 = 402
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.txtDatalimit, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataTancament, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbEstatDemanda, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtUsuariEntigestGestio)
		Me.Controls.Add(txtCodiDemandaOrigen)
		Me.Controls.Add(txtCodi)
		Me.Controls.Add(txtDemandant)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtData)
		Me.Controls.Add(txtTipusDemanda)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtSubtipusDemanda)
		Me.Controls.Add(Text5)
		Me.Controls.Add(txtCentreQueGestiona)
		Me.Controls.Add(Text6)
		Me.Controls.Add(txtPersonaQueGestiona)
		Me.Controls.Add(Text7)
		Me.Controls.Add(txtProblema)
		Me.Controls.Add(cmbEstatDemanda)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(txtDataTancament)
		Me.Controls.Add(txtDatalimit)
		Me.Controls.Add(Label4)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class