Option Strict Off
Option Explicit On
Module AmpansGestio1
	
	'
	'
	'
	Public Sub MouseUpBotoNou(ByRef FRM As FormParent, ByRef CodiEntitat As String)
		
		' Nota: El Parametre CodiEntitat no es fa servir pro es carrega per guardar la compatibilitat amb altres crides sense men� al boto
		Dim TeNomina As Short
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("BotoNouTercer", PopMenuStyle.tsPrimaryMenu, True)
				'TeNomina = CacheXecute("S VALUE=$P(^[""ENTI""]PARA(""RH"",1,EMP),S,14)")
				'If TeNomina <> 1 Then .MenuItems.Add tsMenuCaption, "Personal", , , XPIcon("User"), , , , , "PEI"
				
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Personal inserci�n",  ,  , XPIcon("User2"),  ,  ,  ,  , "PEI")
			End With
		End With
		XpExecutaMenu(FRM, "BotoNouTercer")
	End Sub
	
	Public Sub ResMenuBotoNou(ByRef CodiEntitat As String, ByRef KeyMenu As String)
		
		Select Case KeyMenu
			Case "PEI"
				CreaPersonalInsercion(CodiEntitat, True)
		End Select
		
	End Sub
	
	Public Sub CreaPersonalInsercion(ByRef CodiEntitat As String, ByRef DesdeEntitat As Boolean)
		
		Dim RegEnti As String
		Dim CrearTercer As Boolean
		Dim Nif As String
		Dim Resposta As String
		
		If CodiEntitat = "" Then Exit Sub
		
		If PermisConsulta("frmUsuariInsercioLaboral", e_Permisos.altes) = False Then
			xMsgBox("Alta de personal inserci�n no autorizada", MsgBoxStyle.Critical, "Alta de personal inserci�n desde entidades")
			Exit Sub
		End If
		
		If DesdeEntitat = True Then
			If IsLoad("frmUsuariInsercioLaboral") = True Then
				If frmUsuariInsercioLaboral.txtCodigo.Text <> "" Then
					xMsgBox("Formulario de personal inserci�n ocupado", MsgBoxStyle.Critical, frmUsuariInsercioLaboral.Text)
					frmUsuariInsercioLaboral.Activate()
					Exit Sub
				End If
			End If
			
			If AltesDesdeEntitat(CodiEntitat, True,  , "1,2,4,5,34,35") = False Then Exit Sub
			frmUsuariInsercioLaboral.Unload()
		End If
		If AltesDesdeEntitat(CodiEntitat, True, False, "1,2,4,5,34,35") = False Then Exit Sub
		CacheNetejaParametres()
		MCache.P1 = CodiEntitat
		RegEnti = CacheXecute("S VALUE=^[""ENTI""]GENT(P1)")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Nif = Piece(RegEnti, S, 14)
		If Nif <> "" Then
			CacheNetejaParametres()
			MCache.P1 = Nif
			Resposta = CacheXecute("S VALUE=$$VNifDupPERINS^ENT.G.ENTIAMP(P1)")
			If Resposta <> "" Then
				If xMsgBox("Se ha detectado el c�digo de personal inserci�n { " & Resposta & " }con el mismo NIF que el de la ficha de entidad.#Desea abrir su ficha ?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Information, "Alta de personal desde entidades") <> MsgBoxResult.Yes Then Exit Sub
				If DesdeEntitat = True Then
					ObreFormulari(frmUsuariInsercioLaboral, "frmUsuariInsercioLaboral", Resposta)
				Else
					ResetForm(frmUsuariInsercioLaboral)
					With frmUsuariInsercioLaboral
						.txtCodigo.Text = Resposta
						.ABM = GetReg(frmUsuariInsercioLaboral)
					End With
				End If
				Exit Sub
			End If
		End If
		
		If DesdeEntitat = True Then
			DisableForm(frmEntitats) 'perque no puguin tocar res mentre es dona d'alta el nou tercer
			frmUsuariInsercioLaboral.Unload()
		End If
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'GetCodiAuto frmClientsGarden   ' aix� dona un codi de clients al txtCodigo
		MCache.P1 = frmUsuariInsercioLaboral.txtCodigo
		MCache.P2 = CodiEntitat
		
		frmUsuariInsercioLaboral.Reg = CacheXecute("D RPERINS^ENT.G.ENTIAMP")
		frmUsuariInsercioLaboral.FlagExtern = True
		VisReg(frmUsuariInsercioLaboral)
		
		' desactivem els camps que no es poden tocar ja que nom�s es poden canviar des-de la entitat
		
		DisableCampsEntiPersonalInsercio()
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	Public Sub DisableCampsEntiPersonalInsercio()
		
		With frmUsuariInsercioLaboral
			.txtCodigo.Enabled = False
			.txtCodigoEntidad.Enabled = False
			.txtNombre.Enabled = False
			.txtPrimerApellido.Enabled = False
			.txtSegundoApellido.Enabled = False
			.txtDireccion.Enabled = False
			.txtPoblacion.Enabled = False
			.txtCodigoPostal.Enabled = False
			.txtTelefono.Enabled = False
			.txtTelefonoMovil.Enabled = False
			.cmbSexo.Enabled = False
			.txtNIF.Enabled = False
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			.txtFechaNacimiento.Enabled = False
			.txtEmail.Enabled = False
			'.Text2.Enabled = False
			'.Text5.Enabled = False
			.Show()
		End With
		
	End Sub
	
	'Public Sub IniciFormEntitats(FRM As Form)
	'
	'    FRM.cmdNouTercer.Caption = "Nuevo"
	'    FRM.DirectCast(cmdNouTercer.FindForm, FormParent).ToolTip1.SetToolTip(cmdNouTercer, "Crear una nueva ficha a partir del c�digo de entidad")
	'    'Si hi ha varies opcions posar estil=3
	'    FRM.cmdNouTercer.Style = 3
	'
	'End Sub
	
	'Public Function ControlFormsTercers(FRM As Form) As Boolean
	
	'    Dim A As String
	'    ControlFormsTercers = True
	'    If FRM.ABM = "AL" Then Exit Function
	'    If IsLoad("frmUsuariInsercioLaboral") = True Then
	'        If frmUsuariInsercioLaboral.txtCodigoEntidad = FRM.txtCodigoEntidad Then
	'            xMsgBox "Debe iniciar o descargar previamente el formulario de personas de inserci�n laboral#para efectuar la grabaci�n del registro", vbCritical, FRM.Caption
	'            ControlFormsTercers = False
	'            Exit Function
	'        End If
	'    End If
	'End Function
	
	Public Sub ObrirVincleEntitat1(ByRef Keys As String, ByRef Entitat As String)
		'    Dim Registre As String
		'
		'    If ComprovaEmpresaEntitats(Keys) = False Then Exit Sub
		'
		'    Select Case Piece(Keys, "|", 1)
		'        Case "GEN-TER"
		'            ObreFormTercersEntitat Piece(Keys, "|", 3)
		'        Case "ENT-PEX"
		'            ObreFormulari frmPersonalExtern, "frmPersonalExtern", Piece(Keys, "|", 3)
		'        Case "ENT-PER"
		'            If EMP <> Piece(Keys, "|", 2) Then
		'                xMsgBox "No es posible abrir la ficha del v�nculo seleccionado desde la empresa actual", vbCritical, "Entidades"
		'                Exit Sub
		'            End If
		'            ObreFormulari frmPersonal, "frmPersonal", Piece(Keys, "|", 3)
		'        Case "ENT-DOC"
		'             If EMP <> Piece(Keys, "|", 2) Then
		'                xMsgBox "No es posible abrir la ficha del v�nculo seleccionado desde la empresa actual", vbCritical, "Entidades"
		'                Exit Sub
		'            End If
		'            ObreFormulari frmDocentsExternos, "frmDocentsExternos", Piece(Keys, "|", 3)
		'        Case "ENT-VOL"
		'             If EMP <> Piece(Keys, "|", 2) Then
		'                xMsgBox "No es posible abrir la ficha del v�nculo seleccionado desde la empresa actual", vbCritical, "Entidades"
		'                Exit Sub
		'            End If
		'            ObreFormulari frmVoluntaris, "frmVoluntaris", Piece(Keys, "|", 3)
		'        Case "ENT-SVO"
		'             If EMP <> Piece(Keys, "|", 2) Then
		'                xMsgBox "No es posible abrir la ficha del v�nculo seleccionado desde la empresa actual", vbCritical, "Entidades"
		'                Exit Sub
		'            End If
		'            Registre = ""
		'            Registre = IPiece(Registre, S, 126, Entitat)
		'            ObreFormulari frmSol�licitutVoluntaris, "frmSol�licitutVoluntaris", Registre
		'            ConsultaGlobal frmSol�licitutVoluntaris
		'        Case "ENT-PRA"
		'             If EMP <> Piece(Keys, "|", 2) Then
		'                xMsgBox "No es posible abrir la ficha del v�nculo seleccionado desde la empresa actual", vbCritical, "Entidades"
		'                Exit Sub
		'            End If
		'            ObreFormulari frmPractiques, "frmPractiques", Piece(Keys, "|", 3)
		'        Case "ENT-SPR"
		'             If EMP <> Piece(Keys, "|", 2) Then
		'                xMsgBox "No es posible abrir la ficha del v�nculo seleccionado desde la empresa actual", vbCritical, "Entidades"
		'                Exit Sub
		'            End If
		'            Registre = ""
		'            Registre = IPiece(Registre, S, 126, Entitat)
		'            ObreFormulari frmSol�licitutPractiques, "frmSol�licitutPractiques", Registre
		'            ConsultaGlobal frmSol�licitutPractiques
		'        Case "ENT-CAN"
		'             If EMP <> Piece(Keys, "|", 2) Then
		'                xMsgBox "No es posible abrir la ficha del v�nculo seleccionado desde la empresa actual", vbCritical, "Entidades"
		'                Exit Sub
		'            End If
		'            Registre = ""
		'            Registre = IPiece(Registre, S, 126, Entitat)
		'            ObreFormulari frmCurriculums, "frmCurriculums", Registre
		'            ConsultaGlobal frmCurriculums
		'        Case "ENT-ETT"
		'             If EMP <> Piece(Keys, "|", 2) Then
		'                xMsgBox "No es posible abrir la ficha del v�nculo seleccionado desde la empresa actual", vbCritical, "Entidades"
		'                Exit Sub
		'            End If
		'            ObreFormulari frmEmpresasDeTrabajo, "frmEmpresasDeTrabajo", Piece(Keys, "|", 3)
		'        Case Else
		'            xMsgBox "No es posible abrir la ficha del v�nculo seleccionado desde el m�dulo de recursos humanos", vbCritical, "Entidades"
		'            Exit Sub
		'    End Select
	End Sub
End Module
