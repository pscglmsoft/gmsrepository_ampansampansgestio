'UPGRADE_WARNING: El proyecto entero se debe compilar una vez antes de poder mostrar una matriz de controles ActiveX

Imports System.ComponentModel

<ProvideProperty("Index",GetType(AxSSDataWidgets_B.AxSSDBCombo))> Public Class AxSSDBComboArray
	Inherits Microsoft.VisualBasic.Compatibility.VB6.BaseOcxArray
	Implements IExtenderProvider

	Public Sub New()
		MyBase.New()
	End Sub

	Public Sub New(ByVal Container As IContainer)
		MyBase.New(Container)
	End Sub

	Public Shadows Event [InitColumnProps] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [ClickEvent] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [DropDown] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [CloseUp] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [Change] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [DblClick] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [KeyDownEvent] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_KeyDownEvent)
	Public Shadows Event [KeyPressEvent] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_KeyPressEvent)
	Public Shadows Event [KeyUpEvent] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_KeyUpEvent)
	Public Shadows Event [MouseDownEvent] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_MouseDownEvent)
	Public Shadows Event [MouseMoveEvent] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_MouseMoveEvent)
	Public Shadows Event [MouseUpEvent] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_MouseUpEvent)
	Public Shadows Event [ScrollEvent] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_ScrollEvent)
	Public Shadows Event [PositionList] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_PositionListEvent)
	Public Shadows Event [TextError] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_TextErrorEvent)
	Public Shadows Event [ValidateList] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_ValidateListEvent)
	Public Shadows Event [RowLoaded] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_RowLoadedEvent)
	Public Shadows Event [UnboundReadData] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_UnboundReadDataEvent)
	Public Shadows Event [UnboundPositionData] (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_UnboundPositionDataEvent)
	Public Shadows Event [ScrollAfter] (ByVal sender As System.Object, ByVal e As System.EventArgs)

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Function CanExtend(ByVal target As Object) As Boolean Implements IExtenderProvider.CanExtend
		If TypeOf target Is AxSSDataWidgets_B.AxSSDBCombo Then
			Return BaseCanExtend(target)
		End If
	End Function

	Public Function GetIndex(ByVal o As AxSSDataWidgets_B.AxSSDBCombo) As Short
		Return BaseGetIndex(o)
	End Function

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Sub SetIndex(ByVal o As AxSSDataWidgets_B.AxSSDBCombo, ByVal Index As Short)
		BaseSetIndex(o, Index)
	End Sub

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Function ShouldSerializeIndex(ByVal o As AxSSDataWidgets_B.AxSSDBCombo) As Boolean
		Return BaseShouldSerializeIndex(o)
	End Function

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Sub ResetIndex(ByVal o As AxSSDataWidgets_B.AxSSDBCombo)
		BaseResetIndex(o)
	End Sub

	Default Public ReadOnly Property Item(ByVal Index As Short) As AxSSDataWidgets_B.AxSSDBCombo
		Get
			Item = CType(BaseGetItem(Index), AxSSDataWidgets_B.AxSSDBCombo)
		End Get
	End Property

	Protected Overrides Function GetControlInstanceType() As System.Type
		Return GetType(AxSSDataWidgets_B.AxSSDBCombo)
	End Function

	Protected Overrides Sub HookUpControlEvents(ByVal o As Object)
		Dim ctl As AxSSDataWidgets_B.AxSSDBCombo = CType(o, AxSSDataWidgets_B.AxSSDBCombo)
		MyBase.HookUpControlEvents(o)
		If Not InitColumnPropsEvent Is Nothing Then
			AddHandler ctl.InitColumnProps, New System.EventHandler(AddressOf HandleInitColumnProps)
		End If
		If Not ClickEventEvent Is Nothing Then
			AddHandler ctl.ClickEvent, New System.EventHandler(AddressOf HandleClickEvent)
		End If
		If Not DropDownEvent Is Nothing Then
			AddHandler ctl.DropDown, New System.EventHandler(AddressOf HandleDropDown)
		End If
		If Not CloseUpEvent Is Nothing Then
			AddHandler ctl.CloseUp, New System.EventHandler(AddressOf HandleCloseUp)
		End If
		If Not ChangeEvent Is Nothing Then
			AddHandler ctl.Change, New System.EventHandler(AddressOf HandleChange)
		End If
		If Not DblClickEvent Is Nothing Then
			AddHandler ctl.DblClick, New System.EventHandler(AddressOf HandleDblClick)
		End If
		If Not KeyDownEventEvent Is Nothing Then
			AddHandler ctl.KeyDownEvent, New AxSSDataWidgets_B._DSSDBComboEvents_KeyDownEventHandler(AddressOf HandleKeyDownEvent)
		End If
		If Not KeyPressEventEvent Is Nothing Then
			AddHandler ctl.KeyPressEvent, New AxSSDataWidgets_B._DSSDBComboEvents_KeyPressEventHandler(AddressOf HandleKeyPressEvent)
		End If
		If Not KeyUpEventEvent Is Nothing Then
			AddHandler ctl.KeyUpEvent, New AxSSDataWidgets_B._DSSDBComboEvents_KeyUpEventHandler(AddressOf HandleKeyUpEvent)
		End If
		If Not MouseDownEventEvent Is Nothing Then
			AddHandler ctl.MouseDownEvent, New AxSSDataWidgets_B._DSSDBComboEvents_MouseDownEventHandler(AddressOf HandleMouseDownEvent)
		End If
		If Not MouseMoveEventEvent Is Nothing Then
			AddHandler ctl.MouseMoveEvent, New AxSSDataWidgets_B._DSSDBComboEvents_MouseMoveEventHandler(AddressOf HandleMouseMoveEvent)
		End If
		If Not MouseUpEventEvent Is Nothing Then
			AddHandler ctl.MouseUpEvent, New AxSSDataWidgets_B._DSSDBComboEvents_MouseUpEventHandler(AddressOf HandleMouseUpEvent)
		End If
		If Not ScrollEventEvent Is Nothing Then
			AddHandler ctl.ScrollEvent, New AxSSDataWidgets_B._DSSDBComboEvents_ScrollEventHandler(AddressOf HandleScrollEvent)
		End If
		If Not PositionListEvent Is Nothing Then
			AddHandler ctl.PositionList, New AxSSDataWidgets_B._DSSDBComboEvents_PositionListEventHandler(AddressOf HandlePositionList)
		End If
		If Not TextErrorEvent Is Nothing Then
			AddHandler ctl.TextError, New AxSSDataWidgets_B._DSSDBComboEvents_TextErrorEventHandler(AddressOf HandleTextError)
		End If
		If Not ValidateListEvent Is Nothing Then
			AddHandler ctl.ValidateList, New AxSSDataWidgets_B._DSSDBComboEvents_ValidateListEventHandler(AddressOf HandleValidateList)
		End If
		If Not RowLoadedEvent Is Nothing Then
			AddHandler ctl.RowLoaded, New AxSSDataWidgets_B._DSSDBComboEvents_RowLoadedEventHandler(AddressOf HandleRowLoaded)
		End If
		If Not UnboundReadDataEvent Is Nothing Then
			AddHandler ctl.UnboundReadData, New AxSSDataWidgets_B._DSSDBComboEvents_UnboundReadDataEventHandler(AddressOf HandleUnboundReadData)
		End If
		If Not UnboundPositionDataEvent Is Nothing Then
			AddHandler ctl.UnboundPositionData, New AxSSDataWidgets_B._DSSDBComboEvents_UnboundPositionDataEventHandler(AddressOf HandleUnboundPositionData)
		End If
		If Not ScrollAfterEvent Is Nothing Then
			AddHandler ctl.ScrollAfter, New System.EventHandler(AddressOf HandleScrollAfter)
		End If
	End Sub

	Private Sub HandleInitColumnProps (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [InitColumnProps] (sender, e)
	End Sub

	Private Sub HandleClickEvent (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [ClickEvent] (sender, e)
	End Sub

	Private Sub HandleDropDown (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [DropDown] (sender, e)
	End Sub

	Private Sub HandleCloseUp (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [CloseUp] (sender, e)
	End Sub

	Private Sub HandleChange (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [Change] (sender, e)
	End Sub

	Private Sub HandleDblClick (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [DblClick] (sender, e)
	End Sub

	Private Sub HandleKeyDownEvent (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_KeyDownEvent) 
		RaiseEvent [KeyDownEvent] (sender, e)
	End Sub

	Private Sub HandleKeyPressEvent (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_KeyPressEvent) 
		RaiseEvent [KeyPressEvent] (sender, e)
	End Sub

	Private Sub HandleKeyUpEvent (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_KeyUpEvent) 
		RaiseEvent [KeyUpEvent] (sender, e)
	End Sub

	Private Sub HandleMouseDownEvent (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_MouseDownEvent) 
		RaiseEvent [MouseDownEvent] (sender, e)
	End Sub

	Private Sub HandleMouseMoveEvent (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_MouseMoveEvent) 
		RaiseEvent [MouseMoveEvent] (sender, e)
	End Sub

	Private Sub HandleMouseUpEvent (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_MouseUpEvent) 
		RaiseEvent [MouseUpEvent] (sender, e)
	End Sub

	Private Sub HandleScrollEvent (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_ScrollEvent) 
		RaiseEvent [ScrollEvent] (sender, e)
	End Sub

	Private Sub HandlePositionList (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_PositionListEvent) 
		RaiseEvent [PositionList] (sender, e)
	End Sub

	Private Sub HandleTextError (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_TextErrorEvent) 
		RaiseEvent [TextError] (sender, e)
	End Sub

	Private Sub HandleValidateList (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_ValidateListEvent) 
		RaiseEvent [ValidateList] (sender, e)
	End Sub

	Private Sub HandleRowLoaded (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_RowLoadedEvent) 
		RaiseEvent [RowLoaded] (sender, e)
	End Sub

	Private Sub HandleUnboundReadData (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_UnboundReadDataEvent) 
		RaiseEvent [UnboundReadData] (sender, e)
	End Sub

	Private Sub HandleUnboundPositionData (ByVal sender As System.Object, ByVal e As AxSSDataWidgets_B._DSSDBComboEvents_UnboundPositionDataEvent) 
		RaiseEvent [UnboundPositionData] (sender, e)
	End Sub

	Private Sub HandleScrollAfter (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [ScrollAfter] (sender, e)
	End Sub

End Class

