'UPGRADE_WARNING: El proyecto entero se debe compilar una vez antes de poder mostrar una matriz de controles ActiveX

Imports System.ComponentModel

<ProvideProperty("Index",GetType(AxDataControl.AxGmsImports))> Public Class AxGmsImportsArray
	Inherits Microsoft.VisualBasic.Compatibility.VB6.BaseOcxArray
	Implements IExtenderProvider

	Public Sub New()
		MyBase.New()
	End Sub

	Public Sub New(ByVal Container As IContainer)
		MyBase.New(Container)
	End Sub

	Public Shadows Event [ResizeEvent] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [ClickEvent] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [DblClick] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [KeyDownEvent] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_KeyDownEvent)
	Public Shadows Event [KeyPressEvent] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_KeyPressEvent)
	Public Shadows Event [KeyUpEvent] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_KeyUpEvent)
	Public Shadows Event [MouseDownEvent] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_MouseDownEvent)
	Public Shadows Event [MouseMoveEvent] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_MouseMoveEvent)
	Public Shadows Event [MouseUpEvent] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_MouseUpEvent)
	Public Shadows Event [WriteProperties] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_WritePropertiesEvent)
	Public Shadows Event [ReadProperties] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_ReadPropertiesEvent)
	Public Shadows Event [InitProperties] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [AsyncReadComplete] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_AsyncReadCompleteEvent)
	Public Shadows Event [AsyncReadProgress] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_AsyncReadProgressEvent)
	Public Shadows Event [Validate] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_ValidateEvent)
	Public Shadows Event [ShowEvent] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [PaintEvent] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [OLEStartDrag] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLEStartDragEvent)
	Public Shadows Event [OLESetData] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLESetDataEvent)
	Public Shadows Event [OLEGiveFeedback] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLEGiveFeedbackEvent)
	Public Shadows Event [OLEDragOver] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLEDragOverEvent)
	Public Shadows Event [OLEDragDrop] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLEDragDropEvent)
	Public Shadows Event [OLECompleteDrag] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLECompleteDragEvent)
	Public Shadows Event [HitTest] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_HitTestEvent)
	Public Shadows Event [HideEvent] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [GetDataMember] (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_GetDataMemberEvent)

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Function CanExtend(ByVal target As Object) As Boolean Implements IExtenderProvider.CanExtend
		If TypeOf target Is AxDataControl.AxGmsImports Then
			Return BaseCanExtend(target)
		End If
	End Function

	Public Function GetIndex(ByVal o As AxDataControl.AxGmsImports) As Short
		Return BaseGetIndex(o)
	End Function

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Sub SetIndex(ByVal o As AxDataControl.AxGmsImports, ByVal Index As Short)
		BaseSetIndex(o, Index)
	End Sub

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Function ShouldSerializeIndex(ByVal o As AxDataControl.AxGmsImports) As Boolean
		Return BaseShouldSerializeIndex(o)
	End Function

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Sub ResetIndex(ByVal o As AxDataControl.AxGmsImports)
		BaseResetIndex(o)
	End Sub

	Default Public ReadOnly Property Item(ByVal Index As Short) As AxDataControl.AxGmsImports
		Get
			Item = CType(BaseGetItem(Index), AxDataControl.AxGmsImports)
		End Get
	End Property

	Protected Overrides Function GetControlInstanceType() As System.Type
		Return GetType(AxDataControl.AxGmsImports)
	End Function

	Protected Overrides Sub HookUpControlEvents(ByVal o As Object)
		Dim ctl As AxDataControl.AxGmsImports = CType(o, AxDataControl.AxGmsImports)
		MyBase.HookUpControlEvents(o)
		If Not ResizeEventEvent Is Nothing Then
			AddHandler ctl.ResizeEvent, New System.EventHandler(AddressOf HandleResizeEvent)
		End If
		If Not ClickEventEvent Is Nothing Then
			AddHandler ctl.ClickEvent, New System.EventHandler(AddressOf HandleClickEvent)
		End If
		If Not DblClickEvent Is Nothing Then
			AddHandler ctl.DblClick, New System.EventHandler(AddressOf HandleDblClick)
		End If
		If Not KeyDownEventEvent Is Nothing Then
			AddHandler ctl.KeyDownEvent, New AxDataControl.__GmsImports_KeyDownEventHandler(AddressOf HandleKeyDownEvent)
		End If
		If Not KeyPressEventEvent Is Nothing Then
			AddHandler ctl.KeyPressEvent, New AxDataControl.__GmsImports_KeyPressEventHandler(AddressOf HandleKeyPressEvent)
		End If
		If Not KeyUpEventEvent Is Nothing Then
			AddHandler ctl.KeyUpEvent, New AxDataControl.__GmsImports_KeyUpEventHandler(AddressOf HandleKeyUpEvent)
		End If
		If Not MouseDownEventEvent Is Nothing Then
			AddHandler ctl.MouseDownEvent, New AxDataControl.__GmsImports_MouseDownEventHandler(AddressOf HandleMouseDownEvent)
		End If
		If Not MouseMoveEventEvent Is Nothing Then
			AddHandler ctl.MouseMoveEvent, New AxDataControl.__GmsImports_MouseMoveEventHandler(AddressOf HandleMouseMoveEvent)
		End If
		If Not MouseUpEventEvent Is Nothing Then
			AddHandler ctl.MouseUpEvent, New AxDataControl.__GmsImports_MouseUpEventHandler(AddressOf HandleMouseUpEvent)
		End If
		If Not WritePropertiesEvent Is Nothing Then
			AddHandler ctl.WriteProperties, New AxDataControl.__GmsImports_WritePropertiesEventHandler(AddressOf HandleWriteProperties)
		End If
		If Not ReadPropertiesEvent Is Nothing Then
			AddHandler ctl.ReadProperties, New AxDataControl.__GmsImports_ReadPropertiesEventHandler(AddressOf HandleReadProperties)
		End If
		If Not InitPropertiesEvent Is Nothing Then
			AddHandler ctl.InitProperties, New System.EventHandler(AddressOf HandleInitProperties)
		End If
		If Not AsyncReadCompleteEvent Is Nothing Then
			AddHandler ctl.AsyncReadComplete, New AxDataControl.__GmsImports_AsyncReadCompleteEventHandler(AddressOf HandleAsyncReadComplete)
		End If
		If Not AsyncReadProgressEvent Is Nothing Then
			AddHandler ctl.AsyncReadProgress, New AxDataControl.__GmsImports_AsyncReadProgressEventHandler(AddressOf HandleAsyncReadProgress)
		End If
		If Not ValidateEvent Is Nothing Then
			AddHandler ctl.Validate, New AxDataControl.__GmsImports_ValidateEventHandler(AddressOf HandleValidate)
		End If
		If Not ShowEventEvent Is Nothing Then
			AddHandler ctl.ShowEvent, New System.EventHandler(AddressOf HandleShowEvent)
		End If
		If Not PaintEventEvent Is Nothing Then
			AddHandler ctl.PaintEvent, New System.EventHandler(AddressOf HandlePaintEvent)
		End If
		If Not OLEStartDragEvent Is Nothing Then
			AddHandler ctl.OLEStartDrag, New AxDataControl.__GmsImports_OLEStartDragEventHandler(AddressOf HandleOLEStartDrag)
		End If
		If Not OLESetDataEvent Is Nothing Then
			AddHandler ctl.OLESetData, New AxDataControl.__GmsImports_OLESetDataEventHandler(AddressOf HandleOLESetData)
		End If
		If Not OLEGiveFeedbackEvent Is Nothing Then
			AddHandler ctl.OLEGiveFeedback, New AxDataControl.__GmsImports_OLEGiveFeedbackEventHandler(AddressOf HandleOLEGiveFeedback)
		End If
		If Not OLEDragOverEvent Is Nothing Then
			AddHandler ctl.OLEDragOver, New AxDataControl.__GmsImports_OLEDragOverEventHandler(AddressOf HandleOLEDragOver)
		End If
		If Not OLEDragDropEvent Is Nothing Then
			AddHandler ctl.OLEDragDrop, New AxDataControl.__GmsImports_OLEDragDropEventHandler(AddressOf HandleOLEDragDrop)
		End If
		If Not OLECompleteDragEvent Is Nothing Then
			AddHandler ctl.OLECompleteDrag, New AxDataControl.__GmsImports_OLECompleteDragEventHandler(AddressOf HandleOLECompleteDrag)
		End If
		If Not HitTestEvent Is Nothing Then
			AddHandler ctl.HitTest, New AxDataControl.__GmsImports_HitTestEventHandler(AddressOf HandleHitTest)
		End If
		If Not HideEventEvent Is Nothing Then
			AddHandler ctl.HideEvent, New System.EventHandler(AddressOf HandleHideEvent)
		End If
		If Not GetDataMemberEvent Is Nothing Then
			AddHandler ctl.GetDataMember, New AxDataControl.__GmsImports_GetDataMemberEventHandler(AddressOf HandleGetDataMember)
		End If
	End Sub

	Private Sub HandleResizeEvent (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [ResizeEvent] (sender, e)
	End Sub

	Private Sub HandleClickEvent (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [ClickEvent] (sender, e)
	End Sub

	Private Sub HandleDblClick (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [DblClick] (sender, e)
	End Sub

	Private Sub HandleKeyDownEvent (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_KeyDownEvent) 
		RaiseEvent [KeyDownEvent] (sender, e)
	End Sub

	Private Sub HandleKeyPressEvent (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_KeyPressEvent) 
		RaiseEvent [KeyPressEvent] (sender, e)
	End Sub

	Private Sub HandleKeyUpEvent (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_KeyUpEvent) 
		RaiseEvent [KeyUpEvent] (sender, e)
	End Sub

	Private Sub HandleMouseDownEvent (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_MouseDownEvent) 
		RaiseEvent [MouseDownEvent] (sender, e)
	End Sub

	Private Sub HandleMouseMoveEvent (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_MouseMoveEvent) 
		RaiseEvent [MouseMoveEvent] (sender, e)
	End Sub

	Private Sub HandleMouseUpEvent (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_MouseUpEvent) 
		RaiseEvent [MouseUpEvent] (sender, e)
	End Sub

	Private Sub HandleWriteProperties (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_WritePropertiesEvent) 
		RaiseEvent [WriteProperties] (sender, e)
	End Sub

	Private Sub HandleReadProperties (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_ReadPropertiesEvent) 
		RaiseEvent [ReadProperties] (sender, e)
	End Sub

	Private Sub HandleInitProperties (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [InitProperties] (sender, e)
	End Sub

	Private Sub HandleAsyncReadComplete (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_AsyncReadCompleteEvent) 
		RaiseEvent [AsyncReadComplete] (sender, e)
	End Sub

	Private Sub HandleAsyncReadProgress (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_AsyncReadProgressEvent) 
		RaiseEvent [AsyncReadProgress] (sender, e)
	End Sub

	Private Sub HandleValidate (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_ValidateEvent) 
		RaiseEvent [Validate] (sender, e)
	End Sub

	Private Sub HandleShowEvent (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [ShowEvent] (sender, e)
	End Sub

	Private Sub HandlePaintEvent (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [PaintEvent] (sender, e)
	End Sub

	Private Sub HandleOLEStartDrag (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLEStartDragEvent) 
		RaiseEvent [OLEStartDrag] (sender, e)
	End Sub

	Private Sub HandleOLESetData (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLESetDataEvent) 
		RaiseEvent [OLESetData] (sender, e)
	End Sub

	Private Sub HandleOLEGiveFeedback (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLEGiveFeedbackEvent) 
		RaiseEvent [OLEGiveFeedback] (sender, e)
	End Sub

	Private Sub HandleOLEDragOver (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLEDragOverEvent) 
		RaiseEvent [OLEDragOver] (sender, e)
	End Sub

	Private Sub HandleOLEDragDrop (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLEDragDropEvent) 
		RaiseEvent [OLEDragDrop] (sender, e)
	End Sub

	Private Sub HandleOLECompleteDrag (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_OLECompleteDragEvent) 
		RaiseEvent [OLECompleteDrag] (sender, e)
	End Sub

	Private Sub HandleHitTest (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_HitTestEvent) 
		RaiseEvent [HitTest] (sender, e)
	End Sub

	Private Sub HandleHideEvent (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [HideEvent] (sender, e)
	End Sub

	Private Sub HandleGetDataMember (ByVal sender As System.Object, ByVal e As AxDataControl.__GmsImports_GetDataMemberEvent) 
		RaiseEvent [GetDataMember] (sender, e)
	End Sub

End Class

