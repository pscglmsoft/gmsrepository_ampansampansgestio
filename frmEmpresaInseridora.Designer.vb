<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmEmpresaInseridora
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents txtServei As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtOferta As System.Windows.Forms.TextBox
	Public WithEvents txtLlocTreball As System.Windows.Forms.TextBox
	Public WithEvents Text51 As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresaInseridora As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtSucursal As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtUsuari As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtDataInici As AxDataControl.AxGmsData
	Public WithEvents cmbTipusRelacio As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents cmbTipusContracte As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents txtDataFi As AxDataControl.AxGmsData
	Public WithEvents txtObservacions As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents chkInsercioDirecta As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkTas As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl51 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEmpresaInseridora))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.Text7 = New System.Windows.Forms.TextBox
		Me.txtServei = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtOferta = New System.Windows.Forms.TextBox
		Me.txtLlocTreball = New System.Windows.Forms.TextBox
		Me.Text51 = New System.Windows.Forms.TextBox
		Me.txtEmpresaInseridora = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtSucursal = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtUsuari = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtDataInici = New AxDataControl.AxGmsData
		Me.cmbTipusRelacio = New AxSSDataWidgets_B.AxSSDBCombo
		Me.cmbTipusContracte = New AxSSDataWidgets_B.AxSSDBCombo
		Me.txtDataFi = New AxDataControl.AxGmsData
		Me.txtObservacions = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.chkInsercioDirecta = New AxXtremeSuiteControls.AxCheckBox
		Me.chkTas = New AxXtremeSuiteControls.AxCheckBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl51 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtDataInici, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbTipusRelacio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbTipusContracte, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataFi, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkInsercioDirecta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkTas, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Contractes/Pr�ctiques Inserci�"
		Me.ClientSize = New System.Drawing.Size(565, 403)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-EMPRESA INSERIDORA"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmEmpresaInseridora"
		Me.Text7.AutoSize = False
		Me.Text7.BackColor = System.Drawing.Color.White
		Me.Text7.Enabled = False
		Me.Text7.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text7.Size = New System.Drawing.Size(382, 19)
		Me.Text7.Location = New System.Drawing.Point(166, 156)
		Me.Text7.TabIndex = 31
		Me.Text7.Tag = "^13"
		Me.Text7.AcceptsReturn = True
		Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text7.CausesValidation = True
		Me.Text7.HideSelection = True
		Me.Text7.ReadOnly = False
		Me.Text7.Maxlength = 0
		Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text7.MultiLine = False
		Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text7.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text7.TabStop = True
		Me.Text7.Visible = True
		Me.Text7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text7.Name = "Text7"
		Me.txtServei.AutoSize = False
		Me.txtServei.Size = New System.Drawing.Size(21, 19)
		Me.txtServei.Location = New System.Drawing.Point(142, 156)
		Me.txtServei.Maxlength = 2
		Me.txtServei.TabIndex = 8
		Me.txtServei.Tag = "13"
		Me.txtServei.AcceptsReturn = True
		Me.txtServei.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtServei.BackColor = System.Drawing.SystemColors.Window
		Me.txtServei.CausesValidation = True
		Me.txtServei.Enabled = True
		Me.txtServei.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtServei.HideSelection = True
		Me.txtServei.ReadOnly = False
		Me.txtServei.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtServei.MultiLine = False
		Me.txtServei.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtServei.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtServei.TabStop = True
		Me.txtServei.Visible = True
		Me.txtServei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtServei.Name = "txtServei"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(382, 19)
		Me.Text5.Location = New System.Drawing.Point(166, 132)
		Me.Text5.TabIndex = 29
		Me.Text5.Tag = "^11"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtOferta.AutoSize = False
		Me.txtOferta.Enabled = False
		Me.txtOferta.Size = New System.Drawing.Size(21, 19)
		Me.txtOferta.Location = New System.Drawing.Point(142, 132)
		Me.txtOferta.Maxlength = 2
		Me.txtOferta.TabIndex = 7
		Me.txtOferta.Tag = "11"
		Me.txtOferta.AcceptsReturn = True
		Me.txtOferta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtOferta.BackColor = System.Drawing.SystemColors.Window
		Me.txtOferta.CausesValidation = True
		Me.txtOferta.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtOferta.HideSelection = True
		Me.txtOferta.ReadOnly = False
		Me.txtOferta.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtOferta.MultiLine = False
		Me.txtOferta.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtOferta.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtOferta.TabStop = True
		Me.txtOferta.Visible = True
		Me.txtOferta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtOferta.Name = "txtOferta"
		Me.txtLlocTreball.AutoSize = False
		Me.txtLlocTreball.Size = New System.Drawing.Size(21, 19)
		Me.txtLlocTreball.Location = New System.Drawing.Point(142, 228)
		Me.txtLlocTreball.Maxlength = 2
		Me.txtLlocTreball.TabIndex = 11
		Me.txtLlocTreball.Tag = "9"
		Me.txtLlocTreball.AcceptsReturn = True
		Me.txtLlocTreball.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLlocTreball.BackColor = System.Drawing.SystemColors.Window
		Me.txtLlocTreball.CausesValidation = True
		Me.txtLlocTreball.Enabled = True
		Me.txtLlocTreball.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLlocTreball.HideSelection = True
		Me.txtLlocTreball.ReadOnly = False
		Me.txtLlocTreball.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLlocTreball.MultiLine = False
		Me.txtLlocTreball.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLlocTreball.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLlocTreball.TabStop = True
		Me.txtLlocTreball.Visible = True
		Me.txtLlocTreball.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtLlocTreball.Name = "txtLlocTreball"
		Me.Text51.AutoSize = False
		Me.Text51.BackColor = System.Drawing.Color.White
		Me.Text51.Enabled = False
		Me.Text51.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text51.Size = New System.Drawing.Size(382, 19)
		Me.Text51.Location = New System.Drawing.Point(166, 228)
		Me.Text51.TabIndex = 23
		Me.Text51.Tag = "^9"
		Me.Text51.AcceptsReturn = True
		Me.Text51.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text51.CausesValidation = True
		Me.Text51.HideSelection = True
		Me.Text51.ReadOnly = False
		Me.Text51.Maxlength = 0
		Me.Text51.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text51.MultiLine = False
		Me.Text51.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text51.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text51.TabStop = True
		Me.Text51.Visible = True
		Me.Text51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text51.Name = "Text51"
		Me.txtEmpresaInseridora.AutoSize = False
		Me.txtEmpresaInseridora.Size = New System.Drawing.Size(83, 19)
		Me.txtEmpresaInseridora.Location = New System.Drawing.Point(142, 10)
		Me.txtEmpresaInseridora.Maxlength = 8
		Me.txtEmpresaInseridora.TabIndex = 1
		Me.txtEmpresaInseridora.Tag = "*1"
		Me.txtEmpresaInseridora.AcceptsReturn = True
		Me.txtEmpresaInseridora.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresaInseridora.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresaInseridora.CausesValidation = True
		Me.txtEmpresaInseridora.Enabled = True
		Me.txtEmpresaInseridora.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresaInseridora.HideSelection = True
		Me.txtEmpresaInseridora.ReadOnly = False
		Me.txtEmpresaInseridora.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresaInseridora.MultiLine = False
		Me.txtEmpresaInseridora.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresaInseridora.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresaInseridora.TabStop = True
		Me.txtEmpresaInseridora.Visible = True
		Me.txtEmpresaInseridora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresaInseridora.Name = "txtEmpresaInseridora"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(322, 19)
		Me.Text1.Location = New System.Drawing.Point(228, 10)
		Me.Text1.TabIndex = 15
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtSucursal.AutoSize = False
		Me.txtSucursal.Size = New System.Drawing.Size(31, 19)
		Me.txtSucursal.Location = New System.Drawing.Point(142, 34)
		Me.txtSucursal.Maxlength = 3
		Me.txtSucursal.TabIndex = 2
		Me.txtSucursal.Tag = "*2"
		Me.txtSucursal.AcceptsReturn = True
		Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSucursal.BackColor = System.Drawing.SystemColors.Window
		Me.txtSucursal.CausesValidation = True
		Me.txtSucursal.Enabled = True
		Me.txtSucursal.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSucursal.HideSelection = True
		Me.txtSucursal.ReadOnly = False
		Me.txtSucursal.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSucursal.MultiLine = False
		Me.txtSucursal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSucursal.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSucursal.TabStop = True
		Me.txtSucursal.Visible = True
		Me.txtSucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSucursal.Name = "txtSucursal"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(374, 19)
		Me.Text2.Location = New System.Drawing.Point(176, 34)
		Me.Text2.TabIndex = 17
		Me.Text2.Tag = "^2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtUsuari.AutoSize = False
		Me.txtUsuari.Size = New System.Drawing.Size(55, 19)
		Me.txtUsuari.Location = New System.Drawing.Point(142, 58)
		Me.txtUsuari.Maxlength = 8
		Me.txtUsuari.TabIndex = 3
		Me.txtUsuari.Tag = "*3"
		Me.txtUsuari.AcceptsReturn = True
		Me.txtUsuari.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsuari.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsuari.CausesValidation = True
		Me.txtUsuari.Enabled = True
		Me.txtUsuari.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsuari.HideSelection = True
		Me.txtUsuari.ReadOnly = False
		Me.txtUsuari.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsuari.MultiLine = False
		Me.txtUsuari.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsuari.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsuari.TabStop = True
		Me.txtUsuari.Visible = True
		Me.txtUsuari.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUsuari.Name = "txtUsuari"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(350, 19)
		Me.Text3.Location = New System.Drawing.Point(200, 58)
		Me.Text3.TabIndex = 19
		Me.Text3.Tag = "^3"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		txtDataInici.OcxState = CType(resources.GetObject("txtDataInici.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataInici.Size = New System.Drawing.Size(87, 19)
		Me.txtDataInici.Location = New System.Drawing.Point(142, 82)
		Me.txtDataInici.TabIndex = 4
		Me.txtDataInici.Name = "txtDataInici"
		cmbTipusRelacio.OcxState = CType(resources.GetObject("cmbTipusRelacio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbTipusRelacio.Size = New System.Drawing.Size(145, 19)
		Me.cmbTipusRelacio.Location = New System.Drawing.Point(142, 180)
		Me.cmbTipusRelacio.TabIndex = 9
		Me.cmbTipusRelacio.Name = "cmbTipusRelacio"
		cmbTipusContracte.OcxState = CType(resources.GetObject("cmbTipusContracte.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbTipusContracte.Size = New System.Drawing.Size(145, 19)
		Me.cmbTipusContracte.Location = New System.Drawing.Point(142, 204)
		Me.cmbTipusContracte.TabIndex = 10
		Me.cmbTipusContracte.Name = "cmbTipusContracte"
		txtDataFi.OcxState = CType(resources.GetObject("txtDataFi.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataFi.Size = New System.Drawing.Size(87, 19)
		Me.txtDataFi.Location = New System.Drawing.Point(142, 252)
		Me.txtDataFi.TabIndex = 12
		Me.txtDataFi.Name = "txtDataFi"
		Me.txtObservacions.AutoSize = False
		Me.txtObservacions.Size = New System.Drawing.Size(408, 71)
		Me.txtObservacions.Location = New System.Drawing.Point(142, 276)
		Me.txtObservacions.Maxlength = 150
		Me.txtObservacions.MultiLine = True
		Me.txtObservacions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtObservacions.TabIndex = 13
		Me.txtObservacions.Tag = "8"
		Me.txtObservacions.AcceptsReturn = True
		Me.txtObservacions.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacions.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacions.CausesValidation = True
		Me.txtObservacions.Enabled = True
		Me.txtObservacions.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacions.HideSelection = True
		Me.txtObservacions.ReadOnly = False
		Me.txtObservacions.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacions.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacions.TabStop = True
		Me.txtObservacions.Visible = True
		Me.txtObservacions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacions.Name = "txtObservacions"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(379, 364)
		Me.cmdAceptar.TabIndex = 14
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(469, 364)
		Me.cmdGuardar.TabIndex = 26
		Me.cmdGuardar.Name = "cmdGuardar"
		chkInsercioDirecta.OcxState = CType(resources.GetObject("chkInsercioDirecta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkInsercioDirecta.Size = New System.Drawing.Size(145, 19)
		Me.chkInsercioDirecta.Location = New System.Drawing.Point(10, 108)
		Me.chkInsercioDirecta.TabIndex = 5
		Me.chkInsercioDirecta.Name = "chkInsercioDirecta"
		chkTas.OcxState = CType(resources.GetObject("chkTas.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkTas.Size = New System.Drawing.Size(55, 19)
		Me.chkTas.Location = New System.Drawing.Point(190, 108)
		Me.chkTas.TabIndex = 6
		Me.chkTas.Name = "chkTas"
		Me.Label2.Text = "Servei"
		Me.Label2.Size = New System.Drawing.Size(111, 15)
		Me.Label2.Location = New System.Drawing.Point(10, 160)
		Me.Label2.TabIndex = 32
		Me.Label2.Tag = "13"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Oferta"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 136)
		Me.Label1.TabIndex = 30
		Me.Label1.Tag = "11"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl51.Text = "Lloc de treball"
		Me.lbl51.Size = New System.Drawing.Size(111, 15)
		Me.lbl51.Location = New System.Drawing.Point(10, 232)
		Me.lbl51.TabIndex = 28
		Me.lbl51.Tag = "9"
		Me.lbl51.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl51.BackColor = System.Drawing.SystemColors.Control
		Me.lbl51.Enabled = True
		Me.lbl51.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl51.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl51.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl51.UseMnemonic = True
		Me.lbl51.Visible = True
		Me.lbl51.AutoSize = False
		Me.lbl51.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl51.Name = "lbl51"
		Me.lbl1.Text = "Empresa inseridora"
		Me.lbl1.Size = New System.Drawing.Size(153, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Sucursal"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 16
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Usuari"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 18
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Data inici"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 86)
		Me.lbl4.TabIndex = 20
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Tipus relaci�"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 184)
		Me.lbl5.TabIndex = 21
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Tipus contracte"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 208)
		Me.lbl6.TabIndex = 22
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "Data fi"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 256)
		Me.lbl7.TabIndex = 24
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl8.Text = "Observacions"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(10, 280)
		Me.lbl8.TabIndex = 25
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 363)
		Me.lblLock.TabIndex = 27
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 357
		Me.Line1.Y2 = 357
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 358
		Me.Line2.Y2 = 358
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.chkTas, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkInsercioDirecta, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataFi, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbTipusContracte, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbTipusRelacio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataInici, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(Text7)
		Me.Controls.Add(txtServei)
		Me.Controls.Add(Text5)
		Me.Controls.Add(txtOferta)
		Me.Controls.Add(txtLlocTreball)
		Me.Controls.Add(Text51)
		Me.Controls.Add(txtEmpresaInseridora)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtSucursal)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtUsuari)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtDataInici)
		Me.Controls.Add(cmbTipusRelacio)
		Me.Controls.Add(cmbTipusContracte)
		Me.Controls.Add(txtDataFi)
		Me.Controls.Add(txtObservacions)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(chkInsercioDirecta)
		Me.Controls.Add(chkTas)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl51)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class