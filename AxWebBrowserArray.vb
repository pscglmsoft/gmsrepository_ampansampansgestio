'UPGRADE_WARNING: El proyecto entero se debe compilar una vez antes de poder mostrar una matriz de controles ActiveX

Imports System.ComponentModel

<ProvideProperty("Index",GetType(AxXtremeSuiteControls.AxWebBrowser))> Public Class AxWebBrowserArray
	Inherits Microsoft.VisualBasic.Compatibility.VB6.BaseOcxArray
	Implements IExtenderProvider

	Public Sub New()
		MyBase.New()
	End Sub

	Public Sub New(ByVal Container As IContainer)
		MyBase.New(Container)
	End Sub

	Public Shadows Event [DownloadComplete] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [DocumentComplete] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_DocumentCompleteEvent)
	Public Shadows Event [NavigateComplete2] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_NavigateComplete2Event)
	Public Shadows Event [BeforeNavigate2] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_BeforeNavigate2Event)
	Public Shadows Event [StatusTextChange] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_StatusTextChangeEvent)
	Public Shadows Event [ProgressChange] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_ProgressChangeEvent)
	Public Shadows Event [TitleChange] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_TitleChangeEvent)
	Public Shadows Event [DownloadBegin] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [CommandStateChange] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_CommandStateChangeEvent)
	Public Shadows Event [PropertyChange] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_PropertyChangeEvent)
	Public Shadows Event [NewWindow2] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_NewWindow2Event)
	Public Shadows Event [OnQuit] (ByVal sender As System.Object, ByVal e As System.EventArgs)
	Public Shadows Event [NavigateError] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_NavigateErrorEvent)
	Public Shadows Event [WindowSetResizable] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowSetResizableEvent)
	Public Shadows Event [WindowSetLeft] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowSetLeftEvent)
	Public Shadows Event [WindowSetTop] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowSetTopEvent)
	Public Shadows Event [WindowSetWidth] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowSetWidthEvent)
	Public Shadows Event [WindowSetHeight] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowSetHeightEvent)
	Public Shadows Event [WindowClosing] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowClosingEvent)
	Public Shadows Event [SetSecureLockIcon] (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_SetSecureLockIconEvent)

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Function CanExtend(ByVal target As Object) As Boolean Implements IExtenderProvider.CanExtend
		If TypeOf target Is AxXtremeSuiteControls.AxWebBrowser Then
			Return BaseCanExtend(target)
		End If
	End Function

	Public Function GetIndex(ByVal o As AxXtremeSuiteControls.AxWebBrowser) As Short
		Return BaseGetIndex(o)
	End Function

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Sub SetIndex(ByVal o As AxXtremeSuiteControls.AxWebBrowser, ByVal Index As Short)
		BaseSetIndex(o, Index)
	End Sub

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Function ShouldSerializeIndex(ByVal o As AxXtremeSuiteControls.AxWebBrowser) As Boolean
		Return BaseShouldSerializeIndex(o)
	End Function

	<System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)> Public Sub ResetIndex(ByVal o As AxXtremeSuiteControls.AxWebBrowser)
		BaseResetIndex(o)
	End Sub

	Default Public ReadOnly Property Item(ByVal Index As Short) As AxXtremeSuiteControls.AxWebBrowser
		Get
			Item = CType(BaseGetItem(Index), AxXtremeSuiteControls.AxWebBrowser)
		End Get
	End Property

	Protected Overrides Function GetControlInstanceType() As System.Type
		Return GetType(AxXtremeSuiteControls.AxWebBrowser)
	End Function

	Protected Overrides Sub HookUpControlEvents(ByVal o As Object)
		Dim ctl As AxXtremeSuiteControls.AxWebBrowser = CType(o, AxXtremeSuiteControls.AxWebBrowser)
		MyBase.HookUpControlEvents(o)
		If Not DownloadCompleteEvent Is Nothing Then
			AddHandler ctl.DownloadComplete, New System.EventHandler(AddressOf HandleDownloadComplete)
		End If
		If Not DocumentCompleteEvent Is Nothing Then
			AddHandler ctl.DocumentComplete, New AxXtremeSuiteControls._DWebBrowserEvents_DocumentCompleteEventHandler(AddressOf HandleDocumentComplete)
		End If
		If Not NavigateComplete2Event Is Nothing Then
			AddHandler ctl.NavigateComplete2, New AxXtremeSuiteControls._DWebBrowserEvents_NavigateComplete2EventHandler(AddressOf HandleNavigateComplete2)
		End If
		If Not BeforeNavigate2Event Is Nothing Then
			AddHandler ctl.BeforeNavigate2, New AxXtremeSuiteControls._DWebBrowserEvents_BeforeNavigate2EventHandler(AddressOf HandleBeforeNavigate2)
		End If
		If Not StatusTextChangeEvent Is Nothing Then
			AddHandler ctl.StatusTextChange, New AxXtremeSuiteControls._DWebBrowserEvents_StatusTextChangeEventHandler(AddressOf HandleStatusTextChange)
		End If
		If Not ProgressChangeEvent Is Nothing Then
			AddHandler ctl.ProgressChange, New AxXtremeSuiteControls._DWebBrowserEvents_ProgressChangeEventHandler(AddressOf HandleProgressChange)
		End If
		If Not TitleChangeEvent Is Nothing Then
			AddHandler ctl.TitleChange, New AxXtremeSuiteControls._DWebBrowserEvents_TitleChangeEventHandler(AddressOf HandleTitleChange)
		End If
		If Not DownloadBeginEvent Is Nothing Then
			AddHandler ctl.DownloadBegin, New System.EventHandler(AddressOf HandleDownloadBegin)
		End If
		If Not CommandStateChangeEvent Is Nothing Then
			AddHandler ctl.CommandStateChange, New AxXtremeSuiteControls._DWebBrowserEvents_CommandStateChangeEventHandler(AddressOf HandleCommandStateChange)
		End If
		If Not PropertyChangeEvent Is Nothing Then
			AddHandler ctl.PropertyChange, New AxXtremeSuiteControls._DWebBrowserEvents_PropertyChangeEventHandler(AddressOf HandlePropertyChange)
		End If
		If Not NewWindow2Event Is Nothing Then
			AddHandler ctl.NewWindow2, New AxXtremeSuiteControls._DWebBrowserEvents_NewWindow2EventHandler(AddressOf HandleNewWindow2)
		End If
		If Not OnQuitEvent Is Nothing Then
			AddHandler ctl.OnQuit, New System.EventHandler(AddressOf HandleOnQuit)
		End If
		If Not NavigateErrorEvent Is Nothing Then
			AddHandler ctl.NavigateError, New AxXtremeSuiteControls._DWebBrowserEvents_NavigateErrorEventHandler(AddressOf HandleNavigateError)
		End If
		If Not WindowSetResizableEvent Is Nothing Then
			AddHandler ctl.WindowSetResizable, New AxXtremeSuiteControls._DWebBrowserEvents_WindowSetResizableEventHandler(AddressOf HandleWindowSetResizable)
		End If
		If Not WindowSetLeftEvent Is Nothing Then
			AddHandler ctl.WindowSetLeft, New AxXtremeSuiteControls._DWebBrowserEvents_WindowSetLeftEventHandler(AddressOf HandleWindowSetLeft)
		End If
		If Not WindowSetTopEvent Is Nothing Then
			AddHandler ctl.WindowSetTop, New AxXtremeSuiteControls._DWebBrowserEvents_WindowSetTopEventHandler(AddressOf HandleWindowSetTop)
		End If
		If Not WindowSetWidthEvent Is Nothing Then
			AddHandler ctl.WindowSetWidth, New AxXtremeSuiteControls._DWebBrowserEvents_WindowSetWidthEventHandler(AddressOf HandleWindowSetWidth)
		End If
		If Not WindowSetHeightEvent Is Nothing Then
			AddHandler ctl.WindowSetHeight, New AxXtremeSuiteControls._DWebBrowserEvents_WindowSetHeightEventHandler(AddressOf HandleWindowSetHeight)
		End If
		If Not WindowClosingEvent Is Nothing Then
			AddHandler ctl.WindowClosing, New AxXtremeSuiteControls._DWebBrowserEvents_WindowClosingEventHandler(AddressOf HandleWindowClosing)
		End If
		If Not SetSecureLockIconEvent Is Nothing Then
			AddHandler ctl.SetSecureLockIcon, New AxXtremeSuiteControls._DWebBrowserEvents_SetSecureLockIconEventHandler(AddressOf HandleSetSecureLockIcon)
		End If
	End Sub

	Private Sub HandleDownloadComplete (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [DownloadComplete] (sender, e)
	End Sub

	Private Sub HandleDocumentComplete (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_DocumentCompleteEvent) 
		RaiseEvent [DocumentComplete] (sender, e)
	End Sub

	Private Sub HandleNavigateComplete2 (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_NavigateComplete2Event) 
		RaiseEvent [NavigateComplete2] (sender, e)
	End Sub

	Private Sub HandleBeforeNavigate2 (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_BeforeNavigate2Event) 
		RaiseEvent [BeforeNavigate2] (sender, e)
	End Sub

	Private Sub HandleStatusTextChange (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_StatusTextChangeEvent) 
		RaiseEvent [StatusTextChange] (sender, e)
	End Sub

	Private Sub HandleProgressChange (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_ProgressChangeEvent) 
		RaiseEvent [ProgressChange] (sender, e)
	End Sub

	Private Sub HandleTitleChange (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_TitleChangeEvent) 
		RaiseEvent [TitleChange] (sender, e)
	End Sub

	Private Sub HandleDownloadBegin (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [DownloadBegin] (sender, e)
	End Sub

	Private Sub HandleCommandStateChange (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_CommandStateChangeEvent) 
		RaiseEvent [CommandStateChange] (sender, e)
	End Sub

	Private Sub HandlePropertyChange (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_PropertyChangeEvent) 
		RaiseEvent [PropertyChange] (sender, e)
	End Sub

	Private Sub HandleNewWindow2 (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_NewWindow2Event) 
		RaiseEvent [NewWindow2] (sender, e)
	End Sub

	Private Sub HandleOnQuit (ByVal sender As System.Object, ByVal e As System.EventArgs) 
		RaiseEvent [OnQuit] (sender, e)
	End Sub

	Private Sub HandleNavigateError (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_NavigateErrorEvent) 
		RaiseEvent [NavigateError] (sender, e)
	End Sub

	Private Sub HandleWindowSetResizable (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowSetResizableEvent) 
		RaiseEvent [WindowSetResizable] (sender, e)
	End Sub

	Private Sub HandleWindowSetLeft (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowSetLeftEvent) 
		RaiseEvent [WindowSetLeft] (sender, e)
	End Sub

	Private Sub HandleWindowSetTop (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowSetTopEvent) 
		RaiseEvent [WindowSetTop] (sender, e)
	End Sub

	Private Sub HandleWindowSetWidth (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowSetWidthEvent) 
		RaiseEvent [WindowSetWidth] (sender, e)
	End Sub

	Private Sub HandleWindowSetHeight (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowSetHeightEvent) 
		RaiseEvent [WindowSetHeight] (sender, e)
	End Sub

	Private Sub HandleWindowClosing (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_WindowClosingEvent) 
		RaiseEvent [WindowClosing] (sender, e)
	End Sub

	Private Sub HandleSetSecureLockIcon (ByVal sender As System.Object, ByVal e As AxXtremeSuiteControls._DWebBrowserEvents_SetSecureLockIconEvent) 
		RaiseEvent [SetSecureLockIcon] (sender, e)
	End Sub

End Class

