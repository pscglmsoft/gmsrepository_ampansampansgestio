Option Strict Off
Option Explicit On
Friend Class frmAssignaDemanda
	Inherits FormParent
	''OKPublic Appl As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic ABM As String
	''OKPublic Nkeys As Short
	Public Data As String
	Public CodiDemanda As Short
	Public TipusDem As String
	Public CodiTipusDem As Short
	
	
	
Overrides 	Sub Inici()
		ResetForm(Me)
	End Sub
	
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		CacheNetejaParametres()
		MCache.P1 = CodiDemanda
		
		
		If TipusDem = "U" Then
			MCache.P2 = txtPersonaQueGestiona.Text
			MCache.P3 = txtProblema.Text
			MCache.P4 = TipusDem
			CacheXecute("D ASSIGNADEMU^DEMANDES")
		End If
		
		If TipusDem = "C" Then
			MCache.P2 = txtCentreQueGestiona.Text
			MCache.P3 = txtProblema.Text
			MCache.P4 = TipusDem
			CacheXecute("D ASSIGNADEMC^DEMANDES")
		End If
		
		If TipusDem = "A" Then
			MCache.P2 = txtCentreQueGestiona.Text
			MCache.P3 = txtProblema.Text
			MCache.P4 = TipusDem
			CacheXecute("D CREADEMCEN^DEMANDES")
		End If
		
		Me.Unload()
		CarregaDemandesP()
		CarregaConsultaDemandes()
	End Sub
	
	Private Sub frmAssignaDemanda_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmAssignaDemanda_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmAssignaDemanda_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
	End Sub
	
	
	Private Sub txtCentreQueGestiona_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreQueGestiona.DoubleClick
		ConsultaTaula(Me, txtCentreQueGestiona,  ,  , "S WL=$$XVERCEN^DEMANDES(" & CodiTipusDem & ",$P(%REGC,S,1))")
	End Sub
	
	Private Sub txtCentreQueGestiona_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentreQueGestiona.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPersonaQueGestiona_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPersonaQueGestiona.DoubleClick
		ConsultaTaula(Me, txtPersonaQueGestiona,  ,  , "S WL=$$XVERCON^DEMANDES(" & txtCentreQueGestiona.Text & " ,$P(%REGC,S,1)," & Data & ")")
	End Sub
	
	Private Sub txtPersonaQueGestiona_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPersonaQueGestiona.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	Private Sub CarregaDemandesP()
		CarregaFGrid((frmDemandesPendents.GrdDemandesPendents), "CGDEMP^DEMANDES")
	End Sub
	
	Private Sub CarregaConsultaDemandes()
		CarregaFGrid((frmDemandesPendents.GrdConsultaDemandes), "CGCONDEM^DEMANDES")
	End Sub
End Class
