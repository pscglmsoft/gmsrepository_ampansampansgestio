Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmKilometratge
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	Private Sub frmKilometratge_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmKilometratge_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmKilometratge_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmKilometratge_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtNumeroRegistre_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumeroRegistre.GotFocus
		XGotFocus(Me, txtNumeroRegistre)
	End Sub
	
	Private Sub txtNumeroRegistre_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumeroRegistre.LostFocus
		XLostFocus(Me, txtNumeroRegistre)
	End Sub
	
	Private Sub txtVehicle_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtVehicle.GotFocus
		XGotFocus(Me, txtVehicle)
	End Sub
	
	Private Sub txtVehicle_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtVehicle.DoubleClick
		ConsultaTaula(Me, txtVehicle)
	End Sub
	
	Private Sub txtVehicle_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtVehicle.LostFocus
		If txtVehicle.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtVehicle)
	End Sub
	
	Private Sub txtVehicle_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtVehicle.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtData_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtData.GotFocus
		XGotFocus(Me, txtData)
	End Sub
	
	Private Sub txtData_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtData.LostFocus
		If txtData.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtData)
	End Sub
	
	Private Sub txtPersona_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPersona.GotFocus
		XGotFocus(Me, txtPersona)
	End Sub
	
	Private Sub txtPersona_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPersona.DoubleClick
		ConsultaTaula(Me, txtPersona)
	End Sub
	
	Private Sub txtPersona_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPersona.LostFocus
		XLostFocus(Me, txtPersona)
	End Sub
	
	Private Sub txtPersona_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPersona.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCentreTreball_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreTreball.GotFocus
		XGotFocus(Me, txtCentreTreball)
	End Sub
	
	Private Sub txtCentreTreball_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreTreball.DoubleClick
		ConsultaTaula(Me, txtCentreTreball)
	End Sub
	
	Private Sub txtCentreTreball_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreTreball.LostFocus
		XLostFocus(Me, txtCentreTreball)
	End Sub
	
	Private Sub txtCentreTreball_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentreTreball.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtKilometresInici_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtKilometresInici.GotFocus
		XGotFocus(Me, txtKilometresInici)
	End Sub
	
	Private Sub txtKilometresInici_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtKilometresInici.LostFocus
		XLostFocus(Me, txtKilometresInici)
	End Sub
	
	Private Sub txtKilometresInici_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtKilometresInici.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtKilometresFi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtKilometresFi.GotFocus
		XGotFocus(Me, txtKilometresFi)
	End Sub
	
	Private Sub txtKilometresFi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtKilometresFi.LostFocus
		XLostFocus(Me, txtKilometresFi)
	End Sub
	
	Private Sub txtKilometresFi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtKilometresFi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDiferencia_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDiferencia.GotFocus
		XGotFocus(Me, txtDiferencia)
	End Sub
	
	Private Sub txtDiferencia_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDiferencia.LostFocus
		XLostFocus(Me, txtDiferencia)
	End Sub
	
	Private Sub txtDiferencia_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDiferencia.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
