Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmLoteria
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
Overrides 	Sub MoveReg(ByRef Accio As String)
		MouReg(Me, Accio)
		CambiColor()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		CambiColor()
	End Sub
	
	
	Private Sub cmdImprimir_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdImprimir.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		ImpresioPDF("I-IMPRES_LOTERIA", txtCodi.Text)
	End Sub
	
	Private Sub frmLoteria_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If txtDataPagat.Text <> "" Then
			If txtImportBanc.Text = "" And txtImportEfectiu.Text = "" Then
				xMsgBox("Falta informar Import Efectiu o Import Banc", MsgBoxStyle.Information, Me.Text)
				txtImportEfectiu.Focus()
				Exit Sub
			End If
		End If
		
		If Val(txtImportBanc.Text) + Val(txtImportEfectiu.Text) > Val(txtImport.Text) Then
			xMsgBox("L'import Efectiu + Import Banc no pot superar a l'Import total de la loteria", MsgBoxStyle.Information, Me.Text)
			txtImportEfectiu.Focus()
			Exit Sub
		End If
		
		If (txtImportBanc.Text <> "" Or txtImportEfectiu.Text <> "") And txtDataPagat.Text = "" Then
			xMsgBox("Per informar els imports has de informar primer la data de pagament", MsgBoxStyle.Information, Me.Text)
			txtDataPagat.Focus()
			Exit Sub
		End If
		
		If txtDataEntrega.Text = "" And txtDataPagat.Text <> "" Then txtDataEntrega.Text = txtDataPagat.Text
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmLoteria_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		'    txtImportBanc.Enabled = False
		'    txtImportEfectiu.Enabled = False
		cmdImprimir.Picture = SetIcon("Print", 16)
	End Sub
	
	Private Sub frmLoteria_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmLoteria_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCodi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.GotFocus
		XGotFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.DoubleClick
		ConsultaTaula(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.LostFocus
		If txtCodi.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodi)
		If txtAny.Text = "" Then txtAny.Text = CStr(Year(Today))
		If txtPreu.Text = "" Then txtPreu.Text = CStr(5)
	End Sub
	
	Private Sub txtCodi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtCodi)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtAny_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAny.GotFocus
		XGotFocus(Me, txtAny)
	End Sub
	
	Private Sub txtAny_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAny.LostFocus
		XLostFocus(Me, txtAny)
	End Sub
	
	Private Sub txtAny_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtAny.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtAny)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEntitat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntitat.GotFocus
		XGotFocus(Me, txtEntitat)
	End Sub
	
	Private Sub txtEntitat_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntitat.DoubleClick
		ConsultaTaula(Me, txtEntitat)
	End Sub
	
	Private Sub txtEntitat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntitat.LostFocus
		XLostFocus(Me, txtEntitat)
	End Sub
	
	Private Sub txtEntitat_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEntitat.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtEntitat)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtEntitat)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbEstat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstat.GotFocus
		XGotFocus(Me, cmbEstat)
	End Sub
	
	Private Sub cmbEstat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstat.LostFocus
		XLostFocus(Me, cmbEstat)
	End Sub
	
	Private Sub txtCentre_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentre.GotFocus
		XGotFocus(Me, txtCentre)
	End Sub
	
	Private Sub txtCentre_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentre.DoubleClick
		ConsultaTaula(Me, txtCentre)
	End Sub
	
	Private Sub txtCentre_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentre.LostFocus
		XLostFocus(Me, txtCentre)
	End Sub
	
	Private Sub txtCentre_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentre.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtCentre)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtCentre)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	'UPGRADE_WARNING: El evento txtImportBanc.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtImportBanc_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImportBanc.TextChanged
		CambiColor()
	End Sub
	
	'UPGRADE_WARNING: El evento txtImportEfectiu.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtImportEfectiu_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImportEfectiu.TextChanged
		CambiColor()
	End Sub
	
	Private Sub txtQuantitat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtQuantitat.GotFocus
		XGotFocus(Me, txtQuantitat)
	End Sub
	
	Private Sub txtQuantitat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtQuantitat.LostFocus
		XLostFocus(Me, txtQuantitat)
		If txtQuantitat.Text <> "" Then txtImport.Text = CStr(CDbl(txtQuantitat.Text) * CDbl(txtPreu.Text))
	End Sub
	
	Private Sub txtQuantitat_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtQuantitat.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtQuantitat)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPreu_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPreu.GotFocus
		XGotFocus(Me, txtPreu)
	End Sub
	
	Private Sub txtPreu_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPreu.LostFocus
		XLostFocus(Me, txtPreu)
	End Sub
	
	Private Sub txtPreu_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPreu.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtPreu)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtImport_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImport.GotFocus
		XGotFocus(Me, txtImport)
	End Sub
	
	Private Sub txtImport_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImport.LostFocus
		XLostFocus(Me, txtImport)
	End Sub
	
	Private Sub txtImport_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtImport.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtImport)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtNumeroButlletes_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumeroButlletes.GotFocus
		XGotFocus(Me, txtNumeroButlletes)
	End Sub
	
	Private Sub txtNumeroButlletes_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumeroButlletes.LostFocus
		XLostFocus(Me, txtNumeroButlletes)
	End Sub
	
	Private Sub txtNumeroButlletes_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNumeroButlletes.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtNumeroButlletes)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservacions_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.GotFocus
		XGotFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.LostFocus
		XLostFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservacions.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtObservacions)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDataEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataEntrega.GotFocus
		XGotFocus(Me, txtDataEntrega)
	End Sub
	
	Private Sub txtDataEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataEntrega.LostFocus
		XLostFocus(Me, txtDataEntrega)
	End Sub
	
	Private Sub txtDataPagat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataPagat.GotFocus
		XGotFocus(Me, txtDataPagat)
	End Sub
	
	Private Sub txtDataPagat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataPagat.LostFocus
		XLostFocus(Me, txtDataPagat)
		'    If txtDataPagat <> "" Then
		'        txtImportBanc.Enabled = True
		'        txtImportEfectiu.Enabled = True
		'    Else
		'        txtImportBanc = ""
		'        txtImportBanc.Enabled = False
		'        txtImportEfectiu = ""
		'        txtImportEfectiu.Enabled = False
		'    End If
	End Sub
	
	Private Sub txtImportEfectiu_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImportEfectiu.GotFocus
		XGotFocus(Me, txtImportEfectiu)
	End Sub
	
	Private Sub txtImportEfectiu_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImportEfectiu.LostFocus
		XLostFocus(Me, txtImportEfectiu)
	End Sub
	
	Private Sub txtImportEfectiu_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtImportEfectiu.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtImportEfectiu)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtImportBanc_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImportBanc.GotFocus
		XGotFocus(Me, txtImportBanc)
	End Sub
	
	Private Sub txtImportBanc_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImportBanc.LostFocus
		XLostFocus(Me, txtImportBanc)
	End Sub
	
	Private Sub txtImportBanc_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtImportBanc.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtImportBanc)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub CambiColor()
		If Val(txtImportEfectiu.Text) + Val(txtImportBanc.Text) <> 0 And Val(txtImportEfectiu.Text) + Val(txtImportBanc.Text) = Val(txtImport.Text) Then
			txtImportBanc.BackColor = System.Drawing.ColorTranslator.FromOle(65280)
			txtImportEfectiu.BackColor = System.Drawing.ColorTranslator.FromOle(65280)
		ElseIf Val(txtImportEfectiu.Text) + Val(txtImportBanc.Text) <> 0 And Val(txtImportEfectiu.Text) + Val(txtImportBanc.Text) <> Val(txtImport.Text) Then 
			txtImportBanc.BackColor = System.Drawing.ColorTranslator.FromOle(RGB(255, 0, 0))
			txtImportEfectiu.BackColor = System.Drawing.ColorTranslator.FromOle(RGB(255, 0, 0))
		Else
			txtImportBanc.BackColor = System.Drawing.ColorTranslator.FromOle(&H80000005)
			txtImportEfectiu.BackColor = System.Drawing.ColorTranslator.FromOle(&H80000005)
		End If
	End Sub
	
	
	Private Sub txtServei_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.DoubleClick
		ConsultaTaula(Me, txtServei)
	End Sub
	
	Private Sub txtServei_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.GotFocus
		XGotFocus(Me, txtServei)
	End Sub
	
	Private Sub txtServei_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.LostFocus
		XLostFocus(Me, txtServei)
	End Sub
	
	Private Sub txtServei_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtServei.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtServei)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtServei)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
