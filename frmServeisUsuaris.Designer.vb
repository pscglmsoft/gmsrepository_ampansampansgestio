<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmServeisUsuaris
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtF1 As AxDataControl.AxGmsData
	Public WithEvents txtF2 As AxDataControl.AxGmsData
	Public WithEvents txtF3 As AxDataControl.AxGmsData
	Public WithEvents txtF4 As AxDataControl.AxGmsData
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents FrameNovesOp As AxciaXPFrame30.AxXPFrame30
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtTecnico As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtMotivoBaja As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtMotivoAlta As System.Windows.Forms.TextBox
	Public WithEvents txtUsuario As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtServicio As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtFechaAlta As AxDataControl.AxGmsData
	Public WithEvents txtFechaBaja As AxDataControl.AxGmsData
	Public WithEvents txtPuntControl As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtObservaciones As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents grdProgrames As AxFlexCell.AxGrid
	Public WithEvents txtFechaRecogidaDatos As AxDataControl.AxGmsData
	Public WithEvents txtFechaValoracion As AxDataControl.AxGmsData
	Public WithEvents txtFechaPrepLaboral As AxDataControl.AxGmsData
	Public WithEvents txtFechaIntermediacion As AxDataControl.AxGmsData
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmServeisUsuaris))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.FrameNovesOp = New AxciaXPFrame30.AxXPFrame30
		Me.txtF1 = New AxDataControl.AxGmsData
		Me.txtF2 = New AxDataControl.AxGmsData
		Me.txtF3 = New AxDataControl.AxGmsData
		Me.txtF4 = New AxDataControl.AxGmsData
		Me.Label12 = New System.Windows.Forms.Label
		Me.Label11 = New System.Windows.Forms.Label
		Me.Label10 = New System.Windows.Forms.Label
		Me.Label9 = New System.Windows.Forms.Label
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtTecnico = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtMotivoBaja = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtMotivoAlta = New System.Windows.Forms.TextBox
		Me.txtUsuario = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtServicio = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtFechaAlta = New AxDataControl.AxGmsData
		Me.txtFechaBaja = New AxDataControl.AxGmsData
		Me.txtPuntControl = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtObservaciones = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.grdProgrames = New AxFlexCell.AxGrid
		Me.txtFechaRecogidaDatos = New AxDataControl.AxGmsData
		Me.txtFechaValoracion = New AxDataControl.AxGmsData
		Me.txtFechaPrepLaboral = New AxDataControl.AxGmsData
		Me.txtFechaIntermediacion = New AxDataControl.AxGmsData
		Me.Label8 = New System.Windows.Forms.Label
		Me.Label7 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.FrameNovesOp.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtF1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtF2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtF3, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtF4, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.FrameNovesOp, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaAlta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.grdProgrames, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaRecogidaDatos, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaValoracion, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaPrepLaboral, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaIntermediacion, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Serveis usuaris"
		Me.ClientSize = New System.Drawing.Size(544, 593)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-USUARIS_SERVEIS"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmServeisUsuaris"
		FrameNovesOp.OcxState = CType(resources.GetObject("FrameNovesOp.OcxState"), System.Windows.Forms.AxHost.State)
		Me.FrameNovesOp.Size = New System.Drawing.Size(319, 81)
		Me.FrameNovesOp.Location = New System.Drawing.Point(128, 322)
		Me.FrameNovesOp.Name = "FrameNovesOp"
		txtF1.OcxState = CType(resources.GetObject("txtF1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtF1.Size = New System.Drawing.Size(87, 19)
		Me.txtF1.Location = New System.Drawing.Point(46, 22)
		Me.txtF1.TabIndex = 37
		Me.txtF1.Name = "txtF1"
		txtF2.OcxState = CType(resources.GetObject("txtF2.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtF2.Size = New System.Drawing.Size(87, 19)
		Me.txtF2.Location = New System.Drawing.Point(46, 46)
		Me.txtF2.TabIndex = 39
		Me.txtF2.Name = "txtF2"
		txtF3.OcxState = CType(resources.GetObject("txtF3.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtF3.Size = New System.Drawing.Size(87, 19)
		Me.txtF3.Location = New System.Drawing.Point(220, 22)
		Me.txtF3.TabIndex = 41
		Me.txtF3.Name = "txtF3"
		txtF4.OcxState = CType(resources.GetObject("txtF4.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtF4.Size = New System.Drawing.Size(87, 19)
		Me.txtF4.Location = New System.Drawing.Point(220, 46)
		Me.txtF4.TabIndex = 43
		Me.txtF4.Name = "txtF4"
		Me.Label12.Text = "F4"
		Me.Label12.Size = New System.Drawing.Size(25, 15)
		Me.Label12.Location = New System.Drawing.Point(188, 50)
		Me.Label12.TabIndex = 44
		Me.Label12.Tag = "17"
		Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label12.BackColor = System.Drawing.SystemColors.Control
		Me.Label12.Enabled = True
		Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label12.UseMnemonic = True
		Me.Label12.Visible = True
		Me.Label12.AutoSize = False
		Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label12.Name = "Label12"
		Me.Label11.Text = "F3"
		Me.Label11.Size = New System.Drawing.Size(25, 15)
		Me.Label11.Location = New System.Drawing.Point(188, 26)
		Me.Label11.TabIndex = 42
		Me.Label11.Tag = "16"
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label11.BackColor = System.Drawing.SystemColors.Control
		Me.Label11.Enabled = True
		Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label11.UseMnemonic = True
		Me.Label11.Visible = True
		Me.Label11.AutoSize = False
		Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label11.Name = "Label11"
		Me.Label10.Text = "F2"
		Me.Label10.Size = New System.Drawing.Size(25, 15)
		Me.Label10.Location = New System.Drawing.Point(14, 50)
		Me.Label10.TabIndex = 40
		Me.Label10.Tag = "15"
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label10.BackColor = System.Drawing.SystemColors.Control
		Me.Label10.Enabled = True
		Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label10.UseMnemonic = True
		Me.Label10.Visible = True
		Me.Label10.AutoSize = False
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label10.Name = "Label10"
		Me.Label9.Text = "F1"
		Me.Label9.Size = New System.Drawing.Size(25, 15)
		Me.Label9.Location = New System.Drawing.Point(14, 26)
		Me.Label9.TabIndex = 38
		Me.Label9.Tag = "14"
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label9.BackColor = System.Drawing.SystemColors.Control
		Me.Label9.Enabled = True
		Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.UseMnemonic = True
		Me.Label9.Visible = True
		Me.Label9.AutoSize = False
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.Name = "Label9"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(292, 19)
		Me.Text3.Location = New System.Drawing.Point(226, 178)
		Me.Text3.TabIndex = 15
		Me.Text3.Tag = "^13"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtTecnico.AutoSize = False
		Me.txtTecnico.Size = New System.Drawing.Size(99, 19)
		Me.txtTecnico.Location = New System.Drawing.Point(124, 178)
		Me.txtTecnico.Maxlength = 8
		Me.txtTecnico.TabIndex = 14
		Me.txtTecnico.Tag = "13"
		Me.txtTecnico.AcceptsReturn = True
		Me.txtTecnico.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTecnico.BackColor = System.Drawing.SystemColors.Window
		Me.txtTecnico.CausesValidation = True
		Me.txtTecnico.Enabled = True
		Me.txtTecnico.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTecnico.HideSelection = True
		Me.txtTecnico.ReadOnly = False
		Me.txtTecnico.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTecnico.MultiLine = False
		Me.txtTecnico.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTecnico.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTecnico.TabStop = True
		Me.txtTecnico.Visible = True
		Me.txtTecnico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTecnico.Name = "txtTecnico"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(372, 19)
		Me.Text6.Location = New System.Drawing.Point(148, 130)
		Me.Text6.TabIndex = 34
		Me.Text6.Tag = "^5"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtMotivoBaja.AutoSize = False
		Me.txtMotivoBaja.Size = New System.Drawing.Size(21, 19)
		Me.txtMotivoBaja.Location = New System.Drawing.Point(124, 130)
		Me.txtMotivoBaja.Maxlength = 2
		Me.txtMotivoBaja.TabIndex = 11
		Me.txtMotivoBaja.Tag = "5"
		Me.txtMotivoBaja.AcceptsReturn = True
		Me.txtMotivoBaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMotivoBaja.BackColor = System.Drawing.SystemColors.Window
		Me.txtMotivoBaja.CausesValidation = True
		Me.txtMotivoBaja.Enabled = True
		Me.txtMotivoBaja.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMotivoBaja.HideSelection = True
		Me.txtMotivoBaja.ReadOnly = False
		Me.txtMotivoBaja.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMotivoBaja.MultiLine = False
		Me.txtMotivoBaja.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMotivoBaja.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMotivoBaja.TabStop = True
		Me.txtMotivoBaja.Visible = True
		Me.txtMotivoBaja.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMotivoBaja.Name = "txtMotivoBaja"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(372, 19)
		Me.Text4.Location = New System.Drawing.Point(148, 82)
		Me.Text4.TabIndex = 26
		Me.Text4.Tag = "^7"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtMotivoAlta.AutoSize = False
		Me.txtMotivoAlta.Size = New System.Drawing.Size(21, 19)
		Me.txtMotivoAlta.Location = New System.Drawing.Point(124, 82)
		Me.txtMotivoAlta.Maxlength = 2
		Me.txtMotivoAlta.TabIndex = 8
		Me.txtMotivoAlta.Tag = "7"
		Me.txtMotivoAlta.AcceptsReturn = True
		Me.txtMotivoAlta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMotivoAlta.BackColor = System.Drawing.SystemColors.Window
		Me.txtMotivoAlta.CausesValidation = True
		Me.txtMotivoAlta.Enabled = True
		Me.txtMotivoAlta.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMotivoAlta.HideSelection = True
		Me.txtMotivoAlta.ReadOnly = False
		Me.txtMotivoAlta.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMotivoAlta.MultiLine = False
		Me.txtMotivoAlta.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMotivoAlta.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMotivoAlta.TabStop = True
		Me.txtMotivoAlta.Visible = True
		Me.txtMotivoAlta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMotivoAlta.Name = "txtMotivoAlta"
		Me.txtUsuario.AutoSize = False
		Me.txtUsuario.Size = New System.Drawing.Size(42, 19)
		Me.txtUsuario.Location = New System.Drawing.Point(124, 10)
		Me.txtUsuario.Maxlength = 4
		Me.txtUsuario.TabIndex = 1
		Me.txtUsuario.Tag = "*1"
		Me.txtUsuario.AcceptsReturn = True
		Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsuario.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsuario.CausesValidation = True
		Me.txtUsuario.Enabled = True
		Me.txtUsuario.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsuario.HideSelection = True
		Me.txtUsuario.ReadOnly = False
		Me.txtUsuario.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsuario.MultiLine = False
		Me.txtUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsuario.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsuario.TabStop = True
		Me.txtUsuario.Visible = True
		Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUsuario.Name = "txtUsuario"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(352, 19)
		Me.Text1.Location = New System.Drawing.Point(169, 10)
		Me.Text1.TabIndex = 2
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtServicio.AutoSize = False
		Me.txtServicio.Size = New System.Drawing.Size(21, 19)
		Me.txtServicio.Location = New System.Drawing.Point(124, 34)
		Me.txtServicio.Maxlength = 2
		Me.txtServicio.TabIndex = 4
		Me.txtServicio.Tag = "*2"
		Me.txtServicio.AcceptsReturn = True
		Me.txtServicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtServicio.BackColor = System.Drawing.SystemColors.Window
		Me.txtServicio.CausesValidation = True
		Me.txtServicio.Enabled = True
		Me.txtServicio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtServicio.HideSelection = True
		Me.txtServicio.ReadOnly = False
		Me.txtServicio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtServicio.MultiLine = False
		Me.txtServicio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtServicio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtServicio.TabStop = True
		Me.txtServicio.Visible = True
		Me.txtServicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtServicio.Name = "txtServicio"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(372, 19)
		Me.Text2.Location = New System.Drawing.Point(148, 34)
		Me.Text2.TabIndex = 5
		Me.Text2.Tag = "^2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		txtFechaAlta.OcxState = CType(resources.GetObject("txtFechaAlta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaAlta.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaAlta.Location = New System.Drawing.Point(124, 58)
		Me.txtFechaAlta.TabIndex = 7
		Me.txtFechaAlta.Name = "txtFechaAlta"
		txtFechaBaja.OcxState = CType(resources.GetObject("txtFechaBaja.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaBaja.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaBaja.Location = New System.Drawing.Point(124, 106)
		Me.txtFechaBaja.TabIndex = 10
		Me.txtFechaBaja.Name = "txtFechaBaja"
		Me.txtPuntControl.AutoSize = False
		Me.txtPuntControl.Size = New System.Drawing.Size(21, 19)
		Me.txtPuntControl.Location = New System.Drawing.Point(124, 154)
		Me.txtPuntControl.Maxlength = 2
		Me.txtPuntControl.TabIndex = 13
		Me.txtPuntControl.Tag = "12"
		Me.txtPuntControl.AcceptsReturn = True
		Me.txtPuntControl.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPuntControl.BackColor = System.Drawing.SystemColors.Window
		Me.txtPuntControl.CausesValidation = True
		Me.txtPuntControl.Enabled = True
		Me.txtPuntControl.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPuntControl.HideSelection = True
		Me.txtPuntControl.ReadOnly = False
		Me.txtPuntControl.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPuntControl.MultiLine = False
		Me.txtPuntControl.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPuntControl.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPuntControl.TabStop = True
		Me.txtPuntControl.Visible = True
		Me.txtPuntControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPuntControl.Name = "txtPuntControl"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(372, 19)
		Me.Text5.Location = New System.Drawing.Point(148, 154)
		Me.Text5.TabIndex = 16
		Me.Text5.Tag = "^12"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtObservaciones.AutoSize = False
		Me.txtObservaciones.Size = New System.Drawing.Size(397, 55)
		Me.txtObservaciones.Location = New System.Drawing.Point(124, 202)
		Me.txtObservaciones.Maxlength = 100
		Me.txtObservaciones.MultiLine = True
		Me.txtObservaciones.TabIndex = 18
		Me.txtObservaciones.Tag = "6"
		Me.txtObservaciones.AcceptsReturn = True
		Me.txtObservaciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservaciones.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservaciones.CausesValidation = True
		Me.txtObservaciones.Enabled = True
		Me.txtObservaciones.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservaciones.HideSelection = True
		Me.txtObservaciones.ReadOnly = False
		Me.txtObservaciones.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservaciones.TabStop = True
		Me.txtObservaciones.Visible = True
		Me.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservaciones.Name = "txtObservaciones"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(349, 558)
		Me.cmdAceptar.TabIndex = 23
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(439, 558)
		Me.cmdGuardar.TabIndex = 24
		Me.cmdGuardar.Name = "cmdGuardar"
		grdProgrames.OcxState = CType(resources.GetObject("grdProgrames.OcxState"), System.Windows.Forms.AxHost.State)
		Me.grdProgrames.Size = New System.Drawing.Size(504, 111)
		Me.grdProgrames.Location = New System.Drawing.Point(16, 428)
		Me.grdProgrames.TabIndex = 28
		Me.grdProgrames.Name = "grdProgrames"
		txtFechaRecogidaDatos.OcxState = CType(resources.GetObject("txtFechaRecogidaDatos.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaRecogidaDatos.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaRecogidaDatos.Location = New System.Drawing.Point(146, 268)
		Me.txtFechaRecogidaDatos.TabIndex = 19
		Me.txtFechaRecogidaDatos.Name = "txtFechaRecogidaDatos"
		txtFechaValoracion.OcxState = CType(resources.GetObject("txtFechaValoracion.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaValoracion.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaValoracion.Location = New System.Drawing.Point(434, 268)
		Me.txtFechaValoracion.TabIndex = 20
		Me.txtFechaValoracion.Name = "txtFechaValoracion"
		txtFechaPrepLaboral.OcxState = CType(resources.GetObject("txtFechaPrepLaboral.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaPrepLaboral.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaPrepLaboral.Location = New System.Drawing.Point(146, 292)
		Me.txtFechaPrepLaboral.TabIndex = 21
		Me.txtFechaPrepLaboral.Name = "txtFechaPrepLaboral"
		txtFechaIntermediacion.OcxState = CType(resources.GetObject("txtFechaIntermediacion.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaIntermediacion.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaIntermediacion.Location = New System.Drawing.Point(434, 292)
		Me.txtFechaIntermediacion.TabIndex = 22
		Me.txtFechaIntermediacion.Name = "txtFechaIntermediacion"
		Me.Label8.Text = "T�cnico"
		Me.Label8.Size = New System.Drawing.Size(111, 15)
		Me.Label8.Location = New System.Drawing.Point(10, 182)
		Me.Label8.TabIndex = 36
		Me.Label8.Tag = "13"
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label8.BackColor = System.Drawing.SystemColors.Control
		Me.Label8.Enabled = True
		Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.UseMnemonic = True
		Me.Label8.Visible = True
		Me.Label8.AutoSize = False
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.Name = "Label8"
		Me.Label7.Text = "Motivo baja"
		Me.Label7.Size = New System.Drawing.Size(111, 15)
		Me.Label7.Location = New System.Drawing.Point(10, 134)
		Me.Label7.TabIndex = 35
		Me.Label7.Tag = "5"
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label7.BackColor = System.Drawing.SystemColors.Control
		Me.Label7.Enabled = True
		Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.UseMnemonic = True
		Me.Label7.Visible = True
		Me.Label7.AutoSize = False
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.Name = "Label7"
		Me.Label6.Text = "Fecha intermediaci�n"
		Me.Label6.Size = New System.Drawing.Size(111, 15)
		Me.Label6.Location = New System.Drawing.Point(320, 296)
		Me.Label6.TabIndex = 33
		Me.Label6.Tag = "11"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.Label5.Text = "Fecha preparaci�n laboral"
		Me.Label5.Size = New System.Drawing.Size(131, 15)
		Me.Label5.Location = New System.Drawing.Point(10, 296)
		Me.Label5.TabIndex = 32
		Me.Label5.Tag = "10"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label4.Text = "Fecha valoraci�n"
		Me.Label4.Size = New System.Drawing.Size(111, 15)
		Me.Label4.Location = New System.Drawing.Point(320, 272)
		Me.Label4.TabIndex = 31
		Me.Label4.Tag = "9"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label3.Text = "Fecha recogida de datos"
		Me.Label3.Size = New System.Drawing.Size(131, 15)
		Me.Label3.Location = New System.Drawing.Point(10, 272)
		Me.Label3.TabIndex = 30
		Me.Label3.Tag = "8"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Programes"
		Me.Label2.ForeColor = System.Drawing.Color.Blue
		Me.Label2.Size = New System.Drawing.Size(93, 15)
		Me.Label2.Location = New System.Drawing.Point(18, 408)
		Me.Label2.TabIndex = 29
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Motivo alta"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 86)
		Me.Label1.TabIndex = 27
		Me.Label1.Tag = "7"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Usuario"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Servicio"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 3
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Fecha alta"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 6
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Fecha baja"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 110)
		Me.lbl4.TabIndex = 9
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Punt Control"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 158)
		Me.lbl5.TabIndex = 12
		Me.lbl5.Tag = "12"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Observaciones"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 206)
		Me.lbl6.TabIndex = 17
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 557)
		Me.lblLock.TabIndex = 25
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 551
		Me.Line1.Y2 = 551
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 552
		Me.Line2.Y2 = 552
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.txtFechaIntermediacion, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaPrepLaboral, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaValoracion, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaRecogidaDatos, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.grdProgrames, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaAlta, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.FrameNovesOp, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtF4, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtF3, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtF2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtF1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(FrameNovesOp)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtTecnico)
		Me.Controls.Add(Text6)
		Me.Controls.Add(txtMotivoBaja)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtMotivoAlta)
		Me.Controls.Add(txtUsuario)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtServicio)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtFechaAlta)
		Me.Controls.Add(txtFechaBaja)
		Me.Controls.Add(txtPuntControl)
		Me.Controls.Add(Text5)
		Me.Controls.Add(txtObservaciones)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(grdProgrames)
		Me.Controls.Add(txtFechaRecogidaDatos)
		Me.Controls.Add(txtFechaValoracion)
		Me.Controls.Add(txtFechaPrepLaboral)
		Me.Controls.Add(txtFechaIntermediacion)
		Me.Controls.Add(Label8)
		Me.Controls.Add(Label7)
		Me.Controls.Add(Label6)
		Me.Controls.Add(Label5)
		Me.Controls.Add(Label4)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.FrameNovesOp.Controls.Add(txtF1)
		Me.FrameNovesOp.Controls.Add(txtF2)
		Me.FrameNovesOp.Controls.Add(txtF3)
		Me.FrameNovesOp.Controls.Add(txtF4)
		Me.FrameNovesOp.Controls.Add(Label12)
		Me.FrameNovesOp.Controls.Add(Label11)
		Me.FrameNovesOp.Controls.Add(Label10)
		Me.FrameNovesOp.Controls.Add(Label9)
		Me.FrameNovesOp.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class