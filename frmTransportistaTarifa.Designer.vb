<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmTransportistaTarifa
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtTransportista As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtFixT As System.Windows.Forms.TextBox
	Public WithEvents txtTarifa As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtFecha As AxDataControl.AxGmsData
	Public WithEvents txtImporte As AxDataControl.AxGmsImports
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTransportistaTarifa))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtTransportista = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtFixT = New System.Windows.Forms.TextBox
		Me.txtTarifa = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtFecha = New AxDataControl.AxGmsData
		Me.txtImporte = New AxDataControl.AxGmsImports
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtFecha, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtImporte, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Transportista tarifa"
		Me.ClientSize = New System.Drawing.Size(471, 157)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "T-TRANSPORTISTA_TARIFA"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmTransportistaTarifa"
		Me.txtTransportista.AutoSize = False
		Me.txtTransportista.Size = New System.Drawing.Size(21, 19)
		Me.txtTransportista.Location = New System.Drawing.Point(124, 10)
		Me.txtTransportista.Maxlength = 2
		Me.txtTransportista.TabIndex = 1
		Me.txtTransportista.Tag = "*1"
		Me.txtTransportista.AcceptsReturn = True
		Me.txtTransportista.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTransportista.BackColor = System.Drawing.SystemColors.Window
		Me.txtTransportista.CausesValidation = True
		Me.txtTransportista.Enabled = True
		Me.txtTransportista.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTransportista.HideSelection = True
		Me.txtTransportista.ReadOnly = False
		Me.txtTransportista.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTransportista.MultiLine = False
		Me.txtTransportista.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTransportista.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTransportista.TabStop = True
		Me.txtTransportista.Visible = True
		Me.txtTransportista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTransportista.Name = "txtTransportista"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(314, 19)
		Me.Text1.Location = New System.Drawing.Point(148, 10)
		Me.Text1.TabIndex = 2
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtFixT.AutoSize = False
		Me.txtFixT.BackColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.txtFixT.Size = New System.Drawing.Size(11, 19)
		Me.txtFixT.Location = New System.Drawing.Point(428, 56)
		Me.txtFixT.Maxlength = 1
		Me.txtFixT.TabIndex = 4
		Me.txtFixT.Tag = "*2"
		Me.txtFixT.AcceptsReturn = True
		Me.txtFixT.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFixT.CausesValidation = True
		Me.txtFixT.Enabled = True
		Me.txtFixT.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFixT.HideSelection = True
		Me.txtFixT.ReadOnly = False
		Me.txtFixT.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFixT.MultiLine = False
		Me.txtFixT.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFixT.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFixT.TabStop = True
		Me.txtFixT.Visible = True
		Me.txtFixT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFixT.Name = "txtFixT"
		Me.txtTarifa.AutoSize = False
		Me.txtTarifa.Size = New System.Drawing.Size(20, 19)
		Me.txtTarifa.Location = New System.Drawing.Point(124, 34)
		Me.txtTarifa.Maxlength = 1
		Me.txtTarifa.TabIndex = 6
		Me.txtTarifa.Tag = "*3"
		Me.txtTarifa.AcceptsReturn = True
		Me.txtTarifa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTarifa.BackColor = System.Drawing.SystemColors.Window
		Me.txtTarifa.CausesValidation = True
		Me.txtTarifa.Enabled = True
		Me.txtTarifa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTarifa.HideSelection = True
		Me.txtTarifa.ReadOnly = False
		Me.txtTarifa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTarifa.MultiLine = False
		Me.txtTarifa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTarifa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTarifa.TabStop = True
		Me.txtTarifa.Visible = True
		Me.txtTarifa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTarifa.Name = "txtTarifa"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(314, 19)
		Me.Text3.Location = New System.Drawing.Point(148, 34)
		Me.Text3.TabIndex = 7
		Me.Text3.Tag = "^3"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		txtFecha.OcxState = CType(resources.GetObject("txtFecha.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFecha.Size = New System.Drawing.Size(87, 19)
		Me.txtFecha.Location = New System.Drawing.Point(124, 58)
		Me.txtFecha.TabIndex = 9
		Me.txtFecha.Name = "txtFecha"
		txtImporte.OcxState = CType(resources.GetObject("txtImporte.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtImporte.Size = New System.Drawing.Size(83, 19)
		Me.txtImporte.Location = New System.Drawing.Point(124, 82)
		Me.txtImporte.TabIndex = 11
		Me.txtImporte.Name = "txtImporte"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(293, 124)
		Me.cmdAceptar.TabIndex = 12
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(383, 124)
		Me.cmdGuardar.TabIndex = 13
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl1.Text = "Transportista"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Fix 't'"
		Me.lbl2.Size = New System.Drawing.Size(25, 15)
		Me.lbl2.Location = New System.Drawing.Point(398, 60)
		Me.lbl2.TabIndex = 3
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Tarifa"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 38)
		Me.lbl3.TabIndex = 5
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Fecha"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 62)
		Me.lbl4.TabIndex = 8
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Importe"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 86)
		Me.lbl5.TabIndex = 10
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 123)
		Me.lblLock.TabIndex = 14
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 117
		Me.Line1.Y2 = 117
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 118
		Me.Line2.Y2 = 118
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtImporte, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFecha, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtTransportista)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtFixT)
		Me.Controls.Add(txtTarifa)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtFecha)
		Me.Controls.Add(txtImporte)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class