Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmGestorsDeTransport
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	
	
	Private Sub chkPotModificarPreus_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPotModificarPreus.GotFocus
		XGotFocus(Me, chkPotModificarPreus)
	End Sub
	
	Private Sub chkPotModificarPreus_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPotModificarPreus.LostFocus
		XLostFocus(Me, chkPotModificarPreus)
	End Sub
	
	Private Sub frmGestorsDeTransport_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmGestorsDeTransport_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmGestorsDeTransport_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmGestorsDeTransport_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub cmbTipo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipo.GotFocus
		XGotFocus(Me, cmbTipo)
	End Sub
	
	Private Sub cmbTipo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbTipo.LostFocus
		If cmbTipo.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, cmbTipo)
	End Sub
	
	Private Sub txtResponsable_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.GotFocus
		XGotFocus(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.DoubleClick
		ConsultaTaula(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.LostFocus
		If txtResponsable.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtResponsable.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtMail_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMail.GotFocus
		XGotFocus(Me, txtMail)
	End Sub
	
	Private Sub txtMail_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMail.LostFocus
		XLostFocus(Me, txtMail)
	End Sub
	
	Private Sub txtMail_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMail.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.GotFocus
		XGotFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtFechaBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.LostFocus
		XLostFocus(Me, txtFechaBaja)
	End Sub
End Class
