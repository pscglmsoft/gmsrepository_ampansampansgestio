Option Strict Off
Option Explicit On
Friend Class FrmVerificacioFaixes
	Inherits FormParent
	
	Private Sub cmdNovaVerificacio_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNovaVerificacio.ClickEvent
		txtFaixa.Enabled = True
		Inici()
	End Sub
	
	Private Sub FrmVerificacioFaixes_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		CentrarForm(Me)
		cmdNovaVerificacio.Picture = SetIcon("Genera", 24)
	End Sub
	
Overrides 	Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub FrmVerificacioFaixes_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub FrmVerificacioFaixes_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtFaixa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFaixa.LostFocus
		txtFaixa.Enabled = False
	End Sub
	
	'UPGRADE_WARNING: El evento txtFaixaCompara.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtFaixaCompara_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFaixaCompara.TextChanged
		Comprova()
	End Sub
	
	Private Sub Comprova()
		On Error Resume Next
		If (Len(txtFaixa.Text) = 13) And (Len(txtFaixaCompara.Text) = 13) Then 'poner 13
			If txtFaixa.Text <> "" And txtFaixaCompara.Text <> "" Then
				If txtFaixa.Text = txtFaixaCompara.Text Then
					Retard()
					frmIguals.ShowDialog()
				Else
					Retard()
					frmDiferents.ShowDialog()
				End If
				
			End If
		End If
		
	End Sub
End Class
