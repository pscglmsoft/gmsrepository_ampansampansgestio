Option Strict Off
Option Explicit On
Friend Class frmPCobrar
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	'UPGRADE_WARNING: Form evento frmPCobrar.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmPCobrar_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaPCobrament()
	End Sub
	
	Private Sub frmPCobrar_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmPCobrar_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		CarregaPCobrament()
	End Sub
	
	Sub CarregaPCobrament()
		CarregaFGrid(GrdPCobrament, "PCOBRAMENT^VARIS")
	End Sub
	
	Private Sub GrdPCobrament_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdPCobrament.DoubleClick
		If GrdPCobrament.ActiveCell.Row = 0 Then Exit Sub
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmLoteria, "frmLoteria", Piece(GrdPCobrament.Cell(GrdPCobrament.ActiveCell.Row, 1).Text, "|", 1))
		frmLoteria.txtDataPagat.Focus()
		'    If frmLoteria.txtDataPagat <> "" Then
		'        frmLoteria.txtImportBanc.Enabled = True
		'        frmLoteria.txtImportEfectiu.Enabled = True
		'    End If
		
	End Sub
	
	Private Sub GrdPCobrament_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdPCobrament.MouseUp
		
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdPCobrament", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Cobrar Loteria",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdPCobrament.Rows > 1 And PermisConsulta("frmLoteria", e_Permisos.Consultes)
			End With
		End With
		XpExecutaMenu(Me, "GrdPCobrament", eventArgs.X, eventArgs.Y)
	End Sub
	
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Select Case NomMenu
			Case "GrdPCobrament"
				Select Case KeyMenu
					Case "E"
						If GrdPCobrament.ActiveCell.Row = 0 Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmLoteria, "frmLoteria", Piece(GrdPCobrament.Cell(GrdPCobrament.ActiveCell.Row, 1).Text, "|", 1))
						frmLoteria.txtDataPagat.Focus()
						'                    If frmLoteria.txtDataPagat <> "" Then
						'                        frmLoteria.txtImportBanc.Enabled = True
						'                        frmLoteria.txtImportEfectiu.Enabled = True
						'                    End If
				End Select
		End Select
	End Sub
End Class
