Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmTipusRelacions
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Public Overrides Sub FGetReg()
		CarregaSubtipusRel()
	End Sub
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		CarregaSubtipusRel()
	End Sub
	
	'UPGRADE_WARNING: Form evento frmTipusRelacions.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmTipusRelacions_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaSubtipusRel()
	End Sub
	
	Private Sub frmTipusRelacions_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmTipusRelacions_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmTipusRelacions_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmTipusRelacions_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub GrdSubTipus_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdSubTipus.DoubleClick
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmSubtipusRelacions, "frmSubtipusRelacions", txtCodigo.Text & S & Piece(GrdSubTipus.Cell(GrdSubTipus.ActiveCell.Row, 1).Text, "|", 2))
	End Sub
	
	Private Sub GrdSubTipus_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdSubTipus.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtCodigo.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdSubTipus", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nuevo subtipo relaci�n",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmSubtipusRelacions", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar subtipo relaci�n",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdSubTipus.Rows > 1 And PermisConsulta("frmSubtipusRelacions", e_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar subtipo relaci�n",  ,  , XPIcon("Delete"),  ,  ,  ,  , "B")
				.MenuItems.Item("B").Enabled = GrdSubTipus.Rows > 1 And PermisConsulta("frmSubtipusRelacions", e_Permisos.Baixes)
			End With
		End With
		XpExecutaMenu(Me, "GrdSubTipus", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub txtCodigo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.GotFocus
		XGotFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.DoubleClick
		ConsultaTaula(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.LostFocus
		If txtCodigo.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodigo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcion.GotFocus
		XGotFocus(Me, txtDescripcion)
	End Sub
	
	Private Sub txtDescripcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcion.LostFocus
		XLostFocus(Me, txtDescripcion)
	End Sub
	
	Private Sub txtDescripcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.GotFocus
		XGotFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtFechaBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.LostFocus
		XLostFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub CarregaSubtipusRel()
		CarregaFGrid(GrdSubTipus, "SUBTIPUSR^INSERCIO", txtCodigo.Text)
	End Sub
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Dim intColOrdAux As Short
		Select Case NomMenu
			Case "GrdSubTipus"
				Select Case KeyMenu
					Case "N"
						With frmSubtipusRelacions
							'.blnEntExterna = True
							.txtTipoRelacion.Text = txtCodigo.Text
							.txtTipoRelacion.Enabled = False
							.Show()
						End With
					Case "E"
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmSubtipusRelacions, "frmSubtipusRelacions", txtCodigo.Text & S & Piece(GrdSubTipus.Cell(GrdSubTipus.ActiveCell.Row, 1).Text, "|", 2))
					Case "B"
						A = GrdSubTipus.Cell(GrdSubTipus.ActiveCell.Row, 2).Text
						If A = "" Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmSubtipusRelacions, "frmSubtipusRelacions", txtCodigo.Text & S & Piece(GrdSubTipus.Cell(GrdSubTipus.ActiveCell.Row, 1).Text, "|", 2))
						LlibAplicacio.DeleteReg(frmSubtipusRelacions   )
						frmSubtipusRelacions.Unload()
						
				End Select
				
		End Select
		
	End Sub
End Class
