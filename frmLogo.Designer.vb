<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLogo
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents _picSplash_1 As System.Windows.Forms.PictureBox
	Public WithEvents _picConexio_1 As System.Windows.Forms.PictureBox
	Public WithEvents _picConexio_2 As System.Windows.Forms.PictureBox
	Public WithEvents _picSplash_2 As System.Windows.Forms.PictureBox
	Public WithEvents picConexio As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	Public WithEvents picSplash As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLogo))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me._picSplash_1 = New System.Windows.Forms.PictureBox
		Me._picConexio_1 = New System.Windows.Forms.PictureBox
		Me._picConexio_2 = New System.Windows.Forms.PictureBox
		Me._picSplash_2 = New System.Windows.Forms.PictureBox
		Me.picConexio = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(components)
		Me.picSplash = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(components)
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.picConexio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picSplash, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
		Me.ClientSize = New System.Drawing.Size(938, 548)
		Me.Location = New System.Drawing.Point(220, 348)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.Visible = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmLogo"
		Me._picSplash_1.ForeColor = System.Drawing.Color.White
		Me._picSplash_1.Size = New System.Drawing.Size(500, 327)
		Me._picSplash_1.Location = New System.Drawing.Point(354, 164)
		Me._picSplash_1.Image = CType(resources.GetObject("_picSplash_1.Image"), System.Drawing.Image)
		Me._picSplash_1.TabIndex = 3
		Me._picSplash_1.Dock = System.Windows.Forms.DockStyle.None
		Me._picSplash_1.BackColor = System.Drawing.SystemColors.Control
		Me._picSplash_1.CausesValidation = True
		Me._picSplash_1.Enabled = True
		Me._picSplash_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._picSplash_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._picSplash_1.TabStop = True
		Me._picSplash_1.Visible = True
		Me._picSplash_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
		Me._picSplash_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._picSplash_1.Name = "_picSplash_1"
		Me._picConexio_1.ForeColor = System.Drawing.Color.White
		Me._picConexio_1.Size = New System.Drawing.Size(500, 327)
		Me._picConexio_1.Location = New System.Drawing.Point(132, 72)
		Me._picConexio_1.Image = CType(resources.GetObject("_picConexio_1.Image"), System.Drawing.Image)
		Me._picConexio_1.TabIndex = 0
		Me._picConexio_1.Dock = System.Windows.Forms.DockStyle.None
		Me._picConexio_1.BackColor = System.Drawing.SystemColors.Control
		Me._picConexio_1.CausesValidation = True
		Me._picConexio_1.Enabled = True
		Me._picConexio_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._picConexio_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._picConexio_1.TabStop = True
		Me._picConexio_1.Visible = True
		Me._picConexio_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
		Me._picConexio_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._picConexio_1.Name = "_picConexio_1"
		Me._picConexio_2.ForeColor = System.Drawing.Color.White
		Me._picConexio_2.Size = New System.Drawing.Size(500, 327)
		Me._picConexio_2.Location = New System.Drawing.Point(8, 16)
		Me._picConexio_2.Image = CType(resources.GetObject("_picConexio_2.Image"), System.Drawing.Image)
		Me._picConexio_2.TabIndex = 1
		Me._picConexio_2.Dock = System.Windows.Forms.DockStyle.None
		Me._picConexio_2.BackColor = System.Drawing.SystemColors.Control
		Me._picConexio_2.CausesValidation = True
		Me._picConexio_2.Enabled = True
		Me._picConexio_2.Cursor = System.Windows.Forms.Cursors.Default
		Me._picConexio_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._picConexio_2.TabStop = True
		Me._picConexio_2.Visible = True
		Me._picConexio_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
		Me._picConexio_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._picConexio_2.Name = "_picConexio_2"
		Me._picSplash_2.ForeColor = System.Drawing.Color.White
		Me._picSplash_2.Size = New System.Drawing.Size(500, 327)
		Me._picSplash_2.Location = New System.Drawing.Point(426, 36)
		Me._picSplash_2.Image = CType(resources.GetObject("_picSplash_2.Image"), System.Drawing.Image)
		Me._picSplash_2.TabIndex = 2
		Me._picSplash_2.Dock = System.Windows.Forms.DockStyle.None
		Me._picSplash_2.BackColor = System.Drawing.SystemColors.Control
		Me._picSplash_2.CausesValidation = True
		Me._picSplash_2.Enabled = True
		Me._picSplash_2.Cursor = System.Windows.Forms.Cursors.Default
		Me._picSplash_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._picSplash_2.TabStop = True
		Me._picSplash_2.Visible = True
		Me._picSplash_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
		Me._picSplash_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._picSplash_2.Name = "_picSplash_2"
		Me.Controls.Add(_picSplash_1)
		Me.Controls.Add(_picConexio_1)
		Me.Controls.Add(_picConexio_2)
		Me.Controls.Add(_picSplash_2)
		Me.picConexio.SetIndex(_picConexio_1, CType(1, Short))
		Me.picConexio.SetIndex(_picConexio_2, CType(2, Short))
		Me.picSplash.SetIndex(_picSplash_1, CType(1, Short))
		Me.picSplash.SetIndex(_picSplash_2, CType(2, Short))
		CType(Me.picSplash, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.picConexio, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class