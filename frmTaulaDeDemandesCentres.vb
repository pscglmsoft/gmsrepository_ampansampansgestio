Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmTaulaDeDemandesCentres
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	Private Sub frmTaulaDeDemandesCentres_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If ComprovaDades = False Then Exit Sub
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If ComprovaDades = False Then Exit Sub
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmTaulaDeDemandesCentres_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmTaulaDeDemandesCentres_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmTaulaDeDemandesCentres_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCodi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.GotFocus
		XGotFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.DoubleClick
		ConsultaTaula(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.LostFocus
		If txtCodi.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtCodi)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub FixeC_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles FixeC.GotFocus
		XGotFocus(Me, FixeC)
	End Sub
	
	Private Sub FixeC_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles FixeC.Leave
		If FixeC.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, FixeC)
	End Sub
	
	Private Sub FixeC_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles FixeC.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, FixeC)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCentre_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentre.GotFocus
		XGotFocus(Me, txtCentre)
	End Sub
	
	Private Sub txtCentre_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentre.DoubleClick
		ConsultaTaula(Me, txtCentre)
	End Sub
	
	Private Sub txtCentre_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentre.LostFocus
		If txtCentre.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCentre)
	End Sub
	
	Private Sub txtCentre_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentre.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtCentre)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtCentre)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtEmail_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmail.GotFocus
		XGotFocus(Me, txtEmail)
	End Sub
	
	Private Sub txtEmail_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmail.LostFocus
		XLostFocus(Me, txtEmail)
	End Sub
	
	Private Sub txtUsuariEntigest_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuariEntigest.GotFocus
		XGotFocus(Me, txtUsuariEntigest)
	End Sub
	
	Private Sub txtUsuariEntigest_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuariEntigest.DoubleClick
		ConsultaTaula(Me, txtUsuariEntigest)
	End Sub
	
	Private Sub txtUsuariEntigest_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuariEntigest.LostFocus
		XLostFocus(Me, txtUsuariEntigest)
	End Sub
	
	Private Sub txtUsuariEntigest_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUsuariEntigest.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtUsuariEntigest)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtUsuariEntigest)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Function ComprovaDades() As Boolean
		ComprovaDades = True
		
		If txtUsuariEntigest.Text = "" Then
			xMsgBox("Falta informar l'usuari de EntiGest", MsgBoxStyle.Information, Me.Text)
			txtUsuariEntigest.Focus()
			ComprovaDades = False
			Exit Function
		End If
		
		If txtEmail.Text = "" Then
			xMsgBox("Falta informar el Mail", MsgBoxStyle.Information, Me.Text)
			txtEmail.Focus()
			ComprovaDades = False
			Exit Function
		End If
		
		
		
	End Function
End Class
