<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLoteriaPEntrega
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents GrdPEntrega As AxFlexCell.AxGrid
	Public WithEvents txtDataEntrega As AxDataControl.AxGmsData
	Public WithEvents cmdLliurar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdLliurarImprimir As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl11 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLoteriaPEntrega))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.GrdPEntrega = New AxFlexCell.AxGrid
		Me.txtDataEntrega = New AxDataControl.AxGmsData
		Me.cmdLliurar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdLliurarImprimir = New AxXtremeSuiteControls.AxPushButton
		Me.lbl11 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.GrdPEntrega, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataEntrega, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdLliurar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdLliurarImprimir, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "LOTERIA PENDENT LLIURAR"
		Me.ClientSize = New System.Drawing.Size(772, 518)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmLoteriaPEntrega"
		GrdPEntrega.OcxState = CType(resources.GetObject("GrdPEntrega.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdPEntrega.Size = New System.Drawing.Size(758, 437)
		Me.GrdPEntrega.Location = New System.Drawing.Point(8, 44)
		Me.GrdPEntrega.TabIndex = 1
		Me.GrdPEntrega.Name = "GrdPEntrega"
		txtDataEntrega.OcxState = CType(resources.GetObject("txtDataEntrega.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataEntrega.Size = New System.Drawing.Size(87, 19)
		Me.txtDataEntrega.Location = New System.Drawing.Point(90, 10)
		Me.txtDataEntrega.TabIndex = 0
		Me.txtDataEntrega.Name = "txtDataEntrega"
		cmdLliurar.OcxState = CType(resources.GetObject("cmdLliurar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdLliurar.Size = New System.Drawing.Size(83, 27)
		Me.cmdLliurar.Location = New System.Drawing.Point(684, 486)
		Me.cmdLliurar.TabIndex = 3
		Me.cmdLliurar.Name = "cmdLliurar"
		cmdLliurarImprimir.OcxState = CType(resources.GetObject("cmdLliurarImprimir.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdLliurarImprimir.Size = New System.Drawing.Size(119, 27)
		Me.cmdLliurarImprimir.Location = New System.Drawing.Point(514, 486)
		Me.cmdLliurarImprimir.TabIndex = 4
		Me.cmdLliurarImprimir.Name = "cmdLliurarImprimir"
		Me.lbl11.Text = "Data entrega"
		Me.lbl11.Size = New System.Drawing.Size(111, 15)
		Me.lbl11.Location = New System.Drawing.Point(10, 14)
		Me.lbl11.TabIndex = 2
		Me.lbl11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl11.BackColor = System.Drawing.SystemColors.Control
		Me.lbl11.Enabled = True
		Me.lbl11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl11.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl11.UseMnemonic = True
		Me.lbl11.Visible = True
		Me.lbl11.AutoSize = False
		Me.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl11.Name = "lbl11"
		CType(Me.cmdLliurarImprimir, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdLliurar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataEntrega, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdPEntrega, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(GrdPEntrega)
		Me.Controls.Add(txtDataEntrega)
		Me.Controls.Add(cmdLliurar)
		Me.Controls.Add(cmdLliurarImprimir)
		Me.Controls.Add(lbl11)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class