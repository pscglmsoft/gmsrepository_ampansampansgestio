<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmParticipants
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents GrdParticipants As AxFlexCell.AxGrid
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdCancelar As AxXtremeSuiteControls.AxPushButton
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmParticipants))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.GrdParticipants = New AxFlexCell.AxGrid
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdCancelar = New AxXtremeSuiteControls.AxPushButton
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.GrdParticipants, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdCancelar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Text = "Participants"
		Me.ClientSize = New System.Drawing.Size(494, 534)
		Me.Location = New System.Drawing.Point(8, 30)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmParticipants"
		GrdParticipants.OcxState = CType(resources.GetObject("GrdParticipants.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdParticipants.Size = New System.Drawing.Size(476, 491)
		Me.GrdParticipants.Location = New System.Drawing.Point(8, 6)
		Me.GrdParticipants.TabIndex = 0
		Me.GrdParticipants.Name = "GrdParticipants"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(312, 502)
		Me.cmdAceptar.TabIndex = 1
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdCancelar.OcxState = CType(resources.GetObject("cmdCancelar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdCancelar.Size = New System.Drawing.Size(83, 27)
		Me.cmdCancelar.Location = New System.Drawing.Point(402, 502)
		Me.cmdCancelar.TabIndex = 2
		Me.cmdCancelar.Name = "cmdCancelar"
		Me.Controls.Add(GrdParticipants)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdCancelar)
		CType(Me.cmdCancelar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdParticipants, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class