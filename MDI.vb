Option Strict Off
Option Explicit On
Friend Class MDI
	Inherits FormParent
	Public XWord As New EventClassModule
	Public XExcel As New EventClassModuleX
	
	Private Sub CommandBars_Execute(sender As Object, e As AxXtremeCommandBars._DCommandBarsEvents_ExecuteEvent) Handles CommandBars.Execute
		ExecutaBotonsToolBarAmpans(CStr(Control.Id), Me.ActiveMDIChild)
	End Sub
	
	'UPGRADE_WARNING: Form evento MDI.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub MDI_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		frmSplash.Unload()
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
	End Sub
	
	Private Sub MDI_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		GrupModuls = "AMP" '"ENT"
		'GrupModuls = "ENT"
		Modul = "AMPANS" '"INFAM"   'INFORMATICA
		'Modul = "INFAM"
		Splash()
		MCache = CacheCub
		Wsock = New Net.Sockets.Socket(Net.Sockets.AddressFamily.InterNetwork, Net.Sockets.SocketType.Stream, Net.Sockets.ProtocolType.Tcp)
		'Set PopUp = gmsMenu1
		PopMenuXp = gmsPopUpCode.GmsPopUp
		Estil.Versio = e_Versio.Estil2007
		CarregaEstil()
		
		
		
		If Conecta() = False Then
			Me.Unload()
			Exit Sub
		End If
		VarEmpresa()
		'UPGRADE_ISSUE: App propiedad app.HelpFile no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		''MIGRARapp.HelpFile = PathExes & "General.chm"
		
		'cache object
		Ocache = New CacheActiveX.Factory
		
		MdiPicture()
	End Sub
	
	Sub ExecutaMenu(ByRef NomMenu As String)
		frmMenus.Enabled = False
		Select Case NomMenu
			' (CINS) Contractes/Pr�ctiques Inserci�
			' (CRM) CRM
			Case "FrmConsultaDadesEntitat" : FrmConsultaDadesEntitat.Show() : FrmConsultaDadesEntitat.Activate()
			Case "frmConsultaAccions" : frmConsultaAccions.Show() : frmConsultaAccions.Activate()
			Case "frmAccions" : frmAccions.Show() : frmAccions.Activate()
			Case "frmObjectius" : frmObjectius.Show() : frmObjectius.Activate()
			Case "frmPlans" : frmPlans.Show() : frmPlans.Activate()
			Case "frmMassiuPlans" : frmMassiuPlans.Show() : frmMassiuPlans.Activate()
			Case "frmCampanyes" : frmCampanyes.Show() : frmCampanyes.Activate()
			Case "frmMassiuCampanya" : frmMassiuCampanya.Show() : frmMassiuCampanya.Activate()
			Case "frmCampanyesNou" : frmCampanyesNou.Show() : frmCampanyesNou.Activate()
			Case "frmLlistatsAplicacio|1" : LlistatParametric(("CRM-INFORMACIO_ENTI_SUCUR"))
				' (ENT) Entidades
			Case "frmEntitats" : frmEntitats.Show() : frmEntitats.Activate()
			Case "frmGrupsEntitats" : frmGrupsEntitats.Show() : frmGrupsEntitats.Activate()
			Case "frmEntitatsContactes" : frmEntitatsContactes.Show() : frmEntitatsContactes.Activate()
				' (GATES) Gates
			Case "frmControlGates" : frmControlGates.Show() : frmControlGates.Activate()
			Case "frmLlistatsAplicacio|2" : LlistatParametric(("GATES"))
			Case "FrmVerificacioFaixes" : FrmVerificacioFaixes.Show() : FrmVerificacioFaixes.Activate()
			Case "frmLlistatsAplicacio|3" : LlistatParametric(("GATESF"))
				' (GDEM) Gesti� de Peticions
			Case "frmDemandesPendents" : frmDemandesPendents.Show() : frmDemandesPendents.Activate()
			Case "frmDemandes" : frmDemandes.Show() : frmDemandes.Activate()
			Case "frmReunions" : frmReunions.Show() : frmReunions.Activate()
				' (INS) Insercion Laboral
			Case "frmUsuariInsercioLaboral" : frmUsuariInsercioLaboral.Show() : frmUsuariInsercioLaboral.Activate()
			Case "frmHistorialLaboral" : frmHistorialLaboral.Show() : frmHistorialLaboral.Activate()
			Case "frmUsuariPreparacioLaboral" : frmUsuariPreparacioLaboral.Show() : frmUsuariPreparacioLaboral.Activate()
			Case "frmSeguimentUsuari" : frmSeguimentUsuari.Show() : frmSeguimentUsuari.Activate()
			Case "frmServeisUsuaris" : frmServeisUsuaris.Show() : frmServeisUsuaris.Activate()
			Case "frmUsuariPrograma" : frmUsuariPrograma.Show() : frmUsuariPrograma.Activate()
			Case "frmRelacions" : frmRelacions.Show() : frmRelacions.Activate()
			Case "frmUsuariInsercioFormacio" : frmUsuariInsercioFormacio.Show() : frmUsuariInsercioFormacio.Activate()
			Case "frmEmpresaInseridora" : frmEmpresaInseridora.Show() : frmEmpresaInseridora.Activate()
			Case "frmSessioFormativa" : frmSessioFormativa.Show() : frmSessioFormativa.Activate()
			Case "frmGestionsVaries" : frmGestionsVaries.Show() : frmGestionsVaries.Activate()
			Case "frmGestioDeOfertes" : frmGestioDeOfertes.Show() : frmGestioDeOfertes.Activate()
			Case "frmEntrevistesPresentats" : frmEntrevistesPresentats.Show() : frmEntrevistesPresentats.Activate()
				' (INV) Inventari
			Case "frmKilometratge" : frmKilometratge.Show() : frmKilometratge.Activate()
			Case "frmActualitzaRevisions" : frmActualitzaRevisions.Show() : frmActualitzaRevisions.Activate()
				' (LOTE) Loteria Ampans
			Case "frmLoteria" : frmLoteria.Show() : frmLoteria.Activate()
			Case "frmLoteriaPEntrega" : frmLoteriaPEntrega.Show() : frmLoteriaPEntrega.Activate()
			Case "frmPCobrar" : frmPCobrar.Show() : frmPCobrar.Activate()
				' (MENU) Gesti� Ampans
				' (SEG) Seguridad
			Case "FrmSeguretatServeiIn" : FrmSeguretatServeiIn.Show() : FrmSeguretatServeiIn.Activate()
				' (TAU) Tablas
			Case "frmTaulaTipusElements" : frmTaulaTipusElements.Show() : frmTaulaTipusElements.Activate()
			Case "frmTipusTarifes" : frmTipusTarifes.Show() : frmTipusTarifes.Activate()
			Case "frmTransportistaTarifa" : frmTransportistaTarifa.Show() : frmTransportistaTarifa.Activate()
			Case "frmTransportistaVdaInterna" : frmTransportistaVdaInterna.Show() : frmTransportistaVdaInterna.Activate()
			Case "frmMotivosNoRealizado" : frmMotivosNoRealizado.Show() : frmMotivosNoRealizado.Activate()
			Case "frmTarifesPreus" : frmTarifesPreus.Show() : frmTarifesPreus.Activate()
			Case "frmGestorsDeTransport" : frmGestorsDeTransport.Show() : frmGestorsDeTransport.Activate()
			Case "frmTransportista" : frmTransportista.Show() : frmTransportista.Activate()
			Case "frmTransportistaPreuHora" : frmTransportistaPreuHora.Show() : frmTransportistaPreuHora.Activate()
			Case "frmTransportistaPreuKm" : frmTransportistaPreuKm.Show() : frmTransportistaPreuKm.Activate()
				' (TAUIN) Tablas inserci�n laboral
			Case "frmProgramesInsercioLaboral" : frmProgramesInsercioLaboral.Show() : frmProgramesInsercioLaboral.Activate()
			Case "frmTaulaProgrames" : frmTaulaProgrames.Show() : frmTaulaProgrames.Activate()
			Case "frmMotiusBaixaPrograma" : frmMotiusBaixaPrograma.Show() : frmMotiusBaixaPrograma.Activate()
			Case "frmTipusAccions" : frmTipusAccions.Show() : frmTipusAccions.Activate()
			Case "frmTipusSubaccions" : frmTipusSubaccions.Show() : frmTipusSubaccions.Activate()
			Case "frmServiciosDeInsercion" : frmServiciosDeInsercion.Show() : frmServiciosDeInsercion.Activate()
			Case "frmMotiusAltaServeis" : frmMotiusAltaServeis.Show() : frmMotiusAltaServeis.Activate()
			Case "frmMotiusBaixaServeis" : frmMotiusBaixaServeis.Show() : frmMotiusBaixaServeis.Activate()
			Case "frmMotivosBajaLaboral" : frmMotivosBajaLaboral.Show() : frmMotivosBajaLaboral.Activate()
			Case "frmTaulaColectius" : frmTaulaColectius.Show() : frmTaulaColectius.Activate()
			Case "frmTipusPrestacions" : frmTipusPrestacions.Show() : frmTipusPrestacions.Activate()
			Case "frmTipusLlocsTreball" : frmTipusLlocsTreball.Show() : frmTipusLlocsTreball.Activate()
			Case "frmTipusContractes" : frmTipusContractes.Show() : frmTipusContractes.Activate()
			Case "frmPrestacions" : frmPrestacions.Show() : frmPrestacions.Activate()
			Case "frmTipusRelacions" : frmTipusRelacions.Show() : frmTipusRelacions.Activate()
			Case "frmSubtipusRelacions" : frmSubtipusRelacions.Show() : frmSubtipusRelacions.Activate()
			Case "frmPreferenciesFormatives" : frmPreferenciesFormatives.Show() : frmPreferenciesFormatives.Activate()
			Case "frmSeguimentRelacio" : frmSeguimentRelacio.Show() : frmSeguimentRelacio.Activate()
			Case "frmOrigenFormacio" : frmOrigenFormacio.Show() : frmOrigenFormacio.Activate()
			Case "frmFormacio" : frmFormacio.Show() : frmFormacio.Activate()
			Case "frmPuntsAtencio" : frmPuntsAtencio.Show() : frmPuntsAtencio.Activate()
			Case "frmTecnicsInsercio" : frmTecnicsInsercio.Show() : frmTecnicsInsercio.Activate()
			Case "frmTipusGestions" : frmTipusGestions.Show() : frmTipusGestions.Activate()
			Case "frmIdoneitatAPrograma" : frmIdoneitatAPrograma.Show() : frmIdoneitatAPrograma.Activate()
				' (TCRM) Tablas CRM
			Case "frmTaulaTipusAccions" : frmTaulaTipusAccions.Show() : frmTaulaTipusAccions.Activate()
			Case "frmTaulaTipusSubaccions" : frmTaulaTipusSubaccions.Show() : frmTaulaTipusSubaccions.Activate()
			Case "frmTaulaTipusEvents" : frmTaulaTipusEvents.Show() : frmTaulaTipusEvents.Activate()
			Case "frmTipusObjectiu" : frmTipusObjectiu.Show() : frmTipusObjectiu.Activate()
			Case "frmResultatsObjectius" : frmResultatsObjectius.Show() : frmResultatsObjectius.Activate()
			Case "frmTaulaResultats" : frmTaulaResultats.Show() : frmTaulaResultats.Activate()
			Case "frmTaulaResultatsDetall" : frmTaulaResultatsDetall.Show() : frmTaulaResultatsDetall.Activate()
			Case "frmMailCites" : frmMailCites.Show() : frmMailCites.Activate()
				' (TDEM) Taules
			Case "frmTaulaDeDemandes" : frmTaulaDeDemandes.Show() : frmTaulaDeDemandes.Activate()
			Case "frmTaulaSubtipusDemanda" : frmTaulaSubtipusDemanda.Show() : frmTaulaSubtipusDemanda.Activate()
			Case "frmCentresDemandes" : frmCentresDemandes.Show() : frmCentresDemandes.Activate()
				' (TRANS) Transporte
			Case "frmSolicitudTransport" : frmSolicitudTransport.Show() : frmSolicitudTransport.Activate()
			Case "frmSolPendents" : frmSolPendents.Show() : frmSolPendents.Activate()
			Case "FrmSolPlanificades" : FrmSolPlanificades.Show() : FrmSolPlanificades.Activate()
			Case "frmSolicitudTransportHistoric" : frmSolicitudTransportHistoric.Show() : frmSolicitudTransportHistoric.Activate()
		End Select
		frmMenus.Enabled = True
		
	End Sub
	
	'UPGRADE_ISSUE: El evento Form MDIForm.MouseDown no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub MDI_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles MyBase.MouseDown
		If Button = MouseButtons.Right Then MDIPopupVentana()
	End Sub
	
	'UPGRADE_ISSUE: El evento Form MDIForm.MouseMove no se actualiz�. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub MDI_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles MyBase.MouseMove
		TempsActual = Date.Now.TimeOfDay.TotalSeconds
	End Sub
	
	'UPGRADE_WARNING: El evento MDI.Resize se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub MDI_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
		MDIResize()
	End Sub
	
	Private Sub MDI_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		DescarregaFormularis()
Application.Exit() : End    ''MIGRAT
	End Sub
	
	Private Sub TimerDesconexio_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles TimerDesconexio.Tick
		ControlDesconexio()
	End Sub
	Private Sub CacheCub_OnError(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CacheCub.OnError
		ErrorCache()
	End Sub
End Class
