Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmTaulaDeDemandes
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub FGetReg()
		CarregaCentresOnAplica()
		CarregaSubtipusDemandes()
	End Sub
	
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		CarregaCentresOnAplica()
		CarregaSubtipusDemandes()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	'UPGRADE_WARNING: Form evento frmTaulaDeDemandes.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmTaulaDeDemandes_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaCentresOnAplica()
		CarregaSubtipusDemandes()
	End Sub
	
	Private Sub frmTaulaDeDemandes_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		'    GravaCentresMarcats
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		'GravaCentresMarcats
	End Sub
	
	Private Sub frmTaulaDeDemandes_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmTaulaDeDemandes_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmTaulaDeDemandes_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	
	Private Sub GrdCentresOnAplica_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdCentresOnAplica.Click
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		
		
		CacheNetejaParametres()
		MCache.P1 = txtCodi.Text
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MCache.P2 = Piece(GrdCentresOnAplica.Cell(GrdCentresOnAplica.ActiveCell.Row, 1).Text, "|", 1)
		MCache.P3 = GrdCentresOnAplica.Cell(GrdCentresOnAplica.ActiveCell.Row, 5).Text
		CacheXecute(" D GRAVACENT^DEMANDES")
	End Sub
	
	Private Sub GrdCentresOnAplica_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdCentresOnAplica.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdCentresOnAplica", PopMenuStyle.tsPrimaryMenu, True)
				'.MenuItems.Add tsMenuCaption, "Marca tots", , , XPIcon("Checked"), , , , , "M"
				'.MenuItems.Add tsMenuCaption, "Desmarca tots", , , XPIcon("UnChecked"), , , , , "D"
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Assigna Usuari per Gestionar",  ,  , XPIcon("User"),  ,  ,  ,  , "A")
			End With
		End With
		XpExecutaMenu(Me, "GrdCentresOnAplica", eventArgs.X, eventArgs.Y)
	End Sub
	
	
	Private Sub GrdSubDemandes_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdSubDemandes.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtCodi.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdSubDemandes", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nova Subdemanda",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmTaulaSubtipusDemanda", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar Subdemanda",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdSubDemandes.Rows > 1 And PermisConsulta("frmTaulaSubtipusDemanda", e_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar Subdemanda",  ,  , XPIcon("Delete"),  ,  ,  ,  , "B")
				.MenuItems.Item("B").Enabled = GrdSubDemandes.Rows > 1 And PermisConsulta("frmTaulaSubtipusDemanda", e_Permisos.Baixes)
			End With
		End With
		XpExecutaMenu(Me, "GrdSubDemandes", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub txtCodi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.GotFocus
		XGotFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.DoubleClick
		ConsultaTaula(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.LostFocus
		If txtCodi.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtCodi)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.GotFocus
		XGotFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.LostFocus
		XLostFocus(Me, txtDescripcio)
		CarregaCentresOnAplica()
	End Sub
	
	Private Sub txtDescripcio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtDescripcio)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDataBaixa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataBaixa.GotFocus
		XGotFocus(Me, txtDataBaixa)
	End Sub
	
	Private Sub txtDataBaixa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataBaixa.LostFocus
		XLostFocus(Me, txtDataBaixa)
	End Sub
	
	Private Sub CarregaCentresOnAplica()
		CarregaFGrid(GrdCentresOnAplica, "CCENT^DEMANDES", txtCodi.Text, CStr(3))
		MarcaRowFG(GrdCentresOnAplica, 1)
		'grdCatPermeses.Column(4).Sort cellDescending
	End Sub
	
	
	Private Sub GravaCentresMarcats()
		'CacheNetejaParametres
		'MCache.P1 = txtCodi.Text
		'MCache.P2 = FGMontaKeysSeleccio(GrdCentresOnAplica, 5)
		'MCache.P3 = FGMontaRowsSeleccio(GrdCentresOnAplica, 4)
		'CacheXecute "D GRAVCENTRES^DEMANDES"
		
	End Sub
	
	
	Private Sub CarregaSubtipusDemandes()
		CarregaFGrid(GrdSubDemandes, "CSUBDEMANDES^DEMANDES", txtCodi.Text)
		MarcaRowFG(GrdSubDemandes, 1)
	End Sub
	
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Dim I As Short
		Select Case NomMenu
			Case "GrdCentresOnAplica"
				Select Case KeyMenu
					Case "M"
						FGCheckGrid(GrdCentresOnAplica, True)
					Case "D"
						FGCheckGrid(GrdCentresOnAplica, False)
					Case "A"
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTaulaDeDemandesCentres, "frmTaulaDeDemandesCentres", txtCodi.Text & S & "C" & S & Piece(GrdCentresOnAplica.Cell(GrdCentresOnAplica.ActiveCell.Row, 1).Text, "|", 1), Me.Name)
				End Select
				
			Case "GrdSubDemandes"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "N"
						ObreFormulari(frmTaulaSubtipusDemanda, "frmTaulaSubtipusDemanda", txtCodi.Text)
						
					Case "E"
						If GrdSubDemandes.ActiveCell.Row = 0 Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTaulaSubtipusDemanda, "frmTaulaSubtipusDemanda", txtCodi.Text & S & Piece(GrdSubDemandes.Cell(GrdSubDemandes.ActiveCell.Row, 1).Text, "|", 2))
					Case "B"
						If GrdSubDemandes.ActiveCell.Row = 0 Then Exit Sub
						
						A = GrdSubDemandes.Cell(GrdSubDemandes.ActiveCell.Row, 2).Text
						If A = "" Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTaulaSubtipusDemanda, "frmTaulaSubtipusDemanda", txtCodi.Text & S & Piece(GrdSubDemandes.Cell(GrdSubDemandes.ActiveCell.Row, 1).Text, "|", 2))
						LlibAplicacio.DeleteReg(frmTaulaSubtipusDemanda   )
						frmTaulaSubtipusDemanda.Unload()
				End Select
				
		End Select
	End Sub
End Class
