<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmProgramesInsercioLaboral
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtTipoPrograma As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtCodigo As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcion As System.Windows.Forms.TextBox
	Public WithEvents txtEntidad As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtFechaBaja As AxDataControl.AxGmsData
	Public WithEvents txtNombreEmpresa As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmProgramesInsercioLaboral))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtTipoPrograma = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtCodigo = New System.Windows.Forms.TextBox
		Me.txtDescripcion = New System.Windows.Forms.TextBox
		Me.txtEntidad = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtFechaBaja = New AxDataControl.AxGmsData
		Me.txtNombreEmpresa = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Programes insercio laboral"
		Me.ClientSize = New System.Drawing.Size(471, 206)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-PROGRAMES_INSERCIO"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmProgramesInsercioLaboral"
		Me.txtTipoPrograma.AutoSize = False
		Me.txtTipoPrograma.Size = New System.Drawing.Size(21, 19)
		Me.txtTipoPrograma.Location = New System.Drawing.Point(124, 58)
		Me.txtTipoPrograma.Maxlength = 2
		Me.txtTipoPrograma.TabIndex = 4
		Me.txtTipoPrograma.Tag = "6"
		Me.txtTipoPrograma.AcceptsReturn = True
		Me.txtTipoPrograma.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoPrograma.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoPrograma.CausesValidation = True
		Me.txtTipoPrograma.Enabled = True
		Me.txtTipoPrograma.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoPrograma.HideSelection = True
		Me.txtTipoPrograma.ReadOnly = False
		Me.txtTipoPrograma.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoPrograma.MultiLine = False
		Me.txtTipoPrograma.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoPrograma.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoPrograma.TabStop = True
		Me.txtTipoPrograma.Visible = True
		Me.txtTipoPrograma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoPrograma.Name = "txtTipoPrograma"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(316, 19)
		Me.Text4.Location = New System.Drawing.Point(148, 58)
		Me.Text4.TabIndex = 15
		Me.Text4.Tag = "^6"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtCodigo.AutoSize = False
		Me.txtCodigo.Size = New System.Drawing.Size(21, 19)
		Me.txtCodigo.Location = New System.Drawing.Point(124, 10)
		Me.txtCodigo.Maxlength = 2
		Me.txtCodigo.TabIndex = 1
		Me.txtCodigo.Tag = "*1"
		Me.txtCodigo.AcceptsReturn = True
		Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigo.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigo.CausesValidation = True
		Me.txtCodigo.Enabled = True
		Me.txtCodigo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigo.HideSelection = True
		Me.txtCodigo.ReadOnly = False
		Me.txtCodigo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigo.MultiLine = False
		Me.txtCodigo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigo.TabStop = True
		Me.txtCodigo.Visible = True
		Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigo.Name = "txtCodigo"
		Me.txtDescripcion.AutoSize = False
		Me.txtDescripcion.Size = New System.Drawing.Size(341, 19)
		Me.txtDescripcion.Location = New System.Drawing.Point(124, 34)
		Me.txtDescripcion.Maxlength = 50
		Me.txtDescripcion.TabIndex = 3
		Me.txtDescripcion.Tag = "2"
		Me.txtDescripcion.AcceptsReturn = True
		Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcion.CausesValidation = True
		Me.txtDescripcion.Enabled = True
		Me.txtDescripcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcion.HideSelection = True
		Me.txtDescripcion.ReadOnly = False
		Me.txtDescripcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcion.MultiLine = False
		Me.txtDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcion.TabStop = True
		Me.txtDescripcion.Visible = True
		Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcion.Name = "txtDescripcion"
		Me.txtEntidad.AutoSize = False
		Me.txtEntidad.Size = New System.Drawing.Size(83, 19)
		Me.txtEntidad.Location = New System.Drawing.Point(124, 82)
		Me.txtEntidad.Maxlength = 8
		Me.txtEntidad.TabIndex = 5
		Me.txtEntidad.Tag = "3"
		Me.txtEntidad.AcceptsReturn = True
		Me.txtEntidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEntidad.BackColor = System.Drawing.SystemColors.Window
		Me.txtEntidad.CausesValidation = True
		Me.txtEntidad.Enabled = True
		Me.txtEntidad.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEntidad.HideSelection = True
		Me.txtEntidad.ReadOnly = False
		Me.txtEntidad.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEntidad.MultiLine = False
		Me.txtEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEntidad.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEntidad.TabStop = True
		Me.txtEntidad.Visible = True
		Me.txtEntidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEntidad.Name = "txtEntidad"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(256, 19)
		Me.Text3.Location = New System.Drawing.Point(208, 82)
		Me.Text3.TabIndex = 10
		Me.Text3.Tag = "^3"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		txtFechaBaja.OcxState = CType(resources.GetObject("txtFechaBaja.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaBaja.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaBaja.Location = New System.Drawing.Point(124, 130)
		Me.txtFechaBaja.TabIndex = 7
		Me.txtFechaBaja.Name = "txtFechaBaja"
		Me.txtNombreEmpresa.AutoSize = False
		Me.txtNombreEmpresa.Size = New System.Drawing.Size(341, 19)
		Me.txtNombreEmpresa.Location = New System.Drawing.Point(124, 106)
		Me.txtNombreEmpresa.Maxlength = 100
		Me.txtNombreEmpresa.TabIndex = 6
		Me.txtNombreEmpresa.Tag = "5"
		Me.txtNombreEmpresa.AcceptsReturn = True
		Me.txtNombreEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNombreEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtNombreEmpresa.CausesValidation = True
		Me.txtNombreEmpresa.Enabled = True
		Me.txtNombreEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNombreEmpresa.HideSelection = True
		Me.txtNombreEmpresa.ReadOnly = False
		Me.txtNombreEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNombreEmpresa.MultiLine = False
		Me.txtNombreEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNombreEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNombreEmpresa.TabStop = True
		Me.txtNombreEmpresa.Visible = True
		Me.txtNombreEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNombreEmpresa.Name = "txtNombreEmpresa"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(293, 170)
		Me.cmdAceptar.TabIndex = 8
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(383, 170)
		Me.cmdGuardar.TabIndex = 13
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.Label1.Text = "Tipo Programa"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 62)
		Me.Label1.TabIndex = 16
		Me.Label1.Tag = "6"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "C�digo"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Descripci�n"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Entidad"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 86)
		Me.lbl3.TabIndex = 9
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Fecha baja"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 134)
		Me.lbl4.TabIndex = 11
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Nombre empresa"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 110)
		Me.lbl5.TabIndex = 12
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 169)
		Me.lblLock.TabIndex = 14
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 163
		Me.Line1.Y2 = 163
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 164
		Me.Line2.Y2 = 164
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtTipoPrograma)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtCodigo)
		Me.Controls.Add(txtDescripcion)
		Me.Controls.Add(txtEntidad)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtFechaBaja)
		Me.Controls.Add(txtNombreEmpresa)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class