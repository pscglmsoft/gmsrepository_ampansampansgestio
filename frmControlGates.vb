Option Strict Off
Option Explicit On
Friend Class frmControlGates
	Inherits FormParent
	
	
	Private Sub frmControlGates_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		'If IniciForm(Me) = False Then
		'    Exit Sub
		'End If
		CentrarForm(Me)
	End Sub
	
Overrides 	Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub frmControlGates_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmControlGates_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	Private Sub txtAlbara_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAlbara.LostFocus
		Comprova()
	End Sub
	
	'UPGRADE_WARNING: El evento txtEtiqueta.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtEtiqueta_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEtiqueta.TextChanged
		Comprova()
	End Sub
	
	'UPGRADE_WARNING: El evento txtFaixa.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtFaixa_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFaixa.TextChanged
		Comprova()
	End Sub
	
	Private Sub Comprova()
		On Error Resume Next
		If (Len(txtAlbara.Text) = 14) And (Len(txtEtiqueta.Text) = 13) And (Len(txtFaixa.Text) = 13) Then
			
			
			If txtAlbara.Text <> "" And txtFaixa.Text <> "" And txtEtiqueta.Text <> "" Then
				If Mid(txtAlbara.Text, 2, 99) = txtFaixa.Text And txtFaixa.Text = txtEtiqueta.Text Then
					Retard()
					frmCorrecte.ShowDialog()
				Else
					Retard()
					frmError.ShowDialog()
				End If
				
			End If
		End If
	End Sub
End Class
