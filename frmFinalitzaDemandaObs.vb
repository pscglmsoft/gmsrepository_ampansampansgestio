Option Strict Off
Option Explicit On
Friend Class frmFinalitzaDemandaObs
	Inherits FormParent
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		Dim I As Short
		Dim Nod As String
		
		Nod = ""
		
		For I = 1 To GrdMails.Rows - 1
			If GrdMails.Cell(I, 4).Text = "1" Then
				If Nod <> "" Then Nod = Nod & ";" '& Chr(13)
				Nod = Nod & GrdMails.Cell(I, 3).Text
			End If
		Next I
		
		
		If txtObservacions.Text = "" Then
			xMsgBox("No s'ha omplert el camp observacions", MsgBoxStyle.Information, Me.Text)
			txtObservacions.Focus()
			Exit Sub
		End If
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(Nod, ;, 1). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Piece(Nod, ";", 1) = "" Then
			xMsgBox("No s'ha sel�leccionat cap adre�a de mail", MsgBoxStyle.Information, Me.Text)
			Exit Sub
		End If
		
		
		
		CacheNetejaParametres()
		
		MCache.P1 = txtCodi.Text
		MCache.P2 = txtObservacions.Text
		MCache.P3 = Nod
		
		CacheXecute("D FINALITZADEM^DEMANDES")
		CacheXecute("D ENVIAMAILOBS^DEMANDES")
		
		frmDemandesPendents.CarregaConsultaDemandes()
		frmDemandesPendents.CarregaDemandesQueHeFet()
		Me.Unload()
	End Sub
	
	Private Sub frmFinalitzaDemandaObs_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		CarregaGridMails()
	End Sub
	
	
	
	
	Private Sub frmFinalitzaDemandaObs_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub frmFinalitzaDemandaObs_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmFinalitzaDemandaObs_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub CarregaGridMails()
		CarregaFGrid(GrdMails, "CMAIL^DEMANDES")
	End Sub
End Class
