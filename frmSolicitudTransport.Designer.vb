<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSolicitudTransport
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents grdHSol As AxFlexCell.AxGrid
	Public WithEvents TabControlPage3 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents txtKilometros As System.Windows.Forms.TextBox
	Public WithEvents Text39 As System.Windows.Forms.TextBox
	Public WithEvents txtMotivoNoRealizado As System.Windows.Forms.TextBox
	Public WithEvents txtComentario As System.Windows.Forms.TextBox
	Public WithEvents Text34 As System.Windows.Forms.TextBox
	Public WithEvents txtTipoTarifa As System.Windows.Forms.TextBox
	Public WithEvents Text38 As System.Windows.Forms.TextBox
	Public WithEvents txtTransportista As System.Windows.Forms.TextBox
	Public WithEvents txtFechaPrevista As AxDataControl.AxGmsData
	Public WithEvents txtFechaRealizacion As AxDataControl.AxGmsData
	Public WithEvents txtFechaRecogida As AxDataControl.AxGmsData
	Public WithEvents txtHoraRecogida As AxgmsTime.AxgmsTemps
	Public WithEvents txtFechaEntrega As AxDataControl.AxGmsData
	Public WithEvents txtHoraEntrega As AxgmsTime.AxgmsTemps
	Public WithEvents txtPrecio As AxDataControl.AxGmsImports
	Public WithEvents txtHorasTransporte As AxgmsTime.AxgmsTemps
	Public WithEvents txtImporteVtaInterna As AxDataControl.AxGmsImports
	Public WithEvents cmdModifPreu As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents lbl39 As System.Windows.Forms.Label
	Public WithEvents lbl44 As System.Windows.Forms.Label
	Public WithEvents lbl35 As System.Windows.Forms.Label
	Public WithEvents lbl34 As System.Windows.Forms.Label
	Public WithEvents lbl41 As System.Windows.Forms.Label
	Public WithEvents lbl40 As System.Windows.Forms.Label
	Public WithEvents lbl43 As System.Windows.Forms.Label
	Public WithEvents lbl42 As System.Windows.Forms.Label
	Public WithEvents lbl37 As System.Windows.Forms.Label
	Public WithEvents lbl36 As System.Windows.Forms.Label
	Public WithEvents lbl38 As System.Windows.Forms.Label
	Public WithEvents FrameTransp As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents TabControlPage2 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents txtSubdireccionDestino As System.Windows.Forms.TextBox
	Public WithEvents txtObservacionesDestino As System.Windows.Forms.TextBox
	Public WithEvents txtEntidadEntrega As System.Windows.Forms.TextBox
	Public WithEvents txtSucursalEntrega As System.Windows.Forms.TextBox
	Public WithEvents Text33 As System.Windows.Forms.TextBox
	Public WithEvents txtPuntoEntrega As System.Windows.Forms.TextBox
	Public WithEvents Text25 As System.Windows.Forms.TextBox
	Public WithEvents txtDireccionEntrega As System.Windows.Forms.TextBox
	Public WithEvents txtPoblacionEntrega As System.Windows.Forms.TextBox
	Public WithEvents Text27 As System.Windows.Forms.TextBox
	Public WithEvents txtCpEntrega As System.Windows.Forms.TextBox
	Public WithEvents txtTelefonoEntrega As System.Windows.Forms.TextBox
	Public WithEvents txtContactoEntrega As System.Windows.Forms.TextBox
	Public WithEvents txtHorarioEntrega As System.Windows.Forms.TextBox
	Public WithEvents txtDesti As System.Windows.Forms.TextBox
	Public WithEvents _optDesti_1 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents _optDesti_2 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents frDesti As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents chkCopiaRegistrada As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents lbl32 As System.Windows.Forms.Label
	Public WithEvents lbl33 As System.Windows.Forms.Label
	Public WithEvents lbl25 As System.Windows.Forms.Label
	Public WithEvents lbl26 As System.Windows.Forms.Label
	Public WithEvents lbl27 As System.Windows.Forms.Label
	Public WithEvents lbl28 As System.Windows.Forms.Label
	Public WithEvents lbl29 As System.Windows.Forms.Label
	Public WithEvents lbl30 As System.Windows.Forms.Label
	Public WithEvents lbl31 As System.Windows.Forms.Label
	Public WithEvents FrameDesti As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents txtSubdireccionOrigen As System.Windows.Forms.TextBox
	Public WithEvents txtObservacionesorigen As System.Windows.Forms.TextBox
	Public WithEvents txtPuntoRecepcion As System.Windows.Forms.TextBox
	Public WithEvents Text16 As System.Windows.Forms.TextBox
	Public WithEvents txtDireccionRecepcion As System.Windows.Forms.TextBox
	Public WithEvents txtPoblacionRecepcion As System.Windows.Forms.TextBox
	Public WithEvents Text18 As System.Windows.Forms.TextBox
	Public WithEvents txtCpRecepcion As System.Windows.Forms.TextBox
	Public WithEvents txtTelefonoRecepcion As System.Windows.Forms.TextBox
	Public WithEvents txtContactoRecepcion As System.Windows.Forms.TextBox
	Public WithEvents txtHorarioRecepcion As System.Windows.Forms.TextBox
	Public WithEvents txtEntidadRecepcion As System.Windows.Forms.TextBox
	Public WithEvents txtSucursalRecepcion As System.Windows.Forms.TextBox
	Public WithEvents Text24 As System.Windows.Forms.TextBox
	Public WithEvents txtOrigen As System.Windows.Forms.TextBox
	Public WithEvents _optOrigen_1 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents _optOrigen_2 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents frOrigen As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl16 As System.Windows.Forms.Label
	Public WithEvents lbl17 As System.Windows.Forms.Label
	Public WithEvents lbl18 As System.Windows.Forms.Label
	Public WithEvents lbl19 As System.Windows.Forms.Label
	Public WithEvents lbl20 As System.Windows.Forms.Label
	Public WithEvents lbl21 As System.Windows.Forms.Label
	Public WithEvents lbl22 As System.Windows.Forms.Label
	Public WithEvents lbl23 As System.Windows.Forms.Label
	Public WithEvents lbl24 As System.Windows.Forms.Label
	Public WithEvents FrameOrigen As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents TabControlPage1 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents TabControl1 As AxXtremeSuiteControls.AxTabControl
	Public WithEvents txtPes As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcionTransporte As System.Windows.Forms.TextBox
	Public WithEvents Text14 As System.Windows.Forms.TextBox
	Public WithEvents txtTipoElemento As System.Windows.Forms.TextBox
	Public WithEvents txtCantidad As System.Windows.Forms.TextBox
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents lbl15 As System.Windows.Forms.Label
	Public WithEvents lbl14 As System.Windows.Forms.Label
	Public WithEvents lbl13 As System.Windows.Forms.Label
	Public WithEvents GroupT As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents txtSolicitud As System.Windows.Forms.TextBox
	Public WithEvents cmbEstat As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents txtDataSolicitud As AxDataControl.AxGmsData
	Public WithEvents txtDataTransport As AxDataControl.AxGmsData
	Public WithEvents cmbPrioritat As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents txtSolicitant As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents txtCentreCost As System.Windows.Forms.TextBox
	Public WithEvents Text8 As System.Windows.Forms.TextBox
	Public WithEvents txtFeina As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents txtTarea As System.Windows.Forms.TextBox
	Public WithEvents Text10 As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtTipo As System.Windows.Forms.TextBox
	Public WithEvents _optTipo_1 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents _optTipo_2 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents Image2 As System.Windows.Forms.PictureBox
	Public WithEvents Image1 As System.Windows.Forms.PictureBox
	Public WithEvents frTipo As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents cmdReobrir As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdImprimir As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl10 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents optDesti As AxRadioButtonArray
	Public WithEvents optOrigen As AxRadioButtonArray
	Public WithEvents optTipo As AxRadioButtonArray
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSolicitudTransport))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.TabControl1 = New AxXtremeSuiteControls.AxTabControl
		Me.TabControlPage3 = New AxXtremeSuiteControls.AxTabControlPage
		Me.grdHSol = New AxFlexCell.AxGrid
		Me.TabControlPage2 = New AxXtremeSuiteControls.AxTabControlPage
		Me.FrameTransp = New AxXtremeSuiteControls.AxGroupBox
		Me.txtKilometros = New System.Windows.Forms.TextBox
		Me.Text39 = New System.Windows.Forms.TextBox
		Me.txtMotivoNoRealizado = New System.Windows.Forms.TextBox
		Me.txtComentario = New System.Windows.Forms.TextBox
		Me.Text34 = New System.Windows.Forms.TextBox
		Me.txtTipoTarifa = New System.Windows.Forms.TextBox
		Me.Text38 = New System.Windows.Forms.TextBox
		Me.txtTransportista = New System.Windows.Forms.TextBox
		Me.txtFechaPrevista = New AxDataControl.AxGmsData
		Me.txtFechaRealizacion = New AxDataControl.AxGmsData
		Me.txtFechaRecogida = New AxDataControl.AxGmsData
		Me.txtHoraRecogida = New AxgmsTime.AxgmsTemps
		Me.txtFechaEntrega = New AxDataControl.AxGmsData
		Me.txtHoraEntrega = New AxgmsTime.AxgmsTemps
		Me.txtPrecio = New AxDataControl.AxGmsImports
		Me.txtHorasTransporte = New AxgmsTime.AxgmsTemps
		Me.txtImporteVtaInterna = New AxDataControl.AxGmsImports
		Me.cmdModifPreu = New AxXtremeSuiteControls.AxPushButton
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.lbl39 = New System.Windows.Forms.Label
		Me.lbl44 = New System.Windows.Forms.Label
		Me.lbl35 = New System.Windows.Forms.Label
		Me.lbl34 = New System.Windows.Forms.Label
		Me.lbl41 = New System.Windows.Forms.Label
		Me.lbl40 = New System.Windows.Forms.Label
		Me.lbl43 = New System.Windows.Forms.Label
		Me.lbl42 = New System.Windows.Forms.Label
		Me.lbl37 = New System.Windows.Forms.Label
		Me.lbl36 = New System.Windows.Forms.Label
		Me.lbl38 = New System.Windows.Forms.Label
		Me.TabControlPage1 = New AxXtremeSuiteControls.AxTabControlPage
		Me.FrameDesti = New AxXtremeSuiteControls.AxGroupBox
		Me.txtSubdireccionDestino = New System.Windows.Forms.TextBox
		Me.txtObservacionesDestino = New System.Windows.Forms.TextBox
		Me.txtEntidadEntrega = New System.Windows.Forms.TextBox
		Me.txtSucursalEntrega = New System.Windows.Forms.TextBox
		Me.Text33 = New System.Windows.Forms.TextBox
		Me.txtPuntoEntrega = New System.Windows.Forms.TextBox
		Me.Text25 = New System.Windows.Forms.TextBox
		Me.txtDireccionEntrega = New System.Windows.Forms.TextBox
		Me.txtPoblacionEntrega = New System.Windows.Forms.TextBox
		Me.Text27 = New System.Windows.Forms.TextBox
		Me.txtCpEntrega = New System.Windows.Forms.TextBox
		Me.txtTelefonoEntrega = New System.Windows.Forms.TextBox
		Me.txtContactoEntrega = New System.Windows.Forms.TextBox
		Me.txtHorarioEntrega = New System.Windows.Forms.TextBox
		Me.frDesti = New AxXtremeSuiteControls.AxGroupBox
		Me.txtDesti = New System.Windows.Forms.TextBox
		Me._optDesti_1 = New AxXtremeSuiteControls.AxRadioButton
		Me._optDesti_2 = New AxXtremeSuiteControls.AxRadioButton
		Me.chkCopiaRegistrada = New AxXtremeSuiteControls.AxCheckBox
		Me.Label9 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.lbl32 = New System.Windows.Forms.Label
		Me.lbl33 = New System.Windows.Forms.Label
		Me.lbl25 = New System.Windows.Forms.Label
		Me.lbl26 = New System.Windows.Forms.Label
		Me.lbl27 = New System.Windows.Forms.Label
		Me.lbl28 = New System.Windows.Forms.Label
		Me.lbl29 = New System.Windows.Forms.Label
		Me.lbl30 = New System.Windows.Forms.Label
		Me.lbl31 = New System.Windows.Forms.Label
		Me.FrameOrigen = New AxXtremeSuiteControls.AxGroupBox
		Me.txtSubdireccionOrigen = New System.Windows.Forms.TextBox
		Me.txtObservacionesorigen = New System.Windows.Forms.TextBox
		Me.txtPuntoRecepcion = New System.Windows.Forms.TextBox
		Me.Text16 = New System.Windows.Forms.TextBox
		Me.txtDireccionRecepcion = New System.Windows.Forms.TextBox
		Me.txtPoblacionRecepcion = New System.Windows.Forms.TextBox
		Me.Text18 = New System.Windows.Forms.TextBox
		Me.txtCpRecepcion = New System.Windows.Forms.TextBox
		Me.txtTelefonoRecepcion = New System.Windows.Forms.TextBox
		Me.txtContactoRecepcion = New System.Windows.Forms.TextBox
		Me.txtHorarioRecepcion = New System.Windows.Forms.TextBox
		Me.txtEntidadRecepcion = New System.Windows.Forms.TextBox
		Me.txtSucursalRecepcion = New System.Windows.Forms.TextBox
		Me.Text24 = New System.Windows.Forms.TextBox
		Me.frOrigen = New AxXtremeSuiteControls.AxGroupBox
		Me.txtOrigen = New System.Windows.Forms.TextBox
		Me._optOrigen_1 = New AxXtremeSuiteControls.AxRadioButton
		Me._optOrigen_2 = New AxXtremeSuiteControls.AxRadioButton
		Me.Label8 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl16 = New System.Windows.Forms.Label
		Me.lbl17 = New System.Windows.Forms.Label
		Me.lbl18 = New System.Windows.Forms.Label
		Me.lbl19 = New System.Windows.Forms.Label
		Me.lbl20 = New System.Windows.Forms.Label
		Me.lbl21 = New System.Windows.Forms.Label
		Me.lbl22 = New System.Windows.Forms.Label
		Me.lbl23 = New System.Windows.Forms.Label
		Me.lbl24 = New System.Windows.Forms.Label
		Me.GroupT = New AxXtremeSuiteControls.AxGroupBox
		Me.txtPes = New System.Windows.Forms.TextBox
		Me.txtDescripcionTransporte = New System.Windows.Forms.TextBox
		Me.Text14 = New System.Windows.Forms.TextBox
		Me.txtTipoElemento = New System.Windows.Forms.TextBox
		Me.txtCantidad = New System.Windows.Forms.TextBox
		Me.Label7 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.lbl15 = New System.Windows.Forms.Label
		Me.lbl14 = New System.Windows.Forms.Label
		Me.lbl13 = New System.Windows.Forms.Label
		Me.txtSolicitud = New System.Windows.Forms.TextBox
		Me.cmbEstat = New AxSSDataWidgets_B.AxSSDBCombo
		Me.txtDataSolicitud = New AxDataControl.AxGmsData
		Me.txtDataTransport = New AxDataControl.AxGmsData
		Me.cmbPrioritat = New AxSSDataWidgets_B.AxSSDBCombo
		Me.txtSolicitant = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text7 = New System.Windows.Forms.TextBox
		Me.txtCentreCost = New System.Windows.Forms.TextBox
		Me.Text8 = New System.Windows.Forms.TextBox
		Me.txtFeina = New System.Windows.Forms.TextBox
		Me.Text9 = New System.Windows.Forms.TextBox
		Me.txtTarea = New System.Windows.Forms.TextBox
		Me.Text10 = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.frTipo = New AxXtremeSuiteControls.AxGroupBox
		Me.txtTipo = New System.Windows.Forms.TextBox
		Me._optTipo_1 = New AxXtremeSuiteControls.AxRadioButton
		Me._optTipo_2 = New AxXtremeSuiteControls.AxRadioButton
		Me.Image2 = New System.Windows.Forms.PictureBox
		Me.Image1 = New System.Windows.Forms.PictureBox
		Me.cmdReobrir = New AxXtremeSuiteControls.AxPushButton
		Me.cmdImprimir = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl10 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.optDesti = New AxRadioButtonArray(components)
		Me.optOrigen = New AxRadioButtonArray(components)
		Me.optTipo = New AxRadioButtonArray(components)
		Me.TabControl1.SuspendLayout()
		Me.TabControlPage3.SuspendLayout()
		Me.TabControlPage2.SuspendLayout()
		Me.FrameTransp.SuspendLayout()
		Me.TabControlPage1.SuspendLayout()
		Me.FrameDesti.SuspendLayout()
		Me.frDesti.SuspendLayout()
		Me.FrameOrigen.SuspendLayout()
		Me.frOrigen.SuspendLayout()
		Me.GroupT.SuspendLayout()
		Me.frTipo.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.grdHSol, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaPrevista, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaRealizacion, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaRecogida, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtHoraRecogida, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaEntrega, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtHoraEntrega, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtHorasTransporte, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtImporteVtaInterna, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdModifPreu, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.FrameTransp, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optDesti_1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optDesti_2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.frDesti, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkCopiaRegistrada, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.FrameDesti, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optOrigen_1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optOrigen_2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.frOrigen, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.FrameOrigen, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GroupT, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbEstat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataSolicitud, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataTransport, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbPrioritat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optTipo_1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optTipo_2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.frTipo, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdReobrir, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdImprimir, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.optDesti, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.optOrigen, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.optTipo, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Solicitud de transporte"
		Me.ClientSize = New System.Drawing.Size(908, 621)
		Me.Location = New System.Drawing.Point(358, 228)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "T-SOL_TRANSPORT"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmSolicitudTransport"
		TabControl1.OcxState = CType(resources.GetObject("TabControl1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControl1.Size = New System.Drawing.Size(887, 361)
		Me.TabControl1.Location = New System.Drawing.Point(10, 224)
		Me.TabControl1.TabIndex = 63
		Me.TabControl1.Name = "TabControl1"
		TabControlPage3.OcxState = CType(resources.GetObject("TabControlPage3.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage3.Size = New System.Drawing.Size(883, 337)
		Me.TabControlPage3.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage3.TabIndex = 66
		Me.TabControlPage3.Visible = False
		Me.TabControlPage3.Name = "TabControlPage3"
		grdHSol.OcxState = CType(resources.GetObject("grdHSol.OcxState"), System.Windows.Forms.AxHost.State)
		Me.grdHSol.Size = New System.Drawing.Size(866, 326)
		Me.grdHSol.Location = New System.Drawing.Point(6, 6)
		Me.grdHSol.TabIndex = 133
		Me.grdHSol.Name = "grdHSol"
		TabControlPage2.OcxState = CType(resources.GetObject("TabControlPage2.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage2.Size = New System.Drawing.Size(777, 431)
		Me.TabControlPage2.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage2.TabIndex = 65
		Me.TabControlPage2.Visible = False
		Me.TabControlPage2.Name = "TabControlPage2"
		FrameTransp.OcxState = CType(resources.GetObject("FrameTransp.OcxState"), System.Windows.Forms.AxHost.State)
		Me.FrameTransp.Size = New System.Drawing.Size(623, 263)
		Me.FrameTransp.Location = New System.Drawing.Point(6, 12)
		Me.FrameTransp.TabIndex = 67
		Me.FrameTransp.Name = "FrameTransp"
		Me.txtKilometros.AutoSize = False
		Me.txtKilometros.Size = New System.Drawing.Size(41, 19)
		Me.txtKilometros.Location = New System.Drawing.Point(514, 110)
		Me.txtKilometros.Maxlength = 3
		Me.txtKilometros.TabIndex = 76
		Me.txtKilometros.Tag = "52"
		Me.txtKilometros.AcceptsReturn = True
		Me.txtKilometros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtKilometros.BackColor = System.Drawing.SystemColors.Window
		Me.txtKilometros.CausesValidation = True
		Me.txtKilometros.Enabled = True
		Me.txtKilometros.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtKilometros.HideSelection = True
		Me.txtKilometros.ReadOnly = False
		Me.txtKilometros.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtKilometros.MultiLine = False
		Me.txtKilometros.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtKilometros.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtKilometros.TabStop = True
		Me.txtKilometros.Visible = True
		Me.txtKilometros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtKilometros.Name = "txtKilometros"
		Me.Text39.AutoSize = False
		Me.Text39.BackColor = System.Drawing.Color.White
		Me.Text39.Enabled = False
		Me.Text39.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text39.Size = New System.Drawing.Size(456, 19)
		Me.Text39.Location = New System.Drawing.Point(142, 232)
		Me.Text39.TabIndex = 84
		Me.Text39.Tag = "^39"
		Me.Text39.AcceptsReturn = True
		Me.Text39.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text39.CausesValidation = True
		Me.Text39.HideSelection = True
		Me.Text39.ReadOnly = False
		Me.Text39.Maxlength = 0
		Me.Text39.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text39.MultiLine = False
		Me.Text39.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text39.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text39.TabStop = True
		Me.Text39.Visible = True
		Me.Text39.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text39.Name = "Text39"
		Me.txtMotivoNoRealizado.AutoSize = False
		Me.txtMotivoNoRealizado.Size = New System.Drawing.Size(21, 19)
		Me.txtMotivoNoRealizado.Location = New System.Drawing.Point(118, 232)
		Me.txtMotivoNoRealizado.Maxlength = 2
		Me.txtMotivoNoRealizado.TabIndex = 81
		Me.txtMotivoNoRealizado.Tag = "39"
		Me.txtMotivoNoRealizado.AcceptsReturn = True
		Me.txtMotivoNoRealizado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMotivoNoRealizado.BackColor = System.Drawing.SystemColors.Window
		Me.txtMotivoNoRealizado.CausesValidation = True
		Me.txtMotivoNoRealizado.Enabled = True
		Me.txtMotivoNoRealizado.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMotivoNoRealizado.HideSelection = True
		Me.txtMotivoNoRealizado.ReadOnly = False
		Me.txtMotivoNoRealizado.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMotivoNoRealizado.MultiLine = False
		Me.txtMotivoNoRealizado.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMotivoNoRealizado.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMotivoNoRealizado.TabStop = True
		Me.txtMotivoNoRealizado.Visible = True
		Me.txtMotivoNoRealizado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMotivoNoRealizado.Name = "txtMotivoNoRealizado"
		Me.txtComentario.AutoSize = False
		Me.txtComentario.Size = New System.Drawing.Size(481, 45)
		Me.txtComentario.Location = New System.Drawing.Point(118, 182)
		Me.txtComentario.Maxlength = 200
		Me.txtComentario.MultiLine = True
		Me.txtComentario.TabIndex = 80
		Me.txtComentario.Tag = "44"
		Me.txtComentario.AcceptsReturn = True
		Me.txtComentario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtComentario.BackColor = System.Drawing.SystemColors.Window
		Me.txtComentario.CausesValidation = True
		Me.txtComentario.Enabled = True
		Me.txtComentario.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtComentario.HideSelection = True
		Me.txtComentario.ReadOnly = False
		Me.txtComentario.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtComentario.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtComentario.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtComentario.TabStop = True
		Me.txtComentario.Visible = True
		Me.txtComentario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtComentario.Name = "txtComentario"
		Me.Text34.AutoSize = False
		Me.Text34.BackColor = System.Drawing.Color.White
		Me.Text34.Enabled = False
		Me.Text34.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text34.Size = New System.Drawing.Size(259, 19)
		Me.Text34.Location = New System.Drawing.Point(142, 134)
		Me.Text34.TabIndex = 83
		Me.Text34.Tag = "^34"
		Me.Text34.AcceptsReturn = True
		Me.Text34.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text34.CausesValidation = True
		Me.Text34.HideSelection = True
		Me.Text34.ReadOnly = False
		Me.Text34.Maxlength = 0
		Me.Text34.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text34.MultiLine = False
		Me.Text34.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text34.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text34.TabStop = True
		Me.Text34.Visible = True
		Me.Text34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text34.Name = "Text34"
		Me.txtTipoTarifa.AutoSize = False
		Me.txtTipoTarifa.Size = New System.Drawing.Size(21, 19)
		Me.txtTipoTarifa.Location = New System.Drawing.Point(118, 134)
		Me.txtTipoTarifa.Maxlength = 2
		Me.txtTipoTarifa.TabIndex = 77
		Me.txtTipoTarifa.Tag = "34"
		Me.txtTipoTarifa.AcceptsReturn = True
		Me.txtTipoTarifa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoTarifa.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoTarifa.CausesValidation = True
		Me.txtTipoTarifa.Enabled = True
		Me.txtTipoTarifa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoTarifa.HideSelection = True
		Me.txtTipoTarifa.ReadOnly = False
		Me.txtTipoTarifa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoTarifa.MultiLine = False
		Me.txtTipoTarifa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoTarifa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoTarifa.TabStop = True
		Me.txtTipoTarifa.Visible = True
		Me.txtTipoTarifa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoTarifa.Name = "txtTipoTarifa"
		Me.Text38.AutoSize = False
		Me.Text38.BackColor = System.Drawing.Color.White
		Me.Text38.Enabled = False
		Me.Text38.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text38.Size = New System.Drawing.Size(438, 19)
		Me.Text38.Location = New System.Drawing.Point(163, 14)
		Me.Text38.TabIndex = 82
		Me.Text38.Tag = "^38"
		Me.Text38.AcceptsReturn = True
		Me.Text38.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text38.CausesValidation = True
		Me.Text38.HideSelection = True
		Me.Text38.ReadOnly = False
		Me.Text38.Maxlength = 0
		Me.Text38.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text38.MultiLine = False
		Me.Text38.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text38.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text38.TabStop = True
		Me.Text38.Visible = True
		Me.Text38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text38.Name = "Text38"
		Me.txtTransportista.AutoSize = False
		Me.txtTransportista.Size = New System.Drawing.Size(42, 19)
		Me.txtTransportista.Location = New System.Drawing.Point(118, 14)
		Me.txtTransportista.Maxlength = 4
		Me.txtTransportista.TabIndex = 68
		Me.txtTransportista.Tag = "38"
		Me.txtTransportista.AcceptsReturn = True
		Me.txtTransportista.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTransportista.BackColor = System.Drawing.SystemColors.Window
		Me.txtTransportista.CausesValidation = True
		Me.txtTransportista.Enabled = True
		Me.txtTransportista.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTransportista.HideSelection = True
		Me.txtTransportista.ReadOnly = False
		Me.txtTransportista.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTransportista.MultiLine = False
		Me.txtTransportista.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTransportista.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTransportista.TabStop = True
		Me.txtTransportista.Visible = True
		Me.txtTransportista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTransportista.Name = "txtTransportista"
		txtFechaPrevista.OcxState = CType(resources.GetObject("txtFechaPrevista.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaPrevista.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaPrevista.Location = New System.Drawing.Point(118, 38)
		Me.txtFechaPrevista.TabIndex = 69
		Me.txtFechaPrevista.Name = "txtFechaPrevista"
		txtFechaRealizacion.OcxState = CType(resources.GetObject("txtFechaRealizacion.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaRealizacion.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaRealizacion.Location = New System.Drawing.Point(514, 38)
		Me.txtFechaRealizacion.TabIndex = 70
		Me.txtFechaRealizacion.Name = "txtFechaRealizacion"
		txtFechaRecogida.OcxState = CType(resources.GetObject("txtFechaRecogida.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaRecogida.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaRecogida.Location = New System.Drawing.Point(118, 62)
		Me.txtFechaRecogida.TabIndex = 71
		Me.txtFechaRecogida.Name = "txtFechaRecogida"
		txtHoraRecogida.OcxState = CType(resources.GetObject("txtHoraRecogida.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtHoraRecogida.Size = New System.Drawing.Size(52, 19)
		Me.txtHoraRecogida.Location = New System.Drawing.Point(514, 62)
		Me.txtHoraRecogida.TabIndex = 72
		Me.txtHoraRecogida.Name = "txtHoraRecogida"
		txtFechaEntrega.OcxState = CType(resources.GetObject("txtFechaEntrega.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaEntrega.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaEntrega.Location = New System.Drawing.Point(118, 86)
		Me.txtFechaEntrega.TabIndex = 73
		Me.txtFechaEntrega.Name = "txtFechaEntrega"
		txtHoraEntrega.OcxState = CType(resources.GetObject("txtHoraEntrega.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtHoraEntrega.Size = New System.Drawing.Size(52, 19)
		Me.txtHoraEntrega.Location = New System.Drawing.Point(514, 86)
		Me.txtHoraEntrega.TabIndex = 74
		Me.txtHoraEntrega.Name = "txtHoraEntrega"
		txtPrecio.OcxState = CType(resources.GetObject("txtPrecio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtPrecio.Size = New System.Drawing.Size(85, 19)
		Me.txtPrecio.Location = New System.Drawing.Point(514, 134)
		Me.txtPrecio.TabIndex = 78
		Me.txtPrecio.Name = "txtPrecio"
		txtHorasTransporte.OcxState = CType(resources.GetObject("txtHorasTransporte.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtHorasTransporte.Size = New System.Drawing.Size(52, 19)
		Me.txtHorasTransporte.Location = New System.Drawing.Point(118, 110)
		Me.txtHorasTransporte.TabIndex = 75
		Me.txtHorasTransporte.Name = "txtHorasTransporte"
		txtImporteVtaInterna.OcxState = CType(resources.GetObject("txtImporteVtaInterna.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtImporteVtaInterna.Size = New System.Drawing.Size(85, 19)
		Me.txtImporteVtaInterna.Location = New System.Drawing.Point(514, 158)
		Me.txtImporteVtaInterna.TabIndex = 79
		Me.txtImporteVtaInterna.Name = "txtImporteVtaInterna"
		cmdModifPreu.OcxState = CType(resources.GetObject("cmdModifPreu.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdModifPreu.Size = New System.Drawing.Size(25, 23)
		Me.cmdModifPreu.Location = New System.Drawing.Point(488, 132)
		Me.cmdModifPreu.TabIndex = 139
		Me.cmdModifPreu.Name = "cmdModifPreu"
		Me.Label5.Text = "Importe vta. interna"
		Me.Label5.Size = New System.Drawing.Size(111, 15)
		Me.Label5.Location = New System.Drawing.Point(408, 162)
		Me.Label5.TabIndex = 137
		Me.Label5.Tag = "54"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label4.Text = "Kil�metros recorridos"
		Me.Label4.Size = New System.Drawing.Size(111, 15)
		Me.Label4.Location = New System.Drawing.Point(406, 114)
		Me.Label4.TabIndex = 136
		Me.Label4.Tag = "52"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label3.Text = "Horas transporte"
		Me.Label3.Size = New System.Drawing.Size(111, 15)
		Me.Label3.Location = New System.Drawing.Point(4, 114)
		Me.Label3.TabIndex = 135
		Me.Label3.Tag = "51"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.lbl39.Text = "Motivo no realizado"
		Me.lbl39.Size = New System.Drawing.Size(111, 15)
		Me.lbl39.Location = New System.Drawing.Point(4, 236)
		Me.lbl39.TabIndex = 95
		Me.lbl39.Tag = "39"
		Me.lbl39.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl39.BackColor = System.Drawing.SystemColors.Control
		Me.lbl39.Enabled = True
		Me.lbl39.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl39.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl39.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl39.UseMnemonic = True
		Me.lbl39.Visible = True
		Me.lbl39.AutoSize = False
		Me.lbl39.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl39.Name = "lbl39"
		Me.lbl44.Text = "Comentario"
		Me.lbl44.Size = New System.Drawing.Size(111, 15)
		Me.lbl44.Location = New System.Drawing.Point(4, 186)
		Me.lbl44.TabIndex = 94
		Me.lbl44.Tag = "44"
		Me.lbl44.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl44.BackColor = System.Drawing.SystemColors.Control
		Me.lbl44.Enabled = True
		Me.lbl44.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl44.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl44.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl44.UseMnemonic = True
		Me.lbl44.Visible = True
		Me.lbl44.AutoSize = False
		Me.lbl44.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl44.Name = "lbl44"
		Me.lbl35.Text = "Precio"
		Me.lbl35.Size = New System.Drawing.Size(51, 15)
		Me.lbl35.Location = New System.Drawing.Point(408, 138)
		Me.lbl35.TabIndex = 93
		Me.lbl35.Tag = "35"
		Me.lbl35.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl35.BackColor = System.Drawing.SystemColors.Control
		Me.lbl35.Enabled = True
		Me.lbl35.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl35.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl35.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl35.UseMnemonic = True
		Me.lbl35.Visible = True
		Me.lbl35.AutoSize = False
		Me.lbl35.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl35.Name = "lbl35"
		Me.lbl34.Text = "Tipo tarifa"
		Me.lbl34.Size = New System.Drawing.Size(111, 15)
		Me.lbl34.Location = New System.Drawing.Point(4, 138)
		Me.lbl34.TabIndex = 92
		Me.lbl34.Tag = "34"
		Me.lbl34.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl34.BackColor = System.Drawing.SystemColors.Control
		Me.lbl34.Enabled = True
		Me.lbl34.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl34.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl34.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl34.UseMnemonic = True
		Me.lbl34.Visible = True
		Me.lbl34.AutoSize = False
		Me.lbl34.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl34.Name = "lbl34"
		Me.lbl41.Text = "Hora entrega"
		Me.lbl41.Size = New System.Drawing.Size(111, 15)
		Me.lbl41.Location = New System.Drawing.Point(408, 90)
		Me.lbl41.TabIndex = 91
		Me.lbl41.Tag = "41"
		Me.lbl41.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl41.BackColor = System.Drawing.SystemColors.Control
		Me.lbl41.Enabled = True
		Me.lbl41.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl41.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl41.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl41.UseMnemonic = True
		Me.lbl41.Visible = True
		Me.lbl41.AutoSize = False
		Me.lbl41.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl41.Name = "lbl41"
		Me.lbl40.Text = "Fecha entrega"
		Me.lbl40.Size = New System.Drawing.Size(111, 15)
		Me.lbl40.Location = New System.Drawing.Point(4, 90)
		Me.lbl40.TabIndex = 90
		Me.lbl40.Tag = "40"
		Me.lbl40.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl40.BackColor = System.Drawing.SystemColors.Control
		Me.lbl40.Enabled = True
		Me.lbl40.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl40.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl40.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl40.UseMnemonic = True
		Me.lbl40.Visible = True
		Me.lbl40.AutoSize = False
		Me.lbl40.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl40.Name = "lbl40"
		Me.lbl43.Text = "Hora recogida"
		Me.lbl43.Size = New System.Drawing.Size(111, 15)
		Me.lbl43.Location = New System.Drawing.Point(408, 66)
		Me.lbl43.TabIndex = 89
		Me.lbl43.Tag = "43"
		Me.lbl43.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl43.BackColor = System.Drawing.SystemColors.Control
		Me.lbl43.Enabled = True
		Me.lbl43.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl43.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl43.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl43.UseMnemonic = True
		Me.lbl43.Visible = True
		Me.lbl43.AutoSize = False
		Me.lbl43.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl43.Name = "lbl43"
		Me.lbl42.Text = "Fecha recogida"
		Me.lbl42.Size = New System.Drawing.Size(111, 15)
		Me.lbl42.Location = New System.Drawing.Point(4, 66)
		Me.lbl42.TabIndex = 88
		Me.lbl42.Tag = "42"
		Me.lbl42.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl42.BackColor = System.Drawing.SystemColors.Control
		Me.lbl42.Enabled = True
		Me.lbl42.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl42.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl42.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl42.UseMnemonic = True
		Me.lbl42.Visible = True
		Me.lbl42.AutoSize = False
		Me.lbl42.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl42.Name = "lbl42"
		Me.lbl37.Text = "Fecha realizaci�n"
		Me.lbl37.Size = New System.Drawing.Size(111, 15)
		Me.lbl37.Location = New System.Drawing.Point(408, 42)
		Me.lbl37.TabIndex = 87
		Me.lbl37.Tag = "37"
		Me.lbl37.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl37.BackColor = System.Drawing.SystemColors.Control
		Me.lbl37.Enabled = True
		Me.lbl37.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl37.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl37.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl37.UseMnemonic = True
		Me.lbl37.Visible = True
		Me.lbl37.AutoSize = False
		Me.lbl37.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl37.Name = "lbl37"
		Me.lbl36.Text = "Fecha prevista"
		Me.lbl36.Size = New System.Drawing.Size(111, 15)
		Me.lbl36.Location = New System.Drawing.Point(4, 42)
		Me.lbl36.TabIndex = 86
		Me.lbl36.Tag = "36"
		Me.lbl36.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl36.BackColor = System.Drawing.SystemColors.Control
		Me.lbl36.Enabled = True
		Me.lbl36.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl36.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl36.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl36.UseMnemonic = True
		Me.lbl36.Visible = True
		Me.lbl36.AutoSize = False
		Me.lbl36.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl36.Name = "lbl36"
		Me.lbl38.Text = "Transportista"
		Me.lbl38.Size = New System.Drawing.Size(111, 15)
		Me.lbl38.Location = New System.Drawing.Point(4, 18)
		Me.lbl38.TabIndex = 85
		Me.lbl38.Tag = "38"
		Me.lbl38.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl38.BackColor = System.Drawing.SystemColors.Control
		Me.lbl38.Enabled = True
		Me.lbl38.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl38.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl38.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl38.UseMnemonic = True
		Me.lbl38.Visible = True
		Me.lbl38.AutoSize = False
		Me.lbl38.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl38.Name = "lbl38"
		TabControlPage1.OcxState = CType(resources.GetObject("TabControlPage1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage1.Size = New System.Drawing.Size(883, 337)
		Me.TabControlPage1.Location = New System.Drawing.Point(2, 22)
		Me.TabControlPage1.TabIndex = 64
		Me.TabControlPage1.Name = "TabControlPage1"
		FrameDesti.OcxState = CType(resources.GetObject("FrameDesti.OcxState"), System.Windows.Forms.AxHost.State)
		Me.FrameDesti.Size = New System.Drawing.Size(869, 165)
		Me.FrameDesti.Location = New System.Drawing.Point(8, 166)
		Me.FrameDesti.TabIndex = 96
		Me.FrameDesti.Name = "FrameDesti"
		Me.txtSubdireccionDestino.AutoSize = False
		Me.txtSubdireccionDestino.Size = New System.Drawing.Size(487, 19)
		Me.txtSubdireccionDestino.Location = New System.Drawing.Point(120, 62)
		Me.txtSubdireccionDestino.Maxlength = 100
		Me.txtSubdireccionDestino.TabIndex = 32
		Me.txtSubdireccionDestino.Tag = "57"
		Me.txtSubdireccionDestino.AcceptsReturn = True
		Me.txtSubdireccionDestino.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSubdireccionDestino.BackColor = System.Drawing.SystemColors.Window
		Me.txtSubdireccionDestino.CausesValidation = True
		Me.txtSubdireccionDestino.Enabled = True
		Me.txtSubdireccionDestino.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSubdireccionDestino.HideSelection = True
		Me.txtSubdireccionDestino.ReadOnly = False
		Me.txtSubdireccionDestino.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSubdireccionDestino.MultiLine = False
		Me.txtSubdireccionDestino.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSubdireccionDestino.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSubdireccionDestino.TabStop = True
		Me.txtSubdireccionDestino.Visible = True
		Me.txtSubdireccionDestino.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSubdireccionDestino.Name = "txtSubdireccionDestino"
		Me.txtObservacionesDestino.AutoSize = False
		Me.txtObservacionesDestino.Size = New System.Drawing.Size(485, 19)
		Me.txtObservacionesDestino.Location = New System.Drawing.Point(120, 134)
		Me.txtObservacionesDestino.Maxlength = 80
		Me.txtObservacionesDestino.TabIndex = 39
		Me.txtObservacionesDestino.Tag = "50"
		Me.txtObservacionesDestino.AcceptsReturn = True
		Me.txtObservacionesDestino.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacionesDestino.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacionesDestino.CausesValidation = True
		Me.txtObservacionesDestino.Enabled = True
		Me.txtObservacionesDestino.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacionesDestino.HideSelection = True
		Me.txtObservacionesDestino.ReadOnly = False
		Me.txtObservacionesDestino.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacionesDestino.MultiLine = False
		Me.txtObservacionesDestino.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacionesDestino.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservacionesDestino.TabStop = True
		Me.txtObservacionesDestino.Visible = True
		Me.txtObservacionesDestino.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacionesDestino.Name = "txtObservacionesDestino"
		Me.txtEntidadEntrega.AutoSize = False
		Me.txtEntidadEntrega.Size = New System.Drawing.Size(83, 19)
		Me.txtEntidadEntrega.Location = New System.Drawing.Point(120, 14)
		Me.txtEntidadEntrega.Maxlength = 8
		Me.txtEntidadEntrega.TabIndex = 29
		Me.txtEntidadEntrega.Tag = "32"
		Me.txtEntidadEntrega.AcceptsReturn = True
		Me.txtEntidadEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEntidadEntrega.BackColor = System.Drawing.SystemColors.Window
		Me.txtEntidadEntrega.CausesValidation = True
		Me.txtEntidadEntrega.Enabled = True
		Me.txtEntidadEntrega.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEntidadEntrega.HideSelection = True
		Me.txtEntidadEntrega.ReadOnly = False
		Me.txtEntidadEntrega.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEntidadEntrega.MultiLine = False
		Me.txtEntidadEntrega.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEntidadEntrega.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEntidadEntrega.TabStop = True
		Me.txtEntidadEntrega.Visible = True
		Me.txtEntidadEntrega.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEntidadEntrega.Name = "txtEntidadEntrega"
		Me.txtSucursalEntrega.AutoSize = False
		Me.txtSucursalEntrega.Size = New System.Drawing.Size(31, 19)
		Me.txtSucursalEntrega.Location = New System.Drawing.Point(312, 14)
		Me.txtSucursalEntrega.Maxlength = 3
		Me.txtSucursalEntrega.TabIndex = 30
		Me.txtSucursalEntrega.Tag = "33"
		Me.txtSucursalEntrega.AcceptsReturn = True
		Me.txtSucursalEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSucursalEntrega.BackColor = System.Drawing.SystemColors.Window
		Me.txtSucursalEntrega.CausesValidation = True
		Me.txtSucursalEntrega.Enabled = True
		Me.txtSucursalEntrega.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSucursalEntrega.HideSelection = True
		Me.txtSucursalEntrega.ReadOnly = False
		Me.txtSucursalEntrega.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSucursalEntrega.MultiLine = False
		Me.txtSucursalEntrega.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSucursalEntrega.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSucursalEntrega.TabStop = True
		Me.txtSucursalEntrega.Visible = True
		Me.txtSucursalEntrega.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSucursalEntrega.Name = "txtSucursalEntrega"
		Me.Text33.AutoSize = False
		Me.Text33.BackColor = System.Drawing.Color.White
		Me.Text33.Enabled = False
		Me.Text33.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text33.Size = New System.Drawing.Size(262, 19)
		Me.Text33.Location = New System.Drawing.Point(346, 14)
		Me.Text33.TabIndex = 99
		Me.Text33.Tag = "^33"
		Me.Text33.AcceptsReturn = True
		Me.Text33.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text33.CausesValidation = True
		Me.Text33.HideSelection = True
		Me.Text33.ReadOnly = False
		Me.Text33.Maxlength = 0
		Me.Text33.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text33.MultiLine = False
		Me.Text33.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text33.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text33.TabStop = True
		Me.Text33.Visible = True
		Me.Text33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text33.Name = "Text33"
		Me.txtPuntoEntrega.AutoSize = False
		Me.txtPuntoEntrega.Size = New System.Drawing.Size(42, 19)
		Me.txtPuntoEntrega.Location = New System.Drawing.Point(120, 14)
		Me.txtPuntoEntrega.Maxlength = 4
		Me.txtPuntoEntrega.TabIndex = 28
		Me.txtPuntoEntrega.Tag = "25"
		Me.txtPuntoEntrega.AcceptsReturn = True
		Me.txtPuntoEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPuntoEntrega.BackColor = System.Drawing.SystemColors.Window
		Me.txtPuntoEntrega.CausesValidation = True
		Me.txtPuntoEntrega.Enabled = True
		Me.txtPuntoEntrega.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPuntoEntrega.HideSelection = True
		Me.txtPuntoEntrega.ReadOnly = False
		Me.txtPuntoEntrega.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPuntoEntrega.MultiLine = False
		Me.txtPuntoEntrega.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPuntoEntrega.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPuntoEntrega.TabStop = True
		Me.txtPuntoEntrega.Visible = True
		Me.txtPuntoEntrega.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPuntoEntrega.Name = "txtPuntoEntrega"
		Me.Text25.AutoSize = False
		Me.Text25.BackColor = System.Drawing.Color.White
		Me.Text25.Enabled = False
		Me.Text25.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text25.Size = New System.Drawing.Size(440, 19)
		Me.Text25.Location = New System.Drawing.Point(165, 14)
		Me.Text25.TabIndex = 98
		Me.Text25.Tag = "^25"
		Me.Text25.AcceptsReturn = True
		Me.Text25.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text25.CausesValidation = True
		Me.Text25.HideSelection = True
		Me.Text25.ReadOnly = False
		Me.Text25.Maxlength = 0
		Me.Text25.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text25.MultiLine = False
		Me.Text25.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text25.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text25.TabStop = True
		Me.Text25.Visible = True
		Me.Text25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text25.Name = "Text25"
		Me.txtDireccionEntrega.AutoSize = False
		Me.txtDireccionEntrega.Size = New System.Drawing.Size(487, 19)
		Me.txtDireccionEntrega.Location = New System.Drawing.Point(120, 38)
		Me.txtDireccionEntrega.Maxlength = 100
		Me.txtDireccionEntrega.TabIndex = 31
		Me.txtDireccionEntrega.Tag = "26"
		Me.txtDireccionEntrega.AcceptsReturn = True
		Me.txtDireccionEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDireccionEntrega.BackColor = System.Drawing.SystemColors.Window
		Me.txtDireccionEntrega.CausesValidation = True
		Me.txtDireccionEntrega.Enabled = True
		Me.txtDireccionEntrega.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDireccionEntrega.HideSelection = True
		Me.txtDireccionEntrega.ReadOnly = False
		Me.txtDireccionEntrega.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDireccionEntrega.MultiLine = False
		Me.txtDireccionEntrega.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDireccionEntrega.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDireccionEntrega.TabStop = True
		Me.txtDireccionEntrega.Visible = True
		Me.txtDireccionEntrega.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDireccionEntrega.Name = "txtDireccionEntrega"
		Me.txtPoblacionEntrega.AutoSize = False
		Me.txtPoblacionEntrega.Size = New System.Drawing.Size(62, 19)
		Me.txtPoblacionEntrega.Location = New System.Drawing.Point(120, 86)
		Me.txtPoblacionEntrega.Maxlength = 6
		Me.txtPoblacionEntrega.TabIndex = 33
		Me.txtPoblacionEntrega.Tag = "27"
		Me.txtPoblacionEntrega.AcceptsReturn = True
		Me.txtPoblacionEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPoblacionEntrega.BackColor = System.Drawing.SystemColors.Window
		Me.txtPoblacionEntrega.CausesValidation = True
		Me.txtPoblacionEntrega.Enabled = True
		Me.txtPoblacionEntrega.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPoblacionEntrega.HideSelection = True
		Me.txtPoblacionEntrega.ReadOnly = False
		Me.txtPoblacionEntrega.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPoblacionEntrega.MultiLine = False
		Me.txtPoblacionEntrega.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPoblacionEntrega.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPoblacionEntrega.TabStop = True
		Me.txtPoblacionEntrega.Visible = True
		Me.txtPoblacionEntrega.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPoblacionEntrega.Name = "txtPoblacionEntrega"
		Me.Text27.AutoSize = False
		Me.Text27.BackColor = System.Drawing.Color.White
		Me.Text27.Enabled = False
		Me.Text27.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text27.Size = New System.Drawing.Size(259, 19)
		Me.Text27.Location = New System.Drawing.Point(185, 86)
		Me.Text27.TabIndex = 97
		Me.Text27.Tag = "^27"
		Me.Text27.AcceptsReturn = True
		Me.Text27.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text27.CausesValidation = True
		Me.Text27.HideSelection = True
		Me.Text27.ReadOnly = False
		Me.Text27.Maxlength = 0
		Me.Text27.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text27.MultiLine = False
		Me.Text27.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text27.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text27.TabStop = True
		Me.Text27.Visible = True
		Me.Text27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text27.Name = "Text27"
		Me.txtCpEntrega.AutoSize = False
		Me.txtCpEntrega.Size = New System.Drawing.Size(62, 19)
		Me.txtCpEntrega.Location = New System.Drawing.Point(476, 86)
		Me.txtCpEntrega.Maxlength = 6
		Me.txtCpEntrega.TabIndex = 34
		Me.txtCpEntrega.Tag = "28"
		Me.txtCpEntrega.AcceptsReturn = True
		Me.txtCpEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCpEntrega.BackColor = System.Drawing.SystemColors.Window
		Me.txtCpEntrega.CausesValidation = True
		Me.txtCpEntrega.Enabled = True
		Me.txtCpEntrega.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCpEntrega.HideSelection = True
		Me.txtCpEntrega.ReadOnly = False
		Me.txtCpEntrega.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCpEntrega.MultiLine = False
		Me.txtCpEntrega.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCpEntrega.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCpEntrega.TabStop = True
		Me.txtCpEntrega.Visible = True
		Me.txtCpEntrega.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCpEntrega.Name = "txtCpEntrega"
		Me.txtTelefonoEntrega.AutoSize = False
		Me.txtTelefonoEntrega.Size = New System.Drawing.Size(104, 19)
		Me.txtTelefonoEntrega.Location = New System.Drawing.Point(652, 86)
		Me.txtTelefonoEntrega.Maxlength = 10
		Me.txtTelefonoEntrega.TabIndex = 35
		Me.txtTelefonoEntrega.Tag = "29"
		Me.txtTelefonoEntrega.AcceptsReturn = True
		Me.txtTelefonoEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTelefonoEntrega.BackColor = System.Drawing.SystemColors.Window
		Me.txtTelefonoEntrega.CausesValidation = True
		Me.txtTelefonoEntrega.Enabled = True
		Me.txtTelefonoEntrega.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTelefonoEntrega.HideSelection = True
		Me.txtTelefonoEntrega.ReadOnly = False
		Me.txtTelefonoEntrega.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTelefonoEntrega.MultiLine = False
		Me.txtTelefonoEntrega.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTelefonoEntrega.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTelefonoEntrega.TabStop = True
		Me.txtTelefonoEntrega.Visible = True
		Me.txtTelefonoEntrega.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTelefonoEntrega.Name = "txtTelefonoEntrega"
		Me.txtContactoEntrega.AutoSize = False
		Me.txtContactoEntrega.Size = New System.Drawing.Size(307, 19)
		Me.txtContactoEntrega.Location = New System.Drawing.Point(120, 110)
		Me.txtContactoEntrega.Maxlength = 80
		Me.txtContactoEntrega.TabIndex = 36
		Me.txtContactoEntrega.Tag = "30"
		Me.txtContactoEntrega.AcceptsReturn = True
		Me.txtContactoEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtContactoEntrega.BackColor = System.Drawing.SystemColors.Window
		Me.txtContactoEntrega.CausesValidation = True
		Me.txtContactoEntrega.Enabled = True
		Me.txtContactoEntrega.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtContactoEntrega.HideSelection = True
		Me.txtContactoEntrega.ReadOnly = False
		Me.txtContactoEntrega.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtContactoEntrega.MultiLine = False
		Me.txtContactoEntrega.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtContactoEntrega.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtContactoEntrega.TabStop = True
		Me.txtContactoEntrega.Visible = True
		Me.txtContactoEntrega.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtContactoEntrega.Name = "txtContactoEntrega"
		Me.txtHorarioEntrega.AutoSize = False
		Me.txtHorarioEntrega.Size = New System.Drawing.Size(385, 19)
		Me.txtHorarioEntrega.Location = New System.Drawing.Point(476, 110)
		Me.txtHorarioEntrega.Maxlength = 80
		Me.txtHorarioEntrega.TabIndex = 37
		Me.txtHorarioEntrega.Tag = "31"
		Me.txtHorarioEntrega.AcceptsReturn = True
		Me.txtHorarioEntrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtHorarioEntrega.BackColor = System.Drawing.SystemColors.Window
		Me.txtHorarioEntrega.CausesValidation = True
		Me.txtHorarioEntrega.Enabled = True
		Me.txtHorarioEntrega.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtHorarioEntrega.HideSelection = True
		Me.txtHorarioEntrega.ReadOnly = False
		Me.txtHorarioEntrega.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtHorarioEntrega.MultiLine = False
		Me.txtHorarioEntrega.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtHorarioEntrega.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtHorarioEntrega.TabStop = True
		Me.txtHorarioEntrega.Visible = True
		Me.txtHorarioEntrega.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtHorarioEntrega.Name = "txtHorarioEntrega"
		frDesti.OcxState = CType(resources.GetObject("frDesti.OcxState"), System.Windows.Forms.AxHost.State)
		Me.frDesti.Size = New System.Drawing.Size(85, 65)
		Me.frDesti.Location = New System.Drawing.Point(642, 8)
		Me.frDesti.TabIndex = 128
		Me.frDesti.Name = "frDesti"
		Me.txtDesti.AutoSize = False
		Me.txtDesti.BackColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.txtDesti.Size = New System.Drawing.Size(11, 19)
		Me.txtDesti.Location = New System.Drawing.Point(4, 4)
		Me.txtDesti.Maxlength = 1
		Me.txtDesti.TabIndex = 129
		Me.txtDesti.Tag = "47"
		Me.txtDesti.AcceptsReturn = True
		Me.txtDesti.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDesti.CausesValidation = True
		Me.txtDesti.Enabled = True
		Me.txtDesti.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDesti.HideSelection = True
		Me.txtDesti.ReadOnly = False
		Me.txtDesti.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDesti.MultiLine = False
		Me.txtDesti.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDesti.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDesti.TabStop = True
		Me.txtDesti.Visible = True
		Me.txtDesti.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDesti.Name = "txtDesti"
		_optDesti_1.OcxState = CType(resources.GetObject("_optDesti_1.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optDesti_1.Size = New System.Drawing.Size(67, 21)
		Me._optDesti_1.Location = New System.Drawing.Point(16, 10)
		Me._optDesti_1.TabIndex = 27
		Me._optDesti_1.Name = "_optDesti_1"
		_optDesti_2.OcxState = CType(resources.GetObject("_optDesti_2.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optDesti_2.Size = New System.Drawing.Size(63, 21)
		Me._optDesti_2.Location = New System.Drawing.Point(16, 32)
		Me._optDesti_2.TabIndex = 130
		Me._optDesti_2.Name = "_optDesti_2"
		chkCopiaRegistrada.OcxState = CType(resources.GetObject("chkCopiaRegistrada.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkCopiaRegistrada.Size = New System.Drawing.Size(109, 13)
		Me.chkCopiaRegistrada.Location = New System.Drawing.Point(654, 138)
		Me.chkCopiaRegistrada.TabIndex = 38
		Me.chkCopiaRegistrada.Name = "chkCopiaRegistrada"
		Me.Label9.Text = "Subirecci�n entrega"
		Me.Label9.Size = New System.Drawing.Size(111, 15)
		Me.Label9.Location = New System.Drawing.Point(6, 66)
		Me.Label9.TabIndex = 143
		Me.Label9.Tag = "57"
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label9.BackColor = System.Drawing.SystemColors.Control
		Me.Label9.Enabled = True
		Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.UseMnemonic = True
		Me.Label9.Visible = True
		Me.Label9.AutoSize = False
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.Name = "Label9"
		Me.Label2.Text = "Observaciones"
		Me.Label2.Size = New System.Drawing.Size(105, 15)
		Me.Label2.Location = New System.Drawing.Point(8, 138)
		Me.Label2.TabIndex = 132
		Me.Label2.Tag = "50"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.lbl32.Text = "Entidad entrega"
		Me.lbl32.Size = New System.Drawing.Size(111, 15)
		Me.lbl32.Location = New System.Drawing.Point(6, 18)
		Me.lbl32.TabIndex = 108
		Me.lbl32.Tag = "32"
		Me.lbl32.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl32.BackColor = System.Drawing.SystemColors.Control
		Me.lbl32.Enabled = True
		Me.lbl32.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl32.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl32.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl32.UseMnemonic = True
		Me.lbl32.Visible = True
		Me.lbl32.AutoSize = False
		Me.lbl32.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl32.Name = "lbl32"
		Me.lbl33.Text = "Sucursal entrega"
		Me.lbl33.Size = New System.Drawing.Size(111, 15)
		Me.lbl33.Location = New System.Drawing.Point(210, 18)
		Me.lbl33.TabIndex = 107
		Me.lbl33.Tag = "33"
		Me.lbl33.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl33.BackColor = System.Drawing.SystemColors.Control
		Me.lbl33.Enabled = True
		Me.lbl33.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl33.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl33.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl33.UseMnemonic = True
		Me.lbl33.Visible = True
		Me.lbl33.AutoSize = False
		Me.lbl33.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl33.Name = "lbl33"
		Me.lbl25.Text = "Punto entrega"
		Me.lbl25.Size = New System.Drawing.Size(111, 15)
		Me.lbl25.Location = New System.Drawing.Point(6, 18)
		Me.lbl25.TabIndex = 106
		Me.lbl25.Tag = "25"
		Me.lbl25.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl25.BackColor = System.Drawing.SystemColors.Control
		Me.lbl25.Enabled = True
		Me.lbl25.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl25.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl25.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl25.UseMnemonic = True
		Me.lbl25.Visible = True
		Me.lbl25.AutoSize = False
		Me.lbl25.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl25.Name = "lbl25"
		Me.lbl26.Text = "Direcci�n entrega"
		Me.lbl26.Size = New System.Drawing.Size(111, 15)
		Me.lbl26.Location = New System.Drawing.Point(6, 42)
		Me.lbl26.TabIndex = 105
		Me.lbl26.Tag = "26"
		Me.lbl26.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl26.BackColor = System.Drawing.SystemColors.Control
		Me.lbl26.Enabled = True
		Me.lbl26.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl26.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl26.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl26.UseMnemonic = True
		Me.lbl26.Visible = True
		Me.lbl26.AutoSize = False
		Me.lbl26.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl26.Name = "lbl26"
		Me.lbl27.Text = "Poblaci�n entrega"
		Me.lbl27.Size = New System.Drawing.Size(111, 15)
		Me.lbl27.Location = New System.Drawing.Point(6, 90)
		Me.lbl27.TabIndex = 104
		Me.lbl27.Tag = "27"
		Me.lbl27.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl27.BackColor = System.Drawing.SystemColors.Control
		Me.lbl27.Enabled = True
		Me.lbl27.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl27.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl27.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl27.UseMnemonic = True
		Me.lbl27.Visible = True
		Me.lbl27.AutoSize = False
		Me.lbl27.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl27.Name = "lbl27"
		Me.lbl28.Text = "CP entrega"
		Me.lbl28.Size = New System.Drawing.Size(87, 15)
		Me.lbl28.Location = New System.Drawing.Point(458, 90)
		Me.lbl28.TabIndex = 103
		Me.lbl28.Tag = "28"
		Me.lbl28.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl28.BackColor = System.Drawing.SystemColors.Control
		Me.lbl28.Enabled = True
		Me.lbl28.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl28.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl28.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl28.UseMnemonic = True
		Me.lbl28.Visible = True
		Me.lbl28.AutoSize = False
		Me.lbl28.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl28.Name = "lbl28"
		Me.lbl29.Text = "Tel�fono entrega"
		Me.lbl29.Size = New System.Drawing.Size(93, 15)
		Me.lbl29.Location = New System.Drawing.Point(554, 90)
		Me.lbl29.TabIndex = 102
		Me.lbl29.Tag = "29"
		Me.lbl29.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl29.BackColor = System.Drawing.SystemColors.Control
		Me.lbl29.Enabled = True
		Me.lbl29.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl29.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl29.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl29.UseMnemonic = True
		Me.lbl29.Visible = True
		Me.lbl29.AutoSize = False
		Me.lbl29.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl29.Name = "lbl29"
		Me.lbl30.Text = "Contacto entrega"
		Me.lbl30.Size = New System.Drawing.Size(111, 15)
		Me.lbl30.Location = New System.Drawing.Point(8, 114)
		Me.lbl30.TabIndex = 101
		Me.lbl30.Tag = "30"
		Me.lbl30.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl30.BackColor = System.Drawing.SystemColors.Control
		Me.lbl30.Enabled = True
		Me.lbl30.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl30.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl30.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl30.UseMnemonic = True
		Me.lbl30.Visible = True
		Me.lbl30.AutoSize = False
		Me.lbl30.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl30.Name = "lbl30"
		Me.lbl31.Text = "Horario entrega"
		Me.lbl31.Size = New System.Drawing.Size(65, 15)
		Me.lbl31.Location = New System.Drawing.Point(438, 114)
		Me.lbl31.TabIndex = 100
		Me.lbl31.Tag = "31"
		Me.lbl31.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl31.BackColor = System.Drawing.SystemColors.Control
		Me.lbl31.Enabled = True
		Me.lbl31.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl31.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl31.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl31.UseMnemonic = True
		Me.lbl31.Visible = True
		Me.lbl31.AutoSize = False
		Me.lbl31.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl31.Name = "lbl31"
		FrameOrigen.OcxState = CType(resources.GetObject("FrameOrigen.OcxState"), System.Windows.Forms.AxHost.State)
		Me.FrameOrigen.Size = New System.Drawing.Size(869, 165)
		Me.FrameOrigen.Location = New System.Drawing.Point(8, 0)
		Me.FrameOrigen.TabIndex = 109
		Me.FrameOrigen.Name = "FrameOrigen"
		Me.txtSubdireccionOrigen.AutoSize = False
		Me.txtSubdireccionOrigen.Size = New System.Drawing.Size(487, 19)
		Me.txtSubdireccionOrigen.Location = New System.Drawing.Point(122, 64)
		Me.txtSubdireccionOrigen.Maxlength = 100
		Me.txtSubdireccionOrigen.TabIndex = 20
		Me.txtSubdireccionOrigen.Tag = "56"
		Me.txtSubdireccionOrigen.AcceptsReturn = True
		Me.txtSubdireccionOrigen.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSubdireccionOrigen.BackColor = System.Drawing.SystemColors.Window
		Me.txtSubdireccionOrigen.CausesValidation = True
		Me.txtSubdireccionOrigen.Enabled = True
		Me.txtSubdireccionOrigen.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSubdireccionOrigen.HideSelection = True
		Me.txtSubdireccionOrigen.ReadOnly = False
		Me.txtSubdireccionOrigen.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSubdireccionOrigen.MultiLine = False
		Me.txtSubdireccionOrigen.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSubdireccionOrigen.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSubdireccionOrigen.TabStop = True
		Me.txtSubdireccionOrigen.Visible = True
		Me.txtSubdireccionOrigen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSubdireccionOrigen.Name = "txtSubdireccionOrigen"
		Me.txtObservacionesorigen.AutoSize = False
		Me.txtObservacionesorigen.Size = New System.Drawing.Size(485, 19)
		Me.txtObservacionesorigen.Location = New System.Drawing.Point(122, 136)
		Me.txtObservacionesorigen.Maxlength = 80
		Me.txtObservacionesorigen.TabIndex = 26
		Me.txtObservacionesorigen.Tag = "49"
		Me.txtObservacionesorigen.AcceptsReturn = True
		Me.txtObservacionesorigen.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacionesorigen.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacionesorigen.CausesValidation = True
		Me.txtObservacionesorigen.Enabled = True
		Me.txtObservacionesorigen.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacionesorigen.HideSelection = True
		Me.txtObservacionesorigen.ReadOnly = False
		Me.txtObservacionesorigen.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacionesorigen.MultiLine = False
		Me.txtObservacionesorigen.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacionesorigen.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservacionesorigen.TabStop = True
		Me.txtObservacionesorigen.Visible = True
		Me.txtObservacionesorigen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacionesorigen.Name = "txtObservacionesorigen"
		Me.txtPuntoRecepcion.AutoSize = False
		Me.txtPuntoRecepcion.Size = New System.Drawing.Size(42, 19)
		Me.txtPuntoRecepcion.Location = New System.Drawing.Point(122, 16)
		Me.txtPuntoRecepcion.Maxlength = 4
		Me.txtPuntoRecepcion.TabIndex = 16
		Me.txtPuntoRecepcion.Tag = "16"
		Me.txtPuntoRecepcion.AcceptsReturn = True
		Me.txtPuntoRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPuntoRecepcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtPuntoRecepcion.CausesValidation = True
		Me.txtPuntoRecepcion.Enabled = True
		Me.txtPuntoRecepcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPuntoRecepcion.HideSelection = True
		Me.txtPuntoRecepcion.ReadOnly = False
		Me.txtPuntoRecepcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPuntoRecepcion.MultiLine = False
		Me.txtPuntoRecepcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPuntoRecepcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPuntoRecepcion.TabStop = True
		Me.txtPuntoRecepcion.Visible = True
		Me.txtPuntoRecepcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPuntoRecepcion.Name = "txtPuntoRecepcion"
		Me.Text16.AutoSize = False
		Me.Text16.BackColor = System.Drawing.Color.White
		Me.Text16.Enabled = False
		Me.Text16.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text16.Size = New System.Drawing.Size(442, 19)
		Me.Text16.Location = New System.Drawing.Point(167, 16)
		Me.Text16.TabIndex = 112
		Me.Text16.Tag = "^16"
		Me.Text16.AcceptsReturn = True
		Me.Text16.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text16.CausesValidation = True
		Me.Text16.HideSelection = True
		Me.Text16.ReadOnly = False
		Me.Text16.Maxlength = 0
		Me.Text16.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text16.MultiLine = False
		Me.Text16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text16.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text16.TabStop = True
		Me.Text16.Visible = True
		Me.Text16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text16.Name = "Text16"
		Me.txtDireccionRecepcion.AutoSize = False
		Me.txtDireccionRecepcion.Size = New System.Drawing.Size(487, 19)
		Me.txtDireccionRecepcion.Location = New System.Drawing.Point(122, 40)
		Me.txtDireccionRecepcion.Maxlength = 100
		Me.txtDireccionRecepcion.TabIndex = 19
		Me.txtDireccionRecepcion.Tag = "17"
		Me.txtDireccionRecepcion.AcceptsReturn = True
		Me.txtDireccionRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDireccionRecepcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtDireccionRecepcion.CausesValidation = True
		Me.txtDireccionRecepcion.Enabled = True
		Me.txtDireccionRecepcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDireccionRecepcion.HideSelection = True
		Me.txtDireccionRecepcion.ReadOnly = False
		Me.txtDireccionRecepcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDireccionRecepcion.MultiLine = False
		Me.txtDireccionRecepcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDireccionRecepcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDireccionRecepcion.TabStop = True
		Me.txtDireccionRecepcion.Visible = True
		Me.txtDireccionRecepcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDireccionRecepcion.Name = "txtDireccionRecepcion"
		Me.txtPoblacionRecepcion.AutoSize = False
		Me.txtPoblacionRecepcion.Size = New System.Drawing.Size(62, 19)
		Me.txtPoblacionRecepcion.Location = New System.Drawing.Point(122, 88)
		Me.txtPoblacionRecepcion.Maxlength = 6
		Me.txtPoblacionRecepcion.TabIndex = 21
		Me.txtPoblacionRecepcion.Tag = "18"
		Me.txtPoblacionRecepcion.AcceptsReturn = True
		Me.txtPoblacionRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPoblacionRecepcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtPoblacionRecepcion.CausesValidation = True
		Me.txtPoblacionRecepcion.Enabled = True
		Me.txtPoblacionRecepcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPoblacionRecepcion.HideSelection = True
		Me.txtPoblacionRecepcion.ReadOnly = False
		Me.txtPoblacionRecepcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPoblacionRecepcion.MultiLine = False
		Me.txtPoblacionRecepcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPoblacionRecepcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPoblacionRecepcion.TabStop = True
		Me.txtPoblacionRecepcion.Visible = True
		Me.txtPoblacionRecepcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPoblacionRecepcion.Name = "txtPoblacionRecepcion"
		Me.Text18.AutoSize = False
		Me.Text18.BackColor = System.Drawing.Color.White
		Me.Text18.Enabled = False
		Me.Text18.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text18.Size = New System.Drawing.Size(259, 19)
		Me.Text18.Location = New System.Drawing.Point(187, 88)
		Me.Text18.TabIndex = 111
		Me.Text18.Tag = "^18"
		Me.Text18.AcceptsReturn = True
		Me.Text18.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text18.CausesValidation = True
		Me.Text18.HideSelection = True
		Me.Text18.ReadOnly = False
		Me.Text18.Maxlength = 0
		Me.Text18.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text18.MultiLine = False
		Me.Text18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text18.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text18.TabStop = True
		Me.Text18.Visible = True
		Me.Text18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text18.Name = "Text18"
		Me.txtCpRecepcion.AutoSize = False
		Me.txtCpRecepcion.Size = New System.Drawing.Size(62, 19)
		Me.txtCpRecepcion.Location = New System.Drawing.Point(476, 88)
		Me.txtCpRecepcion.Maxlength = 6
		Me.txtCpRecepcion.TabIndex = 22
		Me.txtCpRecepcion.Tag = "19"
		Me.txtCpRecepcion.AcceptsReturn = True
		Me.txtCpRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCpRecepcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtCpRecepcion.CausesValidation = True
		Me.txtCpRecepcion.Enabled = True
		Me.txtCpRecepcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCpRecepcion.HideSelection = True
		Me.txtCpRecepcion.ReadOnly = False
		Me.txtCpRecepcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCpRecepcion.MultiLine = False
		Me.txtCpRecepcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCpRecepcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCpRecepcion.TabStop = True
		Me.txtCpRecepcion.Visible = True
		Me.txtCpRecepcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCpRecepcion.Name = "txtCpRecepcion"
		Me.txtTelefonoRecepcion.AutoSize = False
		Me.txtTelefonoRecepcion.Size = New System.Drawing.Size(104, 19)
		Me.txtTelefonoRecepcion.Location = New System.Drawing.Point(652, 88)
		Me.txtTelefonoRecepcion.Maxlength = 10
		Me.txtTelefonoRecepcion.TabIndex = 23
		Me.txtTelefonoRecepcion.Tag = "20"
		Me.txtTelefonoRecepcion.AcceptsReturn = True
		Me.txtTelefonoRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTelefonoRecepcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtTelefonoRecepcion.CausesValidation = True
		Me.txtTelefonoRecepcion.Enabled = True
		Me.txtTelefonoRecepcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTelefonoRecepcion.HideSelection = True
		Me.txtTelefonoRecepcion.ReadOnly = False
		Me.txtTelefonoRecepcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTelefonoRecepcion.MultiLine = False
		Me.txtTelefonoRecepcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTelefonoRecepcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTelefonoRecepcion.TabStop = True
		Me.txtTelefonoRecepcion.Visible = True
		Me.txtTelefonoRecepcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTelefonoRecepcion.Name = "txtTelefonoRecepcion"
		Me.txtContactoRecepcion.AutoSize = False
		Me.txtContactoRecepcion.Size = New System.Drawing.Size(303, 19)
		Me.txtContactoRecepcion.Location = New System.Drawing.Point(122, 112)
		Me.txtContactoRecepcion.Maxlength = 80
		Me.txtContactoRecepcion.TabIndex = 24
		Me.txtContactoRecepcion.Tag = "21"
		Me.txtContactoRecepcion.AcceptsReturn = True
		Me.txtContactoRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtContactoRecepcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtContactoRecepcion.CausesValidation = True
		Me.txtContactoRecepcion.Enabled = True
		Me.txtContactoRecepcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtContactoRecepcion.HideSelection = True
		Me.txtContactoRecepcion.ReadOnly = False
		Me.txtContactoRecepcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtContactoRecepcion.MultiLine = False
		Me.txtContactoRecepcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtContactoRecepcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtContactoRecepcion.TabStop = True
		Me.txtContactoRecepcion.Visible = True
		Me.txtContactoRecepcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtContactoRecepcion.Name = "txtContactoRecepcion"
		Me.txtHorarioRecepcion.AutoSize = False
		Me.txtHorarioRecepcion.Size = New System.Drawing.Size(385, 19)
		Me.txtHorarioRecepcion.Location = New System.Drawing.Point(476, 112)
		Me.txtHorarioRecepcion.Maxlength = 80
		Me.txtHorarioRecepcion.TabIndex = 25
		Me.txtHorarioRecepcion.Tag = "22"
		Me.txtHorarioRecepcion.AcceptsReturn = True
		Me.txtHorarioRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtHorarioRecepcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtHorarioRecepcion.CausesValidation = True
		Me.txtHorarioRecepcion.Enabled = True
		Me.txtHorarioRecepcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtHorarioRecepcion.HideSelection = True
		Me.txtHorarioRecepcion.ReadOnly = False
		Me.txtHorarioRecepcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtHorarioRecepcion.MultiLine = False
		Me.txtHorarioRecepcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtHorarioRecepcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtHorarioRecepcion.TabStop = True
		Me.txtHorarioRecepcion.Visible = True
		Me.txtHorarioRecepcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtHorarioRecepcion.Name = "txtHorarioRecepcion"
		Me.txtEntidadRecepcion.AutoSize = False
		Me.txtEntidadRecepcion.Size = New System.Drawing.Size(83, 19)
		Me.txtEntidadRecepcion.Location = New System.Drawing.Point(122, 16)
		Me.txtEntidadRecepcion.Maxlength = 8
		Me.txtEntidadRecepcion.TabIndex = 17
		Me.txtEntidadRecepcion.Tag = "23"
		Me.txtEntidadRecepcion.AcceptsReturn = True
		Me.txtEntidadRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEntidadRecepcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtEntidadRecepcion.CausesValidation = True
		Me.txtEntidadRecepcion.Enabled = True
		Me.txtEntidadRecepcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEntidadRecepcion.HideSelection = True
		Me.txtEntidadRecepcion.ReadOnly = False
		Me.txtEntidadRecepcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEntidadRecepcion.MultiLine = False
		Me.txtEntidadRecepcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEntidadRecepcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEntidadRecepcion.TabStop = True
		Me.txtEntidadRecepcion.Visible = True
		Me.txtEntidadRecepcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEntidadRecepcion.Name = "txtEntidadRecepcion"
		Me.txtSucursalRecepcion.AutoSize = False
		Me.txtSucursalRecepcion.Size = New System.Drawing.Size(31, 19)
		Me.txtSucursalRecepcion.Location = New System.Drawing.Point(318, 16)
		Me.txtSucursalRecepcion.Maxlength = 3
		Me.txtSucursalRecepcion.TabIndex = 18
		Me.txtSucursalRecepcion.Tag = "24"
		Me.txtSucursalRecepcion.AcceptsReturn = True
		Me.txtSucursalRecepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSucursalRecepcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtSucursalRecepcion.CausesValidation = True
		Me.txtSucursalRecepcion.Enabled = True
		Me.txtSucursalRecepcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSucursalRecepcion.HideSelection = True
		Me.txtSucursalRecepcion.ReadOnly = False
		Me.txtSucursalRecepcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSucursalRecepcion.MultiLine = False
		Me.txtSucursalRecepcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSucursalRecepcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSucursalRecepcion.TabStop = True
		Me.txtSucursalRecepcion.Visible = True
		Me.txtSucursalRecepcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSucursalRecepcion.Name = "txtSucursalRecepcion"
		Me.Text24.AutoSize = False
		Me.Text24.BackColor = System.Drawing.Color.White
		Me.Text24.Enabled = False
		Me.Text24.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text24.Size = New System.Drawing.Size(259, 19)
		Me.Text24.Location = New System.Drawing.Point(352, 16)
		Me.Text24.TabIndex = 110
		Me.Text24.Tag = "^24"
		Me.Text24.AcceptsReturn = True
		Me.Text24.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text24.CausesValidation = True
		Me.Text24.HideSelection = True
		Me.Text24.ReadOnly = False
		Me.Text24.Maxlength = 0
		Me.Text24.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text24.MultiLine = False
		Me.Text24.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text24.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text24.TabStop = True
		Me.Text24.Visible = True
		Me.Text24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text24.Name = "Text24"
		frOrigen.OcxState = CType(resources.GetObject("frOrigen.OcxState"), System.Windows.Forms.AxHost.State)
		Me.frOrigen.Size = New System.Drawing.Size(85, 65)
		Me.frOrigen.Location = New System.Drawing.Point(642, 8)
		Me.frOrigen.TabIndex = 125
		Me.frOrigen.Name = "frOrigen"
		Me.txtOrigen.AutoSize = False
		Me.txtOrigen.BackColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.txtOrigen.Size = New System.Drawing.Size(11, 19)
		Me.txtOrigen.Location = New System.Drawing.Point(4, 4)
		Me.txtOrigen.Maxlength = 1
		Me.txtOrigen.TabIndex = 126
		Me.txtOrigen.Tag = "45"
		Me.txtOrigen.AcceptsReturn = True
		Me.txtOrigen.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtOrigen.CausesValidation = True
		Me.txtOrigen.Enabled = True
		Me.txtOrigen.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtOrigen.HideSelection = True
		Me.txtOrigen.ReadOnly = False
		Me.txtOrigen.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtOrigen.MultiLine = False
		Me.txtOrigen.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtOrigen.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtOrigen.TabStop = True
		Me.txtOrigen.Visible = True
		Me.txtOrigen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtOrigen.Name = "txtOrigen"
		_optOrigen_1.OcxState = CType(resources.GetObject("_optOrigen_1.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optOrigen_1.Size = New System.Drawing.Size(67, 21)
		Me._optOrigen_1.Location = New System.Drawing.Point(16, 10)
		Me._optOrigen_1.TabIndex = 15
		Me._optOrigen_1.Name = "_optOrigen_1"
		_optOrigen_2.OcxState = CType(resources.GetObject("_optOrigen_2.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optOrigen_2.Size = New System.Drawing.Size(63, 21)
		Me._optOrigen_2.Location = New System.Drawing.Point(16, 32)
		Me._optOrigen_2.TabIndex = 127
		Me._optOrigen_2.Name = "_optOrigen_2"
		Me.Label8.Text = "Subdirecci�n recepci�n"
		Me.Label8.Size = New System.Drawing.Size(111, 15)
		Me.Label8.Location = New System.Drawing.Point(8, 68)
		Me.Label8.TabIndex = 142
		Me.Label8.Tag = "56"
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label8.BackColor = System.Drawing.SystemColors.Control
		Me.Label8.Enabled = True
		Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.UseMnemonic = True
		Me.Label8.Visible = True
		Me.Label8.AutoSize = False
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.Name = "Label8"
		Me.Label1.Text = "Observaciones"
		Me.Label1.Size = New System.Drawing.Size(105, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 140)
		Me.Label1.TabIndex = 131
		Me.Label1.Tag = "49"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl16.Text = "Punto recepci�n"
		Me.lbl16.Size = New System.Drawing.Size(93, 15)
		Me.lbl16.Location = New System.Drawing.Point(10, 20)
		Me.lbl16.TabIndex = 121
		Me.lbl16.Tag = "16"
		Me.lbl16.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl16.BackColor = System.Drawing.SystemColors.Control
		Me.lbl16.Enabled = True
		Me.lbl16.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl16.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl16.UseMnemonic = True
		Me.lbl16.Visible = True
		Me.lbl16.AutoSize = False
		Me.lbl16.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl16.Name = "lbl16"
		Me.lbl17.Text = "Direcci�n recepci�n"
		Me.lbl17.Size = New System.Drawing.Size(111, 15)
		Me.lbl17.Location = New System.Drawing.Point(8, 44)
		Me.lbl17.TabIndex = 120
		Me.lbl17.Tag = "17"
		Me.lbl17.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl17.BackColor = System.Drawing.SystemColors.Control
		Me.lbl17.Enabled = True
		Me.lbl17.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl17.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl17.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl17.UseMnemonic = True
		Me.lbl17.Visible = True
		Me.lbl17.AutoSize = False
		Me.lbl17.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl17.Name = "lbl17"
		Me.lbl18.Text = "Poblaci�n recepci�n"
		Me.lbl18.Size = New System.Drawing.Size(111, 15)
		Me.lbl18.Location = New System.Drawing.Point(8, 92)
		Me.lbl18.TabIndex = 119
		Me.lbl18.Tag = "18"
		Me.lbl18.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl18.BackColor = System.Drawing.SystemColors.Control
		Me.lbl18.Enabled = True
		Me.lbl18.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl18.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl18.UseMnemonic = True
		Me.lbl18.Visible = True
		Me.lbl18.AutoSize = False
		Me.lbl18.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl18.Name = "lbl18"
		Me.lbl19.Text = "CP recepci�n"
		Me.lbl19.Size = New System.Drawing.Size(89, 15)
		Me.lbl19.Location = New System.Drawing.Point(458, 92)
		Me.lbl19.TabIndex = 118
		Me.lbl19.Tag = "19"
		Me.lbl19.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl19.BackColor = System.Drawing.SystemColors.Control
		Me.lbl19.Enabled = True
		Me.lbl19.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl19.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl19.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl19.UseMnemonic = True
		Me.lbl19.Visible = True
		Me.lbl19.AutoSize = False
		Me.lbl19.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl19.Name = "lbl19"
		Me.lbl20.Text = "Tel�fono recepci�n"
		Me.lbl20.Size = New System.Drawing.Size(93, 15)
		Me.lbl20.Location = New System.Drawing.Point(554, 92)
		Me.lbl20.TabIndex = 117
		Me.lbl20.Tag = "20"
		Me.lbl20.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl20.BackColor = System.Drawing.SystemColors.Control
		Me.lbl20.Enabled = True
		Me.lbl20.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl20.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl20.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl20.UseMnemonic = True
		Me.lbl20.Visible = True
		Me.lbl20.AutoSize = False
		Me.lbl20.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl20.Name = "lbl20"
		Me.lbl21.Text = "Contacto recepci�n"
		Me.lbl21.Size = New System.Drawing.Size(111, 15)
		Me.lbl21.Location = New System.Drawing.Point(8, 116)
		Me.lbl21.TabIndex = 116
		Me.lbl21.Tag = "21"
		Me.lbl21.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl21.BackColor = System.Drawing.SystemColors.Control
		Me.lbl21.Enabled = True
		Me.lbl21.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl21.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl21.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl21.UseMnemonic = True
		Me.lbl21.Visible = True
		Me.lbl21.AutoSize = False
		Me.lbl21.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl21.Name = "lbl21"
		Me.lbl22.Text = "Horario recepci�n"
		Me.lbl22.Size = New System.Drawing.Size(105, 15)
		Me.lbl22.Location = New System.Drawing.Point(438, 116)
		Me.lbl22.TabIndex = 115
		Me.lbl22.Tag = "22"
		Me.lbl22.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl22.BackColor = System.Drawing.SystemColors.Control
		Me.lbl22.Enabled = True
		Me.lbl22.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl22.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl22.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl22.UseMnemonic = True
		Me.lbl22.Visible = True
		Me.lbl22.AutoSize = False
		Me.lbl22.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl22.Name = "lbl22"
		Me.lbl23.Text = "Entidad recepci�n"
		Me.lbl23.Size = New System.Drawing.Size(111, 15)
		Me.lbl23.Location = New System.Drawing.Point(10, 20)
		Me.lbl23.TabIndex = 114
		Me.lbl23.Tag = "23"
		Me.lbl23.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl23.BackColor = System.Drawing.SystemColors.Control
		Me.lbl23.Enabled = True
		Me.lbl23.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl23.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl23.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl23.UseMnemonic = True
		Me.lbl23.Visible = True
		Me.lbl23.AutoSize = False
		Me.lbl23.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl23.Name = "lbl23"
		Me.lbl24.Text = "Sucursal recepci�n"
		Me.lbl24.Size = New System.Drawing.Size(111, 15)
		Me.lbl24.Location = New System.Drawing.Point(222, 20)
		Me.lbl24.TabIndex = 113
		Me.lbl24.Tag = "24"
		Me.lbl24.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl24.BackColor = System.Drawing.SystemColors.Control
		Me.lbl24.Enabled = True
		Me.lbl24.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl24.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl24.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl24.UseMnemonic = True
		Me.lbl24.Visible = True
		Me.lbl24.AutoSize = False
		Me.lbl24.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl24.Name = "lbl24"
		GroupT.OcxState = CType(resources.GetObject("GroupT.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GroupT.Size = New System.Drawing.Size(637, 75)
		Me.GroupT.Location = New System.Drawing.Point(260, 146)
		Me.GroupT.TabIndex = 58
		Me.GroupT.Name = "GroupT"
		Me.txtPes.AutoSize = False
		Me.txtPes.Size = New System.Drawing.Size(43, 19)
		Me.txtPes.Location = New System.Drawing.Point(574, 16)
		Me.txtPes.Maxlength = 3
		Me.txtPes.TabIndex = 13
		Me.txtPes.Tag = "55"
		Me.txtPes.AcceptsReturn = True
		Me.txtPes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPes.BackColor = System.Drawing.SystemColors.Window
		Me.txtPes.CausesValidation = True
		Me.txtPes.Enabled = True
		Me.txtPes.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPes.HideSelection = True
		Me.txtPes.ReadOnly = False
		Me.txtPes.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPes.MultiLine = False
		Me.txtPes.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPes.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPes.TabStop = True
		Me.txtPes.Visible = True
		Me.txtPes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPes.Name = "txtPes"
		Me.txtDescripcionTransporte.AutoSize = False
		Me.txtDescripcionTransporte.Size = New System.Drawing.Size(548, 31)
		Me.txtDescripcionTransporte.Location = New System.Drawing.Point(80, 40)
		Me.txtDescripcionTransporte.Maxlength = 150
		Me.txtDescripcionTransporte.MultiLine = True
		Me.txtDescripcionTransporte.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtDescripcionTransporte.TabIndex = 14
		Me.txtDescripcionTransporte.Tag = "15"
		Me.txtDescripcionTransporte.AcceptsReturn = True
		Me.txtDescripcionTransporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcionTransporte.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcionTransporte.CausesValidation = True
		Me.txtDescripcionTransporte.Enabled = True
		Me.txtDescripcionTransporte.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcionTransporte.HideSelection = True
		Me.txtDescripcionTransporte.ReadOnly = False
		Me.txtDescripcionTransporte.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcionTransporte.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcionTransporte.TabStop = True
		Me.txtDescripcionTransporte.Visible = True
		Me.txtDescripcionTransporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcionTransporte.Name = "txtDescripcionTransporte"
		Me.Text14.AutoSize = False
		Me.Text14.BackColor = System.Drawing.Color.White
		Me.Text14.Enabled = False
		Me.Text14.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text14.Size = New System.Drawing.Size(310, 19)
		Me.Text14.Location = New System.Drawing.Point(232, 16)
		Me.Text14.TabIndex = 60
		Me.Text14.Tag = "^14"
		Me.Text14.AcceptsReturn = True
		Me.Text14.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text14.CausesValidation = True
		Me.Text14.HideSelection = True
		Me.Text14.ReadOnly = False
		Me.Text14.Maxlength = 0
		Me.Text14.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text14.MultiLine = False
		Me.Text14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text14.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text14.TabStop = True
		Me.Text14.Visible = True
		Me.Text14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text14.Name = "Text14"
		Me.txtTipoElemento.AutoSize = False
		Me.txtTipoElemento.Size = New System.Drawing.Size(33, 19)
		Me.txtTipoElemento.Location = New System.Drawing.Point(196, 16)
		Me.txtTipoElemento.Maxlength = 2
		Me.txtTipoElemento.TabIndex = 12
		Me.txtTipoElemento.Tag = "14"
		Me.txtTipoElemento.AcceptsReturn = True
		Me.txtTipoElemento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoElemento.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoElemento.CausesValidation = True
		Me.txtTipoElemento.Enabled = True
		Me.txtTipoElemento.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoElemento.HideSelection = True
		Me.txtTipoElemento.ReadOnly = False
		Me.txtTipoElemento.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoElemento.MultiLine = False
		Me.txtTipoElemento.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoElemento.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoElemento.TabStop = True
		Me.txtTipoElemento.Visible = True
		Me.txtTipoElemento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoElemento.Name = "txtTipoElemento"
		Me.txtCantidad.AutoSize = False
		Me.txtCantidad.Size = New System.Drawing.Size(31, 19)
		Me.txtCantidad.Location = New System.Drawing.Point(80, 16)
		Me.txtCantidad.Maxlength = 3
		Me.txtCantidad.TabIndex = 11
		Me.txtCantidad.Tag = "13"
		Me.txtCantidad.AcceptsReturn = True
		Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCantidad.BackColor = System.Drawing.SystemColors.Window
		Me.txtCantidad.CausesValidation = True
		Me.txtCantidad.Enabled = True
		Me.txtCantidad.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCantidad.HideSelection = True
		Me.txtCantidad.ReadOnly = False
		Me.txtCantidad.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCantidad.MultiLine = False
		Me.txtCantidad.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCantidad.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCantidad.TabStop = True
		Me.txtCantidad.Visible = True
		Me.txtCantidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCantidad.Name = "txtCantidad"
		Me.Label7.Text = "Kg"
		Me.Label7.Size = New System.Drawing.Size(13, 19)
		Me.Label7.Location = New System.Drawing.Point(620, 20)
		Me.Label7.TabIndex = 140
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label7.BackColor = System.Drawing.SystemColors.Control
		Me.Label7.Enabled = True
		Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.UseMnemonic = True
		Me.Label7.Visible = True
		Me.Label7.AutoSize = False
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.Name = "Label7"
		Me.Label6.Text = "Pes"
		Me.Label6.Size = New System.Drawing.Size(43, 15)
		Me.Label6.Location = New System.Drawing.Point(552, 20)
		Me.Label6.TabIndex = 138
		Me.Label6.Tag = "55"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.lbl15.Text = "Descripci�n"
		Me.lbl15.Size = New System.Drawing.Size(111, 15)
		Me.lbl15.Location = New System.Drawing.Point(8, 44)
		Me.lbl15.TabIndex = 62
		Me.lbl15.Tag = "15"
		Me.lbl15.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl15.BackColor = System.Drawing.SystemColors.Control
		Me.lbl15.Enabled = True
		Me.lbl15.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl15.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl15.UseMnemonic = True
		Me.lbl15.Visible = True
		Me.lbl15.AutoSize = False
		Me.lbl15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl15.Name = "lbl15"
		Me.lbl14.Text = "Tipo elemento"
		Me.lbl14.Size = New System.Drawing.Size(111, 15)
		Me.lbl14.Location = New System.Drawing.Point(120, 20)
		Me.lbl14.TabIndex = 61
		Me.lbl14.Tag = "14"
		Me.lbl14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl14.BackColor = System.Drawing.SystemColors.Control
		Me.lbl14.Enabled = True
		Me.lbl14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl14.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl14.UseMnemonic = True
		Me.lbl14.Visible = True
		Me.lbl14.AutoSize = False
		Me.lbl14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl14.Name = "lbl14"
		Me.lbl13.Text = "Cantidad"
		Me.lbl13.Size = New System.Drawing.Size(111, 15)
		Me.lbl13.Location = New System.Drawing.Point(8, 20)
		Me.lbl13.TabIndex = 59
		Me.lbl13.Tag = "13"
		Me.lbl13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl13.BackColor = System.Drawing.SystemColors.Control
		Me.lbl13.Enabled = True
		Me.lbl13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl13.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl13.UseMnemonic = True
		Me.lbl13.Visible = True
		Me.lbl13.AutoSize = False
		Me.lbl13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl13.Name = "lbl13"
		Me.txtSolicitud.AutoSize = False
		Me.txtSolicitud.Size = New System.Drawing.Size(83, 19)
		Me.txtSolicitud.Location = New System.Drawing.Point(104, 6)
		Me.txtSolicitud.Maxlength = 8
		Me.txtSolicitud.TabIndex = 0
		Me.txtSolicitud.Tag = "*1"
		Me.txtSolicitud.AcceptsReturn = True
		Me.txtSolicitud.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSolicitud.BackColor = System.Drawing.SystemColors.Window
		Me.txtSolicitud.CausesValidation = True
		Me.txtSolicitud.Enabled = True
		Me.txtSolicitud.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSolicitud.HideSelection = True
		Me.txtSolicitud.ReadOnly = False
		Me.txtSolicitud.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSolicitud.MultiLine = False
		Me.txtSolicitud.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSolicitud.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSolicitud.TabStop = True
		Me.txtSolicitud.Visible = True
		Me.txtSolicitud.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSolicitud.Name = "txtSolicitud"
		cmbEstat.OcxState = CType(resources.GetObject("cmbEstat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbEstat.Size = New System.Drawing.Size(145, 19)
		Me.cmbEstat.Location = New System.Drawing.Point(502, 6)
		Me.cmbEstat.TabIndex = 1
		Me.cmbEstat.Name = "cmbEstat"
		txtDataSolicitud.OcxState = CType(resources.GetObject("txtDataSolicitud.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataSolicitud.Size = New System.Drawing.Size(87, 19)
		Me.txtDataSolicitud.Location = New System.Drawing.Point(104, 30)
		Me.txtDataSolicitud.TabIndex = 2
		Me.txtDataSolicitud.Name = "txtDataSolicitud"
		txtDataTransport.OcxState = CType(resources.GetObject("txtDataTransport.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataTransport.Size = New System.Drawing.Size(87, 19)
		Me.txtDataTransport.Location = New System.Drawing.Point(310, 30)
		Me.txtDataTransport.TabIndex = 3
		Me.txtDataTransport.Name = "txtDataTransport"
		cmbPrioritat.OcxState = CType(resources.GetObject("cmbPrioritat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbPrioritat.Size = New System.Drawing.Size(145, 19)
		Me.cmbPrioritat.Location = New System.Drawing.Point(502, 30)
		Me.cmbPrioritat.TabIndex = 4
		Me.cmbPrioritat.Name = "cmbPrioritat"
		Me.txtSolicitant.AutoSize = False
		Me.txtSolicitant.Size = New System.Drawing.Size(104, 19)
		Me.txtSolicitant.Location = New System.Drawing.Point(104, 54)
		Me.txtSolicitant.Maxlength = 10
		Me.txtSolicitant.TabIndex = 5
		Me.txtSolicitant.Tag = "6"
		Me.txtSolicitant.AcceptsReturn = True
		Me.txtSolicitant.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSolicitant.BackColor = System.Drawing.SystemColors.Window
		Me.txtSolicitant.CausesValidation = True
		Me.txtSolicitant.Enabled = True
		Me.txtSolicitant.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSolicitant.HideSelection = True
		Me.txtSolicitant.ReadOnly = False
		Me.txtSolicitant.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSolicitant.MultiLine = False
		Me.txtSolicitant.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSolicitant.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSolicitant.TabStop = True
		Me.txtSolicitant.Visible = True
		Me.txtSolicitant.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSolicitant.Name = "txtSolicitant"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(310, 19)
		Me.Text6.Location = New System.Drawing.Point(211, 54)
		Me.Text6.TabIndex = 47
		Me.Text6.Tag = "^6"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(42, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(104, 78)
		Me.txtEmpresa.Maxlength = 4
		Me.txtEmpresa.TabIndex = 6
		Me.txtEmpresa.Tag = "7"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text7.AutoSize = False
		Me.Text7.BackColor = System.Drawing.Color.White
		Me.Text7.Enabled = False
		Me.Text7.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text7.Size = New System.Drawing.Size(372, 19)
		Me.Text7.Location = New System.Drawing.Point(149, 78)
		Me.Text7.TabIndex = 49
		Me.Text7.Tag = "^7"
		Me.Text7.AcceptsReturn = True
		Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text7.CausesValidation = True
		Me.Text7.HideSelection = True
		Me.Text7.ReadOnly = False
		Me.Text7.Maxlength = 0
		Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text7.MultiLine = False
		Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text7.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text7.TabStop = True
		Me.Text7.Visible = True
		Me.Text7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text7.Name = "Text7"
		Me.txtCentreCost.AutoSize = False
		Me.txtCentreCost.Size = New System.Drawing.Size(42, 19)
		Me.txtCentreCost.Location = New System.Drawing.Point(104, 102)
		Me.txtCentreCost.Maxlength = 4
		Me.txtCentreCost.TabIndex = 7
		Me.txtCentreCost.Tag = "8"
		Me.txtCentreCost.AcceptsReturn = True
		Me.txtCentreCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCentreCost.BackColor = System.Drawing.SystemColors.Window
		Me.txtCentreCost.CausesValidation = True
		Me.txtCentreCost.Enabled = True
		Me.txtCentreCost.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCentreCost.HideSelection = True
		Me.txtCentreCost.ReadOnly = False
		Me.txtCentreCost.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCentreCost.MultiLine = False
		Me.txtCentreCost.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCentreCost.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCentreCost.TabStop = True
		Me.txtCentreCost.Visible = True
		Me.txtCentreCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCentreCost.Name = "txtCentreCost"
		Me.Text8.AutoSize = False
		Me.Text8.BackColor = System.Drawing.Color.White
		Me.Text8.Enabled = False
		Me.Text8.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text8.Size = New System.Drawing.Size(372, 19)
		Me.Text8.Location = New System.Drawing.Point(149, 102)
		Me.Text8.TabIndex = 51
		Me.Text8.Tag = "^8"
		Me.Text8.AcceptsReturn = True
		Me.Text8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text8.CausesValidation = True
		Me.Text8.HideSelection = True
		Me.Text8.ReadOnly = False
		Me.Text8.Maxlength = 0
		Me.Text8.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text8.MultiLine = False
		Me.Text8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text8.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text8.TabStop = True
		Me.Text8.Visible = True
		Me.Text8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text8.Name = "Text8"
		Me.txtFeina.AutoSize = False
		Me.txtFeina.Size = New System.Drawing.Size(40, 19)
		Me.txtFeina.Location = New System.Drawing.Point(104, 126)
		Me.txtFeina.Maxlength = 6
		Me.txtFeina.TabIndex = 8
		Me.txtFeina.Tag = "9"
		Me.txtFeina.AcceptsReturn = True
		Me.txtFeina.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFeina.BackColor = System.Drawing.SystemColors.Window
		Me.txtFeina.CausesValidation = True
		Me.txtFeina.Enabled = True
		Me.txtFeina.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFeina.HideSelection = True
		Me.txtFeina.ReadOnly = False
		Me.txtFeina.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFeina.MultiLine = False
		Me.txtFeina.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFeina.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFeina.TabStop = True
		Me.txtFeina.Visible = True
		Me.txtFeina.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFeina.Name = "txtFeina"
		Me.Text9.AutoSize = False
		Me.Text9.BackColor = System.Drawing.Color.White
		Me.Text9.Enabled = False
		Me.Text9.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text9.Size = New System.Drawing.Size(244, 19)
		Me.Text9.Location = New System.Drawing.Point(149, 126)
		Me.Text9.TabIndex = 53
		Me.Text9.Tag = "^9"
		Me.Text9.AcceptsReturn = True
		Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text9.CausesValidation = True
		Me.Text9.HideSelection = True
		Me.Text9.ReadOnly = False
		Me.Text9.Maxlength = 0
		Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text9.MultiLine = False
		Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text9.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text9.TabStop = True
		Me.Text9.Visible = True
		Me.Text9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text9.Name = "Text9"
		Me.txtTarea.AutoSize = False
		Me.txtTarea.Size = New System.Drawing.Size(40, 19)
		Me.txtTarea.Location = New System.Drawing.Point(434, 126)
		Me.txtTarea.Maxlength = 6
		Me.txtTarea.TabIndex = 9
		Me.txtTarea.Tag = "10"
		Me.txtTarea.AcceptsReturn = True
		Me.txtTarea.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTarea.BackColor = System.Drawing.SystemColors.Window
		Me.txtTarea.CausesValidation = True
		Me.txtTarea.Enabled = True
		Me.txtTarea.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTarea.HideSelection = True
		Me.txtTarea.ReadOnly = False
		Me.txtTarea.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTarea.MultiLine = False
		Me.txtTarea.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTarea.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTarea.TabStop = True
		Me.txtTarea.Visible = True
		Me.txtTarea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTarea.Name = "txtTarea"
		Me.Text10.AutoSize = False
		Me.Text10.BackColor = System.Drawing.Color.White
		Me.Text10.Enabled = False
		Me.Text10.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text10.Size = New System.Drawing.Size(168, 19)
		Me.Text10.Location = New System.Drawing.Point(479, 126)
		Me.Text10.TabIndex = 55
		Me.Text10.Tag = "^10"
		Me.Text10.AcceptsReturn = True
		Me.Text10.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text10.CausesValidation = True
		Me.Text10.HideSelection = True
		Me.Text10.ReadOnly = False
		Me.Text10.Maxlength = 0
		Me.Text10.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text10.MultiLine = False
		Me.Text10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text10.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text10.TabStop = True
		Me.Text10.Visible = True
		Me.Text10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text10.Name = "Text10"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(813, 589)
		Me.cmdAceptar.TabIndex = 40
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(379, 589)
		Me.cmdGuardar.TabIndex = 56
		Me.cmdGuardar.Visible = False
		Me.cmdGuardar.Name = "cmdGuardar"
		frTipo.OcxState = CType(resources.GetObject("frTipo.OcxState"), System.Windows.Forms.AxHost.State)
		Me.frTipo.Size = New System.Drawing.Size(247, 39)
		Me.frTipo.Location = New System.Drawing.Point(10, 142)
		Me.frTipo.TabIndex = 122
		Me.frTipo.Name = "frTipo"
		Me.txtTipo.AutoSize = False
		Me.txtTipo.BackColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.txtTipo.Size = New System.Drawing.Size(11, 19)
		Me.txtTipo.Location = New System.Drawing.Point(4, 4)
		Me.txtTipo.Maxlength = 1
		Me.txtTipo.TabIndex = 123
		Me.txtTipo.Tag = "11"
		Me.txtTipo.AcceptsReturn = True
		Me.txtTipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipo.CausesValidation = True
		Me.txtTipo.Enabled = True
		Me.txtTipo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipo.HideSelection = True
		Me.txtTipo.ReadOnly = False
		Me.txtTipo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipo.MultiLine = False
		Me.txtTipo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipo.TabStop = True
		Me.txtTipo.Visible = True
		Me.txtTipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipo.Name = "txtTipo"
		_optTipo_1.OcxState = CType(resources.GetObject("_optTipo_1.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optTipo_1.Size = New System.Drawing.Size(81, 21)
		Me._optTipo_1.Location = New System.Drawing.Point(40, 12)
		Me._optTipo_1.TabIndex = 10
		Me._optTipo_1.Name = "_optTipo_1"
		_optTipo_2.OcxState = CType(resources.GetObject("_optTipo_2.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optTipo_2.Size = New System.Drawing.Size(97, 21)
		Me._optTipo_2.Location = New System.Drawing.Point(154, 12)
		Me._optTipo_2.TabIndex = 124
		Me._optTipo_2.Name = "_optTipo_2"
		Me.Image2.Size = New System.Drawing.Size(16, 16)
		Me.Image2.Location = New System.Drawing.Point(130, 14)
		Me.Image2.Image = CType(resources.GetObject("Image2.Image"), System.Drawing.Image)
		Me.Image2.Enabled = True
		Me.Image2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Image2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.Image2.Visible = True
		Me.Image2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Image2.Name = "Image2"
		Me.Image1.Size = New System.Drawing.Size(16, 16)
		Me.Image1.Location = New System.Drawing.Point(18, 14)
		Me.Image1.Image = CType(resources.GetObject("Image1.Image"), System.Drawing.Image)
		Me.Image1.Enabled = True
		Me.Image1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.Image1.Visible = True
		Me.Image1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Image1.Name = "Image1"
		cmdReobrir.OcxState = CType(resources.GetObject("cmdReobrir.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdReobrir.Size = New System.Drawing.Size(79, 36)
		Me.cmdReobrir.Location = New System.Drawing.Point(568, 88)
		Me.cmdReobrir.TabIndex = 134
		Me.cmdReobrir.Visible = False
		Me.cmdReobrir.Name = "cmdReobrir"
		cmdImprimir.OcxState = CType(resources.GetObject("cmdImprimir.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdImprimir.Size = New System.Drawing.Size(79, 35)
		Me.cmdImprimir.Location = New System.Drawing.Point(568, 50)
		Me.cmdImprimir.TabIndex = 141
		Me.cmdImprimir.Name = "cmdImprimir"
		Me.lbl1.Text = "Solicitud"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 10)
		Me.lbl1.TabIndex = 41
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Estado"
		Me.lbl2.Size = New System.Drawing.Size(51, 15)
		Me.lbl2.Location = New System.Drawing.Point(422, 10)
		Me.lbl2.TabIndex = 42
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Fecha solicitud"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 34)
		Me.lbl3.TabIndex = 43
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Fecha transporte"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(212, 34)
		Me.lbl4.TabIndex = 44
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Prioridad"
		Me.lbl5.Size = New System.Drawing.Size(69, 15)
		Me.lbl5.Location = New System.Drawing.Point(422, 34)
		Me.lbl5.TabIndex = 45
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Solicitante"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 58)
		Me.lbl6.TabIndex = 46
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "Empresa"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 82)
		Me.lbl7.TabIndex = 48
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl8.Text = "Centro coste"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(10, 106)
		Me.lbl8.TabIndex = 50
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl9.Text = "Orden de trabajo"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(10, 130)
		Me.lbl9.TabIndex = 52
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl10.Text = "Tarea"
		Me.lbl10.Size = New System.Drawing.Size(111, 15)
		Me.lbl10.Location = New System.Drawing.Point(396, 130)
		Me.lbl10.TabIndex = 54
		Me.lbl10.Tag = "10"
		Me.lbl10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl10.BackColor = System.Drawing.SystemColors.Control
		Me.lbl10.Enabled = True
		Me.lbl10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl10.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl10.UseMnemonic = True
		Me.lbl10.Visible = True
		Me.lbl10.AutoSize = False
		Me.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl10.Name = "lbl10"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(12, 591)
		Me.lblLock.TabIndex = 57
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.optDesti.SetIndex(_optDesti_1, CType(1, Short))
		Me.optDesti.SetIndex(_optDesti_2, CType(2, Short))
		Me.optOrigen.SetIndex(_optOrigen_1, CType(1, Short))
		Me.optOrigen.SetIndex(_optOrigen_2, CType(2, Short))
		Me.optTipo.SetIndex(_optTipo_1, CType(1, Short))
		Me.optTipo.SetIndex(_optTipo_2, CType(2, Short))
		CType(Me.optTipo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.optOrigen, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.optDesti, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdImprimir, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdReobrir, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.frTipo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optTipo_2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optTipo_1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbPrioritat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataTransport, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataSolicitud, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbEstat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GroupT, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.FrameOrigen, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.frOrigen, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optOrigen_2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optOrigen_1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.FrameDesti, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkCopiaRegistrada, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.frDesti, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optDesti_2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optDesti_1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.FrameTransp, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdModifPreu, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtImporteVtaInterna, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtHorasTransporte, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtPrecio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtHoraEntrega, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaEntrega, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtHoraRecogida, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaRecogida, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaRealizacion, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaPrevista, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.grdHSol, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(TabControl1)
		Me.Controls.Add(GroupT)
		Me.Controls.Add(txtSolicitud)
		Me.Controls.Add(cmbEstat)
		Me.Controls.Add(txtDataSolicitud)
		Me.Controls.Add(txtDataTransport)
		Me.Controls.Add(cmbPrioritat)
		Me.Controls.Add(txtSolicitant)
		Me.Controls.Add(Text6)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text7)
		Me.Controls.Add(txtCentreCost)
		Me.Controls.Add(Text8)
		Me.Controls.Add(txtFeina)
		Me.Controls.Add(Text9)
		Me.Controls.Add(txtTarea)
		Me.Controls.Add(Text10)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(frTipo)
		Me.Controls.Add(cmdReobrir)
		Me.Controls.Add(cmdImprimir)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(lbl10)
		Me.Controls.Add(lblLock)
		Me.TabControl1.Controls.Add(TabControlPage3)
		Me.TabControl1.Controls.Add(TabControlPage2)
		Me.TabControl1.Controls.Add(TabControlPage1)
		Me.TabControlPage3.Controls.Add(grdHSol)
		Me.TabControlPage2.Controls.Add(FrameTransp)
		Me.FrameTransp.Controls.Add(txtKilometros)
		Me.FrameTransp.Controls.Add(Text39)
		Me.FrameTransp.Controls.Add(txtMotivoNoRealizado)
		Me.FrameTransp.Controls.Add(txtComentario)
		Me.FrameTransp.Controls.Add(Text34)
		Me.FrameTransp.Controls.Add(txtTipoTarifa)
		Me.FrameTransp.Controls.Add(Text38)
		Me.FrameTransp.Controls.Add(txtTransportista)
		Me.FrameTransp.Controls.Add(txtFechaPrevista)
		Me.FrameTransp.Controls.Add(txtFechaRealizacion)
		Me.FrameTransp.Controls.Add(txtFechaRecogida)
		Me.FrameTransp.Controls.Add(txtHoraRecogida)
		Me.FrameTransp.Controls.Add(txtFechaEntrega)
		Me.FrameTransp.Controls.Add(txtHoraEntrega)
		Me.FrameTransp.Controls.Add(txtPrecio)
		Me.FrameTransp.Controls.Add(txtHorasTransporte)
		Me.FrameTransp.Controls.Add(txtImporteVtaInterna)
		Me.FrameTransp.Controls.Add(cmdModifPreu)
		Me.FrameTransp.Controls.Add(Label5)
		Me.FrameTransp.Controls.Add(Label4)
		Me.FrameTransp.Controls.Add(Label3)
		Me.FrameTransp.Controls.Add(lbl39)
		Me.FrameTransp.Controls.Add(lbl44)
		Me.FrameTransp.Controls.Add(lbl35)
		Me.FrameTransp.Controls.Add(lbl34)
		Me.FrameTransp.Controls.Add(lbl41)
		Me.FrameTransp.Controls.Add(lbl40)
		Me.FrameTransp.Controls.Add(lbl43)
		Me.FrameTransp.Controls.Add(lbl42)
		Me.FrameTransp.Controls.Add(lbl37)
		Me.FrameTransp.Controls.Add(lbl36)
		Me.FrameTransp.Controls.Add(lbl38)
		Me.TabControlPage1.Controls.Add(FrameDesti)
		Me.TabControlPage1.Controls.Add(FrameOrigen)
		Me.FrameDesti.Controls.Add(txtSubdireccionDestino)
		Me.FrameDesti.Controls.Add(txtObservacionesDestino)
		Me.FrameDesti.Controls.Add(txtEntidadEntrega)
		Me.FrameDesti.Controls.Add(txtSucursalEntrega)
		Me.FrameDesti.Controls.Add(Text33)
		Me.FrameDesti.Controls.Add(txtPuntoEntrega)
		Me.FrameDesti.Controls.Add(Text25)
		Me.FrameDesti.Controls.Add(txtDireccionEntrega)
		Me.FrameDesti.Controls.Add(txtPoblacionEntrega)
		Me.FrameDesti.Controls.Add(Text27)
		Me.FrameDesti.Controls.Add(txtCpEntrega)
		Me.FrameDesti.Controls.Add(txtTelefonoEntrega)
		Me.FrameDesti.Controls.Add(txtContactoEntrega)
		Me.FrameDesti.Controls.Add(txtHorarioEntrega)
		Me.FrameDesti.Controls.Add(frDesti)
		Me.FrameDesti.Controls.Add(chkCopiaRegistrada)
		Me.FrameDesti.Controls.Add(Label9)
		Me.FrameDesti.Controls.Add(Label2)
		Me.FrameDesti.Controls.Add(lbl32)
		Me.FrameDesti.Controls.Add(lbl33)
		Me.FrameDesti.Controls.Add(lbl25)
		Me.FrameDesti.Controls.Add(lbl26)
		Me.FrameDesti.Controls.Add(lbl27)
		Me.FrameDesti.Controls.Add(lbl28)
		Me.FrameDesti.Controls.Add(lbl29)
		Me.FrameDesti.Controls.Add(lbl30)
		Me.FrameDesti.Controls.Add(lbl31)
		Me.frDesti.Controls.Add(txtDesti)
		Me.frDesti.Controls.Add(_optDesti_1)
		Me.frDesti.Controls.Add(_optDesti_2)
		Me.FrameOrigen.Controls.Add(txtSubdireccionOrigen)
		Me.FrameOrigen.Controls.Add(txtObservacionesorigen)
		Me.FrameOrigen.Controls.Add(txtPuntoRecepcion)
		Me.FrameOrigen.Controls.Add(Text16)
		Me.FrameOrigen.Controls.Add(txtDireccionRecepcion)
		Me.FrameOrigen.Controls.Add(txtPoblacionRecepcion)
		Me.FrameOrigen.Controls.Add(Text18)
		Me.FrameOrigen.Controls.Add(txtCpRecepcion)
		Me.FrameOrigen.Controls.Add(txtTelefonoRecepcion)
		Me.FrameOrigen.Controls.Add(txtContactoRecepcion)
		Me.FrameOrigen.Controls.Add(txtHorarioRecepcion)
		Me.FrameOrigen.Controls.Add(txtEntidadRecepcion)
		Me.FrameOrigen.Controls.Add(txtSucursalRecepcion)
		Me.FrameOrigen.Controls.Add(Text24)
		Me.FrameOrigen.Controls.Add(frOrigen)
		Me.FrameOrigen.Controls.Add(Label8)
		Me.FrameOrigen.Controls.Add(Label1)
		Me.FrameOrigen.Controls.Add(lbl16)
		Me.FrameOrigen.Controls.Add(lbl17)
		Me.FrameOrigen.Controls.Add(lbl18)
		Me.FrameOrigen.Controls.Add(lbl19)
		Me.FrameOrigen.Controls.Add(lbl20)
		Me.FrameOrigen.Controls.Add(lbl21)
		Me.FrameOrigen.Controls.Add(lbl22)
		Me.FrameOrigen.Controls.Add(lbl23)
		Me.FrameOrigen.Controls.Add(lbl24)
		Me.frOrigen.Controls.Add(txtOrigen)
		Me.frOrigen.Controls.Add(_optOrigen_1)
		Me.frOrigen.Controls.Add(_optOrigen_2)
		Me.GroupT.Controls.Add(txtPes)
		Me.GroupT.Controls.Add(txtDescripcionTransporte)
		Me.GroupT.Controls.Add(Text14)
		Me.GroupT.Controls.Add(txtTipoElemento)
		Me.GroupT.Controls.Add(txtCantidad)
		Me.GroupT.Controls.Add(Label7)
		Me.GroupT.Controls.Add(Label6)
		Me.GroupT.Controls.Add(lbl15)
		Me.GroupT.Controls.Add(lbl14)
		Me.GroupT.Controls.Add(lbl13)
		Me.frTipo.Controls.Add(txtTipo)
		Me.frTipo.Controls.Add(_optTipo_1)
		Me.frTipo.Controls.Add(_optTipo_2)
		Me.frTipo.Controls.Add(Image2)
		Me.frTipo.Controls.Add(Image1)
		Me.TabControl1.ResumeLayout(False)
		Me.TabControlPage3.ResumeLayout(False)
		Me.TabControlPage2.ResumeLayout(False)
		Me.FrameTransp.ResumeLayout(False)
		Me.TabControlPage1.ResumeLayout(False)
		Me.FrameDesti.ResumeLayout(False)
		Me.frDesti.ResumeLayout(False)
		Me.FrameOrigen.ResumeLayout(False)
		Me.frOrigen.ResumeLayout(False)
		Me.GroupT.ResumeLayout(False)
		Me.frTipo.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class