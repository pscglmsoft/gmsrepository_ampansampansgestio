Option Strict Off
Option Explicit On
Friend Class FrmSolPlanificades
	Inherits FormParent
	''OKPublic Appl As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	'Dim EsIntern As Boolean
	Dim TransportIntern As String
	
	Public Overrides Sub Inici()
		Dim DataDesde As String
		Dim DataFinsA As String
		Dim Transportista As String
		DataDesde = txtDataIni.Text
		DataFinsA = txtDataFi.Text
		Transportista = txtTransportistaF.Text
		ResetForm(Me)
		txtDataIni.Text = DataDesde
		txtDataFi.Text = DataFinsA
		txtTransportistaF.Text = Transportista
		DisplayDescripcio(Me, txtTransportistaF)
		
		FrameDes.Visible = False
		FrameTransp.Visible = False
		'    optTipo(1).Value = False
		'    optTipo(2).Value = False
		txtTipo.Text = ""
		CarregaSolP()
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		Dim I As Short
		Dim Nod As String
		Dim Valor As String
		Dim dataRealitzat As String
		
		Valor = ValorSeleccionat
		
		If Valor = "P" Then
			If GravaSol = False Then Exit Sub
		ElseIf Valor = "D" Then 
			If txtMotivoNoRealizado.Text = "" Then
				xMsgBox("Falta informar el motivo no realizado", MsgBoxStyle.Information, Me.Text)
				txtMotivoNoRealizado.Focus()
				Exit Sub
			End If
		Else
			Exit Sub
		End If
		CacheNetejaParametres()
		MCache.P1 = txtSolicitud.Text
		'MCache.P2 = dataRealitzat & S & txtPrecio
		MCache.P2 = txtFechaEntrega.Text & S & txtPrecio.Text
		MCache.P3 = txtMotivoNoRealizado.Text & S & txtComentarioDes.Text
		
		CacheXecute("D GRAVSOLP^TRANSPORT")
		Inici()
		
		CarregaFGrid(grdSolPlanificades, "CGSOLP^TRANSPORT")
		
	End Sub
	
	Private Sub cmdFiltraData_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdFiltraData.ClickEvent
		CarregaSolP()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaSol = False Then Exit Sub
		Inici()
		CarregaFGrid(grdSolPlanificades, "CGSOLP^TRANSPORT")
	End Sub
	
	Private Sub cmdImprimir_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdImprimir.ClickEvent
		ObreFormulari(FrmImprimirTransport, "FrmImprimirTransport",  , Me.Name)
	End Sub
	
	'UPGRADE_WARNING: Form evento FrmSolPlanificades.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub FrmSolPlanificades_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaSolP()
	End Sub
	
	Private Sub FrmSolPlanificades_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub FrmSolPlanificades_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub FrmSolPlanificades_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		CarregaFGrid(grdSolPlanificades, "CGSOL^TRANSPORT")
		FrameTransp.Visible = False
		FrameDes.Visible = False
		cmdGuardar.Picture = SetIcon("Save", 16)
		cmdFiltraData.Picture = SetIcon("Grid2", 16)
		cmdImprimir.Picture = SetIcon("Print", 16)
		CanviEstil()
	End Sub
	
	Private Sub grdSolPlanificades_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdSolPlanificades.Click
		Dim Nod As String
		If grdSolPlanificades.ActiveCell.Row = 0 Then Exit Sub
		EsborraDades()
		
		CacheNetejaParametres()
		txtSolicitud.Text = grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 1).Text
		MCache.P1 = txtSolicitud.Text
		cmdAceptar.Visible = False
		Label4.Visible = False
		txtKilometros.Visible = False
		Label5.Visible = False
		txtHorasTransporte.Visible = False
		lbl35.Visible = False
		txtPrecio.Visible = False
		Label6.Visible = False
		txtVdaInt.Visible = False
		lblVdaInt.Visible = False
		txtMotivoNoRealizadoT.Visible = False
		Text1.Visible = False
		
		TransportIntern = CacheXecute("S VALUE=$$ESINTERN1^TRANSPORT(P1)")
		
		
		If grdSolPlanificades.ActiveCell.Col = 17 Or grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 17).Text = "1" Then
			If grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 17).Text = "1" Then
				DesmarcaTot((grdSolPlanificades.ActiveCell.Row))
				grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 18).Text = ""
				FrameTransp.Visible = True
				FrameDes.Visible = False
				ActualitzaDades()
			End If
			If (grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 17).Text = "0" Or grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 17).Text = "") And (grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 18).Text = "0" Or grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 18).Text = "") Then
				DesmarcaTot((grdSolPlanificades.ActiveCell.Row))
				FrameTransp.Visible = True
				FrameDes.Visible = False
				ActualitzaDades()
			Else
				cmdAceptar.Visible = True
				'Label4.Visible = True          ARA JORDI
				'txtKilometros.Visible = True
				'Label5.Visible = True
				'txtHorasTransporte.Visible = True
				lbl35.Visible = True
				txtPrecio.Visible = True
				If TransportIntern = "SI" Then
					lblVdaInt.Visible = True
					txtVdaInt.Visible = True
				End If
			End If
		ElseIf grdSolPlanificades.ActiveCell.Col = 18 Then 
			If grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 18).Text = "1" Then
				grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 17).Text = ""
				DesmarcaTot((grdSolPlanificades.ActiveCell.Row))
				FrameTransp.Visible = False
				FrameDes.Visible = True
				FrameDes.Top = 386      ' MigratPixels
				FrameDes.Left = 6      ' MigratPixels
				txtMotivoNoRealizado.Focus()
				cmdAceptar.Visible = True
				Label4.Visible = True
				txtKilometros.Visible = True
				Label5.Visible = True
				txtHorasTransporte.Visible = True
				lbl35.Visible = True
				txtPrecio.Visible = True
				txtVdaInt.Visible = True
				lblVdaInt.Visible = True
			Else
				FrameDes.Visible = False
				FrameTransp.Visible = False
			End If
		Else
			ActualitzaDades()
		End If
		If grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 17).Text = "1" Or grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 14).Text = "1" Then
			txtSolicitud.Text = grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 1).Text
		Else
			'txtSolicitud = ""
		End If
		If grdSolPlanificades.ActiveCell.Col <> 17 Or grdSolPlanificades.ActiveCell.Col <> 18 Then MarcaRowFG(grdSolPlanificades) : Exit Sub
	End Sub
	
	Private Sub grdSolPlanificades_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdSolPlanificades.DoubleClick
		If grdSolPlanificades.MouseRow < 1 Then Exit Sub
		ObreSolicitud()
	End Sub
	
	Private Sub ObreSolicitud()
		Dim sol As String
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		sol = grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 1).Text
		ObreFormulari(frmSolicitudTransport, "frmSolicitudTransport", sol)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	Private Sub grdSolPlanificades_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdSolPlanificades.MouseUp
		Dim strLin As String
		Dim strTipPer As String
		
		'If grdSolPendents.Cell(grdSolPendents.ActiveCell.Row, 13).Text = "" Then Exit Sub
		If grdSolPlanificades.MouseRow < 1 Then Exit Sub
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		If grdSolPlanificades.Rows < 2 Then Exit Sub
		strLin = FGGetRegRow(grdSolPlanificades)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		strTipPer = Piece(Piece(strLin, S, 1), "|", 10)
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			
			'UPGRADE_ISSUE: Control Name no se pudo resolver porque est� dentro del espacio de nombres gen�rico ActiveControl. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="084D22AD-ECB1-400F-B4C7-418ECEC5E36E"'
			With .Menus.Add(ActiveControl.Name, PopMenuStyle.tsSecondaryMenu, True)
				If grdSolPlanificades.Rows > 1 And grdSolPlanificades.Selection.FirstRow <> 0 Then
					'UPGRADE_ISSUE: Control Name no se pudo resolver porque est� dentro del espacio de nombres gen�rico ActiveControl. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="084D22AD-ECB1-400F-B4C7-418ECEC5E36E"'
					.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Ir a la solicitud",  , True, XPIcon("WindowsSendTo"),  ,  ,  ,  , "ASOL")
					'.MenuItems.Add tsMenuCaption, "Planificar", , True, XPIcon("Verify"), , , , , "PLAN"
					'.MenuItems.Add tsMenuCaption, "Desestimar", , True, XPIcon("Delete"), , , , , "DES"
				End If
			End With
		End With
		XpExecutaMenu(Me, "grdSolPlanificades", eventArgs.X, eventArgs.Y)
		
	End Sub
	
	Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim I As Short
		Dim FRM As FormParent
		
		Select Case KeyMenu
			Case "ASOL" 'anar a la sol�licitud
				For	Each FRM In forms
					If FRM.Name = "frmSolicitudTransport" Then
						'UPGRADE_ISSUE: Control txtCodi no se pudo resolver porque est� dentro del espacio de nombres gen�rico Form. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="084D22AD-ECB1-400F-B4C7-418ECEC5E36E"'
						If FRM.txtCodi <> "" Then
							'UPGRADE_ISSUE: Control txtCodi no se pudo resolver porque est� dentro del espacio de nombres gen�rico Form. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="084D22AD-ECB1-400F-B4C7-418ECEC5E36E"'
							If xMsgBox("Existe el presupuesto {" & FRM.txtCodi & "} abierto.#�Quiere perder los cambios?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, Me.Text) = MsgBoxResult.No Then Exit Sub
						End If
					End If
				Next FRM
				'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
				System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
				
				With frmSolicitudTransport
					ObreSolicitud()
				End With
				'Unload Me  'per seguretat
				'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
				System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
			Case "PLAN" 'planificar el transport
				'PlanificaDesestima (2)
			Case "DES" 'desestimar la sol�licitud
				'PlanificaDesestima (3)
				'End Select
				
		End Select
	End Sub
	
	Private Sub optTipo_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optTipo.ClickEvent
		Dim Index As Short = optTipo.GetIndex(eventSender)
		Select Case Index
			Case 1
				txtTipo.Text = CStr(1) 'mercancia
				CarregaSolP()
			Case 2
				txtTipo.Text = CStr(2) 'mensajeria
				CarregaSolP()
		End Select
	End Sub
	
	Private Sub txtDataFi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataFi.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtDataIni_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataIni.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtFechaEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaEntrega.LostFocus
		If grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 15).Text = "" Then Exit Sub
		If txtFechaEntrega.Text <> Mid(grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 15).Text, 7, 4) & Mid(grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 3).Text, 4, 2) & Mid(grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 3).Text, 1, 2) Then
			Label6.Visible = True
			txtMotivoNoRealizadoT.Visible = True
			Text1.Visible = True
		Else
			Label6.Visible = False
			txtMotivoNoRealizadoT.Text = ""
			txtMotivoNoRealizadoT.Visible = False
			Text1.Visible = False
		End If
		
	End Sub
	
	
	Private Sub txtFechaRecogida_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaRecogida.LostFocus
		'    If grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 15).Text = "" Then Exit Sub
		'    If txtFechaEntrega <> Mid(grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 15).Text, 7, 4) & Mid(grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 3).Text, 4, 2) & Mid(grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 3).Text, 1, 2) Then
		'        Label6.Visible = True
		'        txtMotivoNoRealizadoT.Visible = True
		'        Text1.Visible = True
		'    Else
		'        Label6.Visible = False
		'        txtMotivoNoRealizadoT.Text = ""
		'        txtMotivoNoRealizadoT.Visible = False
		'        Text1.Visible = False
		'    End If
	End Sub
	
	Private Sub txtHorasTransporte_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHorasTransporte.LostFocus
		CalculaPreuTransport()
	End Sub
	
	Private Sub txtKilometros_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtKilometros.LostFocus
		CalculaPreuTransport()
	End Sub
	
	Private Sub txtMotivoNoRealizado_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoNoRealizado.DoubleClick
		ConsultaTaula(Me, txtMotivoNoRealizado)
	End Sub
	
	Private Sub txtMotivoNoRealizado_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMotivoNoRealizado.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtMotivoNoRealizado)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtMotivoNoRealizadoT_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoNoRealizadoT.DoubleClick
		ConsultaTaula(Me, txtMotivoNoRealizadoT)
	End Sub
	
	Private Sub txtMotivoNoRealizadoT_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMotivoNoRealizadoT.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtMotivoNoRealizadoT)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipoTarifa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoTarifa.DoubleClick
		ConsultaTaula(Me, txtTipoTarifa)
	End Sub
	
	Private Sub txtTipoTarifa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoTarifa.LostFocus
		CacheNetejaParametres()
		MCache.P1 = txtTransportista.Text
		MCache.P2 = txtTipoTarifa.Text
		MCache.P3 = txtFechaEntrega.Text
		txtPrecio.Text = CacheXecute("S VALUE=$$BP^TRANSPORT(P1,P2,P3)")
	End Sub
	
	Private Sub txtTipoTarifa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipoTarifa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTipoTarifa)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTransportista_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTransportista.DoubleClick
		ConsultaTaula(Me, txtTransportista)
	End Sub
	
	Private Sub txtTransportista_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTransportista.LostFocus
		'CalculaPreuTransport
	End Sub
	
	Private Sub txtTransportista_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTransportista.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTransportista)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub DesmarcaTot(ByRef noDesmarcar As Short)
		Dim I As Short
		
		For I = 1 To grdSolPlanificades.Rows - 1
			If noDesmarcar <> I Then
				grdSolPlanificades.Cell(I, 17).Text = ""
				grdSolPlanificades.Cell(I, 18).Text = ""
			End If
		Next I
	End Sub
	
	Private Sub EsborraDades()
		txtFechaRecogida.Text = ""
		txtFechaEntrega.Text = ""
		txtTipoTarifa.Text = ""
		txtComentario.Text = ""
		txtMotivoNoRealizado.Text = ""
		txtComentarioDes.Text = ""
		txtKilometros.Text = ""
		txtPrecio.Text = ""
		txtHorasTransporte.Value = ""
		Text38.Text = ""
		Text39.Text = ""
		Text34.Text = ""
		txtMotivoNoRealizadoT.Text = ""
		Text1.Text = ""
		txtVdaInt.Text = ""
		
	End Sub
	
	Function ValorSeleccionat() As String
		Dim I As Short
		Dim Valor As String
		Valor = ""
		For I = 0 To grdSolPlanificades.Rows - 1
			If grdSolPlanificades.Cell(I, 17).Text = "1" Then
				Valor = "P"
				Exit For
			End If
			If grdSolPlanificades.Cell(I, 18).Text = "1" Then
				Valor = "D"
				Exit For
			End If
		Next I
		ValorSeleccionat = Valor
	End Function
	
	Private Sub CarregaSolP()
		CarregaFGrid(grdSolPlanificades, "CGSOLP^TRANSPORT", txtTipo.Text & S & txtDataIni.Text & S & txtDataFi.Text & S & txtTransportistaF.Text)
	End Sub
	
	Private Sub CanviEstil()
		Me.BackColor = System.Drawing.ColorTranslator.FromOle(&HC0C0C0)
		frTipo.BackColor = System.Drawing.ColorTranslator.FromOle(&HC0C0C0)
		optTipo(1).BackColor = System.Drawing.ColorTranslator.FromOle(&HC0C0C0)
		optTipo(2).BackColor = System.Drawing.ColorTranslator.FromOle(&HC0C0C0)
		FrameTransp.BackColor = System.Drawing.ColorTranslator.FromOle(&HC0C0C0)
		FrameDes.BackColor = System.Drawing.ColorTranslator.FromOle(&HC0C0C0)
	End Sub
	
	Private Sub txtTransportistaF_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTransportistaF.DoubleClick
		ConsultaTaula(Me, txtTransportistaF)
		
	End Sub
	
	Private Sub txtTransportistaF_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTransportistaF.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtTransportistaF_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTransportistaF.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTransportistaF)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub CalculaPreuTransport()
		CacheNetejaParametres()
		MCache.P1 = txtFechaEntrega.Text
		MCache.P2 = txtTransportista.Text
		MCache.P3 = txtHorasTransporte.Text
		MCache.P4 = txtKilometros.Text
		txtPrecio.Text = CacheXecute("S VALUE=$$PREU^TRANSPORT(P1,P2,P3,P4)")
		txtVdaInt.Text = CacheXecute("S VALUE=$$VDAINTERNA^TRANSPORT(P2,P1,P4)")
	End Sub
	
	Private Sub ActualitzaDades()
		Dim Nod As String
		
		CacheNetejaParametres()
		MCache.P1 = txtSolicitud.Text
		
		Nod = CacheXecute("S VALUE=$G(^SOLTRANS(P1))")
		DesmarcaTot((grdSolPlanificades.ActiveCell.Row))
		'grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 18).Text = ""
		FrameTransp.Visible = True
		FrameDes.Visible = False
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(Nod, S, 41). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Piece(Nod, S, 41) = "" Then
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			txtFechaRecogida.Text = Piece(Nod, S, 35)
		Else
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			txtFechaRecogida.Text = Piece(Nod, S, 41) '35)
		End If
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(Nod, S, 39). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Piece(Nod, S, 39) = "" Then
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			txtFechaEntrega.Text = Piece(Nod, S, 35)
		Else
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			txtFechaEntrega.Text = Piece(Nod, S, 39) '35)
		End If
		If txtFechaRecogida.Text = "" Then
			txtFechaRecogida.Text = Mid(grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 3).Text, 7, 4) & Mid(grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 3).Text, 4, 2) & Mid(grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 3).Text, 1, 2)
		End If
		If txtFechaEntrega.Text = "" Then
			
		End If
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtHoraRecogida.Value = Piece(Nod, S, 42)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtHoraEntrega.Value = Piece(Nod, S, 40)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtTipoTarifa.Text = Piece(Nod, S, 33)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtPrecio.Text = Piece(Nod, S, 34)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtTransportista.Text = Piece(Nod, S, 37)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtComentario.Text = Piece(Nod, S, 43)
		DisplayDescripcio(Me, txtTipoTarifa)
		DisplayDescripcio(Me, txtTransportista)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtHorasTransporte.Value = Piece(Nod, S, 50)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtKilometros.Text = Piece(Nod, S, 51)
		
		If TransportIntern = "NO" Then
			lbl34.Visible = True
			Text34.Visible = True
			txtTipoTarifa.Visible = True
			lbl34.Top = 72      ' MigratPixels
			lbl34.Left = 8      ' MigratPixels
			txtTipoTarifa.Top = 68      ' MigratPixels
			txtTipoTarifa.Left = 98      ' MigratPixels
			Text34.Top = 68      ' MigratPixels
			Text34.Left = 120      ' MigratPixels
			Label4.Visible = False
			Label5.Visible = False
			txtHorasTransporte.Visible = False
			txtKilometros.Visible = False
		Else
			lbl34.Visible = False
			Text34.Visible = False
			txtTipoTarifa.Visible = False
			Label4.Visible = True
			Label5.Visible = True
			txtHorasTransporte.Visible = True
			txtKilometros.Visible = True
		End If
		
	End Sub
	Private Function GravaSol() As Boolean
		Dim Valor As String
		Dim EsIntern As String
		Dim Tipus As String
		
		Tipus = grdSolPlanificades.Cell(grdSolPlanificades.ActiveCell.Row, 6).Text
		
		GravaSol = False
		Valor = ValorSeleccionat
		If txtMotivoNoRealizadoT.Visible = True And txtMotivoNoRealizadoT.Text = "" Then
			xMsgBox("Falta informar el motivo de porqu� se ha cambiado la fecha del transporte", MsgBoxStyle.Information, Me.Text)
			txtMotivoNoRealizadoT.Focus()
			Exit Function
		End If
		
		If Valor = "P" Or Valor = "" Then
			If txtFechaRecogida.Text = "" Then
				xMsgBox("Falta informar la fecha recogida", MsgBoxStyle.Information, Me.Text)
				txtFechaRecogida.Focus()
				Exit Function
			End If
			If txtHoraRecogida.Value = "" Then
				xMsgBox("Falta informar la hora recogida", MsgBoxStyle.Information, Me.Text)
				txtHoraRecogida.Focus()
				Exit Function
			End If
			If txtFechaEntrega.Text = "" Then
				xMsgBox("Falta informar la fecha entrega", MsgBoxStyle.Information, Me.Text)
				txtFechaEntrega.Focus()
				Exit Function
			End If
			If txtHoraEntrega.Value = "" Then
				xMsgBox("Falta informar la hora entrega", MsgBoxStyle.Information, Me.Text)
				txtHoraEntrega.Focus()
				Exit Function
			End If
			
		End If
		
		If Valor = "P" Then
			CacheNetejaParametres()
			MCache.P1 = txtSolicitud.Text
			EsIntern = CacheXecute("S VALUE=$$ESINTERN^TRANSPORT(P1)")
			If txtHorasTransporte.Value = "" And EsIntern = "SI" And Tipus = "MER" Then
				xMsgBox("Falta informar el tiempo de transporte", MsgBoxStyle.Information, Me.Text)
				txtHorasTransporte.Focus()
				Exit Function
			End If
			If txtKilometros.Text = "" And EsIntern = "SI" And Tipus = "MER" Then
				xMsgBox("Falta informar los kil�metros", MsgBoxStyle.Information, Me.Text)
				txtKilometros.Focus()
				Exit Function
			End If
			If txtTipoTarifa.Visible = True And txtTipoTarifa.Text = "" Then
				xMsgBox("Falta informar el tipo tarifa", MsgBoxStyle.Information, Me.Text)
				txtTipoTarifa.Focus()
				Exit Function
			End If
		End If
		If txtFechaEntrega.Text < txtFechaRecogida.Text Then
			xMsgBox("La fecha entrega no puede ser m�s peque�a que la fecha recogida", MsgBoxStyle.Information, Me.Text)
			txtFechaEntrega.Focus()
			Exit Function
		End If
		
		If txtFechaEntrega.Text = txtFechaRecogida.Text And txtHoraEntrega.Value < txtHoraRecogida.Value Then
			xMsgBox("La hora de entrega no puede ser m�s peque�a que la hora de recogida", MsgBoxStyle.Information, Me.Text)
			txtHoraEntrega.Focus()
			Exit Function
		End If
		
		CacheNetejaParametres()
		MCache.P1 = txtSolicitud.Text
		MCache.P2 = txtFechaRecogida.Text & S & txtFechaEntrega.Text & S & txtHoraRecogida.Value & S & txtHoraEntrega.Value & S & txtTipoTarifa.Text & S & txtPrecio.Text & S & txtComentario.Text & S & txtTransportista.Text & S & txtHorasTransporte.Value & S & txtKilometros.Text & S & txtMotivoNoRealizadoT.Text
		CacheXecute("D GRAVSOLPD^TRANSPORT")
		GravaSol = True
	End Function
End Class
