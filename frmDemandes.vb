Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmDemandes
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
		cmbEstatDemanda.Enabled = False
		'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		txtDataTancament.Enabled = False
	End Sub
	
	Private Sub frmDemandes_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If ABM = "AL" Then cmbEstatDemanda.Columns(1).Value = 1
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If ABM = "AL" Then cmbEstatDemanda.Columns(1).Value = 1
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		
	End Sub
	
	Private Sub frmDemandes_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmDemandes_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmDemandes_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCodi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.GotFocus
		XGotFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.DoubleClick
		ConsultaTaula(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.LostFocus
		If txtCodi.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtCodi)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDatalimit_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDatalimit.GotFocus
		XGotFocus(Me, txtDatalimit)
	End Sub
	
	Private Sub txtDatalimit_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDatalimit.LostFocus
		XLostFocus(Me, txtDatalimit)
	End Sub
	
	Private Sub txtDemandant_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDemandant.GotFocus
		XGotFocus(Me, txtDemandant)
	End Sub
	
	Private Sub txtDemandant_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDemandant.DoubleClick
		ConsultaTaula(Me, txtDemandant)
	End Sub
	
	Private Sub txtDemandant_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDemandant.LostFocus
		XLostFocus(Me, txtDemandant)
	End Sub
	
	Private Sub txtDemandant_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDemandant.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtDemandant)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtDemandant)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtData_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtData.GotFocus
		XGotFocus(Me, txtData)
	End Sub
	
	Private Sub txtData_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtData.LostFocus
		XLostFocus(Me, txtData)
	End Sub
	
	Private Sub txtTipusDemanda_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusDemanda.GotFocus
		XGotFocus(Me, txtTipusDemanda)
	End Sub
	
	Private Sub txtTipusDemanda_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusDemanda.DoubleClick
		ConsultaTaula(Me, txtTipusDemanda)
	End Sub
	
	Private Sub txtTipusDemanda_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusDemanda.LostFocus
		XLostFocus(Me, txtTipusDemanda)
	End Sub
	
	Private Sub txtTipusDemanda_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipusDemanda.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTipusDemanda)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtTipusDemanda)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSubtipusDemanda_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubtipusDemanda.GotFocus
		XGotFocus(Me, txtSubtipusDemanda)
	End Sub
	
	Private Sub txtSubtipusDemanda_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubtipusDemanda.DoubleClick
		ConsultaTaula(Me, txtSubtipusDemanda,  , txtTipusDemanda.Text)
	End Sub
	
	Private Sub txtSubtipusDemanda_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubtipusDemanda.LostFocus
		XLostFocus(Me, txtSubtipusDemanda)
	End Sub
	
	Private Sub txtSubtipusDemanda_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSubtipusDemanda.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtSubtipusDemanda, txtTipusDemanda.Text)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtSubtipusDemanda)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCentreQueGestiona_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreQueGestiona.GotFocus
		XGotFocus(Me, txtCentreQueGestiona)
	End Sub
	
	Private Sub txtCentreQueGestiona_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreQueGestiona.DoubleClick
		ConsultaTaula(Me, txtCentreQueGestiona)
	End Sub
	
	Private Sub txtCentreQueGestiona_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreQueGestiona.LostFocus
		XLostFocus(Me, txtCentreQueGestiona)
	End Sub
	
	Private Sub txtCentreQueGestiona_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentreQueGestiona.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtCentreQueGestiona)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtCentreQueGestiona)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPersonaQueGestiona_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPersonaQueGestiona.GotFocus
		XGotFocus(Me, txtPersonaQueGestiona)
	End Sub
	
	Private Sub txtPersonaQueGestiona_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPersonaQueGestiona.DoubleClick
		ConsultaTaula(Me, txtPersonaQueGestiona)
	End Sub
	
	Private Sub txtPersonaQueGestiona_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPersonaQueGestiona.LostFocus
		XLostFocus(Me, txtPersonaQueGestiona)
	End Sub
	
	Private Sub txtPersonaQueGestiona_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPersonaQueGestiona.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtPersonaQueGestiona)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtPersonaQueGestiona)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtProblema_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtProblema.GotFocus
		XGotFocus(Me, txtProblema)
	End Sub
	
	Private Sub txtProblema_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtProblema.LostFocus
		XLostFocus(Me, txtProblema)
	End Sub
	
	Private Sub txtProblema_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtProblema.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtProblema)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbEstatDemanda_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstatDemanda.GotFocus
		XGotFocus(Me, cmbEstatDemanda)
	End Sub
	
	Private Sub cmbEstatDemanda_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstatDemanda.LostFocus
		XLostFocus(Me, cmbEstatDemanda)
	End Sub
	
	
	Private Sub txtUsuariEntigestGestio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuariEntigestGestio.GotFocus
		XGotFocus(Me, txtUsuariEntigestGestio)
	End Sub
	
	Private Sub txtUsuariEntigestGestio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuariEntigestGestio.LostFocus
		XLostFocus(Me, txtUsuariEntigestGestio)
	End Sub
End Class
