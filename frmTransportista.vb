Option Strict Off
Option Explicit On
Friend Class frmTransportista
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		CarregaGrids()
	End Sub
	
	Public Overrides Sub FGetReg()
		CarregaGrids()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	'UPGRADE_WARNING: Form evento frmTransportista.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmTransportista_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaGrids()
	End Sub
	
	Private Sub frmTransportista_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmTransportista_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmTransportista_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmTransportista_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub grdKM_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdKM.DoubleClick
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmTransportistaPreuKm, "frmTransportistaPreuKm", txtCodigo.Text & S & "KM" & S & Piece(grdKM.Cell(grdKM.ActiveCell.Row, 1).Text, "|", 2))
	End Sub
	
	Private Sub grdKM_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdKM.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtCodigo.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdKM", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nuevo precio kil�metro",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmTransportista", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar precio kil�metro",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = grdKM.Rows > 1 And PermisConsulta("frmTransportista", e_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar precio kil�metro",  ,  , XPIcon("Delete"),  ,  ,  ,  , "B")
				.MenuItems.Item("B").Enabled = grdKM.Rows > 1 And PermisConsulta("frmTransportista", e_Permisos.Baixes)
			End With
		End With
		XpExecutaMenu(Me, "grdKM", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub grdPT_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdPT.DoubleClick
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmTransportistaPreuHora, "frmTransportistaPreuHora", txtCodigo.Text & S & "H" & S & Piece(grdPT.Cell(grdPT.ActiveCell.Row, 1).Text, "|", 2))
	End Sub
	
	Private Sub grdPT_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdPT.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtCodigo.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdPT", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nuevo precio hora",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmTransportista", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar precio hora",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = grdPT.Rows > 1 And PermisConsulta("frmTransportista", e_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar precio hora",  ,  , XPIcon("Delete"),  ,  ,  ,  , "B")
				.MenuItems.Item("B").Enabled = grdPT.Rows > 1 And PermisConsulta("frmTransportista", e_Permisos.Baixes)
			End With
		End With
		XpExecutaMenu(Me, "grdPT", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub GrdTTarif_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdTTarif.DoubleClick
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdTTarif.Cell(GrdTTarif.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmTransportistaTarifa, "frmTransportistaTarifa", txtCodigo.Text & S & "T" & S & Piece(GrdTTarif.Cell(GrdTTarif.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdTTarif.Cell(GrdTTarif.ActiveCell.Row, 1).Text, "|", 3))
	End Sub
	
	Private Sub GrdTTarif_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdTTarif.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtCodigo.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdTTarif", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nuevo precio tarifa",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmTransportista", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar precio tarifa",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdTTarif.Rows > 1 And PermisConsulta("frmTransportista", e_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar precio tarifa",  ,  , XPIcon("Delete"),  ,  ,  ,  , "B")
				.MenuItems.Item("B").Enabled = GrdTTarif.Rows > 1 And PermisConsulta("frmTransportista", e_Permisos.Baixes)
			End With
		End With
		XpExecutaMenu(Me, "GrdTTarif", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub GrdVdaInt_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdVdaInt.DoubleClick
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdVdaInt.Cell(GrdVdaInt.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmTransportistaVdaInterna, "frmTransportistaVdaInterna", txtCodigo.Text & S & "I" & S & Piece(GrdVdaInt.Cell(GrdVdaInt.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdVdaInt.Cell(GrdVdaInt.ActiveCell.Row, 1).Text, "|", 3))
	End Sub
	
	Private Sub GrdVdaInt_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdVdaInt.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtCodigo.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdVdaInt", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nuevo precio tarifa",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmTransportista", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar precio tarifa",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdVdaInt.Rows > 1 And PermisConsulta("frmTransportista", e_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar precio tarifa",  ,  , XPIcon("Delete"),  ,  ,  ,  , "B")
				.MenuItems.Item("B").Enabled = GrdVdaInt.Rows > 1 And PermisConsulta("frmTransportista", e_Permisos.Baixes)
			End With
		End With
		XpExecutaMenu(Me, "GrdVdaInt", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub optTipo_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optTipo.ClickEvent
		Dim Index As Short = optTipo.GetIndex(eventSender)
		Select Case Index
			Case 1
				txtTipo.Text = CStr(1) 'intern
			Case 2
				txtTipo.Text = CStr(2) 'extern
		End Select
	End Sub
	
	Private Sub optTipoTrans_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optTipoTrans.ClickEvent
		Dim Index As Short = optTipoTrans.GetIndex(eventSender)
		Select Case Index
			Case 1
				txtTransporte.Text = CStr(1) 'intern
			Case 2
				txtTransporte.Text = CStr(2) 'extern
		End Select
	End Sub
	
	Private Sub txtCodigo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.GotFocus
		XGotFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.DoubleClick
		ConsultaTaula(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.LostFocus
		If txtCodigo.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodigo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtNombre_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombre.GotFocus
		XGotFocus(Me, txtNombre)
	End Sub
	
	Private Sub txtNombre_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombre.LostFocus
		XLostFocus(Me, txtNombre)
	End Sub
	
	Private Sub txtNombre_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNombre.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.GotFocus
		XGotFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtFechaBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.LostFocus
		XLostFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub CarregaGrids()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(grdPT, "CGH^TRANSPORT", txtCodigo.Text)
		CarregaFGrid(grdKM, "CGKM^TRANSPORT", txtCodigo.Text)
		CarregaFGrid(GrdTTarif, "TTARIF^TRANSPORT", txtCodigo.Text)
		CarregaFGrid(GrdVdaInt, "VDAINT^TRANSPORT", txtCodigo.Text)
	End Sub
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Dim intColOrdAux As Short
		Select Case NomMenu
			Case "grdPT"
				Select Case KeyMenu
					Case "N"
						With frmTransportistaPreuHora
							'.blnEntExterna = True
							.txtCodigo.Text = txtCodigo.Text
							.txtCodigo.Enabled = False
							.Show()
						End With
					Case "E"
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTransportistaPreuHora, "frmTransportistaPreuHora", txtCodigo.Text & S & "H" & S & Piece(grdPT.Cell(grdPT.ActiveCell.Row, 1).Text, "|", 2))
					Case "B"
						'If Not VerObertSubAct Then Exit Sub
						A = grdPT.Cell(grdPT.ActiveCell.Row, 2).Text
						If A = "" Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTransportistaPreuHora, "frmTransportistaPreuHora", txtCodigo.Text & S & "H" & S & Piece(grdPT.Cell(grdPT.ActiveCell.Row, 1).Text, "|", 2))
						LlibAplicacio.DeleteReg(frmTransportistaPreuHora   )
						frmTransportistaPreuHora.Unload()
						
				End Select
				
			Case "grdKM"
				Select Case KeyMenu
					Case "N"
						With frmTransportistaPreuKm
							'.blnEntExterna = True
							.txtCodigo.Text = txtCodigo.Text
							.txtCodigo.Enabled = False
							.Show()
						End With
					Case "E"
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTransportistaPreuKm, "frmTransportistaPreuKm", txtCodigo.Text & S & "KM" & S & Piece(grdKM.Cell(grdKM.ActiveCell.Row, 1).Text, "|", 2))
					Case "B"
						'If Not VerObertSubAct Then Exit Sub
						A = grdPT.Cell(grdKM.ActiveCell.Row, 2).Text
						If A = "" Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTransportistaPreuKm, "frmTransportistaPreuKm", txtCodigo.Text & S & "KM" & S & Piece(grdKM.Cell(grdKM.ActiveCell.Row, 1).Text, "|", 2))
						LlibAplicacio.DeleteReg(frmTransportistaPreuKm   )
						frmTransportistaPreuKm.Unload()
						
				End Select
			Case "GrdTTarif"
				Select Case KeyMenu
					Case "N"
						With frmTransportistaTarifa
							'.blnEntExterna = True
							.txtTransportista.Text = txtCodigo.Text
							.txtTransportista.Enabled = False
							.Show()
						End With
					Case "E"
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdTTarif.Cell(GrdTTarif.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTransportistaTarifa, "frmTransportistaTarifa", txtCodigo.Text & S & "T" & S & Piece(GrdTTarif.Cell(GrdTTarif.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdTTarif.Cell(GrdTTarif.ActiveCell.Row, 1).Text, "|", 3))
					Case "B"
						'If Not VerObertSubAct Then Exit Sub
						A = GrdTTarif.Cell(GrdTTarif.ActiveCell.Row, 2).Text
						If A = "" Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdTTarif.Cell(GrdTTarif.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTransportistaTarifa, "frmTransportistaTarifa", txtCodigo.Text & S & "T" & S & Piece(GrdTTarif.Cell(GrdTTarif.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdTTarif.Cell(GrdTTarif.ActiveCell.Row, 1).Text, "|", 3))
						LlibAplicacio.DeleteReg(frmTransportistaTarifa   )
						frmTransportistaTarifa.Unload()
						
				End Select
			Case "GrdVdaInt"
				Select Case KeyMenu
					Case "N"
						With frmTransportistaVdaInterna
							'.blnEntExterna = True
							.txtTransportista.Text = txtCodigo.Text
							.txtTransportista.Enabled = False
							.Show()
						End With
					Case "E"
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdVdaInt.Cell(GrdVdaInt.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTransportistaVdaInterna, "frmTransportistaVdaInterna", txtCodigo.Text & S & "I" & S & Piece(GrdVdaInt.Cell(GrdVdaInt.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdVdaInt.Cell(GrdVdaInt.ActiveCell.Row, 1).Text, "|", 3))
					Case "B"
						'If Not VerObertSubAct Then Exit Sub
						A = grdPT.Cell(grdKM.ActiveCell.Row, 2).Text
						If A = "" Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdVdaInt.Cell(GrdVdaInt.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTransportistaVdaInterna, "frmTransportistaVdaInterna", txtCodigo.Text & S & "I" & S & Piece(GrdVdaInt.Cell(GrdVdaInt.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdVdaInt.Cell(GrdVdaInt.ActiveCell.Row, 1).Text, "|", 3))
						LlibAplicacio.DeleteReg(frmTransportistaVdaInterna   )
						frmTransportistaVdaInterna.Unload()
						
				End Select
				
		End Select
		
	End Sub
	
	'UPGRADE_WARNING: El evento txtTipo.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtTipo_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipo.TextChanged
		If txtTipo.Text = "" Then Exit Sub
		optTipo(txtTipo).Value = True
	End Sub
	
	'UPGRADE_WARNING: El evento txtTransporte.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtTransporte_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTransporte.TextChanged
		If txtTransporte.Text = "" Then Exit Sub
		optTipoTrans(txtTransporte).Value = True
	End Sub
End Class
