Option Strict Off
Option Explicit On
Friend Class frmLoteriaPEntrega
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmdLliurar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdLliurar.ClickEvent
		LliuraLoteria()
	End Sub
	
	Private Sub cmdLliurarImprimir_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdLliurarImprimir.ClickEvent
		If txtDataEntrega.Text = "" Then
			xMsgBox("Falta informar la data de lliurament", MsgBoxStyle.Information, Me.Text)
			txtDataEntrega.Focus()
			Exit Sub
		End If
		
		ImprimeixLoteria()
		LliuraLoteria()
	End Sub
	
	'UPGRADE_WARNING: Form evento frmLoteriaPEntrega.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmLoteriaPEntrega_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaPEntrega()
	End Sub
	
	Private Sub frmLoteriaPEntrega_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmLoteriaPEntrega_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		cmdLliurar.Picture = SetIcon("BlueOK", 16)
		cmdLliurarImprimir.Picture = SetIcon("Genera", 16)
		CarregaPEntrega()
	End Sub
	
	Sub CarregaPEntrega()
		CarregaFGrid(GrdPEntrega, "PENTREGA^VARIS")
	End Sub
	
	Private Sub frmLoteriaPEntrega_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		CacheNetejaParametres()
		MCache.P1 = JobNumber
		CacheXecute("K ^WORK(P1)")
	End Sub
	
	Private Sub txtDataEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataEntrega.GotFocus
		SelTxT(Me)
	End Sub
	
	
	Private Sub CarregaErrors()
		Dim Errors As String
		CacheNetejaParametres()
		MCache.P1 = JobNumber
		Errors = CacheXecute("S VALUE=$$ERRORS^VARIS(P1)")
		If Errors <> "" Then xMsgBox("Registre agafat per una altre sessi� de treball." & vbCr & "No s'ha pogut lliurar a :" & Errors, MsgBoxStyle.Critical, Me.Text)
	End Sub
	
	
	Private Sub LliuraLoteria()
		Dim I As Short
		Dim Nod As String
		Dim Resp As MsgBoxResult
		Nod = ""
		
		If txtDataEntrega.Text = "" Then
			xMsgBox("Falta informar la data de lliurament", MsgBoxStyle.Information, Me.Text)
			txtDataEntrega.Focus()
			Exit Sub
		End If
		
		
		For I = 1 To GrdPEntrega.Rows - 1
			If GrdPEntrega.Cell(I, 6).Text = "1" Then
				If Nod <> "" Then Nod = Nod & Chr(13)
				Nod = Nod & GrdPEntrega.Cell(I, 1).Text
			End If
		Next I
		
		
		Resp = MsgBox("Est�s segur/a de lliurar la loteria a les persones sel�leccionades ?", MsgBoxStyle.YesNo, Me.Text)
		If Resp = MsgBoxResult.Yes Then
			If Nod = "" Then
				xMsgBox("No hi ha cap persona sel�leccionada !!!", MsgBoxStyle.Information, Me.Text)
				Exit Sub
			End If
			
			CacheNetejaParametres()
			MCache.PDELIM = Chr(13)
			MCache.PLIST = Nod
			MCache.P1 = txtDataEntrega.Text
			CacheXecute("D LLPENTREGA^VARIS")
		End If
		
		
		
		CarregaPEntrega()
		
		CarregaErrors()
	End Sub
	
	Private Sub ImprimeixLoteria()
		Dim I As Short
		Dim Nod As String
		Dim Codi As Short
		
		For I = 1 To GrdPEntrega.Rows - 1
			If GrdPEntrega.Cell(I, 6).Text = "1" Then
				Codi = CShort(GrdPEntrega.Cell(I, 1).Text)
				ImpresioPDF("I-IMPRES_LOTERIA_B", Codi & S & txtDataEntrega.Text)
				Retard(1)
			End If
		Next I
		
	End Sub
End Class
