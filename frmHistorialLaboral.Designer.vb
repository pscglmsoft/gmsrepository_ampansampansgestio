<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmHistorialLaboral
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtTipoContrato As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents txtjornada As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresaNoCod As System.Windows.Forms.TextBox
	Public WithEvents txtUsuario As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtFechaAlta As AxDataControl.AxGmsData
	Public WithEvents txtNumRegistro As System.Windows.Forms.TextBox
	Public WithEvents txtFechaBaja As AxDataControl.AxGmsData
	Public WithEvents txtMotivoBaja As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtLugarTrabajo As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents txtObservaciones As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents Label30 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmHistorialLaboral))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtTipoContrato = New System.Windows.Forms.TextBox
		Me.Text9 = New System.Windows.Forms.TextBox
		Me.txtjornada = New System.Windows.Forms.TextBox
		Me.txtEmpresaNoCod = New System.Windows.Forms.TextBox
		Me.txtUsuario = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtFechaAlta = New AxDataControl.AxGmsData
		Me.txtNumRegistro = New System.Windows.Forms.TextBox
		Me.txtFechaBaja = New AxDataControl.AxGmsData
		Me.txtMotivoBaja = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtLugarTrabajo = New System.Windows.Forms.TextBox
		Me.Text7 = New System.Windows.Forms.TextBox
		Me.txtObservaciones = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl9 = New System.Windows.Forms.Label
		Me.Label30 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtFechaAlta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Historial laboral"
		Me.ClientSize = New System.Drawing.Size(487, 355)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-HISTORIAL_LABORAL"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmHistorialLaboral"
		Me.txtTipoContrato.AutoSize = False
		Me.txtTipoContrato.Size = New System.Drawing.Size(42, 19)
		Me.txtTipoContrato.Location = New System.Drawing.Point(124, 202)
		Me.txtTipoContrato.Maxlength = 3
		Me.txtTipoContrato.TabIndex = 8
		Me.txtTipoContrato.Tag = "11"
		Me.txtTipoContrato.AcceptsReturn = True
		Me.txtTipoContrato.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoContrato.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoContrato.CausesValidation = True
		Me.txtTipoContrato.Enabled = True
		Me.txtTipoContrato.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoContrato.HideSelection = True
		Me.txtTipoContrato.ReadOnly = False
		Me.txtTipoContrato.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoContrato.MultiLine = False
		Me.txtTipoContrato.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoContrato.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoContrato.TabStop = True
		Me.txtTipoContrato.Visible = True
		Me.txtTipoContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoContrato.Name = "txtTipoContrato"
		Me.Text9.AutoSize = False
		Me.Text9.BackColor = System.Drawing.Color.White
		Me.Text9.Enabled = False
		Me.Text9.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text9.Size = New System.Drawing.Size(306, 19)
		Me.Text9.Location = New System.Drawing.Point(169, 202)
		Me.Text9.TabIndex = 28
		Me.Text9.Tag = "^11"
		Me.Text9.AcceptsReturn = True
		Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text9.CausesValidation = True
		Me.Text9.HideSelection = True
		Me.Text9.ReadOnly = False
		Me.Text9.Maxlength = 0
		Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text9.MultiLine = False
		Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text9.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text9.TabStop = True
		Me.Text9.Visible = True
		Me.Text9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text9.Name = "Text9"
		Me.txtjornada.AutoSize = False
		Me.txtjornada.Size = New System.Drawing.Size(43, 19)
		Me.txtjornada.Location = New System.Drawing.Point(124, 226)
		Me.txtjornada.Maxlength = 6
		Me.txtjornada.TabIndex = 9
		Me.txtjornada.Tag = "10"
		Me.txtjornada.AcceptsReturn = True
		Me.txtjornada.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtjornada.BackColor = System.Drawing.SystemColors.Window
		Me.txtjornada.CausesValidation = True
		Me.txtjornada.Enabled = True
		Me.txtjornada.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtjornada.HideSelection = True
		Me.txtjornada.ReadOnly = False
		Me.txtjornada.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtjornada.MultiLine = False
		Me.txtjornada.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtjornada.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtjornada.TabStop = True
		Me.txtjornada.Visible = True
		Me.txtjornada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtjornada.Name = "txtjornada"
		Me.txtEmpresaNoCod.AutoSize = False
		Me.txtEmpresaNoCod.BackColor = System.Drawing.Color.White
		Me.txtEmpresaNoCod.ForeColor = System.Drawing.Color.Black
		Me.txtEmpresaNoCod.Size = New System.Drawing.Size(350, 19)
		Me.txtEmpresaNoCod.Location = New System.Drawing.Point(125, 154)
		Me.txtEmpresaNoCod.TabIndex = 6
		Me.txtEmpresaNoCod.Tag = "9"
		Me.txtEmpresaNoCod.AcceptsReturn = True
		Me.txtEmpresaNoCod.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresaNoCod.CausesValidation = True
		Me.txtEmpresaNoCod.Enabled = True
		Me.txtEmpresaNoCod.HideSelection = True
		Me.txtEmpresaNoCod.ReadOnly = False
		Me.txtEmpresaNoCod.Maxlength = 0
		Me.txtEmpresaNoCod.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresaNoCod.MultiLine = False
		Me.txtEmpresaNoCod.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresaNoCod.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresaNoCod.TabStop = True
		Me.txtEmpresaNoCod.Visible = True
		Me.txtEmpresaNoCod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresaNoCod.Name = "txtEmpresaNoCod"
		Me.txtUsuario.AutoSize = False
		Me.txtUsuario.Size = New System.Drawing.Size(42, 19)
		Me.txtUsuario.Location = New System.Drawing.Point(124, 10)
		Me.txtUsuario.Maxlength = 4
		Me.txtUsuario.TabIndex = 0
		Me.txtUsuario.Tag = "*1"
		Me.txtUsuario.AcceptsReturn = True
		Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsuario.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsuario.CausesValidation = True
		Me.txtUsuario.Enabled = True
		Me.txtUsuario.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsuario.HideSelection = True
		Me.txtUsuario.ReadOnly = False
		Me.txtUsuario.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsuario.MultiLine = False
		Me.txtUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsuario.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsuario.TabStop = True
		Me.txtUsuario.Visible = True
		Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUsuario.Name = "txtUsuario"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(306, 19)
		Me.Text1.Location = New System.Drawing.Point(169, 10)
		Me.Text1.TabIndex = 13
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		txtFechaAlta.OcxState = CType(resources.GetObject("txtFechaAlta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaAlta.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaAlta.Location = New System.Drawing.Point(124, 34)
		Me.txtFechaAlta.TabIndex = 1
		Me.txtFechaAlta.Name = "txtFechaAlta"
		Me.txtNumRegistro.AutoSize = False
		Me.txtNumRegistro.Size = New System.Drawing.Size(21, 19)
		Me.txtNumRegistro.Location = New System.Drawing.Point(124, 58)
		Me.txtNumRegistro.Maxlength = 2
		Me.txtNumRegistro.TabIndex = 2
		Me.txtNumRegistro.Tag = "*3"
		Me.txtNumRegistro.AcceptsReturn = True
		Me.txtNumRegistro.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNumRegistro.BackColor = System.Drawing.SystemColors.Window
		Me.txtNumRegistro.CausesValidation = True
		Me.txtNumRegistro.Enabled = True
		Me.txtNumRegistro.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNumRegistro.HideSelection = True
		Me.txtNumRegistro.ReadOnly = False
		Me.txtNumRegistro.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNumRegistro.MultiLine = False
		Me.txtNumRegistro.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNumRegistro.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNumRegistro.TabStop = True
		Me.txtNumRegistro.Visible = True
		Me.txtNumRegistro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNumRegistro.Name = "txtNumRegistro"
		txtFechaBaja.OcxState = CType(resources.GetObject("txtFechaBaja.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaBaja.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaBaja.Location = New System.Drawing.Point(124, 82)
		Me.txtFechaBaja.TabIndex = 3
		Me.txtFechaBaja.Name = "txtFechaBaja"
		Me.txtMotivoBaja.AutoSize = False
		Me.txtMotivoBaja.Size = New System.Drawing.Size(21, 19)
		Me.txtMotivoBaja.Location = New System.Drawing.Point(124, 106)
		Me.txtMotivoBaja.Maxlength = 2
		Me.txtMotivoBaja.TabIndex = 4
		Me.txtMotivoBaja.Tag = "5"
		Me.txtMotivoBaja.AcceptsReturn = True
		Me.txtMotivoBaja.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMotivoBaja.BackColor = System.Drawing.SystemColors.Window
		Me.txtMotivoBaja.CausesValidation = True
		Me.txtMotivoBaja.Enabled = True
		Me.txtMotivoBaja.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMotivoBaja.HideSelection = True
		Me.txtMotivoBaja.ReadOnly = False
		Me.txtMotivoBaja.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMotivoBaja.MultiLine = False
		Me.txtMotivoBaja.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMotivoBaja.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMotivoBaja.TabStop = True
		Me.txtMotivoBaja.Visible = True
		Me.txtMotivoBaja.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMotivoBaja.Name = "txtMotivoBaja"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(326, 19)
		Me.Text5.Location = New System.Drawing.Point(148, 106)
		Me.Text5.TabIndex = 18
		Me.Text5.Tag = "^5"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(83, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(124, 130)
		Me.txtEmpresa.Maxlength = 8
		Me.txtEmpresa.TabIndex = 5
		Me.txtEmpresa.Tag = "6"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(264, 19)
		Me.Text6.Location = New System.Drawing.Point(210, 130)
		Me.Text6.TabIndex = 20
		Me.Text6.Tag = "^6"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtLugarTrabajo.AutoSize = False
		Me.txtLugarTrabajo.Size = New System.Drawing.Size(21, 19)
		Me.txtLugarTrabajo.Location = New System.Drawing.Point(124, 178)
		Me.txtLugarTrabajo.Maxlength = 2
		Me.txtLugarTrabajo.TabIndex = 7
		Me.txtLugarTrabajo.Tag = "7"
		Me.txtLugarTrabajo.AcceptsReturn = True
		Me.txtLugarTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLugarTrabajo.BackColor = System.Drawing.SystemColors.Window
		Me.txtLugarTrabajo.CausesValidation = True
		Me.txtLugarTrabajo.Enabled = True
		Me.txtLugarTrabajo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLugarTrabajo.HideSelection = True
		Me.txtLugarTrabajo.ReadOnly = False
		Me.txtLugarTrabajo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLugarTrabajo.MultiLine = False
		Me.txtLugarTrabajo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLugarTrabajo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLugarTrabajo.TabStop = True
		Me.txtLugarTrabajo.Visible = True
		Me.txtLugarTrabajo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtLugarTrabajo.Name = "txtLugarTrabajo"
		Me.Text7.AutoSize = False
		Me.Text7.BackColor = System.Drawing.Color.White
		Me.Text7.Enabled = False
		Me.Text7.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text7.Size = New System.Drawing.Size(326, 19)
		Me.Text7.Location = New System.Drawing.Point(148, 178)
		Me.Text7.TabIndex = 22
		Me.Text7.Tag = "^7"
		Me.Text7.AcceptsReturn = True
		Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text7.CausesValidation = True
		Me.Text7.HideSelection = True
		Me.Text7.ReadOnly = False
		Me.Text7.Maxlength = 0
		Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text7.MultiLine = False
		Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text7.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text7.TabStop = True
		Me.Text7.Visible = True
		Me.Text7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text7.Name = "Text7"
		Me.txtObservaciones.AutoSize = False
		Me.txtObservaciones.Size = New System.Drawing.Size(351, 45)
		Me.txtObservaciones.Location = New System.Drawing.Point(124, 250)
		Me.txtObservaciones.Maxlength = 100
		Me.txtObservaciones.TabIndex = 10
		Me.txtObservaciones.Tag = "8"
		Me.txtObservaciones.AcceptsReturn = True
		Me.txtObservaciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservaciones.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservaciones.CausesValidation = True
		Me.txtObservaciones.Enabled = True
		Me.txtObservaciones.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservaciones.HideSelection = True
		Me.txtObservaciones.ReadOnly = False
		Me.txtObservaciones.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservaciones.MultiLine = False
		Me.txtObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservaciones.TabStop = True
		Me.txtObservaciones.Visible = True
		Me.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservaciones.Name = "txtObservaciones"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(293, 314)
		Me.cmdAceptar.TabIndex = 11
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(383, 314)
		Me.cmdGuardar.TabIndex = 24
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl9.Text = "Tipo contrato"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(10, 206)
		Me.lbl9.TabIndex = 29
		Me.lbl9.Tag = "11"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.Label30.Text = "% Jornada"
		Me.Label30.Size = New System.Drawing.Size(111, 15)
		Me.Label30.Location = New System.Drawing.Point(10, 230)
		Me.Label30.TabIndex = 27
		Me.Label30.Tag = "10"
		Me.Label30.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label30.BackColor = System.Drawing.SystemColors.Control
		Me.Label30.Enabled = True
		Me.Label30.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label30.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label30.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label30.UseMnemonic = True
		Me.Label30.Visible = True
		Me.Label30.AutoSize = False
		Me.Label30.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label30.Name = "Label30"
		Me.Label1.Text = "Empresa no cod."
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 158)
		Me.Label1.TabIndex = 26
		Me.Label1.Tag = "9"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Usuario"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 12
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Fecha alta"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 14
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Num. registro"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 15
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Fecha baja"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 86)
		Me.lbl4.TabIndex = 16
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Motivo baja"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 110)
		Me.lbl5.TabIndex = 17
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Empresa"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 134)
		Me.lbl6.TabIndex = 19
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "Lugar de trabajo"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 182)
		Me.lbl7.TabIndex = 21
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl8.Text = "Observaciones"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(10, 254)
		Me.lbl8.TabIndex = 23
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 313)
		Me.lblLock.TabIndex = 25
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 307
		Me.Line1.Y2 = 307
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 308
		Me.Line2.Y2 = 308
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaAlta, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtTipoContrato)
		Me.Controls.Add(Text9)
		Me.Controls.Add(txtjornada)
		Me.Controls.Add(txtEmpresaNoCod)
		Me.Controls.Add(txtUsuario)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtFechaAlta)
		Me.Controls.Add(txtNumRegistro)
		Me.Controls.Add(txtFechaBaja)
		Me.Controls.Add(txtMotivoBaja)
		Me.Controls.Add(Text5)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text6)
		Me.Controls.Add(txtLugarTrabajo)
		Me.Controls.Add(Text7)
		Me.Controls.Add(txtObservaciones)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(Label30)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class