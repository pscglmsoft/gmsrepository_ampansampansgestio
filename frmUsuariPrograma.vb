Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmUsuariPrograma
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic FlagExtern As Boolean
	''OKPublic RegistreExtern As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	Private Sub frmUsuariPrograma_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		FlagExtern = False
		RegistreExtern = ""
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmUsuariPrograma_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmUsuariPrograma_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmUsuariPrograma_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtUsuari_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.GotFocus
		XGotFocus(Me, txtUsuari)
	End Sub
	
	Private Sub txtUsuari_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.DoubleClick
		ConsultaTaula(Me, txtUsuari)
	End Sub
	
	Private Sub txtUsuari_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.LostFocus
		If txtUsuari.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtUsuari)
	End Sub
	
	Private Sub txtUsuari_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUsuari.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtUsuari)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtUsuari)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtServei_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.GotFocus
		XGotFocus(Me, txtServei)
	End Sub
	
	Private Sub txtServei_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.DoubleClick
		ConsultaTaula(Me, txtServei)
	End Sub
	
	Private Sub txtServei_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServei.LostFocus
		If txtServei.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtServei)
	End Sub
	
	Private Sub txtServei_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtServei.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtServei)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtServei)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPrograma_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrograma.GotFocus
		XGotFocus(Me, txtPrograma)
	End Sub
	
	Private Sub txtPrograma_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrograma.DoubleClick
		ConsultaTaula(Me, txtPrograma)
	End Sub
	
	Private Sub txtPrograma_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrograma.LostFocus
		If txtPrograma.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtPrograma)
	End Sub
	
	Private Sub txtPrograma_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPrograma.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtPrograma)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtPrograma)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDataInici_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataInici.GotFocus
		XGotFocus(Me, txtDataInici)
	End Sub
	
	Private Sub txtDataInici_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataInici.LostFocus
		If txtDataInici.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtDataInici)
	End Sub
	
	Private Sub txtDataFi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataFi.GotFocus
		XGotFocus(Me, txtDataFi)
	End Sub
	
	Private Sub txtDataFi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataFi.LostFocus
		XLostFocus(Me, txtDataFi)
	End Sub
	
	Private Sub txtObservacions_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.GotFocus
		XGotFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.LostFocus
		XLostFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservacions.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtObservacions)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
