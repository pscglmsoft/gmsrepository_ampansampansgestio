<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSolicitudTransportHistoric
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtComentario As System.Windows.Forms.TextBox
	Public WithEvents Text38 As System.Windows.Forms.TextBox
	Public WithEvents txtSolicitud As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtNum As System.Windows.Forms.TextBox
	Public WithEvents cmbEstat As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents txtDataSolicitud As AxDataControl.AxGmsData
	Public WithEvents txtDataTransport As AxDataControl.AxGmsData
	Public WithEvents cmbPrioritat As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents txtSolicitant As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents txtEmpresa As System.Windows.Forms.TextBox
	Public WithEvents Text8 As System.Windows.Forms.TextBox
	Public WithEvents txtCentreCost As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents txtFeina As System.Windows.Forms.TextBox
	Public WithEvents Text10 As System.Windows.Forms.TextBox
	Public WithEvents txtTarea As System.Windows.Forms.TextBox
	Public WithEvents Text11 As System.Windows.Forms.TextBox
	Public WithEvents txtTipo As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents txtMotivoNoRealizado As System.Windows.Forms.TextBox
	Public WithEvents Text13 As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtTransportista As System.Windows.Forms.TextBox
	Public WithEvents lbl44 As System.Windows.Forms.Label
	Public WithEvents lbl38 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl10 As System.Windows.Forms.Label
	Public WithEvents lbl11 As System.Windows.Forms.Label
	Public WithEvents lbl12 As System.Windows.Forms.Label
	Public WithEvents lbl13 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSolicitudTransportHistoric))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtComentario = New System.Windows.Forms.TextBox
		Me.Text38 = New System.Windows.Forms.TextBox
		Me.txtSolicitud = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtNum = New System.Windows.Forms.TextBox
		Me.cmbEstat = New AxSSDataWidgets_B.AxSSDBCombo
		Me.txtDataSolicitud = New AxDataControl.AxGmsData
		Me.txtDataTransport = New AxDataControl.AxGmsData
		Me.cmbPrioritat = New AxSSDataWidgets_B.AxSSDBCombo
		Me.txtSolicitant = New System.Windows.Forms.TextBox
		Me.Text7 = New System.Windows.Forms.TextBox
		Me.txtEmpresa = New System.Windows.Forms.TextBox
		Me.Text8 = New System.Windows.Forms.TextBox
		Me.txtCentreCost = New System.Windows.Forms.TextBox
		Me.Text9 = New System.Windows.Forms.TextBox
		Me.txtFeina = New System.Windows.Forms.TextBox
		Me.Text10 = New System.Windows.Forms.TextBox
		Me.txtTarea = New System.Windows.Forms.TextBox
		Me.Text11 = New System.Windows.Forms.TextBox
		Me.txtTipo = New AxSSDataWidgets_B.AxSSDBCombo
		Me.txtMotivoNoRealizado = New System.Windows.Forms.TextBox
		Me.Text13 = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.txtTransportista = New System.Windows.Forms.TextBox
		Me.lbl44 = New System.Windows.Forms.Label
		Me.lbl38 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl10 = New System.Windows.Forms.Label
		Me.lbl11 = New System.Windows.Forms.Label
		Me.lbl12 = New System.Windows.Forms.Label
		Me.lbl13 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmbEstat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataSolicitud, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataTransport, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbPrioritat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtTipo, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Solicitud de transporte hist�rico"
		Me.ClientSize = New System.Drawing.Size(499, 438)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "T-SOL_TRANSPORT_HISTORIC"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmSolicitudTransportHistoric"
		Me.txtComentario.AutoSize = False
		Me.txtComentario.Size = New System.Drawing.Size(365, 45)
		Me.txtComentario.Location = New System.Drawing.Point(124, 346)
		Me.txtComentario.Maxlength = 200
		Me.txtComentario.MultiLine = True
		Me.txtComentario.TabIndex = 15
		Me.txtComentario.Tag = "15"
		Me.txtComentario.AcceptsReturn = True
		Me.txtComentario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtComentario.BackColor = System.Drawing.SystemColors.Window
		Me.txtComentario.CausesValidation = True
		Me.txtComentario.Enabled = True
		Me.txtComentario.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtComentario.HideSelection = True
		Me.txtComentario.ReadOnly = False
		Me.txtComentario.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtComentario.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtComentario.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtComentario.TabStop = True
		Me.txtComentario.Visible = True
		Me.txtComentario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtComentario.Name = "txtComentario"
		Me.Text38.AutoSize = False
		Me.Text38.BackColor = System.Drawing.Color.White
		Me.Text38.Enabled = False
		Me.Text38.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text38.Size = New System.Drawing.Size(320, 19)
		Me.Text38.Location = New System.Drawing.Point(169, 322)
		Me.Text38.TabIndex = 38
		Me.Text38.Tag = "^14"
		Me.Text38.AcceptsReturn = True
		Me.Text38.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text38.CausesValidation = True
		Me.Text38.HideSelection = True
		Me.Text38.ReadOnly = False
		Me.Text38.Maxlength = 0
		Me.Text38.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text38.MultiLine = False
		Me.Text38.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text38.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text38.TabStop = True
		Me.Text38.Visible = True
		Me.Text38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text38.Name = "Text38"
		Me.txtSolicitud.AutoSize = False
		Me.txtSolicitud.Size = New System.Drawing.Size(83, 19)
		Me.txtSolicitud.Location = New System.Drawing.Point(124, 10)
		Me.txtSolicitud.Maxlength = 8
		Me.txtSolicitud.TabIndex = 1
		Me.txtSolicitud.Tag = "*1"
		Me.txtSolicitud.AcceptsReturn = True
		Me.txtSolicitud.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSolicitud.BackColor = System.Drawing.SystemColors.Window
		Me.txtSolicitud.CausesValidation = True
		Me.txtSolicitud.Enabled = True
		Me.txtSolicitud.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSolicitud.HideSelection = True
		Me.txtSolicitud.ReadOnly = False
		Me.txtSolicitud.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSolicitud.MultiLine = False
		Me.txtSolicitud.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSolicitud.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSolicitud.TabStop = True
		Me.txtSolicitud.Visible = True
		Me.txtSolicitud.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSolicitud.Name = "txtSolicitud"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(280, 19)
		Me.Text1.Location = New System.Drawing.Point(210, 10)
		Me.Text1.TabIndex = 17
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtNum.AutoSize = False
		Me.txtNum.Size = New System.Drawing.Size(31, 19)
		Me.txtNum.Location = New System.Drawing.Point(124, 34)
		Me.txtNum.Maxlength = 3
		Me.txtNum.TabIndex = 2
		Me.txtNum.Tag = "*2"
		Me.txtNum.AcceptsReturn = True
		Me.txtNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNum.BackColor = System.Drawing.SystemColors.Window
		Me.txtNum.CausesValidation = True
		Me.txtNum.Enabled = True
		Me.txtNum.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNum.HideSelection = True
		Me.txtNum.ReadOnly = False
		Me.txtNum.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNum.MultiLine = False
		Me.txtNum.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNum.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNum.TabStop = True
		Me.txtNum.Visible = True
		Me.txtNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNum.Name = "txtNum"
		cmbEstat.OcxState = CType(resources.GetObject("cmbEstat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbEstat.Size = New System.Drawing.Size(145, 19)
		Me.cmbEstat.Location = New System.Drawing.Point(124, 58)
		Me.cmbEstat.TabIndex = 3
		Me.cmbEstat.Name = "cmbEstat"
		txtDataSolicitud.OcxState = CType(resources.GetObject("txtDataSolicitud.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataSolicitud.Size = New System.Drawing.Size(87, 19)
		Me.txtDataSolicitud.Location = New System.Drawing.Point(124, 82)
		Me.txtDataSolicitud.TabIndex = 4
		Me.txtDataSolicitud.Name = "txtDataSolicitud"
		txtDataTransport.OcxState = CType(resources.GetObject("txtDataTransport.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataTransport.Size = New System.Drawing.Size(87, 19)
		Me.txtDataTransport.Location = New System.Drawing.Point(124, 106)
		Me.txtDataTransport.TabIndex = 5
		Me.txtDataTransport.Name = "txtDataTransport"
		cmbPrioritat.OcxState = CType(resources.GetObject("cmbPrioritat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbPrioritat.Size = New System.Drawing.Size(145, 19)
		Me.cmbPrioritat.Location = New System.Drawing.Point(124, 130)
		Me.cmbPrioritat.TabIndex = 6
		Me.cmbPrioritat.Name = "cmbPrioritat"
		Me.txtSolicitant.AutoSize = False
		Me.txtSolicitant.Size = New System.Drawing.Size(104, 19)
		Me.txtSolicitant.Location = New System.Drawing.Point(124, 154)
		Me.txtSolicitant.Maxlength = 10
		Me.txtSolicitant.TabIndex = 7
		Me.txtSolicitant.Tag = "7"
		Me.txtSolicitant.AcceptsReturn = True
		Me.txtSolicitant.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSolicitant.BackColor = System.Drawing.SystemColors.Window
		Me.txtSolicitant.CausesValidation = True
		Me.txtSolicitant.Enabled = True
		Me.txtSolicitant.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSolicitant.HideSelection = True
		Me.txtSolicitant.ReadOnly = False
		Me.txtSolicitant.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSolicitant.MultiLine = False
		Me.txtSolicitant.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSolicitant.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSolicitant.TabStop = True
		Me.txtSolicitant.Visible = True
		Me.txtSolicitant.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSolicitant.Name = "txtSolicitant"
		Me.Text7.AutoSize = False
		Me.Text7.BackColor = System.Drawing.Color.White
		Me.Text7.Enabled = False
		Me.Text7.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text7.Size = New System.Drawing.Size(259, 19)
		Me.Text7.Location = New System.Drawing.Point(231, 154)
		Me.Text7.TabIndex = 24
		Me.Text7.Tag = "^7"
		Me.Text7.AcceptsReturn = True
		Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text7.CausesValidation = True
		Me.Text7.HideSelection = True
		Me.Text7.ReadOnly = False
		Me.Text7.Maxlength = 0
		Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text7.MultiLine = False
		Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text7.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text7.TabStop = True
		Me.Text7.Visible = True
		Me.Text7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text7.Name = "Text7"
		Me.txtEmpresa.AutoSize = False
		Me.txtEmpresa.Size = New System.Drawing.Size(42, 19)
		Me.txtEmpresa.Location = New System.Drawing.Point(124, 178)
		Me.txtEmpresa.Maxlength = 4
		Me.txtEmpresa.TabIndex = 8
		Me.txtEmpresa.Tag = "8"
		Me.txtEmpresa.AcceptsReturn = True
		Me.txtEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmpresa.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmpresa.CausesValidation = True
		Me.txtEmpresa.Enabled = True
		Me.txtEmpresa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmpresa.HideSelection = True
		Me.txtEmpresa.ReadOnly = False
		Me.txtEmpresa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmpresa.MultiLine = False
		Me.txtEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmpresa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmpresa.TabStop = True
		Me.txtEmpresa.Visible = True
		Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmpresa.Name = "txtEmpresa"
		Me.Text8.AutoSize = False
		Me.Text8.BackColor = System.Drawing.Color.White
		Me.Text8.Enabled = False
		Me.Text8.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text8.Size = New System.Drawing.Size(320, 19)
		Me.Text8.Location = New System.Drawing.Point(169, 178)
		Me.Text8.TabIndex = 26
		Me.Text8.Tag = "^8"
		Me.Text8.AcceptsReturn = True
		Me.Text8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text8.CausesValidation = True
		Me.Text8.HideSelection = True
		Me.Text8.ReadOnly = False
		Me.Text8.Maxlength = 0
		Me.Text8.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text8.MultiLine = False
		Me.Text8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text8.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text8.TabStop = True
		Me.Text8.Visible = True
		Me.Text8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text8.Name = "Text8"
		Me.txtCentreCost.AutoSize = False
		Me.txtCentreCost.Size = New System.Drawing.Size(42, 19)
		Me.txtCentreCost.Location = New System.Drawing.Point(124, 202)
		Me.txtCentreCost.Maxlength = 4
		Me.txtCentreCost.TabIndex = 9
		Me.txtCentreCost.Tag = "9"
		Me.txtCentreCost.AcceptsReturn = True
		Me.txtCentreCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCentreCost.BackColor = System.Drawing.SystemColors.Window
		Me.txtCentreCost.CausesValidation = True
		Me.txtCentreCost.Enabled = True
		Me.txtCentreCost.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCentreCost.HideSelection = True
		Me.txtCentreCost.ReadOnly = False
		Me.txtCentreCost.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCentreCost.MultiLine = False
		Me.txtCentreCost.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCentreCost.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCentreCost.TabStop = True
		Me.txtCentreCost.Visible = True
		Me.txtCentreCost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCentreCost.Name = "txtCentreCost"
		Me.Text9.AutoSize = False
		Me.Text9.BackColor = System.Drawing.Color.White
		Me.Text9.Enabled = False
		Me.Text9.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text9.Size = New System.Drawing.Size(320, 19)
		Me.Text9.Location = New System.Drawing.Point(169, 202)
		Me.Text9.TabIndex = 28
		Me.Text9.Tag = "^9"
		Me.Text9.AcceptsReturn = True
		Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text9.CausesValidation = True
		Me.Text9.HideSelection = True
		Me.Text9.ReadOnly = False
		Me.Text9.Maxlength = 0
		Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text9.MultiLine = False
		Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text9.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text9.TabStop = True
		Me.Text9.Visible = True
		Me.Text9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text9.Name = "Text9"
		Me.txtFeina.AutoSize = False
		Me.txtFeina.Size = New System.Drawing.Size(62, 19)
		Me.txtFeina.Location = New System.Drawing.Point(124, 226)
		Me.txtFeina.Maxlength = 6
		Me.txtFeina.TabIndex = 10
		Me.txtFeina.Tag = "10"
		Me.txtFeina.AcceptsReturn = True
		Me.txtFeina.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFeina.BackColor = System.Drawing.SystemColors.Window
		Me.txtFeina.CausesValidation = True
		Me.txtFeina.Enabled = True
		Me.txtFeina.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFeina.HideSelection = True
		Me.txtFeina.ReadOnly = False
		Me.txtFeina.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFeina.MultiLine = False
		Me.txtFeina.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFeina.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFeina.TabStop = True
		Me.txtFeina.Visible = True
		Me.txtFeina.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtFeina.Name = "txtFeina"
		Me.Text10.AutoSize = False
		Me.Text10.BackColor = System.Drawing.Color.White
		Me.Text10.Enabled = False
		Me.Text10.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text10.Size = New System.Drawing.Size(300, 19)
		Me.Text10.Location = New System.Drawing.Point(190, 226)
		Me.Text10.TabIndex = 30
		Me.Text10.Tag = "^10"
		Me.Text10.AcceptsReturn = True
		Me.Text10.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text10.CausesValidation = True
		Me.Text10.HideSelection = True
		Me.Text10.ReadOnly = False
		Me.Text10.Maxlength = 0
		Me.Text10.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text10.MultiLine = False
		Me.Text10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text10.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text10.TabStop = True
		Me.Text10.Visible = True
		Me.Text10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text10.Name = "Text10"
		Me.txtTarea.AutoSize = False
		Me.txtTarea.Size = New System.Drawing.Size(62, 19)
		Me.txtTarea.Location = New System.Drawing.Point(124, 250)
		Me.txtTarea.Maxlength = 6
		Me.txtTarea.TabIndex = 11
		Me.txtTarea.Tag = "11"
		Me.txtTarea.AcceptsReturn = True
		Me.txtTarea.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTarea.BackColor = System.Drawing.SystemColors.Window
		Me.txtTarea.CausesValidation = True
		Me.txtTarea.Enabled = True
		Me.txtTarea.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTarea.HideSelection = True
		Me.txtTarea.ReadOnly = False
		Me.txtTarea.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTarea.MultiLine = False
		Me.txtTarea.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTarea.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTarea.TabStop = True
		Me.txtTarea.Visible = True
		Me.txtTarea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTarea.Name = "txtTarea"
		Me.Text11.AutoSize = False
		Me.Text11.BackColor = System.Drawing.Color.White
		Me.Text11.Enabled = False
		Me.Text11.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text11.Size = New System.Drawing.Size(300, 19)
		Me.Text11.Location = New System.Drawing.Point(190, 250)
		Me.Text11.TabIndex = 32
		Me.Text11.Tag = "^11"
		Me.Text11.AcceptsReturn = True
		Me.Text11.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text11.CausesValidation = True
		Me.Text11.HideSelection = True
		Me.Text11.ReadOnly = False
		Me.Text11.Maxlength = 0
		Me.Text11.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text11.MultiLine = False
		Me.Text11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text11.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text11.TabStop = True
		Me.Text11.Visible = True
		Me.Text11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text11.Name = "Text11"
		txtTipo.OcxState = CType(resources.GetObject("txtTipo.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtTipo.Size = New System.Drawing.Size(145, 19)
		Me.txtTipo.Location = New System.Drawing.Point(124, 274)
		Me.txtTipo.TabIndex = 12
		Me.txtTipo.Name = "txtTipo"
		Me.txtMotivoNoRealizado.AutoSize = False
		Me.txtMotivoNoRealizado.Size = New System.Drawing.Size(21, 19)
		Me.txtMotivoNoRealizado.Location = New System.Drawing.Point(124, 298)
		Me.txtMotivoNoRealizado.Maxlength = 2
		Me.txtMotivoNoRealizado.TabIndex = 13
		Me.txtMotivoNoRealizado.Tag = "13"
		Me.txtMotivoNoRealizado.AcceptsReturn = True
		Me.txtMotivoNoRealizado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMotivoNoRealizado.BackColor = System.Drawing.SystemColors.Window
		Me.txtMotivoNoRealizado.CausesValidation = True
		Me.txtMotivoNoRealizado.Enabled = True
		Me.txtMotivoNoRealizado.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMotivoNoRealizado.HideSelection = True
		Me.txtMotivoNoRealizado.ReadOnly = False
		Me.txtMotivoNoRealizado.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMotivoNoRealizado.MultiLine = False
		Me.txtMotivoNoRealizado.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMotivoNoRealizado.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMotivoNoRealizado.TabStop = True
		Me.txtMotivoNoRealizado.Visible = True
		Me.txtMotivoNoRealizado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMotivoNoRealizado.Name = "txtMotivoNoRealizado"
		Me.Text13.AutoSize = False
		Me.Text13.BackColor = System.Drawing.Color.White
		Me.Text13.Enabled = False
		Me.Text13.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text13.Size = New System.Drawing.Size(340, 19)
		Me.Text13.Location = New System.Drawing.Point(148, 298)
		Me.Text13.TabIndex = 35
		Me.Text13.Tag = "^13"
		Me.Text13.AcceptsReturn = True
		Me.Text13.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text13.CausesValidation = True
		Me.Text13.HideSelection = True
		Me.Text13.ReadOnly = False
		Me.Text13.Maxlength = 0
		Me.Text13.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text13.MultiLine = False
		Me.Text13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text13.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text13.TabStop = True
		Me.Text13.Visible = True
		Me.Text13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text13.Name = "Text13"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(317, 408)
		Me.cmdAceptar.TabIndex = 16
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(407, 408)
		Me.cmdGuardar.TabIndex = 36
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.txtTransportista.AutoSize = False
		Me.txtTransportista.Size = New System.Drawing.Size(42, 19)
		Me.txtTransportista.Location = New System.Drawing.Point(124, 322)
		Me.txtTransportista.Maxlength = 4
		Me.txtTransportista.TabIndex = 14
		Me.txtTransportista.Tag = "14"
		Me.txtTransportista.AcceptsReturn = True
		Me.txtTransportista.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTransportista.BackColor = System.Drawing.SystemColors.Window
		Me.txtTransportista.CausesValidation = True
		Me.txtTransportista.Enabled = True
		Me.txtTransportista.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTransportista.HideSelection = True
		Me.txtTransportista.ReadOnly = False
		Me.txtTransportista.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTransportista.MultiLine = False
		Me.txtTransportista.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTransportista.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTransportista.TabStop = True
		Me.txtTransportista.Visible = True
		Me.txtTransportista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTransportista.Name = "txtTransportista"
		Me.lbl44.Text = "Comentario"
		Me.lbl44.Size = New System.Drawing.Size(111, 15)
		Me.lbl44.Location = New System.Drawing.Point(10, 350)
		Me.lbl44.TabIndex = 40
		Me.lbl44.Tag = "15"
		Me.lbl44.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl44.BackColor = System.Drawing.SystemColors.Control
		Me.lbl44.Enabled = True
		Me.lbl44.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl44.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl44.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl44.UseMnemonic = True
		Me.lbl44.Visible = True
		Me.lbl44.AutoSize = False
		Me.lbl44.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl44.Name = "lbl44"
		Me.lbl38.Text = "Transportista"
		Me.lbl38.Size = New System.Drawing.Size(111, 15)
		Me.lbl38.Location = New System.Drawing.Point(10, 326)
		Me.lbl38.TabIndex = 39
		Me.lbl38.Tag = "14"
		Me.lbl38.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl38.BackColor = System.Drawing.SystemColors.Control
		Me.lbl38.Enabled = True
		Me.lbl38.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl38.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl38.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl38.UseMnemonic = True
		Me.lbl38.Visible = True
		Me.lbl38.AutoSize = False
		Me.lbl38.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl38.Name = "lbl38"
		Me.lbl1.Text = "Solicitud"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Num"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 18
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Estado"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 19
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Fecha solicitud"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 86)
		Me.lbl4.TabIndex = 20
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Fecha transporte"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 110)
		Me.lbl5.TabIndex = 21
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Prioridad"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 134)
		Me.lbl6.TabIndex = 22
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "Solicitante"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 158)
		Me.lbl7.TabIndex = 23
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl8.Text = "Empresa"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(10, 182)
		Me.lbl8.TabIndex = 25
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl9.Text = "Centro coste"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(10, 206)
		Me.lbl9.TabIndex = 27
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl10.Text = "Orden de trabajo"
		Me.lbl10.Size = New System.Drawing.Size(111, 15)
		Me.lbl10.Location = New System.Drawing.Point(10, 230)
		Me.lbl10.TabIndex = 29
		Me.lbl10.Tag = "10"
		Me.lbl10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl10.BackColor = System.Drawing.SystemColors.Control
		Me.lbl10.Enabled = True
		Me.lbl10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl10.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl10.UseMnemonic = True
		Me.lbl10.Visible = True
		Me.lbl10.AutoSize = False
		Me.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl10.Name = "lbl10"
		Me.lbl11.Text = "Tarea"
		Me.lbl11.Size = New System.Drawing.Size(111, 15)
		Me.lbl11.Location = New System.Drawing.Point(10, 254)
		Me.lbl11.TabIndex = 31
		Me.lbl11.Tag = "11"
		Me.lbl11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl11.BackColor = System.Drawing.SystemColors.Control
		Me.lbl11.Enabled = True
		Me.lbl11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl11.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl11.UseMnemonic = True
		Me.lbl11.Visible = True
		Me.lbl11.AutoSize = False
		Me.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl11.Name = "lbl11"
		Me.lbl12.Text = "Tipo"
		Me.lbl12.Size = New System.Drawing.Size(111, 15)
		Me.lbl12.Location = New System.Drawing.Point(10, 278)
		Me.lbl12.TabIndex = 33
		Me.lbl12.Tag = "12"
		Me.lbl12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl12.BackColor = System.Drawing.SystemColors.Control
		Me.lbl12.Enabled = True
		Me.lbl12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl12.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl12.UseMnemonic = True
		Me.lbl12.Visible = True
		Me.lbl12.AutoSize = False
		Me.lbl12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl12.Name = "lbl12"
		Me.lbl13.Text = "Motivo no realizado"
		Me.lbl13.Size = New System.Drawing.Size(111, 15)
		Me.lbl13.Location = New System.Drawing.Point(10, 302)
		Me.lbl13.TabIndex = 34
		Me.lbl13.Tag = "13"
		Me.lbl13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl13.BackColor = System.Drawing.SystemColors.Control
		Me.lbl13.Enabled = True
		Me.lbl13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl13.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl13.UseMnemonic = True
		Me.lbl13.Visible = True
		Me.lbl13.AutoSize = False
		Me.lbl13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl13.Name = "lbl13"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 407)
		Me.lblLock.TabIndex = 37
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 401
		Me.Line1.Y2 = 401
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 402
		Me.Line2.Y2 = 402
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtTipo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbPrioritat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataTransport, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataSolicitud, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbEstat, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtComentario)
		Me.Controls.Add(Text38)
		Me.Controls.Add(txtSolicitud)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtNum)
		Me.Controls.Add(cmbEstat)
		Me.Controls.Add(txtDataSolicitud)
		Me.Controls.Add(txtDataTransport)
		Me.Controls.Add(cmbPrioritat)
		Me.Controls.Add(txtSolicitant)
		Me.Controls.Add(Text7)
		Me.Controls.Add(txtEmpresa)
		Me.Controls.Add(Text8)
		Me.Controls.Add(txtCentreCost)
		Me.Controls.Add(Text9)
		Me.Controls.Add(txtFeina)
		Me.Controls.Add(Text10)
		Me.Controls.Add(txtTarea)
		Me.Controls.Add(Text11)
		Me.Controls.Add(txtTipo)
		Me.Controls.Add(txtMotivoNoRealizado)
		Me.Controls.Add(Text13)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(txtTransportista)
		Me.Controls.Add(lbl44)
		Me.Controls.Add(lbl38)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(lbl10)
		Me.Controls.Add(lbl11)
		Me.Controls.Add(lbl12)
		Me.Controls.Add(lbl13)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class