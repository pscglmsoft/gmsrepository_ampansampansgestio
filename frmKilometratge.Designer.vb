<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmKilometratge
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtNumeroRegistre As System.Windows.Forms.TextBox
	Public WithEvents txtVehicle As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtData As AxDataControl.AxGmsData
	Public WithEvents txtPersona As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtCentreTreball As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtKilometresInici As AxDataControl.AxGmsImports
	Public WithEvents txtKilometresFi As AxDataControl.AxGmsImports
	Public WithEvents txtDiferencia As AxDataControl.AxGmsImports
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmKilometratge))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtNumeroRegistre = New System.Windows.Forms.TextBox
		Me.txtVehicle = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtData = New AxDataControl.AxGmsData
		Me.txtPersona = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtCentreTreball = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtKilometresInici = New AxDataControl.AxGmsImports
		Me.txtKilometresFi = New AxDataControl.AxGmsImports
		Me.txtDiferencia = New AxDataControl.AxGmsImports
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtKilometresInici, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtKilometresFi, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDiferencia, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Kilometratge"
		Me.ClientSize = New System.Drawing.Size(497, 263)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "C-KILOMETRATGE"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmKilometratge"
		Me.txtNumeroRegistre.AutoSize = False
		Me.txtNumeroRegistre.Size = New System.Drawing.Size(50, 19)
		Me.txtNumeroRegistre.Location = New System.Drawing.Point(124, 58)
		Me.txtNumeroRegistre.Maxlength = 12
		Me.txtNumeroRegistre.TabIndex = 3
		Me.txtNumeroRegistre.Tag = "*3"
		Me.txtNumeroRegistre.AcceptsReturn = True
		Me.txtNumeroRegistre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNumeroRegistre.BackColor = System.Drawing.SystemColors.Window
		Me.txtNumeroRegistre.CausesValidation = True
		Me.txtNumeroRegistre.Enabled = True
		Me.txtNumeroRegistre.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNumeroRegistre.HideSelection = True
		Me.txtNumeroRegistre.ReadOnly = False
		Me.txtNumeroRegistre.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNumeroRegistre.MultiLine = False
		Me.txtNumeroRegistre.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNumeroRegistre.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNumeroRegistre.TabStop = True
		Me.txtNumeroRegistre.Visible = True
		Me.txtNumeroRegistre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNumeroRegistre.Name = "txtNumeroRegistre"
		Me.txtVehicle.AutoSize = False
		Me.txtVehicle.Size = New System.Drawing.Size(69, 19)
		Me.txtVehicle.Location = New System.Drawing.Point(124, 10)
		Me.txtVehicle.Maxlength = 10
		Me.txtVehicle.TabIndex = 1
		Me.txtVehicle.Tag = "*1"
		Me.txtVehicle.AcceptsReturn = True
		Me.txtVehicle.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtVehicle.BackColor = System.Drawing.SystemColors.Window
		Me.txtVehicle.CausesValidation = True
		Me.txtVehicle.Enabled = True
		Me.txtVehicle.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtVehicle.HideSelection = True
		Me.txtVehicle.ReadOnly = False
		Me.txtVehicle.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtVehicle.MultiLine = False
		Me.txtVehicle.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtVehicle.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtVehicle.TabStop = True
		Me.txtVehicle.Visible = True
		Me.txtVehicle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtVehicle.Name = "txtVehicle"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(294, 19)
		Me.Text1.Location = New System.Drawing.Point(195, 10)
		Me.Text1.TabIndex = 10
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		txtData.OcxState = CType(resources.GetObject("txtData.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtData.Size = New System.Drawing.Size(87, 19)
		Me.txtData.Location = New System.Drawing.Point(124, 34)
		Me.txtData.TabIndex = 2
		Me.txtData.Name = "txtData"
		Me.txtPersona.AutoSize = False
		Me.txtPersona.Size = New System.Drawing.Size(104, 19)
		Me.txtPersona.Location = New System.Drawing.Point(124, 82)
		Me.txtPersona.Maxlength = 10
		Me.txtPersona.TabIndex = 4
		Me.txtPersona.Tag = "4"
		Me.txtPersona.AcceptsReturn = True
		Me.txtPersona.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPersona.BackColor = System.Drawing.SystemColors.Window
		Me.txtPersona.CausesValidation = True
		Me.txtPersona.Enabled = True
		Me.txtPersona.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPersona.HideSelection = True
		Me.txtPersona.ReadOnly = False
		Me.txtPersona.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPersona.MultiLine = False
		Me.txtPersona.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPersona.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPersona.TabStop = True
		Me.txtPersona.Visible = True
		Me.txtPersona.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPersona.Name = "txtPersona"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(259, 19)
		Me.Text3.Location = New System.Drawing.Point(231, 82)
		Me.Text3.TabIndex = 13
		Me.Text3.Tag = "^4"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtCentreTreball.AutoSize = False
		Me.txtCentreTreball.Size = New System.Drawing.Size(42, 19)
		Me.txtCentreTreball.Location = New System.Drawing.Point(124, 106)
		Me.txtCentreTreball.Maxlength = 4
		Me.txtCentreTreball.TabIndex = 5
		Me.txtCentreTreball.Tag = "5"
		Me.txtCentreTreball.AcceptsReturn = True
		Me.txtCentreTreball.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCentreTreball.BackColor = System.Drawing.SystemColors.Window
		Me.txtCentreTreball.CausesValidation = True
		Me.txtCentreTreball.Enabled = True
		Me.txtCentreTreball.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCentreTreball.HideSelection = True
		Me.txtCentreTreball.ReadOnly = False
		Me.txtCentreTreball.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCentreTreball.MultiLine = False
		Me.txtCentreTreball.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCentreTreball.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCentreTreball.TabStop = True
		Me.txtCentreTreball.Visible = True
		Me.txtCentreTreball.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCentreTreball.Name = "txtCentreTreball"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(259, 19)
		Me.Text4.Location = New System.Drawing.Point(169, 106)
		Me.Text4.TabIndex = 15
		Me.Text4.Tag = "^5"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		txtKilometresInici.OcxState = CType(resources.GetObject("txtKilometresInici.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtKilometresInici.Size = New System.Drawing.Size(59, 19)
		Me.txtKilometresInici.Location = New System.Drawing.Point(124, 130)
		Me.txtKilometresInici.TabIndex = 6
		Me.txtKilometresInici.Name = "txtKilometresInici"
		txtKilometresFi.OcxState = CType(resources.GetObject("txtKilometresFi.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtKilometresFi.Size = New System.Drawing.Size(59, 19)
		Me.txtKilometresFi.Location = New System.Drawing.Point(124, 154)
		Me.txtKilometresFi.TabIndex = 7
		Me.txtKilometresFi.Name = "txtKilometresFi"
		txtDiferencia.OcxState = CType(resources.GetObject("txtDiferencia.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDiferencia.Size = New System.Drawing.Size(59, 19)
		Me.txtDiferencia.Location = New System.Drawing.Point(124, 178)
		Me.txtDiferencia.TabIndex = 8
		Me.txtDiferencia.Name = "txtDiferencia"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(319, 218)
		Me.cmdAceptar.TabIndex = 9
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(409, 218)
		Me.cmdGuardar.TabIndex = 19
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.Label1.Text = "N�mero registro"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 62)
		Me.Label1.TabIndex = 21
		Me.Label1.Tag = "3"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Vehicle"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Data"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 11
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Persona"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 86)
		Me.lbl3.TabIndex = 12
		Me.lbl3.Tag = "4"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Centre treball"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 110)
		Me.lbl4.TabIndex = 14
		Me.lbl4.Tag = "5"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Kil�metres inici"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 134)
		Me.lbl5.TabIndex = 16
		Me.lbl5.Tag = "6"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Kil�metres fi"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 158)
		Me.lbl6.TabIndex = 17
		Me.lbl6.Tag = "7"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "Difer�ncia"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 182)
		Me.lbl7.TabIndex = 18
		Me.lbl7.Tag = "8"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 217)
		Me.lblLock.TabIndex = 20
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 211
		Me.Line1.Y2 = 211
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 212
		Me.Line2.Y2 = 212
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDiferencia, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtKilometresFi, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtKilometresInici, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtNumeroRegistre)
		Me.Controls.Add(txtVehicle)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtData)
		Me.Controls.Add(txtPersona)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtCentreTreball)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtKilometresInici)
		Me.Controls.Add(txtKilometresFi)
		Me.Controls.Add(txtDiferencia)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class