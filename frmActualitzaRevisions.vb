Option Strict Off
Option Explicit On
Friend Class frmActualitzaRevisions
	Inherits FormParent
	
	Private Sub cmdActualitza_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdActualitza.ClickEvent
		CacheXecute("D REVIFUTURES^INDICADORSBI")
		xMsgBox("Revisions actualitzades correctament", MsgBoxStyle.Information, Me.Text)
		Me.Unload()
	End Sub
	
	Private Sub frmActualitzaRevisions_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		cmdActualitza.Picture = SetIcon("Genera", 24)
	End Sub
End Class
