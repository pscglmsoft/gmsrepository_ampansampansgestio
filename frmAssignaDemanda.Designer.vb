<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAssignaDemanda
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtProblema As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents txtPersonaQueGestiona As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtCentreQueGestiona As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAssignaDemanda))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtProblema = New System.Windows.Forms.TextBox
		Me.Text7 = New System.Windows.Forms.TextBox
		Me.txtPersonaQueGestiona = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtCentreQueGestiona = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Assigna demanda"
		Me.ClientSize = New System.Drawing.Size(605, 181)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmAssignaDemanda"
		Me.txtProblema.AutoSize = False
		Me.txtProblema.Size = New System.Drawing.Size(459, 67)
		Me.txtProblema.Location = New System.Drawing.Point(128, 62)
		Me.txtProblema.Maxlength = 250
		Me.txtProblema.TabIndex = 3
		Me.txtProblema.AcceptsReturn = True
		Me.txtProblema.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtProblema.BackColor = System.Drawing.SystemColors.Window
		Me.txtProblema.CausesValidation = True
		Me.txtProblema.Enabled = True
		Me.txtProblema.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtProblema.HideSelection = True
		Me.txtProblema.ReadOnly = False
		Me.txtProblema.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtProblema.MultiLine = False
		Me.txtProblema.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtProblema.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtProblema.TabStop = True
		Me.txtProblema.Visible = True
		Me.txtProblema.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtProblema.Name = "txtProblema"
		Me.Text7.AutoSize = False
		Me.Text7.BackColor = System.Drawing.Color.White
		Me.Text7.Enabled = False
		Me.Text7.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text7.Size = New System.Drawing.Size(362, 19)
		Me.Text7.Location = New System.Drawing.Point(224, 38)
		Me.Text7.TabIndex = 5
		Me.Text7.Tag = "^2"
		Me.Text7.AcceptsReturn = True
		Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text7.CausesValidation = True
		Me.Text7.HideSelection = True
		Me.Text7.ReadOnly = False
		Me.Text7.Maxlength = 0
		Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text7.MultiLine = False
		Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text7.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text7.TabStop = True
		Me.Text7.Visible = True
		Me.Text7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text7.Name = "Text7"
		Me.txtPersonaQueGestiona.AutoSize = False
		Me.txtPersonaQueGestiona.Size = New System.Drawing.Size(93, 19)
		Me.txtPersonaQueGestiona.Location = New System.Drawing.Point(128, 38)
		Me.txtPersonaQueGestiona.Maxlength = 9
		Me.txtPersonaQueGestiona.TabIndex = 2
		Me.txtPersonaQueGestiona.Tag = "2####I-PERSONAL#1#1"
		Me.txtPersonaQueGestiona.AcceptsReturn = True
		Me.txtPersonaQueGestiona.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPersonaQueGestiona.BackColor = System.Drawing.SystemColors.Window
		Me.txtPersonaQueGestiona.CausesValidation = True
		Me.txtPersonaQueGestiona.Enabled = True
		Me.txtPersonaQueGestiona.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPersonaQueGestiona.HideSelection = True
		Me.txtPersonaQueGestiona.ReadOnly = False
		Me.txtPersonaQueGestiona.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPersonaQueGestiona.MultiLine = False
		Me.txtPersonaQueGestiona.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPersonaQueGestiona.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPersonaQueGestiona.TabStop = True
		Me.txtPersonaQueGestiona.Visible = True
		Me.txtPersonaQueGestiona.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPersonaQueGestiona.Name = "txtPersonaQueGestiona"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(414, 19)
		Me.Text6.Location = New System.Drawing.Point(173, 14)
		Me.Text6.TabIndex = 1
		Me.Text6.Tag = "^1"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtCentreQueGestiona.AutoSize = False
		Me.txtCentreQueGestiona.Size = New System.Drawing.Size(42, 19)
		Me.txtCentreQueGestiona.Location = New System.Drawing.Point(128, 14)
		Me.txtCentreQueGestiona.Maxlength = 4
		Me.txtCentreQueGestiona.TabIndex = 0
		Me.txtCentreQueGestiona.Tag = "1####DEM-CENTRES_DEMANDES#1#1"
		Me.txtCentreQueGestiona.AcceptsReturn = True
		Me.txtCentreQueGestiona.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCentreQueGestiona.BackColor = System.Drawing.SystemColors.Window
		Me.txtCentreQueGestiona.CausesValidation = True
		Me.txtCentreQueGestiona.Enabled = True
		Me.txtCentreQueGestiona.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCentreQueGestiona.HideSelection = True
		Me.txtCentreQueGestiona.ReadOnly = False
		Me.txtCentreQueGestiona.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCentreQueGestiona.MultiLine = False
		Me.txtCentreQueGestiona.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCentreQueGestiona.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCentreQueGestiona.TabStop = True
		Me.txtCentreQueGestiona.Visible = True
		Me.txtCentreQueGestiona.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCentreQueGestiona.Name = "txtCentreQueGestiona"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(504, 142)
		Me.cmdAceptar.TabIndex = 4
		Me.cmdAceptar.Name = "cmdAceptar"
		Me.lbl8.Text = "Observacions"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(14, 66)
		Me.lbl8.TabIndex = 8
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl7.Text = "Persona que gestiona"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(14, 42)
		Me.lbl7.TabIndex = 7
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl6.Text = "Centre que gestiona"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(14, 18)
		Me.lbl6.TabIndex = 6
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.Controls.Add(txtProblema)
		Me.Controls.Add(Text7)
		Me.Controls.Add(txtPersonaQueGestiona)
		Me.Controls.Add(Text6)
		Me.Controls.Add(txtCentreQueGestiona)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl6)
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class