<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmConsultaAccions
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtCentreTreball As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtResponsable As System.Windows.Forms.TextBox
	Public WithEvents rbObjectius As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents rbAccions As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents GroupBox1 As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents cmbEstadoAccion As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents GrdAccions As AxFlexCell.AxGrid
	Public WithEvents cmdExit As AxXtremeSuiteControls.AxPushButton
	Public WithEvents ChBJardineria As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBImpremta As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBManipulats As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBNeteja As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBIncorpora As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBManteniment As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBGarden As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBCanonge As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBVi As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBDonant As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBDonacioPuntual As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBEsponsor As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBVoluntariat As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents GrupInteres As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents ChBDesestimat As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBNoAssolit As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBAssolit As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBEnCurs As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents ChBPendent As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents GroupBoxObjectius As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents ChBTotesLesAccions As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmConsultaAccions))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtCentreTreball = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtResponsable = New System.Windows.Forms.TextBox
		Me.GroupBox1 = New AxXtremeSuiteControls.AxGroupBox
		Me.rbObjectius = New AxXtremeSuiteControls.AxRadioButton
		Me.rbAccions = New AxXtremeSuiteControls.AxRadioButton
		Me.cmbEstadoAccion = New AxSSDataWidgets_B.AxSSDBCombo
		Me.GrdAccions = New AxFlexCell.AxGrid
		Me.cmdExit = New AxXtremeSuiteControls.AxPushButton
		Me.GrupInteres = New AxXtremeSuiteControls.AxGroupBox
		Me.ChBJardineria = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBImpremta = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBManipulats = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBNeteja = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBIncorpora = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBManteniment = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBGarden = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBCanonge = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBVi = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBDonant = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBDonacioPuntual = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBEsponsor = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBVoluntariat = New AxXtremeSuiteControls.AxCheckBox
		Me.GroupBoxObjectius = New AxXtremeSuiteControls.AxGroupBox
		Me.ChBDesestimat = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBNoAssolit = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBAssolit = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBEnCurs = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBPendent = New AxXtremeSuiteControls.AxCheckBox
		Me.ChBTotesLesAccions = New AxXtremeSuiteControls.AxCheckBox
		Me.lbl8 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.GroupBox1.SuspendLayout()
		Me.GrupInteres.SuspendLayout()
		Me.GroupBoxObjectius.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.rbObjectius, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.rbAccions, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbEstadoAccion, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdAccions, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdExit, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBJardineria, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBImpremta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBManipulats, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBNeteja, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBIncorpora, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBManteniment, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBGarden, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBCanonge, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBVi, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBDonant, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBDonacioPuntual, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBEsponsor, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBVoluntariat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrupInteres, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBDesestimat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBNoAssolit, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBAssolit, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBEnCurs, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBPendent, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GroupBoxObjectius, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ChBTotesLesAccions, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Consulta Accions / Objectius"
		Me.ClientSize = New System.Drawing.Size(1004, 541)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmConsultaAccions"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(308, 19)
		Me.Text2.Location = New System.Drawing.Point(193, 114)
		Me.Text2.TabIndex = 8
		Me.Text2.Tag = "^2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtCentreTreball.AutoSize = False
		Me.txtCentreTreball.Size = New System.Drawing.Size(54, 19)
		Me.txtCentreTreball.Location = New System.Drawing.Point(134, 114)
		Me.txtCentreTreball.Maxlength = 15
		Me.txtCentreTreball.TabIndex = 4
		Me.txtCentreTreball.Tag = "2####RH-CENTRESTREBALL#2"
		Me.txtCentreTreball.AcceptsReturn = True
		Me.txtCentreTreball.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCentreTreball.BackColor = System.Drawing.SystemColors.Window
		Me.txtCentreTreball.CausesValidation = True
		Me.txtCentreTreball.Enabled = True
		Me.txtCentreTreball.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCentreTreball.HideSelection = True
		Me.txtCentreTreball.ReadOnly = False
		Me.txtCentreTreball.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCentreTreball.MultiLine = False
		Me.txtCentreTreball.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCentreTreball.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCentreTreball.TabStop = True
		Me.txtCentreTreball.Visible = True
		Me.txtCentreTreball.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCentreTreball.Name = "txtCentreTreball"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(280, 19)
		Me.Text4.Location = New System.Drawing.Point(221, 90)
		Me.Text4.TabIndex = 6
		Me.Text4.Tag = "^1"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtResponsable.AutoSize = False
		Me.txtResponsable.Size = New System.Drawing.Size(82, 19)
		Me.txtResponsable.Location = New System.Drawing.Point(134, 90)
		Me.txtResponsable.Maxlength = 15
		Me.txtResponsable.TabIndex = 3
		Me.txtResponsable.Tag = "1####I-PERSONAL#1"
		Me.txtResponsable.AcceptsReturn = True
		Me.txtResponsable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtResponsable.BackColor = System.Drawing.SystemColors.Window
		Me.txtResponsable.CausesValidation = True
		Me.txtResponsable.Enabled = True
		Me.txtResponsable.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtResponsable.HideSelection = True
		Me.txtResponsable.ReadOnly = False
		Me.txtResponsable.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtResponsable.MultiLine = False
		Me.txtResponsable.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtResponsable.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtResponsable.TabStop = True
		Me.txtResponsable.Visible = True
		Me.txtResponsable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtResponsable.Name = "txtResponsable"
		GroupBox1.OcxState = CType(resources.GetObject("GroupBox1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GroupBox1.Size = New System.Drawing.Size(281, 49)
		Me.GroupBox1.Location = New System.Drawing.Point(20, 24)
		Me.GroupBox1.TabIndex = 0
		Me.GroupBox1.Name = "GroupBox1"
		rbObjectius.OcxState = CType(resources.GetObject("rbObjectius.OcxState"), System.Windows.Forms.AxHost.State)
		Me.rbObjectius.Size = New System.Drawing.Size(69, 27)
		Me.rbObjectius.Location = New System.Drawing.Point(180, 14)
		Me.rbObjectius.TabIndex = 2
		Me.rbObjectius.Name = "rbObjectius"
		rbAccions.OcxState = CType(resources.GetObject("rbAccions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.rbAccions.Size = New System.Drawing.Size(109, 19)
		Me.rbAccions.Location = New System.Drawing.Point(36, 16)
		Me.rbAccions.TabIndex = 1
		Me.rbAccions.Name = "rbAccions"
		cmbEstadoAccion.OcxState = CType(resources.GetObject("cmbEstadoAccion.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbEstadoAccion.Size = New System.Drawing.Size(145, 19)
		Me.cmbEstadoAccion.Location = New System.Drawing.Point(134, 140)
		Me.cmbEstadoAccion.TabIndex = 5
		Me.cmbEstadoAccion.Name = "cmbEstadoAccion"
		GrdAccions.OcxState = CType(resources.GetObject("GrdAccions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdAccions.Size = New System.Drawing.Size(968, 327)
		Me.GrdAccions.Location = New System.Drawing.Point(18, 176)
		Me.GrdAccions.TabIndex = 11
		Me.GrdAccions.Name = "GrdAccions"
		cmdExit.OcxState = CType(resources.GetObject("cmdExit.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdExit.Size = New System.Drawing.Size(83, 27)
		Me.cmdExit.Location = New System.Drawing.Point(904, 510)
		Me.cmdExit.TabIndex = 12
		Me.cmdExit.Name = "cmdExit"
		GrupInteres.OcxState = CType(resources.GetObject("GrupInteres.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrupInteres.Size = New System.Drawing.Size(229, 161)
		Me.GrupInteres.Location = New System.Drawing.Point(508, 8)
		Me.GrupInteres.TabIndex = 19
		Me.GrupInteres.Name = "GrupInteres"
		ChBJardineria.OcxState = CType(resources.GetObject("ChBJardineria.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBJardineria.Size = New System.Drawing.Size(65, 15)
		Me.ChBJardineria.Location = New System.Drawing.Point(10, 98)
		Me.ChBJardineria.TabIndex = 20
		Me.ChBJardineria.Name = "ChBJardineria"
		ChBImpremta.OcxState = CType(resources.GetObject("ChBImpremta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBImpremta.Size = New System.Drawing.Size(63, 13)
		Me.ChBImpremta.Location = New System.Drawing.Point(10, 80)
		Me.ChBImpremta.TabIndex = 21
		Me.ChBImpremta.Name = "ChBImpremta"
		ChBManipulats.OcxState = CType(resources.GetObject("ChBManipulats.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBManipulats.Size = New System.Drawing.Size(67, 17)
		Me.ChBManipulats.Location = New System.Drawing.Point(10, 58)
		Me.ChBManipulats.TabIndex = 22
		Me.ChBManipulats.Name = "ChBManipulats"
		ChBNeteja.OcxState = CType(resources.GetObject("ChBNeteja.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBNeteja.Size = New System.Drawing.Size(51, 17)
		Me.ChBNeteja.Location = New System.Drawing.Point(10, 40)
		Me.ChBNeteja.TabIndex = 23
		Me.ChBNeteja.Name = "ChBNeteja"
		ChBIncorpora.OcxState = CType(resources.GetObject("ChBIncorpora.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBIncorpora.Size = New System.Drawing.Size(61, 15)
		Me.ChBIncorpora.Location = New System.Drawing.Point(10, 20)
		Me.ChBIncorpora.TabIndex = 24
		Me.ChBIncorpora.Name = "ChBIncorpora"
		ChBManteniment.OcxState = CType(resources.GetObject("ChBManteniment.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBManteniment.Size = New System.Drawing.Size(77, 15)
		Me.ChBManteniment.Location = New System.Drawing.Point(10, 116)
		Me.ChBManteniment.TabIndex = 25
		Me.ChBManteniment.Name = "ChBManteniment"
		ChBGarden.OcxState = CType(resources.GetObject("ChBGarden.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBGarden.Size = New System.Drawing.Size(51, 17)
		Me.ChBGarden.Location = New System.Drawing.Point(10, 134)
		Me.ChBGarden.TabIndex = 26
		Me.ChBGarden.Name = "ChBGarden"
		ChBCanonge.OcxState = CType(resources.GetObject("ChBCanonge.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBCanonge.Size = New System.Drawing.Size(61, 15)
		Me.ChBCanonge.Location = New System.Drawing.Point(116, 20)
		Me.ChBCanonge.TabIndex = 27
		Me.ChBCanonge.Name = "ChBCanonge"
		ChBVi.OcxState = CType(resources.GetObject("ChBVi.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBVi.Size = New System.Drawing.Size(31, 15)
		Me.ChBVi.Location = New System.Drawing.Point(116, 40)
		Me.ChBVi.TabIndex = 28
		Me.ChBVi.Name = "ChBVi"
		ChBDonant.OcxState = CType(resources.GetObject("ChBDonant.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBDonant.Size = New System.Drawing.Size(85, 15)
		Me.ChBDonant.Location = New System.Drawing.Point(116, 58)
		Me.ChBDonant.TabIndex = 29
		Me.ChBDonant.Name = "ChBDonant"
		ChBDonacioPuntual.OcxState = CType(resources.GetObject("ChBDonacioPuntual.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBDonacioPuntual.Size = New System.Drawing.Size(97, 15)
		Me.ChBDonacioPuntual.Location = New System.Drawing.Point(116, 78)
		Me.ChBDonacioPuntual.TabIndex = 30
		Me.ChBDonacioPuntual.Name = "ChBDonacioPuntual"
		ChBEsponsor.OcxState = CType(resources.GetObject("ChBEsponsor.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBEsponsor.Size = New System.Drawing.Size(61, 15)
		Me.ChBEsponsor.Location = New System.Drawing.Point(116, 98)
		Me.ChBEsponsor.TabIndex = 31
		Me.ChBEsponsor.Name = "ChBEsponsor"
		ChBVoluntariat.OcxState = CType(resources.GetObject("ChBVoluntariat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBVoluntariat.Size = New System.Drawing.Size(101, 15)
		Me.ChBVoluntariat.Location = New System.Drawing.Point(116, 118)
		Me.ChBVoluntariat.TabIndex = 32
		Me.ChBVoluntariat.Name = "ChBVoluntariat"
		GroupBoxObjectius.OcxState = CType(resources.GetObject("GroupBoxObjectius.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GroupBoxObjectius.Size = New System.Drawing.Size(415, 55)
		Me.GroupBoxObjectius.Location = New System.Drawing.Point(508, 88)
		Me.GroupBoxObjectius.TabIndex = 13
		Me.GroupBoxObjectius.Visible = False
		Me.GroupBoxObjectius.Name = "GroupBoxObjectius"
		ChBDesestimat.OcxState = CType(resources.GetObject("ChBDesestimat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBDesestimat.Size = New System.Drawing.Size(81, 15)
		Me.ChBDesestimat.Location = New System.Drawing.Point(322, 28)
		Me.ChBDesestimat.TabIndex = 18
		Me.ChBDesestimat.Name = "ChBDesestimat"
		ChBNoAssolit.OcxState = CType(resources.GetObject("ChBNoAssolit.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBNoAssolit.Size = New System.Drawing.Size(73, 13)
		Me.ChBNoAssolit.Location = New System.Drawing.Point(234, 30)
		Me.ChBNoAssolit.TabIndex = 17
		Me.ChBNoAssolit.Name = "ChBNoAssolit"
		ChBAssolit.OcxState = CType(resources.GetObject("ChBAssolit.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBAssolit.Size = New System.Drawing.Size(57, 17)
		Me.ChBAssolit.Location = New System.Drawing.Point(164, 28)
		Me.ChBAssolit.TabIndex = 16
		Me.ChBAssolit.Name = "ChBAssolit"
		ChBEnCurs.OcxState = CType(resources.GetObject("ChBEnCurs.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBEnCurs.Size = New System.Drawing.Size(67, 17)
		Me.ChBEnCurs.Location = New System.Drawing.Point(88, 28)
		Me.ChBEnCurs.TabIndex = 15
		Me.ChBEnCurs.Name = "ChBEnCurs"
		ChBPendent.OcxState = CType(resources.GetObject("ChBPendent.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBPendent.Size = New System.Drawing.Size(73, 15)
		Me.ChBPendent.Location = New System.Drawing.Point(10, 28)
		Me.ChBPendent.TabIndex = 14
		Me.ChBPendent.Name = "ChBPendent"
		ChBTotesLesAccions.OcxState = CType(resources.GetObject("ChBTotesLesAccions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.ChBTotesLesAccions.Size = New System.Drawing.Size(113, 15)
		Me.ChBTotesLesAccions.Location = New System.Drawing.Point(394, 8)
		Me.ChBTotesLesAccions.TabIndex = 33
		Me.ChBTotesLesAccions.Name = "ChBTotesLesAccions"
		Me.lbl8.Text = "Estat Acci�"
		Me.lbl8.Size = New System.Drawing.Size(81, 15)
		Me.lbl8.Location = New System.Drawing.Point(20, 142)
		Me.lbl8.TabIndex = 10
		Me.lbl8.Tag = "3"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.Label1.Text = "Centre Treball"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(20, 118)
		Me.Label1.TabIndex = 9
		Me.Label1.Tag = "2"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl4.Text = "Responsable"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(20, 94)
		Me.lbl4.TabIndex = 7
		Me.lbl4.Tag = "1"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		CType(Me.ChBTotesLesAccions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GroupBoxObjectius, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBPendent, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBEnCurs, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBAssolit, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBNoAssolit, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBDesestimat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrupInteres, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBVoluntariat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBEsponsor, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBDonacioPuntual, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBDonant, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBVi, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBCanonge, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBGarden, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBManteniment, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBIncorpora, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBNeteja, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBManipulats, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBImpremta, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ChBJardineria, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdExit, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdAccions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbEstadoAccion, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.rbAccions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.rbObjectius, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtCentreTreball)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtResponsable)
		Me.Controls.Add(GroupBox1)
		Me.Controls.Add(cmbEstadoAccion)
		Me.Controls.Add(GrdAccions)
		Me.Controls.Add(cmdExit)
		Me.Controls.Add(GrupInteres)
		Me.Controls.Add(GroupBoxObjectius)
		Me.Controls.Add(ChBTotesLesAccions)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl4)
		Me.GroupBox1.Controls.Add(rbObjectius)
		Me.GroupBox1.Controls.Add(rbAccions)
		Me.GrupInteres.Controls.Add(ChBJardineria)
		Me.GrupInteres.Controls.Add(ChBImpremta)
		Me.GrupInteres.Controls.Add(ChBManipulats)
		Me.GrupInteres.Controls.Add(ChBNeteja)
		Me.GrupInteres.Controls.Add(ChBIncorpora)
		Me.GrupInteres.Controls.Add(ChBManteniment)
		Me.GrupInteres.Controls.Add(ChBGarden)
		Me.GrupInteres.Controls.Add(ChBCanonge)
		Me.GrupInteres.Controls.Add(ChBVi)
		Me.GrupInteres.Controls.Add(ChBDonant)
		Me.GrupInteres.Controls.Add(ChBDonacioPuntual)
		Me.GrupInteres.Controls.Add(ChBEsponsor)
		Me.GrupInteres.Controls.Add(ChBVoluntariat)
		Me.GroupBoxObjectius.Controls.Add(ChBDesestimat)
		Me.GroupBoxObjectius.Controls.Add(ChBNoAssolit)
		Me.GroupBoxObjectius.Controls.Add(ChBAssolit)
		Me.GroupBoxObjectius.Controls.Add(ChBEnCurs)
		Me.GroupBoxObjectius.Controls.Add(ChBPendent)
		Me.GroupBox1.ResumeLayout(False)
		Me.GrupInteres.ResumeLayout(False)
		Me.GroupBoxObjectius.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class