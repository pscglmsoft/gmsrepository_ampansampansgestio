Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmCampanyes
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		CarregaAccions()
	End Sub
	
	
	Public Overrides Sub FGetReg()
		CarregaAccions()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	'UPGRADE_WARNING: Form evento frmCampanyes.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmCampanyes_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaAccions()
	End Sub
	
	Private Sub frmCampanyes_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmCampanyes_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmCampanyes_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmCampanyes_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	Private Sub GrdCampAccions_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdCampAccions.DoubleClick
		If GrdCampAccions.ActiveCell.Row = 0 Then Exit Sub
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, |, 2). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmAccions, "frmAccions", Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, "|", 1) & S & Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, "|", 3))
	End Sub
	
	Private Sub GrdCampAccions_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdCampAccions.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtCodigo.Text = "" Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdCampAccions", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nueva Acci�n",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmAccions", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar Acci�n",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdCampAccions.Rows > 1 And PermisConsulta("frmAccions", e_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar Acci�n",  ,  , XPIcon("Delete"),  ,  ,  ,  , "B")
				.MenuItems.Item("B").Enabled = GrdCampAccions.Rows > 1 And PermisConsulta("frmAccions", e_Permisos.Baixes)
			End With
		End With
		XpExecutaMenu(Me, "GrdCampAccions", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub txtCodigo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.GotFocus
		XGotFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.DoubleClick
		ConsultaTaula(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.LostFocus
		If txtCodigo.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodigo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcion.GotFocus
		XGotFocus(Me, txtDescripcion)
	End Sub
	
	Private Sub txtDescripcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcion.LostFocus
		XLostFocus(Me, txtDescripcion)
	End Sub
	
	Private Sub txtDescripcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtResponsable_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.DoubleClick
		ConsultaTaula(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.GotFocus
		XGotFocus(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.LostFocus
		XLostFocus(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtResponsable.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipoEvento_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoEvento.GotFocus
		XGotFocus(Me, txtTipoEvento)
	End Sub
	
	Private Sub txtTipoEvento_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoEvento.DoubleClick
		ConsultaTaula(Me, txtTipoEvento)
	End Sub
	
	Private Sub txtTipoEvento_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoEvento.LostFocus
		XLostFocus(Me, txtTipoEvento)
	End Sub
	
	Private Sub txtTipoEvento_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipoEvento.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaInicio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaInicio.GotFocus
		XGotFocus(Me, txtFechaInicio)
	End Sub
	
	Private Sub txtFechaInicio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaInicio.LostFocus
		XLostFocus(Me, txtFechaInicio)
	End Sub
	
	Private Sub txtFechaFin_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaFin.GotFocus
		XGotFocus(Me, txtFechaFin)
		If txtFechaFin.Text = "" Then txtFechaFin.Text = txtFechaInicio.Text
	End Sub
	
	Private Sub txtFechaFin_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaFin.LostFocus
		XLostFocus(Me, txtFechaFin)
	End Sub
	
	Private Sub txtObservaciones_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservaciones.GotFocus
		XGotFocus(Me, txtObservaciones)
	End Sub
	
	Private Sub txtObservaciones_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservaciones.LostFocus
		XLostFocus(Me, txtObservaciones)
	End Sub
	
	Private Sub txtObservaciones_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservaciones.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub CarregaAccions()
		CarregaFGrid(GrdCampAccions, "CGACCICAMP^CRM", txtCodigo.Text)
	End Sub
	
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Select Case NomMenu
			Case "GrdCampAccions"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "N"
						ObreFormulari(frmAccions, "frmAccions")
						frmAccions.txtCampana.Text = txtTipoEvento.Text
						frmAccions.txtCampana.Enabled = False
						DisplayDescripcio(frmAccions, (frmAccions.txtCampana))
					Case "E"
						If GrdCampAccions.ActiveCell.Row = 0 Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, |, 2). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmAccions, "frmAccions", Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, "|", 1) & S & Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, "|", 3))
					Case "B"
						If GrdCampAccions.ActiveCell.Row = 0 Then Exit Sub
						
						A = GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 2).Text
						If A = "" Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, |, 2). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmAccions, "frmAccions", Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, "|", 1) & S & Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdCampAccions.Cell(GrdCampAccions.ActiveCell.Row, 1).Text, "|", 3))
						LlibAplicacio.DeleteReg(frmAccions   )
						frmAccions.Unload()
				End Select
		End Select
	End Sub
End Class
