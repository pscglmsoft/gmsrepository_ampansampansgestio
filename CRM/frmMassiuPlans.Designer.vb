<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMassiuPlans
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtContacto As System.Windows.Forms.TextBox
	Public WithEvents txtEntidad As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtSucursal As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtResponsable As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcionObjetivo As System.Windows.Forms.TextBox
	Public WithEvents txtObjectiu As System.Windows.Forms.TextBox
	Public WithEvents Text15 As System.Windows.Forms.TextBox
	Public WithEvents txtPla As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtDataInici As AxDataControl.AxGmsData
	Public WithEvents txtDataFi As AxDataControl.AxGmsData
	Public WithEvents GrdPlans As AxFlexCell.AxGrid
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents CmdInserta As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdBorra As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents lbl15 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMassiuPlans))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtContacto = New System.Windows.Forms.TextBox
		Me.txtEntidad = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtSucursal = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtResponsable = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtDescripcionObjetivo = New System.Windows.Forms.TextBox
		Me.txtObjectiu = New System.Windows.Forms.TextBox
		Me.Text15 = New System.Windows.Forms.TextBox
		Me.txtPla = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtDataInici = New AxDataControl.AxGmsData
		Me.txtDataFi = New AxDataControl.AxGmsData
		Me.GrdPlans = New AxFlexCell.AxGrid
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.CmdInserta = New AxXtremeSuiteControls.AxPushButton
		Me.cmdBorra = New AxXtremeSuiteControls.AxPushButton
		Me.Label3 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.lbl15 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtDataInici, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataFi, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdPlans, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.CmdInserta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdBorra, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Generar Massiu Plans"
		Me.ClientSize = New System.Drawing.Size(816, 439)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmMassiuPlans"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(302, 19)
		Me.Text3.Location = New System.Drawing.Point(187, 382)
		Me.Text3.TabIndex = 26
		Me.Text3.Tag = "^9"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtContacto.AutoSize = False
		Me.txtContacto.Size = New System.Drawing.Size(62, 19)
		Me.txtContacto.Location = New System.Drawing.Point(122, 382)
		Me.txtContacto.Maxlength = 6
		Me.txtContacto.TabIndex = 7
		Me.txtContacto.Tag = "9####G-ENTITATS CONTACTES#1"
		Me.txtContacto.AcceptsReturn = True
		Me.txtContacto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtContacto.BackColor = System.Drawing.SystemColors.Window
		Me.txtContacto.CausesValidation = True
		Me.txtContacto.Enabled = True
		Me.txtContacto.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtContacto.HideSelection = True
		Me.txtContacto.ReadOnly = False
		Me.txtContacto.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtContacto.MultiLine = False
		Me.txtContacto.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtContacto.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtContacto.TabStop = True
		Me.txtContacto.Visible = True
		Me.txtContacto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtContacto.Name = "txtContacto"
		Me.txtEntidad.AutoSize = False
		Me.txtEntidad.Size = New System.Drawing.Size(83, 19)
		Me.txtEntidad.Location = New System.Drawing.Point(122, 334)
		Me.txtEntidad.Maxlength = 8
		Me.txtEntidad.TabIndex = 5
		Me.txtEntidad.Tag = "6####G-ENTITATS#1#1"
		Me.txtEntidad.AcceptsReturn = True
		Me.txtEntidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEntidad.BackColor = System.Drawing.SystemColors.Window
		Me.txtEntidad.CausesValidation = True
		Me.txtEntidad.Enabled = True
		Me.txtEntidad.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEntidad.HideSelection = True
		Me.txtEntidad.ReadOnly = False
		Me.txtEntidad.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEntidad.MultiLine = False
		Me.txtEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEntidad.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEntidad.TabStop = True
		Me.txtEntidad.Visible = True
		Me.txtEntidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEntidad.Name = "txtEntidad"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(280, 19)
		Me.Text1.Location = New System.Drawing.Point(208, 334)
		Me.Text1.TabIndex = 21
		Me.Text1.Tag = "^6"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtSucursal.AutoSize = False
		Me.txtSucursal.Size = New System.Drawing.Size(31, 19)
		Me.txtSucursal.Location = New System.Drawing.Point(122, 358)
		Me.txtSucursal.Maxlength = 3
		Me.txtSucursal.TabIndex = 6
		Me.txtSucursal.Tag = "7####G-ENTITATS SUCURSALS#1#1"
		Me.txtSucursal.AcceptsReturn = True
		Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSucursal.BackColor = System.Drawing.SystemColors.Window
		Me.txtSucursal.CausesValidation = True
		Me.txtSucursal.Enabled = True
		Me.txtSucursal.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSucursal.HideSelection = True
		Me.txtSucursal.ReadOnly = False
		Me.txtSucursal.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSucursal.MultiLine = False
		Me.txtSucursal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSucursal.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSucursal.TabStop = True
		Me.txtSucursal.Visible = True
		Me.txtSucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSucursal.Name = "txtSucursal"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(332, 19)
		Me.Text2.Location = New System.Drawing.Point(156, 358)
		Me.Text2.TabIndex = 20
		Me.Text2.Tag = "^7"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtResponsable.AutoSize = False
		Me.txtResponsable.Size = New System.Drawing.Size(82, 19)
		Me.txtResponsable.Location = New System.Drawing.Point(122, 406)
		Me.txtResponsable.Maxlength = 15
		Me.txtResponsable.TabIndex = 8
		Me.txtResponsable.Tag = "8####I-PERSONAL#1#1"
		Me.txtResponsable.AcceptsReturn = True
		Me.txtResponsable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtResponsable.BackColor = System.Drawing.SystemColors.Window
		Me.txtResponsable.CausesValidation = True
		Me.txtResponsable.Enabled = True
		Me.txtResponsable.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtResponsable.HideSelection = True
		Me.txtResponsable.ReadOnly = False
		Me.txtResponsable.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtResponsable.MultiLine = False
		Me.txtResponsable.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtResponsable.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtResponsable.TabStop = True
		Me.txtResponsable.Visible = True
		Me.txtResponsable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtResponsable.Name = "txtResponsable"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(280, 19)
		Me.Text4.Location = New System.Drawing.Point(209, 406)
		Me.Text4.TabIndex = 19
		Me.Text4.Tag = "^8"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtDescripcionObjetivo.AutoSize = False
		Me.txtDescripcionObjetivo.Size = New System.Drawing.Size(677, 51)
		Me.txtDescripcionObjetivo.Location = New System.Drawing.Point(122, 80)
		Me.txtDescripcionObjetivo.Maxlength = 200
		Me.txtDescripcionObjetivo.MultiLine = True
		Me.txtDescripcionObjetivo.TabIndex = 4
		Me.txtDescripcionObjetivo.Tag = "5"
		Me.txtDescripcionObjetivo.AcceptsReturn = True
		Me.txtDescripcionObjetivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcionObjetivo.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcionObjetivo.CausesValidation = True
		Me.txtDescripcionObjetivo.Enabled = True
		Me.txtDescripcionObjetivo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcionObjetivo.HideSelection = True
		Me.txtDescripcionObjetivo.ReadOnly = False
		Me.txtDescripcionObjetivo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcionObjetivo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcionObjetivo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcionObjetivo.TabStop = True
		Me.txtDescripcionObjetivo.Visible = True
		Me.txtDescripcionObjetivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcionObjetivo.Name = "txtDescripcionObjetivo"
		Me.txtObjectiu.AutoSize = False
		Me.txtObjectiu.Size = New System.Drawing.Size(62, 19)
		Me.txtObjectiu.Location = New System.Drawing.Point(122, 56)
		Me.txtObjectiu.Maxlength = 6
		Me.txtObjectiu.TabIndex = 3
		Me.txtObjectiu.Tag = "4####CRM-TIPUS_OBJECTIUS#1#1"
		Me.txtObjectiu.AcceptsReturn = True
		Me.txtObjectiu.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObjectiu.BackColor = System.Drawing.SystemColors.Window
		Me.txtObjectiu.CausesValidation = True
		Me.txtObjectiu.Enabled = True
		Me.txtObjectiu.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObjectiu.HideSelection = True
		Me.txtObjectiu.ReadOnly = False
		Me.txtObjectiu.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObjectiu.MultiLine = False
		Me.txtObjectiu.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObjectiu.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObjectiu.TabStop = True
		Me.txtObjectiu.Visible = True
		Me.txtObjectiu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObjectiu.Name = "txtObjectiu"
		Me.Text15.AutoSize = False
		Me.Text15.BackColor = System.Drawing.Color.White
		Me.Text15.Enabled = False
		Me.Text15.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text15.Size = New System.Drawing.Size(300, 19)
		Me.Text15.Location = New System.Drawing.Point(187, 56)
		Me.Text15.TabIndex = 15
		Me.Text15.Tag = "^4"
		Me.Text15.AcceptsReturn = True
		Me.Text15.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text15.CausesValidation = True
		Me.Text15.HideSelection = True
		Me.Text15.ReadOnly = False
		Me.Text15.Maxlength = 0
		Me.Text15.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text15.MultiLine = False
		Me.Text15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text15.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text15.TabStop = True
		Me.Text15.Visible = True
		Me.Text15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text15.Name = "Text15"
		Me.txtPla.AutoSize = False
		Me.txtPla.Size = New System.Drawing.Size(31, 19)
		Me.txtPla.Location = New System.Drawing.Point(122, 8)
		Me.txtPla.Maxlength = 3
		Me.txtPla.TabIndex = 0
		Me.txtPla.Tag = "1####CRM-PLANS#1#1"
		Me.txtPla.AcceptsReturn = True
		Me.txtPla.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPla.BackColor = System.Drawing.SystemColors.Window
		Me.txtPla.CausesValidation = True
		Me.txtPla.Enabled = True
		Me.txtPla.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPla.HideSelection = True
		Me.txtPla.ReadOnly = False
		Me.txtPla.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPla.MultiLine = False
		Me.txtPla.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPla.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPla.TabStop = True
		Me.txtPla.Visible = True
		Me.txtPla.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPla.Name = "txtPla"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(332, 19)
		Me.Text6.Location = New System.Drawing.Point(156, 8)
		Me.Text6.TabIndex = 11
		Me.Text6.Tag = "^1"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		txtDataInici.OcxState = CType(resources.GetObject("txtDataInici.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataInici.Size = New System.Drawing.Size(87, 19)
		Me.txtDataInici.Location = New System.Drawing.Point(122, 32)
		Me.txtDataInici.TabIndex = 1
		Me.txtDataInici.Name = "txtDataInici"
		txtDataFi.OcxState = CType(resources.GetObject("txtDataFi.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataFi.Size = New System.Drawing.Size(87, 19)
		Me.txtDataFi.Location = New System.Drawing.Point(402, 32)
		Me.txtDataFi.TabIndex = 2
		Me.txtDataFi.Name = "txtDataFi"
		GrdPlans.OcxState = CType(resources.GetObject("GrdPlans.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdPlans.Size = New System.Drawing.Size(794, 183)
		Me.GrdPlans.Location = New System.Drawing.Point(8, 142)
		Me.GrdPlans.TabIndex = 18
		Me.GrdPlans.Name = "GrdPlans"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(720, 398)
		Me.cmdAceptar.TabIndex = 10
		Me.cmdAceptar.Name = "cmdAceptar"
		CmdInserta.OcxState = CType(resources.GetObject("CmdInserta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.CmdInserta.Size = New System.Drawing.Size(25, 25)
		Me.CmdInserta.Location = New System.Drawing.Point(494, 400)
		Me.CmdInserta.TabIndex = 9
		Me.CmdInserta.Name = "CmdInserta"
		cmdBorra.OcxState = CType(resources.GetObject("cmdBorra.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdBorra.Size = New System.Drawing.Size(25, 25)
		Me.cmdBorra.Location = New System.Drawing.Point(522, 400)
		Me.cmdBorra.TabIndex = 22
		Me.cmdBorra.Name = "cmdBorra"
		Me.Label3.Text = "Contacto"
		Me.Label3.Size = New System.Drawing.Size(111, 15)
		Me.Label3.Location = New System.Drawing.Point(8, 386)
		Me.Label3.TabIndex = 27
		Me.Label3.Tag = "4"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.lbl1.Text = "Entidad"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(8, 338)
		Me.lbl1.TabIndex = 25
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Sucursal"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(8, 362)
		Me.lbl2.TabIndex = 24
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl4.Text = "Responsable"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(8, 410)
		Me.lbl4.TabIndex = 23
		Me.lbl4.Tag = "10"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.Label2.Text = "Descripci� Objectiu"
		Me.Label2.Size = New System.Drawing.Size(111, 15)
		Me.Label2.Location = New System.Drawing.Point(8, 84)
		Me.Label2.TabIndex = 17
		Me.Label2.Tag = "5"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.lbl15.Text = "Tipus Objectiu"
		Me.lbl15.Size = New System.Drawing.Size(111, 15)
		Me.lbl15.Location = New System.Drawing.Point(8, 60)
		Me.lbl15.TabIndex = 16
		Me.lbl15.Tag = "4"
		Me.lbl15.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl15.BackColor = System.Drawing.SystemColors.Control
		Me.lbl15.Enabled = True
		Me.lbl15.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl15.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl15.UseMnemonic = True
		Me.lbl15.Visible = True
		Me.lbl15.AutoSize = False
		Me.lbl15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl15.Name = "lbl15"
		Me.Label1.Text = "Data Fi"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(288, 36)
		Me.Label1.TabIndex = 14
		Me.Label1.Tag = "3"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl7.Text = "Data Inici"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(8, 36)
		Me.lbl7.TabIndex = 13
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl6.Text = "Pla"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(8, 12)
		Me.lbl6.TabIndex = 12
		Me.lbl6.Tag = "1"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		CType(Me.cmdBorra, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.CmdInserta, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdPlans, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataFi, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataInici, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtContacto)
		Me.Controls.Add(txtEntidad)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtSucursal)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtResponsable)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtDescripcionObjetivo)
		Me.Controls.Add(txtObjectiu)
		Me.Controls.Add(Text15)
		Me.Controls.Add(txtPla)
		Me.Controls.Add(Text6)
		Me.Controls.Add(txtDataInici)
		Me.Controls.Add(txtDataFi)
		Me.Controls.Add(GrdPlans)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(CmdInserta)
		Me.Controls.Add(cmdBorra)
		Me.Controls.Add(Label3)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(Label2)
		Me.Controls.Add(lbl15)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl6)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class