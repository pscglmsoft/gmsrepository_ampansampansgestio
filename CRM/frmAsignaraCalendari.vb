Option Strict Off
Option Explicit On
Friend Class frmAsignaraCalendari
	Inherits FormParent
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		Dim I As Short
		Dim Nod As String
		
		Nod = ""
		
		For I = 1 To GrdMails.Rows - 1
			If GrdMails.Cell(I, 4).Text = "1" Then
				If Nod <> "" Then Nod = Nod & "," '& Chr(13)
				Nod = Nod & GrdMails.Cell(I, 3).Text
			End If
		Next I
		frmAccions.LlistaMails = Nod
		frmAccions.ConcepteCita = txtConcepteCita.Text
		Me.Unload()
	End Sub
	
	Private Sub frmAsignaraCalendari_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		CarregaGridMails()
	End Sub
	
	Private Sub CarregaGridMails()
		CarregaFGrid(GrdMails, "CMAILORD^CRM")
	End Sub
	
	
	Private Sub frmAsignaraCalendari_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub frmAsignaraCalendari_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmAsignaraCalendari_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
End Class
