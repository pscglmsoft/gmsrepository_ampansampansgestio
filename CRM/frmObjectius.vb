Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmObjectius
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	
	
	Public Overrides Sub Clone()
		Dim pla As String
		Dim DataI As String
		Dim DataF As String
		Dim DescO As String
		Dim Resp As String
		Dim CT As Short
		'Dim EntCon As String
		
		pla = txtPla.Text
		DataI = txtFechaInicio.Text
		DataF = txtFechaFin.Text
		DescO = txtDescripcionObjetivo.Text
		Resp = txtResponsable.Text
		CT = CShort(txtCentroTrabajo.Text)
		'EntCon = txtEntitatContacte.Text
		
		Inici()
		
		'txtEntitatContacte = EntCon
		'DisplayDescripcio Me, txtEntitatContacte
		txtPla.Text = pla
		DisplayDescripcio(Me, txtPla)
		txtFechaInicio.Text = DataI
		txtFechaFin.Text = DataF
		txtDescripcionObjetivo.Text = DescO
		DisplayDescripcio(Me, txtDescripcionObjetivo)
		txtResponsable.Text = Resp
		DisplayDescripcio(Me, txtResponsable)
		txtCentroTrabajo.Text = CStr(CT)
		DisplayDescripcio(Me, txtCentroTrabajo)
		txtResultado.Text = ""
		'DisplayDescripcio Me, txtResultado
		Text10.Text = ""
		
	End Sub
	
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		CarregaAccions()
	End Sub
	
	Public Overrides Sub FGetReg()
		CarregaAccions()
		DisplayDescripcio(Me, txtEntitatContacte, txtEntidad.Text & S & "S" & S & txtSucursal.Text & S & "C")
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	'UPGRADE_WARNING: Form evento frmObjectius.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmObjectius_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaAccions()
	End Sub
	
	Private Sub frmObjectius_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmObjectius_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmObjectius_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmObjectius_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub GrdObjAccions_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdObjAccions.DoubleClick
		If GrdObjAccions.ActiveCell.Row = 0 Then Exit Sub
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmAccions, "frmAccions", txtEntidad.Text & S & txtSucursal.Text & S & Piece(GrdObjAccions.Cell(GrdObjAccions.ActiveCell.Row, 1).Text, "|", 3))
	End Sub
	
	Private Sub GrdObjAccions_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdObjAccions.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtEntidad.Text = "" Then Exit Sub
		If txtSucursal.Text = "" Then Exit Sub
		If txtObjetivo.Text = "" Then Exit Sub
		If txtNumRegistro.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdObjAccions", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nueva Acci�n",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmAccions", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar Acci�n",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdObjAccions.Rows > 1 And PermisConsulta("frmAccions", e_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar Acci�n",  ,  , XPIcon("Delete"),  ,  ,  ,  , "B")
				.MenuItems.Item("B").Enabled = GrdObjAccions.Rows > 1 And PermisConsulta("frmAccions", e_Permisos.Baixes)
			End With
		End With
		XpExecutaMenu(Me, "GrdObjAccions", eventArgs.X, eventArgs.Y)
		
	End Sub
	
	Private Sub txtEntidad_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidad.GotFocus
		XGotFocus(Me, txtEntidad)
	End Sub
	
	Private Sub txtEntidad_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidad.DoubleClick
		ConsultaTaula(Me, txtEntidad)
	End Sub
	
	Private Sub txtEntidad_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidad.LostFocus
		If txtEntidad.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtEntidad)
	End Sub
	
	Private Sub txtEntidad_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEntidad.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtEntitatContacte_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntitatContacte.DoubleClick
		ConsultaTaula(Me, txtEntitatContacte,  , txtEntidad.Text & S & "S" & S & txtSucursal.Text & S & "C")
	End Sub
	
	Private Sub txtEntitatContacte_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntitatContacte.GotFocus
		XGotFocus(Me, txtEntitatContacte)
	End Sub
	
	Private Sub txtEntitatContacte_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntitatContacte.LostFocus
		XLostFocus(Me, txtEntitatContacte)
	End Sub
	
	Private Sub txtEntitatContacte_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEntitatContacte.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtEntidad.Text & S & "S" & S & txtSucursal.Text & S & "C")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPla_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPla.DoubleClick
		ConsultaTaula(Me, txtPla)
	End Sub
	
	Private Sub txtPla_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPla.GotFocus
		XGotFocus(Me, txtPla)
	End Sub
	
	Private Sub txtPla_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPla.LostFocus
		XLostFocus(Me, txtPla)
	End Sub
	
	Private Sub txtPla_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPla.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSucursal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.GotFocus
		XGotFocus(Me, txtSucursal)
	End Sub
	
	Private Sub txtSucursal_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.DoubleClick
		ConsultaTaula(Me, txtSucursal,  , txtEntidad.Text & S & "S")
	End Sub
	
	Private Sub txtSucursal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.LostFocus
		If txtSucursal.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtSucursal)
	End Sub
	
	Private Sub txtSucursal_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSucursal.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtEntidad.Text & S & "S")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObjetivo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObjetivo.GotFocus
		XGotFocus(Me, txtObjetivo)
	End Sub
	
	Private Sub txtObjetivo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObjetivo.DoubleClick
		ConsultaTaula(Me, txtObjetivo)
	End Sub
	
	Private Sub txtObjetivo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObjetivo.LostFocus
		If txtObjetivo.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtObjetivo)
	End Sub
	
	Private Sub txtObjetivo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObjetivo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtNumRegistro_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumRegistro.GotFocus
		XGotFocus(Me, txtNumRegistro)
	End Sub
	
	Private Sub txtNumRegistro_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumRegistro.LostFocus
		If txtNumRegistro.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtNumRegistro)
	End Sub
	
	Private Sub txtNumRegistro_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNumRegistro.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaInicio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaInicio.GotFocus
		XGotFocus(Me, txtFechaInicio)
	End Sub
	
	Private Sub txtFechaInicio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaInicio.LostFocus
		XLostFocus(Me, txtFechaInicio)
	End Sub
	
	Private Sub txtFechaFin_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaFin.GotFocus
		XGotFocus(Me, txtFechaFin)
	End Sub
	
	Private Sub txtFechaFin_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaFin.LostFocus
		XLostFocus(Me, txtFechaFin)
	End Sub
	
	Private Sub txtDescripcionObjetivo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcionObjetivo.GotFocus
		XGotFocus(Me, txtDescripcionObjetivo)
	End Sub
	
	Private Sub txtDescripcionObjetivo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcionObjetivo.LostFocus
		XLostFocus(Me, txtDescripcionObjetivo)
	End Sub
	
	Private Sub txtDescripcionObjetivo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcionObjetivo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtResponsable_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.GotFocus
		XGotFocus(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.DoubleClick
		ConsultaTaula(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.LostFocus
		XLostFocus(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtResponsable.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCentroTrabajo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentroTrabajo.GotFocus
		XGotFocus(Me, txtCentroTrabajo)
	End Sub
	
	Private Sub txtCentroTrabajo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentroTrabajo.DoubleClick
		ConsultaTaula(Me, txtCentroTrabajo)
	End Sub
	
	Private Sub txtCentroTrabajo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentroTrabajo.LostFocus
		XLostFocus(Me, txtCentroTrabajo)
	End Sub
	
	Private Sub txtCentroTrabajo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentroTrabajo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtResultado_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResultado.GotFocus
		XGotFocus(Me, txtResultado)
	End Sub
	
	Private Sub txtResultado_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResultado.DoubleClick
		ConsultaTaula(Me, txtResultado)
	End Sub
	
	Private Sub txtResultado_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResultado.LostFocus
		XLostFocus(Me, txtResultado)
	End Sub
	
	Private Sub txtResultado_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtResultado.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Select Case NomMenu
			Case "GrdObjAccions"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "N"
						ObreFormulari(frmAccions, "frmAccions", txtEntidad.Text & S & txtSucursal.Text)
						frmAccions.txtObjetivo.Text = txtObjetivo.Text
						DisplayDescripcio(frmAccions, (frmAccions.txtObjetivo))
						frmAccions.txtNumero.Text = txtNumRegistro.Text
						frmAccions.txtResponsable.Text = txtResponsable.Text
						DisplayDescripcio(frmAccions, (frmAccions.txtResponsable))
						frmAccions.txtCentroTrabajo.Text = txtCentroTrabajo.Text
						DisplayDescripcio(frmAccions, (frmAccions.txtCentroTrabajo))
					Case "E"
						If GrdObjAccions.ActiveCell.Row = 0 Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmAccions, "frmAccions", txtEntidad.Text & S & txtSucursal.Text & S & Piece(GrdObjAccions.Cell(GrdObjAccions.ActiveCell.Row, 1).Text, "|", 3))
					Case "B"
						If GrdObjAccions.ActiveCell.Row = 0 Then Exit Sub
						
						A = GrdObjAccions.Cell(GrdObjAccions.ActiveCell.Row, 2).Text
						If A = "" Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmAccions, "frmAccions", txtEntidad.Text & S & txtSucursal.Text & S & Piece(GrdObjAccions.Cell(GrdObjAccions.ActiveCell.Row, 1).Text, "|", 3))
						LlibAplicacio.DeleteReg(frmAccions   )
						frmAccions.Unload()
				End Select
		End Select
	End Sub
	
	Private Sub CarregaAccions()
		CarregaFGrid(GrdObjAccions, "CGACCIONS^CRM", txtEntidad.Text & S & txtSucursal.Text & S & txtObjetivo.Text & S & txtNumRegistro.Text)
	End Sub
End Class
