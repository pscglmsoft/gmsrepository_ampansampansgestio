Option Strict Off
Option Explicit On
Friend Class frmMailResumAccio
	Inherits FormParent
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		Dim I As Short
		Dim Nod As String
		Dim Resp As MsgBoxResult
		Nod = ""
		
		If GravaRegistre(frmAccions) = False Then
			Exit Sub
		End If
		
		If frmAccions.cmbEstadoAccion.Columns(1).Value = 2 And frmAccions.txtOrigen.Text = "" Then
			xMsgBox("Falta informar l'Origen", MsgBoxStyle.Information, Me.Text)
			Exit Sub
		End If
		
		If frmAccions.cmbEstadoAccion.Columns(1).Value = 2 And frmAccions.txtCanal.Text = "" Then
			xMsgBox("Falta informar el Canal de comunicaci�n", MsgBoxStyle.Information, Me.Text)
			Exit Sub
		End If
		
		
		For I = 1 To GrdMails.Rows - 1
			If GrdMails.Cell(I, 2).Text = "1" Then
				If Nod <> "" Then Nod = Nod & ";" '& Chr(13)
				Nod = Nod & GrdMails.Cell(I, 4).Text
			End If
		Next I
		
		
		Resp = MsgBox("Vols enviar un resum de l'Acci� ?", MsgBoxStyle.YesNo, "E-MAIL")
		If Resp = MsgBoxResult.Yes Then
			CacheNetejaParametres()
			MCache.P1 = frmAccions.txtEntidad.Text & S & frmAccions.txtSucursal.Text & S & frmAccions.txtNumRegistro.Text
			MCache.P2 = Nod
			If Nod = "" Then
				xMsgBox("Falta sel�leccionar destinataris", MsgBoxStyle.Information, Me.Text)
				Exit Sub
			End If
			CacheXecute("D RESUMMAIL^CRM")
		End If
		Me.Unload()
	End Sub
	
	Private Sub frmMailResumAccio_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		CarregaGridMails()
	End Sub
	
	Private Sub CarregaGridMails()
		CarregaFGrid(GrdMails, "CMAIL1ORD^CRM")
	End Sub
	
	
	Private Sub frmMailResumAccio_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub frmMailResumAccio_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmMailResumAccio_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
End Class
