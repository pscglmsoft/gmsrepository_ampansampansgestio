<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAsignaraCalendari
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtConcepteCita As System.Windows.Forms.TextBox
	Public WithEvents GrdMails As AxFlexCell.AxGrid
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAsignaraCalendari))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtConcepteCita = New System.Windows.Forms.TextBox
		Me.GrdMails = New AxFlexCell.AxGrid
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.Label1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.GrdMails, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Informar calendarios"
		Me.ClientSize = New System.Drawing.Size(506, 413)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmAsignaraCalendari"
		Me.txtConcepteCita.AutoSize = False
		Me.txtConcepteCita.Size = New System.Drawing.Size(409, 21)
		Me.txtConcepteCita.Location = New System.Drawing.Point(86, 4)
		Me.txtConcepteCita.TabIndex = 2
		Me.txtConcepteCita.AcceptsReturn = True
		Me.txtConcepteCita.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtConcepteCita.BackColor = System.Drawing.SystemColors.Window
		Me.txtConcepteCita.CausesValidation = True
		Me.txtConcepteCita.Enabled = True
		Me.txtConcepteCita.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtConcepteCita.HideSelection = True
		Me.txtConcepteCita.ReadOnly = False
		Me.txtConcepteCita.Maxlength = 0
		Me.txtConcepteCita.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtConcepteCita.MultiLine = False
		Me.txtConcepteCita.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtConcepteCita.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtConcepteCita.TabStop = True
		Me.txtConcepteCita.Visible = True
		Me.txtConcepteCita.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtConcepteCita.Name = "txtConcepteCita"
		GrdMails.OcxState = CType(resources.GetObject("GrdMails.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdMails.Size = New System.Drawing.Size(490, 347)
		Me.GrdMails.Location = New System.Drawing.Point(8, 30)
		Me.GrdMails.TabIndex = 0
		Me.GrdMails.Name = "GrdMails"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(416, 382)
		Me.cmdAceptar.TabIndex = 1
		Me.cmdAceptar.Name = "cmdAceptar"
		Me.Label1.Text = "Ubicaci� Cita"
		Me.Label1.Size = New System.Drawing.Size(83, 19)
		Me.Label1.Location = New System.Drawing.Point(10, 6)
		Me.Label1.TabIndex = 3
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Controls.Add(txtConcepteCita)
		Me.Controls.Add(GrdMails)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(Label1)
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdMails, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class