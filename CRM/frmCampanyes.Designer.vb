<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCampanyes
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents txtResponsable As System.Windows.Forms.TextBox
	Public WithEvents txtCodigo As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcion As System.Windows.Forms.TextBox
	Public WithEvents txtTipoEvento As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtFechaInicio As AxDataControl.AxGmsData
	Public WithEvents txtFechaFin As AxDataControl.AxGmsData
	Public WithEvents txtObservaciones As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents GrdCampAccions As AxFlexCell.AxGrid
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCampanyes))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.Text9 = New System.Windows.Forms.TextBox
		Me.txtResponsable = New System.Windows.Forms.TextBox
		Me.txtCodigo = New System.Windows.Forms.TextBox
		Me.txtDescripcion = New System.Windows.Forms.TextBox
		Me.txtTipoEvento = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtFechaInicio = New AxDataControl.AxGmsData
		Me.txtFechaFin = New AxDataControl.AxGmsData
		Me.txtObservaciones = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.GrdCampAccions = New AxFlexCell.AxGrid
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtFechaInicio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaFin, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdCampAccions, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Campa�as"
		Me.ClientSize = New System.Drawing.Size(624, 514)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "CRM-CAMPANYES"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmCampanyes"
		Me.Text9.AutoSize = False
		Me.Text9.BackColor = System.Drawing.Color.White
		Me.Text9.Enabled = False
		Me.Text9.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text9.Size = New System.Drawing.Size(268, 19)
		Me.Text9.Location = New System.Drawing.Point(231, 82)
		Me.Text9.TabIndex = 18
		Me.Text9.Tag = "^7"
		Me.Text9.AcceptsReturn = True
		Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text9.CausesValidation = True
		Me.Text9.HideSelection = True
		Me.Text9.ReadOnly = False
		Me.Text9.Maxlength = 0
		Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text9.MultiLine = False
		Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text9.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text9.TabStop = True
		Me.Text9.Visible = True
		Me.Text9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text9.Name = "Text9"
		Me.txtResponsable.AutoSize = False
		Me.txtResponsable.Size = New System.Drawing.Size(104, 19)
		Me.txtResponsable.Location = New System.Drawing.Point(124, 82)
		Me.txtResponsable.Maxlength = 10
		Me.txtResponsable.TabIndex = 6
		Me.txtResponsable.Tag = "7"
		Me.txtResponsable.AcceptsReturn = True
		Me.txtResponsable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtResponsable.BackColor = System.Drawing.SystemColors.Window
		Me.txtResponsable.CausesValidation = True
		Me.txtResponsable.Enabled = True
		Me.txtResponsable.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtResponsable.HideSelection = True
		Me.txtResponsable.ReadOnly = False
		Me.txtResponsable.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtResponsable.MultiLine = False
		Me.txtResponsable.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtResponsable.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtResponsable.TabStop = True
		Me.txtResponsable.Visible = True
		Me.txtResponsable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtResponsable.Name = "txtResponsable"
		Me.txtCodigo.AutoSize = False
		Me.txtCodigo.Size = New System.Drawing.Size(31, 19)
		Me.txtCodigo.Location = New System.Drawing.Point(124, 10)
		Me.txtCodigo.Maxlength = 3
		Me.txtCodigo.TabIndex = 1
		Me.txtCodigo.Tag = "*1"
		Me.txtCodigo.AcceptsReturn = True
		Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigo.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigo.CausesValidation = True
		Me.txtCodigo.Enabled = True
		Me.txtCodigo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigo.HideSelection = True
		Me.txtCodigo.ReadOnly = False
		Me.txtCodigo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigo.MultiLine = False
		Me.txtCodigo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigo.TabStop = True
		Me.txtCodigo.Visible = True
		Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigo.Name = "txtCodigo"
		Me.txtDescripcion.AutoSize = False
		Me.txtDescripcion.Size = New System.Drawing.Size(491, 19)
		Me.txtDescripcion.Location = New System.Drawing.Point(124, 34)
		Me.txtDescripcion.Maxlength = 100
		Me.txtDescripcion.TabIndex = 3
		Me.txtDescripcion.Tag = "2"
		Me.txtDescripcion.AcceptsReturn = True
		Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcion.CausesValidation = True
		Me.txtDescripcion.Enabled = True
		Me.txtDescripcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcion.HideSelection = True
		Me.txtDescripcion.ReadOnly = False
		Me.txtDescripcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcion.MultiLine = False
		Me.txtDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcion.TabStop = True
		Me.txtDescripcion.Visible = True
		Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcion.Name = "txtDescripcion"
		Me.txtTipoEvento.AutoSize = False
		Me.txtTipoEvento.Size = New System.Drawing.Size(21, 19)
		Me.txtTipoEvento.Location = New System.Drawing.Point(124, 58)
		Me.txtTipoEvento.Maxlength = 2
		Me.txtTipoEvento.TabIndex = 5
		Me.txtTipoEvento.Tag = "3"
		Me.txtTipoEvento.AcceptsReturn = True
		Me.txtTipoEvento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoEvento.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoEvento.CausesValidation = True
		Me.txtTipoEvento.Enabled = True
		Me.txtTipoEvento.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoEvento.HideSelection = True
		Me.txtTipoEvento.ReadOnly = False
		Me.txtTipoEvento.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoEvento.MultiLine = False
		Me.txtTipoEvento.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoEvento.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoEvento.TabStop = True
		Me.txtTipoEvento.Visible = True
		Me.txtTipoEvento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoEvento.Name = "txtTipoEvento"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(350, 19)
		Me.Text3.Location = New System.Drawing.Point(148, 58)
		Me.Text3.TabIndex = 11
		Me.Text3.Tag = "^3"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		txtFechaInicio.OcxState = CType(resources.GetObject("txtFechaInicio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaInicio.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaInicio.Location = New System.Drawing.Point(124, 106)
		Me.txtFechaInicio.TabIndex = 7
		Me.txtFechaInicio.Name = "txtFechaInicio"
		txtFechaFin.OcxState = CType(resources.GetObject("txtFechaFin.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaFin.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaFin.Location = New System.Drawing.Point(124, 130)
		Me.txtFechaFin.TabIndex = 8
		Me.txtFechaFin.Name = "txtFechaFin"
		Me.txtObservaciones.AutoSize = False
		Me.txtObservaciones.Size = New System.Drawing.Size(491, 61)
		Me.txtObservaciones.Location = New System.Drawing.Point(124, 154)
		Me.txtObservaciones.Maxlength = 200
		Me.txtObservaciones.MultiLine = True
		Me.txtObservaciones.TabIndex = 9
		Me.txtObservaciones.Tag = "6"
		Me.txtObservaciones.AcceptsReturn = True
		Me.txtObservaciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservaciones.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservaciones.CausesValidation = True
		Me.txtObservaciones.Enabled = True
		Me.txtObservaciones.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservaciones.HideSelection = True
		Me.txtObservaciones.ReadOnly = False
		Me.txtObservaciones.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservaciones.TabStop = True
		Me.txtObservaciones.Visible = True
		Me.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservaciones.Name = "txtObservaciones"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(445, 486)
		Me.cmdAceptar.TabIndex = 10
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(535, 486)
		Me.cmdGuardar.TabIndex = 15
		Me.cmdGuardar.Name = "cmdGuardar"
		GrdCampAccions.OcxState = CType(resources.GetObject("GrdCampAccions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdCampAccions.Size = New System.Drawing.Size(606, 239)
		Me.GrdCampAccions.Location = New System.Drawing.Point(10, 226)
		Me.GrdCampAccions.TabIndex = 17
		Me.GrdCampAccions.Name = "GrdCampAccions"
		Me.lbl9.Text = "Responsable"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(10, 86)
		Me.lbl9.TabIndex = 19
		Me.lbl9.Tag = "7"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl1.Text = "C�digo"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Descripci�n"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Tipo evento"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 4
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Fecha inicio"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 110)
		Me.lbl4.TabIndex = 12
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Fecha fin"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 134)
		Me.lbl5.TabIndex = 13
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Observaciones"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 158)
		Me.lbl6.TabIndex = 14
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 485)
		Me.lblLock.TabIndex = 16
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 479
		Me.Line1.Y2 = 479
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 480
		Me.Line2.Y2 = 480
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.GrdCampAccions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaFin, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaInicio, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(Text9)
		Me.Controls.Add(txtResponsable)
		Me.Controls.Add(txtCodigo)
		Me.Controls.Add(txtDescripcion)
		Me.Controls.Add(txtTipoEvento)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtFechaInicio)
		Me.Controls.Add(txtFechaFin)
		Me.Controls.Add(txtObservaciones)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(GrdCampAccions)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class