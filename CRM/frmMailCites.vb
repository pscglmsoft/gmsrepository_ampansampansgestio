Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmMailCites
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	
	
	Private Sub chkIncorpora_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkIncorpora.GotFocus
		XGotFocus(Me, chkIncorpora)
	End Sub
	
	Private Sub chkIncorpora_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkIncorpora.LostFocus
		XLostFocus(Me, chkIncorpora)
	End Sub
	
	Private Sub chkLimpieza_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkLimpieza.GotFocus
		XGotFocus(Me, chkLimpieza)
	End Sub
	
	Private Sub chkLimpieza_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkLimpieza.LostFocus
		XLostFocus(Me, chkLimpieza)
	End Sub
	
	Private Sub chkManipulados_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkManipulados.GotFocus
		XGotFocus(Me, chkManipulados)
	End Sub
	
	Private Sub chkManipulados_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkManipulados.LostFocus
		XLostFocus(Me, chkManipulados)
	End Sub
	
	Private Sub chkImprenta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkImprenta.GotFocus
		XGotFocus(Me, chkImprenta)
	End Sub
	
	Private Sub chkImprenta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkImprenta.LostFocus
		XLostFocus(Me, chkImprenta)
	End Sub
	
	Private Sub chkJardineria_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkJardineria.GotFocus
		XGotFocus(Me, chkJardineria)
	End Sub
	
	Private Sub chkJardineria_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkJardineria.LostFocus
		XLostFocus(Me, chkJardineria)
	End Sub
	
	Private Sub chkMantenimiento_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkMantenimiento.GotFocus
		XGotFocus(Me, chkMantenimiento)
	End Sub
	
	Private Sub chkMantenimiento_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkMantenimiento.LostFocus
		XLostFocus(Me, chkMantenimiento)
	End Sub
	
	Private Sub chkGarden_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkGarden.GotFocus
		XGotFocus(Me, chkGarden)
	End Sub
	
	Private Sub chkGarden_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkGarden.LostFocus
		XLostFocus(Me, chkGarden)
	End Sub
	
	Private Sub chkCanonge_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkCanonge.GotFocus
		XGotFocus(Me, chkCanonge)
	End Sub
	
	Private Sub chkCanonge_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkCanonge.LostFocus
		XLostFocus(Me, chkCanonge)
	End Sub
	
	
	
	Private Sub chkPertanyComiteDireccio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPertanyComiteDireccio.GotFocus
		XGotFocus(Me, chkPertanyComiteDireccio)
	End Sub
	
	Private Sub chkPertanyComiteDireccio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPertanyComiteDireccio.LostFocus
		XLostFocus(Me, chkPertanyComiteDireccio)
	End Sub
	
	Private Sub chkPuedeVerAcciones_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPuedeVerAcciones.GotFocus
		XGotFocus(Me, chkPuedeVerAcciones)
	End Sub
	
	Private Sub chkPuedeVerAcciones_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPuedeVerAcciones.LostFocus
		XLostFocus(Me, chkPuedeVerAcciones)
	End Sub
	
	Private Sub chkVino_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkVino.GotFocus
		XGotFocus(Me, chkVino)
	End Sub
	
	Private Sub chkVino_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkVino.LostFocus
		XLostFocus(Me, chkVino)
	End Sub
	
	Private Sub chkDonantecuota_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDonantecuota.GotFocus
		XGotFocus(Me, chkDonantecuota)
	End Sub
	
	Private Sub chkDonantecuota_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDonantecuota.LostFocus
		XLostFocus(Me, chkDonantecuota)
	End Sub
	
	Private Sub chkDonacionPuntual_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDonacionPuntual.GotFocus
		XGotFocus(Me, chkDonacionPuntual)
	End Sub
	
	Private Sub chkDonacionPuntual_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkDonacionPuntual.LostFocus
		XLostFocus(Me, chkDonacionPuntual)
	End Sub
	
	Private Sub chkEsponsor_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkEsponsor.GotFocus
		XGotFocus(Me, chkEsponsor)
	End Sub
	
	Private Sub chkEsponsor_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkEsponsor.LostFocus
		XLostFocus(Me, chkEsponsor)
	End Sub
	
	Private Sub chkVoluntariado_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkVoluntariado.GotFocus
		XGotFocus(Me, chkVoluntariado)
	End Sub
	
	Private Sub chkVoluntariado_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkVoluntariado.LostFocus
		XLostFocus(Me, chkVoluntariado)
	End Sub
	
	
	Private Sub frmMailCites_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmMailCites_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmMailCites_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmMailCites_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtColaborador_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtColaborador.GotFocus
		XGotFocus(Me, txtColaborador)
	End Sub
	
	Private Sub txtColaborador_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtColaborador.DoubleClick
		ConsultaTaula(Me, txtColaborador)
	End Sub
	
	Private Sub txtColaborador_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtColaborador.LostFocus
		If txtColaborador.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtColaborador)
	End Sub
	
	Private Sub txtColaborador_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtColaborador.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtMail_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMail.GotFocus
		XGotFocus(Me, txtMail)
	End Sub
	
	Private Sub txtMail_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMail.LostFocus
		XLostFocus(Me, txtMail)
	End Sub
	
	Private Sub txtMail_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMail.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.GotFocus
		XGotFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtFechaBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.LostFocus
		XLostFocus(Me, txtFechaBaja)
	End Sub
End Class
