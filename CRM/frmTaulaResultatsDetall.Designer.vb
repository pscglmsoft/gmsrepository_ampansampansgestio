<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmTaulaResultatsDetall
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtResultadoObjetivo As System.Windows.Forms.TextBox
	Public WithEvents txtResultado As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtRFix As System.Windows.Forms.TextBox
	Public WithEvents txtCodigo As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcion As System.Windows.Forms.TextBox
	Public WithEvents txtFechaBaja As AxDataControl.AxGmsData
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTaulaResultatsDetall))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtResultadoObjetivo = New System.Windows.Forms.TextBox
		Me.txtResultado = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtRFix = New System.Windows.Forms.TextBox
		Me.txtCodigo = New System.Windows.Forms.TextBox
		Me.txtDescripcion = New System.Windows.Forms.TextBox
		Me.txtFechaBaja = New AxDataControl.AxGmsData
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Tabla resultados detalle"
		Me.ClientSize = New System.Drawing.Size(471, 188)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "CRM-TAULA_RESULTATS_DETALL"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmTaulaResultatsDetall"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(318, 19)
		Me.Text3.Location = New System.Drawing.Point(148, 82)
		Me.Text3.TabIndex = 14
		Me.Text3.Tag = "^6"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtResultadoObjetivo.AutoSize = False
		Me.txtResultadoObjetivo.Size = New System.Drawing.Size(21, 19)
		Me.txtResultadoObjetivo.Location = New System.Drawing.Point(124, 82)
		Me.txtResultadoObjetivo.Maxlength = 2
		Me.txtResultadoObjetivo.TabIndex = 4
		Me.txtResultadoObjetivo.Tag = "6"
		Me.txtResultadoObjetivo.AcceptsReturn = True
		Me.txtResultadoObjetivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtResultadoObjetivo.BackColor = System.Drawing.SystemColors.Window
		Me.txtResultadoObjetivo.CausesValidation = True
		Me.txtResultadoObjetivo.Enabled = True
		Me.txtResultadoObjetivo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtResultadoObjetivo.HideSelection = True
		Me.txtResultadoObjetivo.ReadOnly = False
		Me.txtResultadoObjetivo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtResultadoObjetivo.MultiLine = False
		Me.txtResultadoObjetivo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtResultadoObjetivo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtResultadoObjetivo.TabStop = True
		Me.txtResultadoObjetivo.Visible = True
		Me.txtResultadoObjetivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtResultadoObjetivo.Name = "txtResultadoObjetivo"
		Me.txtResultado.AutoSize = False
		Me.txtResultado.Size = New System.Drawing.Size(21, 19)
		Me.txtResultado.Location = New System.Drawing.Point(124, 10)
		Me.txtResultado.Maxlength = 2
		Me.txtResultado.TabIndex = 1
		Me.txtResultado.Tag = "*1"
		Me.txtResultado.AcceptsReturn = True
		Me.txtResultado.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtResultado.BackColor = System.Drawing.SystemColors.Window
		Me.txtResultado.CausesValidation = True
		Me.txtResultado.Enabled = True
		Me.txtResultado.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtResultado.HideSelection = True
		Me.txtResultado.ReadOnly = False
		Me.txtResultado.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtResultado.MultiLine = False
		Me.txtResultado.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtResultado.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtResultado.TabStop = True
		Me.txtResultado.Visible = True
		Me.txtResultado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtResultado.Name = "txtResultado"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(318, 19)
		Me.Text1.Location = New System.Drawing.Point(148, 10)
		Me.Text1.TabIndex = 7
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtRFix.AutoSize = False
		Me.txtRFix.Size = New System.Drawing.Size(11, 21)
		Me.txtRFix.Location = New System.Drawing.Point(224, 104)
		Me.txtRFix.Maxlength = 1
		Me.txtRFix.TabIndex = 8
		Me.txtRFix.Tag = "*2"
		Me.txtRFix.AcceptsReturn = True
		Me.txtRFix.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtRFix.BackColor = System.Drawing.SystemColors.Window
		Me.txtRFix.CausesValidation = True
		Me.txtRFix.Enabled = True
		Me.txtRFix.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtRFix.HideSelection = True
		Me.txtRFix.ReadOnly = False
		Me.txtRFix.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtRFix.MultiLine = False
		Me.txtRFix.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtRFix.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtRFix.TabStop = True
		Me.txtRFix.Visible = True
		Me.txtRFix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtRFix.Name = "txtRFix"
		Me.txtCodigo.AutoSize = False
		Me.txtCodigo.Size = New System.Drawing.Size(21, 19)
		Me.txtCodigo.Location = New System.Drawing.Point(124, 34)
		Me.txtCodigo.Maxlength = 2
		Me.txtCodigo.TabIndex = 2
		Me.txtCodigo.Tag = "*3"
		Me.txtCodigo.AcceptsReturn = True
		Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigo.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigo.CausesValidation = True
		Me.txtCodigo.Enabled = True
		Me.txtCodigo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigo.HideSelection = True
		Me.txtCodigo.ReadOnly = False
		Me.txtCodigo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigo.MultiLine = False
		Me.txtCodigo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigo.TabStop = True
		Me.txtCodigo.Visible = True
		Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigo.Name = "txtCodigo"
		Me.txtDescripcion.AutoSize = False
		Me.txtDescripcion.Size = New System.Drawing.Size(343, 19)
		Me.txtDescripcion.Location = New System.Drawing.Point(124, 58)
		Me.txtDescripcion.Maxlength = 50
		Me.txtDescripcion.TabIndex = 3
		Me.txtDescripcion.Tag = "4"
		Me.txtDescripcion.AcceptsReturn = True
		Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcion.CausesValidation = True
		Me.txtDescripcion.Enabled = True
		Me.txtDescripcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcion.HideSelection = True
		Me.txtDescripcion.ReadOnly = False
		Me.txtDescripcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcion.MultiLine = False
		Me.txtDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcion.TabStop = True
		Me.txtDescripcion.Visible = True
		Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcion.Name = "txtDescripcion"
		txtFechaBaja.OcxState = CType(resources.GetObject("txtFechaBaja.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaBaja.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaBaja.Location = New System.Drawing.Point(124, 106)
		Me.txtFechaBaja.TabIndex = 5
		Me.txtFechaBaja.Name = "txtFechaBaja"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(293, 158)
		Me.cmdAceptar.TabIndex = 6
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(383, 158)
		Me.cmdGuardar.TabIndex = 12
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.Label1.Text = "Resultado"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 86)
		Me.Label1.TabIndex = 15
		Me.Label1.Tag = "6"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Resultado"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl3.Text = "Codigo"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 38)
		Me.lbl3.TabIndex = 9
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Descripci�n"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 62)
		Me.lbl4.TabIndex = 10
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Fecha baja"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 110)
		Me.lbl5.TabIndex = 11
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 157)
		Me.lblLock.TabIndex = 13
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 151
		Me.Line1.Y2 = 151
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 152
		Me.Line2.Y2 = 152
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtResultadoObjetivo)
		Me.Controls.Add(txtResultado)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtRFix)
		Me.Controls.Add(txtCodigo)
		Me.Controls.Add(txtDescripcion)
		Me.Controls.Add(txtFechaBaja)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class