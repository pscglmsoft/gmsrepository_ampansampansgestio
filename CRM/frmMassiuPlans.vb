Option Strict Off
Option Explicit On
Friend Class frmMassiuPlans
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If VerificaCamps = False Then Exit Sub
		
		CacheNetejaParametres()
		MCache.P1 = txtPla.Text
		MCache.P2 = txtDataInici.Text
		MCache.P3 = txtDataFi.Text
		MCache.P4 = txtObjectiu.Text
		MCache.P5 = txtDescripcionObjetivo.Text
		
		'CacheXecute "D GRAVPLANS^CRM"
		CacheXecute("D GRAVPLANSNOU^CRM")
		
		xMsgBox("Plans gravats correctament !!! ", MsgBoxStyle.Information, Me.Text)
		Inici()
	End Sub
	
	Private Sub cmdBorra_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdBorra.ClickEvent
		Dim Resp As Short
		Dim Enti As String
		Dim pla As Short
		CacheNetejaParametres()
		
		If txtEntidad.Text = "" Then Exit Sub
		If txtSucursal.Text = "" Then Exit Sub
		If txtResponsable.Text = "" Then Exit Sub
		
		MCache.P1 = txtEntidad.Text
		MCache.P2 = txtSucursal.Text
		MCache.P3 = txtResponsable.Text
		
		Resp = xMsgBox("�Voles eliminar la Entitat/Sucursal ?", MsgBoxStyle.Information + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, Me.Text)
		If Resp = MsgBoxResult.Yes Then
			CacheXecute("D ELIMENTIP^CRM")
			CarregaEntitats()
			txtEntidad.Text = ""
			Text1.Text = ""
			txtSucursal.Text = ""
			Text2.Text = ""
			txtResponsable.Text = ""
			Text4.Text = ""
		End If
	End Sub
	
	Private Sub CmdInserta_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdInserta.ClickEvent
		If Verifica = False Then Exit Sub
		CacheNetejaParametres()
		MCache.P1 = txtEntidad.Text
		MCache.P2 = txtSucursal.Text
		MCache.P3 = txtResponsable.Text
		MCache.P4 = txtContacto.Text
		
		'CacheXecute ("D GRAVPLANSW^CRM")
		CacheXecute("D GRAVPLANSWNOU^CRM")
		CarregaEntitats()
		txtEntidad.Text = ""
		Text1.Text = ""
		txtSucursal.Text = ""
		Text2.Text = ""
		txtResponsable.Text = ""
		Text4.Text = ""
		txtContacto.Text = ""
		Text3.Text = ""
	End Sub
	
	Private Sub frmMassiuPlans_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		
		ControlKey(Me, KeyAscii)
		
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmMassiuPlans_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		CmdInserta.Picture = SetIcon("Plus", 16)
		cmdBorra.Picture = SetIcon("Delete", 16)
		
	End Sub
	
	
	Private Sub frmMassiuPlans_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		CacheXecute("D BUIDAP^CRM")
	End Sub
	
	Private Sub GrdPlans_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdPlans.Click
		If GrdPlans.ActiveCell.Row = 0 Then Exit Sub
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtEntidad.Text = Piece(GrdPlans.Cell(GrdPlans.ActiveCell.Row, 1).Text, "|", 1)
		DisplayDescripcio(Me, txtEntidad)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtSucursal.Text = Piece(GrdPlans.Cell(GrdPlans.ActiveCell.Row, 1).Text, "|", 2)
		DisplayDescripcio(Me, txtSucursal, txtEntidad.Text & S & "S")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtResponsable.Text = Piece(GrdPlans.Cell(GrdPlans.ActiveCell.Row, 1).Text, "|", 3)
		DisplayDescripcio(Me, txtResponsable)
		
	End Sub
	
	
	
	Private Sub txtContacto_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtContacto.DoubleClick
		ConsultaTaula(Me, txtContacto,  , txtEntidad.Text & S & "S" & S & txtSucursal.Text & S & "C")
	End Sub
	
	Private Sub txtContacto_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtContacto.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtEntidad.Text & S & "S" & S & txtSucursal.Text & S & "C")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEntidad_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidad.LostFocus
		If txtEntidad.Text = "" Then Exit Sub
		CacheNetejaParametres()
		MCache.P1 = txtEntidad.Text
		txtSucursal.Text = CacheXecute("S VALUE=$$SUC^CRM($P(P1,S,1))")
	End Sub
	
	Private Sub txtObjectiu_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObjectiu.DoubleClick
		ConsultaTaula(Me, txtObjectiu)
	End Sub
	
	Private Sub txtObjectiu_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObjectiu.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	Private Sub txtDataFi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataFi.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtDataInici_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataInici.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtDescripcionObjetivo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcionObjetivo.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtEntidad_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidad.DoubleClick
		ConsultaTaula(Me, txtEntidad)
	End Sub
	
	Private Sub txtEntidad_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEntidad.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPla_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPla.DoubleClick
		ConsultaTaula(Me, txtPla)
	End Sub
	
	Private Sub txtPla_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPla.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	Private Sub txtResponsable_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.DoubleClick
		ConsultaTaula(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtResponsable.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSucursal_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.DoubleClick
		ConsultaTaula(Me, txtSucursal,  , txtEntidad.Text & S & "S")
	End Sub
	
	Private Sub txtSucursal_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSucursal.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtEntidad.Text & S & "S")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Sub CarregaEntitats()
		'CarregaFGrid GrdPlans, "ENTIP^CRM", txtPla
		CarregaFGrid(GrdPlans, "ENTIPNOU^CRM", txtPla.Text)
		GrdPlans.Column(1).Sort(FlexCell.SortEnum.Ascending)
	End Sub
	
	
	Private Function VerificaCamps() As Boolean
		VerificaCamps = True
		
		If txtPla.Text = "" Then
			xMsgBox("Falta informar el Pla", MsgBoxStyle.Information, Me.Text)
			txtPla.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If txtDataInici.Text = "" Then
			xMsgBox("Falta informar la Data Inici", MsgBoxStyle.Information, Me.Text)
			txtDataInici.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		
		'    If txtDataFi = "" Then
		'        xMsgBox "Falta informar la Data Fi", vbInformation, Me.Caption
		'        txtDataFi.SetFocus
		'        VerificaCamps = False
		'        Exit Function
		'    End If
		
		If txtObjectiu.Text = "" Then
			xMsgBox("Falta informar l'Objectiu", MsgBoxStyle.Information, Me.Text)
			txtObjectiu.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If txtDescripcionObjetivo.Text = "" Then
			xMsgBox("Falta informar la Descripci� de l'Objectiu", MsgBoxStyle.Information, Me.Text)
			txtDescripcionObjetivo.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If GrdPlans.Rows <= 1 Then
			xMsgBox("Falta entrar Entitats / Sucursals", MsgBoxStyle.Information, Me.Text)
			VerificaCamps = False
			Exit Function
		End If
		
		
		
	End Function
	
	Function Verifica() As Boolean
		Verifica = True
		
		If txtEntidad.Text = "" Then
			xMsgBox("Falta informat la Entitat", MsgBoxStyle.Information, Me.Text)
			txtEntidad.Focus()
			Verifica = False
			Exit Function
		End If
		
		If txtSucursal.Text = "" Then
			xMsgBox("Falta informat la Sucursal", MsgBoxStyle.Information, Me.Text)
			txtSucursal.Focus()
			Verifica = False
			Exit Function
		End If
		
		If txtResponsable.Text = "" Then
			xMsgBox("Falta informat el Responsable", MsgBoxStyle.Information, Me.Text)
			txtResponsable.Focus()
			Verifica = False
			Exit Function
		End If
		
		
	End Function
End Class
