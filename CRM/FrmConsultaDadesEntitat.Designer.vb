<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class FrmConsultaDadesEntitat
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents GrdInsercions As AxFlexCell.AxGrid
	Public WithEvents TabControlPage6 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdOfertes As AxFlexCell.AxGrid
	Public WithEvents TabControlPage5 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdParticipacio As AxFlexCell.AxGrid
	Public WithEvents TabControlPage4 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdObjectius As AxFlexCell.AxGrid
	Public WithEvents TabControlPage3 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdAccions As AxFlexCell.AxGrid
	Public WithEvents TabControlPage2 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents lblPLots As System.Windows.Forms.Label
	Public WithEvents lblILots As System.Windows.Forms.Label
	Public WithEvents Label24 As System.Windows.Forms.Label
	Public WithEvents lblCLots As System.Windows.Forms.Label
	Public WithEvents Line15 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lblCPstPLots As System.Windows.Forms.Label
	Public WithEvents lblCPstALots As System.Windows.Forms.Label
	Public WithEvents lblCPstPGarden As System.Windows.Forms.Label
	Public WithEvents lblCPstAManteniment As System.Windows.Forms.Label
	Public WithEvents lblCPstPManteniment As System.Windows.Forms.Label
	Public WithEvents lblCPstAJardineria As System.Windows.Forms.Label
	Public WithEvents lblCPstPJardineria As System.Windows.Forms.Label
	Public WithEvents lblCPstAImpremta As System.Windows.Forms.Label
	Public WithEvents lblCPstPImpremta As System.Windows.Forms.Label
	Public WithEvents lblCPstAManip As System.Windows.Forms.Label
	Public WithEvents lblCPstPManip As System.Windows.Forms.Label
	Public WithEvents lblCPstANeteja As System.Windows.Forms.Label
	Public WithEvents lblCPstPNeteja As System.Windows.Forms.Label
	Public WithEvents Label20 As System.Windows.Forms.Label
	Public WithEvents Label19 As System.Windows.Forms.Label
	Public WithEvents lblCConveni As System.Windows.Forms.Label
	Public WithEvents Label18 As System.Windows.Forms.Label
	Public WithEvents Line14 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lblCProveidor As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Line13 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lblCVi As System.Windows.Forms.Label
	Public WithEvents Line22 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line100 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lblCNeteja As System.Windows.Forms.Label
	Public WithEvents Line12 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line11 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line10 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line9 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line8 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line7 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line6 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line5 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line4 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line3 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Label17 As System.Windows.Forms.Label
	Public WithEvents Label16 As System.Windows.Forms.Label
	Public WithEvents Label15 As System.Windows.Forms.Label
	Public WithEvents Label14 As System.Windows.Forms.Label
	Public WithEvents Label13 As System.Windows.Forms.Label
	Public WithEvents Label12 As System.Windows.Forms.Label
	Public WithEvents Label11 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lblCManip As System.Windows.Forms.Label
	Public WithEvents lblCImpremta As System.Windows.Forms.Label
	Public WithEvents lblCJardineria As System.Windows.Forms.Label
	Public WithEvents lblCManteniment As System.Windows.Forms.Label
	Public WithEvents lblCGarden As System.Windows.Forms.Label
	Public WithEvents lblCCanonge As System.Windows.Forms.Label
	Public WithEvents lblCIncorpora As System.Windows.Forms.Label
	Public WithEvents lblCDonacioP As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents lblIDonacioP As System.Windows.Forms.Label
	Public WithEvents lblIIncorpora As System.Windows.Forms.Label
	Public WithEvents lblICanonge As System.Windows.Forms.Label
	Public WithEvents lblIGarden As System.Windows.Forms.Label
	Public WithEvents lblIManteniment As System.Windows.Forms.Label
	Public WithEvents lblIJardineria As System.Windows.Forms.Label
	Public WithEvents lblIImpremta As System.Windows.Forms.Label
	Public WithEvents lblINeteja As System.Windows.Forms.Label
	Public WithEvents lblIManip As System.Windows.Forms.Label
	Public WithEvents lblPDonacioP As System.Windows.Forms.Label
	Public WithEvents lblPIncorpora As System.Windows.Forms.Label
	Public WithEvents lblPCanonge As System.Windows.Forms.Label
	Public WithEvents lblPGarden As System.Windows.Forms.Label
	Public WithEvents lblPManteniment As System.Windows.Forms.Label
	Public WithEvents lblPJardineria As System.Windows.Forms.Label
	Public WithEvents lblPImpremta As System.Windows.Forms.Label
	Public WithEvents lblPNeteja As System.Windows.Forms.Label
	Public WithEvents lblPManip As System.Windows.Forms.Label
	Public WithEvents lblIVi As System.Windows.Forms.Label
	Public WithEvents lblPVi As System.Windows.Forms.Label
	Public WithEvents lblIDonantQ As System.Windows.Forms.Label
	Public WithEvents lblpDonantQ As System.Windows.Forms.Label
	Public WithEvents lblCDonantQ As System.Windows.Forms.Label
	Public WithEvents lblIEsponsor As System.Windows.Forms.Label
	Public WithEvents lblPEsponsor As System.Windows.Forms.Label
	Public WithEvents lblCEsponsor As System.Windows.Forms.Label
	Public WithEvents lblIVoluntariat As System.Windows.Forms.Label
	Public WithEvents lblPVoluntariat As System.Windows.Forms.Label
	Public WithEvents lblCVoluntariat As System.Windows.Forms.Label
	Public WithEvents TabControlPage1 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents TabControl1 As AxXtremeSuiteControls.AxTabControl
	Public WithEvents txtEntidad As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtSucursal As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents cmdExit As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdEntidad As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmConsultaDadesEntitat))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.TabControl1 = New AxXtremeSuiteControls.AxTabControl
		Me.TabControlPage6 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdInsercions = New AxFlexCell.AxGrid
		Me.TabControlPage5 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdOfertes = New AxFlexCell.AxGrid
		Me.TabControlPage4 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdParticipacio = New AxFlexCell.AxGrid
		Me.TabControlPage3 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdObjectius = New AxFlexCell.AxGrid
		Me.TabControlPage2 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdAccions = New AxFlexCell.AxGrid
		Me.TabControlPage1 = New AxXtremeSuiteControls.AxTabControlPage
		Me.lblPLots = New System.Windows.Forms.Label
		Me.lblILots = New System.Windows.Forms.Label
		Me.Label24 = New System.Windows.Forms.Label
		Me.lblCLots = New System.Windows.Forms.Label
		Me.Line15 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lblCPstPLots = New System.Windows.Forms.Label
		Me.lblCPstALots = New System.Windows.Forms.Label
		Me.lblCPstPGarden = New System.Windows.Forms.Label
		Me.lblCPstAManteniment = New System.Windows.Forms.Label
		Me.lblCPstPManteniment = New System.Windows.Forms.Label
		Me.lblCPstAJardineria = New System.Windows.Forms.Label
		Me.lblCPstPJardineria = New System.Windows.Forms.Label
		Me.lblCPstAImpremta = New System.Windows.Forms.Label
		Me.lblCPstPImpremta = New System.Windows.Forms.Label
		Me.lblCPstAManip = New System.Windows.Forms.Label
		Me.lblCPstPManip = New System.Windows.Forms.Label
		Me.lblCPstANeteja = New System.Windows.Forms.Label
		Me.lblCPstPNeteja = New System.Windows.Forms.Label
		Me.Label20 = New System.Windows.Forms.Label
		Me.Label19 = New System.Windows.Forms.Label
		Me.lblCConveni = New System.Windows.Forms.Label
		Me.Label18 = New System.Windows.Forms.Label
		Me.Line14 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lblCProveidor = New System.Windows.Forms.Label
		Me.Label10 = New System.Windows.Forms.Label
		Me.Line13 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lblCVi = New System.Windows.Forms.Label
		Me.Line22 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line100 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lblCNeteja = New System.Windows.Forms.Label
		Me.Line12 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line11 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line10 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line9 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line8 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line7 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line6 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line5 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line4 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line3 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Label17 = New System.Windows.Forms.Label
		Me.Label16 = New System.Windows.Forms.Label
		Me.Label15 = New System.Windows.Forms.Label
		Me.Label14 = New System.Windows.Forms.Label
		Me.Label13 = New System.Windows.Forms.Label
		Me.Label12 = New System.Windows.Forms.Label
		Me.Label11 = New System.Windows.Forms.Label
		Me.Label9 = New System.Windows.Forms.Label
		Me.Label8 = New System.Windows.Forms.Label
		Me.Label7 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lblCManip = New System.Windows.Forms.Label
		Me.lblCImpremta = New System.Windows.Forms.Label
		Me.lblCJardineria = New System.Windows.Forms.Label
		Me.lblCManteniment = New System.Windows.Forms.Label
		Me.lblCGarden = New System.Windows.Forms.Label
		Me.lblCCanonge = New System.Windows.Forms.Label
		Me.lblCIncorpora = New System.Windows.Forms.Label
		Me.lblCDonacioP = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.lblIDonacioP = New System.Windows.Forms.Label
		Me.lblIIncorpora = New System.Windows.Forms.Label
		Me.lblICanonge = New System.Windows.Forms.Label
		Me.lblIGarden = New System.Windows.Forms.Label
		Me.lblIManteniment = New System.Windows.Forms.Label
		Me.lblIJardineria = New System.Windows.Forms.Label
		Me.lblIImpremta = New System.Windows.Forms.Label
		Me.lblINeteja = New System.Windows.Forms.Label
		Me.lblIManip = New System.Windows.Forms.Label
		Me.lblPDonacioP = New System.Windows.Forms.Label
		Me.lblPIncorpora = New System.Windows.Forms.Label
		Me.lblPCanonge = New System.Windows.Forms.Label
		Me.lblPGarden = New System.Windows.Forms.Label
		Me.lblPManteniment = New System.Windows.Forms.Label
		Me.lblPJardineria = New System.Windows.Forms.Label
		Me.lblPImpremta = New System.Windows.Forms.Label
		Me.lblPNeteja = New System.Windows.Forms.Label
		Me.lblPManip = New System.Windows.Forms.Label
		Me.lblIVi = New System.Windows.Forms.Label
		Me.lblPVi = New System.Windows.Forms.Label
		Me.lblIDonantQ = New System.Windows.Forms.Label
		Me.lblpDonantQ = New System.Windows.Forms.Label
		Me.lblCDonantQ = New System.Windows.Forms.Label
		Me.lblIEsponsor = New System.Windows.Forms.Label
		Me.lblPEsponsor = New System.Windows.Forms.Label
		Me.lblCEsponsor = New System.Windows.Forms.Label
		Me.lblIVoluntariat = New System.Windows.Forms.Label
		Me.lblPVoluntariat = New System.Windows.Forms.Label
		Me.lblCVoluntariat = New System.Windows.Forms.Label
		Me.txtEntidad = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtSucursal = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.cmdExit = New AxXtremeSuiteControls.AxPushButton
		Me.cmdEntidad = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.TabControl1.SuspendLayout()
		Me.TabControlPage6.SuspendLayout()
		Me.TabControlPage5.SuspendLayout()
		Me.TabControlPage4.SuspendLayout()
		Me.TabControlPage3.SuspendLayout()
		Me.TabControlPage2.SuspendLayout()
		Me.TabControlPage1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.GrdInsercions, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage6, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdOfertes, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage5, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdParticipacio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage4, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdObjectius, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdAccions, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdExit, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdEntidad, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Dades Entitat/Sucursal"
		Me.ClientSize = New System.Drawing.Size(1120, 572)
		Me.Location = New System.Drawing.Point(115, 243)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "FrmConsultaDadesEntitat"
		TabControl1.OcxState = CType(resources.GetObject("TabControl1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControl1.Size = New System.Drawing.Size(1113, 439)
		Me.TabControl1.Location = New System.Drawing.Point(4, 74)
		Me.TabControl1.TabIndex = 7
		Me.TabControl1.Name = "TabControl1"
		TabControlPage6.OcxState = CType(resources.GetObject("TabControlPage6.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage6.Size = New System.Drawing.Size(1109, 415)
		Me.TabControlPage6.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage6.TabIndex = 95
		Me.TabControlPage6.Visible = False
		Me.TabControlPage6.Name = "TabControlPage6"
		GrdInsercions.OcxState = CType(resources.GetObject("GrdInsercions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdInsercions.Size = New System.Drawing.Size(1094, 398)
		Me.GrdInsercions.Location = New System.Drawing.Point(8, 8)
		Me.GrdInsercions.TabIndex = 97
		Me.GrdInsercions.Name = "GrdInsercions"
		TabControlPage5.OcxState = CType(resources.GetObject("TabControlPage5.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage5.Size = New System.Drawing.Size(1109, 415)
		Me.TabControlPage5.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage5.TabIndex = 94
		Me.TabControlPage5.Visible = False
		Me.TabControlPage5.Name = "TabControlPage5"
		GrdOfertes.OcxState = CType(resources.GetObject("GrdOfertes.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdOfertes.Size = New System.Drawing.Size(1094, 398)
		Me.GrdOfertes.Location = New System.Drawing.Point(8, 8)
		Me.GrdOfertes.TabIndex = 96
		Me.GrdOfertes.Name = "GrdOfertes"
		TabControlPage4.OcxState = CType(resources.GetObject("TabControlPage4.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage4.Size = New System.Drawing.Size(1109, 415)
		Me.TabControlPage4.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage4.TabIndex = 11
		Me.TabControlPage4.Visible = False
		Me.TabControlPage4.Name = "TabControlPage4"
		GrdParticipacio.OcxState = CType(resources.GetObject("GrdParticipacio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdParticipacio.Size = New System.Drawing.Size(1094, 398)
		Me.GrdParticipacio.Location = New System.Drawing.Point(8, 8)
		Me.GrdParticipacio.TabIndex = 14
		Me.GrdParticipacio.Name = "GrdParticipacio"
		TabControlPage3.OcxState = CType(resources.GetObject("TabControlPage3.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage3.Size = New System.Drawing.Size(1109, 415)
		Me.TabControlPage3.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage3.TabIndex = 10
		Me.TabControlPage3.Visible = False
		Me.TabControlPage3.Name = "TabControlPage3"
		GrdObjectius.OcxState = CType(resources.GetObject("GrdObjectius.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdObjectius.Size = New System.Drawing.Size(1094, 398)
		Me.GrdObjectius.Location = New System.Drawing.Point(8, 8)
		Me.GrdObjectius.TabIndex = 13
		Me.GrdObjectius.Name = "GrdObjectius"
		TabControlPage2.OcxState = CType(resources.GetObject("TabControlPage2.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage2.Size = New System.Drawing.Size(1109, 415)
		Me.TabControlPage2.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage2.TabIndex = 9
		Me.TabControlPage2.Visible = False
		Me.TabControlPage2.Name = "TabControlPage2"
		GrdAccions.OcxState = CType(resources.GetObject("GrdAccions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdAccions.Size = New System.Drawing.Size(1094, 398)
		Me.GrdAccions.Location = New System.Drawing.Point(8, 8)
		Me.GrdAccions.TabIndex = 12
		Me.GrdAccions.Name = "GrdAccions"
		TabControlPage1.OcxState = CType(resources.GetObject("TabControlPage1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage1.Size = New System.Drawing.Size(1109, 415)
		Me.TabControlPage1.Location = New System.Drawing.Point(2, 22)
		Me.TabControlPage1.TabIndex = 8
		Me.TabControlPage1.Name = "TabControlPage1"
		Me.lblPLots.Text = "CLots"
		Me.lblPLots.Size = New System.Drawing.Size(55, 19)
		Me.lblPLots.Location = New System.Drawing.Point(248, 244)
		Me.lblPLots.TabIndex = 93
		Me.lblPLots.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPLots.BackColor = System.Drawing.SystemColors.Control
		Me.lblPLots.Enabled = True
		Me.lblPLots.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPLots.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPLots.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPLots.UseMnemonic = True
		Me.lblPLots.Visible = True
		Me.lblPLots.AutoSize = False
		Me.lblPLots.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPLots.Name = "lblPLots"
		Me.lblILots.Text = "CLots"
		Me.lblILots.Size = New System.Drawing.Size(55, 19)
		Me.lblILots.Location = New System.Drawing.Point(132, 244)
		Me.lblILots.TabIndex = 92
		Me.lblILots.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblILots.BackColor = System.Drawing.SystemColors.Control
		Me.lblILots.Enabled = True
		Me.lblILots.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblILots.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblILots.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblILots.UseMnemonic = True
		Me.lblILots.Visible = True
		Me.lblILots.AutoSize = False
		Me.lblILots.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblILots.Name = "lblILots"
		Me.Label24.Text = "Lots"
		Me.Label24.Size = New System.Drawing.Size(57, 15)
		Me.Label24.Location = New System.Drawing.Point(26, 244)
		Me.Label24.TabIndex = 91
		Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label24.BackColor = System.Drawing.SystemColors.Control
		Me.Label24.Enabled = True
		Me.Label24.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label24.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label24.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label24.UseMnemonic = True
		Me.Label24.Visible = True
		Me.Label24.AutoSize = False
		Me.Label24.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label24.Name = "Label24"
		Me.lblCLots.Text = "CLots"
		Me.lblCLots.Size = New System.Drawing.Size(55, 15)
		Me.lblCLots.Location = New System.Drawing.Point(580, 246)
		Me.lblCLots.TabIndex = 90
		Me.lblCLots.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCLots.BackColor = System.Drawing.SystemColors.Control
		Me.lblCLots.Enabled = True
		Me.lblCLots.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCLots.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCLots.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCLots.UseMnemonic = True
		Me.lblCLots.Visible = True
		Me.lblCLots.AutoSize = False
		Me.lblCLots.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCLots.Name = "lblCLots"
		Me.Line15.X1 = 24
		Me.Line15.X2 = 666
		Me.Line15.Y1 = 264
		Me.Line15.Y2 = 264
		Me.Line15.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line15.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line15.BorderWidth = 1
		Me.Line15.Visible = True
		Me.Line15.Name = "Line15"
		Me.lblCPstPLots.Text = "PLots"
		Me.lblCPstPLots.Size = New System.Drawing.Size(55, 15)
		Me.lblCPstPLots.Location = New System.Drawing.Point(366, 244)
		Me.lblCPstPLots.TabIndex = 89
		Me.lblCPstPLots.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstPLots.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstPLots.Enabled = True
		Me.lblCPstPLots.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstPLots.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstPLots.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstPLots.UseMnemonic = True
		Me.lblCPstPLots.Visible = True
		Me.lblCPstPLots.AutoSize = False
		Me.lblCPstPLots.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstPLots.Name = "lblCPstPLots"
		Me.lblCPstALots.Text = "ALots"
		Me.lblCPstALots.Size = New System.Drawing.Size(55, 15)
		Me.lblCPstALots.Location = New System.Drawing.Point(468, 244)
		Me.lblCPstALots.TabIndex = 88
		Me.lblCPstALots.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstALots.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstALots.Enabled = True
		Me.lblCPstALots.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstALots.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstALots.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstALots.UseMnemonic = True
		Me.lblCPstALots.Visible = True
		Me.lblCPstALots.AutoSize = False
		Me.lblCPstALots.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstALots.Name = "lblCPstALots"
		Me.lblCPstPGarden.Text = "PManteniment"
		Me.lblCPstPGarden.Size = New System.Drawing.Size(55, 17)
		Me.lblCPstPGarden.Location = New System.Drawing.Point(366, 174)
		Me.lblCPstPGarden.TabIndex = 86
		Me.lblCPstPGarden.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstPGarden.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstPGarden.Enabled = True
		Me.lblCPstPGarden.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstPGarden.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstPGarden.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstPGarden.UseMnemonic = True
		Me.lblCPstPGarden.Visible = True
		Me.lblCPstPGarden.AutoSize = False
		Me.lblCPstPGarden.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstPGarden.Name = "lblCPstPGarden"
		Me.lblCPstAManteniment.Text = "AManteniment"
		Me.lblCPstAManteniment.Size = New System.Drawing.Size(55, 17)
		Me.lblCPstAManteniment.Location = New System.Drawing.Point(470, 152)
		Me.lblCPstAManteniment.TabIndex = 85
		Me.lblCPstAManteniment.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstAManteniment.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstAManteniment.Enabled = True
		Me.lblCPstAManteniment.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstAManteniment.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstAManteniment.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstAManteniment.UseMnemonic = True
		Me.lblCPstAManteniment.Visible = True
		Me.lblCPstAManteniment.AutoSize = False
		Me.lblCPstAManteniment.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstAManteniment.Name = "lblCPstAManteniment"
		Me.lblCPstPManteniment.Text = "PManteniment"
		Me.lblCPstPManteniment.Size = New System.Drawing.Size(55, 17)
		Me.lblCPstPManteniment.Location = New System.Drawing.Point(366, 152)
		Me.lblCPstPManteniment.TabIndex = 84
		Me.lblCPstPManteniment.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstPManteniment.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstPManteniment.Enabled = True
		Me.lblCPstPManteniment.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstPManteniment.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstPManteniment.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstPManteniment.UseMnemonic = True
		Me.lblCPstPManteniment.Visible = True
		Me.lblCPstPManteniment.AutoSize = False
		Me.lblCPstPManteniment.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstPManteniment.Name = "lblCPstPManteniment"
		Me.lblCPstAJardineria.Text = "AJardineria"
		Me.lblCPstAJardineria.Size = New System.Drawing.Size(55, 19)
		Me.lblCPstAJardineria.Location = New System.Drawing.Point(470, 128)
		Me.lblCPstAJardineria.TabIndex = 83
		Me.lblCPstAJardineria.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstAJardineria.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstAJardineria.Enabled = True
		Me.lblCPstAJardineria.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstAJardineria.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstAJardineria.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstAJardineria.UseMnemonic = True
		Me.lblCPstAJardineria.Visible = True
		Me.lblCPstAJardineria.AutoSize = False
		Me.lblCPstAJardineria.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstAJardineria.Name = "lblCPstAJardineria"
		Me.lblCPstPJardineria.Text = "PJardineria"
		Me.lblCPstPJardineria.Size = New System.Drawing.Size(55, 19)
		Me.lblCPstPJardineria.Location = New System.Drawing.Point(366, 128)
		Me.lblCPstPJardineria.TabIndex = 82
		Me.lblCPstPJardineria.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstPJardineria.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstPJardineria.Enabled = True
		Me.lblCPstPJardineria.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstPJardineria.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstPJardineria.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstPJardineria.UseMnemonic = True
		Me.lblCPstPJardineria.Visible = True
		Me.lblCPstPJardineria.AutoSize = False
		Me.lblCPstPJardineria.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstPJardineria.Name = "lblCPstPJardineria"
		Me.lblCPstAImpremta.Text = "AImpremta"
		Me.lblCPstAImpremta.Size = New System.Drawing.Size(55, 19)
		Me.lblCPstAImpremta.Location = New System.Drawing.Point(470, 104)
		Me.lblCPstAImpremta.TabIndex = 81
		Me.lblCPstAImpremta.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstAImpremta.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstAImpremta.Enabled = True
		Me.lblCPstAImpremta.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstAImpremta.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstAImpremta.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstAImpremta.UseMnemonic = True
		Me.lblCPstAImpremta.Visible = True
		Me.lblCPstAImpremta.AutoSize = False
		Me.lblCPstAImpremta.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstAImpremta.Name = "lblCPstAImpremta"
		Me.lblCPstPImpremta.Text = "PImpremta"
		Me.lblCPstPImpremta.Size = New System.Drawing.Size(55, 19)
		Me.lblCPstPImpremta.Location = New System.Drawing.Point(366, 104)
		Me.lblCPstPImpremta.TabIndex = 80
		Me.lblCPstPImpremta.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstPImpremta.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstPImpremta.Enabled = True
		Me.lblCPstPImpremta.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstPImpremta.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstPImpremta.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstPImpremta.UseMnemonic = True
		Me.lblCPstPImpremta.Visible = True
		Me.lblCPstPImpremta.AutoSize = False
		Me.lblCPstPImpremta.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstPImpremta.Name = "lblCPstPImpremta"
		Me.lblCPstAManip.Text = "AManip"
		Me.lblCPstAManip.Size = New System.Drawing.Size(55, 19)
		Me.lblCPstAManip.Location = New System.Drawing.Point(470, 80)
		Me.lblCPstAManip.TabIndex = 79
		Me.lblCPstAManip.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstAManip.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstAManip.Enabled = True
		Me.lblCPstAManip.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstAManip.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstAManip.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstAManip.UseMnemonic = True
		Me.lblCPstAManip.Visible = True
		Me.lblCPstAManip.AutoSize = False
		Me.lblCPstAManip.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstAManip.Name = "lblCPstAManip"
		Me.lblCPstPManip.Text = "PManip"
		Me.lblCPstPManip.Size = New System.Drawing.Size(55, 19)
		Me.lblCPstPManip.Location = New System.Drawing.Point(368, 80)
		Me.lblCPstPManip.TabIndex = 78
		Me.lblCPstPManip.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstPManip.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstPManip.Enabled = True
		Me.lblCPstPManip.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstPManip.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstPManip.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstPManip.UseMnemonic = True
		Me.lblCPstPManip.Visible = True
		Me.lblCPstPManip.AutoSize = False
		Me.lblCPstPManip.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstPManip.Name = "lblCPstPManip"
		Me.lblCPstANeteja.Text = "ANeteja"
		Me.lblCPstANeteja.Size = New System.Drawing.Size(55, 15)
		Me.lblCPstANeteja.Location = New System.Drawing.Point(468, 56)
		Me.lblCPstANeteja.TabIndex = 77
		Me.lblCPstANeteja.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstANeteja.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstANeteja.Enabled = True
		Me.lblCPstANeteja.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstANeteja.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstANeteja.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstANeteja.UseMnemonic = True
		Me.lblCPstANeteja.Visible = True
		Me.lblCPstANeteja.AutoSize = False
		Me.lblCPstANeteja.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstANeteja.Name = "lblCPstANeteja"
		Me.lblCPstPNeteja.Text = "PNeteja"
		Me.lblCPstPNeteja.Size = New System.Drawing.Size(55, 15)
		Me.lblCPstPNeteja.Location = New System.Drawing.Point(366, 56)
		Me.lblCPstPNeteja.TabIndex = 76
		Me.lblCPstPNeteja.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCPstPNeteja.BackColor = System.Drawing.SystemColors.Control
		Me.lblCPstPNeteja.Enabled = True
		Me.lblCPstPNeteja.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCPstPNeteja.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCPstPNeteja.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCPstPNeteja.UseMnemonic = True
		Me.lblCPstPNeteja.Visible = True
		Me.lblCPstPNeteja.AutoSize = False
		Me.lblCPstPNeteja.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCPstPNeteja.Name = "lblCPstPNeteja"
		Me.Label20.Text = "Pst. Acceptats/ Comanda"
		Me.Label20.Size = New System.Drawing.Size(91, 31)
		Me.Label20.Location = New System.Drawing.Point(450, 20)
		Me.Label20.TabIndex = 75
		Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label20.BackColor = System.Drawing.SystemColors.Control
		Me.Label20.Enabled = True
		Me.Label20.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label20.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label20.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label20.UseMnemonic = True
		Me.Label20.Visible = True
		Me.Label20.AutoSize = False
		Me.Label20.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label20.Name = "Label20"
		Me.Label19.Text = "Pst. Pendents"
		Me.Label19.Size = New System.Drawing.Size(87, 15)
		Me.Label19.Location = New System.Drawing.Point(348, 20)
		Me.Label19.TabIndex = 74
		Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label19.BackColor = System.Drawing.SystemColors.Control
		Me.Label19.Enabled = True
		Me.Label19.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label19.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label19.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label19.UseMnemonic = True
		Me.Label19.Visible = True
		Me.Label19.AutoSize = False
		Me.Label19.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label19.Name = "Label19"
		Me.lblCConveni.Text = "CConv"
		Me.lblCConveni.Size = New System.Drawing.Size(59, 13)
		Me.lblCConveni.Location = New System.Drawing.Point(582, 392)
		Me.lblCConveni.TabIndex = 73
		Me.lblCConveni.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCConveni.BackColor = System.Drawing.SystemColors.Control
		Me.lblCConveni.Enabled = True
		Me.lblCConveni.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCConveni.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCConveni.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCConveni.UseMnemonic = True
		Me.lblCConveni.Visible = True
		Me.lblCConveni.AutoSize = False
		Me.lblCConveni.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCConveni.Name = "lblCConveni"
		Me.Label18.Text = "Conveni"
		Me.Label18.Size = New System.Drawing.Size(95, 15)
		Me.Label18.Location = New System.Drawing.Point(24, 392)
		Me.Label18.TabIndex = 72
		Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label18.BackColor = System.Drawing.SystemColors.Control
		Me.Label18.Enabled = True
		Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label18.UseMnemonic = True
		Me.Label18.Visible = True
		Me.Label18.AutoSize = False
		Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label18.Name = "Label18"
		Me.Line14.X1 = 24
		Me.Line14.X2 = 666
		Me.Line14.Y1 = 386
		Me.Line14.Y2 = 386
		Me.Line14.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line14.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line14.BorderWidth = 1
		Me.Line14.Visible = True
		Me.Line14.Name = "Line14"
		Me.lblCProveidor.Text = "CProve"
		Me.lblCProveidor.Size = New System.Drawing.Size(63, 13)
		Me.lblCProveidor.Location = New System.Drawing.Point(582, 368)
		Me.lblCProveidor.TabIndex = 71
		Me.lblCProveidor.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCProveidor.BackColor = System.Drawing.SystemColors.Control
		Me.lblCProveidor.Enabled = True
		Me.lblCProveidor.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCProveidor.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCProveidor.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCProveidor.UseMnemonic = True
		Me.lblCProveidor.Visible = True
		Me.lblCProveidor.AutoSize = False
		Me.lblCProveidor.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCProveidor.Name = "lblCProveidor"
		Me.Label10.Text = "Prove�dor"
		Me.Label10.Size = New System.Drawing.Size(95, 15)
		Me.Label10.Location = New System.Drawing.Point(24, 368)
		Me.Label10.TabIndex = 70
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label10.BackColor = System.Drawing.SystemColors.Control
		Me.Label10.Enabled = True
		Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label10.UseMnemonic = True
		Me.Label10.Visible = True
		Me.Label10.AutoSize = False
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label10.Name = "Label10"
		Me.Line13.X1 = 24
		Me.Line13.X2 = 666
		Me.Line13.Y1 = 362
		Me.Line13.Y2 = 362
		Me.Line13.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line13.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line13.BorderWidth = 1
		Me.Line13.Visible = True
		Me.Line13.Name = "Line13"
		Me.lblCVi.Text = "CVi"
		Me.lblCVi.Size = New System.Drawing.Size(63, 15)
		Me.lblCVi.Location = New System.Drawing.Point(580, 222)
		Me.lblCVi.TabIndex = 69
		Me.lblCVi.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCVi.BackColor = System.Drawing.SystemColors.Control
		Me.lblCVi.Enabled = True
		Me.lblCVi.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCVi.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCVi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCVi.UseMnemonic = True
		Me.lblCVi.Visible = True
		Me.lblCVi.AutoSize = False
		Me.lblCVi.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCVi.Name = "lblCVi"
		Me.Line22.X1 = 24
		Me.Line22.X2 = 666
		Me.Line22.Y1 = 52
		Me.Line22.Y2 = 52
		Me.Line22.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line22.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line22.BorderWidth = 1
		Me.Line22.Visible = True
		Me.Line22.Name = "Line22"
		Me.Line100.X1 = 24
		Me.Line100.X2 = 666
		Me.Line100.Y1 = 76
		Me.Line100.Y2 = 76
		Me.Line100.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line100.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line100.BorderWidth = 1
		Me.Line100.Visible = True
		Me.Line100.Name = "Line100"
		Me.lblCNeteja.Text = "CNeteja"
		Me.lblCNeteja.Size = New System.Drawing.Size(55, 15)
		Me.lblCNeteja.Location = New System.Drawing.Point(580, 58)
		Me.lblCNeteja.TabIndex = 68
		Me.lblCNeteja.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCNeteja.BackColor = System.Drawing.SystemColors.Control
		Me.lblCNeteja.Enabled = True
		Me.lblCNeteja.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCNeteja.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCNeteja.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCNeteja.UseMnemonic = True
		Me.lblCNeteja.Visible = True
		Me.lblCNeteja.AutoSize = False
		Me.lblCNeteja.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCNeteja.Name = "lblCNeteja"
		Me.Line12.X1 = 24
		Me.Line12.X2 = 666
		Me.Line12.Y1 = 338
		Me.Line12.Y2 = 338
		Me.Line12.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line12.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line12.BorderWidth = 1
		Me.Line12.Visible = True
		Me.Line12.Name = "Line12"
		Me.Line11.X1 = 24
		Me.Line11.X2 = 666
		Me.Line11.Y1 = 314
		Me.Line11.Y2 = 314
		Me.Line11.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line11.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line11.BorderWidth = 1
		Me.Line11.Visible = True
		Me.Line11.Name = "Line11"
		Me.Line10.X1 = 24
		Me.Line10.X2 = 666
		Me.Line10.Y1 = 290
		Me.Line10.Y2 = 290
		Me.Line10.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line10.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line10.BorderWidth = 1
		Me.Line10.Visible = True
		Me.Line10.Name = "Line10"
		Me.Line9.X1 = 24
		Me.Line9.X2 = 666
		Me.Line9.Y1 = 240
		Me.Line9.Y2 = 240
		Me.Line9.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line9.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line9.BorderWidth = 1
		Me.Line9.Visible = True
		Me.Line9.Name = "Line9"
		Me.Line8.X1 = 24
		Me.Line8.X2 = 666
		Me.Line8.Y1 = 218
		Me.Line8.Y2 = 218
		Me.Line8.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line8.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line8.BorderWidth = 1
		Me.Line8.Visible = True
		Me.Line8.Name = "Line8"
		Me.Line7.X1 = 24
		Me.Line7.X2 = 666
		Me.Line7.Y1 = 194
		Me.Line7.Y2 = 194
		Me.Line7.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line7.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line7.BorderWidth = 1
		Me.Line7.Visible = True
		Me.Line7.Name = "Line7"
		Me.Line6.X1 = 24
		Me.Line6.X2 = 666
		Me.Line6.Y1 = 170
		Me.Line6.Y2 = 170
		Me.Line6.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line6.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line6.BorderWidth = 1
		Me.Line6.Visible = True
		Me.Line6.Name = "Line6"
		Me.Line5.X1 = 24
		Me.Line5.X2 = 666
		Me.Line5.Y1 = 148
		Me.Line5.Y2 = 148
		Me.Line5.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line5.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line5.BorderWidth = 1
		Me.Line5.Visible = True
		Me.Line5.Name = "Line5"
		Me.Line4.X1 = 24
		Me.Line4.X2 = 666
		Me.Line4.Y1 = 124
		Me.Line4.Y2 = 124
		Me.Line4.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line4.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line4.BorderWidth = 1
		Me.Line4.Visible = True
		Me.Line4.Name = "Line4"
		Me.Line3.X1 = 24
		Me.Line3.X2 = 666
		Me.Line3.Y1 = 100
		Me.Line3.Y2 = 100
		Me.Line3.BorderColor = System.Drawing.SystemColors.WindowText
		Me.Line3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line3.BorderWidth = 1
		Me.Line3.Visible = True
		Me.Line3.Name = "Line3"
		Me.Label17.Text = "Voluntariat"
		Me.Label17.Size = New System.Drawing.Size(95, 15)
		Me.Label17.Location = New System.Drawing.Point(24, 342)
		Me.Label17.TabIndex = 67
		Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label17.BackColor = System.Drawing.SystemColors.Control
		Me.Label17.Enabled = True
		Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label17.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label17.UseMnemonic = True
		Me.Label17.Visible = True
		Me.Label17.AutoSize = False
		Me.Label17.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label17.Name = "Label17"
		Me.Label16.Text = "Esponsor"
		Me.Label16.Size = New System.Drawing.Size(95, 15)
		Me.Label16.Location = New System.Drawing.Point(24, 320)
		Me.Label16.TabIndex = 66
		Me.Label16.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label16.BackColor = System.Drawing.SystemColors.Control
		Me.Label16.Enabled = True
		Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label16.UseMnemonic = True
		Me.Label16.Visible = True
		Me.Label16.AutoSize = False
		Me.Label16.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label16.Name = "Label16"
		Me.Label15.Text = "Donaci� puntual"
		Me.Label15.Size = New System.Drawing.Size(95, 15)
		Me.Label15.Location = New System.Drawing.Point(24, 296)
		Me.Label15.TabIndex = 65
		Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label15.BackColor = System.Drawing.SystemColors.Control
		Me.Label15.Enabled = True
		Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label15.UseMnemonic = True
		Me.Label15.Visible = True
		Me.Label15.AutoSize = False
		Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label15.Name = "Label15"
		Me.Label14.Text = "Donant-quota"
		Me.Label14.Size = New System.Drawing.Size(85, 15)
		Me.Label14.Location = New System.Drawing.Point(24, 272)
		Me.Label14.TabIndex = 64
		Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label14.BackColor = System.Drawing.SystemColors.Control
		Me.Label14.Enabled = True
		Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label14.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label14.UseMnemonic = True
		Me.Label14.Visible = True
		Me.Label14.AutoSize = False
		Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label14.Name = "Label14"
		Me.Label13.Text = "Vi"
		Me.Label13.Size = New System.Drawing.Size(75, 15)
		Me.Label13.Location = New System.Drawing.Point(24, 222)
		Me.Label13.TabIndex = 63
		Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label13.BackColor = System.Drawing.SystemColors.Control
		Me.Label13.Enabled = True
		Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label13.UseMnemonic = True
		Me.Label13.Visible = True
		Me.Label13.AutoSize = False
		Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label13.Name = "Label13"
		Me.Label12.Text = "Canonge"
		Me.Label12.Size = New System.Drawing.Size(75, 15)
		Me.Label12.Location = New System.Drawing.Point(24, 198)
		Me.Label12.TabIndex = 62
		Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label12.BackColor = System.Drawing.SystemColors.Control
		Me.Label12.Enabled = True
		Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label12.UseMnemonic = True
		Me.Label12.Visible = True
		Me.Label12.AutoSize = False
		Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label12.Name = "Label12"
		Me.Label11.Text = "Garden"
		Me.Label11.Size = New System.Drawing.Size(75, 15)
		Me.Label11.Location = New System.Drawing.Point(24, 174)
		Me.Label11.TabIndex = 61
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label11.BackColor = System.Drawing.SystemColors.Control
		Me.Label11.Enabled = True
		Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label11.UseMnemonic = True
		Me.Label11.Visible = True
		Me.Label11.AutoSize = False
		Me.Label11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label11.Name = "Label11"
		Me.Label9.Text = "Manteniment"
		Me.Label9.Size = New System.Drawing.Size(75, 15)
		Me.Label9.Location = New System.Drawing.Point(26, 152)
		Me.Label9.TabIndex = 60
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label9.BackColor = System.Drawing.SystemColors.Control
		Me.Label9.Enabled = True
		Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.UseMnemonic = True
		Me.Label9.Visible = True
		Me.Label9.AutoSize = False
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.Name = "Label9"
		Me.Label8.Text = "Jardineria"
		Me.Label8.Size = New System.Drawing.Size(65, 15)
		Me.Label8.Location = New System.Drawing.Point(26, 128)
		Me.Label8.TabIndex = 59
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label8.BackColor = System.Drawing.SystemColors.Control
		Me.Label8.Enabled = True
		Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.UseMnemonic = True
		Me.Label8.Visible = True
		Me.Label8.AutoSize = False
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.Name = "Label8"
		Me.Label7.Text = "Impremta"
		Me.Label7.Size = New System.Drawing.Size(65, 15)
		Me.Label7.Location = New System.Drawing.Point(26, 104)
		Me.Label7.TabIndex = 58
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label7.BackColor = System.Drawing.SystemColors.Control
		Me.Label7.Enabled = True
		Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.UseMnemonic = True
		Me.Label7.Visible = True
		Me.Label7.AutoSize = False
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.Name = "Label7"
		Me.Label3.Text = "Manipulats"
		Me.Label3.Size = New System.Drawing.Size(65, 15)
		Me.Label3.Location = New System.Drawing.Point(26, 80)
		Me.Label3.TabIndex = 57
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Neteja"
		Me.Label2.Size = New System.Drawing.Size(57, 15)
		Me.Label2.Location = New System.Drawing.Point(26, 56)
		Me.Label2.TabIndex = 56
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "E. Inseridora"
		Me.Label1.Size = New System.Drawing.Size(73, 15)
		Me.Label1.Location = New System.Drawing.Point(26, 30)
		Me.Label1.TabIndex = 55
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lblCManip.Text = "CManip"
		Me.lblCManip.Size = New System.Drawing.Size(55, 19)
		Me.lblCManip.Location = New System.Drawing.Point(580, 84)
		Me.lblCManip.TabIndex = 54
		Me.lblCManip.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCManip.BackColor = System.Drawing.SystemColors.Control
		Me.lblCManip.Enabled = True
		Me.lblCManip.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCManip.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCManip.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCManip.UseMnemonic = True
		Me.lblCManip.Visible = True
		Me.lblCManip.AutoSize = False
		Me.lblCManip.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCManip.Name = "lblCManip"
		Me.lblCImpremta.Text = "CImpremta"
		Me.lblCImpremta.Size = New System.Drawing.Size(55, 19)
		Me.lblCImpremta.Location = New System.Drawing.Point(580, 106)
		Me.lblCImpremta.TabIndex = 53
		Me.lblCImpremta.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCImpremta.BackColor = System.Drawing.SystemColors.Control
		Me.lblCImpremta.Enabled = True
		Me.lblCImpremta.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCImpremta.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCImpremta.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCImpremta.UseMnemonic = True
		Me.lblCImpremta.Visible = True
		Me.lblCImpremta.AutoSize = False
		Me.lblCImpremta.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCImpremta.Name = "lblCImpremta"
		Me.lblCJardineria.Text = "CJardineria"
		Me.lblCJardineria.Size = New System.Drawing.Size(55, 19)
		Me.lblCJardineria.Location = New System.Drawing.Point(580, 130)
		Me.lblCJardineria.TabIndex = 52
		Me.lblCJardineria.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCJardineria.BackColor = System.Drawing.SystemColors.Control
		Me.lblCJardineria.Enabled = True
		Me.lblCJardineria.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCJardineria.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCJardineria.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCJardineria.UseMnemonic = True
		Me.lblCJardineria.Visible = True
		Me.lblCJardineria.AutoSize = False
		Me.lblCJardineria.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCJardineria.Name = "lblCJardineria"
		Me.lblCManteniment.Text = "CManteniment"
		Me.lblCManteniment.Size = New System.Drawing.Size(55, 19)
		Me.lblCManteniment.Location = New System.Drawing.Point(580, 152)
		Me.lblCManteniment.TabIndex = 51
		Me.lblCManteniment.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCManteniment.BackColor = System.Drawing.SystemColors.Control
		Me.lblCManteniment.Enabled = True
		Me.lblCManteniment.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCManteniment.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCManteniment.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCManteniment.UseMnemonic = True
		Me.lblCManteniment.Visible = True
		Me.lblCManteniment.AutoSize = False
		Me.lblCManteniment.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCManteniment.Name = "lblCManteniment"
		Me.lblCGarden.Text = "CGarden"
		Me.lblCGarden.Size = New System.Drawing.Size(55, 19)
		Me.lblCGarden.Location = New System.Drawing.Point(580, 176)
		Me.lblCGarden.TabIndex = 50
		Me.lblCGarden.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCGarden.BackColor = System.Drawing.SystemColors.Control
		Me.lblCGarden.Enabled = True
		Me.lblCGarden.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCGarden.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCGarden.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCGarden.UseMnemonic = True
		Me.lblCGarden.Visible = True
		Me.lblCGarden.AutoSize = False
		Me.lblCGarden.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCGarden.Name = "lblCGarden"
		Me.lblCCanonge.Text = "CCanonge"
		Me.lblCCanonge.Size = New System.Drawing.Size(55, 19)
		Me.lblCCanonge.Location = New System.Drawing.Point(580, 200)
		Me.lblCCanonge.TabIndex = 49
		Me.lblCCanonge.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCCanonge.BackColor = System.Drawing.SystemColors.Control
		Me.lblCCanonge.Enabled = True
		Me.lblCCanonge.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCCanonge.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCCanonge.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCCanonge.UseMnemonic = True
		Me.lblCCanonge.Visible = True
		Me.lblCCanonge.AutoSize = False
		Me.lblCCanonge.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCCanonge.Name = "lblCCanonge"
		Me.lblCIncorpora.Text = "CIncorpora"
		Me.lblCIncorpora.Size = New System.Drawing.Size(55, 19)
		Me.lblCIncorpora.Location = New System.Drawing.Point(580, 36)
		Me.lblCIncorpora.TabIndex = 48
		Me.lblCIncorpora.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCIncorpora.BackColor = System.Drawing.SystemColors.Control
		Me.lblCIncorpora.Enabled = True
		Me.lblCIncorpora.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCIncorpora.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCIncorpora.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCIncorpora.UseMnemonic = True
		Me.lblCIncorpora.Visible = True
		Me.lblCIncorpora.AutoSize = False
		Me.lblCIncorpora.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCIncorpora.Name = "lblCIncorpora"
		Me.lblCDonacioP.Text = "CDonacioP"
		Me.lblCDonacioP.Size = New System.Drawing.Size(55, 19)
		Me.lblCDonacioP.Location = New System.Drawing.Point(582, 298)
		Me.lblCDonacioP.TabIndex = 47
		Me.lblCDonacioP.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCDonacioP.BackColor = System.Drawing.SystemColors.Control
		Me.lblCDonacioP.Enabled = True
		Me.lblCDonacioP.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCDonacioP.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCDonacioP.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCDonacioP.UseMnemonic = True
		Me.lblCDonacioP.Visible = True
		Me.lblCDonacioP.AutoSize = False
		Me.lblCDonacioP.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCDonacioP.Name = "lblCDonacioP"
		Me.Label4.Text = "Inter�s"
		Me.Label4.Size = New System.Drawing.Size(43, 15)
		Me.Label4.Location = New System.Drawing.Point(136, 18)
		Me.Label4.TabIndex = 46
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label5.Text = "�ltima Acci� Comercial"
		Me.Label5.Size = New System.Drawing.Size(133, 15)
		Me.Label5.Location = New System.Drawing.Point(206, 20)
		Me.Label5.TabIndex = 45
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label6.Text = "�lt. Venda/Relaci�"
		Me.Label6.Size = New System.Drawing.Size(111, 15)
		Me.Label6.Location = New System.Drawing.Point(562, 20)
		Me.Label6.TabIndex = 44
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.lblIDonacioP.Text = "CDonacioP"
		Me.lblIDonacioP.Size = New System.Drawing.Size(55, 19)
		Me.lblIDonacioP.Location = New System.Drawing.Point(132, 294)
		Me.lblIDonacioP.TabIndex = 43
		Me.lblIDonacioP.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIDonacioP.BackColor = System.Drawing.SystemColors.Control
		Me.lblIDonacioP.Enabled = True
		Me.lblIDonacioP.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIDonacioP.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIDonacioP.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIDonacioP.UseMnemonic = True
		Me.lblIDonacioP.Visible = True
		Me.lblIDonacioP.AutoSize = False
		Me.lblIDonacioP.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIDonacioP.Name = "lblIDonacioP"
		Me.lblIIncorpora.Text = "CIncorpora"
		Me.lblIIncorpora.Size = New System.Drawing.Size(55, 19)
		Me.lblIIncorpora.Location = New System.Drawing.Point(132, 32)
		Me.lblIIncorpora.TabIndex = 42
		Me.lblIIncorpora.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIIncorpora.BackColor = System.Drawing.SystemColors.Control
		Me.lblIIncorpora.Enabled = True
		Me.lblIIncorpora.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIIncorpora.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIIncorpora.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIIncorpora.UseMnemonic = True
		Me.lblIIncorpora.Visible = True
		Me.lblIIncorpora.AutoSize = False
		Me.lblIIncorpora.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIIncorpora.Name = "lblIIncorpora"
		Me.lblICanonge.Text = "CCanonge"
		Me.lblICanonge.Size = New System.Drawing.Size(55, 19)
		Me.lblICanonge.Location = New System.Drawing.Point(132, 198)
		Me.lblICanonge.TabIndex = 41
		Me.lblICanonge.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblICanonge.BackColor = System.Drawing.SystemColors.Control
		Me.lblICanonge.Enabled = True
		Me.lblICanonge.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblICanonge.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblICanonge.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblICanonge.UseMnemonic = True
		Me.lblICanonge.Visible = True
		Me.lblICanonge.AutoSize = False
		Me.lblICanonge.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblICanonge.Name = "lblICanonge"
		Me.lblIGarden.Text = "CGarden"
		Me.lblIGarden.Size = New System.Drawing.Size(55, 19)
		Me.lblIGarden.Location = New System.Drawing.Point(132, 174)
		Me.lblIGarden.TabIndex = 40
		Me.lblIGarden.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIGarden.BackColor = System.Drawing.SystemColors.Control
		Me.lblIGarden.Enabled = True
		Me.lblIGarden.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIGarden.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIGarden.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIGarden.UseMnemonic = True
		Me.lblIGarden.Visible = True
		Me.lblIGarden.AutoSize = False
		Me.lblIGarden.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIGarden.Name = "lblIGarden"
		Me.lblIManteniment.Text = "CManteniment"
		Me.lblIManteniment.Size = New System.Drawing.Size(55, 19)
		Me.lblIManteniment.Location = New System.Drawing.Point(132, 152)
		Me.lblIManteniment.TabIndex = 39
		Me.lblIManteniment.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIManteniment.BackColor = System.Drawing.SystemColors.Control
		Me.lblIManteniment.Enabled = True
		Me.lblIManteniment.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIManteniment.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIManteniment.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIManteniment.UseMnemonic = True
		Me.lblIManteniment.Visible = True
		Me.lblIManteniment.AutoSize = False
		Me.lblIManteniment.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIManteniment.Name = "lblIManteniment"
		Me.lblIJardineria.Text = "CJardineria"
		Me.lblIJardineria.Size = New System.Drawing.Size(55, 19)
		Me.lblIJardineria.Location = New System.Drawing.Point(132, 128)
		Me.lblIJardineria.TabIndex = 38
		Me.lblIJardineria.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIJardineria.BackColor = System.Drawing.SystemColors.Control
		Me.lblIJardineria.Enabled = True
		Me.lblIJardineria.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIJardineria.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIJardineria.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIJardineria.UseMnemonic = True
		Me.lblIJardineria.Visible = True
		Me.lblIJardineria.AutoSize = False
		Me.lblIJardineria.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIJardineria.Name = "lblIJardineria"
		Me.lblIImpremta.Text = "CImpremta"
		Me.lblIImpremta.Size = New System.Drawing.Size(55, 19)
		Me.lblIImpremta.Location = New System.Drawing.Point(132, 104)
		Me.lblIImpremta.TabIndex = 37
		Me.lblIImpremta.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIImpremta.BackColor = System.Drawing.SystemColors.Control
		Me.lblIImpremta.Enabled = True
		Me.lblIImpremta.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIImpremta.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIImpremta.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIImpremta.UseMnemonic = True
		Me.lblIImpremta.Visible = True
		Me.lblIImpremta.AutoSize = False
		Me.lblIImpremta.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIImpremta.Name = "lblIImpremta"
		Me.lblINeteja.Text = "CNeteja"
		Me.lblINeteja.Size = New System.Drawing.Size(55, 19)
		Me.lblINeteja.Location = New System.Drawing.Point(132, 56)
		Me.lblINeteja.TabIndex = 36
		Me.lblINeteja.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblINeteja.BackColor = System.Drawing.SystemColors.Control
		Me.lblINeteja.Enabled = True
		Me.lblINeteja.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblINeteja.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblINeteja.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblINeteja.UseMnemonic = True
		Me.lblINeteja.Visible = True
		Me.lblINeteja.AutoSize = False
		Me.lblINeteja.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblINeteja.Name = "lblINeteja"
		Me.lblIManip.Text = "CManip"
		Me.lblIManip.Size = New System.Drawing.Size(55, 19)
		Me.lblIManip.Location = New System.Drawing.Point(132, 80)
		Me.lblIManip.TabIndex = 35
		Me.lblIManip.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIManip.BackColor = System.Drawing.SystemColors.Control
		Me.lblIManip.Enabled = True
		Me.lblIManip.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIManip.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIManip.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIManip.UseMnemonic = True
		Me.lblIManip.Visible = True
		Me.lblIManip.AutoSize = False
		Me.lblIManip.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIManip.Name = "lblIManip"
		Me.lblPDonacioP.Text = "CDonacioP"
		Me.lblPDonacioP.Size = New System.Drawing.Size(55, 19)
		Me.lblPDonacioP.Location = New System.Drawing.Point(250, 296)
		Me.lblPDonacioP.TabIndex = 34
		Me.lblPDonacioP.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPDonacioP.BackColor = System.Drawing.SystemColors.Control
		Me.lblPDonacioP.Enabled = True
		Me.lblPDonacioP.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPDonacioP.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPDonacioP.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPDonacioP.UseMnemonic = True
		Me.lblPDonacioP.Visible = True
		Me.lblPDonacioP.AutoSize = False
		Me.lblPDonacioP.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPDonacioP.Name = "lblPDonacioP"
		Me.lblPIncorpora.Text = "CIncorpora"
		Me.lblPIncorpora.Size = New System.Drawing.Size(55, 19)
		Me.lblPIncorpora.Location = New System.Drawing.Point(248, 34)
		Me.lblPIncorpora.TabIndex = 33
		Me.lblPIncorpora.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPIncorpora.BackColor = System.Drawing.SystemColors.Control
		Me.lblPIncorpora.Enabled = True
		Me.lblPIncorpora.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPIncorpora.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPIncorpora.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPIncorpora.UseMnemonic = True
		Me.lblPIncorpora.Visible = True
		Me.lblPIncorpora.AutoSize = False
		Me.lblPIncorpora.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPIncorpora.Name = "lblPIncorpora"
		Me.lblPCanonge.Text = "CCanonge"
		Me.lblPCanonge.Size = New System.Drawing.Size(55, 19)
		Me.lblPCanonge.Location = New System.Drawing.Point(248, 200)
		Me.lblPCanonge.TabIndex = 32
		Me.lblPCanonge.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPCanonge.BackColor = System.Drawing.SystemColors.Control
		Me.lblPCanonge.Enabled = True
		Me.lblPCanonge.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPCanonge.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPCanonge.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPCanonge.UseMnemonic = True
		Me.lblPCanonge.Visible = True
		Me.lblPCanonge.AutoSize = False
		Me.lblPCanonge.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPCanonge.Name = "lblPCanonge"
		Me.lblPGarden.Text = "CGarden"
		Me.lblPGarden.Size = New System.Drawing.Size(55, 19)
		Me.lblPGarden.Location = New System.Drawing.Point(248, 176)
		Me.lblPGarden.TabIndex = 31
		Me.lblPGarden.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPGarden.BackColor = System.Drawing.SystemColors.Control
		Me.lblPGarden.Enabled = True
		Me.lblPGarden.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPGarden.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPGarden.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPGarden.UseMnemonic = True
		Me.lblPGarden.Visible = True
		Me.lblPGarden.AutoSize = False
		Me.lblPGarden.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPGarden.Name = "lblPGarden"
		Me.lblPManteniment.Text = "CManteniment"
		Me.lblPManteniment.Size = New System.Drawing.Size(55, 19)
		Me.lblPManteniment.Location = New System.Drawing.Point(248, 152)
		Me.lblPManteniment.TabIndex = 30
		Me.lblPManteniment.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPManteniment.BackColor = System.Drawing.SystemColors.Control
		Me.lblPManteniment.Enabled = True
		Me.lblPManteniment.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPManteniment.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPManteniment.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPManteniment.UseMnemonic = True
		Me.lblPManteniment.Visible = True
		Me.lblPManteniment.AutoSize = False
		Me.lblPManteniment.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPManteniment.Name = "lblPManteniment"
		Me.lblPJardineria.Text = "CJardineria"
		Me.lblPJardineria.Size = New System.Drawing.Size(55, 19)
		Me.lblPJardineria.Location = New System.Drawing.Point(248, 128)
		Me.lblPJardineria.TabIndex = 29
		Me.lblPJardineria.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPJardineria.BackColor = System.Drawing.SystemColors.Control
		Me.lblPJardineria.Enabled = True
		Me.lblPJardineria.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPJardineria.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPJardineria.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPJardineria.UseMnemonic = True
		Me.lblPJardineria.Visible = True
		Me.lblPJardineria.AutoSize = False
		Me.lblPJardineria.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPJardineria.Name = "lblPJardineria"
		Me.lblPImpremta.Text = "CImpremta"
		Me.lblPImpremta.Size = New System.Drawing.Size(55, 19)
		Me.lblPImpremta.Location = New System.Drawing.Point(248, 106)
		Me.lblPImpremta.TabIndex = 28
		Me.lblPImpremta.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPImpremta.BackColor = System.Drawing.SystemColors.Control
		Me.lblPImpremta.Enabled = True
		Me.lblPImpremta.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPImpremta.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPImpremta.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPImpremta.UseMnemonic = True
		Me.lblPImpremta.Visible = True
		Me.lblPImpremta.AutoSize = False
		Me.lblPImpremta.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPImpremta.Name = "lblPImpremta"
		Me.lblPNeteja.Text = "CNeteja"
		Me.lblPNeteja.Size = New System.Drawing.Size(55, 19)
		Me.lblPNeteja.Location = New System.Drawing.Point(248, 58)
		Me.lblPNeteja.TabIndex = 27
		Me.lblPNeteja.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPNeteja.BackColor = System.Drawing.SystemColors.Control
		Me.lblPNeteja.Enabled = True
		Me.lblPNeteja.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPNeteja.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPNeteja.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPNeteja.UseMnemonic = True
		Me.lblPNeteja.Visible = True
		Me.lblPNeteja.AutoSize = False
		Me.lblPNeteja.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPNeteja.Name = "lblPNeteja"
		Me.lblPManip.Text = "CManip"
		Me.lblPManip.Size = New System.Drawing.Size(55, 19)
		Me.lblPManip.Location = New System.Drawing.Point(248, 82)
		Me.lblPManip.TabIndex = 26
		Me.lblPManip.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPManip.BackColor = System.Drawing.SystemColors.Control
		Me.lblPManip.Enabled = True
		Me.lblPManip.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPManip.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPManip.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPManip.UseMnemonic = True
		Me.lblPManip.Visible = True
		Me.lblPManip.AutoSize = False
		Me.lblPManip.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPManip.Name = "lblPManip"
		Me.lblIVi.Text = "IVi"
		Me.lblIVi.Size = New System.Drawing.Size(55, 19)
		Me.lblIVi.Location = New System.Drawing.Point(132, 222)
		Me.lblIVi.TabIndex = 25
		Me.lblIVi.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIVi.BackColor = System.Drawing.SystemColors.Control
		Me.lblIVi.Enabled = True
		Me.lblIVi.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIVi.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIVi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIVi.UseMnemonic = True
		Me.lblIVi.Visible = True
		Me.lblIVi.AutoSize = False
		Me.lblIVi.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIVi.Name = "lblIVi"
		Me.lblPVi.Text = "PVi"
		Me.lblPVi.Size = New System.Drawing.Size(55, 19)
		Me.lblPVi.Location = New System.Drawing.Point(250, 222)
		Me.lblPVi.TabIndex = 24
		Me.lblPVi.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPVi.BackColor = System.Drawing.SystemColors.Control
		Me.lblPVi.Enabled = True
		Me.lblPVi.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPVi.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPVi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPVi.UseMnemonic = True
		Me.lblPVi.Visible = True
		Me.lblPVi.AutoSize = False
		Me.lblPVi.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPVi.Name = "lblPVi"
		Me.lblIDonantQ.Text = "IDonantQ"
		Me.lblIDonantQ.Size = New System.Drawing.Size(55, 19)
		Me.lblIDonantQ.Location = New System.Drawing.Point(132, 272)
		Me.lblIDonantQ.TabIndex = 23
		Me.lblIDonantQ.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIDonantQ.BackColor = System.Drawing.SystemColors.Control
		Me.lblIDonantQ.Enabled = True
		Me.lblIDonantQ.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIDonantQ.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIDonantQ.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIDonantQ.UseMnemonic = True
		Me.lblIDonantQ.Visible = True
		Me.lblIDonantQ.AutoSize = False
		Me.lblIDonantQ.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIDonantQ.Name = "lblIDonantQ"
		Me.lblpDonantQ.Text = "PDonantQ"
		Me.lblpDonantQ.Size = New System.Drawing.Size(55, 19)
		Me.lblpDonantQ.Location = New System.Drawing.Point(250, 274)
		Me.lblpDonantQ.TabIndex = 22
		Me.lblpDonantQ.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblpDonantQ.BackColor = System.Drawing.SystemColors.Control
		Me.lblpDonantQ.Enabled = True
		Me.lblpDonantQ.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblpDonantQ.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblpDonantQ.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblpDonantQ.UseMnemonic = True
		Me.lblpDonantQ.Visible = True
		Me.lblpDonantQ.AutoSize = False
		Me.lblpDonantQ.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblpDonantQ.Name = "lblpDonantQ"
		Me.lblCDonantQ.Text = "PConantQ"
		Me.lblCDonantQ.Size = New System.Drawing.Size(55, 19)
		Me.lblCDonantQ.Location = New System.Drawing.Point(582, 274)
		Me.lblCDonantQ.TabIndex = 21
		Me.lblCDonantQ.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCDonantQ.BackColor = System.Drawing.SystemColors.Control
		Me.lblCDonantQ.Enabled = True
		Me.lblCDonantQ.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCDonantQ.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCDonantQ.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCDonantQ.UseMnemonic = True
		Me.lblCDonantQ.Visible = True
		Me.lblCDonantQ.AutoSize = False
		Me.lblCDonantQ.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCDonantQ.Name = "lblCDonantQ"
		Me.lblIEsponsor.Text = "CEsponsor"
		Me.lblIEsponsor.Size = New System.Drawing.Size(55, 19)
		Me.lblIEsponsor.Location = New System.Drawing.Point(132, 320)
		Me.lblIEsponsor.TabIndex = 20
		Me.lblIEsponsor.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIEsponsor.BackColor = System.Drawing.SystemColors.Control
		Me.lblIEsponsor.Enabled = True
		Me.lblIEsponsor.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIEsponsor.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIEsponsor.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIEsponsor.UseMnemonic = True
		Me.lblIEsponsor.Visible = True
		Me.lblIEsponsor.AutoSize = False
		Me.lblIEsponsor.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIEsponsor.Name = "lblIEsponsor"
		Me.lblPEsponsor.Text = "PEsponsor"
		Me.lblPEsponsor.Size = New System.Drawing.Size(55, 19)
		Me.lblPEsponsor.Location = New System.Drawing.Point(250, 320)
		Me.lblPEsponsor.TabIndex = 19
		Me.lblPEsponsor.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPEsponsor.BackColor = System.Drawing.SystemColors.Control
		Me.lblPEsponsor.Enabled = True
		Me.lblPEsponsor.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPEsponsor.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPEsponsor.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPEsponsor.UseMnemonic = True
		Me.lblPEsponsor.Visible = True
		Me.lblPEsponsor.AutoSize = False
		Me.lblPEsponsor.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPEsponsor.Name = "lblPEsponsor"
		Me.lblCEsponsor.Text = "CEsponsor"
		Me.lblCEsponsor.Size = New System.Drawing.Size(55, 19)
		Me.lblCEsponsor.Location = New System.Drawing.Point(582, 320)
		Me.lblCEsponsor.TabIndex = 18
		Me.lblCEsponsor.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCEsponsor.BackColor = System.Drawing.SystemColors.Control
		Me.lblCEsponsor.Enabled = True
		Me.lblCEsponsor.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCEsponsor.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCEsponsor.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCEsponsor.UseMnemonic = True
		Me.lblCEsponsor.Visible = True
		Me.lblCEsponsor.AutoSize = False
		Me.lblCEsponsor.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCEsponsor.Name = "lblCEsponsor"
		Me.lblIVoluntariat.Text = "CVoluntariat"
		Me.lblIVoluntariat.Size = New System.Drawing.Size(55, 19)
		Me.lblIVoluntariat.Location = New System.Drawing.Point(132, 344)
		Me.lblIVoluntariat.TabIndex = 17
		Me.lblIVoluntariat.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblIVoluntariat.BackColor = System.Drawing.SystemColors.Control
		Me.lblIVoluntariat.Enabled = True
		Me.lblIVoluntariat.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblIVoluntariat.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblIVoluntariat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblIVoluntariat.UseMnemonic = True
		Me.lblIVoluntariat.Visible = True
		Me.lblIVoluntariat.AutoSize = False
		Me.lblIVoluntariat.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblIVoluntariat.Name = "lblIVoluntariat"
		Me.lblPVoluntariat.Text = "PVoluntariat"
		Me.lblPVoluntariat.Size = New System.Drawing.Size(55, 19)
		Me.lblPVoluntariat.Location = New System.Drawing.Point(250, 344)
		Me.lblPVoluntariat.TabIndex = 16
		Me.lblPVoluntariat.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPVoluntariat.BackColor = System.Drawing.SystemColors.Control
		Me.lblPVoluntariat.Enabled = True
		Me.lblPVoluntariat.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPVoluntariat.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPVoluntariat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPVoluntariat.UseMnemonic = True
		Me.lblPVoluntariat.Visible = True
		Me.lblPVoluntariat.AutoSize = False
		Me.lblPVoluntariat.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblPVoluntariat.Name = "lblPVoluntariat"
		Me.lblCVoluntariat.Text = "CVoluntariat"
		Me.lblCVoluntariat.Size = New System.Drawing.Size(55, 19)
		Me.lblCVoluntariat.Location = New System.Drawing.Point(582, 344)
		Me.lblCVoluntariat.TabIndex = 15
		Me.lblCVoluntariat.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCVoluntariat.BackColor = System.Drawing.SystemColors.Control
		Me.lblCVoluntariat.Enabled = True
		Me.lblCVoluntariat.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblCVoluntariat.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCVoluntariat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCVoluntariat.UseMnemonic = True
		Me.lblCVoluntariat.Visible = True
		Me.lblCVoluntariat.AutoSize = False
		Me.lblCVoluntariat.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCVoluntariat.Name = "lblCVoluntariat"
		Me.txtEntidad.AutoSize = False
		Me.txtEntidad.Size = New System.Drawing.Size(83, 19)
		Me.txtEntidad.Location = New System.Drawing.Point(122, 10)
		Me.txtEntidad.Maxlength = 8
		Me.txtEntidad.TabIndex = 0
		Me.txtEntidad.Tag = "1####G-ENTITATS#1#1"
		Me.txtEntidad.AcceptsReturn = True
		Me.txtEntidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEntidad.BackColor = System.Drawing.SystemColors.Window
		Me.txtEntidad.CausesValidation = True
		Me.txtEntidad.Enabled = True
		Me.txtEntidad.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEntidad.HideSelection = True
		Me.txtEntidad.ReadOnly = False
		Me.txtEntidad.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEntidad.MultiLine = False
		Me.txtEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEntidad.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEntidad.TabStop = True
		Me.txtEntidad.Visible = True
		Me.txtEntidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEntidad.Name = "txtEntidad"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(280, 19)
		Me.Text1.Location = New System.Drawing.Point(210, 10)
		Me.Text1.TabIndex = 4
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtSucursal.AutoSize = False
		Me.txtSucursal.Size = New System.Drawing.Size(31, 19)
		Me.txtSucursal.Location = New System.Drawing.Point(122, 32)
		Me.txtSucursal.Maxlength = 3
		Me.txtSucursal.TabIndex = 1
		Me.txtSucursal.Tag = "2####G-ENTITATS SUCURSALS#1#1"
		Me.txtSucursal.AcceptsReturn = True
		Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSucursal.BackColor = System.Drawing.SystemColors.Window
		Me.txtSucursal.CausesValidation = True
		Me.txtSucursal.Enabled = True
		Me.txtSucursal.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSucursal.HideSelection = True
		Me.txtSucursal.ReadOnly = False
		Me.txtSucursal.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSucursal.MultiLine = False
		Me.txtSucursal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSucursal.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSucursal.TabStop = True
		Me.txtSucursal.Visible = True
		Me.txtSucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSucursal.Name = "txtSucursal"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(334, 19)
		Me.Text2.Location = New System.Drawing.Point(156, 32)
		Me.Text2.TabIndex = 3
		Me.Text2.Tag = "^2"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		cmdExit.OcxState = CType(resources.GetObject("cmdExit.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdExit.Size = New System.Drawing.Size(81, 28)
		Me.cmdExit.Location = New System.Drawing.Point(1036, 538)
		Me.cmdExit.TabIndex = 2
		Me.cmdExit.Name = "cmdExit"
		cmdEntidad.OcxState = CType(resources.GetObject("cmdEntidad.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdEntidad.Size = New System.Drawing.Size(27, 27)
		Me.cmdEntidad.Location = New System.Drawing.Point(502, 6)
		Me.cmdEntidad.TabIndex = 87
		Me.cmdEntidad.Name = "cmdEntidad"
		Me.lbl1.Text = "Entidad"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 12)
		Me.lbl1.TabIndex = 6
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Sucursal"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 36)
		Me.lbl2.TabIndex = 5
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		CType(Me.cmdEntidad, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdExit, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdAccions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdObjectius, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage4, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdParticipacio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage5, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdOfertes, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage6, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdInsercions, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(TabControl1)
		Me.Controls.Add(txtEntidad)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtSucursal)
		Me.Controls.Add(Text2)
		Me.Controls.Add(cmdExit)
		Me.Controls.Add(cmdEntidad)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.TabControl1.Controls.Add(TabControlPage6)
		Me.TabControl1.Controls.Add(TabControlPage5)
		Me.TabControl1.Controls.Add(TabControlPage4)
		Me.TabControl1.Controls.Add(TabControlPage3)
		Me.TabControl1.Controls.Add(TabControlPage2)
		Me.TabControl1.Controls.Add(TabControlPage1)
		Me.TabControlPage6.Controls.Add(GrdInsercions)
		Me.TabControlPage5.Controls.Add(GrdOfertes)
		Me.TabControlPage4.Controls.Add(GrdParticipacio)
		Me.TabControlPage3.Controls.Add(GrdObjectius)
		Me.TabControlPage2.Controls.Add(GrdAccions)
		Me.TabControlPage1.Controls.Add(lblPLots)
		Me.TabControlPage1.Controls.Add(lblILots)
		Me.TabControlPage1.Controls.Add(Label24)
		Me.TabControlPage1.Controls.Add(lblCLots)
		Me.ShapeContainer1.Shapes.Add(Line15)
		Me.TabControlPage1.Controls.Add(lblCPstPLots)
		Me.TabControlPage1.Controls.Add(lblCPstALots)
		Me.TabControlPage1.Controls.Add(lblCPstPGarden)
		Me.TabControlPage1.Controls.Add(lblCPstAManteniment)
		Me.TabControlPage1.Controls.Add(lblCPstPManteniment)
		Me.TabControlPage1.Controls.Add(lblCPstAJardineria)
		Me.TabControlPage1.Controls.Add(lblCPstPJardineria)
		Me.TabControlPage1.Controls.Add(lblCPstAImpremta)
		Me.TabControlPage1.Controls.Add(lblCPstPImpremta)
		Me.TabControlPage1.Controls.Add(lblCPstAManip)
		Me.TabControlPage1.Controls.Add(lblCPstPManip)
		Me.TabControlPage1.Controls.Add(lblCPstANeteja)
		Me.TabControlPage1.Controls.Add(lblCPstPNeteja)
		Me.TabControlPage1.Controls.Add(Label20)
		Me.TabControlPage1.Controls.Add(Label19)
		Me.TabControlPage1.Controls.Add(lblCConveni)
		Me.TabControlPage1.Controls.Add(Label18)
		Me.ShapeContainer1.Shapes.Add(Line14)
		Me.TabControlPage1.Controls.Add(lblCProveidor)
		Me.TabControlPage1.Controls.Add(Label10)
		Me.ShapeContainer1.Shapes.Add(Line13)
		Me.TabControlPage1.Controls.Add(lblCVi)
		Me.ShapeContainer1.Shapes.Add(Line22)
		Me.ShapeContainer1.Shapes.Add(Line100)
		Me.TabControlPage1.Controls.Add(lblCNeteja)
		Me.ShapeContainer1.Shapes.Add(Line12)
		Me.ShapeContainer1.Shapes.Add(Line11)
		Me.ShapeContainer1.Shapes.Add(Line10)
		Me.ShapeContainer1.Shapes.Add(Line9)
		Me.ShapeContainer1.Shapes.Add(Line8)
		Me.ShapeContainer1.Shapes.Add(Line7)
		Me.ShapeContainer1.Shapes.Add(Line6)
		Me.ShapeContainer1.Shapes.Add(Line5)
		Me.ShapeContainer1.Shapes.Add(Line4)
		Me.ShapeContainer1.Shapes.Add(Line3)
		Me.TabControlPage1.Controls.Add(Label17)
		Me.TabControlPage1.Controls.Add(Label16)
		Me.TabControlPage1.Controls.Add(Label15)
		Me.TabControlPage1.Controls.Add(Label14)
		Me.TabControlPage1.Controls.Add(Label13)
		Me.TabControlPage1.Controls.Add(Label12)
		Me.TabControlPage1.Controls.Add(Label11)
		Me.TabControlPage1.Controls.Add(Label9)
		Me.TabControlPage1.Controls.Add(Label8)
		Me.TabControlPage1.Controls.Add(Label7)
		Me.TabControlPage1.Controls.Add(Label3)
		Me.TabControlPage1.Controls.Add(Label2)
		Me.TabControlPage1.Controls.Add(Label1)
		Me.TabControlPage1.Controls.Add(lblCManip)
		Me.TabControlPage1.Controls.Add(lblCImpremta)
		Me.TabControlPage1.Controls.Add(lblCJardineria)
		Me.TabControlPage1.Controls.Add(lblCManteniment)
		Me.TabControlPage1.Controls.Add(lblCGarden)
		Me.TabControlPage1.Controls.Add(lblCCanonge)
		Me.TabControlPage1.Controls.Add(lblCIncorpora)
		Me.TabControlPage1.Controls.Add(lblCDonacioP)
		Me.TabControlPage1.Controls.Add(Label4)
		Me.TabControlPage1.Controls.Add(Label5)
		Me.TabControlPage1.Controls.Add(Label6)
		Me.TabControlPage1.Controls.Add(lblIDonacioP)
		Me.TabControlPage1.Controls.Add(lblIIncorpora)
		Me.TabControlPage1.Controls.Add(lblICanonge)
		Me.TabControlPage1.Controls.Add(lblIGarden)
		Me.TabControlPage1.Controls.Add(lblIManteniment)
		Me.TabControlPage1.Controls.Add(lblIJardineria)
		Me.TabControlPage1.Controls.Add(lblIImpremta)
		Me.TabControlPage1.Controls.Add(lblINeteja)
		Me.TabControlPage1.Controls.Add(lblIManip)
		Me.TabControlPage1.Controls.Add(lblPDonacioP)
		Me.TabControlPage1.Controls.Add(lblPIncorpora)
		Me.TabControlPage1.Controls.Add(lblPCanonge)
		Me.TabControlPage1.Controls.Add(lblPGarden)
		Me.TabControlPage1.Controls.Add(lblPManteniment)
		Me.TabControlPage1.Controls.Add(lblPJardineria)
		Me.TabControlPage1.Controls.Add(lblPImpremta)
		Me.TabControlPage1.Controls.Add(lblPNeteja)
		Me.TabControlPage1.Controls.Add(lblPManip)
		Me.TabControlPage1.Controls.Add(lblIVi)
		Me.TabControlPage1.Controls.Add(lblPVi)
		Me.TabControlPage1.Controls.Add(lblIDonantQ)
		Me.TabControlPage1.Controls.Add(lblpDonantQ)
		Me.TabControlPage1.Controls.Add(lblCDonantQ)
		Me.TabControlPage1.Controls.Add(lblIEsponsor)
		Me.TabControlPage1.Controls.Add(lblPEsponsor)
		Me.TabControlPage1.Controls.Add(lblCEsponsor)
		Me.TabControlPage1.Controls.Add(lblIVoluntariat)
		Me.TabControlPage1.Controls.Add(lblPVoluntariat)
		Me.TabControlPage1.Controls.Add(lblCVoluntariat)
		Me.TabControlPage1.Controls.Add(ShapeContainer1)
		Me.TabControl1.ResumeLayout(False)
		Me.TabControlPage6.ResumeLayout(False)
		Me.TabControlPage5.ResumeLayout(False)
		Me.TabControlPage4.ResumeLayout(False)
		Me.TabControlPage3.ResumeLayout(False)
		Me.TabControlPage2.ResumeLayout(False)
		Me.TabControlPage1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class