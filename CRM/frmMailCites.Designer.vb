<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMailCites
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkIncorpora As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkLimpieza As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkManipulados As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkImprenta As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkJardineria As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkMantenimiento As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkGarden As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkCanonge As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkVino As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkDonantecuota As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkDonacionPuntual As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkEsponsor As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkVoluntariado As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkLots As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents XPFrame301 As AxciaXPFrame30.AxXPFrame30
	Public WithEvents txtColaborador As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtMail As System.Windows.Forms.TextBox
	Public WithEvents txtFechaBaja As AxDataControl.AxGmsData
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents chkPuedeVerAcciones As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents chkPertanyComiteDireccio As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMailCites))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.XPFrame301 = New AxciaXPFrame30.AxXPFrame30
		Me.chkIncorpora = New AxXtremeSuiteControls.AxCheckBox
		Me.chkLimpieza = New AxXtremeSuiteControls.AxCheckBox
		Me.chkManipulados = New AxXtremeSuiteControls.AxCheckBox
		Me.chkImprenta = New AxXtremeSuiteControls.AxCheckBox
		Me.chkJardineria = New AxXtremeSuiteControls.AxCheckBox
		Me.chkMantenimiento = New AxXtremeSuiteControls.AxCheckBox
		Me.chkGarden = New AxXtremeSuiteControls.AxCheckBox
		Me.chkCanonge = New AxXtremeSuiteControls.AxCheckBox
		Me.chkVino = New AxXtremeSuiteControls.AxCheckBox
		Me.chkDonantecuota = New AxXtremeSuiteControls.AxCheckBox
		Me.chkDonacionPuntual = New AxXtremeSuiteControls.AxCheckBox
		Me.chkEsponsor = New AxXtremeSuiteControls.AxCheckBox
		Me.chkVoluntariado = New AxXtremeSuiteControls.AxCheckBox
		Me.chkLots = New AxXtremeSuiteControls.AxCheckBox
		Me.txtColaborador = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtMail = New System.Windows.Forms.TextBox
		Me.txtFechaBaja = New AxDataControl.AxGmsData
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.chkPuedeVerAcciones = New AxXtremeSuiteControls.AxCheckBox
		Me.chkPertanyComiteDireccio = New AxXtremeSuiteControls.AxCheckBox
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.XPFrame301.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.chkIncorpora, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkLimpieza, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkManipulados, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkImprenta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkJardineria, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkMantenimiento, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkGarden, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkCanonge, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkVino, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkDonantecuota, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkDonacionPuntual, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkEsponsor, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkVoluntariado, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkLots, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.XPFrame301, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkPuedeVerAcciones, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkPertanyComiteDireccio, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Mail citas"
		Me.ClientSize = New System.Drawing.Size(499, 359)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "CRM-MAIL_CITES"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmMailCites"
		XPFrame301.OcxState = CType(resources.GetObject("XPFrame301.OcxState"), System.Windows.Forms.AxHost.State)
		Me.XPFrame301.Size = New System.Drawing.Size(481, 195)
		Me.XPFrame301.Location = New System.Drawing.Point(8, 112)
		Me.XPFrame301.Name = "XPFrame301"
		chkIncorpora.OcxState = CType(resources.GetObject("chkIncorpora.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkIncorpora.Size = New System.Drawing.Size(105, 19)
		Me.chkIncorpora.Location = New System.Drawing.Point(36, 16)
		Me.chkIncorpora.TabIndex = 5
		Me.chkIncorpora.Name = "chkIncorpora"
		chkLimpieza.OcxState = CType(resources.GetObject("chkLimpieza.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkLimpieza.Size = New System.Drawing.Size(105, 19)
		Me.chkLimpieza.Location = New System.Drawing.Point(36, 40)
		Me.chkLimpieza.TabIndex = 6
		Me.chkLimpieza.Name = "chkLimpieza"
		chkManipulados.OcxState = CType(resources.GetObject("chkManipulados.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkManipulados.Size = New System.Drawing.Size(105, 19)
		Me.chkManipulados.Location = New System.Drawing.Point(36, 64)
		Me.chkManipulados.TabIndex = 7
		Me.chkManipulados.Name = "chkManipulados"
		chkImprenta.OcxState = CType(resources.GetObject("chkImprenta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkImprenta.Size = New System.Drawing.Size(105, 19)
		Me.chkImprenta.Location = New System.Drawing.Point(36, 88)
		Me.chkImprenta.TabIndex = 8
		Me.chkImprenta.Name = "chkImprenta"
		chkJardineria.OcxState = CType(resources.GetObject("chkJardineria.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkJardineria.Size = New System.Drawing.Size(105, 19)
		Me.chkJardineria.Location = New System.Drawing.Point(36, 112)
		Me.chkJardineria.TabIndex = 9
		Me.chkJardineria.Name = "chkJardineria"
		chkMantenimiento.OcxState = CType(resources.GetObject("chkMantenimiento.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkMantenimiento.Size = New System.Drawing.Size(105, 19)
		Me.chkMantenimiento.Location = New System.Drawing.Point(36, 136)
		Me.chkMantenimiento.TabIndex = 10
		Me.chkMantenimiento.Name = "chkMantenimiento"
		chkGarden.OcxState = CType(resources.GetObject("chkGarden.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkGarden.Size = New System.Drawing.Size(105, 19)
		Me.chkGarden.Location = New System.Drawing.Point(36, 160)
		Me.chkGarden.TabIndex = 11
		Me.chkGarden.Name = "chkGarden"
		chkCanonge.OcxState = CType(resources.GetObject("chkCanonge.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkCanonge.Size = New System.Drawing.Size(105, 19)
		Me.chkCanonge.Location = New System.Drawing.Point(234, 16)
		Me.chkCanonge.TabIndex = 12
		Me.chkCanonge.Name = "chkCanonge"
		chkVino.OcxState = CType(resources.GetObject("chkVino.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkVino.Size = New System.Drawing.Size(105, 19)
		Me.chkVino.Location = New System.Drawing.Point(234, 40)
		Me.chkVino.TabIndex = 13
		Me.chkVino.Name = "chkVino"
		chkDonantecuota.OcxState = CType(resources.GetObject("chkDonantecuota.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkDonantecuota.Size = New System.Drawing.Size(105, 19)
		Me.chkDonantecuota.Location = New System.Drawing.Point(234, 88)
		Me.chkDonantecuota.TabIndex = 15
		Me.chkDonantecuota.Name = "chkDonantecuota"
		chkDonacionPuntual.OcxState = CType(resources.GetObject("chkDonacionPuntual.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkDonacionPuntual.Size = New System.Drawing.Size(105, 19)
		Me.chkDonacionPuntual.Location = New System.Drawing.Point(234, 112)
		Me.chkDonacionPuntual.TabIndex = 16
		Me.chkDonacionPuntual.Name = "chkDonacionPuntual"
		chkEsponsor.OcxState = CType(resources.GetObject("chkEsponsor.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkEsponsor.Size = New System.Drawing.Size(105, 19)
		Me.chkEsponsor.Location = New System.Drawing.Point(234, 136)
		Me.chkEsponsor.TabIndex = 17
		Me.chkEsponsor.Name = "chkEsponsor"
		chkVoluntariado.OcxState = CType(resources.GetObject("chkVoluntariado.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkVoluntariado.Size = New System.Drawing.Size(105, 19)
		Me.chkVoluntariado.Location = New System.Drawing.Point(234, 160)
		Me.chkVoluntariado.TabIndex = 18
		Me.chkVoluntariado.Name = "chkVoluntariado"
		chkLots.OcxState = CType(resources.GetObject("chkLots.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkLots.Size = New System.Drawing.Size(105, 19)
		Me.chkLots.Location = New System.Drawing.Point(234, 64)
		Me.chkLots.TabIndex = 14
		Me.chkLots.Name = "chkLots"
		Me.txtColaborador.AutoSize = False
		Me.txtColaborador.Size = New System.Drawing.Size(104, 19)
		Me.txtColaborador.Location = New System.Drawing.Point(124, 10)
		Me.txtColaborador.Maxlength = 10
		Me.txtColaborador.TabIndex = 0
		Me.txtColaborador.Tag = "*1"
		Me.txtColaborador.AcceptsReturn = True
		Me.txtColaborador.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtColaborador.BackColor = System.Drawing.SystemColors.Window
		Me.txtColaborador.CausesValidation = True
		Me.txtColaborador.Enabled = True
		Me.txtColaborador.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtColaborador.HideSelection = True
		Me.txtColaborador.ReadOnly = False
		Me.txtColaborador.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtColaborador.MultiLine = False
		Me.txtColaborador.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtColaborador.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtColaborador.TabStop = True
		Me.txtColaborador.Visible = True
		Me.txtColaborador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtColaborador.Name = "txtColaborador"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(259, 19)
		Me.Text1.Location = New System.Drawing.Point(231, 10)
		Me.Text1.TabIndex = 21
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtMail.AutoSize = False
		Me.txtMail.Size = New System.Drawing.Size(365, 19)
		Me.txtMail.Location = New System.Drawing.Point(124, 34)
		Me.txtMail.Maxlength = 80
		Me.txtMail.TabIndex = 1
		Me.txtMail.Tag = "2"
		Me.txtMail.AcceptsReturn = True
		Me.txtMail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtMail.BackColor = System.Drawing.SystemColors.Window
		Me.txtMail.CausesValidation = True
		Me.txtMail.Enabled = True
		Me.txtMail.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtMail.HideSelection = True
		Me.txtMail.ReadOnly = False
		Me.txtMail.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtMail.MultiLine = False
		Me.txtMail.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtMail.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtMail.TabStop = True
		Me.txtMail.Visible = True
		Me.txtMail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtMail.Name = "txtMail"
		txtFechaBaja.OcxState = CType(resources.GetObject("txtFechaBaja.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaBaja.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaBaja.Location = New System.Drawing.Point(124, 58)
		Me.txtFechaBaja.TabIndex = 2
		Me.txtFechaBaja.Name = "txtFechaBaja"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(315, 324)
		Me.cmdAceptar.TabIndex = 19
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(405, 324)
		Me.cmdGuardar.TabIndex = 24
		Me.cmdGuardar.Name = "cmdGuardar"
		chkPuedeVerAcciones.OcxState = CType(resources.GetObject("chkPuedeVerAcciones.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkPuedeVerAcciones.Size = New System.Drawing.Size(163, 19)
		Me.chkPuedeVerAcciones.Location = New System.Drawing.Point(324, 60)
		Me.chkPuedeVerAcciones.TabIndex = 3
		Me.chkPuedeVerAcciones.Name = "chkPuedeVerAcciones"
		chkPertanyComiteDireccio.OcxState = CType(resources.GetObject("chkPertanyComiteDireccio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkPertanyComiteDireccio.Size = New System.Drawing.Size(127, 19)
		Me.chkPertanyComiteDireccio.Location = New System.Drawing.Point(10, 86)
		Me.chkPertanyComiteDireccio.TabIndex = 4
		Me.chkPertanyComiteDireccio.Name = "chkPertanyComiteDireccio"
		Me.lbl1.Text = "Colaborador"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 20
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Mail"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 22
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Fecha baja"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 23
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(12, 323)
		Me.lblLock.TabIndex = 25
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 12
		Me.Line1.X2 = 71
		Me.Line1.Y1 = 319
		Me.Line1.Y2 = 319
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 12
		Me.Line2.X2 = 71
		Me.Line2.Y1 = 318
		Me.Line2.Y2 = 318
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.chkPertanyComiteDireccio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkPuedeVerAcciones, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.XPFrame301, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkLots, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkVoluntariado, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkEsponsor, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkDonacionPuntual, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkDonantecuota, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkVino, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkCanonge, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkGarden, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkMantenimiento, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkJardineria, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkImprenta, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkManipulados, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkLimpieza, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.chkIncorpora, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(XPFrame301)
		Me.Controls.Add(txtColaborador)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtMail)
		Me.Controls.Add(txtFechaBaja)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(chkPuedeVerAcciones)
		Me.Controls.Add(chkPertanyComiteDireccio)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.XPFrame301.Controls.Add(chkIncorpora)
		Me.XPFrame301.Controls.Add(chkLimpieza)
		Me.XPFrame301.Controls.Add(chkManipulados)
		Me.XPFrame301.Controls.Add(chkImprenta)
		Me.XPFrame301.Controls.Add(chkJardineria)
		Me.XPFrame301.Controls.Add(chkMantenimiento)
		Me.XPFrame301.Controls.Add(chkGarden)
		Me.XPFrame301.Controls.Add(chkCanonge)
		Me.XPFrame301.Controls.Add(chkVino)
		Me.XPFrame301.Controls.Add(chkDonantecuota)
		Me.XPFrame301.Controls.Add(chkDonacionPuntual)
		Me.XPFrame301.Controls.Add(chkEsponsor)
		Me.XPFrame301.Controls.Add(chkVoluntariado)
		Me.XPFrame301.Controls.Add(chkLots)
		Me.XPFrame301.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class