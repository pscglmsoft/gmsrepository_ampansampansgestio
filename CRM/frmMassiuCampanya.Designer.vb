<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMassiuCampanya
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtContacto As System.Windows.Forms.TextBox
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtSucursal As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtEntidad As System.Windows.Forms.TextBox
	Public WithEvents Text18 As System.Windows.Forms.TextBox
	Public WithEvents txtResultadoObjectivoCamp As System.Windows.Forms.TextBox
	Public WithEvents Text16 As System.Windows.Forms.TextBox
	Public WithEvents txtSubaccion As System.Windows.Forms.TextBox
	Public WithEvents Text15 As System.Windows.Forms.TextBox
	Public WithEvents txtAccion As System.Windows.Forms.TextBox
	Public WithEvents Text9 As System.Windows.Forms.TextBox
	Public WithEvents txtResponsable As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtCampana As System.Windows.Forms.TextBox
	Public WithEvents GrdEntitats As AxFlexCell.AxGrid
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtFechaAccion As AxDataControl.AxGmsData
	Public WithEvents cmbEstadoAccion As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents CmdInserta As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdBorra As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl18 As System.Windows.Forms.Label
	Public WithEvents lbl16 As System.Windows.Forms.Label
	Public WithEvents lbl15 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMassiuCampanya))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtContacto = New System.Windows.Forms.TextBox
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtSucursal = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtEntidad = New System.Windows.Forms.TextBox
		Me.Text18 = New System.Windows.Forms.TextBox
		Me.txtResultadoObjectivoCamp = New System.Windows.Forms.TextBox
		Me.Text16 = New System.Windows.Forms.TextBox
		Me.txtSubaccion = New System.Windows.Forms.TextBox
		Me.Text15 = New System.Windows.Forms.TextBox
		Me.txtAccion = New System.Windows.Forms.TextBox
		Me.Text9 = New System.Windows.Forms.TextBox
		Me.txtResponsable = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtCampana = New System.Windows.Forms.TextBox
		Me.GrdEntitats = New AxFlexCell.AxGrid
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.txtFechaAccion = New AxDataControl.AxGmsData
		Me.cmbEstadoAccion = New AxSSDataWidgets_B.AxSSDBCombo
		Me.CmdInserta = New AxXtremeSuiteControls.AxPushButton
		Me.cmdBorra = New AxXtremeSuiteControls.AxPushButton
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl18 = New System.Windows.Forms.Label
		Me.lbl16 = New System.Windows.Forms.Label
		Me.lbl15 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.GrdEntitats, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaAccion, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbEstadoAccion, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.CmdInserta, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdBorra, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Generar Accions Events"
		Me.ClientSize = New System.Drawing.Size(700, 469)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmMassiuCampanya"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(302, 19)
		Me.Text4.Location = New System.Drawing.Point(187, 438)
		Me.Text4.TabIndex = 30
		Me.Text4.Tag = "^10"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtContacto.AutoSize = False
		Me.txtContacto.Size = New System.Drawing.Size(62, 19)
		Me.txtContacto.Location = New System.Drawing.Point(122, 438)
		Me.txtContacto.Maxlength = 6
		Me.txtContacto.TabIndex = 9
		Me.txtContacto.Tag = "10####G-ENTITATS CONTACTES#1"
		Me.txtContacto.AcceptsReturn = True
		Me.txtContacto.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtContacto.BackColor = System.Drawing.SystemColors.Window
		Me.txtContacto.CausesValidation = True
		Me.txtContacto.Enabled = True
		Me.txtContacto.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtContacto.HideSelection = True
		Me.txtContacto.ReadOnly = False
		Me.txtContacto.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtContacto.MultiLine = False
		Me.txtContacto.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtContacto.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtContacto.TabStop = True
		Me.txtContacto.Visible = True
		Me.txtContacto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtContacto.Name = "txtContacto"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(332, 19)
		Me.Text2.Location = New System.Drawing.Point(156, 414)
		Me.Text2.TabIndex = 26
		Me.Text2.Tag = "^9"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtSucursal.AutoSize = False
		Me.txtSucursal.Size = New System.Drawing.Size(31, 19)
		Me.txtSucursal.Location = New System.Drawing.Point(122, 414)
		Me.txtSucursal.Maxlength = 3
		Me.txtSucursal.TabIndex = 8
		Me.txtSucursal.Tag = "9####G-ENTITATS SUCURSALS#1#1"
		Me.txtSucursal.AcceptsReturn = True
		Me.txtSucursal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSucursal.BackColor = System.Drawing.SystemColors.Window
		Me.txtSucursal.CausesValidation = True
		Me.txtSucursal.Enabled = True
		Me.txtSucursal.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSucursal.HideSelection = True
		Me.txtSucursal.ReadOnly = False
		Me.txtSucursal.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSucursal.MultiLine = False
		Me.txtSucursal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSucursal.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSucursal.TabStop = True
		Me.txtSucursal.Visible = True
		Me.txtSucursal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSucursal.Name = "txtSucursal"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(280, 19)
		Me.Text1.Location = New System.Drawing.Point(208, 390)
		Me.Text1.TabIndex = 25
		Me.Text1.Tag = "^8"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		Me.txtEntidad.AutoSize = False
		Me.txtEntidad.Size = New System.Drawing.Size(83, 19)
		Me.txtEntidad.Location = New System.Drawing.Point(122, 390)
		Me.txtEntidad.Maxlength = 8
		Me.txtEntidad.TabIndex = 7
		Me.txtEntidad.Tag = "8####G-ENTITATS#1#1"
		Me.txtEntidad.AcceptsReturn = True
		Me.txtEntidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEntidad.BackColor = System.Drawing.SystemColors.Window
		Me.txtEntidad.CausesValidation = True
		Me.txtEntidad.Enabled = True
		Me.txtEntidad.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEntidad.HideSelection = True
		Me.txtEntidad.ReadOnly = False
		Me.txtEntidad.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEntidad.MultiLine = False
		Me.txtEntidad.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEntidad.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEntidad.TabStop = True
		Me.txtEntidad.Visible = True
		Me.txtEntidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEntidad.Name = "txtEntidad"
		Me.Text18.AutoSize = False
		Me.Text18.BackColor = System.Drawing.Color.White
		Me.Text18.Enabled = False
		Me.Text18.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text18.Size = New System.Drawing.Size(340, 19)
		Me.Text18.Location = New System.Drawing.Point(146, 124)
		Me.Text18.TabIndex = 21
		Me.Text18.Tag = "^7"
		Me.Text18.AcceptsReturn = True
		Me.Text18.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text18.CausesValidation = True
		Me.Text18.HideSelection = True
		Me.Text18.ReadOnly = False
		Me.Text18.Maxlength = 0
		Me.Text18.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text18.MultiLine = False
		Me.Text18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text18.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text18.TabStop = True
		Me.Text18.Visible = True
		Me.Text18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text18.Name = "Text18"
		Me.txtResultadoObjectivoCamp.AutoSize = False
		Me.txtResultadoObjectivoCamp.Size = New System.Drawing.Size(21, 19)
		Me.txtResultadoObjectivoCamp.Location = New System.Drawing.Point(122, 124)
		Me.txtResultadoObjectivoCamp.Maxlength = 2
		Me.txtResultadoObjectivoCamp.TabIndex = 6
		Me.txtResultadoObjectivoCamp.Tag = "7####CRM-TAULA_RESULTATS_DETALL#1#1"
		Me.txtResultadoObjectivoCamp.AcceptsReturn = True
		Me.txtResultadoObjectivoCamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtResultadoObjectivoCamp.BackColor = System.Drawing.SystemColors.Window
		Me.txtResultadoObjectivoCamp.CausesValidation = True
		Me.txtResultadoObjectivoCamp.Enabled = True
		Me.txtResultadoObjectivoCamp.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtResultadoObjectivoCamp.HideSelection = True
		Me.txtResultadoObjectivoCamp.ReadOnly = False
		Me.txtResultadoObjectivoCamp.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtResultadoObjectivoCamp.MultiLine = False
		Me.txtResultadoObjectivoCamp.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtResultadoObjectivoCamp.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtResultadoObjectivoCamp.TabStop = True
		Me.txtResultadoObjectivoCamp.Visible = True
		Me.txtResultadoObjectivoCamp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtResultadoObjectivoCamp.Name = "txtResultadoObjectivoCamp"
		Me.Text16.AutoSize = False
		Me.Text16.BackColor = System.Drawing.Color.White
		Me.Text16.Enabled = False
		Me.Text16.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text16.Size = New System.Drawing.Size(340, 19)
		Me.Text16.Location = New System.Drawing.Point(146, 102)
		Me.Text16.TabIndex = 20
		Me.Text16.Tag = "^6"
		Me.Text16.AcceptsReturn = True
		Me.Text16.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text16.CausesValidation = True
		Me.Text16.HideSelection = True
		Me.Text16.ReadOnly = False
		Me.Text16.Maxlength = 0
		Me.Text16.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text16.MultiLine = False
		Me.Text16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text16.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text16.TabStop = True
		Me.Text16.Visible = True
		Me.Text16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text16.Name = "Text16"
		Me.txtSubaccion.AutoSize = False
		Me.txtSubaccion.Enabled = False
		Me.txtSubaccion.Size = New System.Drawing.Size(21, 19)
		Me.txtSubaccion.Location = New System.Drawing.Point(122, 100)
		Me.txtSubaccion.Maxlength = 2
		Me.txtSubaccion.TabIndex = 5
		Me.txtSubaccion.Tag = "6####CRM-TAULA_TIPUS_SUBACCIONS#1#1"
		Me.txtSubaccion.AcceptsReturn = True
		Me.txtSubaccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSubaccion.BackColor = System.Drawing.SystemColors.Window
		Me.txtSubaccion.CausesValidation = True
		Me.txtSubaccion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSubaccion.HideSelection = True
		Me.txtSubaccion.ReadOnly = False
		Me.txtSubaccion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSubaccion.MultiLine = False
		Me.txtSubaccion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSubaccion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSubaccion.TabStop = True
		Me.txtSubaccion.Visible = True
		Me.txtSubaccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSubaccion.Name = "txtSubaccion"
		Me.Text15.AutoSize = False
		Me.Text15.BackColor = System.Drawing.Color.White
		Me.Text15.Enabled = False
		Me.Text15.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text15.Size = New System.Drawing.Size(300, 19)
		Me.Text15.Location = New System.Drawing.Point(187, 76)
		Me.Text15.TabIndex = 19
		Me.Text15.Tag = "^5"
		Me.Text15.AcceptsReturn = True
		Me.Text15.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text15.CausesValidation = True
		Me.Text15.HideSelection = True
		Me.Text15.ReadOnly = False
		Me.Text15.Maxlength = 0
		Me.Text15.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text15.MultiLine = False
		Me.Text15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text15.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text15.TabStop = True
		Me.Text15.Visible = True
		Me.Text15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text15.Name = "Text15"
		Me.txtAccion.AutoSize = False
		Me.txtAccion.Enabled = False
		Me.txtAccion.Size = New System.Drawing.Size(62, 19)
		Me.txtAccion.Location = New System.Drawing.Point(122, 76)
		Me.txtAccion.Maxlength = 6
		Me.txtAccion.TabIndex = 4
		Me.txtAccion.Tag = "5####CRM-TAULA_TIPUS_ACCIONS#1#1"
		Me.txtAccion.AcceptsReturn = True
		Me.txtAccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtAccion.BackColor = System.Drawing.SystemColors.Window
		Me.txtAccion.CausesValidation = True
		Me.txtAccion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtAccion.HideSelection = True
		Me.txtAccion.ReadOnly = False
		Me.txtAccion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtAccion.MultiLine = False
		Me.txtAccion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtAccion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtAccion.TabStop = True
		Me.txtAccion.Visible = True
		Me.txtAccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtAccion.Name = "txtAccion"
		Me.Text9.AutoSize = False
		Me.Text9.BackColor = System.Drawing.Color.White
		Me.Text9.Enabled = False
		Me.Text9.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text9.Size = New System.Drawing.Size(259, 19)
		Me.Text9.Location = New System.Drawing.Point(230, 52)
		Me.Text9.TabIndex = 17
		Me.Text9.Tag = "^4"
		Me.Text9.AcceptsReturn = True
		Me.Text9.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text9.CausesValidation = True
		Me.Text9.HideSelection = True
		Me.Text9.ReadOnly = False
		Me.Text9.Maxlength = 0
		Me.Text9.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text9.MultiLine = False
		Me.Text9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text9.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text9.TabStop = True
		Me.Text9.Visible = True
		Me.Text9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text9.Name = "Text9"
		Me.txtResponsable.AutoSize = False
		Me.txtResponsable.Size = New System.Drawing.Size(104, 19)
		Me.txtResponsable.Location = New System.Drawing.Point(122, 52)
		Me.txtResponsable.Maxlength = 10
		Me.txtResponsable.TabIndex = 3
		Me.txtResponsable.Tag = "4####I-PERSONAL#1#1"
		Me.txtResponsable.AcceptsReturn = True
		Me.txtResponsable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtResponsable.BackColor = System.Drawing.SystemColors.Window
		Me.txtResponsable.CausesValidation = True
		Me.txtResponsable.Enabled = True
		Me.txtResponsable.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtResponsable.HideSelection = True
		Me.txtResponsable.ReadOnly = False
		Me.txtResponsable.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtResponsable.MultiLine = False
		Me.txtResponsable.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtResponsable.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtResponsable.TabStop = True
		Me.txtResponsable.Visible = True
		Me.txtResponsable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtResponsable.Name = "txtResponsable"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(332, 19)
		Me.Text6.Location = New System.Drawing.Point(156, 4)
		Me.Text6.TabIndex = 13
		Me.Text6.Tag = "^1"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtCampana.AutoSize = False
		Me.txtCampana.Size = New System.Drawing.Size(31, 19)
		Me.txtCampana.Location = New System.Drawing.Point(122, 4)
		Me.txtCampana.Maxlength = 3
		Me.txtCampana.TabIndex = 0
		Me.txtCampana.Tag = "1####CRM-CAMPANYES#1#1"
		Me.txtCampana.AcceptsReturn = True
		Me.txtCampana.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCampana.BackColor = System.Drawing.SystemColors.Window
		Me.txtCampana.CausesValidation = True
		Me.txtCampana.Enabled = True
		Me.txtCampana.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCampana.HideSelection = True
		Me.txtCampana.ReadOnly = False
		Me.txtCampana.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCampana.MultiLine = False
		Me.txtCampana.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCampana.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCampana.TabStop = True
		Me.txtCampana.Visible = True
		Me.txtCampana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCampana.Name = "txtCampana"
		GrdEntitats.OcxState = CType(resources.GetObject("GrdEntitats.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdEntitats.Size = New System.Drawing.Size(686, 219)
		Me.GrdEntitats.Location = New System.Drawing.Point(6, 164)
		Me.GrdEntitats.TabIndex = 12
		Me.GrdEntitats.Name = "GrdEntitats"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(606, 430)
		Me.cmdAceptar.TabIndex = 11
		Me.cmdAceptar.Name = "cmdAceptar"
		txtFechaAccion.OcxState = CType(resources.GetObject("txtFechaAccion.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaAccion.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaAccion.Location = New System.Drawing.Point(122, 28)
		Me.txtFechaAccion.TabIndex = 1
		Me.txtFechaAccion.Name = "txtFechaAccion"
		cmbEstadoAccion.OcxState = CType(resources.GetObject("cmbEstadoAccion.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbEstadoAccion.Size = New System.Drawing.Size(145, 19)
		Me.cmbEstadoAccion.Location = New System.Drawing.Point(342, 28)
		Me.cmbEstadoAccion.TabIndex = 2
		Me.cmbEstadoAccion.Name = "cmbEstadoAccion"
		CmdInserta.OcxState = CType(resources.GetObject("CmdInserta.OcxState"), System.Windows.Forms.AxHost.State)
		Me.CmdInserta.Size = New System.Drawing.Size(25, 25)
		Me.CmdInserta.Location = New System.Drawing.Point(494, 432)
		Me.CmdInserta.TabIndex = 10
		Me.CmdInserta.Name = "CmdInserta"
		cmdBorra.OcxState = CType(resources.GetObject("cmdBorra.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdBorra.Size = New System.Drawing.Size(25, 25)
		Me.cmdBorra.Location = New System.Drawing.Point(522, 432)
		Me.cmdBorra.TabIndex = 29
		Me.cmdBorra.Name = "cmdBorra"
		Me.lbl4.Text = "Contacto"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(8, 442)
		Me.lbl4.TabIndex = 31
		Me.lbl4.Tag = "10"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl2.Text = "Sucursal"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(8, 418)
		Me.lbl2.TabIndex = 28
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl1.Text = "Entidad"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(8, 394)
		Me.lbl1.TabIndex = 27
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl18.Text = "Resultado objectivo / evento"
		Me.lbl18.Size = New System.Drawing.Size(111, 29)
		Me.lbl18.Location = New System.Drawing.Point(8, 128)
		Me.lbl18.TabIndex = 24
		Me.lbl18.Tag = "18"
		Me.lbl18.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl18.BackColor = System.Drawing.SystemColors.Control
		Me.lbl18.Enabled = True
		Me.lbl18.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl18.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl18.UseMnemonic = True
		Me.lbl18.Visible = True
		Me.lbl18.AutoSize = False
		Me.lbl18.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl18.Name = "lbl18"
		Me.lbl16.Text = "Subacci�n"
		Me.lbl16.Size = New System.Drawing.Size(111, 15)
		Me.lbl16.Location = New System.Drawing.Point(8, 104)
		Me.lbl16.TabIndex = 23
		Me.lbl16.Tag = "16"
		Me.lbl16.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl16.BackColor = System.Drawing.SystemColors.Control
		Me.lbl16.Enabled = True
		Me.lbl16.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl16.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl16.UseMnemonic = True
		Me.lbl16.Visible = True
		Me.lbl16.AutoSize = False
		Me.lbl16.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl16.Name = "lbl16"
		Me.lbl15.Text = "Acci�n"
		Me.lbl15.Size = New System.Drawing.Size(111, 15)
		Me.lbl15.Location = New System.Drawing.Point(8, 80)
		Me.lbl15.TabIndex = 22
		Me.lbl15.Tag = "15"
		Me.lbl15.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl15.BackColor = System.Drawing.SystemColors.Control
		Me.lbl15.Enabled = True
		Me.lbl15.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl15.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl15.UseMnemonic = True
		Me.lbl15.Visible = True
		Me.lbl15.AutoSize = False
		Me.lbl15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl15.Name = "lbl15"
		Me.lbl9.Text = "Responsable"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(8, 56)
		Me.lbl9.TabIndex = 18
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl8.Text = "Estado acci�n"
		Me.lbl8.Size = New System.Drawing.Size(81, 15)
		Me.lbl8.Location = New System.Drawing.Point(254, 32)
		Me.lbl8.TabIndex = 16
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl7.Text = "Fecha acci�n"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(8, 32)
		Me.lbl7.TabIndex = 15
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl6.Text = "Evento"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(8, 8)
		Me.lbl6.TabIndex = 14
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		CType(Me.cmdBorra, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.CmdInserta, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbEstadoAccion, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaAccion, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdEntitats, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtContacto)
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtSucursal)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtEntidad)
		Me.Controls.Add(Text18)
		Me.Controls.Add(txtResultadoObjectivoCamp)
		Me.Controls.Add(Text16)
		Me.Controls.Add(txtSubaccion)
		Me.Controls.Add(Text15)
		Me.Controls.Add(txtAccion)
		Me.Controls.Add(Text9)
		Me.Controls.Add(txtResponsable)
		Me.Controls.Add(Text6)
		Me.Controls.Add(txtCampana)
		Me.Controls.Add(GrdEntitats)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(txtFechaAccion)
		Me.Controls.Add(cmbEstadoAccion)
		Me.Controls.Add(CmdInserta)
		Me.Controls.Add(cmdBorra)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl18)
		Me.Controls.Add(lbl16)
		Me.Controls.Add(lbl15)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl6)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class