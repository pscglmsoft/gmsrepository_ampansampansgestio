Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmTaulaTipusAccions
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		CarregaSubaccions()
	End Sub
	
	Public Overrides Sub FGetReg()
		CarregaSubaccions()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	
	Private Sub chkPermiteEventos_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPermiteEventos.GotFocus
		XGotFocus(Me, chkPermiteEventos)
	End Sub
	
	Private Sub chkPermiteEventos_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkPermiteEventos.LostFocus
		XLostFocus(Me, chkPermiteEventos)
	End Sub
	
	'UPGRADE_WARNING: Form evento frmTaulaTipusAccions.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmTaulaTipusAccions_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaSubaccions()
	End Sub
	
	Private Sub frmTaulaTipusAccions_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmTaulaTipusAccions_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmTaulaTipusAccions_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmTaulaTipusAccions_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	Private Sub GrdSubaccions_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdSubaccions.DoubleClick
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmTaulaTipusSubaccions, "frmTaulaTipusSubaccions", txtCodigo.Text & S & Piece(GrdSubaccions.Cell(GrdSubaccions.ActiveCell.Row, 1).Text, "|", 2))
	End Sub
	
	Private Sub GrdSubaccions_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdSubaccions.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtCodigo.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdSubaccions", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nueva subacci�n",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmTaulaTipusSubaccions", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar subacci�n",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdSubaccions.Rows > 1 And PermisConsulta("frmTaulaTipusSubaccions", e_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar subacci�n",  ,  , XPIcon("Delete"),  ,  ,  ,  , "B")
				.MenuItems.Item("B").Enabled = GrdSubaccions.Rows > 1 And PermisConsulta("frmTaulaTipusSubaccions", e_Permisos.Baixes)
			End With
		End With
		XpExecutaMenu(Me, "GrdSubaccions", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub txtCodigo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.GotFocus
		XGotFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.DoubleClick
		ConsultaTaula(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.LostFocus
		If txtCodigo.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodigo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcion.GotFocus
		XGotFocus(Me, txtDescripcion)
	End Sub
	
	Private Sub txtDescripcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcion.LostFocus
		XLostFocus(Me, txtDescripcion)
	End Sub
	
	Private Sub txtDescripcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaBaja_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.GotFocus
		XGotFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub txtFechaBaja_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaBaja.LostFocus
		XLostFocus(Me, txtFechaBaja)
	End Sub
	
	Private Sub CarregaSubaccions()
		MCache.P1 = txtCodigo.Text
		CarregaFGrid(GrdSubaccions, "CGSUBAC^CRM", txtCodigo.Text)
	End Sub
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Select Case NomMenu
			Case "GrdSubaccions"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "N"
						ObreFormulari(frmTaulaTipusSubaccions, "frmTaulaTipusSubaccions", txtCodigo.Text)
					Case "E"
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTaulaTipusSubaccions, "frmTaulaTipusSubaccions", txtCodigo.Text & S & Piece(GrdSubaccions.Cell(GrdSubaccions.ActiveCell.Row, 1).Text, "|", 2))
					Case "B"
						A = GrdSubaccions.Cell(GrdSubaccions.ActiveCell.Row, 2).Text
						If A = "" Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmTaulaTipusSubaccions, "frmTaulaTipusSubaccions", txtCodigo.Text & S & Piece(GrdSubaccions.Cell(GrdSubaccions.ActiveCell.Row, 1).Text, "|", 2))
						LlibAplicacio.DeleteReg(frmTaulaTipusSubaccions   )
						frmTaulaTipusSubaccions.Unload()
						
				End Select
		End Select
	End Sub
End Class
