<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCampanyesNou
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtCodigo As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcion As System.Windows.Forms.TextBox
	Public WithEvents txtFechaInicio As AxDataControl.AxGmsData
	Public WithEvents txtFechaFin As AxDataControl.AxGmsData
	Public WithEvents txtCostePrevisto As AxDataControl.AxGmsImports
	Public WithEvents txtCosteReal As AxDataControl.AxGmsImports
	Public WithEvents txtFinanciadoPor3eros As AxDataControl.AxGmsImports
	Public WithEvents txtNombreTerceros As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcionObjectivo As System.Windows.Forms.TextBox
	Public WithEvents txtObservaciones As System.Windows.Forms.TextBox
	Public WithEvents txtCentroTrabajo As System.Windows.Forms.TextBox
	Public WithEvents Text11 As System.Windows.Forms.TextBox
	Public WithEvents txtPublicoObjetivo As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl10 As System.Windows.Forms.Label
	Public WithEvents lbl11 As System.Windows.Forms.Label
	Public WithEvents lbl12 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCampanyesNou))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtCodigo = New System.Windows.Forms.TextBox
		Me.txtDescripcion = New System.Windows.Forms.TextBox
		Me.txtFechaInicio = New AxDataControl.AxGmsData
		Me.txtFechaFin = New AxDataControl.AxGmsData
		Me.txtCostePrevisto = New AxDataControl.AxGmsImports
		Me.txtCosteReal = New AxDataControl.AxGmsImports
		Me.txtFinanciadoPor3eros = New AxDataControl.AxGmsImports
		Me.txtNombreTerceros = New System.Windows.Forms.TextBox
		Me.txtDescripcionObjectivo = New System.Windows.Forms.TextBox
		Me.txtObservaciones = New System.Windows.Forms.TextBox
		Me.txtCentroTrabajo = New System.Windows.Forms.TextBox
		Me.Text11 = New System.Windows.Forms.TextBox
		Me.txtPublicoObjetivo = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl10 = New System.Windows.Forms.Label
		Me.lbl11 = New System.Windows.Forms.Label
		Me.lbl12 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtFechaInicio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaFin, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtCostePrevisto, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtCosteReal, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFinanciadoPor3eros, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Campanyes"
		Me.ClientSize = New System.Drawing.Size(680, 390)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "CRM-CAMPANYES_NOU"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmCampanyesNou"
		Me.txtCodigo.AutoSize = False
		Me.txtCodigo.Size = New System.Drawing.Size(31, 19)
		Me.txtCodigo.Location = New System.Drawing.Point(124, 10)
		Me.txtCodigo.Maxlength = 3
		Me.txtCodigo.TabIndex = 1
		Me.txtCodigo.Tag = "*1"
		Me.txtCodigo.AcceptsReturn = True
		Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigo.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigo.CausesValidation = True
		Me.txtCodigo.Enabled = True
		Me.txtCodigo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigo.HideSelection = True
		Me.txtCodigo.ReadOnly = False
		Me.txtCodigo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigo.MultiLine = False
		Me.txtCodigo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigo.TabStop = True
		Me.txtCodigo.Visible = True
		Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigo.Name = "txtCodigo"
		Me.txtDescripcion.AutoSize = False
		Me.txtDescripcion.Size = New System.Drawing.Size(545, 19)
		Me.txtDescripcion.Location = New System.Drawing.Point(124, 34)
		Me.txtDescripcion.Maxlength = 100
		Me.txtDescripcion.TabIndex = 3
		Me.txtDescripcion.Tag = "2"
		Me.txtDescripcion.AcceptsReturn = True
		Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcion.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcion.CausesValidation = True
		Me.txtDescripcion.Enabled = True
		Me.txtDescripcion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcion.HideSelection = True
		Me.txtDescripcion.ReadOnly = False
		Me.txtDescripcion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcion.MultiLine = False
		Me.txtDescripcion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcion.TabStop = True
		Me.txtDescripcion.Visible = True
		Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcion.Name = "txtDescripcion"
		txtFechaInicio.OcxState = CType(resources.GetObject("txtFechaInicio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaInicio.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaInicio.Location = New System.Drawing.Point(124, 58)
		Me.txtFechaInicio.TabIndex = 5
		Me.txtFechaInicio.Name = "txtFechaInicio"
		txtFechaFin.OcxState = CType(resources.GetObject("txtFechaFin.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaFin.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaFin.Location = New System.Drawing.Point(124, 82)
		Me.txtFechaFin.TabIndex = 7
		Me.txtFechaFin.Name = "txtFechaFin"
		txtCostePrevisto.OcxState = CType(resources.GetObject("txtCostePrevisto.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtCostePrevisto.Size = New System.Drawing.Size(83, 19)
		Me.txtCostePrevisto.Location = New System.Drawing.Point(124, 106)
		Me.txtCostePrevisto.TabIndex = 9
		Me.txtCostePrevisto.Name = "txtCostePrevisto"
		txtCosteReal.OcxState = CType(resources.GetObject("txtCosteReal.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtCosteReal.Size = New System.Drawing.Size(83, 19)
		Me.txtCosteReal.Location = New System.Drawing.Point(124, 130)
		Me.txtCosteReal.TabIndex = 11
		Me.txtCosteReal.Name = "txtCosteReal"
		txtFinanciadoPor3eros.OcxState = CType(resources.GetObject("txtFinanciadoPor3eros.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFinanciadoPor3eros.Size = New System.Drawing.Size(52, 19)
		Me.txtFinanciadoPor3eros.Location = New System.Drawing.Point(124, 154)
		Me.txtFinanciadoPor3eros.TabIndex = 13
		Me.txtFinanciadoPor3eros.Name = "txtFinanciadoPor3eros"
		Me.txtNombreTerceros.AutoSize = False
		Me.txtNombreTerceros.Size = New System.Drawing.Size(544, 19)
		Me.txtNombreTerceros.Location = New System.Drawing.Point(124, 178)
		Me.txtNombreTerceros.Maxlength = 150
		Me.txtNombreTerceros.TabIndex = 15
		Me.txtNombreTerceros.Tag = "8"
		Me.txtNombreTerceros.AcceptsReturn = True
		Me.txtNombreTerceros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNombreTerceros.BackColor = System.Drawing.SystemColors.Window
		Me.txtNombreTerceros.CausesValidation = True
		Me.txtNombreTerceros.Enabled = True
		Me.txtNombreTerceros.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNombreTerceros.HideSelection = True
		Me.txtNombreTerceros.ReadOnly = False
		Me.txtNombreTerceros.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNombreTerceros.MultiLine = False
		Me.txtNombreTerceros.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNombreTerceros.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNombreTerceros.TabStop = True
		Me.txtNombreTerceros.Visible = True
		Me.txtNombreTerceros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNombreTerceros.Name = "txtNombreTerceros"
		Me.txtDescripcionObjectivo.AutoSize = False
		Me.txtDescripcionObjectivo.Size = New System.Drawing.Size(545, 43)
		Me.txtDescripcionObjectivo.Location = New System.Drawing.Point(124, 202)
		Me.txtDescripcionObjectivo.Maxlength = 200
		Me.txtDescripcionObjectivo.MultiLine = True
		Me.txtDescripcionObjectivo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtDescripcionObjectivo.TabIndex = 17
		Me.txtDescripcionObjectivo.Tag = "9"
		Me.txtDescripcionObjectivo.AcceptsReturn = True
		Me.txtDescripcionObjectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcionObjectivo.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcionObjectivo.CausesValidation = True
		Me.txtDescripcionObjectivo.Enabled = True
		Me.txtDescripcionObjectivo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcionObjectivo.HideSelection = True
		Me.txtDescripcionObjectivo.ReadOnly = False
		Me.txtDescripcionObjectivo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcionObjectivo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcionObjectivo.TabStop = True
		Me.txtDescripcionObjectivo.Visible = True
		Me.txtDescripcionObjectivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcionObjectivo.Name = "txtDescripcionObjectivo"
		Me.txtObservaciones.AutoSize = False
		Me.txtObservaciones.Size = New System.Drawing.Size(545, 43)
		Me.txtObservaciones.Location = New System.Drawing.Point(124, 250)
		Me.txtObservaciones.Maxlength = 200
		Me.txtObservaciones.MultiLine = True
		Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtObservaciones.TabIndex = 19
		Me.txtObservaciones.Tag = "10"
		Me.txtObservaciones.AcceptsReturn = True
		Me.txtObservaciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservaciones.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservaciones.CausesValidation = True
		Me.txtObservaciones.Enabled = True
		Me.txtObservaciones.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservaciones.HideSelection = True
		Me.txtObservaciones.ReadOnly = False
		Me.txtObservaciones.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservaciones.TabStop = True
		Me.txtObservaciones.Visible = True
		Me.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservaciones.Name = "txtObservaciones"
		Me.txtCentroTrabajo.AutoSize = False
		Me.txtCentroTrabajo.Size = New System.Drawing.Size(42, 19)
		Me.txtCentroTrabajo.Location = New System.Drawing.Point(124, 298)
		Me.txtCentroTrabajo.Maxlength = 4
		Me.txtCentroTrabajo.TabIndex = 21
		Me.txtCentroTrabajo.Tag = "11"
		Me.txtCentroTrabajo.AcceptsReturn = True
		Me.txtCentroTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCentroTrabajo.BackColor = System.Drawing.SystemColors.Window
		Me.txtCentroTrabajo.CausesValidation = True
		Me.txtCentroTrabajo.Enabled = True
		Me.txtCentroTrabajo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCentroTrabajo.HideSelection = True
		Me.txtCentroTrabajo.ReadOnly = False
		Me.txtCentroTrabajo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCentroTrabajo.MultiLine = False
		Me.txtCentroTrabajo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCentroTrabajo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCentroTrabajo.TabStop = True
		Me.txtCentroTrabajo.Visible = True
		Me.txtCentroTrabajo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCentroTrabajo.Name = "txtCentroTrabajo"
		Me.Text11.AutoSize = False
		Me.Text11.BackColor = System.Drawing.Color.White
		Me.Text11.Enabled = False
		Me.Text11.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text11.Size = New System.Drawing.Size(502, 19)
		Me.Text11.Location = New System.Drawing.Point(169, 298)
		Me.Text11.TabIndex = 22
		Me.Text11.Tag = "^11"
		Me.Text11.AcceptsReturn = True
		Me.Text11.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text11.CausesValidation = True
		Me.Text11.HideSelection = True
		Me.Text11.ReadOnly = False
		Me.Text11.Maxlength = 0
		Me.Text11.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text11.MultiLine = False
		Me.Text11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text11.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text11.TabStop = True
		Me.Text11.Visible = True
		Me.Text11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text11.Name = "Text11"
		Me.txtPublicoObjetivo.AutoSize = False
		Me.txtPublicoObjetivo.Size = New System.Drawing.Size(547, 19)
		Me.txtPublicoObjetivo.Location = New System.Drawing.Point(124, 322)
		Me.txtPublicoObjetivo.Maxlength = 100
		Me.txtPublicoObjetivo.TabIndex = 24
		Me.txtPublicoObjetivo.Tag = "12"
		Me.txtPublicoObjetivo.AcceptsReturn = True
		Me.txtPublicoObjetivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPublicoObjetivo.BackColor = System.Drawing.SystemColors.Window
		Me.txtPublicoObjetivo.CausesValidation = True
		Me.txtPublicoObjetivo.Enabled = True
		Me.txtPublicoObjetivo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPublicoObjetivo.HideSelection = True
		Me.txtPublicoObjetivo.ReadOnly = False
		Me.txtPublicoObjetivo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPublicoObjetivo.MultiLine = False
		Me.txtPublicoObjetivo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPublicoObjetivo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPublicoObjetivo.TabStop = True
		Me.txtPublicoObjetivo.Visible = True
		Me.txtPublicoObjetivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPublicoObjetivo.Name = "txtPublicoObjetivo"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(499, 362)
		Me.cmdAceptar.TabIndex = 25
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(589, 362)
		Me.cmdGuardar.TabIndex = 26
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl1.Text = "C�digo"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Descripci�n"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Fecha inicio"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 4
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Fecha fin"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 86)
		Me.lbl4.TabIndex = 6
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Coste previsto"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 110)
		Me.lbl5.TabIndex = 8
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Coste real"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 134)
		Me.lbl6.TabIndex = 10
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "% financiado por 3eros"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 158)
		Me.lbl7.TabIndex = 12
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl8.Text = "Nombre terceros"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(10, 182)
		Me.lbl8.TabIndex = 14
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl9.Text = "Descripci�n objectivo"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(10, 206)
		Me.lbl9.TabIndex = 16
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl10.Text = "Observaciones"
		Me.lbl10.Size = New System.Drawing.Size(111, 15)
		Me.lbl10.Location = New System.Drawing.Point(10, 254)
		Me.lbl10.TabIndex = 18
		Me.lbl10.Tag = "10"
		Me.lbl10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl10.BackColor = System.Drawing.SystemColors.Control
		Me.lbl10.Enabled = True
		Me.lbl10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl10.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl10.UseMnemonic = True
		Me.lbl10.Visible = True
		Me.lbl10.AutoSize = False
		Me.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl10.Name = "lbl10"
		Me.lbl11.Text = "Centro trabajo"
		Me.lbl11.Size = New System.Drawing.Size(111, 15)
		Me.lbl11.Location = New System.Drawing.Point(10, 302)
		Me.lbl11.TabIndex = 20
		Me.lbl11.Tag = "11"
		Me.lbl11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl11.BackColor = System.Drawing.SystemColors.Control
		Me.lbl11.Enabled = True
		Me.lbl11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl11.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl11.UseMnemonic = True
		Me.lbl11.Visible = True
		Me.lbl11.AutoSize = False
		Me.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl11.Name = "lbl11"
		Me.lbl12.Text = "P�blico objetivo"
		Me.lbl12.Size = New System.Drawing.Size(111, 15)
		Me.lbl12.Location = New System.Drawing.Point(10, 326)
		Me.lbl12.TabIndex = 23
		Me.lbl12.Tag = "12"
		Me.lbl12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl12.BackColor = System.Drawing.SystemColors.Control
		Me.lbl12.Enabled = True
		Me.lbl12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl12.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl12.UseMnemonic = True
		Me.lbl12.Visible = True
		Me.lbl12.AutoSize = False
		Me.lbl12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl12.Name = "lbl12"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 361)
		Me.lblLock.TabIndex = 27
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 355
		Me.Line1.Y2 = 355
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 356
		Me.Line2.Y2 = 356
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFinanciadoPor3eros, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtCosteReal, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtCostePrevisto, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaFin, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaInicio, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtCodigo)
		Me.Controls.Add(txtDescripcion)
		Me.Controls.Add(txtFechaInicio)
		Me.Controls.Add(txtFechaFin)
		Me.Controls.Add(txtCostePrevisto)
		Me.Controls.Add(txtCosteReal)
		Me.Controls.Add(txtFinanciadoPor3eros)
		Me.Controls.Add(txtNombreTerceros)
		Me.Controls.Add(txtDescripcionObjectivo)
		Me.Controls.Add(txtObservaciones)
		Me.Controls.Add(txtCentroTrabajo)
		Me.Controls.Add(Text11)
		Me.Controls.Add(txtPublicoObjetivo)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(lbl10)
		Me.Controls.Add(lbl11)
		Me.Controls.Add(lbl12)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class