Option Strict Off
Option Explicit On
Friend Class frmConsultaAccions
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	
	Public Overrides Sub Inici()
		'    ResetForm Me
	End Sub
	
	
	
	
	
	Private Sub ChBAssolit_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBAssolit.ClickEvent
		CarregaGrid()
	End Sub
	
	Private Sub ChBCanonge_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBCanonge.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBDesestimat_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBDesestimat.ClickEvent
		CarregaGrid()
	End Sub
	
	Private Sub ChBDonacioPuntual_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBDonacioPuntual.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBDonant_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBDonant.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBEnCurs_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBEnCurs.ClickEvent
		CarregaGrid()
	End Sub
	
	Private Sub ChBEsponsor_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBEsponsor.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBGarden_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBGarden.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBImpremta_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBImpremta.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBIncorpora_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBIncorpora.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBJardineria_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBJardineria.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBManipulats_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBManipulats.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBManteniment_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBManteniment.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBNeteja_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBNeteja.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBNoAssolit_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBNoAssolit.ClickEvent
		CarregaGrid()
	End Sub
	
	Private Sub ChBPendent_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBPendent.ClickEvent
		CarregaGrid()
	End Sub
	
	Private Sub ChBTotesLesAccions_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBTotesLesAccions.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then
			ChBIncorpora.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBNeteja.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBManipulats.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBImpremta.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBJardineria.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBManteniment.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBGarden.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBCanonge.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBVi.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBDonant.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBDonacioPuntual.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBEsponsor.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			ChBVoluntariat.Value = XtremeSuiteControls.CheckBoxValue.xtpUnchecked
			GrupInteres.Enabled = False
		Else
			GrupInteres.Enabled = True
		End If
		CarregaGrid()
	End Sub
	
	Private Sub ChBVi_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBVi.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub ChBVoluntariat_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ChBVoluntariat.ClickEvent
		If ChBTotesLesAccions.Value = XtremeSuiteControls.CheckBoxValue.xtpChecked Then Exit Sub
		CarregaGrid()
	End Sub
	
	Private Sub cmbEstadoAccion_DropDownClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstadoAccion.DropDownClosed
		CarregaGrid()
	End Sub
	
	Private Sub cmdExit_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.ClickEvent
		Me.Unload()
	End Sub
	
	
	Private Sub GrdAccions_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdAccions.DoubleClick
		If rbAccions.Value = True Then
			If GrdAccions.ActiveCell.Row = 0 Then Exit Sub
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 2). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			ObreFormulari(frmAccions, "frmAccions", Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 1) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 3))
		Else
			If GrdAccions.ActiveCell.Row = 0 Then Exit Sub
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 4). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 2). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			ObreFormulari(frmObjectius, "frmObjectius", Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 1) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 3) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 4))
		End If
		
	End Sub
	
	Private Sub GrdAccions_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdAccions.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdAccions", PopMenuStyle.tsPrimaryMenu, True)
				If rbAccions.Value = True Then
					.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar Acci�n",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				Else
					.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar Objectiu",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				End If
				
				.MenuItems.Item("E").Enabled = GrdAccions.Rows > 1 And PermisConsulta("frmAccions", e_Permisos.Consultes)
				'.MenuItems.Item("D").Enabled = GrdAccions.Rows > 1 And PermisConsulta("frmAccions", E_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Exportar a Excel",  ,  , XPIcon("ExportExcel"),  ,  ,  ,  , "XE")
			End With
		End With
		XpExecutaMenu(Me, "GrdAccions", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub rbAccions_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles rbAccions.ClickEvent
		GrdAccions.Tag = "AMPANS#31"
		lbl8.Visible = True
		cmbEstadoAccion.Visible = True
		GroupBoxObjectius.Visible = False
		GrupInteres.Visible = True
		ChBTotesLesAccions.Visible = True
		IniciForm(Me)
		CarregaGrid()
	End Sub
	
	Private Sub rbObjectius_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles rbObjectius.ClickEvent
		GrdAccions.Tag = "AMPANS#32"
		lbl8.Visible = False
		cmbEstadoAccion.Visible = False
		GrupInteres.Visible = False
		GroupBoxObjectius.Visible = True
		ChBTotesLesAccions.Visible = False
		IniciForm(Me)
		CarregaGrid()
	End Sub
	
	Private Sub txtCentreTreball_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreTreball.DoubleClick
		ConsultaTaula(Me, txtCentreTreball)
	End Sub
	
	Private Sub txtCentreTreball_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreTreball.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtCentreTreball_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreTreball.LostFocus
		CarregaGrid()
	End Sub
	
	Private Sub txtCentreTreball_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentreTreball.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtResponsable_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.DoubleClick
		ConsultaTaula(Me, txtResponsable)
	End Sub
	
	Private Sub frmConsultaAccions_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		
		ControlKey(Me, KeyAscii)
		
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmConsultaAccions_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		cmdExit.Picture = SetIcon("Exit", 16)
		CarregaComboNou(Me, cmbEstadoAccion)
		SituaCombo(cmbEstadoAccion, CStr(1))
		CarregaGrid()
	End Sub
	
	Private Sub txtResponsable_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtResponsable_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.LostFocus
		CarregaGrid()
	End Sub
	
	Private Sub txtResponsable_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtResponsable.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub CarregaGrid()
		If rbAccions.Value = True Then
			'CarregaFGrid GrdAccions, "CGAC^CRM", txtResponsable & S & txtCentreTreball & S & cmbEstadoAccion.Columns(1).Value
			CarregaFGrid(GrdAccions, "CGACF^CRM", txtResponsable.Text & S & txtCentreTreball.Text & S & cmbEstadoAccion.Columns(1).Value & S & ChBIncorpora.Value & S & ChBNeteja.Value & S & ChBManipulats.Value & S & ChBImpremta.Value & S & ChBJardineria.Value & S & ChBManteniment.Value & S & ChBGarden.Value & S & ChBCanonge.Value & S & ChBVi.Value & S & ChBDonant.Value & S & ChBDonacioPuntual.Value & S & ChBEsponsor.Value & S & ChBVoluntariat.Value & S & ChBTotesLesAccions.Value)
		Else
			CarregaFGrid(GrdAccions, "CGOBJ^CRM", txtResponsable.Text & S & txtCentreTreball.Text & S & ChBPendent.Value & S & ChBEnCurs.Value & S & ChBAssolit.Value & S & ChBNoAssolit.Value & S & ChBDesestimat.Value)
		End If
	End Sub
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Select Case NomMenu
			Case "GrdAccions"
				Select Case KeyMenu
					Case "E"
						If GrdAccions.ActiveCell.Row = 0 Then Exit Sub
						If rbAccions.Value = True Then
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 2). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							ObreFormulari(frmAccions, "frmAccions", Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 1) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 3))
						Else
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 4). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 2). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
							ObreFormulari(frmObjectius, "frmObjectius", Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 1) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 3) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 4))
						End If
					Case "XE"
						ExportFG(GrdAccions, ETipExport.eExcel, True)
				End Select
				
				
			Case "GrdObjectius"
				'        Select Case KeyMenu
				'             Case "N"
				'                ObreFormulari frmObjectius, "frmObjectius", txtEntidad & S & txtSucursal
				'            Case "E"
				'                If GrdObjectius.ActiveCell.Row = 0 Then Exit Sub
				'                    ObreFormulari frmObjectius, "frmObjectius", Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 1) & S & Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 3) & S & Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 4)
		End Select
	End Sub
End Class
