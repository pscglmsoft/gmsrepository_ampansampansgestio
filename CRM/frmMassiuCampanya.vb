Option Strict Off
Option Explicit On
Friend Class frmMassiuCampanya
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		
		If VerificaCamps = False Then Exit Sub
		
		CacheNetejaParametres()
		MCache.P1 = txtCampana.Text
		MCache.P2 = txtFechaAccion.Text
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto cmbEstadoAccion.Columns().Value. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MCache.P3 = cmbEstadoAccion.Columns(1).Value
		MCache.P4 = txtResponsable.Text
		MCache.P5 = txtAccion.Text
		MCache.P6 = txtSubaccion.Text
		MCache.P7 = txtResultadoObjectivoCamp.Text
		CacheXecute("D GRAVCAMP^CRM")
		
		xMsgBox("Events gravats correctament !!! ", MsgBoxStyle.Information, Me.Text)
		Inici()
		
	End Sub
	
	Private Sub frmMassiuCampanya_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		
		If KeyAscii = 27 Then
			ControlEscape()
		End If
		ControlKey(Me, KeyAscii)
		
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub cmdBorra_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdBorra.ClickEvent
		Dim Resp As Short
		Dim Enti As String
		Dim Campanya As Short
		CacheNetejaParametres()
		
		If txtCampana.Text = "" Then Exit Sub
		If txtEntidad.Text = "" Then Exit Sub
		If txtSucursal.Text = "" Then Exit Sub
		
		
		MCache.P1 = txtCampana.Text
		MCache.P2 = txtEntidad.Text
		MCache.P3 = txtSucursal.Text
		
		'If GrdEntitats.Cell(GrdEntitats.ActiveCell.Row, 1).Text = "0" Then Exit Sub
		Resp = xMsgBox("�Vols eliminar la Entitat/Sucursal ?", MsgBoxStyle.Information + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, Me.Text)
		If Resp = MsgBoxResult.Yes Then
			CacheXecute("D ELIMENTI^CRM")
			CarregaEntitats()
			txtEntidad.Text = ""
			Text1.Text = ""
			txtSucursal.Text = ""
			Text2.Text = ""
		End If
	End Sub
	
	Private Sub CmdInserta_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CmdInserta.ClickEvent
		CacheNetejaParametres()
		MCache.P1 = txtCampana.Text
		MCache.P2 = txtEntidad.Text
		MCache.P3 = txtSucursal.Text
		MCache.P4 = txtContacto.Text
		
		CacheXecute("D GRAVENTI^CRM")
		CarregaEntitats()
		txtEntidad.Text = ""
		Text1.Text = ""
		txtSucursal.Text = ""
		Text2.Text = ""
		txtContacto.Text = ""
		Text4.Text = ""
	End Sub
	
	Private Sub frmMassiuCampanya_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		
		CmdInserta.Picture = SetIcon("Plus", 16)
		cmdBorra.Picture = SetIcon("Delete", 16)
		
		CarregaComboNou(Me, cmbEstadoAccion)
	End Sub
	
	
	Private Sub frmMassiuCampanya_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		CacheXecute("D BUIDA^CRM")
	End Sub
	
	Private Sub GrdEntitats_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdEntitats.Click
		If GrdEntitats.ActiveCell.Row = 0 Then Exit Sub
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtEntidad.Text = Piece(GrdEntitats.Cell(GrdEntitats.ActiveCell.Row, 1).Text, "|", 2)
		DisplayDescripcio(Me, txtEntidad)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtSucursal.Text = Piece(GrdEntitats.Cell(GrdEntitats.ActiveCell.Row, 1).Text, "|", 3)
		DisplayDescripcio(Me, txtSucursal, txtEntidad.Text & S & "S")
	End Sub
	
	Private Sub txtAccion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtAccion.DoubleClick
		ConsultaTaula(Me, txtAccion,  ,  , "S WL=$$EVENT^CRM($P(%REGC,S,1))")
	End Sub
	
	Private Sub txtAccion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtAccion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCampana_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCampana.DoubleClick
		ConsultaTaula(Me, txtCampana)
	End Sub
	
	Private Sub txtCampana_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCampana.LostFocus
		'UPGRADE_NOTE: Registre se actualiz� a Registre_Renamed. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim Registre_Renamed As String
		
		If txtCampana.Text = "" Then Exit Sub
		
		CacheNetejaParametres()
		MCache.P1 = txtCampana.Text
		Registre_Renamed = CacheXecute("S VALUE=$G(^CRMCAMPANYES(EMP,P1))")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If txtFechaAccion.Text = "" Then txtFechaAccion.Text = Piece(Registre_Renamed, S, 3)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If txtResponsable.Text = "" Then txtResponsable.Text = Piece(Registre_Renamed, S, 6)
		DisplayDescripcio(Me, txtResponsable)
		If cmbEstadoAccion.Columns(1).Value = "" Then SituaCombo(cmbEstadoAccion, CStr(2))
		txtAccion.Text = CStr(10)
		DisplayDescripcio(Me, txtAccion)
		txtSubaccion.Text = "1"
		DisplayDescripcio(Me, txtSubaccion, txtAccion.Text)
		
	End Sub
	
	Private Sub txtCampana_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCampana.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtContacto_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtContacto.DoubleClick
		ConsultaTaula(Me, txtContacto,  , txtEntidad.Text & S & "S" & S & txtSucursal.Text & S & "C")
	End Sub
	
	Private Sub txtContacto_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtContacto.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtEntidad.Text & S & "S" & S & txtSucursal.Text & S & "C")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEntidad_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidad.DoubleClick
		ConsultaTaula(Me, txtEntidad)
	End Sub
	
	Private Sub txtEntidad_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidad.LostFocus
		If txtEntidad.Text = "" Then Exit Sub
		CacheNetejaParametres()
		MCache.P1 = txtEntidad.Text
		
		txtSucursal.Text = CacheXecute("S VALUE=$$SUC^CRM($P(P1,S,1))")
		
	End Sub
	
	Private Sub txtEntidad_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEntidad.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtResponsable_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResponsable.DoubleClick
		ConsultaTaula(Me, txtResponsable)
	End Sub
	
	Private Sub txtResponsable_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtResponsable.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	Private Sub txtResultadoObjectivoCamp_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtResultadoObjectivoCamp.DoubleClick
		ConsultaTaula(Me, txtResultadoObjectivoCamp,  , 6 & S & "R")
	End Sub
	
	Private Sub txtResultadoObjectivoCamp_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtResultadoObjectivoCamp.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, 6 & S & "R")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSubaccion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubaccion.DoubleClick
		ConsultaTaula(Me, txtSubaccion,  , txtAccion.Text)
	End Sub
	
	Private Sub txtSubaccion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSubaccion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtAccion.Text)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSucursal_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.DoubleClick
		ConsultaTaula(Me, txtSucursal,  , txtEntidad.Text & S & "S")
	End Sub
	
	
	Private Sub txtSucursal_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSucursal.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtEntidad.Text & S & "S")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Sub CarregaEntitats()
		CarregaFGrid(GrdEntitats, "ENTI^CRM", txtCampana.Text)
		GrdEntitats.Column(1).Sort(FlexCell.SortEnum.Ascending)
	End Sub
	
	Private Sub NetejaCamps()
		txtCampana.Text = ""
		Text6.Text = ""
		txtFechaAccion.Text = ""
		cmbEstadoAccion.Text = ""
		txtResponsable.Text = ""
		NetejaCamps()
	End Sub
	
	Sub ControlEscape()
		Dim Resp As MsgBoxResult
		Resp = xMsgBox("�Quiere guardar los datos?", MsgBoxStyle.Information + MsgBoxStyle.YesNoCancel, Me.Text)
		If Resp = MsgBoxResult.Yes Then
			cmdAceptar_ClickEvent(cmdAceptar, New System.EventArgs())
		ElseIf Resp = MsgBoxResult.Cancel Then 
			Exit Sub
		Else
			Me.Unload()
		End If
	End Sub
	
	Private Function VerificaCamps() As Boolean
		VerificaCamps = True
		
		If txtCampana.Text = "" Then
			xMsgBox("Falta informar l'Event", MsgBoxStyle.Information, Me.Text)
			txtCampana.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If txtFechaAccion.Text = "" Then
			xMsgBox("Falta informar la Data Acci�", MsgBoxStyle.Information, Me.Text)
			txtFechaAccion.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If cmbEstadoAccion.Text = "" Then
			xMsgBox("Falta informar l'Estat de l'acci�", MsgBoxStyle.Information, Me.Text)
			cmbEstadoAccion.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If txtResponsable.Text = "" Then
			xMsgBox("Falta informar el Responsable", MsgBoxStyle.Information, Me.Text)
			txtResponsable.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If txtAccion.Text = "" Then
			xMsgBox("Falta informar l'Acci�", MsgBoxStyle.Information, Me.Text)
			txtAccion.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If txtSubaccion.Text = "" Then
			xMsgBox("Falta informar la Subacci�", MsgBoxStyle.Information, Me.Text)
			txtSubaccion.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If txtResultadoObjectivoCamp.Text = "" Then
			xMsgBox("Falta informar el Resultat ", MsgBoxStyle.Information, Me.Text)
			txtResultadoObjectivoCamp.Focus()
			VerificaCamps = False
			Exit Function
		End If
		
		If GrdEntitats.Rows <= 1 Then
			xMsgBox("Falta entrar Entitats / Sucursals", MsgBoxStyle.Information, Me.Text)
			VerificaCamps = False
			Exit Function
		End If
		
		
		
	End Function
End Class
