Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmCampanyesNou
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		ResetForm(Me)
	End Sub
	
	Private Sub frmCampanyesNou_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmCampanyesNou_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmCampanyesNou_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmCampanyesNou_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtCodigo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.GotFocus
		XGotFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.DoubleClick
		ConsultaTaula(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodigo.LostFocus
		If txtCodigo.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodigo)
	End Sub
	
	Private Sub txtCodigo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodigo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtCodigo)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcion.GotFocus
		XGotFocus(Me, txtDescripcion)
	End Sub
	
	Private Sub txtDescripcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcion.LostFocus
		XLostFocus(Me, txtDescripcion)
	End Sub
	
	Private Sub txtDescripcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtDescripcion)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaInicio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaInicio.GotFocus
		XGotFocus(Me, txtFechaInicio)
	End Sub
	
	Private Sub txtFechaInicio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaInicio.LostFocus
		XLostFocus(Me, txtFechaInicio)
	End Sub
	
	Private Sub txtFechaFin_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaFin.GotFocus
		XGotFocus(Me, txtFechaFin)
	End Sub
	
	Private Sub txtFechaFin_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaFin.LostFocus
		XLostFocus(Me, txtFechaFin)
	End Sub
	
	Private Sub txtCostePrevisto_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCostePrevisto.GotFocus
		XGotFocus(Me, txtCostePrevisto)
	End Sub
	
	Private Sub txtCostePrevisto_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCostePrevisto.LostFocus
		XLostFocus(Me, txtCostePrevisto)
	End Sub
	
	Private Sub txtCostePrevisto_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCostePrevisto.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtCostePrevisto)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCosteReal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCosteReal.GotFocus
		XGotFocus(Me, txtCosteReal)
	End Sub
	
	Private Sub txtCosteReal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCosteReal.LostFocus
		XLostFocus(Me, txtCosteReal)
	End Sub
	
	Private Sub txtCosteReal_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCosteReal.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtCosteReal)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFinanciadoPor3eros_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFinanciadoPor3eros.GotFocus
		XGotFocus(Me, txtFinanciadoPor3eros)
	End Sub
	
	Private Sub txtFinanciadoPor3eros_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFinanciadoPor3eros.LostFocus
		XLostFocus(Me, txtFinanciadoPor3eros)
	End Sub
	
	Private Sub txtFinanciadoPor3eros_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFinanciadoPor3eros.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtFinanciadoPor3eros)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtNombreTerceros_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreTerceros.GotFocus
		XGotFocus(Me, txtNombreTerceros)
	End Sub
	
	Private Sub txtNombreTerceros_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNombreTerceros.LostFocus
		XLostFocus(Me, txtNombreTerceros)
	End Sub
	
	Private Sub txtNombreTerceros_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNombreTerceros.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtNombreTerceros)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcionObjectivo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcionObjectivo.GotFocus
		XGotFocus(Me, txtDescripcionObjectivo)
	End Sub
	
	Private Sub txtDescripcionObjectivo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcionObjectivo.LostFocus
		XLostFocus(Me, txtDescripcionObjectivo)
	End Sub
	
	Private Sub txtDescripcionObjectivo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcionObjectivo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtDescripcionObjectivo)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservaciones_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservaciones.GotFocus
		XGotFocus(Me, txtObservaciones)
	End Sub
	
	Private Sub txtObservaciones_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservaciones.LostFocus
		XLostFocus(Me, txtObservaciones)
	End Sub
	
	Private Sub txtObservaciones_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservaciones.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtObservaciones)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCentroTrabajo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentroTrabajo.GotFocus
		XGotFocus(Me, txtCentroTrabajo)
	End Sub
	
	Private Sub txtCentroTrabajo_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentroTrabajo.DoubleClick
		ConsultaTaula(Me, txtCentroTrabajo)
	End Sub
	
	Private Sub txtCentroTrabajo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentroTrabajo.LostFocus
		XLostFocus(Me, txtCentroTrabajo)
	End Sub
	
	Private Sub txtCentroTrabajo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentroTrabajo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtCentroTrabajo)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtCentroTrabajo)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPublicoObjetivo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPublicoObjetivo.GotFocus
		XGotFocus(Me, txtPublicoObjetivo)
	End Sub
	
	Private Sub txtPublicoObjetivo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPublicoObjetivo.LostFocus
		XLostFocus(Me, txtPublicoObjetivo)
	End Sub
	
	Private Sub txtPublicoObjetivo_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPublicoObjetivo.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtPublicoObjetivo)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
