<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class FrmImprimirTransport
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text38 As System.Windows.Forms.TextBox
	Public WithEvents txtTransportista As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdCancelar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtDataIni As AxDataControl.AxGmsData
	Public WithEvents txtDataFi As AxDataControl.AxGmsData
	Public WithEvents lbl38 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(FrmImprimirTransport))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.Text38 = New System.Windows.Forms.TextBox
		Me.txtTransportista = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdCancelar = New AxXtremeSuiteControls.AxPushButton
		Me.txtDataIni = New AxDataControl.AxGmsData
		Me.txtDataFi = New AxDataControl.AxGmsData
		Me.lbl38 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdCancelar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataIni, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataFi, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Hoja de trabajo"
		Me.ClientSize = New System.Drawing.Size(436, 114)
		Me.Location = New System.Drawing.Point(3, 25)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.MinimizeBox = False
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "FrmImprimirTransport"
		Me.Text38.AutoSize = False
		Me.Text38.BackColor = System.Drawing.Color.White
		Me.Text38.Enabled = False
		Me.Text38.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text38.Size = New System.Drawing.Size(292, 19)
		Me.Text38.Location = New System.Drawing.Point(133, 38)
		Me.Text38.TabIndex = 7
		Me.Text38.Tag = "^2"
		Me.Text38.AcceptsReturn = True
		Me.Text38.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text38.CausesValidation = True
		Me.Text38.HideSelection = True
		Me.Text38.ReadOnly = False
		Me.Text38.Maxlength = 0
		Me.Text38.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text38.MultiLine = False
		Me.Text38.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text38.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text38.TabStop = True
		Me.Text38.Visible = True
		Me.Text38.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text38.Name = "Text38"
		Me.txtTransportista.AutoSize = False
		Me.txtTransportista.Size = New System.Drawing.Size(42, 19)
		Me.txtTransportista.Location = New System.Drawing.Point(88, 38)
		Me.txtTransportista.Maxlength = 4
		Me.txtTransportista.TabIndex = 6
		Me.txtTransportista.Tag = "2####T-TRANSPORTISTA#1#1"
		Me.txtTransportista.AcceptsReturn = True
		Me.txtTransportista.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTransportista.BackColor = System.Drawing.SystemColors.Window
		Me.txtTransportista.CausesValidation = True
		Me.txtTransportista.Enabled = True
		Me.txtTransportista.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTransportista.HideSelection = True
		Me.txtTransportista.ReadOnly = False
		Me.txtTransportista.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTransportista.MultiLine = False
		Me.txtTransportista.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTransportista.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTransportista.TabStop = True
		Me.txtTransportista.Visible = True
		Me.txtTransportista.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTransportista.Name = "txtTransportista"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(250, 80)
		Me.cmdAceptar.TabIndex = 0
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdCancelar.OcxState = CType(resources.GetObject("cmdCancelar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdCancelar.Size = New System.Drawing.Size(83, 27)
		Me.cmdCancelar.Location = New System.Drawing.Point(340, 80)
		Me.cmdCancelar.TabIndex = 1
		Me.cmdCancelar.Name = "cmdCancelar"
		txtDataIni.OcxState = CType(resources.GetObject("txtDataIni.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataIni.Size = New System.Drawing.Size(87, 19)
		Me.txtDataIni.Location = New System.Drawing.Point(88, 12)
		Me.txtDataIni.TabIndex = 2
		Me.txtDataIni.Name = "txtDataIni"
		txtDataFi.OcxState = CType(resources.GetObject("txtDataFi.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataFi.Size = New System.Drawing.Size(87, 19)
		Me.txtDataFi.Location = New System.Drawing.Point(340, 12)
		Me.txtDataFi.TabIndex = 3
		Me.txtDataFi.Name = "txtDataFi"
		Me.lbl38.Text = "Transportista"
		Me.lbl38.Size = New System.Drawing.Size(111, 15)
		Me.lbl38.Location = New System.Drawing.Point(8, 42)
		Me.lbl38.TabIndex = 8
		Me.lbl38.Tag = "38"
		Me.lbl38.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl38.BackColor = System.Drawing.SystemColors.Control
		Me.lbl38.Enabled = True
		Me.lbl38.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl38.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl38.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl38.UseMnemonic = True
		Me.lbl38.Visible = True
		Me.lbl38.AutoSize = False
		Me.lbl38.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl38.Name = "lbl38"
		Me.lbl4.Text = "Fecha Inicio"
		Me.lbl4.Size = New System.Drawing.Size(63, 15)
		Me.lbl4.Location = New System.Drawing.Point(8, 16)
		Me.lbl4.TabIndex = 5
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.Label2.Text = "Fecha Fin"
		Me.Label2.Size = New System.Drawing.Size(61, 15)
		Me.Label2.Location = New System.Drawing.Point(280, 16)
		Me.Label2.TabIndex = 4
		Me.Label2.Tag = "4"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		CType(Me.txtDataFi, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataIni, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdCancelar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(Text38)
		Me.Controls.Add(txtTransportista)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdCancelar)
		Me.Controls.Add(txtDataIni)
		Me.Controls.Add(txtDataFi)
		Me.Controls.Add(lbl38)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(Label2)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class