<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLoteria
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text2 As System.Windows.Forms.TextBox
	Public WithEvents txtServei As System.Windows.Forms.TextBox
	Public WithEvents txtCodi As System.Windows.Forms.TextBox
	Public WithEvents txtAny As System.Windows.Forms.TextBox
	Public WithEvents txtEntitat As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents cmbEstat As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents txtCentre As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtQuantitat As System.Windows.Forms.TextBox
	Public WithEvents txtPreu As System.Windows.Forms.TextBox
	Public WithEvents txtImport As System.Windows.Forms.TextBox
	Public WithEvents txtNumeroButlletes As System.Windows.Forms.TextBox
	Public WithEvents txtObservacions As System.Windows.Forms.TextBox
	Public WithEvents txtDataEntrega As AxDataControl.AxGmsData
	Public WithEvents txtDataPagat As AxDataControl.AxGmsData
	Public WithEvents txtImportEfectiu As System.Windows.Forms.TextBox
	Public WithEvents txtImportBanc As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdImprimir As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl10 As System.Windows.Forms.Label
	Public WithEvents lbl11 As System.Windows.Forms.Label
	Public WithEvents lbl12 As System.Windows.Forms.Label
	Public WithEvents lbl13 As System.Windows.Forms.Label
	Public WithEvents lbl14 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLoteria))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.Text2 = New System.Windows.Forms.TextBox
		Me.txtServei = New System.Windows.Forms.TextBox
		Me.txtCodi = New System.Windows.Forms.TextBox
		Me.txtAny = New System.Windows.Forms.TextBox
		Me.txtEntitat = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.cmbEstat = New AxSSDataWidgets_B.AxSSDBCombo
		Me.txtCentre = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtQuantitat = New System.Windows.Forms.TextBox
		Me.txtPreu = New System.Windows.Forms.TextBox
		Me.txtImport = New System.Windows.Forms.TextBox
		Me.txtNumeroButlletes = New System.Windows.Forms.TextBox
		Me.txtObservacions = New System.Windows.Forms.TextBox
		Me.txtDataEntrega = New AxDataControl.AxGmsData
		Me.txtDataPagat = New AxDataControl.AxGmsData
		Me.txtImportEfectiu = New System.Windows.Forms.TextBox
		Me.txtImportBanc = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdImprimir = New AxXtremeSuiteControls.AxPushButton
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl10 = New System.Windows.Forms.Label
		Me.lbl11 = New System.Windows.Forms.Label
		Me.lbl12 = New System.Windows.Forms.Label
		Me.lbl13 = New System.Windows.Forms.Label
		Me.lbl14 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cmbEstat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataEntrega, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDataPagat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdImprimir, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Loteria Ampans"
		Me.ClientSize = New System.Drawing.Size(664, 356)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "G-LOTERIA"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmLoteria"
		Me.Text2.AutoSize = False
		Me.Text2.BackColor = System.Drawing.Color.White
		Me.Text2.Enabled = False
		Me.Text2.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text2.Size = New System.Drawing.Size(310, 19)
		Me.Text2.Location = New System.Drawing.Point(169, 84)
		Me.Text2.TabIndex = 13
		Me.Text2.Tag = "^15"
		Me.Text2.AcceptsReturn = True
		Me.Text2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text2.CausesValidation = True
		Me.Text2.HideSelection = True
		Me.Text2.ReadOnly = False
		Me.Text2.Maxlength = 0
		Me.Text2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text2.MultiLine = False
		Me.Text2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text2.TabStop = True
		Me.Text2.Visible = True
		Me.Text2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text2.Name = "Text2"
		Me.txtServei.AutoSize = False
		Me.txtServei.Size = New System.Drawing.Size(42, 19)
		Me.txtServei.Location = New System.Drawing.Point(124, 84)
		Me.txtServei.Maxlength = 4
		Me.txtServei.TabIndex = 12
		Me.txtServei.Tag = "15"
		Me.txtServei.AcceptsReturn = True
		Me.txtServei.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtServei.BackColor = System.Drawing.SystemColors.Window
		Me.txtServei.CausesValidation = True
		Me.txtServei.Enabled = True
		Me.txtServei.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtServei.HideSelection = True
		Me.txtServei.ReadOnly = False
		Me.txtServei.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtServei.MultiLine = False
		Me.txtServei.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtServei.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtServei.TabStop = True
		Me.txtServei.Visible = True
		Me.txtServei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtServei.Name = "txtServei"
		Me.txtCodi.AutoSize = False
		Me.txtCodi.Size = New System.Drawing.Size(42, 19)
		Me.txtCodi.Location = New System.Drawing.Point(124, 10)
		Me.txtCodi.Maxlength = 4
		Me.txtCodi.TabIndex = 1
		Me.txtCodi.Tag = "*1"
		Me.txtCodi.AcceptsReturn = True
		Me.txtCodi.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodi.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodi.CausesValidation = True
		Me.txtCodi.Enabled = True
		Me.txtCodi.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodi.HideSelection = True
		Me.txtCodi.ReadOnly = False
		Me.txtCodi.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodi.MultiLine = False
		Me.txtCodi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodi.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodi.TabStop = True
		Me.txtCodi.Visible = True
		Me.txtCodi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodi.Name = "txtCodi"
		Me.txtAny.AutoSize = False
		Me.txtAny.Size = New System.Drawing.Size(42, 19)
		Me.txtAny.Location = New System.Drawing.Point(232, 10)
		Me.txtAny.Maxlength = 4
		Me.txtAny.TabIndex = 3
		Me.txtAny.Tag = "2"
		Me.txtAny.AcceptsReturn = True
		Me.txtAny.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtAny.BackColor = System.Drawing.SystemColors.Window
		Me.txtAny.CausesValidation = True
		Me.txtAny.Enabled = True
		Me.txtAny.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtAny.HideSelection = True
		Me.txtAny.ReadOnly = False
		Me.txtAny.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtAny.MultiLine = False
		Me.txtAny.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtAny.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtAny.TabStop = True
		Me.txtAny.Visible = True
		Me.txtAny.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtAny.Name = "txtAny"
		Me.txtEntitat.AutoSize = False
		Me.txtEntitat.Size = New System.Drawing.Size(83, 19)
		Me.txtEntitat.Location = New System.Drawing.Point(124, 36)
		Me.txtEntitat.Maxlength = 8
		Me.txtEntitat.TabIndex = 5
		Me.txtEntitat.Tag = "3"
		Me.txtEntitat.AcceptsReturn = True
		Me.txtEntitat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEntitat.BackColor = System.Drawing.SystemColors.Window
		Me.txtEntitat.CausesValidation = True
		Me.txtEntitat.Enabled = True
		Me.txtEntitat.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEntitat.HideSelection = True
		Me.txtEntitat.ReadOnly = False
		Me.txtEntitat.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEntitat.MultiLine = False
		Me.txtEntitat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEntitat.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEntitat.TabStop = True
		Me.txtEntitat.Visible = True
		Me.txtEntitat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEntitat.Name = "txtEntitat"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(448, 19)
		Me.Text3.Location = New System.Drawing.Point(210, 36)
		Me.Text3.TabIndex = 6
		Me.Text3.Tag = "^3"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		cmbEstat.OcxState = CType(resources.GetObject("cmbEstat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbEstat.Size = New System.Drawing.Size(145, 19)
		Me.cmbEstat.Location = New System.Drawing.Point(512, 10)
		Me.cmbEstat.TabIndex = 8
		Me.cmbEstat.Name = "cmbEstat"
		Me.txtCentre.AutoSize = False
		Me.txtCentre.Size = New System.Drawing.Size(42, 19)
		Me.txtCentre.Location = New System.Drawing.Point(124, 60)
		Me.txtCentre.Maxlength = 4
		Me.txtCentre.TabIndex = 10
		Me.txtCentre.Tag = "5"
		Me.txtCentre.AcceptsReturn = True
		Me.txtCentre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCentre.BackColor = System.Drawing.SystemColors.Window
		Me.txtCentre.CausesValidation = True
		Me.txtCentre.Enabled = True
		Me.txtCentre.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCentre.HideSelection = True
		Me.txtCentre.ReadOnly = False
		Me.txtCentre.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCentre.MultiLine = False
		Me.txtCentre.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCentre.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCentre.TabStop = True
		Me.txtCentre.Visible = True
		Me.txtCentre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCentre.Name = "txtCentre"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(310, 19)
		Me.Text5.Location = New System.Drawing.Point(169, 60)
		Me.Text5.TabIndex = 11
		Me.Text5.Tag = "^5"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtQuantitat.AutoSize = False
		Me.txtQuantitat.Size = New System.Drawing.Size(42, 19)
		Me.txtQuantitat.Location = New System.Drawing.Point(124, 108)
		Me.txtQuantitat.Maxlength = 4
		Me.txtQuantitat.TabIndex = 15
		Me.txtQuantitat.Tag = "6"
		Me.txtQuantitat.AcceptsReturn = True
		Me.txtQuantitat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtQuantitat.BackColor = System.Drawing.SystemColors.Window
		Me.txtQuantitat.CausesValidation = True
		Me.txtQuantitat.Enabled = True
		Me.txtQuantitat.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtQuantitat.HideSelection = True
		Me.txtQuantitat.ReadOnly = False
		Me.txtQuantitat.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtQuantitat.MultiLine = False
		Me.txtQuantitat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtQuantitat.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtQuantitat.TabStop = True
		Me.txtQuantitat.Visible = True
		Me.txtQuantitat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtQuantitat.Name = "txtQuantitat"
		Me.txtPreu.AutoSize = False
		Me.txtPreu.Enabled = False
		Me.txtPreu.Size = New System.Drawing.Size(21, 19)
		Me.txtPreu.Location = New System.Drawing.Point(286, 108)
		Me.txtPreu.ReadOnly = True
		Me.txtPreu.Maxlength = 2
		Me.txtPreu.TabIndex = 17
		Me.txtPreu.Tag = "7"
		Me.txtPreu.AcceptsReturn = True
		Me.txtPreu.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPreu.BackColor = System.Drawing.SystemColors.Window
		Me.txtPreu.CausesValidation = True
		Me.txtPreu.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPreu.HideSelection = True
		Me.txtPreu.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPreu.MultiLine = False
		Me.txtPreu.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPreu.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPreu.TabStop = True
		Me.txtPreu.Visible = True
		Me.txtPreu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPreu.Name = "txtPreu"
		Me.txtImport.AutoSize = False
		Me.txtImport.Enabled = False
		Me.txtImport.Size = New System.Drawing.Size(62, 19)
		Me.txtImport.Location = New System.Drawing.Point(416, 108)
		Me.txtImport.ReadOnly = True
		Me.txtImport.Maxlength = 6
		Me.txtImport.TabIndex = 19
		Me.txtImport.Tag = "8"
		Me.txtImport.AcceptsReturn = True
		Me.txtImport.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtImport.BackColor = System.Drawing.SystemColors.Window
		Me.txtImport.CausesValidation = True
		Me.txtImport.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtImport.HideSelection = True
		Me.txtImport.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtImport.MultiLine = False
		Me.txtImport.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtImport.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtImport.TabStop = True
		Me.txtImport.Visible = True
		Me.txtImport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtImport.Name = "txtImport"
		Me.txtNumeroButlletes.AutoSize = False
		Me.txtNumeroButlletes.Size = New System.Drawing.Size(535, 19)
		Me.txtNumeroButlletes.Location = New System.Drawing.Point(124, 132)
		Me.txtNumeroButlletes.Maxlength = 100
		Me.txtNumeroButlletes.TabIndex = 21
		Me.txtNumeroButlletes.Tag = "9"
		Me.txtNumeroButlletes.AcceptsReturn = True
		Me.txtNumeroButlletes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNumeroButlletes.BackColor = System.Drawing.SystemColors.Window
		Me.txtNumeroButlletes.CausesValidation = True
		Me.txtNumeroButlletes.Enabled = True
		Me.txtNumeroButlletes.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNumeroButlletes.HideSelection = True
		Me.txtNumeroButlletes.ReadOnly = False
		Me.txtNumeroButlletes.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNumeroButlletes.MultiLine = False
		Me.txtNumeroButlletes.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNumeroButlletes.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNumeroButlletes.TabStop = True
		Me.txtNumeroButlletes.Visible = True
		Me.txtNumeroButlletes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNumeroButlletes.Name = "txtNumeroButlletes"
		Me.txtObservacions.AutoSize = False
		Me.txtObservacions.Size = New System.Drawing.Size(534, 89)
		Me.txtObservacions.Location = New System.Drawing.Point(124, 156)
		Me.txtObservacions.Maxlength = 300
		Me.txtObservacions.MultiLine = True
		Me.txtObservacions.TabIndex = 23
		Me.txtObservacions.Tag = "10"
		Me.txtObservacions.AcceptsReturn = True
		Me.txtObservacions.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacions.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacions.CausesValidation = True
		Me.txtObservacions.Enabled = True
		Me.txtObservacions.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacions.HideSelection = True
		Me.txtObservacions.ReadOnly = False
		Me.txtObservacions.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacions.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacions.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtObservacions.TabStop = True
		Me.txtObservacions.Visible = True
		Me.txtObservacions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacions.Name = "txtObservacions"
		txtDataEntrega.OcxState = CType(resources.GetObject("txtDataEntrega.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataEntrega.Size = New System.Drawing.Size(87, 19)
		Me.txtDataEntrega.Location = New System.Drawing.Point(124, 252)
		Me.txtDataEntrega.TabIndex = 25
		Me.txtDataEntrega.Name = "txtDataEntrega"
		txtDataPagat.OcxState = CType(resources.GetObject("txtDataPagat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataPagat.Size = New System.Drawing.Size(87, 19)
		Me.txtDataPagat.Location = New System.Drawing.Point(124, 276)
		Me.txtDataPagat.TabIndex = 27
		Me.txtDataPagat.Name = "txtDataPagat"
		Me.txtImportEfectiu.AutoSize = False
		Me.txtImportEfectiu.Size = New System.Drawing.Size(62, 19)
		Me.txtImportEfectiu.Location = New System.Drawing.Point(312, 276)
		Me.txtImportEfectiu.Maxlength = 6
		Me.txtImportEfectiu.TabIndex = 29
		Me.txtImportEfectiu.Tag = "13"
		Me.txtImportEfectiu.AcceptsReturn = True
		Me.txtImportEfectiu.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtImportEfectiu.BackColor = System.Drawing.SystemColors.Window
		Me.txtImportEfectiu.CausesValidation = True
		Me.txtImportEfectiu.Enabled = True
		Me.txtImportEfectiu.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtImportEfectiu.HideSelection = True
		Me.txtImportEfectiu.ReadOnly = False
		Me.txtImportEfectiu.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtImportEfectiu.MultiLine = False
		Me.txtImportEfectiu.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtImportEfectiu.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtImportEfectiu.TabStop = True
		Me.txtImportEfectiu.Visible = True
		Me.txtImportEfectiu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtImportEfectiu.Name = "txtImportEfectiu"
		Me.txtImportBanc.AutoSize = False
		Me.txtImportBanc.Size = New System.Drawing.Size(62, 19)
		Me.txtImportBanc.Location = New System.Drawing.Point(472, 276)
		Me.txtImportBanc.Maxlength = 6
		Me.txtImportBanc.TabIndex = 31
		Me.txtImportBanc.Tag = "14"
		Me.txtImportBanc.AcceptsReturn = True
		Me.txtImportBanc.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtImportBanc.BackColor = System.Drawing.SystemColors.Window
		Me.txtImportBanc.CausesValidation = True
		Me.txtImportBanc.Enabled = True
		Me.txtImportBanc.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtImportBanc.HideSelection = True
		Me.txtImportBanc.ReadOnly = False
		Me.txtImportBanc.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtImportBanc.MultiLine = False
		Me.txtImportBanc.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtImportBanc.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtImportBanc.TabStop = True
		Me.txtImportBanc.Visible = True
		Me.txtImportBanc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtImportBanc.Name = "txtImportBanc"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(577, 322)
		Me.cmdAceptar.TabIndex = 32
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(377, 322)
		Me.cmdGuardar.TabIndex = 33
		Me.cmdGuardar.Visible = False
		Me.cmdGuardar.Name = "cmdGuardar"
		cmdImprimir.OcxState = CType(resources.GetObject("cmdImprimir.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdImprimir.Size = New System.Drawing.Size(79, 35)
		Me.cmdImprimir.Location = New System.Drawing.Point(578, 62)
		Me.cmdImprimir.TabIndex = 35
		Me.cmdImprimir.Name = "cmdImprimir"
		Me.Label1.Text = "Servei"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 88)
		Me.Label1.TabIndex = 36
		Me.Label1.Tag = "15"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Codi"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Any"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(208, 14)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Entitat"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 40)
		Me.lbl3.TabIndex = 4
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Estat"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(398, 14)
		Me.lbl4.TabIndex = 7
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Centre"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 64)
		Me.lbl5.TabIndex = 9
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Quantitat"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 112)
		Me.lbl6.TabIndex = 14
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "Preu"
		Me.lbl7.Size = New System.Drawing.Size(35, 15)
		Me.lbl7.Location = New System.Drawing.Point(246, 112)
		Me.lbl7.TabIndex = 16
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl8.Text = "Import"
		Me.lbl8.Size = New System.Drawing.Size(37, 15)
		Me.lbl8.Location = New System.Drawing.Point(370, 112)
		Me.lbl8.TabIndex = 18
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl9.Text = "Numero butlletes"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(10, 136)
		Me.lbl9.TabIndex = 20
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl10.Text = "Observacions"
		Me.lbl10.Size = New System.Drawing.Size(111, 15)
		Me.lbl10.Location = New System.Drawing.Point(10, 160)
		Me.lbl10.TabIndex = 22
		Me.lbl10.Tag = "10"
		Me.lbl10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl10.BackColor = System.Drawing.SystemColors.Control
		Me.lbl10.Enabled = True
		Me.lbl10.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl10.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl10.UseMnemonic = True
		Me.lbl10.Visible = True
		Me.lbl10.AutoSize = False
		Me.lbl10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl10.Name = "lbl10"
		Me.lbl11.Text = "Data entrega"
		Me.lbl11.Size = New System.Drawing.Size(111, 15)
		Me.lbl11.Location = New System.Drawing.Point(10, 256)
		Me.lbl11.TabIndex = 24
		Me.lbl11.Tag = "11"
		Me.lbl11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl11.BackColor = System.Drawing.SystemColors.Control
		Me.lbl11.Enabled = True
		Me.lbl11.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl11.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl11.UseMnemonic = True
		Me.lbl11.Visible = True
		Me.lbl11.AutoSize = False
		Me.lbl11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl11.Name = "lbl11"
		Me.lbl12.Text = "Data pagat"
		Me.lbl12.Size = New System.Drawing.Size(111, 15)
		Me.lbl12.Location = New System.Drawing.Point(10, 280)
		Me.lbl12.TabIndex = 26
		Me.lbl12.Tag = "12"
		Me.lbl12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl12.BackColor = System.Drawing.SystemColors.Control
		Me.lbl12.Enabled = True
		Me.lbl12.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl12.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl12.UseMnemonic = True
		Me.lbl12.Visible = True
		Me.lbl12.AutoSize = False
		Me.lbl12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl12.Name = "lbl12"
		Me.lbl13.Text = "Import efectiu"
		Me.lbl13.Size = New System.Drawing.Size(111, 15)
		Me.lbl13.Location = New System.Drawing.Point(232, 280)
		Me.lbl13.TabIndex = 28
		Me.lbl13.Tag = "13"
		Me.lbl13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl13.BackColor = System.Drawing.SystemColors.Control
		Me.lbl13.Enabled = True
		Me.lbl13.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl13.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl13.UseMnemonic = True
		Me.lbl13.Visible = True
		Me.lbl13.AutoSize = False
		Me.lbl13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl13.Name = "lbl13"
		Me.lbl14.Text = "Import banc"
		Me.lbl14.Size = New System.Drawing.Size(111, 15)
		Me.lbl14.Location = New System.Drawing.Point(404, 280)
		Me.lbl14.TabIndex = 30
		Me.lbl14.Tag = "14"
		Me.lbl14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl14.BackColor = System.Drawing.SystemColors.Control
		Me.lbl14.Enabled = True
		Me.lbl14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl14.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl14.UseMnemonic = True
		Me.lbl14.Visible = True
		Me.lbl14.AutoSize = False
		Me.lbl14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl14.Name = "lbl14"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 321)
		Me.lblLock.TabIndex = 34
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 315
		Me.Line1.Y2 = 315
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 316
		Me.Line2.Y2 = 316
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdImprimir, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataPagat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataEntrega, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbEstat, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(Text2)
		Me.Controls.Add(txtServei)
		Me.Controls.Add(txtCodi)
		Me.Controls.Add(txtAny)
		Me.Controls.Add(txtEntitat)
		Me.Controls.Add(Text3)
		Me.Controls.Add(cmbEstat)
		Me.Controls.Add(txtCentre)
		Me.Controls.Add(Text5)
		Me.Controls.Add(txtQuantitat)
		Me.Controls.Add(txtPreu)
		Me.Controls.Add(txtImport)
		Me.Controls.Add(txtNumeroButlletes)
		Me.Controls.Add(txtObservacions)
		Me.Controls.Add(txtDataEntrega)
		Me.Controls.Add(txtDataPagat)
		Me.Controls.Add(txtImportEfectiu)
		Me.Controls.Add(txtImportBanc)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(cmdImprimir)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(lbl10)
		Me.Controls.Add(lbl11)
		Me.Controls.Add(lbl12)
		Me.Controls.Add(lbl13)
		Me.Controls.Add(lbl14)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class