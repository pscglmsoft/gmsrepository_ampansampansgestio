Option Strict Off
Option Explicit On
Friend Class frmConsultaObjectius
	Inherits FormParent
	''OKPublic Appl As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	Public Entitat As Integer
	Public Sucursal As Short
	
Overrides 	Sub Inici()
		ResetForm(Me)
		CarregaObjectius()
	End Sub
	
	
	'UPGRADE_WARNING: Form evento frmConsultaObjectius.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmConsultaObjectius_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaObjectius()
	End Sub
	
	Private Sub frmConsultaObjectius_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmConsultaObjectius_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub frmConsultaObjectius_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub CarregaObjectius()
		CarregaFGrid(GrdObjAccions, "CGOBJECTIUS^CRM", Entitat & S & Sucursal)
		
	End Sub
	
	Private Sub GrdObjAccions_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdObjAccions.DoubleClick
		If GrdObjAccions.ActiveCell.Row = 0 Then Exit Sub
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		frmAccions.txtObjetivo.Text = Piece(GrdObjAccions.Cell(GrdObjAccions.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		frmAccions.txtNumero.Text = Piece(GrdObjAccions.Cell(GrdObjAccions.ActiveCell.Row, 1).Text, "|", 2)
		Me.Unload()
		DisplayDescripcio(frmAccions, (frmAccions.txtObjetivo))
		frmAccions.txtAccion.Focus()
	End Sub
End Class
