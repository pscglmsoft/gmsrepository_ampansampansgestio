Option Strict Off
Option Explicit On
Friend Class FrmSeguretatServeiIn
	Inherits FormParent
	''OKPublic Appl As String
	''OKPublic FlagConsulta As Boolean
	
	Private Sub cmdCancelar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancelar.ClickEvent
		Inici()
	End Sub
	
	Private Sub grdServeis_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdServeis.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("grdServeis", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Marca tots",  ,  , XPIcon("Plus"),  ,  ,  ,  , "M")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Desmarca tots",  ,  , XPIcon("Minus"),  ,  ,  ,  , "D")
			End With
		End With
		XpExecutaMenu(Me, "grdServeis", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub grdServeis_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdServeis.Click
		'    MarcaRowFG grdServeis
		If grdServeis.MouseRow < 1 Then Exit Sub
		If grdServeis.ActiveCell.Row < 1 Then Exit Sub
		If grdServeis.Cell(grdServeis.ActiveCell.Row, 4).Text = "1" Then
			grdServeis.Cell(grdServeis.ActiveCell.Row, 4).Text = "0"
		Else
			grdServeis.Cell(grdServeis.ActiveCell.Row, 4).Text = "1"
		End If
	End Sub
	
	Private Sub grdServeis_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdServeis.DoubleClick
		'    If grdServeis.MouseRow < 1 Then Exit Sub
		'    If grdServeis.ActiveCell.Row < 1 Then Exit Sub
		'    If grdServeis.Cell(grdServeis.ActiveCell.Row, 4).Text = "1" Then
		'        grdServeis.Cell(grdServeis.ActiveCell.Row, 4).Text = "0"
		'    Else
		'        grdServeis.Cell(grdServeis.ActiveCell.Row, 4).Text = "1"
		'    End If
	End Sub
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim A As String
		Dim I As Short
		
		Select Case NomMenu
			Case "grdServeis"
				Select Case KeyMenu
					Case "M"
						For I = 1 To grdServeis.Rows - 1
							grdServeis.Cell(I, 4).Text = CStr(1)
						Next I
					Case "D"
						For I = 1 To grdServeis.Rows - 1
							grdServeis.Cell(I, 4).Text = CStr(0)
						Next I
				End Select
		End Select
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		Dim I As Short
		Dim Nod As String
		If txtUsuari.Text <> "" Then
			CacheNetejaParametres()
			MCache.P1 = txtUsuari.Text
			MCache.P2 = txtEmpresa.Text
			MCache.P3 = FGMontaKeysSeleccio(grdServeis, 4)
			CacheXecute("D GPermis^INSERCIO")
		End If
		Inici()
	End Sub
	
	Private Sub FrmSeguretatServeiIn_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub FrmSeguretatServeiIn_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	Private Sub FrmSeguretatServeiIn_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		Text2.BackColor = System.Drawing.ColorTranslator.FromOle(Estil.ColorDisabled)
	End Sub
	
	Public Overrides Sub Inici()
		txtUsuari.Enabled = True
		ResetForm(Me)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		DisplayDescripcio(Me, txtEmpresa)
		If txtEmpresa.Text <> "" And txtUsuari.Text <> "" Then
			txtEmpresa.Enabled = False
			txtUsuari.Enabled = False
			CarregaServeis()
		End If
	End Sub
	Private Sub txtUsuari_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.DoubleClick
		ConsultaTaula(Me, txtUsuari)
	End Sub
	
	Private Sub txtUsuari_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtUsuari_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuari.LostFocus
		DisplayDescripcio(Me, txtUsuari)
		If txtUsuari.Text <> "" And txtEmpresa.Text <> "" Then
			txtUsuari.Enabled = False
			txtEmpresa.Enabled = False
			CarregaServeis()
		End If
	End Sub
	
	Private Sub CarregaServeis()
		'UPGRADE_NOTE: Registre se actualiz� a Registre_Renamed. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim Registre_Renamed As String
		CacheNetejaParametres()
		MCache.P1 = txtEmpresa.Text
		MCache.P2 = txtUsuari.Text
		Registre_Renamed = CacheXecute("S VALUE=$G(^ENT.SEGCD(P1,P2))")
		CarregaFGrid(grdServeis, "GServeis^INSERCIO", txtUsuari.Text & S & txtEmpresa.Text, CStr(2))
	End Sub
End Class
