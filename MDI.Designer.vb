<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class MDI
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents TimerFireWall As System.Windows.Forms.Timer
	Public WithEvents TimerDesconexio As System.Windows.Forms.Timer
	Public WithEvents CacheCub As AxVISMLib.AxVisM
	Public CommonDialog1Open As System.Windows.Forms.OpenFileDialog
	Public CommonDialog1Save As System.Windows.Forms.SaveFileDialog
	Public CommonDialog1Font As System.Windows.Forms.FontDialog
	Public CommonDialog1Color As System.Windows.Forms.ColorDialog
	Public CommonDialog1Print As System.Windows.Forms.PrintDialog
	Public WithEvents Winsock1 As AxMSWinsockLib.AxWinsock
	Public WithEvents CommonDialogFolders As AxXtremeSuiteControls.AxCommonDialog
	Public WithEvents SkF As AxXtremeSkinFramework.AxSkinFramework
	Public WithEvents CommandBars As AxXtremeCommandBars.AxCommandBars
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MDI))
		Me.IsMDIContainer = True
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.TimerFireWall = New System.Windows.Forms.Timer(components)
		Me.TimerDesconexio = New System.Windows.Forms.Timer(components)
		Me.CacheCub = New AxVISMLib.AxVisM
		Me.CommonDialog1Open = New System.Windows.Forms.OpenFileDialog
		Me.CommonDialog1Save = New System.Windows.Forms.SaveFileDialog
		Me.CommonDialog1Font = New System.Windows.Forms.FontDialog
		Me.CommonDialog1Color = New System.Windows.Forms.ColorDialog
		Me.CommonDialog1Print = New System.Windows.Forms.PrintDialog
		Me.Winsock1 = New AxMSWinsockLib.AxWinsock
		Me.CommonDialogFolders = New AxXtremeSuiteControls.AxCommonDialog
		Me.SkF = New AxXtremeSkinFramework.AxSkinFramework
		Me.CommandBars = New AxXtremeCommandBars.AxCommandBars
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.CacheCub, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Winsock1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.CommonDialogFolders, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.SkF, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.CommandBars, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
		Me.BackColor = System.Drawing.SystemColors.AppWorkspace
		Me.Text = "Ampans Gesti�"
		Me.ClientSize = New System.Drawing.Size(428, 212)
		Me.Location = New System.Drawing.Point(360, 255)
		Me.Icon = CType(resources.GetObject("MDI.Icon"), System.Drawing.Icon)
		Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Name = "MDI"
		Me.TimerFireWall.Enabled = False
		Me.TimerFireWall.Interval = 1
		Me.TimerDesconexio.Enabled = False
		Me.TimerDesconexio.Interval = 1
		CacheCub.OcxState = CType(resources.GetObject("CacheCub.OcxState"), System.Windows.Forms.AxHost.State)
		Me.CacheCub.Location = New System.Drawing.Point(106, 44)
		Me.CacheCub.Name = "CacheCub"
		Winsock1.OcxState = CType(resources.GetObject("Winsock1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.Winsock1.Location = New System.Drawing.Point(18, 38)
		Me.Winsock1.Name = "Winsock1"
		CommonDialogFolders.OcxState = CType(resources.GetObject("CommonDialogFolders.OcxState"), System.Windows.Forms.AxHost.State)
		Me.CommonDialogFolders.Location = New System.Drawing.Point(158, 114)
		Me.CommonDialogFolders.Name = "CommonDialogFolders"
		SkF.OcxState = CType(resources.GetObject("SkF.OcxState"), System.Windows.Forms.AxHost.State)
		Me.SkF.Location = New System.Drawing.Point(108, 108)
		Me.SkF.Name = "SkF"
		CommandBars.OcxState = CType(resources.GetObject("CommandBars.OcxState"), System.Windows.Forms.AxHost.State)
		Me.CommandBars.Location = New System.Drawing.Point(64, 108)
		Me.CommandBars.Name = "CommandBars"
		Me.Controls.Add(CacheCub)
		Me.Controls.Add(Winsock1)
		Me.Controls.Add(CommonDialogFolders)
		Me.Controls.Add(SkF)
		Me.Controls.Add(CommandBars)
		CType(Me.CommandBars, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.SkF, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.CommonDialogFolders, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Winsock1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.CacheCub, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class