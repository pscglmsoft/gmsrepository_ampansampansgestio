<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmGDDocumentosPersona
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents grdDocuments As AxFlexCell.AxGrid
	Public WithEvents tvClasificacions As System.Windows.Forms.TreeView
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmGDDocumentosPersona))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.grdDocuments = New AxFlexCell.AxGrid
		Me.tvClasificacions = New System.Windows.Forms.TreeView
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.grdDocuments, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Documentos"
		Me.ClientSize = New System.Drawing.Size(1010, 592)
		Me.Location = New System.Drawing.Point(418, 208)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmGDDocumentosPersona"
		grdDocuments.OcxState = CType(resources.GetObject("grdDocuments.OcxState"), System.Windows.Forms.AxHost.State)
		Me.grdDocuments.Size = New System.Drawing.Size(711, 578)
		Me.grdDocuments.Location = New System.Drawing.Point(294, 8)
		Me.grdDocuments.TabIndex = 0
		Me.grdDocuments.Name = "grdDocuments"
		Me.tvClasificacions.CausesValidation = True
		Me.tvClasificacions.Size = New System.Drawing.Size(284, 578)
		Me.tvClasificacions.Location = New System.Drawing.Point(6, 8)
		Me.tvClasificacions.TabIndex = 1
		Me.tvClasificacions.Tag = "G-CLASES DOCUMENTOS GD#3#4"
		Me.tvClasificacions.HideSelection = False
		Me.tvClasificacions.Indent = 36
		Me.tvClasificacions.LabelEdit = False
		Me.tvClasificacions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.tvClasificacions.Name = "tvClasificacions"
		CType(Me.grdDocuments, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(grdDocuments)
		Me.Controls.Add(tvClasificacions)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class