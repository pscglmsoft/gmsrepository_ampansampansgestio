Option Strict Off
Option Explicit On
Friend Class frmSolicitudTransport
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		CarregaHistoric()
	End Sub
	
	Public Overrides Sub FGetReg()
		Dim Valor As String
		Dim Permis As String
		Dim X As Short
		
		
		cmdModifPreu.Visible = False
		
		If CacheXecute("S VALUE=$$MPREU^TRANSPORT()") = "1" Then
			cmdModifPreu.Visible = True
			X = 1
		End If
		
		
		Valor = CacheXecute("S VALUE=$P(^[""ENTI""]QUSCO(EMP,USU),S,1)")
		If Valor <> "" Then
			
			CacheNetejaParametres()
			
			MCache.P1 = txtTipo.Text
			MCache.P2 = Valor
			
			Permis = CacheXecute("S VALUE=$P($G(^GESTORS(EMP,P1,P2)),S,3)")
			
			If Permis = "1" Then
				cmdModifPreu.Visible = True
			ElseIf X <> 1 Then 
				cmdModifPreu.Visible = False
			End If
		End If
		
		
		
		If cmbEstat.Columns(1).Value = 4 Or cmbEstat.Columns(1).Value = 2 Then
			cmdReobrir.Visible = False
			DisableForm(Me)
			Exit Sub
		End If
		cmdReobrir.Visible = False
		If cmbEstat.Columns(1).Value = 3 Then
			DisableForm(Me)
			cmdReobrir.Visible = True
			cmdReobrir.Text = "Reobrir sol�licitud"
			cmdReobrir.Picture = SetIcon("Desfer", 16)
		End If
		DesbloquejaOption()
		
	End Sub
	
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	
	
	
	
	Private Sub chkCopiaRegistrada_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkCopiaRegistrada.GotFocus
		XGotFocus(Me, chkCopiaRegistrada)
	End Sub
	
	Private Sub chkCopiaRegistrada_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkCopiaRegistrada.LostFocus
		XLostFocus(Me, chkCopiaRegistrada)
	End Sub
	
	Private Sub cmdImprimir_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdImprimir.ClickEvent
		'  If txtSolicitud = "" Then Exit Sub
		If verificaDades = False Then Exit Sub
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		
		ImpresioPDF("ALBARA_TRANSPORT_NOU", txtSolicitud.Text)
		Me.Unload()
		
	End Sub
	
	Private Sub cmdModifPreu_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdModifPreu.ClickEvent
		'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		txtPrecio.Enabled = True
		txtPrecio.Focus()
	End Sub
	
	Private Sub cmdReobrir_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdReobrir.ClickEvent
		Dim strCodiSol As String
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		If xMsgBox("Quiere reabrir la solicitud desestimada{?}", MsgBoxStyle.YesNo + MsgBoxStyle.Question, Me.Text) = MsgBoxResult.No Then Exit Sub
		
		strCodiSol = txtSolicitud.Text
		
		Me.Unload()
		
		With frmExecutaProces
			.P1 = strCodiSol
			.Rutina = "ATANCS^TRANSPORT"
			.Show(CStr(1))
		End With
		
		txtSolicitud.Text = strCodiSol
		ABM = GetReg(Me)
	End Sub
	
	'UPGRADE_WARNING: Form evento frmSolicitudTransport.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmSolicitudTransport_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaHistoric()
	End Sub
	
	Private Sub frmSolicitudTransport_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		'If txtOrigen = "" Then txtOrigen = "1"
		'If txtDesti = "" Then txtDesti = "1"
		If verificaDades = False Then Exit Sub
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		'If txtOrigen = "" Then txtOrigen = "1"
		'If txtDesti = "" Then txtDesti = "1"
		If verificaDades = False Then Exit Sub
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmSolicitudTransport_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim Valor As String
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		MostraCampsO()
		MostraCampsD()
		
		cmdModifPreu.Picture = SetIcon("Refresh", 16)
		
	End Sub
	
	Private Sub frmSolicitudTransport_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmSolicitudTransport_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	
	Private Sub grdHSol_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles grdHSol.DoubleClick
		ObreSeguiment()
	End Sub
	
	Private Sub grdHSol_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles grdHSol.MouseUp
		Dim strLin As String
		Dim strTipPer As String
		
		If grdHSol.MouseRow < 1 Then Exit Sub
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		If grdHSol.Rows < 2 Then Exit Sub
		strLin = FGGetRegRow(grdHSol)
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			
			'UPGRADE_ISSUE: Control Name no se pudo resolver porque est� dentro del espacio de nombres gen�rico ActiveControl. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="084D22AD-ECB1-400F-B4C7-418ECEC5E36E"'
			With .Menus.Add(ActiveControl.Name, PopMenuStyle.tsSecondaryMenu, True)
				If grdHSol.Rows > 1 And grdHSol.Selection.FirstRow <> 0 Then
					'UPGRADE_ISSUE: Control Name no se pudo resolver porque est� dentro del espacio de nombres gen�rico ActiveControl. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="084D22AD-ECB1-400F-B4C7-418ECEC5E36E"'
					.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Ir al seguimiento de la solicitud",  , True, XPIcon("WindowsSendTo"),  ,  ,  ,  , "ASOL")
					'.MenuItems.Add tsMenuCaption, "Planificar", , True, XPIcon("Verify"), , , , , "PLAN"
					'.MenuItems.Add tsMenuCaption, "Desestimar", , True, XPIcon("Delete"), , , , , "DES"
				End If
			End With
		End With
		XpExecutaMenu(Me, "grdHSol", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub optDesti_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optDesti.ClickEvent
		Dim Index As Short = optDesti.GetIndex(eventSender)
		Select Case Index
			Case 1
				txtDesti.Text = CStr(1) 'intern
			Case 2
				txtDesti.Text = CStr(2) 'extern
		End Select
	End Sub
	
	Private Sub optOrigen_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optOrigen.ClickEvent
		Dim Index As Short = optOrigen.GetIndex(eventSender)
		Select Case Index
			Case 1
				txtOrigen.Text = CStr(1) 'intern
			Case 2
				txtOrigen.Text = CStr(2) 'extern
		End Select
	End Sub
	
	Private Sub optTipo_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optTipo.ClickEvent
		Dim Index As Short = optTipo.GetIndex(eventSender)
		
		Select Case Index
			Case 1
				txtTipo.Text = CStr(1) 'mercancia
			Case 2
				txtTipo.Text = CStr(2) 'mensajeria
		End Select
		
	End Sub
	
	Private Sub TabControl1_Selected(sender As Object, ByVal eventArgs As TabControlEventArgs) Handles TabControl1.Selected
		CarregaGrids((eventArgs.TabPageIndex))
	End Sub
	
	'UPGRADE_WARNING: El evento txtDesti.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtDesti_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDesti.TextChanged
		If txtDesti.Text = "" Then Exit Sub
		optDesti(txtDesti).Value = True
		MostraCampsD()
	End Sub
	
	
	
	
	
	Private Sub txtHorasTransporte_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHorasTransporte.GotFocus
		XGotFocus(Me, txtHorasTransporte)
	End Sub
	
	Private Sub txtHorasTransporte_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHorasTransporte.LostFocus
		XLostFocus(Me, txtHorasTransporte)
	End Sub
	
	
	
	Private Sub txtImporteVtaInterna_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImporteVtaInterna.GotFocus
		XGotFocus(Me, txtImporteVtaInterna)
	End Sub
	
	
	Private Sub txtImporteVtaInterna_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtImporteVtaInterna.LostFocus
		XLostFocus(Me, txtImporteVtaInterna)
	End Sub
	
	Private Sub txtKilometros_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtKilometros.GotFocus
		XGotFocus(Me, txtKilometros)
	End Sub
	
	Private Sub txtKilometros_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtKilometros.LostFocus
		XLostFocus(Me, txtKilometros)
	End Sub
	
	Private Sub txtObservacionesDestino_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesDestino.GotFocus
		XGotFocus(Me, txtObservacionesDestino)
	End Sub
	
	Private Sub txtObservacionesDestino_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesDestino.LostFocus
		XLostFocus(Me, txtObservacionesDestino)
	End Sub
	
	Private Sub txtObservacionesorigen_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesorigen.GotFocus
		XGotFocus(Me, txtObservacionesorigen)
	End Sub
	
	Private Sub txtObservacionesorigen_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacionesorigen.LostFocus
		XLostFocus(Me, txtObservacionesorigen)
	End Sub
	
	'UPGRADE_WARNING: El evento txtOrigen.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtOrigen_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOrigen.TextChanged
		If txtOrigen.Text = "" Then Exit Sub
		optOrigen(txtOrigen).Value = True
		MostraCampsO()
	End Sub
	
	
	
	Private Sub txtPes_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPes.GotFocus
		XGotFocus(Me, txtPes)
	End Sub
	
	Private Sub txtPes_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPes.LostFocus
		XLostFocus(Me, txtPes)
	End Sub
	
	Private Sub txtSolicitud_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSolicitud.GotFocus
		XGotFocus(Me, txtSolicitud)
	End Sub
	
	Private Sub txtSolicitud_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSolicitud.DoubleClick
		ConsultaTaula(Me, txtSolicitud)
	End Sub
	
	Private Sub txtSolicitud_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSolicitud.LostFocus
		
		If txtTipo.Text = "" Then
			'    optTipo_Click (1)
		End If
		If txtOrigen.Text = "" Then
			optOrigen_ClickEvent(optOrigen.Item(1), New System.EventArgs())
		End If
		If txtDesti.Text = "" Then
			optDesti_ClickEvent(optDesti.Item(1), New System.EventArgs())
		End If
		
		If txtSolicitud.Text <> "" Then ABM = GetReg(Me)
		
		XLostFocus(Me, txtSolicitud)
	End Sub
	
	Private Sub txtSolicitud_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSolicitud.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbEstat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstat.GotFocus
		XGotFocus(Me, cmbEstat)
	End Sub
	
	Private Sub cmbEstat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstat.LostFocus
		XLostFocus(Me, cmbEstat)
	End Sub
	
	Private Sub txtDataSolicitud_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataSolicitud.GotFocus
		XGotFocus(Me, txtDataSolicitud)
	End Sub
	
	Private Sub txtDataSolicitud_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataSolicitud.LostFocus
		XLostFocus(Me, txtDataSolicitud)
	End Sub
	
	Private Sub txtDataTransport_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataTransport.GotFocus
		XGotFocus(Me, txtDataTransport)
	End Sub
	
	Private Sub txtDataTransport_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataTransport.LostFocus
		XLostFocus(Me, txtDataTransport)
	End Sub
	
	Private Sub cmbPrioritat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPrioritat.GotFocus
		XGotFocus(Me, cmbPrioritat)
	End Sub
	
	Private Sub cmbPrioritat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbPrioritat.LostFocus
		XLostFocus(Me, cmbPrioritat)
	End Sub
	
	Private Sub txtSolicitant_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSolicitant.GotFocus
		XGotFocus(Me, txtSolicitant)
	End Sub
	
	Private Sub txtSolicitant_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSolicitant.DoubleClick
		ConsultaTaula(Me, txtSolicitant)
	End Sub
	
	Private Sub txtSolicitant_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSolicitant.LostFocus
		XLostFocus(Me, txtSolicitant)
	End Sub
	
	Private Sub txtSolicitant_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSolicitant.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEmpresa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.GotFocus
		XGotFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.DoubleClick
		ConsultaTaula(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEmpresa.LostFocus
		XLostFocus(Me, txtEmpresa)
	End Sub
	
	Private Sub txtEmpresa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEmpresa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCentreCost_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreCost.GotFocus
		XGotFocus(Me, txtCentreCost)
	End Sub
	
	Private Sub txtCentreCost_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreCost.DoubleClick
		ConsultaTaula(Me, txtCentreCost)
	End Sub
	
	Private Sub txtCentreCost_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCentreCost.LostFocus
		XLostFocus(Me, txtCentreCost)
	End Sub
	
	Private Sub txtCentreCost_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCentreCost.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFeina_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFeina.GotFocus
		XGotFocus(Me, txtFeina)
	End Sub
	
	Private Sub txtFeina_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFeina.DoubleClick
		ConsultaTaula(Me, txtFeina)
	End Sub
	
	Private Sub txtFeina_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFeina.LostFocus
		XLostFocus(Me, txtFeina)
	End Sub
	
	Private Sub txtFeina_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtFeina.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	Private Sub txtSubdireccionOrigen_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubdireccionOrigen.GotFocus
		XGotFocus(Me, txtSubdireccionOrigen)
	End Sub
	
	Private Sub txtSubdireccionOrigen_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSubdireccionOrigen.LostFocus
		XLostFocus(Me, txtSubdireccionOrigen)
	End Sub
	
	Private Sub txtTarea_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTarea.GotFocus
		XGotFocus(Me, txtTarea)
	End Sub
	
	Private Sub txtTarea_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTarea.DoubleClick
		ConsultaTaula(Me, txtTarea,  , txtFeina.Text & S & "O")
	End Sub
	
	Private Sub txtTarea_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTarea.LostFocus
		XLostFocus(Me, txtTarea)
	End Sub
	
	Private Sub txtTarea_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTarea.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtFeina.Text & S & "O")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCantidad_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCantidad.GotFocus
		XGotFocus(Me, txtCantidad)
	End Sub
	
	Private Sub txtCantidad_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCantidad.LostFocus
		XLostFocus(Me, txtCantidad)
	End Sub
	
	Private Sub txtCantidad_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCantidad.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	
	
	'UPGRADE_WARNING: El evento txtTipo.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtTipo_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipo.TextChanged
		If txtTipo.Text = "" Then Exit Sub
		optTipo(txtTipo).Value = True
	End Sub
	
	Private Sub txtTipo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipo.GotFocus
		XGotFocus(Me, txtTipo)
	End Sub
	
	Private Sub txtTipo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipo.LostFocus
		XLostFocus(Me, txtTipo)
	End Sub
	
	Private Sub txtTipoElemento_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoElemento.GotFocus
		XGotFocus(Me, txtTipoElemento)
	End Sub
	
	Private Sub txtTipoElemento_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoElemento.DoubleClick
		If txtTipo.Text = "" Then
			xMsgBox("Falta informar si es del tipo mercancia o mensajer�a", MsgBoxStyle.Information, Me.Text)
			Exit Sub
		End If
		ConsultaTaula(Me, txtTipoElemento)
	End Sub
	
	Private Sub txtTipoElemento_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoElemento.LostFocus
		XLostFocus(Me, txtTipoElemento)
	End Sub
	
	Private Sub txtTipoElemento_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipoElemento.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcionTransporte_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcionTransporte.GotFocus
		XGotFocus(Me, txtDescripcionTransporte)
	End Sub
	
	Private Sub txtDescripcionTransporte_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcionTransporte.LostFocus
		XLostFocus(Me, txtDescripcionTransporte)
	End Sub
	
	Private Sub txtDescripcionTransporte_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcionTransporte.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPuntoRecepcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntoRecepcion.GotFocus
		XGotFocus(Me, txtPuntoRecepcion)
	End Sub
	
	Private Sub txtPuntoRecepcion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntoRecepcion.DoubleClick
		ConsultaTaula(Me, txtPuntoRecepcion)
	End Sub
	
	Private Sub txtPuntoRecepcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntoRecepcion.LostFocus
		XLostFocus(Me, txtPuntoRecepcion)
	End Sub
	
	Private Sub txtPuntoRecepcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPuntoRecepcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDireccionRecepcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDireccionRecepcion.GotFocus
		XGotFocus(Me, txtDireccionRecepcion)
	End Sub
	
	Private Sub txtDireccionRecepcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDireccionRecepcion.LostFocus
		XLostFocus(Me, txtDireccionRecepcion)
	End Sub
	
	Private Sub txtDireccionRecepcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDireccionRecepcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPoblacionRecepcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacionRecepcion.GotFocus
		XGotFocus(Me, txtPoblacionRecepcion)
	End Sub
	
	Private Sub txtPoblacionRecepcion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacionRecepcion.DoubleClick
		ConsultaTaula(Me, txtPoblacionRecepcion)
	End Sub
	
	Private Sub txtPoblacionRecepcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacionRecepcion.LostFocus
		XLostFocus(Me, txtPoblacionRecepcion)
	End Sub
	
	Private Sub txtPoblacionRecepcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPoblacionRecepcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCpRecepcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCpRecepcion.GotFocus
		XGotFocus(Me, txtCpRecepcion)
	End Sub
	
	Private Sub txtCpRecepcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCpRecepcion.LostFocus
		XLostFocus(Me, txtCpRecepcion)
	End Sub
	
	Private Sub txtCpRecepcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCpRecepcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTelefonoRecepcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefonoRecepcion.GotFocus
		XGotFocus(Me, txtTelefonoRecepcion)
	End Sub
	
	Private Sub txtTelefonoRecepcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefonoRecepcion.LostFocus
		XLostFocus(Me, txtTelefonoRecepcion)
	End Sub
	
	Private Sub txtTelefonoRecepcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTelefonoRecepcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtContactoRecepcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtContactoRecepcion.GotFocus
		XGotFocus(Me, txtContactoRecepcion)
	End Sub
	
	Private Sub txtContactoRecepcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtContactoRecepcion.LostFocus
		XLostFocus(Me, txtContactoRecepcion)
	End Sub
	
	Private Sub txtContactoRecepcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtContactoRecepcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtHorarioRecepcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHorarioRecepcion.GotFocus
		XGotFocus(Me, txtHorarioRecepcion)
	End Sub
	
	Private Sub txtHorarioRecepcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHorarioRecepcion.LostFocus
		XLostFocus(Me, txtHorarioRecepcion)
	End Sub
	
	Private Sub txtHorarioRecepcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtHorarioRecepcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEntidadRecepcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidadRecepcion.GotFocus
		XGotFocus(Me, txtEntidadRecepcion)
	End Sub
	
	Private Sub txtEntidadRecepcion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidadRecepcion.DoubleClick
		ConsultaTaula(Me, txtEntidadRecepcion)
	End Sub
	
	Private Sub txtEntidadRecepcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidadRecepcion.LostFocus
		XLostFocus(Me, txtEntidadRecepcion)
		CacheNetejaParametres()
		MCache.P1 = txtEntidadRecepcion.Text
		txtSucursalRecepcion.Text = CacheXecute("S VALUE=$$SUCR^TRANSPORT(P1)")
	End Sub
	
	Private Sub txtEntidadRecepcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEntidadRecepcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSucursalRecepcion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursalRecepcion.GotFocus
		XGotFocus(Me, txtSucursalRecepcion)
	End Sub
	
	Private Sub txtSucursalRecepcion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursalRecepcion.DoubleClick
		ConsultaTaula(Me, txtSucursalRecepcion,  , txtEntidadRecepcion.Text & S & "S")
	End Sub
	
	Private Sub txtSucursalRecepcion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursalRecepcion.LostFocus
		XLostFocus(Me, txtSucursalRecepcion)
	End Sub
	
	Private Sub txtSucursalRecepcion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSucursalRecepcion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtEntidadRecepcion.Text & S & "S")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPuntoEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntoEntrega.GotFocus
		XGotFocus(Me, txtPuntoEntrega)
	End Sub
	
	Private Sub txtPuntoEntrega_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntoEntrega.DoubleClick
		ConsultaTaula(Me, txtPuntoEntrega)
	End Sub
	
	Private Sub txtPuntoEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPuntoEntrega.LostFocus
		XLostFocus(Me, txtPuntoEntrega)
	End Sub
	
	Private Sub txtPuntoEntrega_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPuntoEntrega.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDireccionEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDireccionEntrega.GotFocus
		XGotFocus(Me, txtDireccionEntrega)
	End Sub
	
	Private Sub txtDireccionEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDireccionEntrega.LostFocus
		XLostFocus(Me, txtDireccionEntrega)
	End Sub
	
	Private Sub txtDireccionEntrega_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDireccionEntrega.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPoblacionEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacionEntrega.GotFocus
		XGotFocus(Me, txtPoblacionEntrega)
	End Sub
	
	Private Sub txtPoblacionEntrega_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacionEntrega.DoubleClick
		ConsultaTaula(Me, txtPoblacionEntrega)
	End Sub
	
	Private Sub txtPoblacionEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPoblacionEntrega.LostFocus
		XLostFocus(Me, txtPoblacionEntrega)
	End Sub
	
	Private Sub txtPoblacionEntrega_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPoblacionEntrega.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtCpEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCpEntrega.GotFocus
		XGotFocus(Me, txtCpEntrega)
	End Sub
	
	Private Sub txtCpEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCpEntrega.LostFocus
		XLostFocus(Me, txtCpEntrega)
	End Sub
	
	Private Sub txtCpEntrega_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCpEntrega.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTelefonoEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefonoEntrega.GotFocus
		XGotFocus(Me, txtTelefonoEntrega)
	End Sub
	
	Private Sub txtTelefonoEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTelefonoEntrega.LostFocus
		XLostFocus(Me, txtTelefonoEntrega)
	End Sub
	
	Private Sub txtTelefonoEntrega_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTelefonoEntrega.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtContactoEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtContactoEntrega.GotFocus
		XGotFocus(Me, txtContactoEntrega)
	End Sub
	
	Private Sub txtContactoEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtContactoEntrega.LostFocus
		XLostFocus(Me, txtContactoEntrega)
	End Sub
	
	Private Sub txtContactoEntrega_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtContactoEntrega.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtHorarioEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHorarioEntrega.GotFocus
		XGotFocus(Me, txtHorarioEntrega)
	End Sub
	
	Private Sub txtHorarioEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHorarioEntrega.LostFocus
		XLostFocus(Me, txtHorarioEntrega)
	End Sub
	
	Private Sub txtHorarioEntrega_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtHorarioEntrega.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEntidadEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidadEntrega.GotFocus
		XGotFocus(Me, txtEntidadEntrega)
	End Sub
	
	Private Sub txtEntidadEntrega_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidadEntrega.DoubleClick
		ConsultaTaula(Me, txtEntidadEntrega)
	End Sub
	
	Private Sub txtEntidadEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidadEntrega.LostFocus
		XLostFocus(Me, txtEntidadEntrega)
		
		CacheNetejaParametres()
		MCache.P1 = txtEntidadEntrega.Text
		txtSucursalEntrega.Text = CacheXecute("S VALUE=$$SUCR^TRANSPORT(P1)")
	End Sub
	
	Private Sub txtEntidadEntrega_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEntidadEntrega.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSucursalEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursalEntrega.GotFocus
		XGotFocus(Me, txtSucursalEntrega)
	End Sub
	
	Private Sub txtSucursalEntrega_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursalEntrega.DoubleClick
		ConsultaTaula(Me, txtSucursalEntrega,  , txtEntidadEntrega.Text & S & "S")
	End Sub
	
	Private Sub txtSucursalEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursalEntrega.LostFocus
		XLostFocus(Me, txtSucursalEntrega)
	End Sub
	
	Private Sub txtSucursalEntrega_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSucursalEntrega.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtEntidadEntrega.Text & S & "S")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipoTarifa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoTarifa.GotFocus
		XGotFocus(Me, txtTipoTarifa)
	End Sub
	
	Private Sub txtTipoTarifa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoTarifa.DoubleClick
		ConsultaTaula(Me, txtTipoTarifa)
	End Sub
	
	Private Sub txtTipoTarifa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoTarifa.LostFocus
		XLostFocus(Me, txtTipoTarifa)
	End Sub
	
	Private Sub txtTipoTarifa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipoTarifa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtPrecio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrecio.GotFocus
		XGotFocus(Me, txtPrecio)
	End Sub
	
	Private Sub txtPrecio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPrecio.LostFocus
		XLostFocus(Me, txtPrecio)
	End Sub
	
	Private Sub txtPrecio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtPrecio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaPrevista_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaPrevista.GotFocus
		XGotFocus(Me, txtFechaPrevista)
	End Sub
	
	Private Sub txtFechaPrevista_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaPrevista.LostFocus
		XLostFocus(Me, txtFechaPrevista)
	End Sub
	
	Private Sub txtFechaRealizacion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaRealizacion.GotFocus
		XGotFocus(Me, txtFechaRealizacion)
	End Sub
	
	Private Sub txtFechaRealizacion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaRealizacion.LostFocus
		XLostFocus(Me, txtFechaRealizacion)
	End Sub
	
	Private Sub txtTransportista_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTransportista.GotFocus
		XGotFocus(Me, txtTransportista)
	End Sub
	
	Private Sub txtTransportista_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTransportista.DoubleClick
		ConsultaTaula(Me, txtTransportista)
	End Sub
	
	Private Sub txtTransportista_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTransportista.LostFocus
		XLostFocus(Me, txtTransportista)
	End Sub
	
	Private Sub txtTransportista_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTransportista.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtMotivoNoRealizado_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoNoRealizado.GotFocus
		XGotFocus(Me, txtMotivoNoRealizado)
	End Sub
	
	Private Sub txtMotivoNoRealizado_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoNoRealizado.DoubleClick
		ConsultaTaula(Me, txtMotivoNoRealizado)
	End Sub
	
	Private Sub txtMotivoNoRealizado_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotivoNoRealizado.LostFocus
		XLostFocus(Me, txtMotivoNoRealizado)
	End Sub
	
	Private Sub txtMotivoNoRealizado_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMotivoNoRealizado.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaEntrega.GotFocus
		XGotFocus(Me, txtFechaEntrega)
	End Sub
	
	Private Sub txtFechaEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaEntrega.LostFocus
		XLostFocus(Me, txtFechaEntrega)
	End Sub
	
	Private Sub txtHoraEntrega_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHoraEntrega.GotFocus
		XGotFocus(Me, txtHoraEntrega)
	End Sub
	
	Private Sub txtHoraEntrega_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHoraEntrega.LostFocus
		XLostFocus(Me, txtHoraEntrega)
	End Sub
	
	Private Sub txtFechaRecogida_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaRecogida.GotFocus
		XGotFocus(Me, txtFechaRecogida)
	End Sub
	
	Private Sub txtFechaRecogida_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaRecogida.LostFocus
		XLostFocus(Me, txtFechaRecogida)
	End Sub
	
	Private Sub txtHoraRecogida_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHoraRecogida.GotFocus
		XGotFocus(Me, txtHoraRecogida)
	End Sub
	
	Private Sub txtHoraRecogida_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtHoraRecogida.LostFocus
		XLostFocus(Me, txtHoraRecogida)
	End Sub
	
	Private Sub txtComentario_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtComentario.GotFocus
		XGotFocus(Me, txtComentario)
	End Sub
	
	Private Sub txtComentario_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtComentario.LostFocus
		XLostFocus(Me, txtComentario)
	End Sub
	
	Private Sub txtComentario_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtComentario.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub MostraCampsO()
		If txtOrigen.Text = "2" Then
			lbl16.Visible = False
			Text16.Text = ""
			Text16.Visible = False
			txtPuntoRecepcion.Text = ""
			txtPuntoRecepcion.Visible = False
			
			lbl23.Visible = True
			lbl24.Visible = True
			txtEntidadRecepcion.Visible = True
			txtSucursalRecepcion.Visible = True
			Text24.Visible = True
		Else
			lbl16.Visible = True
			txtPuntoRecepcion.Visible = True
			Text16.Visible = True
			
			lbl23.Visible = False
			lbl24.Visible = False
			Text24.Visible = False
			txtEntidadRecepcion.Text = ""
			txtEntidadRecepcion.Visible = False
			txtSucursalRecepcion.Text = ""
			txtSucursalRecepcion.Visible = False
			
		End If
	End Sub
	
	Private Sub MostraCampsD()
		If txtDesti.Text = "2" Then
			lbl25.Visible = False
			Text25.Text = ""
			Text25.Visible = False
			txtPuntoEntrega.Text = ""
			txtPuntoEntrega.Visible = False
			
			lbl32.Visible = True
			lbl33.Visible = True
			Text33.Visible = True
			
			txtEntidadEntrega.Visible = True
			txtSucursalEntrega.Visible = True
		Else
			lbl25.Visible = True
			txtPuntoEntrega.Visible = True
			Text25.Visible = True
			
			lbl32.Visible = False
			lbl33.Visible = False
			Text33.Visible = False
			txtEntidadEntrega.Text = ""
			txtEntidadEntrega.Visible = False
			txtSucursalEntrega.Text = ""
			txtSucursalEntrega.Visible = False
			
		End If
	End Sub
	
	Function verificaDades() As Boolean
		verificaDades = True
		If txtTipo.Text = "" Then
			xMsgBox("Falta informar si �s Mercaderia o Missatgeria ", MsgBoxStyle.Information, Me.Text)
			Exit Function
		End If
		If CDbl(txtTipo.Text) = 1 And txtPes.Text = "" Then
			xMsgBox("Falta informar el pes", MsgBoxStyle.Information, Me.Text)
			txtPes.Focus()
			verificaDades = False
			Exit Function
		End If
		If txtFeina.Text <> "" And txtTarea.Text = "" Then
			xMsgBox("Falta informar la tarea", MsgBoxStyle.Information, Me.Text)
			txtTarea.Focus()
			verificaDades = False
			Exit Function
		End If
		If txtTarea.Text <> "" And txtFeina.Text = "" Then
			xMsgBox("Falta informar la orden de trabajo", MsgBoxStyle.Information, Me.Text)
			txtFeina.Focus()
			verificaDades = False
			Exit Function
		End If
		If txtOrigen.Text = "1" Then
			If txtPuntoRecepcion.Text = "" Then
				xMsgBox("Falta informar el punto de recepci�n", MsgBoxStyle.Information, Me.Text)
				txtPuntoRecepcion.Focus()
				verificaDades = False
				Exit Function
			End If
			'ElseIf txtOrigen = "2" Then
		End If
		If txtDireccionRecepcion.Text = "" Then
			xMsgBox("Falta informar la direcci�n de recepci�n", MsgBoxStyle.Information, Me.Text)
			txtDireccionRecepcion.Focus()
			verificaDades = False
			Exit Function
		End If
		'    If txtSubdireccionOrigen = "" Then
		'        xMsgBox "Falta informar la subdirecci�n de recepci�n", vbInformation, Me.Caption
		'        txtSubdireccionOrigen.SetFocus
		'        verificaDades = False
		'        Exit Function
		'    End If
		
		If txtPoblacionRecepcion.Text = "" Then
			xMsgBox("Falta informar la poblaci�n de recepci�n", MsgBoxStyle.Information, Me.Text)
			txtPoblacionRecepcion.Focus()
			verificaDades = False
			Exit Function
		End If
		If txtTelefonoRecepcion.Text = "" Then
			xMsgBox("Falta informar el tel�fono de recepci�n", MsgBoxStyle.Information, Me.Text)
			txtTelefonoRecepcion.Focus()
			verificaDades = False
			Exit Function
		End If
		If txtContactoRecepcion.Text = "" Then
			xMsgBox("Falta informar el contacto de recepci�n", MsgBoxStyle.Information, Me.Text)
			txtContactoRecepcion.Focus()
			verificaDades = False
			Exit Function
		End If
		If txtHorarioRecepcion.Text = "" Then
			xMsgBox("Falta informar el horario de recepci�n", MsgBoxStyle.Information, Me.Text)
			txtHorarioRecepcion.Focus()
			verificaDades = False
			Exit Function
		End If
		
		If txtDesti.Text = "1" Then
			If txtPuntoEntrega.Text = "" Then
				xMsgBox("Falta informar el punto de entrega", MsgBoxStyle.Information, Me.Text)
				txtPuntoEntrega.Focus()
				verificaDades = False
				Exit Function
			End If
		End If
		If txtDireccionEntrega.Text = "" Then
			xMsgBox("Falta informar la direcci�n de entrega", MsgBoxStyle.Information, Me.Text)
			txtDireccionEntrega.Focus()
			verificaDades = False
			Exit Function
		End If
		'    If txtSubdireccionDestino = "" Then
		'        xMsgBox "Falta informar la subdirecci�n de entrega", vbInformation, Me.Caption
		'        txtSubdireccionDestino.SetFocus
		'        verificaDades = False
		'        Exit Function
		'    End If
		If txtPoblacionEntrega.Text = "" Then
			xMsgBox("Falta informar la poblaci�n de entrega", MsgBoxStyle.Information, Me.Text)
			txtPoblacionEntrega.Focus()
			verificaDades = False
			Exit Function
		End If
		If txtTelefonoEntrega.Text = "" Then
			xMsgBox("Falta informar el tel�fono de entrega", MsgBoxStyle.Information, Me.Text)
			txtTelefonoEntrega.Focus()
			verificaDades = False
			Exit Function
		End If
		If txtContactoEntrega.Text = "" Then
			xMsgBox("Falta informar el contacto de entrega", MsgBoxStyle.Information, Me.Text)
			txtContactoEntrega.Focus()
			verificaDades = False
			Exit Function
		End If
		If txtHorarioEntrega.Text = "" Then
			xMsgBox("Falta informar el horario de entrega", MsgBoxStyle.Information, Me.Text)
			txtHorarioEntrega.Focus()
			verificaDades = False
			Exit Function
		End If
		If txtEntidadEntrega.Text <> "" And txtSucursalEntrega.Text = "" Then
			xMsgBox("Falta informar la sucursal de entrega", MsgBoxStyle.Information, Me.Text)
			txtSucursalEntrega.Focus()
			verificaDades = False
			Exit Function
		End If
		
	End Function
	
	Private Sub CarregaGrids(ByRef curtab As Short)
		Select Case curtab
			Case 2
				CarregaHistoric()
		End Select
	End Sub
	
	Private Sub CarregaHistoric()
		MCache.P1 = txtSolicitud.Text
		CarregaFGrid(grdHSol, "CGHSOL^TRANSPORT", txtSolicitud.Text)
	End Sub
	
	Private Sub DesbloquejaOption()
		Dim TypeCtrl As String
		Dim I As Short
		'UPGRADE_WARNING: Controls m�todo Controls.Count tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		For I = 0 To Me.FRM.items().Count() - 1
			TypeCtrl = CStr(TipusControl(CType(Me.Controls(I), Object)))
			Select Case TypeCtrl
				Case CStr(e_TipusControl.tcOptionButton), CStr(e_TipusControl.tcGMSRadioButton), CStr(e_TipusControl.tcCodejockRadioButton)
					CType(Me.Controls(I), Object).Enabled = True
					'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(Me(I).Tag, S, 4). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
					If Piece(CType(Me.Controls(I), Object).Tag, S, 4) = 0 Then
						CType(Me.Controls(I), Object).Enabled = False
					End If
			End Select
		Next I
		
	End Sub
	
	Private Sub BorraDadesOrigen()
		Exit Sub
		txtPuntoRecepcion.Text = ""
		Text16.Text = ""
		txtEntidadRecepcion.Text = ""
		txtSucursalRecepcion.Text = ""
		Text24.Text = ""
		txtDireccionRecepcion.Text = ""
		txtSubdireccionOrigen.Text = ""
		txtPoblacionRecepcion.Text = ""
		Text18.Text = ""
		txtCpRecepcion.Text = ""
		txtTelefonoRecepcion.Text = ""
		txtContactoRecepcion.Text = ""
		txtHorarioRecepcion.Text = ""
		txtObservacionesorigen.Text = ""
	End Sub
	
	Private Sub BorraDadesDesti()
		Exit Sub
		txtPuntoEntrega.Text = ""
		Text25.Text = ""
		txtEntidadEntrega.Text = ""
		txtSucursalEntrega.Text = ""
		Text33.Text = ""
		txtDireccionEntrega.Text = ""
		txtSubdireccionDestino.Text = ""
		txtPoblacionEntrega.Text = ""
		Text27.Text = ""
		txtCpEntrega.Text = ""
		txtTelefonoEntrega.Text = ""
		txtContactoEntrega.Text = ""
		txtHorarioEntrega.Text = ""
		txtObservacionesDestino.Text = ""
	End Sub
	
	Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Dim I As Short
		Dim FRM As FormParent
		
		Select Case KeyMenu
			Case "ASOL"
				ObreSeguiment()
		End Select
	End Sub
	
	Private Sub ObreSeguiment()
		Dim sol As String
		Dim Num As Short
		
		If grdHSol.MouseRow < 1 Then Exit Sub
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		sol = Piece(grdHSol.Cell(grdHSol.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Num = Piece(grdHSol.Cell(grdHSol.ActiveCell.Row, 1).Text, "|", 1)
		ObreFormulari(frmSolicitudTransportHistoric, "frmSolicitudTransportHistoric", sol & S & Num)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
	End Sub
End Class
