Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmGestionsVaries
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	Private Sub frmGestionsVaries_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmGestionsVaries_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmGestionsVaries_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmGestionsVaries_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	Private Sub txtTecnic_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnic.GotFocus
		XGotFocus(Me, txtTecnic)
	End Sub
	
	Private Sub txtTecnic_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnic.DoubleClick
		ConsultaTaula(Me, txtTecnic)
	End Sub
	
	Private Sub txtTecnic_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnic.LostFocus
		If txtTecnic.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtTecnic)
	End Sub
	
	Private Sub txtTecnic_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTecnic.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTecnic)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtTecnic)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtData_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtData.GotFocus
		XGotFocus(Me, txtData)
	End Sub
	
	Private Sub txtData_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtData.LostFocus
		If txtData.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtData)
	End Sub
	
	Private Sub txtData_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtData.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtData)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtNumRegistre_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumRegistre.GotFocus
		XGotFocus(Me, txtNumRegistre)
	End Sub
	
	Private Sub txtNumRegistre_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumRegistre.LostFocus
		If txtNumRegistre.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtNumRegistre)
	End Sub
	
	Private Sub txtNumRegistre_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNumRegistre.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtNumRegistre)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTemps_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTemps.GotFocus
		XGotFocus(Me, txtTemps)
	End Sub
	
	Private Sub txtTemps_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTemps.LostFocus
		XLostFocus(Me, txtTemps)
	End Sub
	
	Private Sub txtTemps_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTemps.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtTemps)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipusGestio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusGestio.GotFocus
		XGotFocus(Me, txtTipusGestio)
	End Sub
	
	Private Sub txtTipusGestio_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusGestio.DoubleClick
		ConsultaTaula(Me, txtTipusGestio)
	End Sub
	
	Private Sub txtTipusGestio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusGestio.LostFocus
		XLostFocus(Me, txtTipusGestio)
	End Sub
	
	Private Sub txtTipusGestio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipusGestio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTipusGestio)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtTipusGestio)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservacions_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.GotFocus
		XGotFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.LostFocus
		XLostFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservacions.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtObservacions)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
End Class
