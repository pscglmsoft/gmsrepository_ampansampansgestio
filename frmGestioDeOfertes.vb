Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmGestioDeOfertes
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic FlagExtern As Boolean
	''OKPublic RegistreExtern As String
	''OKPublic Opcions As String
	
	Public Overrides Sub Inici()
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
		ResetForm(Me)
	End Sub
	
	'UPGRADE_WARNING: Form evento frmGestioDeOfertes.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmGestioDeOfertes_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaPresentats()
		CarregaEntrevistats()
		CarregaInsercions()
	End Sub
	
	Private Sub frmGestioDeOfertes_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		FlagExtern = False
		RegistreExtern = ""
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmGestioDeOfertes_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
	End Sub
	
	Private Sub frmGestioDeOfertes_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmGestioDeOfertes_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	
	
	
	Private Sub GrdEntrevistat_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdEntrevistat.DoubleClick
		If GrdEntrevistat.Selection.FirstRow >= 1 Then
			ObreEntrevistat()
		End If
	End Sub
	
	Private Sub GrdEntrevistat_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdEntrevistat.MouseUp
		If eventArgs.Button = MouseButtons.Left And GrdEntrevistat.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdEntrevistat", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar entrevistat per oferta",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar entrevistat per oferta",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou entrevistat per oferta",  , True,  ,  ,  ,  ,  , "mnuNew")
				If GrdEntrevistat.ActiveCell.Row < 1 Or GrdEntrevistat.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "GrdEntrevistat")
	End Sub
	
	
	
	Private Sub GrdInsercions_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdInsercions.DoubleClick
		If GrdInsercions.Selection.FirstRow >= 1 Then
			ObreInsercions()
		End If
	End Sub
	
	Private Sub GrdInsercions_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdInsercions.MouseUp
		If eventArgs.Button = MouseButtons.Left And GrdInsercions.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdInsercions", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar insercions per oferta",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar insercions per oferta",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou insercions per oferta",  , True,  ,  ,  ,  ,  , "mnuNew")
				If GrdInsercions.ActiveCell.Row < 1 Or GrdInsercions.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "GrdInsercions")
	End Sub
	
	Private Sub GrdPresentats_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdPresentats.DoubleClick
		If GrdPresentats.Selection.FirstRow >= 1 Then
			ObrePresentat()
		End If
	End Sub
	
	Private Sub GrdPresentats_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdPresentats.MouseUp
		If eventArgs.Button = MouseButtons.Left And GrdPresentats.Selection.FirstRow >= 1 Then
			Exit Sub
		End If
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdPresentats", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar presentat per oferta",  , True,  ,  ,  ,  ,  , "mnuDelete")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar presentat per oferta",  , True,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nou presentat per oferta",  , True,  ,  ,  ,  ,  , "mnuNew")
				If GrdPresentats.ActiveCell.Row < 1 Or GrdPresentats.Selection.FirstRow < 1 Then
					.MenuItems.Item("mnuDelete").Enabled = False
					.MenuItems.Item("mnuEdit").Enabled = False
				End If
			End With
		End With
		XpExecutaMenu(Me, "GrdPresentats")
	End Sub
	
	Private Sub TabControl1_Selected(sender As Object, ByVal eventArgs As TabControlEventArgs) Handles TabControl1.Selected
		CarregaGrids((eventArgs.TabPageIndex))
	End Sub
	
	Private Sub txtCodi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.GotFocus
		XGotFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.DoubleClick
		ConsultaTaula(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCodi.LostFocus
		If txtCodi.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtCodi)
	End Sub
	
	Private Sub txtCodi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtCodi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtCodi)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDescripcio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.GotFocus
		XGotFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDescripcio.LostFocus
		XLostFocus(Me, txtDescripcio)
	End Sub
	
	Private Sub txtDescripcio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDescripcio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtDescripcio)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtEntitat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntitat.GotFocus
		XGotFocus(Me, txtEntitat)
	End Sub
	
	Private Sub txtEntitat_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntitat.DoubleClick
		ConsultaTaula(Me, txtEntitat)
	End Sub
	
	Private Sub txtEntitat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntitat.LostFocus
		XLostFocus(Me, txtEntitat)
	End Sub
	
	Private Sub txtEntitat_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEntitat.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtEntitat)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtEntitat)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSucursal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.GotFocus
		XGotFocus(Me, txtSucursal)
	End Sub
	
	Private Sub txtSucursal_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.DoubleClick
		ConsultaTaula(Me, txtSucursal,  , txtEntitat.Text & S & "S")
	End Sub
	
	Private Sub txtSucursal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.LostFocus
		XLostFocus(Me, txtSucursal)
	End Sub
	
	Private Sub txtSucursal_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSucursal.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtSucursal, txtEntitat.Text & S & "S")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtSucursal)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtServeiGestor_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServeiGestor.GotFocus
		XGotFocus(Me, txtServeiGestor)
	End Sub
	
	Private Sub txtServeiGestor_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServeiGestor.DoubleClick
		ConsultaTaula(Me, txtServeiGestor)
	End Sub
	
	Private Sub txtServeiGestor_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServeiGestor.LostFocus
		XLostFocus(Me, txtServeiGestor)
	End Sub
	
	Private Sub txtServeiGestor_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtServeiGestor.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtServeiGestor)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtServeiGestor)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTecnic_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnic.GotFocus
		XGotFocus(Me, txtTecnic)
	End Sub
	
	Private Sub txtTecnic_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnic.DoubleClick
		ConsultaTaula(Me, txtTecnic)
	End Sub
	
	Private Sub txtTecnic_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnic.LostFocus
		XLostFocus(Me, txtTecnic)
	End Sub
	
	Private Sub txtTecnic_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTecnic.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtTecnic)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtTecnic)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDataAlta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataAlta.GotFocus
		XGotFocus(Me, txtDataAlta)
	End Sub
	
	Private Sub txtDataAlta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataAlta.LostFocus
		XLostFocus(Me, txtDataAlta)
	End Sub
	
	Private Sub txtDataAlta_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDataAlta.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtDataAlta)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtDataBaixa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataBaixa.GotFocus
		XGotFocus(Me, txtDataBaixa)
	End Sub
	
	Private Sub txtDataBaixa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtDataBaixa.LostFocus
		XLostFocus(Me, txtDataBaixa)
	End Sub
	
	Private Sub txtDataBaixa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtDataBaixa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtDataBaixa)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtMotiuBaixa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotiuBaixa.GotFocus
		XGotFocus(Me, txtMotiuBaixa)
	End Sub
	
	Private Sub txtMotiuBaixa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotiuBaixa.DoubleClick
		ConsultaTaula(Me, txtMotiuBaixa)
	End Sub
	
	Private Sub txtMotiuBaixa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtMotiuBaixa.LostFocus
		XLostFocus(Me, txtMotiuBaixa)
	End Sub
	
	Private Sub txtMotiuBaixa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtMotiuBaixa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtMotiuBaixa)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtMotiuBaixa)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtNumLlocs_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumLlocs.GotFocus
		XGotFocus(Me, txtNumLlocs)
	End Sub
	
	Private Sub txtNumLlocs_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumLlocs.LostFocus
		XLostFocus(Me, txtNumLlocs)
	End Sub
	
	Private Sub txtNumLlocs_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNumLlocs.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtNumLlocs)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtLlocTreball_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLlocTreball.GotFocus
		XGotFocus(Me, txtLlocTreball)
	End Sub
	
	Private Sub txtLlocTreball_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLlocTreball.DoubleClick
		ConsultaTaula(Me, txtLlocTreball)
	End Sub
	
	Private Sub txtLlocTreball_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtLlocTreball.LostFocus
		XLostFocus(Me, txtLlocTreball)
	End Sub
	
	Private Sub txtLlocTreball_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtLlocTreball.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtLlocTreball)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtLlocTreball)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbCertificatDiscapacitat_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbCertificatDiscapacitat.GotFocus
		XGotFocus(Me, cmbCertificatDiscapacitat)
	End Sub
	
	Private Sub cmbCertificatDiscapacitat_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbCertificatDiscapacitat.LostFocus
		XLostFocus(Me, cmbCertificatDiscapacitat)
	End Sub
	
	Private Sub cmbCertificatDiscapacitat_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles cmbCertificatDiscapacitat.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, cmbCertificatDiscapacitat)
		If Cancel = True Then GoTo EventExitSub
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtGrauDiscapacitatSuperi_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGrauDiscapacitatSuperi.GotFocus
		XGotFocus(Me, txtGrauDiscapacitatSuperi)
	End Sub
	
	Private Sub txtGrauDiscapacitatSuperi_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtGrauDiscapacitatSuperi.LostFocus
		XLostFocus(Me, txtGrauDiscapacitatSuperi)
	End Sub
	
	Private Sub txtGrauDiscapacitatSuperi_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtGrauDiscapacitatSuperi.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtGrauDiscapacitatSuperi)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtObservacions_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.GotFocus
		XGotFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtObservacions.LostFocus
		XLostFocus(Me, txtObservacions)
	End Sub
	
	Private Sub txtObservacions_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtObservacions.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, txtObservacions)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub CarregaGrids(ByRef curtab As Short)
		Select Case curtab
			Case 1
				CarregaPresentats()
			Case 2
				CarregaEntrevistats()
			Case 3
				CarregaInsercions()
		End Select
	End Sub
	
	Private Sub CarregaPresentats()
		CacheNetejaParametres()
		MCache.P1 = txtCodi.Text
		CarregaFGrid(GrdPresentats, "CGPREENT^INSERCIO", txtCodi.Text & S & 1)
	End Sub
	
	Private Sub CarregaEntrevistats()
		CacheNetejaParametres()
		MCache.P1 = txtCodi.Text
		CarregaFGrid(GrdEntrevistat, "CGPREENT^INSERCIO", txtCodi.Text & S & 2)
	End Sub
	
	Private Sub CarregaInsercions()
		CacheNetejaParametres()
		MCache.P1 = txtCodi.Text
		CarregaFGrid(GrdInsercions, "CGINSER^INSERCIO", txtCodi.Text)
	End Sub
	
	Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Select Case NomMenu
			
			Case "GrdPresentats"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObrePresentat()
						LlibAplicacio.DeleteReg(frmEntrevistesPresentats)
						CarregaEntrevistats()
					Case "mnuEdit"
						ObrePresentat()
					Case "mnuNew"
						NouPresentat()
				End Select
			Case "GrdEntrevistat"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObreEntrevistat()
						LlibAplicacio.DeleteReg(frmEntrevistesPresentats)
						CarregaEntrevistats()
					Case "mnuEdit"
						ObreEntrevistat()
					Case "mnuNew"
						NouEntrevistat()
				End Select
			Case "GrdInsercions"
				If GravaRegistre(Me) = False Then Exit Sub
				Select Case KeyMenu
					Case "mnuDelete"
						ObreInsercions()
						LlibAplicacio.DeleteReg(frmEmpresaInseridora)
						CarregaInsercions()
					Case "mnuEdit"
						ObreInsercions()
					Case "mnuNew"
						NovaInsercio()
				End Select
		End Select
	End Sub
	
	
	Private Sub ObrePresentat()
		'UPGRADE_NOTE: OF se actualiz� a OF_Renamed. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim OF_Renamed As Short
		Dim Per As Short
		Dim Data As String
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		OF_Renamed = Piece(GrdPresentats.Cell(GrdPresentats.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Per = Piece(GrdPresentats.Cell(GrdPresentats.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(GrdPresentats.Cell(GrdPresentats.ActiveCell.Row, 1).Text, "|", 3)
		ObreFormulari(frmEntrevistesPresentats, "frmEntrevistesPresentats", OF_Renamed & S & Per & S & Data & S & 1)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
	End Sub
	
	Private Sub NouPresentat()
		ObreFormulari(frmEntrevistesPresentats, "frmEntrevistesPresentats", txtCodi.Text & S & "" & S & "" & S & 1)
	End Sub
	
	Private Sub ObreEntrevistat()
		'UPGRADE_NOTE: OF se actualiz� a OF_Renamed. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim OF_Renamed As Short
		Dim Per As Short
		Dim Data As String
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		OF_Renamed = Piece(GrdEntrevistat.Cell(GrdEntrevistat.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Per = Piece(GrdEntrevistat.Cell(GrdEntrevistat.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(GrdEntrevistat.Cell(GrdEntrevistat.ActiveCell.Row, 1).Text, "|", 3)
		ObreFormulari(frmEntrevistesPresentats, "frmEntrevistesPresentats", OF_Renamed & S & Per & S & Data & S & 2)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
	End Sub
	
	Private Sub NouEntrevistat()
		ObreFormulari(frmEntrevistesPresentats, "frmEntrevistesPresentats", txtCodi.Text & S & "" & S & "" & S & 2)
	End Sub
	
	Private Sub ObreInsercions()
		Dim Em As Short
		Dim Suc As Short
		Dim Per As Short
		Dim Data As String
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Em = Piece(GrdInsercions.Cell(GrdInsercions.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Suc = Piece(GrdInsercions.Cell(GrdInsercions.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Per = Piece(GrdInsercions.Cell(GrdInsercions.ActiveCell.Row, 1).Text, "|", 3)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Data = Piece(GrdInsercions.Cell(GrdInsercions.ActiveCell.Row, 1).Text, "|", 4)
		ObreFormulari(frmEmpresaInseridora, "frmEmpresaInseridora", Em & S & Suc & S & Per & S & Data)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
	End Sub
	
	
	Private Sub NovaInsercio()
		ObreFormulari(frmEmpresaInseridora, "frmEmpresaInseridora", txtEntitat.Text & S & txtSucursal.Text & S & "" & S & "" & S & "" & S & "" & S & "" & S & "" & S & "" & S & 1 & S & txtCodi.Text & S & "" & S & txtServeiGestor.Text)
	End Sub
End Class
