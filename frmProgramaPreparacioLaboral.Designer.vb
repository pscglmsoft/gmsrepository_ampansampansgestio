<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSeguimentUsuari
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text14 As System.Windows.Forms.TextBox
	Public WithEvents txtOferta As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtSessioFormativa As System.Windows.Forms.TextBox
	Public WithEvents txtObservaciones As System.Windows.Forms.TextBox
	Public WithEvents txtUsuario As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtFechaAccion As AxDataControl.AxGmsData
	Public WithEvents txtNumRegistro As System.Windows.Forms.TextBox
	Public WithEvents txtTipoAccion As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtTipoSubaccion As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtTecnico As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtServicio As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents cmbOrigen As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents cmbEstado As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents GrdRelacions As AxFlexCell.AxGrid
	Public WithEvents txtTiempo As AxgmsTime.AxgmsTemps
	Public WithEvents txtTipusSeguiment As System.Windows.Forms.TextBox
	Public WithEvents _optSessio_1 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents _optSessio_2 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents frTipo As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents cmdSessio As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl14 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents lbl43 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl37 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents optSessio As AxRadioButtonArray
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSeguimentUsuari))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.Text14 = New System.Windows.Forms.TextBox
		Me.txtOferta = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtSessioFormativa = New System.Windows.Forms.TextBox
		Me.txtObservaciones = New System.Windows.Forms.TextBox
		Me.txtUsuario = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtFechaAccion = New AxDataControl.AxGmsData
		Me.txtNumRegistro = New System.Windows.Forms.TextBox
		Me.txtTipoAccion = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtTipoSubaccion = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtTecnico = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtServicio = New System.Windows.Forms.TextBox
		Me.Text7 = New System.Windows.Forms.TextBox
		Me.cmbOrigen = New AxSSDataWidgets_B.AxSSDBCombo
		Me.cmbEstado = New AxSSDataWidgets_B.AxSSDBCombo
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.GrdRelacions = New AxFlexCell.AxGrid
		Me.txtTiempo = New AxgmsTime.AxgmsTemps
		Me.frTipo = New AxXtremeSuiteControls.AxGroupBox
		Me.txtTipusSeguiment = New System.Windows.Forms.TextBox
		Me._optSessio_1 = New AxXtremeSuiteControls.AxRadioButton
		Me._optSessio_2 = New AxXtremeSuiteControls.AxRadioButton
		Me.cmdSessio = New AxXtremeSuiteControls.AxPushButton
		Me.lbl14 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.lbl43 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl37 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.optSessio = New AxRadioButtonArray(components)
		Me.frTipo.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtFechaAccion, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbOrigen, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbEstado, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdRelacions, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtTiempo, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optSessio_1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optSessio_2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.frTipo, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdSessio, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.optSessio, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Seguimiento usuario"
		Me.ClientSize = New System.Drawing.Size(532, 541)
		Me.Location = New System.Drawing.Point(302, 163)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-SEGUIMENT_LABORAL"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmSeguimentUsuari"
		Me.Text14.AutoSize = False
		Me.Text14.BackColor = System.Drawing.Color.White
		Me.Text14.Enabled = False
		Me.Text14.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text14.Size = New System.Drawing.Size(360, 19)
		Me.Text14.Location = New System.Drawing.Point(158, 154)
		Me.Text14.TabIndex = 20
		Me.Text14.Tag = "^14"
		Me.Text14.AcceptsReturn = True
		Me.Text14.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text14.CausesValidation = True
		Me.Text14.HideSelection = True
		Me.Text14.ReadOnly = False
		Me.Text14.Maxlength = 0
		Me.Text14.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text14.MultiLine = False
		Me.Text14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text14.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text14.TabStop = True
		Me.Text14.Visible = True
		Me.Text14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text14.Name = "Text14"
		Me.txtOferta.AutoSize = False
		Me.txtOferta.Size = New System.Drawing.Size(31, 19)
		Me.txtOferta.Location = New System.Drawing.Point(124, 154)
		Me.txtOferta.Maxlength = 3
		Me.txtOferta.TabIndex = 7
		Me.txtOferta.Tag = "14"
		Me.txtOferta.AcceptsReturn = True
		Me.txtOferta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtOferta.BackColor = System.Drawing.SystemColors.Window
		Me.txtOferta.CausesValidation = True
		Me.txtOferta.Enabled = True
		Me.txtOferta.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtOferta.HideSelection = True
		Me.txtOferta.ReadOnly = False
		Me.txtOferta.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtOferta.MultiLine = False
		Me.txtOferta.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtOferta.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtOferta.TabStop = True
		Me.txtOferta.Visible = True
		Me.txtOferta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtOferta.Name = "txtOferta"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(342, 19)
		Me.Text3.Location = New System.Drawing.Point(148, 130)
		Me.Text3.TabIndex = 19
		Me.Text3.Tag = "^12"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtSessioFormativa.AutoSize = False
		Me.txtSessioFormativa.Size = New System.Drawing.Size(21, 19)
		Me.txtSessioFormativa.Location = New System.Drawing.Point(124, 130)
		Me.txtSessioFormativa.Maxlength = 2
		Me.txtSessioFormativa.TabIndex = 6
		Me.txtSessioFormativa.Tag = "12"
		Me.txtSessioFormativa.AcceptsReturn = True
		Me.txtSessioFormativa.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtSessioFormativa.BackColor = System.Drawing.SystemColors.Window
		Me.txtSessioFormativa.CausesValidation = True
		Me.txtSessioFormativa.Enabled = True
		Me.txtSessioFormativa.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtSessioFormativa.HideSelection = True
		Me.txtSessioFormativa.ReadOnly = False
		Me.txtSessioFormativa.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtSessioFormativa.MultiLine = False
		Me.txtSessioFormativa.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtSessioFormativa.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtSessioFormativa.TabStop = True
		Me.txtSessioFormativa.Visible = True
		Me.txtSessioFormativa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtSessioFormativa.Name = "txtSessioFormativa"
		Me.txtObservaciones.AutoSize = False
		Me.txtObservaciones.Size = New System.Drawing.Size(395, 57)
		Me.txtObservaciones.Location = New System.Drawing.Point(124, 322)
		Me.txtObservaciones.Maxlength = 200
		Me.txtObservaciones.MultiLine = True
		Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtObservaciones.TabIndex = 14
		Me.txtObservaciones.Tag = "10"
		Me.txtObservaciones.AcceptsReturn = True
		Me.txtObservaciones.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservaciones.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservaciones.CausesValidation = True
		Me.txtObservaciones.Enabled = True
		Me.txtObservaciones.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservaciones.HideSelection = True
		Me.txtObservaciones.ReadOnly = False
		Me.txtObservaciones.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservaciones.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservaciones.TabStop = True
		Me.txtObservaciones.Visible = True
		Me.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservaciones.Name = "txtObservaciones"
		Me.txtUsuario.AutoSize = False
		Me.txtUsuario.Size = New System.Drawing.Size(42, 19)
		Me.txtUsuario.Location = New System.Drawing.Point(124, 10)
		Me.txtUsuario.Maxlength = 4
		Me.txtUsuario.TabIndex = 0
		Me.txtUsuario.Tag = "*1"
		Me.txtUsuario.AcceptsReturn = True
		Me.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsuario.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsuario.CausesValidation = True
		Me.txtUsuario.Enabled = True
		Me.txtUsuario.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsuario.HideSelection = True
		Me.txtUsuario.ReadOnly = False
		Me.txtUsuario.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsuario.MultiLine = False
		Me.txtUsuario.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsuario.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsuario.TabStop = True
		Me.txtUsuario.Visible = True
		Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUsuario.Name = "txtUsuario"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(348, 19)
		Me.Text1.Location = New System.Drawing.Point(169, 10)
		Me.Text1.TabIndex = 18
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		txtFechaAccion.OcxState = CType(resources.GetObject("txtFechaAccion.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaAccion.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaAccion.Location = New System.Drawing.Point(124, 34)
		Me.txtFechaAccion.TabIndex = 1
		Me.txtFechaAccion.Name = "txtFechaAccion"
		Me.txtNumRegistro.AutoSize = False
		Me.txtNumRegistro.Size = New System.Drawing.Size(21, 19)
		Me.txtNumRegistro.Location = New System.Drawing.Point(124, 58)
		Me.txtNumRegistro.Maxlength = 2
		Me.txtNumRegistro.TabIndex = 3
		Me.txtNumRegistro.Tag = "*3"
		Me.txtNumRegistro.AcceptsReturn = True
		Me.txtNumRegistro.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNumRegistro.BackColor = System.Drawing.SystemColors.Window
		Me.txtNumRegistro.CausesValidation = True
		Me.txtNumRegistro.Enabled = True
		Me.txtNumRegistro.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNumRegistro.HideSelection = True
		Me.txtNumRegistro.ReadOnly = False
		Me.txtNumRegistro.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNumRegistro.MultiLine = False
		Me.txtNumRegistro.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNumRegistro.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNumRegistro.TabStop = True
		Me.txtNumRegistro.Visible = True
		Me.txtNumRegistro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNumRegistro.Name = "txtNumRegistro"
		Me.txtTipoAccion.AutoSize = False
		Me.txtTipoAccion.Size = New System.Drawing.Size(21, 19)
		Me.txtTipoAccion.Location = New System.Drawing.Point(124, 202)
		Me.txtTipoAccion.Maxlength = 2
		Me.txtTipoAccion.TabIndex = 9
		Me.txtTipoAccion.Tag = "4"
		Me.txtTipoAccion.AcceptsReturn = True
		Me.txtTipoAccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoAccion.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoAccion.CausesValidation = True
		Me.txtTipoAccion.Enabled = True
		Me.txtTipoAccion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoAccion.HideSelection = True
		Me.txtTipoAccion.ReadOnly = False
		Me.txtTipoAccion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoAccion.MultiLine = False
		Me.txtTipoAccion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoAccion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoAccion.TabStop = True
		Me.txtTipoAccion.Visible = True
		Me.txtTipoAccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoAccion.Name = "txtTipoAccion"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(370, 19)
		Me.Text4.Location = New System.Drawing.Point(148, 202)
		Me.Text4.TabIndex = 21
		Me.Text4.Tag = "^4"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtTipoSubaccion.AutoSize = False
		Me.txtTipoSubaccion.Size = New System.Drawing.Size(21, 19)
		Me.txtTipoSubaccion.Location = New System.Drawing.Point(124, 226)
		Me.txtTipoSubaccion.Maxlength = 2
		Me.txtTipoSubaccion.TabIndex = 10
		Me.txtTipoSubaccion.Tag = "5"
		Me.txtTipoSubaccion.AcceptsReturn = True
		Me.txtTipoSubaccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoSubaccion.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoSubaccion.CausesValidation = True
		Me.txtTipoSubaccion.Enabled = True
		Me.txtTipoSubaccion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoSubaccion.HideSelection = True
		Me.txtTipoSubaccion.ReadOnly = False
		Me.txtTipoSubaccion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoSubaccion.MultiLine = False
		Me.txtTipoSubaccion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoSubaccion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoSubaccion.TabStop = True
		Me.txtTipoSubaccion.Visible = True
		Me.txtTipoSubaccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoSubaccion.Name = "txtTipoSubaccion"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(370, 19)
		Me.Text5.Location = New System.Drawing.Point(148, 226)
		Me.Text5.TabIndex = 22
		Me.Text5.Tag = "^5"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtTecnico.AutoSize = False
		Me.txtTecnico.Size = New System.Drawing.Size(99, 19)
		Me.txtTecnico.Location = New System.Drawing.Point(124, 250)
		Me.txtTecnico.Maxlength = 8
		Me.txtTecnico.TabIndex = 11
		Me.txtTecnico.Tag = "6"
		Me.txtTecnico.AcceptsReturn = True
		Me.txtTecnico.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTecnico.BackColor = System.Drawing.SystemColors.Window
		Me.txtTecnico.CausesValidation = True
		Me.txtTecnico.Enabled = True
		Me.txtTecnico.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTecnico.HideSelection = True
		Me.txtTecnico.ReadOnly = False
		Me.txtTecnico.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTecnico.MultiLine = False
		Me.txtTecnico.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTecnico.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTecnico.TabStop = True
		Me.txtTecnico.Visible = True
		Me.txtTecnico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTecnico.Name = "txtTecnico"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(292, 19)
		Me.Text6.Location = New System.Drawing.Point(226, 250)
		Me.Text6.TabIndex = 23
		Me.Text6.Tag = "^6"
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.Visible = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtServicio.AutoSize = False
		Me.txtServicio.Size = New System.Drawing.Size(21, 19)
		Me.txtServicio.Location = New System.Drawing.Point(124, 274)
		Me.txtServicio.Maxlength = 2
		Me.txtServicio.TabIndex = 12
		Me.txtServicio.Tag = "7"
		Me.txtServicio.AcceptsReturn = True
		Me.txtServicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtServicio.BackColor = System.Drawing.SystemColors.Window
		Me.txtServicio.CausesValidation = True
		Me.txtServicio.Enabled = True
		Me.txtServicio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtServicio.HideSelection = True
		Me.txtServicio.ReadOnly = False
		Me.txtServicio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtServicio.MultiLine = False
		Me.txtServicio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtServicio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtServicio.TabStop = True
		Me.txtServicio.Visible = True
		Me.txtServicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtServicio.Name = "txtServicio"
		Me.Text7.AutoSize = False
		Me.Text7.BackColor = System.Drawing.Color.White
		Me.Text7.Enabled = False
		Me.Text7.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text7.Size = New System.Drawing.Size(370, 19)
		Me.Text7.Location = New System.Drawing.Point(148, 274)
		Me.Text7.TabIndex = 24
		Me.Text7.Tag = "^7"
		Me.Text7.AcceptsReturn = True
		Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text7.CausesValidation = True
		Me.Text7.HideSelection = True
		Me.Text7.ReadOnly = False
		Me.Text7.Maxlength = 0
		Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text7.MultiLine = False
		Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text7.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text7.TabStop = True
		Me.Text7.Visible = True
		Me.Text7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text7.Name = "Text7"
		cmbOrigen.OcxState = CType(resources.GetObject("cmbOrigen.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbOrigen.Size = New System.Drawing.Size(203, 19)
		Me.cmbOrigen.Location = New System.Drawing.Point(124, 298)
		Me.cmbOrigen.TabIndex = 13
		Me.cmbOrigen.Name = "cmbOrigen"
		cmbEstado.OcxState = CType(resources.GetObject("cmbEstado.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbEstado.Size = New System.Drawing.Size(135, 19)
		Me.cmbEstado.Location = New System.Drawing.Point(382, 34)
		Me.cmbEstado.TabIndex = 2
		Me.cmbEstado.Name = "cmbEstado"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(347, 502)
		Me.cmdAceptar.TabIndex = 16
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(437, 502)
		Me.cmdGuardar.TabIndex = 17
		Me.cmdGuardar.Name = "cmdGuardar"
		GrdRelacions.OcxState = CType(resources.GetObject("GrdRelacions.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdRelacions.Size = New System.Drawing.Size(508, 92)
		Me.GrdRelacions.Location = New System.Drawing.Point(12, 396)
		Me.GrdRelacions.TabIndex = 25
		Me.GrdRelacions.Name = "GrdRelacions"
		txtTiempo.OcxState = CType(resources.GetObject("txtTiempo.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtTiempo.Size = New System.Drawing.Size(52, 19)
		Me.txtTiempo.Location = New System.Drawing.Point(124, 178)
		Me.txtTiempo.TabIndex = 8
		Me.txtTiempo.Name = "txtTiempo"
		frTipo.OcxState = CType(resources.GetObject("frTipo.OcxState"), System.Windows.Forms.AxHost.State)
		Me.frTipo.Size = New System.Drawing.Size(287, 39)
		Me.frTipo.Location = New System.Drawing.Point(10, 86)
		Me.frTipo.TabIndex = 40
		Me.frTipo.Name = "frTipo"
		Me.txtTipusSeguiment.AutoSize = False
		Me.txtTipusSeguiment.BackColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.txtTipusSeguiment.Size = New System.Drawing.Size(11, 19)
		Me.txtTipusSeguiment.Location = New System.Drawing.Point(0, 0)
		Me.txtTipusSeguiment.Maxlength = 1
		Me.txtTipusSeguiment.TabIndex = 41
		Me.txtTipusSeguiment.Tag = "13"
		Me.txtTipusSeguiment.AcceptsReturn = True
		Me.txtTipusSeguiment.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipusSeguiment.CausesValidation = True
		Me.txtTipusSeguiment.Enabled = True
		Me.txtTipusSeguiment.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipusSeguiment.HideSelection = True
		Me.txtTipusSeguiment.ReadOnly = False
		Me.txtTipusSeguiment.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipusSeguiment.MultiLine = False
		Me.txtTipusSeguiment.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipusSeguiment.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipusSeguiment.TabStop = True
		Me.txtTipusSeguiment.Visible = True
		Me.txtTipusSeguiment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipusSeguiment.Name = "txtTipusSeguiment"
		_optSessio_1.OcxState = CType(resources.GetObject("_optSessio_1.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optSessio_1.Size = New System.Drawing.Size(107, 21)
		Me._optSessio_1.Location = New System.Drawing.Point(32, 12)
		Me._optSessio_1.TabIndex = 4
		Me._optSessio_1.Name = "_optSessio_1"
		_optSessio_2.OcxState = CType(resources.GetObject("_optSessio_2.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optSessio_2.Size = New System.Drawing.Size(97, 21)
		Me._optSessio_2.Location = New System.Drawing.Point(156, 12)
		Me._optSessio_2.TabIndex = 5
		Me._optSessio_2.Name = "_optSessio_2"
		cmdSessio.OcxState = CType(resources.GetObject("cmdSessio.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdSessio.Size = New System.Drawing.Size(27, 27)
		Me.cmdSessio.Location = New System.Drawing.Point(492, 124)
		Me.cmdSessio.TabIndex = 15
		Me.cmdSessio.Name = "cmdSessio"
		Me.lbl14.Text = "Oferta"
		Me.lbl14.Size = New System.Drawing.Size(111, 15)
		Me.lbl14.Location = New System.Drawing.Point(10, 158)
		Me.lbl14.TabIndex = 31
		Me.lbl14.Tag = "14"
		Me.lbl14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl14.BackColor = System.Drawing.SystemColors.Control
		Me.lbl14.Enabled = True
		Me.lbl14.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl14.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl14.UseMnemonic = True
		Me.lbl14.Visible = True
		Me.lbl14.AutoSize = False
		Me.lbl14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl14.Name = "lbl14"
		Me.Label2.Text = "Sessi� Formativa"
		Me.Label2.Size = New System.Drawing.Size(111, 15)
		Me.Label2.Location = New System.Drawing.Point(10, 134)
		Me.Label2.TabIndex = 30
		Me.Label2.Tag = "12"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.lbl43.Text = "Temps"
		Me.lbl43.Size = New System.Drawing.Size(111, 15)
		Me.lbl43.Location = New System.Drawing.Point(10, 182)
		Me.lbl43.TabIndex = 32
		Me.lbl43.Tag = "11"
		Me.lbl43.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl43.BackColor = System.Drawing.SystemColors.Control
		Me.lbl43.Enabled = True
		Me.lbl43.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl43.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl43.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl43.UseMnemonic = True
		Me.lbl43.Visible = True
		Me.lbl43.AutoSize = False
		Me.lbl43.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl43.Name = "lbl43"
		Me.Label1.Text = "Relaciones"
		Me.Label1.ForeColor = System.Drawing.Color.FromARGB(0, 0, 192)
		Me.Label1.Size = New System.Drawing.Size(79, 17)
		Me.Label1.Location = New System.Drawing.Point(14, 378)
		Me.Label1.TabIndex = 39
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl37.Text = "Observaciones"
		Me.lbl37.Size = New System.Drawing.Size(111, 15)
		Me.lbl37.Location = New System.Drawing.Point(10, 326)
		Me.lbl37.TabIndex = 38
		Me.lbl37.Tag = "10"
		Me.lbl37.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl37.BackColor = System.Drawing.SystemColors.Control
		Me.lbl37.Enabled = True
		Me.lbl37.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl37.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl37.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl37.UseMnemonic = True
		Me.lbl37.Visible = True
		Me.lbl37.AutoSize = False
		Me.lbl37.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl37.Name = "lbl37"
		Me.lbl1.Text = "Usuario"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 26
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Fecha acci�n"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 27
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Num. registro"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 29
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Tipo acci�n"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 206)
		Me.lbl4.TabIndex = 33
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Tipo subacci�n"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 230)
		Me.lbl5.TabIndex = 34
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "T�cnico"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 254)
		Me.lbl6.TabIndex = 35
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl7.Text = "Servicio"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(10, 278)
		Me.lbl7.TabIndex = 36
		Me.lbl7.Tag = "7"
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.Visible = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl8.Text = "Origen"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(10, 302)
		Me.lbl8.TabIndex = 37
		Me.lbl8.Tag = "8"
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.Visible = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl9.Text = "Estado"
		Me.lbl9.Size = New System.Drawing.Size(47, 15)
		Me.lbl9.Location = New System.Drawing.Point(332, 38)
		Me.lbl9.TabIndex = 28
		Me.lbl9.Tag = "9"
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.Visible = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 501)
		Me.lblLock.TabIndex = 42
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 495
		Me.Line1.Y2 = 495
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 496
		Me.Line2.Y2 = 496
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		Me.optSessio.SetIndex(_optSessio_1, CType(1, Short))
		Me.optSessio.SetIndex(_optSessio_2, CType(2, Short))
		CType(Me.optSessio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdSessio, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.frTipo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optSessio_2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optSessio_1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtTiempo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdRelacions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbEstado, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbOrigen, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaAccion, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(Text14)
		Me.Controls.Add(txtOferta)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtSessioFormativa)
		Me.Controls.Add(txtObservaciones)
		Me.Controls.Add(txtUsuario)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtFechaAccion)
		Me.Controls.Add(txtNumRegistro)
		Me.Controls.Add(txtTipoAccion)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtTipoSubaccion)
		Me.Controls.Add(Text5)
		Me.Controls.Add(txtTecnico)
		Me.Controls.Add(Text6)
		Me.Controls.Add(txtServicio)
		Me.Controls.Add(Text7)
		Me.Controls.Add(cmbOrigen)
		Me.Controls.Add(cmbEstado)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(GrdRelacions)
		Me.Controls.Add(txtTiempo)
		Me.Controls.Add(frTipo)
		Me.Controls.Add(cmdSessio)
		Me.Controls.Add(lbl14)
		Me.Controls.Add(Label2)
		Me.Controls.Add(lbl43)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl37)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lbl7)
		Me.Controls.Add(lbl8)
		Me.Controls.Add(lbl9)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.frTipo.Controls.Add(txtTipusSeguiment)
		Me.frTipo.Controls.Add(_optSessio_1)
		Me.frTipo.Controls.Add(_optSessio_2)
		Me.frTipo.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class