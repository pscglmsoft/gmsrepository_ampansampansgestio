Option Strict Off
Option Explicit On
Friend Class frmDiferents
	Inherits FormParent
	
	Private Sub cmdCorrecte_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCorrecte.Click
		CacheNetejaParametres()
		MCache.P1 = FrmVerificacioFaixes.txtFaixa
		MCache.P2 = FrmVerificacioFaixes.txtFaixaCompara
		MCache.P3 = "NO"
		CacheXecute("D GRAVAF^GATES")
		
		Me.Unload()
		FrmVerificacioFaixes.txtFaixaCompara.Text = ""
		FrmVerificacioFaixes.txtFaixaCompara.Focus()
	End Sub
	
	Private Sub frmDiferents_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		CentrarForm(Me)
	End Sub
End Class
