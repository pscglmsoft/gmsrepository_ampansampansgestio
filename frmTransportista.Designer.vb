<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmTransportista
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtTelefono As System.Windows.Forms.TextBox
	Public WithEvents GrdVdaInt As AxFlexCell.AxGrid
	Public WithEvents TabControlPage4 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents GrdTTarif As AxFlexCell.AxGrid
	Public WithEvents TabControlPage3 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents grdKM As AxFlexCell.AxGrid
	Public WithEvents TabControlPage2 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents grdPT As AxFlexCell.AxGrid
	Public WithEvents TabControlPage1 As AxXtremeSuiteControls.AxTabControlPage
	Public WithEvents TabControl1 As AxXtremeSuiteControls.AxTabControl
	Public WithEvents txtCodigo As System.Windows.Forms.TextBox
	Public WithEvents txtNombre As System.Windows.Forms.TextBox
	Public WithEvents txtFechaBaja As AxDataControl.AxGmsData
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents txtTipo As System.Windows.Forms.TextBox
	Public WithEvents _optTipo_1 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents _optTipo_2 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents Image1 As System.Windows.Forms.PictureBox
	Public WithEvents Image2 As System.Windows.Forms.PictureBox
	Public WithEvents frTipo As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents txtTransporte As System.Windows.Forms.TextBox
	Public WithEvents _optTipoTrans_1 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents _optTipoTrans_2 As AxXtremeSuiteControls.AxRadioButton
	Public WithEvents FrameTransporte As AxXtremeSuiteControls.AxGroupBox
	Public WithEvents lbl20 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents optTipo As AxRadioButtonArray
	Public WithEvents optTipoTrans As AxRadioButtonArray
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTransportista))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.txtTelefono = New System.Windows.Forms.TextBox
		Me.TabControl1 = New AxXtremeSuiteControls.AxTabControl
		Me.TabControlPage4 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdVdaInt = New AxFlexCell.AxGrid
		Me.TabControlPage3 = New AxXtremeSuiteControls.AxTabControlPage
		Me.GrdTTarif = New AxFlexCell.AxGrid
		Me.TabControlPage2 = New AxXtremeSuiteControls.AxTabControlPage
		Me.grdKM = New AxFlexCell.AxGrid
		Me.TabControlPage1 = New AxXtremeSuiteControls.AxTabControlPage
		Me.grdPT = New AxFlexCell.AxGrid
		Me.txtCodigo = New System.Windows.Forms.TextBox
		Me.txtNombre = New System.Windows.Forms.TextBox
		Me.txtFechaBaja = New AxDataControl.AxGmsData
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.frTipo = New AxXtremeSuiteControls.AxGroupBox
		Me.txtTipo = New System.Windows.Forms.TextBox
		Me._optTipo_1 = New AxXtremeSuiteControls.AxRadioButton
		Me._optTipo_2 = New AxXtremeSuiteControls.AxRadioButton
		Me.Image1 = New System.Windows.Forms.PictureBox
		Me.Image2 = New System.Windows.Forms.PictureBox
		Me.FrameTransporte = New AxXtremeSuiteControls.AxGroupBox
		Me.txtTransporte = New System.Windows.Forms.TextBox
		Me._optTipoTrans_1 = New AxXtremeSuiteControls.AxRadioButton
		Me._optTipoTrans_2 = New AxXtremeSuiteControls.AxRadioButton
		Me.lbl20 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.optTipo = New AxRadioButtonArray(components)
		Me.optTipoTrans = New AxRadioButtonArray(components)
		Me.TabControl1.SuspendLayout()
		Me.TabControlPage4.SuspendLayout()
		Me.TabControlPage3.SuspendLayout()
		Me.TabControlPage2.SuspendLayout()
		Me.TabControlPage1.SuspendLayout()
		Me.frTipo.SuspendLayout()
		Me.FrameTransporte.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.GrdVdaInt, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage4, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdTTarif, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.grdKM, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.grdPT, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optTipo_1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optTipo_2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.frTipo, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optTipoTrans_1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me._optTipoTrans_2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.FrameTransporte, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.optTipo, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.optTipoTrans, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Transportista"
		Me.ClientSize = New System.Drawing.Size(444, 350)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "T-TRANSPORTISTA"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmTransportista"
		Me.txtTelefono.AutoSize = False
		Me.txtTelefono.Size = New System.Drawing.Size(104, 19)
		Me.txtTelefono.Location = New System.Drawing.Point(88, 58)
		Me.txtTelefono.Maxlength = 10
		Me.txtTelefono.TabIndex = 4
		Me.txtTelefono.Tag = "6"
		Me.txtTelefono.AcceptsReturn = True
		Me.txtTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTelefono.BackColor = System.Drawing.SystemColors.Window
		Me.txtTelefono.CausesValidation = True
		Me.txtTelefono.Enabled = True
		Me.txtTelefono.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTelefono.HideSelection = True
		Me.txtTelefono.ReadOnly = False
		Me.txtTelefono.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTelefono.MultiLine = False
		Me.txtTelefono.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTelefono.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTelefono.TabStop = True
		Me.txtTelefono.Visible = True
		Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTelefono.Name = "txtTelefono"
		TabControl1.OcxState = CType(resources.GetObject("TabControl1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControl1.Size = New System.Drawing.Size(417, 149)
		Me.TabControl1.Location = New System.Drawing.Point(12, 160)
		Me.TabControl1.TabIndex = 10
		Me.TabControl1.Name = "TabControl1"
		TabControlPage4.OcxState = CType(resources.GetObject("TabControlPage4.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage4.Size = New System.Drawing.Size(413, 125)
		Me.TabControlPage4.Location = New System.Drawing.Point(2, 22)
		Me.TabControlPage4.TabIndex = 25
		Me.TabControlPage4.Name = "TabControlPage4"
		GrdVdaInt.OcxState = CType(resources.GetObject("GrdVdaInt.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdVdaInt.Size = New System.Drawing.Size(400, 113)
		Me.GrdVdaInt.Location = New System.Drawing.Point(4, 6)
		Me.GrdVdaInt.TabIndex = 26
		Me.GrdVdaInt.Name = "GrdVdaInt"
		TabControlPage3.OcxState = CType(resources.GetObject("TabControlPage3.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage3.Size = New System.Drawing.Size(413, 125)
		Me.TabControlPage3.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage3.TabIndex = 23
		Me.TabControlPage3.Visible = False
		Me.TabControlPage3.Name = "TabControlPage3"
		GrdTTarif.OcxState = CType(resources.GetObject("GrdTTarif.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdTTarif.Size = New System.Drawing.Size(400, 113)
		Me.GrdTTarif.Location = New System.Drawing.Point(4, 6)
		Me.GrdTTarif.TabIndex = 24
		Me.GrdTTarif.Name = "GrdTTarif"
		TabControlPage2.OcxState = CType(resources.GetObject("TabControlPage2.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage2.Size = New System.Drawing.Size(413, 125)
		Me.TabControlPage2.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage2.TabIndex = 12
		Me.TabControlPage2.Visible = False
		Me.TabControlPage2.Name = "TabControlPage2"
		grdKM.OcxState = CType(resources.GetObject("grdKM.OcxState"), System.Windows.Forms.AxHost.State)
		Me.grdKM.Size = New System.Drawing.Size(400, 113)
		Me.grdKM.Location = New System.Drawing.Point(4, 6)
		Me.grdKM.TabIndex = 14
		Me.grdKM.Name = "grdKM"
		TabControlPage1.OcxState = CType(resources.GetObject("TabControlPage1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControlPage1.Size = New System.Drawing.Size(413, 125)
		Me.TabControlPage1.Location = New System.Drawing.Point(-4664, 22)
		Me.TabControlPage1.TabIndex = 11
		Me.TabControlPage1.Visible = False
		Me.TabControlPage1.Name = "TabControlPage1"
		grdPT.OcxState = CType(resources.GetObject("grdPT.OcxState"), System.Windows.Forms.AxHost.State)
		Me.grdPT.Size = New System.Drawing.Size(400, 113)
		Me.grdPT.Location = New System.Drawing.Point(4, 6)
		Me.grdPT.TabIndex = 13
		Me.grdPT.Name = "grdPT"
		Me.txtCodigo.AutoSize = False
		Me.txtCodigo.Size = New System.Drawing.Size(21, 19)
		Me.txtCodigo.Location = New System.Drawing.Point(88, 10)
		Me.txtCodigo.Maxlength = 2
		Me.txtCodigo.TabIndex = 1
		Me.txtCodigo.Tag = "*1"
		Me.txtCodigo.AcceptsReturn = True
		Me.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodigo.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodigo.CausesValidation = True
		Me.txtCodigo.Enabled = True
		Me.txtCodigo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodigo.HideSelection = True
		Me.txtCodigo.ReadOnly = False
		Me.txtCodigo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodigo.MultiLine = False
		Me.txtCodigo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodigo.TabStop = True
		Me.txtCodigo.Visible = True
		Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodigo.Name = "txtCodigo"
		Me.txtNombre.AutoSize = False
		Me.txtNombre.Size = New System.Drawing.Size(343, 19)
		Me.txtNombre.Location = New System.Drawing.Point(88, 34)
		Me.txtNombre.Maxlength = 50
		Me.txtNombre.TabIndex = 3
		Me.txtNombre.Tag = "2"
		Me.txtNombre.AcceptsReturn = True
		Me.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNombre.BackColor = System.Drawing.SystemColors.Window
		Me.txtNombre.CausesValidation = True
		Me.txtNombre.Enabled = True
		Me.txtNombre.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNombre.HideSelection = True
		Me.txtNombre.ReadOnly = False
		Me.txtNombre.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNombre.MultiLine = False
		Me.txtNombre.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNombre.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNombre.TabStop = True
		Me.txtNombre.Visible = True
		Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNombre.Name = "txtNombre"
		txtFechaBaja.OcxState = CType(resources.GetObject("txtFechaBaja.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtFechaBaja.Size = New System.Drawing.Size(87, 19)
		Me.txtFechaBaja.Location = New System.Drawing.Point(88, 136)
		Me.txtFechaBaja.TabIndex = 6
		Me.txtFechaBaja.Name = "txtFechaBaja"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(257, 318)
		Me.cmdAceptar.TabIndex = 7
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(347, 318)
		Me.cmdGuardar.TabIndex = 8
		Me.cmdGuardar.Name = "cmdGuardar"
		frTipo.OcxState = CType(resources.GetObject("frTipo.OcxState"), System.Windows.Forms.AxHost.State)
		Me.frTipo.Size = New System.Drawing.Size(213, 39)
		Me.frTipo.Location = New System.Drawing.Point(8, 88)
		Me.frTipo.TabIndex = 15
		Me.frTipo.Name = "frTipo"
		Me.txtTipo.AutoSize = False
		Me.txtTipo.BackColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.txtTipo.Size = New System.Drawing.Size(11, 19)
		Me.txtTipo.Location = New System.Drawing.Point(4, 4)
		Me.txtTipo.Maxlength = 1
		Me.txtTipo.TabIndex = 16
		Me.txtTipo.Tag = "4"
		Me.txtTipo.AcceptsReturn = True
		Me.txtTipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipo.CausesValidation = True
		Me.txtTipo.Enabled = True
		Me.txtTipo.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipo.HideSelection = True
		Me.txtTipo.ReadOnly = False
		Me.txtTipo.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipo.MultiLine = False
		Me.txtTipo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipo.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipo.TabStop = True
		Me.txtTipo.Visible = True
		Me.txtTipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipo.Name = "txtTipo"
		_optTipo_1.OcxState = CType(resources.GetObject("_optTipo_1.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optTipo_1.Size = New System.Drawing.Size(81, 21)
		Me._optTipo_1.Location = New System.Drawing.Point(28, 12)
		Me._optTipo_1.TabIndex = 17
		Me._optTipo_1.Name = "_optTipo_1"
		_optTipo_2.OcxState = CType(resources.GetObject("_optTipo_2.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optTipo_2.Size = New System.Drawing.Size(97, 21)
		Me._optTipo_2.Location = New System.Drawing.Point(134, 12)
		Me._optTipo_2.TabIndex = 18
		Me._optTipo_2.Name = "_optTipo_2"
		Me.Image1.Size = New System.Drawing.Size(16, 16)
		Me.Image1.Location = New System.Drawing.Point(6, 14)
		Me.Image1.Image = CType(resources.GetObject("Image1.Image"), System.Drawing.Image)
		Me.Image1.Enabled = True
		Me.Image1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.Image1.Visible = True
		Me.Image1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Image1.Name = "Image1"
		Me.Image2.Size = New System.Drawing.Size(16, 16)
		Me.Image2.Location = New System.Drawing.Point(110, 14)
		Me.Image2.Image = CType(resources.GetObject("Image2.Image"), System.Drawing.Image)
		Me.Image2.Enabled = True
		Me.Image2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Image2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.Image2.Visible = True
		Me.Image2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Image2.Name = "Image2"
		FrameTransporte.OcxState = CType(resources.GetObject("FrameTransporte.OcxState"), System.Windows.Forms.AxHost.State)
		Me.FrameTransporte.Size = New System.Drawing.Size(203, 39)
		Me.FrameTransporte.Location = New System.Drawing.Point(226, 88)
		Me.FrameTransporte.TabIndex = 19
		Me.FrameTransporte.Name = "FrameTransporte"
		Me.txtTransporte.AutoSize = False
		Me.txtTransporte.BackColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.txtTransporte.Size = New System.Drawing.Size(11, 19)
		Me.txtTransporte.Location = New System.Drawing.Point(4, 4)
		Me.txtTransporte.Maxlength = 1
		Me.txtTransporte.TabIndex = 20
		Me.txtTransporte.Tag = "5"
		Me.txtTransporte.AcceptsReturn = True
		Me.txtTransporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTransporte.CausesValidation = True
		Me.txtTransporte.Enabled = True
		Me.txtTransporte.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTransporte.HideSelection = True
		Me.txtTransporte.ReadOnly = False
		Me.txtTransporte.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTransporte.MultiLine = False
		Me.txtTransporte.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTransporte.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTransporte.TabStop = True
		Me.txtTransporte.Visible = True
		Me.txtTransporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTransporte.Name = "txtTransporte"
		_optTipoTrans_1.OcxState = CType(resources.GetObject("_optTipoTrans_1.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optTipoTrans_1.Size = New System.Drawing.Size(87, 21)
		Me._optTipoTrans_1.Location = New System.Drawing.Point(10, 12)
		Me._optTipoTrans_1.TabIndex = 21
		Me._optTipoTrans_1.Name = "_optTipoTrans_1"
		_optTipoTrans_2.OcxState = CType(resources.GetObject("_optTipoTrans_2.OcxState"), System.Windows.Forms.AxHost.State)
		Me._optTipoTrans_2.Size = New System.Drawing.Size(93, 21)
		Me._optTipoTrans_2.Location = New System.Drawing.Point(108, 12)
		Me._optTipoTrans_2.TabIndex = 22
		Me._optTipoTrans_2.Name = "_optTipoTrans_2"
		Me.lbl20.Text = "Tel�fono"
		Me.lbl20.Size = New System.Drawing.Size(111, 15)
		Me.lbl20.Location = New System.Drawing.Point(10, 60)
		Me.lbl20.TabIndex = 27
		Me.lbl20.Tag = "6"
		Me.lbl20.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl20.BackColor = System.Drawing.SystemColors.Control
		Me.lbl20.Enabled = True
		Me.lbl20.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl20.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl20.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl20.UseMnemonic = True
		Me.lbl20.Visible = True
		Me.lbl20.AutoSize = False
		Me.lbl20.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl20.Name = "lbl20"
		Me.lbl1.Text = "C�digo"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Nombre"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Fecha baja"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 140)
		Me.lbl3.TabIndex = 5
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 317)
		Me.lblLock.TabIndex = 9
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.optTipo.SetIndex(_optTipo_1, CType(1, Short))
		Me.optTipo.SetIndex(_optTipo_2, CType(2, Short))
		Me.optTipoTrans.SetIndex(_optTipoTrans_1, CType(1, Short))
		Me.optTipoTrans.SetIndex(_optTipoTrans_2, CType(2, Short))
		CType(Me.optTipoTrans, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.optTipo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.FrameTransporte, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optTipoTrans_2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optTipoTrans_1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.frTipo, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optTipo_2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me._optTipo_1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtFechaBaja, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.grdPT, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.grdKM, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage3, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdTTarif, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControlPage4, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdVdaInt, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtTelefono)
		Me.Controls.Add(TabControl1)
		Me.Controls.Add(txtCodigo)
		Me.Controls.Add(txtNombre)
		Me.Controls.Add(txtFechaBaja)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(frTipo)
		Me.Controls.Add(FrameTransporte)
		Me.Controls.Add(lbl20)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lblLock)
		Me.TabControl1.Controls.Add(TabControlPage4)
		Me.TabControl1.Controls.Add(TabControlPage3)
		Me.TabControl1.Controls.Add(TabControlPage2)
		Me.TabControl1.Controls.Add(TabControlPage1)
		Me.TabControlPage4.Controls.Add(GrdVdaInt)
		Me.TabControlPage3.Controls.Add(GrdTTarif)
		Me.TabControlPage2.Controls.Add(grdKM)
		Me.TabControlPage1.Controls.Add(grdPT)
		Me.frTipo.Controls.Add(txtTipo)
		Me.frTipo.Controls.Add(_optTipo_1)
		Me.frTipo.Controls.Add(_optTipo_2)
		Me.frTipo.Controls.Add(Image1)
		Me.frTipo.Controls.Add(Image2)
		Me.FrameTransporte.Controls.Add(txtTransporte)
		Me.FrameTransporte.Controls.Add(_optTipoTrans_1)
		Me.FrameTransporte.Controls.Add(_optTipoTrans_2)
		Me.TabControl1.ResumeLayout(False)
		Me.TabControlPage4.ResumeLayout(False)
		Me.TabControlPage3.ResumeLayout(False)
		Me.TabControlPage2.ResumeLayout(False)
		Me.TabControlPage1.ResumeLayout(False)
		Me.frTipo.ResumeLayout(False)
		Me.FrameTransporte.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class