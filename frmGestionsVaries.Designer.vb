<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmGestionsVaries
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtTecnic As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtData As AxDataControl.AxGmsData
	Public WithEvents txtNumRegistre As System.Windows.Forms.TextBox
	Public WithEvents txtTemps As AxgmsTime.AxgmsTemps
	Public WithEvents txtTipusGestio As System.Windows.Forms.TextBox
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtObservacions As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmGestionsVaries))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtTecnic = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtData = New AxDataControl.AxGmsData
		Me.txtNumRegistre = New System.Windows.Forms.TextBox
		Me.txtTemps = New AxgmsTime.AxgmsTemps
		Me.txtTipusGestio = New System.Windows.Forms.TextBox
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtObservacions = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtTemps, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Gestions varies"
		Me.ClientSize = New System.Drawing.Size(508, 280)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-GESTIONS_VARIES"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmGestionsVaries"
		Me.txtTecnic.AutoSize = False
		Me.txtTecnic.Size = New System.Drawing.Size(105, 19)
		Me.txtTecnic.Location = New System.Drawing.Point(124, 10)
		Me.txtTecnic.Maxlength = 15
		Me.txtTecnic.TabIndex = 1
		Me.txtTecnic.Tag = "*1"
		Me.txtTecnic.AcceptsReturn = True
		Me.txtTecnic.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTecnic.BackColor = System.Drawing.SystemColors.Window
		Me.txtTecnic.CausesValidation = True
		Me.txtTecnic.Enabled = True
		Me.txtTecnic.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTecnic.HideSelection = True
		Me.txtTecnic.ReadOnly = False
		Me.txtTecnic.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTecnic.MultiLine = False
		Me.txtTecnic.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTecnic.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTecnic.TabStop = True
		Me.txtTecnic.Visible = True
		Me.txtTecnic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTecnic.Name = "txtTecnic"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(262, 19)
		Me.Text1.Location = New System.Drawing.Point(234, 10)
		Me.Text1.TabIndex = 2
		Me.Text1.Tag = "^1"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		txtData.OcxState = CType(resources.GetObject("txtData.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtData.Size = New System.Drawing.Size(87, 19)
		Me.txtData.Location = New System.Drawing.Point(124, 34)
		Me.txtData.TabIndex = 4
		Me.txtData.Name = "txtData"
		Me.txtNumRegistre.AutoSize = False
		Me.txtNumRegistre.Size = New System.Drawing.Size(21, 19)
		Me.txtNumRegistre.Location = New System.Drawing.Point(124, 58)
		Me.txtNumRegistre.Maxlength = 2
		Me.txtNumRegistre.TabIndex = 6
		Me.txtNumRegistre.Tag = "*3"
		Me.txtNumRegistre.AcceptsReturn = True
		Me.txtNumRegistre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtNumRegistre.BackColor = System.Drawing.SystemColors.Window
		Me.txtNumRegistre.CausesValidation = True
		Me.txtNumRegistre.Enabled = True
		Me.txtNumRegistre.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtNumRegistre.HideSelection = True
		Me.txtNumRegistre.ReadOnly = False
		Me.txtNumRegistre.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtNumRegistre.MultiLine = False
		Me.txtNumRegistre.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtNumRegistre.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtNumRegistre.TabStop = True
		Me.txtNumRegistre.Visible = True
		Me.txtNumRegistre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtNumRegistre.Name = "txtNumRegistre"
		txtTemps.OcxState = CType(resources.GetObject("txtTemps.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtTemps.Size = New System.Drawing.Size(52, 19)
		Me.txtTemps.Location = New System.Drawing.Point(124, 82)
		Me.txtTemps.TabIndex = 8
		Me.txtTemps.Name = "txtTemps"
		Me.txtTipusGestio.AutoSize = False
		Me.txtTipusGestio.Size = New System.Drawing.Size(31, 19)
		Me.txtTipusGestio.Location = New System.Drawing.Point(124, 106)
		Me.txtTipusGestio.Maxlength = 3
		Me.txtTipusGestio.TabIndex = 10
		Me.txtTipusGestio.Tag = "5"
		Me.txtTipusGestio.AcceptsReturn = True
		Me.txtTipusGestio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipusGestio.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipusGestio.CausesValidation = True
		Me.txtTipusGestio.Enabled = True
		Me.txtTipusGestio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipusGestio.HideSelection = True
		Me.txtTipusGestio.ReadOnly = False
		Me.txtTipusGestio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipusGestio.MultiLine = False
		Me.txtTipusGestio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipusGestio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipusGestio.TabStop = True
		Me.txtTipusGestio.Visible = True
		Me.txtTipusGestio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipusGestio.Name = "txtTipusGestio"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(338, 19)
		Me.Text5.Location = New System.Drawing.Point(159, 106)
		Me.Text5.TabIndex = 11
		Me.Text5.Tag = "^5"
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.Visible = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtObservacions.AutoSize = False
		Me.txtObservacions.Size = New System.Drawing.Size(371, 87)
		Me.txtObservacions.Location = New System.Drawing.Point(124, 130)
		Me.txtObservacions.Maxlength = 100
		Me.txtObservacions.MultiLine = True
		Me.txtObservacions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtObservacions.TabIndex = 13
		Me.txtObservacions.Tag = "6"
		Me.txtObservacions.AcceptsReturn = True
		Me.txtObservacions.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacions.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacions.CausesValidation = True
		Me.txtObservacions.Enabled = True
		Me.txtObservacions.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacions.HideSelection = True
		Me.txtObservacions.ReadOnly = False
		Me.txtObservacions.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacions.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacions.TabStop = True
		Me.txtObservacions.Visible = True
		Me.txtObservacions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacions.Name = "txtObservacions"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(325, 240)
		Me.cmdAceptar.TabIndex = 14
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(415, 240)
		Me.cmdGuardar.TabIndex = 15
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl1.Text = "Tecnic"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Data"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 3
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "N�m. registre"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 5
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Temps"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 86)
		Me.lbl4.TabIndex = 7
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Tipus gesti�"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 110)
		Me.lbl5.TabIndex = 9
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl6.Text = "Observacions"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(10, 134)
		Me.lbl6.TabIndex = 12
		Me.lbl6.Tag = "6"
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.Visible = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 239)
		Me.lblLock.TabIndex = 16
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 233
		Me.Line1.Y2 = 233
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 234
		Me.Line2.Y2 = 234
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtTemps, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtTecnic)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtData)
		Me.Controls.Add(txtNumRegistre)
		Me.Controls.Add(txtTemps)
		Me.Controls.Add(txtTipusGestio)
		Me.Controls.Add(Text5)
		Me.Controls.Add(txtObservacions)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lbl6)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class