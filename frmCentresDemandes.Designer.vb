<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmCentresDemandes
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtEmail As System.Windows.Forms.TextBox
	Public WithEvents txtCodi As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcioCentre As System.Windows.Forms.TextBox
	Public WithEvents txtResponsableCentre As System.Windows.Forms.TextBox
	Public WithEvents Text3 As System.Windows.Forms.TextBox
	Public WithEvents txtUsuariEntigest As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtDataBaixa As AxDataControl.AxGmsData
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents chkTothomLesPotGestionar As AxXtremeSuiteControls.AxCheckBox
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmCentresDemandes))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtEmail = New System.Windows.Forms.TextBox
		Me.txtCodi = New System.Windows.Forms.TextBox
		Me.txtDescripcioCentre = New System.Windows.Forms.TextBox
		Me.txtResponsableCentre = New System.Windows.Forms.TextBox
		Me.Text3 = New System.Windows.Forms.TextBox
		Me.txtUsuariEntigest = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtDataBaixa = New AxDataControl.AxGmsData
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.chkTothomLesPotGestionar = New AxXtremeSuiteControls.AxCheckBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtDataBaixa, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.chkTothomLesPotGestionar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Centres demandes"
		Me.ClientSize = New System.Drawing.Size(539, 231)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "DEM-CENTRES_DEMANDES"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmCentresDemandes"
		Me.txtEmail.AutoSize = False
		Me.txtEmail.Size = New System.Drawing.Size(405, 19)
		Me.txtEmail.Location = New System.Drawing.Point(124, 106)
		Me.txtEmail.Maxlength = 100
		Me.txtEmail.TabIndex = 10
		Me.txtEmail.Tag = "6"
		Me.txtEmail.AcceptsReturn = True
		Me.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEmail.BackColor = System.Drawing.SystemColors.Window
		Me.txtEmail.CausesValidation = True
		Me.txtEmail.Enabled = True
		Me.txtEmail.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtEmail.HideSelection = True
		Me.txtEmail.ReadOnly = False
		Me.txtEmail.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEmail.MultiLine = False
		Me.txtEmail.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEmail.TabStop = True
		Me.txtEmail.Visible = True
		Me.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEmail.Name = "txtEmail"
		Me.txtCodi.AutoSize = False
		Me.txtCodi.Size = New System.Drawing.Size(42, 19)
		Me.txtCodi.Location = New System.Drawing.Point(124, 10)
		Me.txtCodi.Maxlength = 4
		Me.txtCodi.TabIndex = 1
		Me.txtCodi.Tag = "*1"
		Me.txtCodi.AcceptsReturn = True
		Me.txtCodi.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodi.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodi.CausesValidation = True
		Me.txtCodi.Enabled = True
		Me.txtCodi.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodi.HideSelection = True
		Me.txtCodi.ReadOnly = False
		Me.txtCodi.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodi.MultiLine = False
		Me.txtCodi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodi.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodi.TabStop = True
		Me.txtCodi.Visible = True
		Me.txtCodi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodi.Name = "txtCodi"
		Me.txtDescripcioCentre.AutoSize = False
		Me.txtDescripcioCentre.Size = New System.Drawing.Size(405, 19)
		Me.txtDescripcioCentre.Location = New System.Drawing.Point(124, 34)
		Me.txtDescripcioCentre.Maxlength = 100
		Me.txtDescripcioCentre.TabIndex = 3
		Me.txtDescripcioCentre.Tag = "2"
		Me.txtDescripcioCentre.AcceptsReturn = True
		Me.txtDescripcioCentre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcioCentre.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcioCentre.CausesValidation = True
		Me.txtDescripcioCentre.Enabled = True
		Me.txtDescripcioCentre.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcioCentre.HideSelection = True
		Me.txtDescripcioCentre.ReadOnly = False
		Me.txtDescripcioCentre.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcioCentre.MultiLine = False
		Me.txtDescripcioCentre.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcioCentre.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcioCentre.TabStop = True
		Me.txtDescripcioCentre.Visible = True
		Me.txtDescripcioCentre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcioCentre.Name = "txtDescripcioCentre"
		Me.txtResponsableCentre.AutoSize = False
		Me.txtResponsableCentre.Size = New System.Drawing.Size(93, 19)
		Me.txtResponsableCentre.Location = New System.Drawing.Point(124, 58)
		Me.txtResponsableCentre.Maxlength = 9
		Me.txtResponsableCentre.TabIndex = 5
		Me.txtResponsableCentre.Tag = "3"
		Me.txtResponsableCentre.AcceptsReturn = True
		Me.txtResponsableCentre.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtResponsableCentre.BackColor = System.Drawing.SystemColors.Window
		Me.txtResponsableCentre.CausesValidation = True
		Me.txtResponsableCentre.Enabled = True
		Me.txtResponsableCentre.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtResponsableCentre.HideSelection = True
		Me.txtResponsableCentre.ReadOnly = False
		Me.txtResponsableCentre.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtResponsableCentre.MultiLine = False
		Me.txtResponsableCentre.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtResponsableCentre.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtResponsableCentre.TabStop = True
		Me.txtResponsableCentre.Visible = True
		Me.txtResponsableCentre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtResponsableCentre.Name = "txtResponsableCentre"
		Me.Text3.AutoSize = False
		Me.Text3.BackColor = System.Drawing.Color.White
		Me.Text3.Enabled = False
		Me.Text3.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text3.Size = New System.Drawing.Size(308, 19)
		Me.Text3.Location = New System.Drawing.Point(221, 58)
		Me.Text3.TabIndex = 6
		Me.Text3.Tag = "^3"
		Me.Text3.AcceptsReturn = True
		Me.Text3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text3.CausesValidation = True
		Me.Text3.HideSelection = True
		Me.Text3.ReadOnly = False
		Me.Text3.Maxlength = 0
		Me.Text3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text3.MultiLine = False
		Me.Text3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text3.TabStop = True
		Me.Text3.Visible = True
		Me.Text3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text3.Name = "Text3"
		Me.txtUsuariEntigest.AutoSize = False
		Me.txtUsuariEntigest.Size = New System.Drawing.Size(117, 19)
		Me.txtUsuariEntigest.Location = New System.Drawing.Point(124, 82)
		Me.txtUsuariEntigest.Maxlength = 15
		Me.txtUsuariEntigest.TabIndex = 8
		Me.txtUsuariEntigest.Tag = "4"
		Me.txtUsuariEntigest.AcceptsReturn = True
		Me.txtUsuariEntigest.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUsuariEntigest.BackColor = System.Drawing.SystemColors.Window
		Me.txtUsuariEntigest.CausesValidation = True
		Me.txtUsuariEntigest.Enabled = True
		Me.txtUsuariEntigest.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUsuariEntigest.HideSelection = True
		Me.txtUsuariEntigest.ReadOnly = False
		Me.txtUsuariEntigest.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUsuariEntigest.MultiLine = False
		Me.txtUsuariEntigest.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUsuariEntigest.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUsuariEntigest.TabStop = True
		Me.txtUsuariEntigest.Visible = True
		Me.txtUsuariEntigest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtUsuariEntigest.Name = "txtUsuariEntigest"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(284, 19)
		Me.Text4.Location = New System.Drawing.Point(244, 82)
		Me.Text4.TabIndex = 9
		Me.Text4.Tag = "^4"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		txtDataBaixa.OcxState = CType(resources.GetObject("txtDataBaixa.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtDataBaixa.Size = New System.Drawing.Size(87, 19)
		Me.txtDataBaixa.Location = New System.Drawing.Point(124, 154)
		Me.txtDataBaixa.TabIndex = 13
		Me.txtDataBaixa.Name = "txtDataBaixa"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(357, 194)
		Me.cmdAceptar.TabIndex = 14
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(447, 194)
		Me.cmdGuardar.TabIndex = 15
		Me.cmdGuardar.Name = "cmdGuardar"
		chkTothomLesPotGestionar.OcxState = CType(resources.GetObject("chkTothomLesPotGestionar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.chkTothomLesPotGestionar.Size = New System.Drawing.Size(133, 19)
		Me.chkTothomLesPotGestionar.Location = New System.Drawing.Point(8, 132)
		Me.chkTothomLesPotGestionar.TabIndex = 11
		Me.chkTothomLesPotGestionar.Name = "chkTothomLesPotGestionar"
		Me.Label1.Text = "Email"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 110)
		Me.Label1.TabIndex = 17
		Me.Label1.Tag = "6"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Codi"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Descripci� centre"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Responsable centre"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 62)
		Me.lbl3.TabIndex = 4
		Me.lbl3.Tag = "3"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Usuari entigest"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 86)
		Me.lbl4.TabIndex = 7
		Me.lbl4.Tag = "4"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Data baixa"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 158)
		Me.lbl5.TabIndex = 12
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 193)
		Me.lblLock.TabIndex = 16
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 187
		Me.Line1.Y2 = 187
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 188
		Me.Line2.Y2 = 188
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.chkTothomLesPotGestionar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDataBaixa, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtEmail)
		Me.Controls.Add(txtCodi)
		Me.Controls.Add(txtDescripcioCentre)
		Me.Controls.Add(txtResponsableCentre)
		Me.Controls.Add(Text3)
		Me.Controls.Add(txtUsuariEntigest)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtDataBaixa)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(chkTothomLesPotGestionar)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class