Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class FrmConsultaDadesEntitat
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		NetejaLabels()
	End Sub
	
	
	Private Sub cmdEntidad_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdEntidad.ClickEvent
		If txtEntidad.Text = "" Then Exit Sub
		CarregaEntitat(txtEntidad.Text)
		If txtEntidad.Text = "" Then Me.Unload()
	End Sub
	
	Private Sub cmdExit_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.ClickEvent
		Me.Unload()
	End Sub
	
	'UPGRADE_WARNING: Form evento FrmConsultaDadesEntitat.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub FrmConsultaDadesEntitat_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaAccions()
		CarregaObjectius()
		CarregaEvents()
		CarregaOfertes()
		CarregaInsercions()
	End Sub
	
	Private Sub FrmConsultaDadesEntitat_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		cmdEntidad.Picture = SetIcon("Organisation", 16)
		cmdExit.Picture = SetIcon("Exit", 16)
		NetejaLabels()
	End Sub
	
	Private Sub FrmConsultaDadesEntitat_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		
		ControlKey(Me, KeyAscii)
		
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	Private Sub GrdAccions_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdAccions.DoubleClick
		If GrdAccions.ActiveCell.Row = 0 Then Exit Sub
		'ObreFormulari frmAccions, "frmAccions", txtEntidad & S & txtSucursal & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 3)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmAccions, "frmAccions", txtEntidad.Text & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 3))
	End Sub
	
	Private Sub GrdAccions_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdAccions.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtEntidad.Text = "" Then Exit Sub
		If txtSucursal.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdAccions", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nueva Acci�n",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmAccions", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar Acci�n",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdAccions.Rows > 1 And PermisConsulta("frmAccions", e_Permisos.Consultes)
			End With
		End With
		XpExecutaMenu(Me, "GrdAccions", eventArgs.X, eventArgs.Y)
	End Sub
	
	
	
	
	
	
	Private Sub GrdObjectius_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdObjectius.DoubleClick
		If GrdObjectius.ActiveCell.Row = 0 Then Exit Sub
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, |, 4). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, |, 2). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmObjectius, "frmObjectius", Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 1) & S & Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 3) & S & Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 4))
	End Sub
	
	Private Sub GrdObjectius_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdObjectius.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtEntidad.Text = "" Then Exit Sub
		If txtSucursal.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdObjectius", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nuevo Objetivo",  ,  , XPIcon("PagePlus"),  ,  ,  ,  , "N")
				.MenuItems.Item("N").Enabled = PermisConsulta("frmObjectius", e_Permisos.altes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar Objectiu",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdObjectius.Rows > 1 And PermisConsulta("frmObjectius", e_Permisos.Consultes)
			End With
		End With
		XpExecutaMenu(Me, "GrdObjectius", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub GrdOfertes_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdOfertes.DoubleClick
		If GrdOfertes.MouseRow < 1 Then Exit Sub
		ObrirOferta()
	End Sub
	
	Private Sub GrdOfertes_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdOfertes.MouseUp
		If eventArgs.Button <> MouseButtons.Right Then Exit Sub
		If GrdOfertes.MouseRow = 0 Then Exit Sub
		
		Dim FlagSel As Integer
		FlagSel = GrdOfertes.ActiveCell.Row
		
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdOfertes", PopMenuStyle.tsSecondaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar",  , FlagSel > 0,  ,  ,  ,  ,  , "mnuEdit")
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Nova",  , (txtEntidad.Text <> "") And (txtSucursal.Text <> ""),  ,  ,  ,  ,  , "mnuNew")
				.MenuItems.Add(PopMenuItemStyle.tsMenuSeperator)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Eliminar",  , FlagSel > 0,  ,  ,  ,  ,  , "mnuDelete")
			End With
		End With
		
		XpExecutaMenu(Me, "GrdOfertes")
	End Sub
	
	Private Sub ObrirOferta(Optional ByRef OfertaNova As Boolean = False, Optional ByRef Esborrar As Boolean = False)
		If (OfertaNova And Esborrar = True) Then Exit Sub
		
		Dim NumOferta As String
		If OfertaNova = False Then
			NumOferta = GrdOfertes.Cell(GrdOfertes.ActiveCell.Row, 1).Text
		End If
		ObreFormulari(frmGestioDeOfertes, "frmGestioDeOfertes", NumOferta,  ,  ,  ,  , Esborrar)
		If OfertaNova = True Then NouCodi(frmGestioDeOfertes)
	End Sub
	
	Private Sub GrdParticipacio_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdParticipacio.DoubleClick
		If GrdParticipacio.ActiveCell.Row = 0 Then Exit Sub
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ObreFormulari(frmAccions, "frmAccions", txtEntidad.Text & S & txtSucursal.Text & S & Piece(GrdParticipacio.Cell(GrdParticipacio.ActiveCell.Row, 1).Text, "|", 3))
	End Sub
	
	Private Sub GrdParticipacio_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As MouseEventArgs) Handles GrdParticipacio.MouseUp
		Dim OpMenu As gmsPopUpCode.cPopMenuItem
		Dim strTR As String
		Dim strVencT As String
		If eventArgs.Button = MouseButtons.Left Then
			Exit Sub
		End If
		If txtEntidad.Text = "" Then Exit Sub
		If txtSucursal.Text = "" Then Exit Sub
		With PopMenuXp
			.ClearAll()
			.SetImageList((frmImatgesCodejoc.IconsPopUp16))
			With .Menus.Add("GrdParticipacio", PopMenuStyle.tsPrimaryMenu, True)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar Event",  ,  , XPIcon("Edit"),  ,  ,  ,  , "E")
				.MenuItems.Item("E").Enabled = GrdParticipacio.Rows > 1 And PermisConsulta("frmPlans", e_Permisos.Consultes)
				.MenuItems.Add(PopMenuItemStyle.tsMenuCaption, "Editar Acci�",  ,  , XPIcon("Edit"),  ,  ,  ,  , "EA")
				.MenuItems.Item("E").Enabled = GrdParticipacio.Rows > 1 And PermisConsulta("frmAccions", e_Permisos.Consultes)
			End With
		End With
		XpExecutaMenu(Me, "GrdParticipacio", eventArgs.X, eventArgs.Y)
	End Sub
	
	Private Sub txtEntidad_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidad.DoubleClick
		ConsultaTaula(Me, txtEntidad)
	End Sub
	
	Private Sub txtEntidad_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidad.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtEntidad_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtEntidad.LostFocus
		If txtEntidad.Text = "" Then Exit Sub
		CacheNetejaParametres()
		MCache.P1 = txtEntidad.Text
		
		txtSucursal.Text = CacheXecute("S VALUE=$$SUC^CRM($P(P1,S,1))")
		CarregaDadesComercials()
		CarregaAccions()
		CarregaObjectius()
		CarregaEvents()
		CarregaDadesPst()
		CarregaOfertes()
		CarregaInsercions()
	End Sub
	
	Private Sub txtEntidad_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtEntidad.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSucursal_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.DoubleClick
		ConsultaTaula(Me, txtSucursal,  , txtEntidad.Text & S & "S")
	End Sub
	
	Private Sub txtSucursal_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.GotFocus
		SelTxT(Me)
	End Sub
	
	Private Sub txtSucursal_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSucursal.LostFocus
		CarregaDadesComercials()
		CarregaAccions()
		CarregaObjectius()
		CarregaEvents()
		CarregaOfertes()
		CarregaInsercions()
	End Sub
	
	Private Sub txtSucursal_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSucursal.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtEntidad.Text & S & "S")
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub CarregaDadesComercials()
		Dim Reg As String
		'    CacheNetejaParametres
		MCache.P1 = txtEntidad.Text
		MCache.P2 = txtSucursal.Text
		
		If MCache.P1 = "" Then Exit Sub
		If MCache.P2 = "" Then Exit Sub
		
		Reg = CacheXecute("S VALUE=$G(^CRMINFOENT(EMP,P1,P2))")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCIncorpora.Text = IIf(Piece(Reg, S, 1) <> "" And Len(Piece(Reg, S, 1)) <> 1, Mid(Piece(Reg, S, 1), 7, 2) & "/" & Mid(Piece(Reg, S, 1), 5, 2) & "/" & Mid(Piece(Reg, S, 1), 3, 2), Piece(Reg, S, 1))
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPIncorpora.Text = IIf(Piece(Reg, S, 2) <> "", Mid(Piece(Reg, S, 2), 7, 2) & "/" & Mid(Piece(Reg, S, 2), 5, 2) & "/" & Mid(Piece(Reg, S, 2), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIIncorpora.Text = IIf(Piece(Reg, S, 3) <> "", Mid(Piece(Reg, S, 3), 7, 2) & "/" & Mid(Piece(Reg, S, 3), 5, 2) & "/" & Mid(Piece(Reg, S, 3), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCNeteja.Text = IIf(Piece(Reg, S, 4) <> "", Mid(Piece(Reg, S, 4), 7, 2) & "/" & Mid(Piece(Reg, S, 4), 5, 2) & "/" & Mid(Piece(Reg, S, 4), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPNeteja.Text = IIf(Piece(Reg, S, 5) <> "", Mid(Piece(Reg, S, 5), 7, 2) & "/" & Mid(Piece(Reg, S, 5), 5, 2) & "/" & Mid(Piece(Reg, S, 5), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblINeteja.Text = IIf(Piece(Reg, S, 6) <> "", Mid(Piece(Reg, S, 6), 7, 2) & "/" & Mid(Piece(Reg, S, 6), 5, 2) & "/" & Mid(Piece(Reg, S, 6), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCManip.Text = IIf(Piece(Reg, S, 7) <> "", Mid(Piece(Reg, S, 7), 7, 2) & "/" & Mid(Piece(Reg, S, 7), 5, 2) & "/" & Mid(Piece(Reg, S, 7), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPManip.Text = IIf(Piece(Reg, S, 8) <> "", Mid(Piece(Reg, S, 8), 7, 2) & "/" & Mid(Piece(Reg, S, 8), 5, 2) & "/" & Mid(Piece(Reg, S, 8), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIManip.Text = IIf(Piece(Reg, S, 9) <> "", Mid(Piece(Reg, S, 9), 7, 2) & "/" & Mid(Piece(Reg, S, 9), 5, 2) & "/" & Mid(Piece(Reg, S, 9), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCImpremta.Text = IIf(Piece(Reg, S, 10) <> "", Mid(Piece(Reg, S, 10), 7, 2) & "/" & Mid(Piece(Reg, S, 10), 5, 2) & "/" & Mid(Piece(Reg, S, 10), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPImpremta.Text = IIf(Piece(Reg, S, 11) <> "", Mid(Piece(Reg, S, 11), 7, 2) & "/" & Mid(Piece(Reg, S, 11), 5, 2) & "/" & Mid(Piece(Reg, S, 11), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIImpremta.Text = IIf(Piece(Reg, S, 12) <> "", Mid(Piece(Reg, S, 12), 7, 2) & "/" & Mid(Piece(Reg, S, 12), 5, 2) & "/" & Mid(Piece(Reg, S, 12), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCJardineria.Text = IIf(Piece(Reg, S, 13) <> "", Mid(Piece(Reg, S, 13), 7, 2) & "/" & Mid(Piece(Reg, S, 13), 5, 2) & "/" & Mid(Piece(Reg, S, 13), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPJardineria.Text = IIf(Piece(Reg, S, 14) <> "", Mid(Piece(Reg, S, 14), 7, 2) & "/" & Mid(Piece(Reg, S, 14), 5, 2) & "/" & Mid(Piece(Reg, S, 14), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIJardineria.Text = IIf(Piece(Reg, S, 15) <> "", Mid(Piece(Reg, S, 15), 7, 2) & "/" & Mid(Piece(Reg, S, 15), 5, 2) & "/" & Mid(Piece(Reg, S, 15), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCManteniment.Text = IIf(Piece(Reg, S, 16) <> "", Mid(Piece(Reg, S, 16), 7, 2) & "/" & Mid(Piece(Reg, S, 16), 5, 2) & "/" & Mid(Piece(Reg, S, 16), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPManteniment.Text = IIf(Piece(Reg, S, 17) <> "", Mid(Piece(Reg, S, 17), 7, 2) & "/" & Mid(Piece(Reg, S, 17), 5, 2) & "/" & Mid(Piece(Reg, S, 17), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIManteniment.Text = IIf(Piece(Reg, S, 18) <> "", Mid(Piece(Reg, S, 18), 7, 2) & "/" & Mid(Piece(Reg, S, 18), 5, 2) & "/" & Mid(Piece(Reg, S, 18), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCGarden.Text = IIf(Piece(Reg, S, 19) <> "", Mid(Piece(Reg, S, 19), 7, 2) & "/" & Mid(Piece(Reg, S, 19), 5, 2) & "/" & Mid(Piece(Reg, S, 19), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPGarden.Text = IIf(Piece(Reg, S, 20) <> "", Mid(Piece(Reg, S, 20), 7, 2) & "/" & Mid(Piece(Reg, S, 20), 5, 2) & "/" & Mid(Piece(Reg, S, 20), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIGarden.Text = IIf(Piece(Reg, S, 21) <> "", Mid(Piece(Reg, S, 21), 7, 2) & "/" & Mid(Piece(Reg, S, 21), 5, 2) & "/" & Mid(Piece(Reg, S, 21), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCCanonge.Text = IIf(Piece(Reg, S, 22) <> "", Mid(Piece(Reg, S, 22), 7, 2) & "/" & Mid(Piece(Reg, S, 22), 5, 2) & "/" & Mid(Piece(Reg, S, 22), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPCanonge.Text = IIf(Piece(Reg, S, 23) <> "", Mid(Piece(Reg, S, 23), 7, 2) & "/" & Mid(Piece(Reg, S, 23), 5, 2) & "/" & Mid(Piece(Reg, S, 23), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblICanonge.Text = IIf(Piece(Reg, S, 24) <> "", Mid(Piece(Reg, S, 24), 7, 2) & "/" & Mid(Piece(Reg, S, 24), 5, 2) & "/" & Mid(Piece(Reg, S, 24), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCVi.Text = IIf(Piece(Reg, S, 25) <> "", Mid(Piece(Reg, S, 25), 7, 2) & "/" & Mid(Piece(Reg, S, 25), 5, 2) & "/" & Mid(Piece(Reg, S, 25), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPVi.Text = IIf(Piece(Reg, S, 26) <> "", Mid(Piece(Reg, S, 26), 7, 2) & "/" & Mid(Piece(Reg, S, 26), 5, 2) & "/" & Mid(Piece(Reg, S, 26), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIVi.Text = IIf(Piece(Reg, S, 27) <> "", Mid(Piece(Reg, S, 27), 7, 2) & "/" & Mid(Piece(Reg, S, 27), 5, 2) & "/" & Mid(Piece(Reg, S, 27), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCDonantQ.Text = IIf(Piece(Reg, S, 28) <> "" And Len(Piece(Reg, S, 28)) <> 1, Mid(Piece(Reg, S, 28), 7, 2) & "/" & Mid(Piece(Reg, S, 28), 5, 2) & "/" & Mid(Piece(Reg, S, 28), 3, 2), Piece(Reg, S, 28))
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblpDonantQ.Text = IIf(Piece(Reg, S, 29) <> "", Mid(Piece(Reg, S, 29), 7, 2) & "/" & Mid(Piece(Reg, S, 29), 5, 2) & "/" & Mid(Piece(Reg, S, 29), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIDonantQ.Text = IIf(Piece(Reg, S, 30) <> "", Mid(Piece(Reg, S, 30), 7, 2) & "/" & Mid(Piece(Reg, S, 30), 5, 2) & "/" & Mid(Piece(Reg, S, 30), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCDonacioP.Text = IIf(Piece(Reg, S, 31) <> "", Mid(Piece(Reg, S, 31), 7, 2) & "/" & Mid(Piece(Reg, S, 31), 5, 2) & "/" & Mid(Piece(Reg, S, 31), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPDonacioP.Text = IIf(Piece(Reg, S, 32) <> "", Mid(Piece(Reg, S, 32), 7, 2) & "/" & Mid(Piece(Reg, S, 32), 5, 2) & "/" & Mid(Piece(Reg, S, 32), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIDonacioP.Text = IIf(Piece(Reg, S, 33) <> "", Mid(Piece(Reg, S, 33), 7, 2) & "/" & Mid(Piece(Reg, S, 33), 5, 2) & "/" & Mid(Piece(Reg, S, 33), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCEsponsor.Text = IIf(Piece(Reg, S, 34) <> "", Mid(Piece(Reg, S, 34), 7, 2) & "/" & Mid(Piece(Reg, S, 34), 5, 2) & "/" & Mid(Piece(Reg, S, 34), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPEsponsor.Text = IIf(Piece(Reg, S, 35) <> "", Mid(Piece(Reg, S, 35), 7, 2) & "/" & Mid(Piece(Reg, S, 35), 5, 2) & "/" & Mid(Piece(Reg, S, 35), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIEsponsor.Text = IIf(Piece(Reg, S, 36) <> "", Mid(Piece(Reg, S, 36), 7, 2) & "/" & Mid(Piece(Reg, S, 36), 5, 2) & "/" & Mid(Piece(Reg, S, 36), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCVoluntariat.Text = IIf(Piece(Reg, S, 37) <> "" And Len(Piece(Reg, S, 37)) <> 1, Mid(Piece(Reg, S, 37), 7, 2) & "/" & Mid(Piece(Reg, S, 37), 5, 2) & "/" & Mid(Piece(Reg, S, 37), 3, 2), Piece(Reg, S, 37))
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPVoluntariat.Text = IIf(Piece(Reg, S, 38) <> "", Mid(Piece(Reg, S, 38), 7, 2) & "/" & Mid(Piece(Reg, S, 38), 5, 2) & "/" & Mid(Piece(Reg, S, 38), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblIVoluntariat.Text = IIf(Piece(Reg, S, 39) <> "", Mid(Piece(Reg, S, 39), 7, 2) & "/" & Mid(Piece(Reg, S, 39), 5, 2) & "/" & Mid(Piece(Reg, S, 39), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCConveni.Text = IIf(Piece(Reg, S, 40) <> "" And Len(Piece(Reg, S, 40)) <> 1, Mid(Piece(Reg, S, 40), 7, 2) & "/" & Mid(Piece(Reg, S, 40), 5, 2) & "/" & Mid(Piece(Reg, S, 40), 3, 2), Piece(Reg, S, 40))
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCProveidor.Text = IIf(Piece(Reg, S, 41) <> "", Mid(Piece(Reg, S, 41), 7, 2) & "/" & Mid(Piece(Reg, S, 41), 5, 2) & "/" & Mid(Piece(Reg, S, 41), 3, 2), "")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCLots.Text = IIf(Piece(Reg, S, 42) <> "", Mid(Piece(Reg, S, 42), 7, 2) & "/" & Mid(Piece(Reg, S, 42), 5, 2) & "/" & Mid(Piece(Reg, S, 42), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblPLots.Text = IIf(Piece(Reg, S, 43) <> "", Mid(Piece(Reg, S, 43), 7, 2) & "/" & Mid(Piece(Reg, S, 43), 5, 2) & "/" & Mid(Piece(Reg, S, 43), 3, 2), "")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblILots.Text = IIf(Piece(Reg, S, 44) <> "", Mid(Piece(Reg, S, 44), 7, 2) & "/" & Mid(Piece(Reg, S, 44), 5, 2) & "/" & Mid(Piece(Reg, S, 44), 3, 2), "")
		
		
	End Sub
	
	Private Sub NetejaLabels()
		lblCIncorpora.Text = ""
		lblPIncorpora.Text = ""
		lblIIncorpora.Text = ""
		
		lblCNeteja.Text = ""
		lblPNeteja.Text = ""
		lblINeteja.Text = ""
		
		lblCManip.Text = ""
		lblPManip.Text = ""
		lblIManip.Text = ""
		
		lblCImpremta.Text = ""
		lblPImpremta.Text = ""
		lblIImpremta.Text = ""
		
		lblCJardineria.Text = ""
		lblPJardineria.Text = ""
		lblIJardineria.Text = ""
		
		lblCManteniment.Text = ""
		lblPManteniment.Text = ""
		lblIManteniment.Text = ""
		
		lblCGarden.Text = ""
		lblPGarden.Text = ""
		lblIGarden.Text = ""
		
		lblCCanonge.Text = ""
		lblPCanonge.Text = ""
		lblICanonge.Text = ""
		
		lblCVi.Text = ""
		lblPVi.Text = ""
		lblIVi.Text = ""
		
		lblCDonantQ.Text = ""
		lblpDonantQ.Text = ""
		lblIDonantQ.Text = ""
		
		lblCDonacioP.Text = ""
		lblPDonacioP.Text = ""
		lblIDonacioP.Text = ""
		
		lblCEsponsor.Text = ""
		lblPEsponsor.Text = ""
		lblIEsponsor.Text = ""
		
		lblCVoluntariat.Text = ""
		lblPVoluntariat.Text = ""
		lblIVoluntariat.Text = ""
		
		lblCProveidor.Text = ""
		
		lblCConveni.Text = ""
		
		lblCPstPNeteja.Text = ""
		lblCPstANeteja.Text = ""
		
		lblCPstPManteniment.Text = ""
		lblCPstAManteniment.Text = ""
		
		lblCPstPImpremta.Text = ""
		lblCPstAImpremta.Text = ""
		
		lblCPstPManip.Text = ""
		lblCPstAManip.Text = ""
		
		lblCPstPJardineria.Text = ""
		lblCPstAJardineria.Text = ""
		
		lblCPstPGarden.Text = ""
		
		lblCLots.Text = ""
		lblPLots.Text = ""
		lblILots.Text = ""
		lblCPstPLots.Text = ""
		lblCPstALots.Text = ""
		
	End Sub
	
	Private Sub CarregaAccions()
		CarregaFGrid(GrdAccions, "CGACCIONSC^CRM(0)", txtEntidad.Text & S & txtSucursal.Text)
	End Sub
	
	Private Sub CarregaObjectius()
		'    CarregaFGrid GrdObjectius, "CGACCIONSC^CRM(1)", txtEntidad & S & txtSucursal
		CarregaFGrid(GrdObjectius, "CGOBECTIUSNOU^CRM", txtEntidad.Text & S & txtSucursal.Text)
	End Sub
	
	
	Private Sub CarregaEvents()
		CarregaFGrid(GrdParticipacio, "CGACCICAMPC^CRM", txtEntidad.Text & S & txtSucursal.Text)
		
	End Sub
	
	Private Sub CarregaOfertes()
		CarregaFGrid(GrdOfertes, "CGOFERTCRM^CRM", txtEntidad.Text & S & txtSucursal.Text,  , True)
	End Sub
	
	Private Sub CarregaInsercions()
		CarregaFGrid(GrdInsercions, "CGINSERCRM^CRM", txtEntidad.Text & S & txtSucursal.Text)
	End Sub
	
	
	
	Private Sub CarregaDadesPst()
		Dim Reg As String
		
		
		'    CacheNetejaParametres
		MCache.P1 = txtEntidad.Text
		MCache.P2 = txtSucursal.Text
		
		If MCache.P1 = "" Then Exit Sub
		If MCache.P2 = "" Then Exit Sub
		
		CacheXecute("D CREAWORKPSTA^CRM")
		CacheXecute("D CREAWORKPSTP^CRM")
		
		Reg = CacheXecute("S VALUE=$$PST^CRM(P1)")
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCPstPNeteja.Text = Piece(Reg, S, 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCPstANeteja.Text = Piece(Reg, S, 2)
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCPstPManteniment.Text = Piece(Reg, S, 3)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCPstAManteniment.Text = Piece(Reg, S, 4)
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCPstPImpremta.Text = Piece(Reg, S, 5)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCPstAImpremta.Text = Piece(Reg, S, 6)
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCPstPManip.Text = Piece(Reg, S, 7)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCPstAManip.Text = Piece(Reg, S, 8)
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCPstPJardineria.Text = Piece(Reg, S, 9)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lblCPstAJardineria.Text = Piece(Reg, S, 10)
		
		lblCPstPGarden.Text = CacheXecute("S VALUE=$$PSTG^CRM(P1)")
		
		
		lblCPstPLots.Text = CacheXecute("S VALUE=$$PSTPNEX^CRM(P1)")
		lblCPstALots.Text = CacheXecute("S VALUE=$$PSTANEX^CRM(P1)")
		
	End Sub
	
	
	
	Public Sub ResMenu(ByRef NomMenu As String, ByRef KeyMenu As String)
		Select Case NomMenu
			Case "GrdOfertes"
				Select Case KeyMenu
					Case "mnuNew"
						ObrirOferta(True)
					Case "mnuEdit"
						ObrirOferta()
					Case "mnuDelete"
						ObrirOferta( , True)
				End Select
			Case "GrdAccions"
				Select Case KeyMenu
					Case "N"
						ObreFormulari(frmAccions, "frmAccions", txtEntidad.Text & S & txtSucursal.Text)
					Case "E"
						If GrdAccions.ActiveCell.Row = 0 Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmAccions, "frmAccions", txtEntidad.Text & S & txtSucursal.Text & S & Piece(GrdAccions.Cell(GrdAccions.ActiveCell.Row, 1).Text, "|", 3))
				End Select
			Case "GrdObjectius"
				Select Case KeyMenu
					Case "N"
						ObreFormulari(frmObjectius, "frmObjectius", txtEntidad.Text & S & txtSucursal.Text)
					Case "E"
						If GrdObjectius.ActiveCell.Row = 0 Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, |, 4). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, |, 3). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, |, 2). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmObjectius, "frmObjectius", Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 1) & S & Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 2) & S & Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 3) & S & Piece(GrdObjectius.Cell(GrdObjectius.ActiveCell.Row, 1).Text, "|", 4))
				End Select
			Case "GrdParticipacio"
				Select Case KeyMenu
					Case "E"
						If GrdParticipacio.ActiveCell.Row = 0 Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmCampanyes, "frmCampanyes", Piece(GrdParticipacio.Cell(GrdParticipacio.ActiveCell.Row, 1).Text, "|", 3))
					Case "EA"
						If GrdParticipacio.ActiveCell.Row = 0 Then Exit Sub
						'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						ObreFormulari(frmAccions, "frmAccions", txtEntidad.Text & S & txtSucursal.Text & S & Piece(GrdParticipacio.Cell(GrdParticipacio.ActiveCell.Row, 1).Text, "|", 3))
				End Select
		End Select
	End Sub
End Class
