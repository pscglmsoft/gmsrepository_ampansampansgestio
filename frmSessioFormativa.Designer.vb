<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSessioFormativa
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents Text5 As System.Windows.Forms.TextBox
	Public WithEvents txtObservacions As System.Windows.Forms.TextBox
	Public WithEvents Text7 As System.Windows.Forms.TextBox
	Public WithEvents txtTipusSubaccio As System.Windows.Forms.TextBox
	Public WithEvents Text6 As System.Windows.Forms.TextBox
	Public WithEvents txtTipoAccion As System.Windows.Forms.TextBox
	Public WithEvents txtTecnic As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtServei As System.Windows.Forms.TextBox
	Public WithEvents txtData As AxDataControl.AxGmsData
	Public WithEvents txtTemps As AxgmsTime.AxgmsTemps
	Public WithEvents GrdTecnics As AxFlexCell.AxGrid
	Public WithEvents GrdParticipants As AxFlexCell.AxGrid
	Public WithEvents cmdParticipants As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl9 As System.Windows.Forms.Label
	Public WithEvents lbl8 As System.Windows.Forms.Label
	Public WithEvents lbl7 As System.Windows.Forms.Label
	Public WithEvents lbl6 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents TabControl1 As AxXtremeSuiteControls.AxTabControl
	Public WithEvents txtCodi As System.Windows.Forms.TextBox
	Public WithEvents txtDescripcio As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSessioFormativa))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.TabControl1 = New AxXtremeSuiteControls.AxTabControl
		Me.Text5 = New System.Windows.Forms.TextBox
		Me.txtObservacions = New System.Windows.Forms.TextBox
		Me.Text7 = New System.Windows.Forms.TextBox
		Me.txtTipusSubaccio = New System.Windows.Forms.TextBox
		Me.Text6 = New System.Windows.Forms.TextBox
		Me.txtTipoAccion = New System.Windows.Forms.TextBox
		Me.txtTecnic = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtServei = New System.Windows.Forms.TextBox
		Me.txtData = New AxDataControl.AxGmsData
		Me.txtTemps = New AxgmsTime.AxgmsTemps
		Me.GrdTecnics = New AxFlexCell.AxGrid
		Me.GrdParticipants = New AxFlexCell.AxGrid
		Me.cmdParticipants = New AxXtremeSuiteControls.AxPushButton
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl9 = New System.Windows.Forms.Label
		Me.lbl8 = New System.Windows.Forms.Label
		Me.lbl7 = New System.Windows.Forms.Label
		Me.lbl6 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.txtCodi = New System.Windows.Forms.TextBox
		Me.txtDescripcio = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.TabControl1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtTemps, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdTecnics, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GrdParticipants, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdParticipants, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Sessi� formativa"
		Me.ClientSize = New System.Drawing.Size(701, 569)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-SESSIO_FORMATIVA"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmSessioFormativa"
		TabControl1.OcxState = CType(resources.GetObject("TabControl1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.TabControl1.Size = New System.Drawing.Size(677, 457)
		Me.TabControl1.Location = New System.Drawing.Point(10, 64)
		Me.TabControl1.TabIndex = 18
		Me.TabControl1.Name = "TabControl1"
		Me.Text5.AutoSize = False
		Me.Text5.BackColor = System.Drawing.Color.White
		Me.Text5.Enabled = False
		Me.Text5.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text5.Size = New System.Drawing.Size(259, 19)
		Me.Text5.Location = New System.Drawing.Point(-4418, 82)
		Me.Text5.TabIndex = 8
		Me.Text5.Tag = "^5"
		Me.Text5.Visible = False
		Me.Text5.AcceptsReturn = True
		Me.Text5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text5.CausesValidation = True
		Me.Text5.HideSelection = True
		Me.Text5.ReadOnly = False
		Me.Text5.Maxlength = 0
		Me.Text5.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text5.MultiLine = False
		Me.Text5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text5.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text5.TabStop = True
		Me.Text5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text5.Name = "Text5"
		Me.txtObservacions.AutoSize = False
		Me.txtObservacions.Size = New System.Drawing.Size(526, 93)
		Me.txtObservacions.Location = New System.Drawing.Point(-4532, 178)
		Me.txtObservacions.Maxlength = 150
		Me.txtObservacions.MultiLine = True
		Me.txtObservacions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtObservacions.TabIndex = 14
		Me.txtObservacions.Tag = "9"
		Me.txtObservacions.Visible = False
		Me.txtObservacions.AcceptsReturn = True
		Me.txtObservacions.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacions.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacions.CausesValidation = True
		Me.txtObservacions.Enabled = True
		Me.txtObservacions.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacions.HideSelection = True
		Me.txtObservacions.ReadOnly = False
		Me.txtObservacions.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacions.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacions.TabStop = True
		Me.txtObservacions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacions.Name = "txtObservacions"
		Me.Text7.AutoSize = False
		Me.Text7.BackColor = System.Drawing.Color.White
		Me.Text7.Enabled = False
		Me.Text7.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text7.Size = New System.Drawing.Size(348, 19)
		Me.Text7.Location = New System.Drawing.Point(-4508, 130)
		Me.Text7.TabIndex = 12
		Me.Text7.Tag = "^7"
		Me.Text7.Visible = False
		Me.Text7.AcceptsReturn = True
		Me.Text7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text7.CausesValidation = True
		Me.Text7.HideSelection = True
		Me.Text7.ReadOnly = False
		Me.Text7.Maxlength = 0
		Me.Text7.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text7.MultiLine = False
		Me.Text7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text7.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text7.TabStop = True
		Me.Text7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text7.Name = "Text7"
		Me.txtTipusSubaccio.AutoSize = False
		Me.txtTipusSubaccio.Size = New System.Drawing.Size(21, 19)
		Me.txtTipusSubaccio.Location = New System.Drawing.Point(-4532, 130)
		Me.txtTipusSubaccio.Maxlength = 2
		Me.txtTipusSubaccio.TabIndex = 11
		Me.txtTipusSubaccio.Tag = "7"
		Me.txtTipusSubaccio.Visible = False
		Me.txtTipusSubaccio.AcceptsReturn = True
		Me.txtTipusSubaccio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipusSubaccio.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipusSubaccio.CausesValidation = True
		Me.txtTipusSubaccio.Enabled = True
		Me.txtTipusSubaccio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipusSubaccio.HideSelection = True
		Me.txtTipusSubaccio.ReadOnly = False
		Me.txtTipusSubaccio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipusSubaccio.MultiLine = False
		Me.txtTipusSubaccio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipusSubaccio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipusSubaccio.TabStop = True
		Me.txtTipusSubaccio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipusSubaccio.Name = "txtTipusSubaccio"
		Me.Text6.AutoSize = False
		Me.Text6.BackColor = System.Drawing.Color.White
		Me.Text6.Enabled = False
		Me.Text6.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text6.Size = New System.Drawing.Size(348, 19)
		Me.Text6.Location = New System.Drawing.Point(-4508, 106)
		Me.Text6.TabIndex = 10
		Me.Text6.Tag = "^6"
		Me.Text6.Visible = False
		Me.Text6.AcceptsReturn = True
		Me.Text6.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text6.CausesValidation = True
		Me.Text6.HideSelection = True
		Me.Text6.ReadOnly = False
		Me.Text6.Maxlength = 0
		Me.Text6.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text6.MultiLine = False
		Me.Text6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text6.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text6.TabStop = True
		Me.Text6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text6.Name = "Text6"
		Me.txtTipoAccion.AutoSize = False
		Me.txtTipoAccion.Size = New System.Drawing.Size(21, 19)
		Me.txtTipoAccion.Location = New System.Drawing.Point(-4532, 106)
		Me.txtTipoAccion.Maxlength = 2
		Me.txtTipoAccion.TabIndex = 9
		Me.txtTipoAccion.Tag = "6"
		Me.txtTipoAccion.Visible = False
		Me.txtTipoAccion.AcceptsReturn = True
		Me.txtTipoAccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTipoAccion.BackColor = System.Drawing.SystemColors.Window
		Me.txtTipoAccion.CausesValidation = True
		Me.txtTipoAccion.Enabled = True
		Me.txtTipoAccion.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTipoAccion.HideSelection = True
		Me.txtTipoAccion.ReadOnly = False
		Me.txtTipoAccion.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTipoAccion.MultiLine = False
		Me.txtTipoAccion.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTipoAccion.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTipoAccion.TabStop = True
		Me.txtTipoAccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTipoAccion.Name = "txtTipoAccion"
		Me.txtTecnic.AutoSize = False
		Me.txtTecnic.Size = New System.Drawing.Size(109, 19)
		Me.txtTecnic.Location = New System.Drawing.Point(-4532, 82)
		Me.txtTecnic.Maxlength = 15
		Me.txtTecnic.TabIndex = 7
		Me.txtTecnic.Tag = "5"
		Me.txtTecnic.Visible = False
		Me.txtTecnic.AcceptsReturn = True
		Me.txtTecnic.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTecnic.BackColor = System.Drawing.SystemColors.Window
		Me.txtTecnic.CausesValidation = True
		Me.txtTecnic.Enabled = True
		Me.txtTecnic.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTecnic.HideSelection = True
		Me.txtTecnic.ReadOnly = False
		Me.txtTecnic.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTecnic.MultiLine = False
		Me.txtTecnic.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTecnic.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTecnic.TabStop = True
		Me.txtTecnic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtTecnic.Name = "txtTecnic"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(348, 19)
		Me.Text4.Location = New System.Drawing.Point(-4508, 58)
		Me.Text4.TabIndex = 6
		Me.Text4.Tag = "^4"
		Me.Text4.Visible = False
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtServei.AutoSize = False
		Me.txtServei.Size = New System.Drawing.Size(21, 19)
		Me.txtServei.Location = New System.Drawing.Point(-4532, 58)
		Me.txtServei.Maxlength = 2
		Me.txtServei.TabIndex = 5
		Me.txtServei.Tag = "4"
		Me.txtServei.Visible = False
		Me.txtServei.AcceptsReturn = True
		Me.txtServei.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtServei.BackColor = System.Drawing.SystemColors.Window
		Me.txtServei.CausesValidation = True
		Me.txtServei.Enabled = True
		Me.txtServei.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtServei.HideSelection = True
		Me.txtServei.ReadOnly = False
		Me.txtServei.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtServei.MultiLine = False
		Me.txtServei.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtServei.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtServei.TabStop = True
		Me.txtServei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtServei.Name = "txtServei"
		txtData.OcxState = CType(resources.GetObject("txtData.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtData.Size = New System.Drawing.Size(87, 19)
		Me.txtData.Location = New System.Drawing.Point(-4532, 34)
		Me.txtData.TabIndex = 4
		Me.txtData.Visible = False
		Me.txtData.Name = "txtData"
		txtTemps.OcxState = CType(resources.GetObject("txtTemps.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtTemps.Size = New System.Drawing.Size(52, 19)
		Me.txtTemps.Location = New System.Drawing.Point(-4532, 154)
		Me.txtTemps.TabIndex = 13
		Me.txtTemps.Visible = False
		Me.txtTemps.Name = "txtTemps"
		GrdTecnics.OcxState = CType(resources.GetObject("GrdTecnics.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdTecnics.Size = New System.Drawing.Size(298, 399)
		Me.GrdTecnics.Location = New System.Drawing.Point(8, 44)
		Me.GrdTecnics.TabIndex = 26
		Me.GrdTecnics.Name = "GrdTecnics"
		GrdParticipants.OcxState = CType(resources.GetObject("GrdParticipants.OcxState"), System.Windows.Forms.AxHost.State)
		Me.GrdParticipants.Size = New System.Drawing.Size(298, 363)
		Me.GrdParticipants.Location = New System.Drawing.Point(358, 44)
		Me.GrdParticipants.TabIndex = 28
		Me.GrdParticipants.Name = "GrdParticipants"
		cmdParticipants.OcxState = CType(resources.GetObject("cmdParticipants.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdParticipants.Size = New System.Drawing.Size(83, 31)
		Me.cmdParticipants.Location = New System.Drawing.Point(358, 410)
		Me.cmdParticipants.TabIndex = 30
		Me.cmdParticipants.Name = "cmdParticipants"
		Me.Label2.Text = "Participants"
		Me.Label2.ForeColor = System.Drawing.Color.Blue
		Me.Label2.Size = New System.Drawing.Size(141, 13)
		Me.Label2.Location = New System.Drawing.Point(360, 28)
		Me.Label2.TabIndex = 29
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "T�cnics referents"
		Me.Label1.ForeColor = System.Drawing.Color.Blue
		Me.Label1.Size = New System.Drawing.Size(141, 11)
		Me.Label1.Location = New System.Drawing.Point(12, 28)
		Me.Label1.TabIndex = 27
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl9.Text = "Observacions"
		Me.lbl9.Size = New System.Drawing.Size(111, 15)
		Me.lbl9.Location = New System.Drawing.Point(-4646, 180)
		Me.lbl9.TabIndex = 25
		Me.lbl9.Tag = "9"
		Me.lbl9.Visible = False
		Me.lbl9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl9.BackColor = System.Drawing.SystemColors.Control
		Me.lbl9.Enabled = True
		Me.lbl9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl9.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl9.UseMnemonic = True
		Me.lbl9.AutoSize = False
		Me.lbl9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl9.Name = "lbl9"
		Me.lbl8.Text = "Temps"
		Me.lbl8.Size = New System.Drawing.Size(111, 15)
		Me.lbl8.Location = New System.Drawing.Point(-4646, 158)
		Me.lbl8.TabIndex = 24
		Me.lbl8.Tag = "8"
		Me.lbl8.Visible = False
		Me.lbl8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl8.BackColor = System.Drawing.SystemColors.Control
		Me.lbl8.Enabled = True
		Me.lbl8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl8.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl8.UseMnemonic = True
		Me.lbl8.AutoSize = False
		Me.lbl8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl8.Name = "lbl8"
		Me.lbl7.Text = "Tipus subacci�"
		Me.lbl7.Size = New System.Drawing.Size(111, 15)
		Me.lbl7.Location = New System.Drawing.Point(-4646, 134)
		Me.lbl7.TabIndex = 23
		Me.lbl7.Tag = "7"
		Me.lbl7.Visible = False
		Me.lbl7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl7.BackColor = System.Drawing.SystemColors.Control
		Me.lbl7.Enabled = True
		Me.lbl7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl7.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl7.UseMnemonic = True
		Me.lbl7.AutoSize = False
		Me.lbl7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl7.Name = "lbl7"
		Me.lbl6.Text = "Tipo acci�n"
		Me.lbl6.Size = New System.Drawing.Size(111, 15)
		Me.lbl6.Location = New System.Drawing.Point(-4646, 110)
		Me.lbl6.TabIndex = 22
		Me.lbl6.Tag = "6"
		Me.lbl6.Visible = False
		Me.lbl6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl6.BackColor = System.Drawing.SystemColors.Control
		Me.lbl6.Enabled = True
		Me.lbl6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl6.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl6.UseMnemonic = True
		Me.lbl6.AutoSize = False
		Me.lbl6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl6.Name = "lbl6"
		Me.lbl5.Text = "Tecnic"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(-4646, 86)
		Me.lbl5.TabIndex = 21
		Me.lbl5.Tag = "5"
		Me.lbl5.Visible = False
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lbl4.Text = "Servei"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(-4646, 62)
		Me.lbl4.TabIndex = 20
		Me.lbl4.Tag = "4"
		Me.lbl4.Visible = False
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl3.Text = "Data"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(-4646, 38)
		Me.lbl3.TabIndex = 19
		Me.lbl3.Tag = "3"
		Me.lbl3.Visible = False
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.txtCodi.AutoSize = False
		Me.txtCodi.Size = New System.Drawing.Size(31, 19)
		Me.txtCodi.Location = New System.Drawing.Point(124, 10)
		Me.txtCodi.Maxlength = 2
		Me.txtCodi.TabIndex = 1
		Me.txtCodi.Tag = "*1"
		Me.txtCodi.AcceptsReturn = True
		Me.txtCodi.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCodi.BackColor = System.Drawing.SystemColors.Window
		Me.txtCodi.CausesValidation = True
		Me.txtCodi.Enabled = True
		Me.txtCodi.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCodi.HideSelection = True
		Me.txtCodi.ReadOnly = False
		Me.txtCodi.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCodi.MultiLine = False
		Me.txtCodi.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCodi.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCodi.TabStop = True
		Me.txtCodi.Visible = True
		Me.txtCodi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtCodi.Name = "txtCodi"
		Me.txtDescripcio.AutoSize = False
		Me.txtDescripcio.Size = New System.Drawing.Size(559, 19)
		Me.txtDescripcio.Location = New System.Drawing.Point(124, 34)
		Me.txtDescripcio.Maxlength = 50
		Me.txtDescripcio.TabIndex = 3
		Me.txtDescripcio.Tag = "2"
		Me.txtDescripcio.AcceptsReturn = True
		Me.txtDescripcio.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDescripcio.BackColor = System.Drawing.SystemColors.Window
		Me.txtDescripcio.CausesValidation = True
		Me.txtDescripcio.Enabled = True
		Me.txtDescripcio.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDescripcio.HideSelection = True
		Me.txtDescripcio.ReadOnly = False
		Me.txtDescripcio.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDescripcio.MultiLine = False
		Me.txtDescripcio.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDescripcio.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDescripcio.TabStop = True
		Me.txtDescripcio.Visible = True
		Me.txtDescripcio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtDescripcio.Name = "txtDescripcio"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(513, 534)
		Me.cmdAceptar.TabIndex = 16
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(603, 534)
		Me.cmdGuardar.TabIndex = 15
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.lbl1.Text = "Codi"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 14)
		Me.lbl1.TabIndex = 0
		Me.lbl1.Tag = "1"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Descripci�"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 38)
		Me.lbl2.TabIndex = 2
		Me.lbl2.Tag = "2"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 533)
		Me.lblLock.TabIndex = 17
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 527
		Me.Line1.Y2 = 527
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 528
		Me.Line2.Y2 = 528
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdParticipants, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdParticipants, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GrdTecnics, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtTemps, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(TabControl1)
		Me.Controls.Add(txtCodi)
		Me.Controls.Add(txtDescripcio)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.TabControl1.Controls.Add(Text5)
		Me.TabControl1.Controls.Add(txtObservacions)
		Me.TabControl1.Controls.Add(Text7)
		Me.TabControl1.Controls.Add(txtTipusSubaccio)
		Me.TabControl1.Controls.Add(Text6)
		Me.TabControl1.Controls.Add(txtTipoAccion)
		Me.TabControl1.Controls.Add(txtTecnic)
		Me.TabControl1.Controls.Add(Text4)
		Me.TabControl1.Controls.Add(txtServei)
		Me.TabControl1.Controls.Add(txtData)
		Me.TabControl1.Controls.Add(txtTemps)
		Me.TabControl1.Controls.Add(GrdTecnics)
		Me.TabControl1.Controls.Add(GrdParticipants)
		Me.TabControl1.Controls.Add(cmdParticipants)
		Me.TabControl1.Controls.Add(Label2)
		Me.TabControl1.Controls.Add(Label1)
		Me.TabControl1.Controls.Add(lbl9)
		Me.TabControl1.Controls.Add(lbl8)
		Me.TabControl1.Controls.Add(lbl7)
		Me.TabControl1.Controls.Add(lbl6)
		Me.TabControl1.Controls.Add(lbl5)
		Me.TabControl1.Controls.Add(lbl4)
		Me.TabControl1.Controls.Add(lbl3)
		Me.TabControl1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class