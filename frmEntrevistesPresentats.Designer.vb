<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmEntrevistesPresentats
#Region "C�digo generado por el Dise�ador de Windows Forms "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'Llamada necesaria para el Dise�ador de Windows Forms.
		InitializeComponent()
		'�ste es un formulario MDI secundario.
		'Este c�digo simula la funcionalidad de VB6 
		' de cargar autom�ticamente
		' y mostrar el formulario primario de
		' un MDI secundario.
		Me.MDIParent = AmpansGestio.MDI
		AmpansGestio.MDI.Show
	End Sub
	'Form invalida a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents txtEntitat As System.Windows.Forms.TextBox
	Public WithEvents txtPersona As System.Windows.Forms.TextBox
	Public WithEvents Text1 As System.Windows.Forms.TextBox
	Public WithEvents txtData As AxDataControl.AxGmsData
	Public WithEvents cmbEstat As AxSSDataWidgets_B.AxSSDBCombo
	Public WithEvents txtOferta As System.Windows.Forms.TextBox
	Public WithEvents Text4 As System.Windows.Forms.TextBox
	Public WithEvents txtObservacions As System.Windows.Forms.TextBox
	Public WithEvents cmdAceptar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents cmdGuardar As AxXtremeSuiteControls.AxPushButton
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents lbl1 As System.Windows.Forms.Label
	Public WithEvents lbl2 As System.Windows.Forms.Label
	Public WithEvents lbl3 As System.Windows.Forms.Label
	Public WithEvents lbl4 As System.Windows.Forms.Label
	Public WithEvents lbl5 As System.Windows.Forms.Label
	Public WithEvents lblLock As System.Windows.Forms.Label
	Public WithEvents Line1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents Line2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar mediante el Dise�ador de Windows Forms.
	'No lo modifique con el editor de c�digo.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEntrevistesPresentats))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.txtEntitat = New System.Windows.Forms.TextBox
		Me.txtPersona = New System.Windows.Forms.TextBox
		Me.Text1 = New System.Windows.Forms.TextBox
		Me.txtData = New AxDataControl.AxGmsData
		Me.cmbEstat = New AxSSDataWidgets_B.AxSSDBCombo
		Me.txtOferta = New System.Windows.Forms.TextBox
		Me.Text4 = New System.Windows.Forms.TextBox
		Me.txtObservacions = New System.Windows.Forms.TextBox
		Me.cmdAceptar = New AxXtremeSuiteControls.AxPushButton
		Me.cmdGuardar = New AxXtremeSuiteControls.AxPushButton
		Me.Label1 = New System.Windows.Forms.Label
		Me.lbl1 = New System.Windows.Forms.Label
		Me.lbl2 = New System.Windows.Forms.Label
		Me.lbl3 = New System.Windows.Forms.Label
		Me.lbl4 = New System.Windows.Forms.Label
		Me.lbl5 = New System.Windows.Forms.Label
		Me.lblLock = New System.Windows.Forms.Label
		Me.Line1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.Line2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbEstat, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Entrevistes presentats"
		Me.ClientSize = New System.Drawing.Size(475, 250)
		Me.Location = New System.Drawing.Point(155, 131)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
		Me.Tag = "IN-ENTREVISTES_PRESENTATS"
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmEntrevistesPresentats"
		Me.txtEntitat.AutoSize = False
		Me.txtEntitat.BackColor = System.Drawing.Color.White
		Me.txtEntitat.Enabled = False
		Me.txtEntitat.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.txtEntitat.Size = New System.Drawing.Size(336, 19)
		Me.txtEntitat.Location = New System.Drawing.Point(124, 34)
		Me.txtEntitat.TabIndex = 1
		Me.txtEntitat.AcceptsReturn = True
		Me.txtEntitat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtEntitat.CausesValidation = True
		Me.txtEntitat.HideSelection = True
		Me.txtEntitat.ReadOnly = False
		Me.txtEntitat.Maxlength = 0
		Me.txtEntitat.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtEntitat.MultiLine = False
		Me.txtEntitat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtEntitat.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtEntitat.TabStop = True
		Me.txtEntitat.Visible = True
		Me.txtEntitat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtEntitat.Name = "txtEntitat"
		Me.txtPersona.AutoSize = False
		Me.txtPersona.Size = New System.Drawing.Size(42, 19)
		Me.txtPersona.Location = New System.Drawing.Point(124, 56)
		Me.txtPersona.Maxlength = 4
		Me.txtPersona.TabIndex = 2
		Me.txtPersona.Tag = "*2"
		Me.txtPersona.AcceptsReturn = True
		Me.txtPersona.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPersona.BackColor = System.Drawing.SystemColors.Window
		Me.txtPersona.CausesValidation = True
		Me.txtPersona.Enabled = True
		Me.txtPersona.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPersona.HideSelection = True
		Me.txtPersona.ReadOnly = False
		Me.txtPersona.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPersona.MultiLine = False
		Me.txtPersona.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPersona.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPersona.TabStop = True
		Me.txtPersona.Visible = True
		Me.txtPersona.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtPersona.Name = "txtPersona"
		Me.Text1.AutoSize = False
		Me.Text1.BackColor = System.Drawing.Color.White
		Me.Text1.Enabled = False
		Me.Text1.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text1.Size = New System.Drawing.Size(292, 19)
		Me.Text1.Location = New System.Drawing.Point(169, 56)
		Me.Text1.TabIndex = 8
		Me.Text1.Tag = "^2"
		Me.Text1.AcceptsReturn = True
		Me.Text1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text1.CausesValidation = True
		Me.Text1.HideSelection = True
		Me.Text1.ReadOnly = False
		Me.Text1.Maxlength = 0
		Me.Text1.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text1.MultiLine = False
		Me.Text1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text1.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text1.TabStop = True
		Me.Text1.Visible = True
		Me.Text1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text1.Name = "Text1"
		txtData.OcxState = CType(resources.GetObject("txtData.OcxState"), System.Windows.Forms.AxHost.State)
		Me.txtData.Size = New System.Drawing.Size(87, 19)
		Me.txtData.Location = New System.Drawing.Point(124, 80)
		Me.txtData.TabIndex = 3
		Me.txtData.Name = "txtData"
		cmbEstat.OcxState = CType(resources.GetObject("cmbEstat.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmbEstat.Size = New System.Drawing.Size(145, 19)
		Me.cmbEstat.Location = New System.Drawing.Point(124, 106)
		Me.cmbEstat.TabIndex = 4
		Me.cmbEstat.Name = "cmbEstat"
		Me.txtOferta.AutoSize = False
		Me.txtOferta.Size = New System.Drawing.Size(31, 19)
		Me.txtOferta.Location = New System.Drawing.Point(124, 10)
		Me.txtOferta.Maxlength = 3
		Me.txtOferta.TabIndex = 0
		Me.txtOferta.Tag = "*1"
		Me.txtOferta.AcceptsReturn = True
		Me.txtOferta.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtOferta.BackColor = System.Drawing.SystemColors.Window
		Me.txtOferta.CausesValidation = True
		Me.txtOferta.Enabled = True
		Me.txtOferta.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtOferta.HideSelection = True
		Me.txtOferta.ReadOnly = False
		Me.txtOferta.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtOferta.MultiLine = False
		Me.txtOferta.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtOferta.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtOferta.TabStop = True
		Me.txtOferta.Visible = True
		Me.txtOferta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtOferta.Name = "txtOferta"
		Me.Text4.AutoSize = False
		Me.Text4.BackColor = System.Drawing.Color.White
		Me.Text4.Enabled = False
		Me.Text4.ForeColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Text4.Size = New System.Drawing.Size(302, 19)
		Me.Text4.Location = New System.Drawing.Point(159, 10)
		Me.Text4.TabIndex = 12
		Me.Text4.Tag = "^1"
		Me.Text4.AcceptsReturn = True
		Me.Text4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.Text4.CausesValidation = True
		Me.Text4.HideSelection = True
		Me.Text4.ReadOnly = False
		Me.Text4.Maxlength = 0
		Me.Text4.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Text4.MultiLine = False
		Me.Text4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Text4.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.Text4.TabStop = True
		Me.Text4.Visible = True
		Me.Text4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Text4.Name = "Text4"
		Me.txtObservacions.AutoSize = False
		Me.txtObservacions.Size = New System.Drawing.Size(336, 67)
		Me.txtObservacions.Location = New System.Drawing.Point(124, 130)
		Me.txtObservacions.Maxlength = 150
		Me.txtObservacions.MultiLine = True
		Me.txtObservacions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtObservacions.TabIndex = 5
		Me.txtObservacions.Tag = "5"
		Me.txtObservacions.AcceptsReturn = True
		Me.txtObservacions.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtObservacions.BackColor = System.Drawing.SystemColors.Window
		Me.txtObservacions.CausesValidation = True
		Me.txtObservacions.Enabled = True
		Me.txtObservacions.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtObservacions.HideSelection = True
		Me.txtObservacions.ReadOnly = False
		Me.txtObservacions.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtObservacions.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtObservacions.TabStop = True
		Me.txtObservacions.Visible = True
		Me.txtObservacions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.txtObservacions.Name = "txtObservacions"
		cmdAceptar.OcxState = CType(resources.GetObject("cmdAceptar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdAceptar.Size = New System.Drawing.Size(83, 27)
		Me.cmdAceptar.Location = New System.Drawing.Point(293, 214)
		Me.cmdAceptar.TabIndex = 6
		Me.cmdAceptar.Name = "cmdAceptar"
		cmdGuardar.OcxState = CType(resources.GetObject("cmdGuardar.OcxState"), System.Windows.Forms.AxHost.State)
		Me.cmdGuardar.Size = New System.Drawing.Size(83, 27)
		Me.cmdGuardar.Location = New System.Drawing.Point(383, 214)
		Me.cmdGuardar.TabIndex = 14
		Me.cmdGuardar.Name = "cmdGuardar"
		Me.Label1.Text = "Entitat"
		Me.Label1.Size = New System.Drawing.Size(111, 15)
		Me.Label1.Location = New System.Drawing.Point(10, 38)
		Me.Label1.TabIndex = 16
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.lbl1.Text = "Persona"
		Me.lbl1.Size = New System.Drawing.Size(111, 15)
		Me.lbl1.Location = New System.Drawing.Point(10, 60)
		Me.lbl1.TabIndex = 7
		Me.lbl1.Tag = "2"
		Me.lbl1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl1.BackColor = System.Drawing.SystemColors.Control
		Me.lbl1.Enabled = True
		Me.lbl1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl1.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl1.UseMnemonic = True
		Me.lbl1.Visible = True
		Me.lbl1.AutoSize = False
		Me.lbl1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl1.Name = "lbl1"
		Me.lbl2.Text = "Data"
		Me.lbl2.Size = New System.Drawing.Size(111, 15)
		Me.lbl2.Location = New System.Drawing.Point(10, 84)
		Me.lbl2.TabIndex = 9
		Me.lbl2.Tag = "3"
		Me.lbl2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl2.BackColor = System.Drawing.SystemColors.Control
		Me.lbl2.Enabled = True
		Me.lbl2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl2.UseMnemonic = True
		Me.lbl2.Visible = True
		Me.lbl2.AutoSize = False
		Me.lbl2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl2.Name = "lbl2"
		Me.lbl3.Text = "Estat"
		Me.lbl3.Size = New System.Drawing.Size(111, 15)
		Me.lbl3.Location = New System.Drawing.Point(10, 110)
		Me.lbl3.TabIndex = 10
		Me.lbl3.Tag = "4"
		Me.lbl3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl3.BackColor = System.Drawing.SystemColors.Control
		Me.lbl3.Enabled = True
		Me.lbl3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl3.UseMnemonic = True
		Me.lbl3.Visible = True
		Me.lbl3.AutoSize = False
		Me.lbl3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl3.Name = "lbl3"
		Me.lbl4.Text = "Oferta"
		Me.lbl4.Size = New System.Drawing.Size(111, 15)
		Me.lbl4.Location = New System.Drawing.Point(10, 14)
		Me.lbl4.TabIndex = 11
		Me.lbl4.Tag = "1"
		Me.lbl4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl4.BackColor = System.Drawing.SystemColors.Control
		Me.lbl4.Enabled = True
		Me.lbl4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl4.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl4.UseMnemonic = True
		Me.lbl4.Visible = True
		Me.lbl4.AutoSize = False
		Me.lbl4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl4.Name = "lbl4"
		Me.lbl5.Text = "Observacions"
		Me.lbl5.Size = New System.Drawing.Size(111, 15)
		Me.lbl5.Location = New System.Drawing.Point(10, 134)
		Me.lbl5.TabIndex = 13
		Me.lbl5.Tag = "5"
		Me.lbl5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lbl5.BackColor = System.Drawing.SystemColors.Control
		Me.lbl5.Enabled = True
		Me.lbl5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lbl5.Cursor = System.Windows.Forms.Cursors.Default
		Me.lbl5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lbl5.UseMnemonic = True
		Me.lbl5.Visible = True
		Me.lbl5.AutoSize = False
		Me.lbl5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lbl5.Name = "lbl5"
		Me.lblLock.Text = "Registro bloqueado por otra sesi�n de trabajo"
		Me.lblLock.ForeColor = System.Drawing.Color.FromARGB(128, 0, 0)
		Me.lblLock.Size = New System.Drawing.Size(121, 27)
		Me.lblLock.Location = New System.Drawing.Point(14, 213)
		Me.lblLock.TabIndex = 15
		Me.lblLock.Visible = False
		Me.lblLock.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblLock.BackColor = System.Drawing.SystemColors.Control
		Me.lblLock.Enabled = True
		Me.lblLock.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLock.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLock.UseMnemonic = True
		Me.lblLock.AutoSize = False
		Me.lblLock.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLock.Name = "lblLock"
		Me.Line1.BorderColor = System.Drawing.Color.FromARGB(128, 128, 128)
		Me.Line1.X1 = 14
		Me.Line1.X2 = 73
		Me.Line1.Y1 = 207
		Me.Line1.Y2 = 207
		Me.Line1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line1.BorderWidth = 1
		Me.Line1.Visible = True
		Me.Line1.Name = "Line1"
		Me.Line2.BorderColor = System.Drawing.SystemColors.Window
		Me.Line2.X1 = 14
		Me.Line2.X2 = 73
		Me.Line2.Y1 = 208
		Me.Line2.Y2 = 208
		Me.Line2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me.Line2.BorderWidth = 1
		Me.Line2.Visible = True
		Me.Line2.Name = "Line2"
		CType(Me.cmdGuardar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdAceptar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbEstat, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtData, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Controls.Add(txtEntitat)
		Me.Controls.Add(txtPersona)
		Me.Controls.Add(Text1)
		Me.Controls.Add(txtData)
		Me.Controls.Add(cmbEstat)
		Me.Controls.Add(txtOferta)
		Me.Controls.Add(Text4)
		Me.Controls.Add(txtObservacions)
		Me.Controls.Add(cmdAceptar)
		Me.Controls.Add(cmdGuardar)
		Me.Controls.Add(Label1)
		Me.Controls.Add(lbl1)
		Me.Controls.Add(lbl2)
		Me.Controls.Add(lbl3)
		Me.Controls.Add(lbl4)
		Me.Controls.Add(lbl5)
		Me.Controls.Add(lblLock)
		Me.ShapeContainer1.Shapes.Add(Line1)
		Me.ShapeContainer1.Shapes.Add(Line2)
		Me.Controls.Add(ShapeContainer1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class