Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmSeguimentUsuari
	Inherits FormParent
	''OKPublic RegAplicacio As String
	''OKPublic Reg As String
	''OKPublic RegAnt As String
	''OKPublic ABM As String
	''OKPublic Appl As String
	''OKPublic Nkeys As Short
	''OKPublic GLO As String
	''OKPublic FlagConsulta As Boolean
	''OKPublic Opcions As String
	''OKPublic RegistreExtern As String
	''OKPublic FlagExtern As Boolean
	Private usuariExt As String
	
	Public Overrides Sub MoveReg(ByRef Accio As String)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		MouReg(Me, Accio,  , Piece(RegistreExtern, S, 1))
		BloquejaCamps()
	End Sub
	
	Public Overrides Sub FGetReg()
		BloquejaCamps()
	End Sub
	
	Public Overrides Sub Inici()
		ResetForm(Me)
		If FlagExtern = True Then
			Me.Unload()
			Exit Sub
		End If
	End Sub
	
	Private Sub cmdSessio_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSessio.ClickEvent
		If txtSessioFormativa.Text = "" Then Exit Sub
		ObreFormulari(frmSessioFormativa, "frmSessioFormativa", txtSessioFormativa.Text)
	End Sub
	
	'UPGRADE_WARNING: Form evento frmSeguimentUsuari.Activate tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmSeguimentUsuari_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		CarregaRelacions()
	End Sub
	
	Private Sub frmSeguimentUsuari_UnLoadEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles Me.UnLoadEvent
		Desbloqueja(Me)
		RegistreExtern = ""
		usuariExt = ""
		FlagExtern = False
	End Sub
	
	Private Sub cmdAceptar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdAceptar.ClickEvent
		If txtTipusSeguiment.Text = "" Then
			xMsgBox("Falta informar el tipus de seguiment", MsgBoxStyle.Information, Me.Text)
			Exit Sub
		End If
		
		If CDbl(txtTipusSeguiment.Text) = 2 And txtSessioFormativa.Text = "" Then
			xMsgBox("Falta informar la Sess� formativa", MsgBoxStyle.Information, Me.Text)
			txtSessioFormativa.Focus()
			Exit Sub
		End If
		
		If txtSessioFormativa.Text <> "" Then
			If ControlaSessio = 0 Then
				xMsgBox("La data d'alta del seguiment no correspon amb la data d'alta de la sessi� formativa !!!", MsgBoxStyle.Information, Me.Text)
				txtFechaAccion.Focus()
				Exit Sub
			End If
		End If
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		GuardaGrid()
		Inici()
	End Sub
	
	Private Sub cmdGuardar_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdGuardar.ClickEvent
		
		If txtTipusSeguiment.Text = "" Then
			xMsgBox("Falta informar el tipus de seguiment", MsgBoxStyle.Information, Me.Text)
			Exit Sub
		End If
		
		If CDbl(txtTipusSeguiment.Text) = 2 And txtSessioFormativa.Text = "" Then
			xMsgBox("Falta informar la Sess� formativa", MsgBoxStyle.Information, Me.Text)
			txtSessioFormativa.Focus()
			Exit Sub
		End If
		
		If txtSessioFormativa.Text <> "" Then
			If ControlaSessio = 0 Then
				xMsgBox("La data d'alta del seguiment no correspon amb la data d'alta de la sessi� formativa !!!", MsgBoxStyle.Information, Me.Text)
				txtFechaAccion.Focus()
				Exit Sub
			End If
		End If
		
		If GravaRegistre(Me) = False Then
			Exit Sub
		End If
		GuardaGrid()
	End Sub
	
	Private Sub frmSeguimentUsuari_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If IniciForm(Me) = False Then
			Exit Sub
		End If
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		usuariExt = Piece(RegistreExtern, S, 1)
		cmdSessio.Picture = SetIcon("WindowsSendTo", 16)
	End Sub
	
	Private Sub frmSeguimentUsuari_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = CShort(eventArgs.KeyCode)
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		ControlTeclas(Me, KeyCode, Shift)
	End Sub
	
	Private Sub frmSeguimentUsuari_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		ControlKey(Me, KeyAscii)
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
	
	
	
	Private Sub GrdRelacions_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles GrdRelacions.DoubleClick
		If GrdRelacions.Selection.FirstRow >= 1 Then
			ObreRelacions()
		End If
	End Sub
	
	
	
	Private Sub optSessio_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optSessio.ClickEvent
		Dim Index As Short = optSessio.GetIndex(eventSender)
		Select Case Index
			Case 1
				txtTipusSeguiment.Text = CStr(1) 'individual
				txtSessioFormativa.Text = ""
				txtSessioFormativa.Enabled = False
			Case 2
				txtTipusSeguiment.Text = CStr(2) 'grupal
				txtSessioFormativa.Enabled = True
		End Select
	End Sub
	
	Private Sub txtOferta_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOferta.GotFocus
		XGotFocus(Me, txtOferta)
	End Sub
	
	Private Sub txtOferta_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOferta.DoubleClick
		ConsultaTaula(Me, txtOferta)
	End Sub
	
	Private Sub txtOferta_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtOferta.LostFocus
		XLostFocus(Me, txtOferta)
	End Sub
	
	Private Sub txtOferta_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtOferta.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, txtOferta)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, txtOferta)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtSessioFormativa_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSessioFormativa.DoubleClick
		ConsultaTaula(Me, txtSessioFormativa)
	End Sub
	
	Private Sub txtSessioFormativa_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSessioFormativa.GotFocus
		XGotFocus(Me, txtSessioFormativa)
	End Sub
	
	Private Sub txtSessioFormativa_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtSessioFormativa.LostFocus
		XLostFocus(Me, txtSessioFormativa)
		If CDbl(txtTipusSeguiment.Text) = 2 Then OmpleCamps()
	End Sub
	
	Private Sub txtSessioFormativa_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtSessioFormativa.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTiempo_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTiempo.GotFocus
		XGotFocus(Me, txtTiempo)
	End Sub
	
	Private Sub txtTiempo_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTiempo.LostFocus
		XLostFocus(Me, txtTiempo)
	End Sub
	
	'UPGRADE_WARNING: El evento txtTipusSeguiment.TextChanged se puede desencadenar cuando se inicializa el formulario. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtTipusSeguiment_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusSeguiment.TextChanged
		If txtTipusSeguiment.Text = "" Then
			optSessio(1).Value = False
			optSessio(2).Value = False
		Else
			optSessio(txtTipusSeguiment).Value = True
		End If
	End Sub
	
	Private Sub txtTipusSeguiment_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusSeguiment.GotFocus
		XGotFocus(Me, txtTipusSeguiment)
	End Sub
	
	Private Sub txtTipusSeguiment_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipusSeguiment.LostFocus
		XLostFocus(Me, txtTipusSeguiment)
	End Sub
	
	Private Sub txtUsuario_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.GotFocus
		XGotFocus(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.DoubleClick
		ConsultaTaula(Me, txtUsuario)
	End Sub
	
	Private Sub txtUsuario_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtUsuario.LostFocus
		If txtUsuario.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtUsuario)
		
		If ABM = "AL" Then
			optSessio_ClickEvent(optSessio.Item(1), New System.EventArgs())
			txtTipusSeguiment.Text = CStr(1)
		End If
	End Sub
	
	Private Sub txtUsuario_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtUsuario.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtFechaAccion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaAccion.GotFocus
		XGotFocus(Me, txtFechaAccion)
	End Sub
	
	Private Sub txtFechaAccion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFechaAccion.LostFocus
		If txtFechaAccion.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtFechaAccion)
	End Sub
	
	Private Sub txtNumRegistro_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumRegistro.GotFocus
		XGotFocus(Me, txtNumRegistro)
	End Sub
	
	Private Sub txtNumRegistro_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtNumRegistro.LostFocus
		If txtNumRegistro.Text <> "" Then ABM = GetReg(Me)
		XLostFocus(Me, txtNumRegistro)
	End Sub
	
	Private Sub txtNumRegistro_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtNumRegistro.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipoAccion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoAccion.GotFocus
		XGotFocus(Me, txtTipoAccion)
	End Sub
	
	Private Sub txtTipoAccion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoAccion.DoubleClick
		ConsultaTaula(Me, txtTipoAccion)
	End Sub
	
	Private Sub txtTipoAccion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoAccion.LostFocus
		XLostFocus(Me, txtTipoAccion)
	End Sub
	
	Private Sub txtTipoAccion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipoAccion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTipoSubaccion_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoSubaccion.GotFocus
		XGotFocus(Me, txtTipoSubaccion)
	End Sub
	
	Private Sub txtTipoSubaccion_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoSubaccion.DoubleClick
		ConsultaTaula(Me, txtTipoSubaccion,  , txtTipoAccion.Text)
	End Sub
	
	Private Sub txtTipoSubaccion_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTipoSubaccion.LostFocus
		XLostFocus(Me, txtTipoSubaccion)
	End Sub
	
	Private Sub txtTipoSubaccion_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTipoSubaccion.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, Me.ActiveControl, txtTipoAccion.Text)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtTecnico_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnico.GotFocus
		XGotFocus(Me, txtTecnico)
	End Sub
	
	Private Sub txtTecnico_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnico.DoubleClick
		ConsultaTaula(Me, txtTecnico)
	End Sub
	
	Private Sub txtTecnico_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtTecnico.LostFocus
		XLostFocus(Me, txtTecnico)
	End Sub
	
	Private Sub txtTecnico_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtTecnico.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub txtServicio_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServicio.GotFocus
		XGotFocus(Me, txtServicio)
	End Sub
	
	Private Sub txtServicio_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServicio.DoubleClick
		ConsultaTaula(Me, txtServicio)
	End Sub
	
	Private Sub txtServicio_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtServicio.LostFocus
		XLostFocus(Me, txtServicio)
	End Sub
	
	Private Sub txtServicio_Validating(ByVal eventSender As System.Object, ByVal eventArgs As System.ComponentModel.CancelEventArgs) Handles txtServicio.Validating
		Dim Cancel As Boolean = eventArgs.Cancel
		Cancel = DisplayDescripcio(Me, eventSender)
		If Cancel = True Then
			GoTo EventExitSub
		End If
		Cancel = XValidate(Me, Me.ActiveControl)
		If Cancel = True Then
			GoTo EventExitSub
		End If
EventExitSub: 
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub cmbOrigen_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbOrigen.GotFocus
		XGotFocus(Me, cmbOrigen)
	End Sub
	
	Private Sub cmbOrigen_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbOrigen.LostFocus
		XLostFocus(Me, cmbOrigen)
	End Sub
	
	Private Sub cmbEstado_GotFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstado.GotFocus
		XGotFocus(Me, cmbEstado)
	End Sub
	
	Private Sub cmbEstado_LostFocus(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbEstado.LostFocus
		XLostFocus(Me, cmbEstado)
	End Sub
	
	Private Sub CarregaRelacions()
		MCache.P1 = txtUsuario.Text
		txtUsuario.Text = usuariExt
		'If usuariExt = 0 Then usuariExt = ""
		CarregaFGrid(GrdRelacions, "CGRELACIONSUSU^INSERCIO", txtUsuario.Text)
	End Sub
	
	Private Sub GuardaGrid()
		FGCreaWorkGrid(GrdRelacions, "RELACIONS")
		CacheXecute("D GRAVAREL^INSERCIO")
	End Sub
	
	Private Sub ObreRelacions()
		Dim Us As Short
		Dim Per As Short
		Dim Tip As Short
		Dim STip As Short
		
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Us = Piece(GrdRelacions.Cell(GrdRelacions.ActiveCell.Row, 1).Text, "|", 1)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Per = Piece(GrdRelacions.Cell(GrdRelacions.ActiveCell.Row, 1).Text, "|", 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Tip = Piece(GrdRelacions.Cell(GrdRelacions.ActiveCell.Row, 1).Text, "|", 3)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		STip = Piece(GrdRelacions.Cell(GrdRelacions.ActiveCell.Row, 1).Text, "|", 4)
		ObreFormulari(frmSeguimentRelacio, "frmSeguimentRelacio", Us & S & Per & S & Tip & S & STip)
		'UPGRADE_WARNING: Screen propiedad Screen.MousePointer tiene un nuevo comportamiento. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	Function ControlaSessio() As Short
		Dim Data As String
		
		ControlaSessio = 0
		CacheNetejaParametres()
		MCache.P1 = txtSessioFormativa.Text
		MCache.P2 = txtFechaAccion.Text
		
		Data = CacheXecute("S VALUE=$P(^INSESSIOF(EMP,P1),S,2)")
		
		If Data = txtFechaAccion.Text Then
			ControlaSessio = 1
		Else
			ControlaSessio = 0
		End If
		
	End Function
	
	
	Private Sub OmpleCamps()
		Dim Dades As String
		
		CacheNetejaParametres()
		MCache.P1 = txtSessioFormativa.Text
		If MCache.P1 = "" Then Exit Sub
		Dades = CacheXecute("S VALUE=$G(^INSESSIOF(EMP,P1))")
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtFechaAccion.Text = Piece(Dades, S, 2)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtServicio.Text = Piece(Dades, S, 3)
		DisplayDescripcio(Me, txtServicio)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtTecnico.Text = Piece(Dades, S, 4)
		DisplayDescripcio(Me, txtTecnico)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtTipoAccion.Text = Piece(Dades, S, 5)
		DisplayDescripcio(Me, txtTipoAccion)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtTipoSubaccion.Text = Piece(Dades, S, 6)
		DisplayDescripcio(Me, txtTipoSubaccion)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtTiempo.Value = Piece(Dades, S, 7)
		'UPGRADE_WARNING: No se puede resolver la propiedad predeterminada del objeto Piece(). Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		txtObservaciones.Text = Piece(Dades, S, 8)
		
		BloquejaCamps()
		cmdAceptar.Focus()
		
	End Sub
	
	
	Private Sub BloquejaCamps()
		If txtTipusSeguiment.Text = "2" Then
			If ABM <> "AL" Then
				txtSessioFormativa.Enabled = False
			Else
				txtSessioFormativa.Enabled = True
			End If
			frTipo.Enabled = False
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtFechaAccion.Enabled = False
			txtServicio.Enabled = False
			txtTecnico.Enabled = False
			txtTipoAccion.Enabled = False
			txtTipoSubaccion.Enabled = False
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtTiempo.Enabled = False
			txtObservaciones.Enabled = False
		Else
			frTipo.Enabled = True
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtFechaAccion.Enabled = True
			txtServicio.Enabled = True
			txtTecnico.Enabled = True
			txtTipoAccion.Enabled = True
			txtTipoSubaccion.Enabled = True
			'UPGRADE_NOTE: Enabled se actualiz� a CtlEnabled. Haga clic aqu� para obtener m�s informaci�n: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
			txtTiempo.Enabled = True
			txtObservaciones.Enabled = True
			
		End If
	End Sub
End Class
